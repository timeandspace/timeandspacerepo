//---------------------------------------------------------------------------
// AnimatedEntity.h
//---------------------------------------------------------------------------

/**
@file AnimatedEntity.h

Contiene la declaraci�n de la clase que representa una entidad gr�fica 
con animaciones.

@see Graphics::CAnimatedEntity
@see Graphics::CEntity

@author �lvaro Bl�zquez Checa
@date Marzo, 2014
*/

#ifndef __Graphics_NodeAnimatedEntity_H
#define __Graphics_NodeAnimatedEntity_H

#include "Entity.h"
#include <OgreAnimationState.h>


namespace Ogre 
{
	class AnimationState;
}

namespace Graphics 
{
	class CScene;
}

namespace Graphics 
{
	
	class CNodeAnimatedEntity : public CEntity
	{
	public:

		typedef struct NodeTrack
		{
			float time;
			Vector3 translate;
			Quaternion rotate;
			Vector3 scale;

			NodeTrack() : 
				translate(Vector3::ZERO),
				rotate(Quaternion::IDENTITY),
				scale(Vector3::ZERO)
			{
			}
		};

		/**
		Constructor para animaciones creadas por el usuario (TRAKS)

		@param animationState referencia al AnimationState de ogre;
		*/
		CNodeAnimatedEntity(Ogre::AnimationState& animationState);

		/**
		Destructor de la aplicaci�n.
		*/
		virtual ~CNodeAnimatedEntity();


		void createAnimation(Ogre::SceneNode& node, float duration, const std::vector<NodeTrack>& traks);


	protected:

		/**
		Actualiza el estado de la entidad cada ciclo.

		@param secs N�mero de milisegundos transcurridos desde la �ltima 
		llamada.
		*/
		virtual void tick(float secs);

		/**
		Puntero a la animacion definida por el usuario
		*/
		Ogre::AnimationState *_userDefinedAnimation;


	}; // class CNodeAnimatedEntity

} // namespace Graphics

#endif // __Graphics_AnimatedEntity_H
