//---------------------------------------------------------------------------
// Camera.cpp
//---------------------------------------------------------------------------

/**
@file Camera.cpp

Contiene la implementaci�n de la clase que maneja la c�mara.

@see Graphics::CCamera

@author David Llans�
@date Julio, 2010
*/

#include "Camera.h"
#include "Scene.h"

#include "BaseSubsystems/Server.h"
#include "BaseSubsystems/Math.h"

#include <assert.h>

#include <OgreCamera.h>
#include <OgreSceneNode.h>
#include <OgreSceneManager.h>

namespace Graphics 
{
	CCamera::CCamera(const std::string &name, CScene *scene, float nearClip, float farClip)
		: _scene(0), _targetDistance(-1), _cutSceneCameraNode(0), _cutSceneTargetNode(0)
	{
		_name = name;
		_scene = scene;

		// Creamos la estructura de nodos de la c�mara. Los nodos cuelgan
		// de la raiz, son globales.
		_cameraNode = _scene->getSceneMgr()->getRootSceneNode()->
			createChildSceneNode(name + "_camera_node");
		_targetNode = scene->getSceneMgr()->getRootSceneNode()->
			createChildSceneNode(name + "_target_node");

		// Hacemos que el nodo de la c�mara siempre est� mirando al nodo
		// objetivo.
		_cameraNode->setAutoTracking(true, _targetNode);
		// Fijamos el viraje de la c�mara para se mantenga paralelo al
		// suelo.
		_cameraNode->setFixedYawAxis(true);

		_camera = scene->getSceneMgr()->createCamera(name + "_camera");
		//HACK: Valores cableados de las distancias para reenderizar. 
		// Deber�an poder configurarse.
		_camera->setNearClipDistance(nearClip);
		_camera->setFarClipDistance(farClip);
		// Finalmente adjuntamos la c�mara a su nodo.
		_cameraNode->attachObject (_camera);

	} // CCamera

	//--------------------------------------------------------

	CCamera::~CCamera() 
	{
		// desacoplamos la c�mara de su nodo
		_cameraNode->detachAllObjects();
		_scene->getSceneMgr()->destroyCamera(_camera);
		_scene->getSceneMgr()->destroySceneNode(_cameraNode);
		_scene->getSceneMgr()->destroySceneNode(_targetNode);
		if (_cutSceneCameraNode)
			_scene->getSceneMgr()->destroySceneNode(_cutSceneCameraNode);
		if (_cutSceneTargetNode)
			_scene->getSceneMgr()->destroySceneNode(_cutSceneTargetNode);

	} // ~CCamera

	//--------------------------------------------------------

	void CCamera::setClipDistance(float nearDistance, float farDistance)
	{
		_camera->setNearClipDistance(nearDistance);
		_camera->setFarClipDistance(farDistance);
	}

	float CCamera::getTargetDistance() 
	{ 
		if (_targetDistance == -1)
		{
			if (_camera->getParentSceneNode() == _cutSceneCameraNode)
			{
				_targetDistance = _cutSceneCameraNode->getPosition().
					distance(_cutSceneTargetNode->getPosition());
			}
			else
			{
				_targetDistance = _cameraNode->getPosition().
					distance(_targetNode->getPosition());
			}
		}
		return _targetDistance; 
	}

	//--------------------------------------------------------

	const Vector3 &CCamera::getCameraPosition() 
	{
		return _camera->getParentSceneNode()->getPosition();
	}

	//--------------------------------------------------------

	const Vector3 &CCamera::getTargetCameraPosition() 
	{
		if (_camera->getParentSceneNode() == _cutSceneCameraNode)
			return _cutSceneTargetNode->getPosition();

		return _targetNode->getPosition();
	}

	//--------------------------------------------------------

	const Quaternion &CCamera::getCameraOrientation() 
	{
		return _camera->getOrientation();
	}

	//--------------------------------------------------------

	void CCamera::setCameraPosition(const Vector3 &newPosition)
	{
		_camera->getParentSceneNode()->setPosition(newPosition);
	}

	//--------------------------------------------------------

	void CCamera::setTargetCameraPosition(const Vector3 &newPosition)
	{
		if (_camera->getParentSceneNode() == _cutSceneCameraNode)
			_cutSceneTargetNode->setPosition(newPosition);
		else
			_targetNode->setPosition(newPosition);
	}

	//--------------------------------------------------------

	void CCamera::activateCutSceneMode()
	{
		if (_cutSceneCameraNode == NULL && _cutSceneTargetNode == NULL)
		{
			_cutSceneCameraNode = _scene->getSceneMgr()->getRootSceneNode()->
				createChildSceneNode("cutScene_camera_node");
			_cutSceneTargetNode = _scene->getSceneMgr()->getRootSceneNode()->
				createChildSceneNode("cutScene_target_node");

			// Hacemos que el nodo de la c�mara siempre est� mirando al nodo
			// objetivo.
			_cutSceneCameraNode->setAutoTracking(true, _cutSceneTargetNode);
			// Fijamos el viraje de la c�mara para se mantenga paralelo al
			// suelo.
			_cutSceneCameraNode->setFixedYawAxis(true);
		}
		_cutSceneCameraNode->setPosition(_cameraNode->getPosition());
		_cutSceneTargetNode->setPosition(_targetNode->getPosition());
		_camera->detachFromParent();
		_cutSceneCameraNode->attachObject(_camera);
	}

	//--------------------------------------------------------

	void CCamera::deactivateCutSceneMode()
	{
		_camera->detachFromParent();
		_cameraNode->attachObject(_camera);
	}

	//--------------------------------------------------------

	const Vector3* CCamera::getWorldSpaceCorners() const
	{
		return _camera->getWorldSpaceCorners();
	}

} // namespace Graphics
