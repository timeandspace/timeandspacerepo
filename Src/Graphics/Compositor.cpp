/**
@file Compositor.h

Contiene la implementación de la clase que maneja los compositiors (efectos de post-proceso).

@author Alejandro Pérez Alonso

@date Junio, 2014
*/

#include "Compositor.h"
#include "Scene.h"

#include <OgreCompositorManager.h>
#include <OgreCompositorInstance.h>

namespace Graphics
{
	CCompositor::CCompositor(const std::string& name) : _name(name), _compositorInstance(NULL), _viewport(NULL)
	{

	}

	//---------------------------------------------------------

	CCompositor::~CCompositor()
	{
		//Ogre::CompositorManager::getSingleton().removeCompositor(_viewport, _name);
	}

	//---------------------------------------------------------

	bool CCompositor::attachToScene(Ogre::Viewport* viewport)
	{
		_viewport = viewport;
		_compositorInstance = Ogre::CompositorManager::getSingleton().
			addCompositor(_viewport, _name);


		/*
		Ogre::ResourceManager::ResourceMapIterator it = 
			Ogre::CompositorManager::getSingleton().getResourceIterator();

		while (it.hasMoreElements())
		{
			Ogre::ResourcePtr mRes = it.getNext(); 
			std::string s = mRes->getName();
		}
		*/

		return (_compositorInstance != NULL) ? true : false;
	}

	void CCompositor::setEnabled(bool value)
	{
		_compositorInstance->setEnabled(value);
	}

	//---------------------------------------------------------

	bool CCompositor::isEnabled()
	{
		return _compositorInstance->getEnabled();
	}

}
