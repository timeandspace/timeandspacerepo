//---------------------------------------------------------------------------
// Server.cpp
//---------------------------------------------------------------------------

/**
@file Server.cpp

Contiene la implementaci�n de la clase principal de gr�ficos, la que permite crear
la ventana, etc.

@see Graphics::CServer

@author David Llans�
@date Julio, 2010

@author Alejandro P�rez Alonso
@date Abril, 2014
*/

#include "Server.h"
#include "Scene.h"
#include "Entity.h"
#include "Camera.h"

#include "BaseSubsystems/Server.h"
#include "BaseSubsystems/Math.h"

#include <assert.h>

#include <OgreRoot.h>
#include <OgreRenderWindow.h>
#include <OgreWindowEventUtilities.h>
#include <OgreResourceGroupManager.h>
#include <OgreEntity.h>
#include <OgreParticleSystem.h>
#include <OgreSceneQuery.h>
#include <OgreSubMesh.h>
#include <OgreSubEntity.h>
#include <OgreParticleEmitter.h>

namespace Graphics 
{
	CServer *CServer::_instance = 0;

	CServer::CServer() : _root(0), _renderWindow(0), _activeScene(0), _dummyScene(0)
	{
		assert(!_instance && "Segunda inicializaci�n de Graphics::CServer no permitida!");

		_instance = this;

	} // CServer

	//--------------------------------------------------------

	CServer::~CServer() 
	{
		assert(_instance);

		_instance = 0;

	} // ~CServer

	//--------------------------------------------------------

	bool CServer::Init() 
	{
		assert(!_instance && "Segunda inicializaci�n de Graphics::CServer no permitida!");

		new CServer();

		if (!_instance->open())
		{
			Release();
			return false;
		}

		return true;

	} // Init

	//--------------------------------------------------------

	void CServer::Release()
	{
		if(_instance)
		{
			_instance->close();
			delete _instance;
		}

	} // Release

	//--------------------------------------------------------

	void CServer::unLoadResourceGroup(const std::string & groupName)
	{
		Ogre::ResourceGroupManager::getSingletonPtr()->unloadResourceGroup(groupName);
	}

	//--------------------------------------------------------

	void CServer::loadResourceGroup(const std::string & groupName)
	{
		Ogre::ResourceGroupManager::getSingletonPtr()->loadResourceGroup(groupName);
	}

	//--------------------------------------------------------

	void CServer::setResourceGroupListener()
	{

	}

	//--------------------------------------------------------

	float CServer::getAverageFPS()
	{
		return _renderWindow->getAverageFPS();
	}

	bool CServer::open()
	{
		if(!BaseSubsystems::CServer::getSingletonPtr())
			return false;

		_root = BaseSubsystems::CServer::getSingletonPtr()->getOgreRoot();

		_renderWindow = BaseSubsystems::CServer::getSingletonPtr()->getRenderWindow();

		// Creamos la escena dummy para cuando no hay ninguna activa.
		_dummyScene = createScene("dummy_scene");

		// Por defecto la escena activa es la dummy
		setScene(_dummyScene);

		return true;

	} // open

	//--------------------------------------------------------

	void CServer::close() 
	{
		if(_activeScene)
		{
			_activeScene->deactivate();
			_activeScene = 0;
		}
		while(!_scenes.empty())
		{
			removeScene(_scenes.begin());
		}

	} // close

	//--------------------------------------------------------

	typedef std::pair<std::string,CScene*> TStringScenePar;

	CScene* CServer::createScene(const std::string& name)
	{
		//Nos aseguramos de que no exista ya una escena con este nombre.
		assert(_scenes.find(name)==_scenes.end() && 
			"Ya se ha creado una escena con este nombre.");

		CScene *scene = new CScene(name);
		TStringScenePar ssp(name,scene);
		_scenes.insert(ssp);
		return scene;

	} // createScene

	//--------------------------------------------------------

	void CServer::activateScene() 
	{
		if(_activeScene)
			_activeScene->activate();
	} // activateScene

	//--------------------------------------------------------

	void CServer::deactivateScene() 
	{
		if(_activeScene)
			_activeScene->deactivate();
	} // deactivateScene

	//--------------------------------------------------------

	void CServer::pauseScene()
	{
		if(_activeScene)
			_activeScene->pause();
	} // pauseScene

	//--------------------------------------------------------

	void CServer::resumeScene()
	{
		if(_activeScene)
			_activeScene->resume();
	} // resumeScene

	//--------------------------------------------------------

	void CServer::removeScene(CScene* scene)
	{
		// Si borramos la escena activa tenemos que quitarla.
		if(_activeScene == scene)
			_activeScene = 0;
		_scenes.erase(scene->getName());
		delete scene;

	} // removeScene

	//--------------------------------------------------------

	void CServer::removeScene(const std::string& name)
	{
		CScene* scene = (*_scenes.find(name)).second;
		removeScene(scene);

	} // removeScene

	//--------------------------------------------------------

	void CServer::removeScene(TScenes::const_iterator iterator)
	{
		CScene* scene = (*iterator).second;
		// Si borramos la escena activa tenemos que quitarla.
		if(_activeScene == scene)
			_activeScene = 0;
		_scenes.erase(iterator);
		delete scene;

	} // removeScene

	//--------------------------------------------------------

	void CServer::setScene(CScene* scene)
	{
		// En caso de que hubiese una escena activa la desactivamos.
		if(_activeScene)
			_activeScene->deactivate();

		if(scene)
		{
			// Sanity check. Nos aseguramos de que la escena pertenezca 
			// al servidor. Aunque nadie m�s puede crear escenas...
			assert((*_scenes.find(scene->getName())).second == scene && 
				"Esta escena no pertenece al servidor");

			_activeScene = scene;
		}
		// Si se a�ade NULL ponemos la escena dummy.
		else
			_activeScene = _dummyScene;

		_activeScene->activate();

	} // createScene

	//--------------------------------------------------------

	void CServer::setScene(const std::string& name)
	{
		// En caso de que hubiese una escena activa la desactivamos.
		if(_activeScene)
			_activeScene->deactivate();

		// Nos aseguramos de que exista una escena con este nombre.
		assert(_scenes.find(name) != _scenes.end() && 
			"Ya se ha creado una escena con este nombre.");
		_activeScene = (*_scenes.find(name)).second;

		_activeScene->activate();

	} // createScene

	//--------------------------------------------------------

	CEntity* CServer::getEntity(const std::string &entityName)
	{
		Ogre::Entity* ent = _activeScene->getSceneMgr()->getEntity(entityName);
		CScene::TEntityList::const_iterator it = _activeScene->_dynamicEntities.begin();
		CScene::TEntityList::const_iterator end = _activeScene->_dynamicEntities.end();
		for ( ; it != end; ++it)
		{
			if ((*it)->getEntity() == ent)
				return (*it);
		}
		return NULL;
	} //getEntity

	//--------------------------------------------------------

	void CServer::createParticleSystem(const std::string &entityName, const std::string &name, 
		const std::string &templateName)
	{
		CEntity* ent = getEntity(entityName);
		if (!ent) return;
		/*assert(!_activeScene->getSceneMgr()->hasParticleSystem(name) && 
		"Ya Existe una particula con el mismo nombre");*/
		Ogre::ParticleSystem* particleSystem = NULL;
		if (_activeScene->getSceneMgr()->hasParticleSystem(name)) 
		{
			particleSystem = _activeScene->getSceneMgr()->getParticleSystem(name);
			_activeScene->getSceneMgr()->destroyParticleSystem(particleSystem);
		}
		particleSystem = _activeScene->getSceneMgr()->createParticleSystem(name, templateName);

		ent->getEntity()->getParentSceneNode()->createChildSceneNode()->attachObject(particleSystem);
	} // createParticleSystem

	//--------------------------------------------------------

	void CServer::createParticleSystem(const std::string &entityName, const std::string &name, 
		const std::string &templateName, const Ogre::Vector3 &position)
	{
		CEntity* ent = getEntity(entityName);
		/*assert(!_activeScene->getSceneMgr()->hasParticleSystem(name) && 
		"Ya Existe una particula con el mismo nombre");*/
		Ogre::ParticleSystem* particleSystem = NULL;
		if (_activeScene->getSceneMgr()->hasParticleSystem(name)) 
		{
			particleSystem = _activeScene->getSceneMgr()->getParticleSystem(name);
			_activeScene->getSceneMgr()->destroyParticleSystem(particleSystem);
		}

		particleSystem = _activeScene->getSceneMgr()->createParticleSystem(name, templateName);

		ent->getEntity()->getParentSceneNode()->createChildSceneNode()->attachObject(particleSystem);
		particleSystem->getParentNode()->setPosition(position);
	} // createParticleSystem

	//-------------------------------------------------------

	void CServer::activateParticleSystem(std::string name, bool active)
	{
		if (_activeScene->getSceneMgr()->hasParticleSystem(name)) 
		{
			Ogre::ParticleSystem *particleSystem = _activeScene->getSceneMgr()->getParticleSystem(name);
			particleSystem->setVisible(active);
		}
	}

	//-------------------------------------------------------

	void CServer::setParticleSystemDirection(std::string name, const Vector3 &direction)
	{
		if (_activeScene->getSceneMgr()->hasParticleSystem(name)) 
		{
			Ogre::ParticleSystem *particleSystem = _activeScene->getSceneMgr()->getParticleSystem(name);
			particleSystem->getEmitter(0)->setDirection(direction);

		}
	}

	//-------------------------------------------------------

	void CServer::destroyParticleSystem(const std::string &name)
	{
		if (_activeScene->getSceneMgr()->hasParticleSystem(name))
		{
			/*Ogre::ParticleSystem* particleSystem = 
			_activeScene->getSceneMgr()->getParticleSystem(name);*/
			//ent->getEntity()->getParentSceneNode()->detachObject(particleSystem);

			_activeScene->getSceneMgr()->destroyParticleSystem(name);
		}

	} // destroyParticleSystem


	//--------------------------------------------------------

	void CServer::createSpotLight(const std::string &entityName, const std::string &name)
	{
		CEntity* ent = getEntity(entityName);
		if (!ent) return;

		if(_activeScene)
		{
			Ogre::Light *spotLight = _activeScene->getSceneMgr()->createLight(name);
			spotLight->setDiffuseColour(Ogre::ColourValue(1.0f,1.0f,1.0f));
			spotLight->setType(Ogre::Light::LT_SPOTLIGHT);
			spotLight->setSpotlightRange(Ogre::Degree(35), Ogre::Degree(50));

			ent->getEntity()->getParentSceneNode()->createChildSceneNode()->attachObject(spotLight);
		}
		else
			return;
	}

	/**
	Funci�n auxiliar que obtiene la informaci�n de un mesh
	Para m�s info: 
	http://www.ogre3d.org/tikiwiki/tiki-index.php?page=Raycasting+to+the+polygon+level
	*/
	void getMeshInformation(Ogre::Entity *entity,
		size_t &vertex_count,
		Ogre::Vector3* &vertices,
		size_t &index_count,
		unsigned long* &indices,
		const Ogre::Vector3 &position,
		const Ogre::Quaternion &orient,
		const Ogre::Vector3 &scale)
	{
		bool added_shared = false;
		size_t current_offset = 0;
		size_t shared_offset = 0;
		size_t next_offset = 0;
		size_t index_offset = 0;
		vertex_count = index_count = 0;

		Ogre::MeshPtr mesh = entity->getMesh();


		bool useSoftwareBlendingVertices = entity->hasSkeleton();

		if (useSoftwareBlendingVertices)
		{
			entity->_updateAnimation();
		}

		// Calculate how many vertices and indices we're going to need
		for (unsigned short i = 0; i < mesh->getNumSubMeshes(); ++i)
		{
			Ogre::SubMesh* submesh = mesh->getSubMesh( i );

			// We only need to add the shared vertices once
			if(submesh->useSharedVertices)
			{
				if( !added_shared )
				{
					vertex_count += mesh->sharedVertexData->vertexCount;
					added_shared = true;
				}
			}
			else
			{
				vertex_count += submesh->vertexData->vertexCount;
			}

			// Add the indices
			index_count += submesh->indexData->indexCount;
		}


		// Allocate space for the vertices and indices
		vertices = new Ogre::Vector3[vertex_count];
		indices = new unsigned long[index_count];

		added_shared = false;

		// Run through the submeshes again, adding the data into the arrays
		for ( unsigned short i = 0; i < mesh->getNumSubMeshes(); ++i)
		{
			Ogre::SubMesh* submesh = mesh->getSubMesh(i);

			//----------------------------------------------------------------
			// GET VERTEXDATA
			//----------------------------------------------------------------

			//Ogre::VertexData* vertex_data = submesh->useSharedVertices ? mesh->sharedVertexData : submesh->vertexData;
			Ogre::VertexData* vertex_data;

			//When there is animation:
			if(useSoftwareBlendingVertices)
				vertex_data = submesh->useSharedVertices ? entity->_getSkelAnimVertexData() : entity->getSubEntity(i)->_getSkelAnimVertexData();
			else
				vertex_data = submesh->useSharedVertices ? mesh->sharedVertexData : submesh->vertexData;


			if((!submesh->useSharedVertices)||(submesh->useSharedVertices && !added_shared))
			{
				if(submesh->useSharedVertices)
				{
					added_shared = true;
					shared_offset = current_offset;
				}

				const Ogre::VertexElement* posElem =
					vertex_data->vertexDeclaration->findElementBySemantic(Ogre::VES_POSITION);

				Ogre::HardwareVertexBufferSharedPtr vbuf =
					vertex_data->vertexBufferBinding->getBuffer(posElem->getSource());

				unsigned char* vertex =
					static_cast<unsigned char*>(vbuf->lock(Ogre::HardwareBuffer::HBL_READ_ONLY));

				// There is _no_ baseVertexPointerToElement() which takes an Ogre::Real or a double
				//  as second argument. So make it float, to avoid trouble when Ogre::Real will
				//  be comiled/typedefed as double:
				//      Ogre::Real* pReal;
				float* pReal;

				for( size_t j = 0; j < vertex_data->vertexCount; ++j, vertex += vbuf->getVertexSize())
				{
					posElem->baseVertexPointerToElement(vertex, &pReal);

					Ogre::Vector3 pt(pReal[0], pReal[1], pReal[2]);

					vertices[current_offset + j] = (orient * (pt * scale)) + position;
				}

				vbuf->unlock();
				next_offset += vertex_data->vertexCount;
			}


			Ogre::IndexData* index_data = submesh->indexData;
			size_t numTris = index_data->indexCount / 3;
			Ogre::HardwareIndexBufferSharedPtr ibuf = index_data->indexBuffer;

			bool use32bitindexes = (ibuf->getType() == Ogre::HardwareIndexBuffer::IT_32BIT);

			unsigned long*  pLong = static_cast<unsigned long*>(ibuf->lock(Ogre::HardwareBuffer::HBL_READ_ONLY));
			unsigned short* pShort = reinterpret_cast<unsigned short*>(pLong);


			size_t offset = (submesh->useSharedVertices)? shared_offset : current_offset;
			size_t index_start = index_data->indexStart;
			size_t last_index = numTris*3 + index_start;

			if (use32bitindexes)
				for (size_t k = index_start; k < last_index; ++k)
				{
					indices[index_offset++] = pLong[k] + static_cast<unsigned long>( offset );
				}

			else
				for (size_t k = index_start; k < last_index; ++k)
				{
					indices[ index_offset++ ] = static_cast<unsigned long>( pShort[k] ) +
						static_cast<unsigned long>( offset );
				}

				ibuf->unlock();
				current_offset = next_offset;
		}
	}

	//--------------------------------------------------------

	bool CServer::raycastClosest(const Ogre::Vector3& shootingPoint, Ogre::Vector3 &impactPoint, std::string &hitEntityName)
	{
		float height = _activeScene->getSceneMgr()->getCurrentViewport()->getHeight();
		float width = _activeScene->getSceneMgr()->getCurrentViewport()->getWidth();
		Ogre::Ray outRay;
		// Creamos un rayo desde el centro de la c�mara al centro del viewport
		_activeScene->getCamera()->getCamera()->
			getCameraToViewportRay(height/2, width/2, &outRay);

		// Obtenemos la distancia m�nima (al cuadrado).
		float minDistance = shootingPoint.squaredDistance(outRay.getOrigin());

		Ogre::RaySceneQuery *raycast = _activeScene->getSceneMgr()->createRayQuery(outRay);
		raycast->setSortByDistance(true);

		Ogre::RaySceneQueryResult &result = raycast->execute();
		//Ogre::Vector3 closest_result;
		float closest_distance = FLT_MAX;
		for (unsigned int i = 0; i < result.size(); ++i)
		{
			if (/*result[i].distance != 0
				&&*/ result[i].movable->getMovableType().compare("Entity") == 0)
			{
				Ogre::Entity *pEntity = static_cast<Ogre::Entity*>(result[i].movable); 
				// mesh data to retrieve         
				size_t vertex_count;
				size_t index_count;
				Ogre::Vector3 *vertices;
				unsigned long *indices;

				// Obtenemos la info del mesh, para comprobar despues si colisiona
				getMeshInformation(pEntity, vertex_count, vertices, index_count, indices,
					pEntity->getParentNode()->_getDerivedPosition(),
					pEntity->getParentNode()->_getDerivedOrientation(),
					pEntity->getParentNode()->_getDerivedScale());

				for (int j = 0; j < static_cast<int>(index_count); j += 3)
				{
					// check for a hit against this triangle
					std::pair<bool, Ogre::Real> hit = Ogre::Math::intersects(outRay, vertices[indices[j]],
						vertices[indices[j+1]], vertices[indices[j+2]], true, false);

					// if it was a hit check if its the closest
					if (hit.first)
					{
						if (hit.second < closest_distance && 
							(hit.second * hit.second) > minDistance)
						{
							closest_distance = hit.second;
							hitEntityName = pEntity->getName();
							break;
						}
					}
				}
				// free the verticies and indicies memory
				delete[] vertices;
				delete[] indices;
			}
		}

		delete raycast;

		if (closest_distance != FLT_MAX) 
		{
			impactPoint = outRay.getPoint(closest_distance);

			//#ifdef _DEBUG
			//			Ogre::SceneNode* mSceneNode = _activeScene->getSceneMgr()->
			//				getRootSceneNode()->createChildSceneNode();
			//			Ogre::Entity* mEntity = NULL;
			//			if (! _activeScene->getSceneMgr()->hasEntity("mySphere"))
			//			{
			//				mEntity = _activeScene->getSceneMgr()->
			//					createEntity("mySphere", "coverPoint.mesh");
			//				mEntity->setMaterialName("NoMaterial");
			//				mSceneNode->attachObject(mEntity);
			//				mSceneNode->setPosition(impactPoint);
			//				mSceneNode->setScale(0.05, 0.05, 0.05);
			//			}
			//			else
			//			{
			//				mEntity = _activeScene->getSceneMgr()->getEntity("mySphere");
			//				mEntity->getParentSceneNode()->setPosition(impactPoint);
			//			}
			//
			//#endif

			return true;
		}

		impactPoint = outRay.getPoint(150); // Antes 1000

		return false;
	}

	//--------------------------------------------------------

	bool CServer::raycastClosest(const Ogre::Vector3& shootingPoint, const Ogre::Ray & ray, 
		Ogre::Vector3 &impactPoint, std::string &hitEntityName)
	{

		// Obtenemos la distancia m�nima (al cuadrado).
		float minDistance = shootingPoint.squaredDistance(ray.getOrigin());

		Ogre::RaySceneQuery *raycast = _activeScene->getSceneMgr()->createRayQuery(ray);
		raycast->setSortByDistance(true);

		Ogre::RaySceneQueryResult &result = raycast->execute();
		//Ogre::Vector3 closest_result;
		float closest_distance = FLT_MAX;
		for (unsigned int i = 0; i < result.size(); ++i)
		{
			if (/*result[i].distance != 0
				&&*/ result[i].movable->getMovableType().compare("Entity") == 0)
			{
				Ogre::Entity *pEntity = static_cast<Ogre::Entity*>(result[i].movable); 
				// mesh data to retrieve         
				size_t vertex_count;
				size_t index_count;
				Ogre::Vector3 *vertices;
				unsigned long *indices;

				// Obtenemos la info del mesh, para comprobar despues si colisiona
				getMeshInformation(pEntity, vertex_count, vertices, index_count, indices,
					pEntity->getParentNode()->_getDerivedPosition(),
					pEntity->getParentNode()->_getDerivedOrientation(),
					pEntity->getParentNode()->_getDerivedScale());

				for (int j = 0; j < static_cast<int>(index_count); j += 3)
				{
					// check for a hit against this triangle
					std::pair<bool, Ogre::Real> hit = Ogre::Math::intersects(ray, vertices[indices[j]],
						vertices[indices[j+1]], vertices[indices[j+2]], true, false);

					// if it was a hit check if its the closest
					if (hit.first)
					{
						if (hit.second < closest_distance && 
							(hit.second * hit.second) > minDistance)
						{
							closest_distance = hit.second;
							hitEntityName = pEntity->getName();
							break;
						}
					}
				}
				// free the verticies and indicies memory
				delete[] vertices;
				delete[] indices;
			}
		}

		delete raycast;

		if (closest_distance != FLT_MAX) 
		{
			impactPoint = ray.getPoint(closest_distance);

			//#ifdef _DEBUG
			//			Ogre::SceneNode* mSceneNode = _activeScene->getSceneMgr()->
			//				getRootSceneNode()->createChildSceneNode();
			//			Ogre::Entity* mEntity = NULL;
			//			if (! _activeScene->getSceneMgr()->hasEntity("mySphere2"))
			//			{
			//				mEntity = _activeScene->getSceneMgr()->
			//					createEntity("mySphere2", "coverPoint.mesh");
			//				mEntity->setMaterialName("coverPoint");
			//				mSceneNode->attachObject(mEntity);
			//				mSceneNode->setPosition(impactPoint);
			//				mSceneNode->setScale(0.5, 0.5, 0.5);
			//			}
			//			else
			//			{
			//				mEntity = _activeScene->getSceneMgr()->getEntity("mySphere2");
			//				mEntity->getParentSceneNode()->setPosition(impactPoint);
			//			}
			//
			//#endif

			return true;
		}

		impactPoint = ray.getPoint(150);

		return false;
	}

	//--------------------------------------------------------

	bool CServer::rayCastSingle(const Ogre::Ray &ray,const std::string &entityName)
	{

		Ogre::RaySceneQuery *raycast = _activeScene->getSceneMgr()->createRayQuery(ray);
		raycast->setSortByDistance(true);

		Ogre::RaySceneQueryResult &result = raycast->execute();

		for (unsigned int i = 0; i < result.size(); ++i)
		{
			if (result[i].movable->getMovableType().compare("Entity") == 0)
			{
				Ogre::Entity *pEntity = static_cast<Ogre::Entity*>(result[i].movable); 
				std::string a = pEntity->getName();
				if(i==1 && pEntity->getName() == entityName)
				{
					delete raycast;
					return true;
				}
				else if(i == 1)
				{
					delete raycast;
					return false;
				}
			}
		}
		return false;
	}

	//--------------------------------------------------------

	Vector3 CServer::getBonePosition(std::string entityName, std::string boneName)
	{
		Ogre::Entity *ent = _activeScene->getSceneMgr()->getEntity(entityName);
		return ent->getParentNode()->convertLocalToWorldPosition(
			ent->getSkeleton()->getBone(boneName)->_getDerivedPosition());
	}

	//--------------------------------------------------------

	void CServer::setMaterial(std::string entityName, std::string materialName)
	{
		CEntity *ent = getEntity(entityName);
		if (ent != NULL)
		{
			ent->getEntity()->setMaterialName(materialName);
		}
	}

	//--------------------------------------------------------
	void CServer::tick(float secs) 
	{
		if(_activeScene != _dummyScene)
			_activeScene->tick(secs);
		if(_root)
		{
			// Actualizamos todas las ventanas de reenderizado.
			Ogre::WindowEventUtilities::messagePump();
			// Reenderizamos un frame
			_root->renderOneFrame(secs);
		}
	} // tick

} // namespace Graphics
