//---------------------------------------------------------------------------
// Scene.cpp
//---------------------------------------------------------------------------

/**
@file Scene.cpp

Contiene la implementaci�n de la clase contenedora de los elementos
de una escena.

@see Graphics::CScene

@author David Llans�
@date Julio, 2010
*/

#include "Scene.h"
#include "Camera.h"
#include "Server.h"
#include "StaticEntity.h"
#include "BaseSubsystems/Server.h"
#include "ParticleSystem.h"
#include "CompositorManager.h"

#include <assert.h>

#include <OgreRoot.h>
#include <OgreSceneManager.h>
#include <OgreRenderWindow.h>
#include <OgreViewport.h>
#include <OgreStaticGeometry.h>
#include <OgreColourValue.h>
#include <OgreParticleSystem.h>
#include <OgreBillboardSet.h>
#include <OgreBillboard.h>

#include <ParticleUniverseSystemManager.h>

namespace Graphics 
{
	CScene::CScene(const std::string& name) : _viewport(0), 
		_staticGeometry(0), _directionalLight(0), _active(true)
	{
		_root = BaseSubsystems::CServer::getSingletonPtr()->getOgreRoot();
		_sceneMgr = _root->createSceneManager(Ogre::ST_INTERIOR, name);
		_camera = new CCamera(name,this);
		_name = name;

	} // CScene

	//--------------------------------------------------------

	CScene::~CScene() 
	{
		// Liberamos el Compositor Manager
		CCompositorManager::Release();

		ParticleUniverse::ParticleSystemManager::getSingletonPtr()->
			destroyAllParticleSystems(_sceneMgr);
		deactivate();
		_sceneMgr->destroyStaticGeometry(_staticGeometry);
		_sceneMgr->destroyAllParticleSystems();
		delete _camera;
		_root->destroySceneManager(_sceneMgr);

	} // ~CScene

	//--------------------------------------------------------

	bool CScene::addEntity(CEntity* entity)
	{
		if(!entity->attachToScene(this))
			return false;
		_dynamicEntities.push_back(entity);
		return true;

	} // addEntity

	//--------------------------------------------------------

	bool CScene::addStaticEntity(CStaticEntity* entity)
	{
		if(!entity->attachToScene(this))
			return false;
		_staticEntities.push_back(entity);
		return true;

	} // addStaticEntity

	//--------------------------------------------------------

	void CScene::removeEntity(CEntity* entity)
	{
		entity->deattachFromScene();
		_dynamicEntities.remove(entity);

	} // addEntity

	//--------------------------------------------------------

	void CScene::removeStaticEntity(CStaticEntity* entity)
	{
		entity->deattachFromScene();
		_staticEntities.remove(entity);

	} // addStaticEntity

	//--------------------------------------------------------

	void CScene::activate()
	{
		buildStaticGeometry();

		_viewport = BaseSubsystems::CServer::getSingletonPtr()->getRenderWindow()
			->addViewport(_camera->getCamera());

		//_sceneMgr->setShadowUseInfiniteFarPlane(false);
		_camera->getCamera()->setAspectRatio(Ogre::Real(_viewport->getActualWidth()) /
			Ogre::Real(_viewport->getActualHeight()));

		// Llamamos al compositor manager para que cargue en el viewport los compositors necesarios
		if (_name != "dummy_scene")
			CCompositorManager::Init(this);

		// Shadows
		_sceneMgr->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);
		//_sceneMgr->setShadowTextureCount(2);
		//_sceneMgr->setShadowTextureSize();
		_sceneMgr->setShadowFarDistance(30);
		//_sceneMgr->setShadowTextureFadeStart(0.7);
		//_sceneMgr->setShadowTextureFadeEnd (0.9);

	} // activate

	//--------------------------------------------------------

	void CScene::deactivate()
	{
		if(_directionalLight)
		{
			_sceneMgr->destroyLight(_directionalLight);
			_directionalLight = 0;
		}
		if(_viewport)
		{

			BaseSubsystems::CServer::getSingletonPtr()->getRenderWindow()->
				removeViewport(_viewport->getZOrder());
			_viewport = 0;
		}

	} // deactivate

	//--------------------------------------------------------

	void CScene::pause()
	{
		_active = false;
		ParticleUniverse::ParticleSystem *particle = ParticleUniverse::ParticleSystemManager::getSingletonPtr()
			->getParticleSystem("Player");

		if (particle) particle->pause();
	}

	//--------------------------------------------------------

	void CScene::resume()
	{
		_active = true;

		ParticleUniverse::ParticleSystem *particle = ParticleUniverse::ParticleSystemManager::getSingletonPtr()
			->getParticleSystem("Player");

		if (particle) particle->resume();
	}

	//--------------------------------------------------------

	void CScene::tick(float secs)
	{	
		if (_active)
		{
			TEntityList::const_iterator it = _dynamicEntities.begin();
			TEntityList::const_iterator end = _dynamicEntities.end();
			for(; it != end; ++it)
				(*it)->tick(secs);
		}

	} // tick

	//--------------------------------------------------------

	void CScene::buildStaticGeometry()
	{
		if(!_staticGeometry && !_staticEntities.empty())
		{
			_staticGeometry = 
				_sceneMgr->createStaticGeometry("static");

			TStaticEntityList::const_iterator it = _staticEntities.begin();
			TStaticEntityList::const_iterator end = _staticEntities.end();
			for(; it != end; ++it)
				(*it)->addToStaticGeometry();

			_staticGeometry->build();
		}

	} // buildStaticGeometry

	//--------------------------------------------------------

	void CScene::setBackgrounColour(const Ogre::Vector3& colour)
	{
		assert(_viewport && "el viewport es NULL");
		_viewport->setBackgroundColour(Ogre::ColourValue(colour.x, colour.y, colour.z));
	}

	//--------------------------------------------------------

	void CScene::createDirectionalLight(const Ogre::Vector3& diffuseColour, const Ogre::Vector3& specularColour,const Ogre::Vector3& direction)
	{
		_directionalLight = _sceneMgr->createLight("directionalLight");
		_directionalLight->setDiffuseColour(diffuseColour.x,diffuseColour.y,diffuseColour.z);
		_directionalLight->setSpecularColour(specularColour.x,specularColour.y,specularColour.z);
		_directionalLight->setType(Ogre::Light::LT_DIRECTIONAL);
		_directionalLight->setDirection(direction);
	}

	//--------------------------------------------------------

	void CScene::setAmbientLight(float red, float green, float blue)
	{
		_sceneMgr->setAmbientLight(Ogre::ColourValue(red,green,blue));

	}

	//--------------------------------------------------------

	void CScene::createFog()
	{
		_sceneMgr->setFog(Ogre::FOG_LINEAR,Ogre::ColourValue(0.5f,0.5f,0.5f),5,100);
	}

	//--------------------------------------------------------

	void CScene::createSkyDome()
	{
		//enable="true" active="true" distance="2000.0" drawFirst="false" material="MySkyDomeStars" curvature="2" tiling="30"
		_sceneMgr->setSkyDome(true, "MySkyDomeStars", 2, 30, 500.0f, false, Ogre::Quaternion::IDENTITY, 16, 16, -1, Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
	}

	//--------------------------------------------------------

	//--------------------------------------------------------

	Ogre::BillboardSet* CScene::createBillBoardSet(const std::string &name, const std::string &materialName, const Ogre::Vector2 &dim)
	{

		if(_sceneMgr->hasBillboardSet(name))
			return NULL;

		Ogre::SceneNode *bbSetSceneNode;
		bbSetSceneNode =_sceneMgr->getRootSceneNode()->createChildSceneNode();

		//Creamos el grupo de billboards...
		Ogre::BillboardSet *bbSet =_sceneMgr->createBillboardSet(name);
		// ... establecemos el material ...
		bbSet->setMaterialName(materialName);
		// ... y el tama�o predefinido de los billboards.
		bbSet->setDefaultDimensions(dim.x, dim.y);
		bbSet->setUseAccurateFacing(true);

		// Colocamos el BilboardSet en el nodo de escena raiz
		bbSetSceneNode->attachObject(bbSet);

		return bbSet;
	}

	//--------------------------------------------------------

	Ogre::Billboard* CScene::createBillBoard(const std::string &billBoardSetName, const Ogre::Vector3 &pos)
	{
		Ogre::BillboardSet* bbSet = _sceneMgr->getBillboardSet(billBoardSetName);

		if(!bbSet)
			return NULL;

		Ogre::Billboard* billboard = bbSet->createBillboard(pos);

		return billboard;
	}


	//--------------------------------------------------------

	void CScene::setCameraClipDistance(float nearClip, float farClip)
	{
		_camera->setClipDistance(nearClip,farClip);
	}

	//--------------------------------------------------------

	CParticleSystem* CScene::createParticleSystem(const std::string &name, const std::string &templateName)
	{
		CParticleSystem* particleSystem = new CParticleSystem(name, templateName);

		return particleSystem->attachToScene(this) ? particleSystem : NULL;
	}

	//--------------------------------------------------------

	void CScene::destroyParticleSystem(const std::string &name)
	{
		ParticleUniverse::ParticleSystemManager::getSingletonPtr()->
			destroyParticleSystem(name, _sceneMgr);
	}

	//--------------------------------------------------------


} // namespace Graphics
