/**
@file CompositorManager.cpp

Implementación del manager de compositors. Controla los efectos de post-proceso.

@author Alejandro Pérez Alonso
@date Junio, 2014
*/

#include "CompositorManager.h"

#include "Compositor.h"
#include "Scene.h"

#include <OgreCompositorManager.h>
#include <OgreResourceManager.h>
#include <OgreCompositorInstance.h>

namespace Graphics
{
	CCompositorManager* CCompositorManager::_instance = 0;

	CCompositorManager::CCompositorManager(CScene *scene) : _scene(scene)
	{
		_instance = this;
		_compositors.clear();
	}

	//------------------------------------------------------------------

	CCompositorManager::~CCompositorManager()
	{
		_instance = NULL;
	}

	//------------------------------------------------------------------

	CCompositorManager* CCompositorManager::getSingletonPtr()
	{ 
		return _instance; 
	}

	//------------------------------------------------------------------

	bool CCompositorManager::Init(CScene *scene)
	{
		if (_instance != NULL)
		{
			_instance->activate(scene);
			return true;
		}

		else
		{
			new CCompositorManager(scene);

			if(!_instance->open())
			{
				Release();
				return false;
			}
		}

		return true;
	}

	//------------------------------------------------------------------

	bool CCompositorManager::Release()
	{
		if (_instance)
		{
			_instance->close();
			delete _instance;
			_instance = NULL;
		}

		return true;
	}

	//------------------------------------------------------------------

	bool CCompositorManager::open()
	{
		Ogre::ResourceManager::ResourceMapIterator it = 
			Ogre::CompositorManager::getSingleton().getResourceIterator();

		while (it.hasMoreElements())
		{
			Ogre::ResourcePtr mRes = it.getNext(); 
			CCompositor *comp = new CCompositor(mRes->getName());
			comp->attachToScene(_scene->getViewport());
			_compositors.push_back(comp);
		}

		return true;
	}

	//------------------------------------------------------------------

	void CCompositorManager::close()
	{
		for (unsigned int i = 0; i < _compositors.size(); ++i)
		{
			delete _compositors[i];
		}
	}

	//------------------------------------------------------------------

	void CCompositorManager::setEnabled(std::string compositorName, bool value)
	{
		for (unsigned int i = 0; i < _compositors.size(); ++i)
		{
			if (_compositors[i]->getName() == compositorName)
			{
				_compositors[i]->setEnabled(value);
				break;
			}
		}
	}

	//------------------------------------------------------------------

	bool CCompositorManager::isEnabled(std::string compositorName)
	{
		for (unsigned int i = 0; i < _compositors.size(); ++i)
		{
			if (_compositors[i]->getName() == compositorName)
				return _compositors[i]->isEnabled();
		}
		return false;
	}

	//------------------------------------------------------------------

	void CCompositorManager::activate(CScene *scene)
	{
		_scene = scene;

		for (unsigned int i = 0; i < _compositors.size(); ++i)
		{
			bool enabled = _compositors[i]->isEnabled();
			_compositors[i]->attachToScene(_scene->getViewport());
			if (enabled)
			{
				_compositors[i]->setEnabled(true);
			}
		}
	}

}