/**
@file CompositorManager.h

Declaraci�n del manager de compositors. Controla los efectos de post-proceso.

@author Alejandro P�rez Alonso
@date Junio, 2014
*/

#ifndef __Graphics_Compositor_Manager_H
#define __Graphics_Compositor_Manager_H

#include <string>
#include <vector>

namespace Graphics
{
	class CScene;
	class CCompositor;


	class CCompositorManager
	{
	public:
		static CCompositorManager* getSingletonPtr();
		static bool Init(CScene *scene);
		static bool Release();



		void setEnabled(std::string compositorName, bool value);
		bool isEnabled(std::string compositorName);

	private:
		CCompositorManager(CScene *scene);
		~CCompositorManager();

		bool open();
		void close();
		void activate(CScene *scene);

		static CCompositorManager* _instance;

	protected:
		std::vector<CCompositor *> _compositors;
		CScene *_scene;
	};

}

#endif