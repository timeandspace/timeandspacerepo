/**
@file ParticleSystem.cpp



@author Alejandro P�rez Alonso
@date Mayo, 2014
*/

#include "ParticleSystem.h"
#include "ParticleListener.h"
#include "Scene.h"

#include <OgreEntity.h>
#include <OgreSceneNode.h>
#include <OgreSceneManager.h>

#include <ParticleUniverseSystemManager.h>
#include <ParticleUniverseParticle.h>


namespace Graphics 
{

	CParticleSystem::CParticleSystem(const std::string &name, const std::string &templateName) :
		_name(name), _templateName(templateName)
	{

	}

	//--------------------------------------------------------

	CParticleSystem::~CParticleSystem()
	{
		//delete _particleSystem;
		_listeners.clear();
		_particleSystem->removeParticleSystemListener(this);
		ParticleUniverse::ParticleSystemManager::getSingletonPtr()->
			destroyParticleSystem(_name, _scene->getSceneMgr());
	}

	//--------------------------------------------------------

	bool CParticleSystem::attachToScene(CScene *scene)
	{
		_scene = scene;
		ParticleUniverse::ParticleSystemManager* pManager =
			ParticleUniverse::ParticleSystemManager::getSingletonPtr();

		if (pManager->getParticleSystem(_name))
		{
			_particleSystem = pManager->getParticleSystem(_name);
			_sceneNode = _particleSystem->getParentSceneNode();
			if (_sceneNode == NULL)
			{
				_particleSystem->detachFromParent();
				_sceneNode = _scene->getSceneMgr()->getRootSceneNode()->createChildSceneNode();
				_sceneNode->attachObject(_particleSystem); 
			}
		}
		else
		{
			_particleSystem = pManager->createParticleSystem(_name,
				_templateName, _scene->getSceneMgr()); 


			_sceneNode = _scene->getSceneMgr()->getRootSceneNode()->createChildSceneNode();
			_sceneNode->attachObject(_particleSystem); 

			_particleSystem->prepare();
		}

		// Nos registramos como listener
		_particleSystem->addParticleSystemListener(this);

		return true;
	}
	//--------------------------------------------------------

	void CParticleSystem::start()
	{
		_particleSystem->prepare();
		_particleSystem->start();
	}

	//--------------------------------------------------------

	void CParticleSystem::start(const float &stopTime)
	{
		_particleSystem->prepare();
		_particleSystem->start(stopTime);
	}

	void CParticleSystem::startAndStopFade(const float &stopTime)
	{
		_particleSystem->prepare();
		_particleSystem->startAndStopFade(stopTime);
	}

	//--------------------------------------------------------

	void CParticleSystem::stop()
	{
		_particleSystem->stop();
	}

	//--------------------------------------------------------

	void CParticleSystem::stop(const float &stopTime)
	{
		_particleSystem->stop(stopTime);
	}

	//--------------------------------------------------------

	void CParticleSystem::stopFade()
	{
		_particleSystem->stopFade();
	}

	//--------------------------------------------------------

	void CParticleSystem::stopFade(const float &stopTime)
	{
		_particleSystem->stopFade(stopTime);
	}

	//--------------------------------------------------------

	void CParticleSystem::pause()
	{
		_particleSystem->pause();
	}

	//--------------------------------------------------------

	void CParticleSystem::pause(const float &pauseTime)
	{
		_particleSystem->pause(pauseTime);
	}

	//--------------------------------------------------------

	void CParticleSystem::resume()
	{
		_particleSystem->resume();
	}

	//--------------------------------------------------------

	void CParticleSystem::setScale(const Vector3 &scale)
	{
		_particleSystem->setScale(scale);
	}

	//--------------------------------------------------------

	void CParticleSystem::setPosition(const Vector3 &position)
	{
		_sceneNode->setPosition(position);
	}

	//--------------------------------------------------------

	const Vector3& CParticleSystem::getPosition()
	{
		return _sceneNode->getPosition();
	}

	//--------------------------------------------------------

	void CParticleSystem::yaw(const float &yaw)
	{
		_sceneNode->yaw(Ogre::Radian(Math::fromDegreesToRadians(yaw)));
	}

	//--------------------------------------------------------

	void CParticleSystem::pitch(const float &pitch)
	{
		_sceneNode->pitch(Ogre::Radian(Math::fromDegreesToRadians(pitch)));
	}

	//--------------------------------------------------------

	Ogre::SceneNode* CParticleSystem::getSceneNode()
	{
		return _sceneNode;
	}

	//--------------------------------------------------------

	void CParticleSystem::handleParticleSystemEvent(ParticleUniverse::ParticleSystem* particleSystem, 
		ParticleUniverse::ParticleUniverseEvent& particleUniverseEvent)
	{
		switch (particleUniverseEvent.eventType)
		{
		case ParticleUniverse::EventType::PU_EVT_SYSTEM_STARTING:
			for (unsigned int i = 0; i < _listeners.size(); ++i)
				_listeners[i]->onStart();
			break;
		case ParticleUniverse::EventType::PU_EVT_SYSTEM_STOPPING:
			for (unsigned int i = 0; i < _listeners.size(); ++i)
				_listeners[i]->onStop();
			break;
		}
	}

	//--------------------------------------------------------

	void CParticleSystem::attachToEntity(std::string parentName, std::string boneName)
	{
		if (_particleSystem->isAttached())
		{
			_particleSystem->detachFromParent();
		}
		_scene->getSceneMgr()->getEntity(parentName)->attachObjectToBone(boneName, _particleSystem);
	}

}