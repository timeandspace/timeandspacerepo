//---------------------------------------------------------------------------
// SceneCreator.cpp
//---------------------------------------------------------------------------

#include "stdafx.h"

#include "SceneCreator.h"
#include "Scene.h"
#include "Camera.h"
#include "Server.h"
#include "StaticEntity.h"
#include "AnimatedEntity.h"
#include "NodeAnimatedEntity.h"
#include "ParticleSystem.h"
#include "Compositor.h"

#include <assert.h>


#include <Ogre.h>

namespace Graphics 
{

	CScene* CSceneCreator::createSceneForMenu()
	{
		Graphics::CScene* scene =  Graphics::CServer::getSingletonPtr()->createScene("MenuScene");//

	/*	Graphics::CAnimatedEntity* entity = new Graphics::CAnimatedEntity("marine","marine.mesh");

		Graphics::CAnimatedEntity* entity2 = new Graphics::CAnimatedEntity("loco","space.mesh");*/

		/*scene->addEntity(entity);
		scene->addEntity(entity2);

		Matrix4 trans1 = entity->getTransform();
		Matrix4 trans2 = entity2->getTransform();

		Math::setYaw(Math::fromDegreesToRadians(200),trans1);
		Math::setYaw(Math::fromDegreesToRadians(-15),trans2);

		entity->setTransform(trans1);
		entity2->setTransform(trans2);

		entity->setPosition(Vector3(-1.8,-1.8,-5.5));
		entity2->setPosition(Vector3(2,-2.1,-5.5));

		entity->init("Idle");
		entity2->init("Idle");*/

		scene->setAmbientLight(1.0f,1.0f,1.0f);
		scene->createDirectionalLight(Vector3(1.0f,1.0f,1.0f),Vector3(1.0f,1.0f,1.0f),Vector3(0.0f,-1.0f,-1.0f));
		scene->setCameraClipDistance(1.0f,1000.0f);



		Graphics::CParticleSystem* particle =  scene->createParticleSystem("WormHole","WormHole");

		

		float particlePosition = -700.0f;

		float trackNodePosition = -50.0f;

		float particleSpeed = 700.0f;

		float animDuration = 22.0f;

		float trackNodeOffset = std::abs(particlePosition - trackNodePosition)/ particleSpeed;

		float cameraOffset = std::abs(particlePosition) / particleSpeed;

		
		particle->setPosition(Vector3(0,0,particlePosition));

		particle->pitch(90.0f);

		particle->start();


		Ogre::Animation* nodeAnimation = scene->getSceneMgr()->createAnimation("nodeAnimation",animDuration);

		Ogre::Animation* trackNodeAnimation = scene->getSceneMgr()->createAnimation("trackNodeAnimation",animDuration);

		Ogre::Animation* cameraAnimation = scene->getSceneMgr()->createAnimation("cameraAnimation",animDuration);

		/*Ogre::Animation* cameraAnimation1 = scene->getSceneMgr()->createAnimation("cameraAnimation1",animDuration);

		Ogre::Animation* cameraAnimation2 = scene->getSceneMgr()->createAnimation("cameraAnimation2",animDuration);*/


		nodeAnimation->setInterpolationMode(Ogre::Animation::IM_SPLINE);
		trackNodeAnimation->setInterpolationMode(Ogre::Animation::IM_SPLINE);
		cameraAnimation->setInterpolationMode(Ogre::Animation::IM_SPLINE);
		/*cameraAnimation1->setInterpolationMode(Ogre::Animation::IM_SPLINE);
		cameraAnimation2->setInterpolationMode(Ogre::Animation::IM_SPLINE);*/
		

		Ogre::SceneNode* node = particle->getSceneNode();

		Ogre::SceneNode* trackNode = scene->getSceneMgr()->getRootSceneNode()->createChildSceneNode("trackNodeAnimation",Vector3(0,0,trackNodePosition));

		Ogre::SceneNode* cameraNode = scene->getCamera()->getCamera()->getParentSceneNode();


		scene->getCamera()->setCameraPosition(Vector3(0,0,0));

		scene->getCamera()->getCamera()->setAutoTracking(true,trackNode);		


		node->setInitialState();

		trackNode->setInitialState();

		cameraNode->setInitialState();

		//entity->getSceneNode()->setInitialState();

		//entity2->getSceneNode()->setInitialState();

		
		Ogre::NodeAnimationTrack* track = nodeAnimation->createNodeTrack(0);

		Ogre::NodeAnimationTrack* track2 = trackNodeAnimation->createNodeTrack(0);

		Ogre::NodeAnimationTrack* track3 = cameraAnimation->createNodeTrack(0);

		/*Ogre::NodeAnimationTrack* track4 = cameraAnimation1->createNodeTrack(0);

		Ogre::NodeAnimationTrack* track5 = cameraAnimation2->createNodeTrack(0);
		*/
		
		Ogre::TransformKeyFrame * kf;

		Ogre::TransformKeyFrame * kf2; 

		Ogre::TransformKeyFrame * kf3;

		//Ogre::TransformKeyFrame * kf4;

		//Ogre::TransformKeyFrame * kf5;

		
		kf= track->createNodeKeyFrame(0.0f);
		kf->setTranslate(Vector3(0.0f,0,0));

		kf2= track2->createNodeKeyFrame(0.0f +trackNodeOffset);
		kf2->setTranslate(Vector3(0.0f,0,0));

		kf3= track3->createNodeKeyFrame(0.0f  + cameraOffset);
		kf3->setTranslate(Vector3(0.0f,0,0));

	/*	kf4= track4->createNodeKeyFrame(0.0f  + cameraOffset);
		kf4->setTranslate(Vector3(0.0f,0,0));

		kf5= track5->createNodeKeyFrame(0.0f  + cameraOffset);
		kf5->setTranslate(Vector3(0.0f,0,0));*/

		
		kf= track->createNodeKeyFrame(4.0f);
		kf->setTranslate(Vector3(110.0f,0,0));

		kf2= track2->createNodeKeyFrame(4.0f +trackNodeOffset);
		kf2->setTranslate(Vector3(110.0f,0,0));

		kf3= track3->createNodeKeyFrame(4.0f + cameraOffset);
		kf3->setTranslate(Vector3(110.0f,0,0));

		//kf4= track4->createNodeKeyFrame(4.0f + cameraOffset);
		//kf4->setTranslate(Vector3(110.0f,0,0));

		//kf5= track5->createNodeKeyFrame(4.0f + cameraOffset);
		//kf5->setTranslate(Vector3(110.0f,0,0));


		kf= track->createNodeKeyFrame(6.0f);
		kf->setTranslate(Vector3(0.0f,-400.0f,0.0f));

		kf2= track2->createNodeKeyFrame(6.0f +trackNodeOffset);
		kf2->setTranslate(Vector3(0.0f,-400.0f,0.0f));

		kf3= track3->createNodeKeyFrame(6.0f + cameraOffset);
		kf3->setTranslate(Vector3(0.0f,-400.0f,0.0f));

		/*kf4= track4->createNodeKeyFrame(6.0f + cameraOffset);
		kf4->setTranslate(Vector3(0.0f,-400.0f,0.0f));

		kf5= track5->createNodeKeyFrame(6.0f + cameraOffset);
		kf5->setTranslate(Vector3(0.0f,-400.0f,0.0f));*/


		kf= track->createNodeKeyFrame(9.0f);
		kf->setTranslate(Vector3(-200.0f,-400.0f,0.0f));

		kf2= track2->createNodeKeyFrame(9.0f +trackNodeOffset);
		kf2->setTranslate(Vector3(-200.0f,-400.0f,0.0f));

		kf3= track3->createNodeKeyFrame(9.0f + cameraOffset);
		kf3->setTranslate(Vector3(-200.0f,-400.0f,0.0f));

		/*kf4= track4->createNodeKeyFrame(9.0f + cameraOffset);
		kf4->setTranslate(Vector3(-200.0f,-400.0f,0.0f));

		kf5= track5->createNodeKeyFrame(9.0f + cameraOffset);
		kf5->setTranslate(Vector3(-200.0f,-400.0f,0.0f));*/


		kf= track->createNodeKeyFrame(11.0f);
		kf->setTranslate(Vector3(0.0f,1400.0f,0.0f));

		kf2= track2->createNodeKeyFrame(11.0f +trackNodeOffset);
		kf2->setTranslate(Vector3(0.0f,1400.0f,0.0f));

		kf3= track3->createNodeKeyFrame(11.0f + cameraOffset);
		kf3->setTranslate(Vector3(0.0f,1400.0f,0.0f));

		/*kf4= track4->createNodeKeyFrame(11.0f + cameraOffset);
		kf4->setTranslate(Vector3(0.0f,1400.0f,0.0f));

		kf5= track5->createNodeKeyFrame(11.0f + cameraOffset);
		kf5->setTranslate(Vector3(0.0f,1400.0f,0.0f));*/
		


		kf= track->createNodeKeyFrame(14.0f);
		kf->setTranslate(Vector3(360.0f,0,0));

		kf2= track2->createNodeKeyFrame(14.0f + trackNodeOffset);
		kf2->setTranslate(Vector3(360.0f,0,0));

		kf3= track3->createNodeKeyFrame(14.0f + cameraOffset);
		kf3->setTranslate(Vector3(360.0f,0,0));

		/*kf4= track4->createNodeKeyFrame(14.0f + cameraOffset);
		kf4->setTranslate(Vector3(360.0f,0,0));

		kf5= track5->createNodeKeyFrame(14.0f + cameraOffset);
		kf5->setTranslate(Vector3(360.0f,0,0));*/


		kf= track->createNodeKeyFrame(17.0f);
		kf->setTranslate(Vector3(360.0f,1300.0f,0));

		kf2= track2->createNodeKeyFrame(17.0f + trackNodeOffset);
		kf2->setTranslate(Vector3(360.0f,1300.0f,0));

		kf3= track3->createNodeKeyFrame(17.0f + cameraOffset);
		kf3->setTranslate(Vector3(360.0f,1300.0f,0));

		/*kf4= track4->createNodeKeyFrame(17.0f + cameraOffset);
		kf4->setTranslate(Vector3(360.0f,1300.0f,0));

		kf5= track5->createNodeKeyFrame(17.0f + cameraOffset);
		kf5->setTranslate(Vector3(360.0f,1300.0f,0));*/
		
		 
		kf= track->createNodeKeyFrame(19.0f);
		kf->setTranslate(Vector3(0.0f,0,0));

		kf2= track2->createNodeKeyFrame(19.0f + trackNodeOffset);
		kf2->setTranslate(Vector3(0.0f,0,0));
		
		kf3= track3->createNodeKeyFrame(19.0f + cameraOffset);
		kf3->setTranslate(Vector3(0.0f,0,0));

		/*kf4= track4->createNodeKeyFrame(19.0f + cameraOffset);
		kf4->setTranslate(Vector3(0.0f,0,0));

		kf5= track5->createNodeKeyFrame(19.0f + cameraOffset);
		kf5->setTranslate(Vector3(0.0f,0,0));*/



		track->setAssociatedNode(node);

		track2->setAssociatedNode(trackNode);

		track3->setAssociatedNode(cameraNode);

		/*track4->setAssociatedNode(entity->getSceneNode());

		track5->setAssociatedNode(entity2->getSceneNode());*/

		
		Ogre::AnimationState* animState =  scene->getSceneMgr()->createAnimationState("nodeAnimation");

		Ogre::AnimationState* animState2 =  scene->getSceneMgr()->createAnimationState("trackNodeAnimation");

		Ogre::AnimationState* animState3 =  scene->getSceneMgr()->createAnimationState("cameraAnimation");

	/*	Ogre::AnimationState* animState4 =  scene->getSceneMgr()->createAnimationState("cameraAnimation1");

		Ogre::AnimationState* animState5 =  scene->getSceneMgr()->createAnimationState("cameraAnimation2");*/

		
		animState->setLoop(true);

		animState2->setLoop(true);

		animState3->setLoop(true);

	/*	animState4->setLoop(true);

		animState5->setLoop(true);
*/

		animState->setTimePosition(0.0f);

		animState2->setTimePosition(0.0f);

		animState3->setTimePosition(0.0f);

		//animState4->setTimePosition(0.0f);

		//animState5->setTimePosition(0.0f);


		animState->setEnabled(true);

		animState2->setEnabled(true);

		animState3->setEnabled(true);

	/*	animState4->setEnabled(true);

		animState5->setEnabled(true);*/

		//MEMORY LEAKS
		Graphics::CNodeAnimatedEntity* nodeAnimEnt = new Graphics::CNodeAnimatedEntity(*animState);

		Graphics::CNodeAnimatedEntity* trackNodeAnimEnt = new Graphics::CNodeAnimatedEntity(*animState2);

		Graphics::CNodeAnimatedEntity* cameraAnimEnt = new Graphics::CNodeAnimatedEntity(*animState3);

		/*Graphics::CNodeAnimatedEntity* cameraAnimEnt1 = new Graphics::CNodeAnimatedEntity(*animState4);

		Graphics::CNodeAnimatedEntity* cameraAnimEnt2 = new Graphics::CNodeAnimatedEntity(*animState5);*/


		scene->addEntity(nodeAnimEnt);

		scene->addEntity(trackNodeAnimEnt);

		scene->addEntity(cameraAnimEnt);

		//scene->addEntity(cameraAnimEnt1);

		//scene->addEntity(cameraAnimEnt2);

		// Compositor radial blur
		//scene->createCompositor("motionBlur")->activate();

		
		return scene;
	}

} // namespace Graphics
