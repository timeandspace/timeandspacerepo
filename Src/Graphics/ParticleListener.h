/**
@file ParticleListener

Contiene la declaración de la clase que representa oyente del sistema de partículas.

@author Alvaro Blazquez Checa
@date Junio, 2014
*/
#ifndef __Graphics_Particle_Listener_H
#define __Graphics_Particle_Listener_H


namespace Graphics
{
	class IParticleSystemListener
	{
	public:
		IParticleSystemListener() {}
		virtual ~IParticleSystemListener() {}
		virtual void onStart() = 0;
		virtual void onStop() = 0;

	}; // class IParticleSystemListener
}

#endif //__Graphics_Particle_Listener_H