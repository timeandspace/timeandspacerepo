//---------------------------------------------------------------------------
// AnimatedEntity.cpp
//---------------------------------------------------------------------------

/**
@file AnimatedEntity.cpp

Contiene la implementaci�n de la clase que representa una entidad gr�fica 
con animaciones.

@see Graphics::CAnimatedEntity
@see Graphics::CEntity

@author David Llans�
@date Julio, 2010

@author �lvaro Bl�zquez Checa
@author Alejandro P�rez Alonso
@date Marzo, 2014
*/

#include "AnimatedEntity.h"
#include "Server.h"
#include "Scene.h"
#include <assert.h>

#include <OgreEntity.h>
#include <OgreSceneNode.h>
#include <OgreSceneManager.h>

#define BLEND_SPEED 1.0f//(secs);

namespace Graphics 
{

	CAnimatedEntity::CAnimatedEntity(const std::string &name, const std::string &mesh):
		CEntity(name,mesh), 
		_currentBaseAnimation(0), _currentTopAnimation(0),
		_nextBaseAnimation(0), _nextTopAnimation(0),
		_clockModifier(1),
		_blendTimeLeft(0), _topBlendTimeLeft(0), 
		_blendDuration(0), 
		_nextAnimation(0),
		_observer(0),
		_animationFinishedSend(false),
		_legMask(0), 
		_bodyMask(0),
		_currentAnimationTimePosition(0.0f),
		_checkTimeInfo(false),
		_complete(false),
		_loop(true)
	{

	}

	//---------------------------------------------------------

	CAnimatedEntity::~CAnimatedEntity() 
	{

	}

	//---------------------------------------------------------

	void CAnimatedEntity::init(const std::string &anim)
	{
		Ogre::AnimationStateIterator itr = _entity->getAllAnimationStates()->getAnimationStateIterator();

		int i=0;
		while(itr.hasMoreElements())
		{
			Ogre::AnimationState* anim = itr.getNext();

#ifdef _DEBUG
			std::cout<<"-----------------------------------"<<std::endl;
			std::cout<<"anim "<<i<< ":"<<anim->getAnimationName()<<std::endl;
			std::cout<<"lenght "<<i<< ":"<<anim->getLength()<<std::endl;
			std::cout<<"enabled "<<i<< ":"<<anim->getEnabled()<<std::endl;
			std::cout<<"weight "<<i<< ":"<<anim->getWeight()<<std::endl;
			std::cout<<"-----------------------------------"<<std::endl;
			std::cout<<""<<std::endl;
#endif
			anim->setEnabled(false);
			anim->setWeight(0);
			anim->setTimePosition(0);

			_animations.push_back(anim);
			++i;
		}

		_currentTopAnimation = _entity->getAnimationState(anim);

		_currentTopAnimation->setEnabled(true);
		_currentTopAnimation->setWeight(1);
		_topBlendTimeLeft = 0;
		_blendDuration = 1;

		_entity->getSkeleton()->setBlendMode(Ogre::ANIMBLEND_AVERAGE);

		assert(_currentTopAnimation && "Hay que inicializar con una animacion puesta");
	}

	//---------------------------------------------------------

	void CAnimatedEntity::init(const std::string &animBody, const std::string &animLegs)
	{
		Ogre::AnimationStateIterator itr = _entity->getAllAnimationStates()->getAnimationStateIterator();

		int i=0;
		while(itr.hasMoreElements())
		{
			Ogre::AnimationState* anim = itr.getNext();
			anim->setEnabled(false);
			anim->setWeight(0);
			anim->setTimePosition(0);
			_animations.push_back(anim);
			++i;
		}

		// parte de abajo
		_currentBaseAnimation = _entity->getAnimationState(animLegs);
		_currentBaseAnimation->setEnabled(true);
		_currentBaseAnimation->setWeight(1);

		// parte de arriba
		_currentTopAnimation = _entity->getAnimationState(animBody);
		_currentTopAnimation->setEnabled(true);
		_currentTopAnimation->setWeight(1);

		_entity->getSkeleton()->setBlendMode(Ogre::ANIMBLEND_CUMULATIVE);

		_blendTimeLeft = 0;
		_topBlendTimeLeft = 0;
		_blendDuration = 1;

		//assert(_currentBaseAnimation && "Hay que inicializar con una animacion puesta");
	}

	//---------------------------------------------------------

	bool CAnimatedEntity::setAnimation(const std::string &anim, bool loop, bool reset)
	{
		if(!_entity->getAllAnimationStates()->hasAnimationState(anim))
			return false;

		Ogre::AnimationState* newAnimation = _entity->getAnimationState(anim);

		_currentTopAnimation->setEnabled(false);

		_currentTopAnimation = newAnimation;
		_currentTopAnimation->setLoop(loop);
		_currentTopAnimation->setEnabled(true);
		_currentTopAnimation->setWeight(1.0f);
		_currentTopAnimation->setTimePosition(0.0f);

		return true;
	}

	//---------------------------------------------------------

	bool CAnimatedEntity::setBaseAnimation(const std::string &anim, float blendDuration, bool loop, bool reset, MASKTYPE tipoMascara)
	{
		if(!_entity->getAllAnimationStates()->hasAnimationState(anim))
			return false;

		Ogre::AnimationState* newAnimation = _entity->getAnimationState(anim);
		newAnimation->setLoop(loop);

		if(!loop)
			_animationFinishedSend = false;

		if (reset)
		{
			if (tipoMascara == MASKTYPE::LEGS)
			{
				_currentBaseAnimation->setEnabled(false);
				_currentBaseAnimation->setWeight(0);
				_currentBaseAnimation->setTimePosition(0.0f);

				for (unsigned int i = 0; i < _animations.size(); ++i)
				{
					if (_currentTopAnimation != _animations[i] && 
						_nextTopAnimation != _animations[i])
					{
						_animations[i]->setEnabled(false);
						_animations[i]->setWeight(0);
						_animations[i]->setTimePosition(0);
					}
				}
				_currentBaseAnimation = newAnimation;
				_currentBaseAnimation->setWeight(1);
				_currentBaseAnimation->setTimePosition(0.0f);
				_currentBaseAnimation->setEnabled(true);
				//_nextBaseAnimation = _currentBaseAnimation;
				_blendTimeLeft = 0.0f;
				_nextBaseAnimation = NULL;
			}
			else
			{
				_currentTopAnimation->setWeight(0);
				_currentTopAnimation->setTimePosition(0.0f);
				_currentTopAnimation->setEnabled(false);

				for (unsigned int i = 0; i < _animations.size(); ++i)
				{
					if (_currentBaseAnimation != _animations[i] &&
						_nextBaseAnimation != _animations[i])
					{
						_animations[i]->setEnabled(false);
						_animations[i]->setWeight(0);
						_animations[i]->setTimePosition(0);
					}
				}
				_currentTopAnimation = newAnimation;
				_currentTopAnimation->setWeight(1);
				_currentTopAnimation->setTimePosition(0.0f);
				_currentTopAnimation->setEnabled(true);
				//_nextTopAnimation = _currentTopAnimation;
				_topBlendTimeLeft = 0.0f;
				_nextTopAnimation = NULL;
			}
			return true;
		}

		if (tipoMascara == MASKTYPE::LEGS)
		{
			if(_blendTimeLeft > 0)
			{

				if(_nextBaseAnimation == newAnimation)
					return false;
				else if(newAnimation == _currentBaseAnimation)
				{
					_currentBaseAnimation = _nextBaseAnimation;
					_nextBaseAnimation = newAnimation;
					_blendTimeLeft = _blendDuration - _blendTimeLeft;
				}
				else
				{
					//Queda mas de la mitad
					if(_blendTimeLeft >= _blendDuration * 0.5f)
					{
						_nextBaseAnimation->setEnabled(false);
						_nextBaseAnimation->setWeight(0);
					}
					else
					{
						_currentBaseAnimation->setEnabled(false);
						_currentBaseAnimation->setWeight(0);
						_currentBaseAnimation = _nextBaseAnimation;
					}

					_nextBaseAnimation = newAnimation;
					_nextBaseAnimation->setEnabled(true);
					_nextBaseAnimation->setWeight(1.0f - _blendTimeLeft/_blendDuration);
					_nextBaseAnimation->setTimePosition(0.0f);
				}
			}
			else if (_currentBaseAnimation != newAnimation)
			{
				_blendTimeLeft = _blendDuration = blendDuration;
				_nextBaseAnimation = newAnimation;
				_nextBaseAnimation->setEnabled(true);
				_nextBaseAnimation->setWeight(0.0f);
				_nextBaseAnimation->setTimePosition(0.0f);
			}
		}
		else
		{
			if(_topBlendTimeLeft > 0)
			{
				if(_nextTopAnimation == newAnimation)
					return false;
				else if(newAnimation == _currentTopAnimation)
				{
					_currentTopAnimation = _nextTopAnimation;
					_nextTopAnimation = newAnimation;
					_topBlendTimeLeft = _blendDuration - _topBlendTimeLeft;
				}
				else
				{
					//Queda mas de la mitad
					if(_topBlendTimeLeft >= _blendDuration * 0.5f)
					{
						_nextTopAnimation->setEnabled(false);
						_nextTopAnimation->setWeight(0);
					}
					else
					{
						_currentTopAnimation->setEnabled(false);
						_currentTopAnimation->setWeight(0);
						_currentTopAnimation = _nextTopAnimation;
					}

					_nextTopAnimation = newAnimation;
					_nextTopAnimation->setEnabled(true);
					_nextTopAnimation->setWeight(1.0f - _topBlendTimeLeft/_blendDuration);
					_nextTopAnimation->setTimePosition(0.0f);
				}
			}
			else if (_currentTopAnimation != newAnimation)
			{
				_topBlendTimeLeft = _blendDuration = blendDuration;
				_nextTopAnimation = newAnimation;
				_nextTopAnimation->setEnabled(true);
				_nextTopAnimation->setWeight(0.0f);
				_nextTopAnimation->setTimePosition(0.0f);
			}
		}

		//if(tipoMascara != MASKTYPE::NONE)
		//	setAnimationMask(_currentBaseAnimation,tipoMascara);

		//if(reset) _currentBaseAnimation->setTimePosition(0);

		return true;

	} // setAnimation

	//-------------------------------------------------------

	void CAnimatedEntity::tick(float secs)
	{
		if(_blendTimeLeft > 0)
		{
			_blendTimeLeft -= secs;
			if(_blendTimeLeft <= 0)
			{
				_currentBaseAnimation->setEnabled(false);
				_currentBaseAnimation->setWeight(0.0f);
				_currentBaseAnimation = _nextBaseAnimation;
				_currentBaseAnimation->setEnabled(true);
				_currentBaseAnimation->setWeight(1.0f);
				_nextBaseAnimation = NULL;
			}
			else
			{
				_currentBaseAnimation->setWeight(_blendTimeLeft/_blendDuration);
				_nextBaseAnimation->setWeight(1.0f - (_blendTimeLeft/_blendDuration));

				//_nextBaseAnimation->addTime(secs); //MIRAR ESTO
			}
		}

		if (_topBlendTimeLeft > 0)
		{
			_topBlendTimeLeft -= secs;
			if(_topBlendTimeLeft <= 0)
			{
				_currentTopAnimation->setEnabled(false);
				_currentTopAnimation->setWeight(0.0f);
				_currentTopAnimation = _nextTopAnimation;
				_currentTopAnimation->setEnabled(true);
				_currentTopAnimation->setWeight(1.0f);
				_nextTopAnimation = NULL;
			}
			else
			{
				_currentTopAnimation->setWeight(_topBlendTimeLeft/_blendDuration);
				_nextTopAnimation->setWeight(1.0f - (_topBlendTimeLeft/_blendDuration));

				//_nextBaseAnimation->addTime(secs); //MIRAR ESTO
			}
		}

		if (_currentBaseAnimation)
		{
			// HACK::Ajuste de velocidades de las animaciones
			if (_currentBaseAnimation->getAnimationName() == "WalkDown" ||
				_currentBaseAnimation->getAnimationName() == "WalkBackDown")
			{
				_currentBaseAnimation->addTime(secs * 2.5f);
			}
			else if (_currentBaseAnimation->getAnimationName() == "RunDown")
				_currentBaseAnimation->addTime(secs * 2.5f);
			else if (_currentBaseAnimation->getAnimationName() == "StrafeRightDown" ||
				_currentBaseAnimation->getAnimationName() == "StrafeLeftDown")
			{
				_currentBaseAnimation->addTime(secs * 4.0f);
			}
			else if (_currentBaseAnimation->getAnimationName() == "JumpDown")
				_currentBaseAnimation->addTime(secs);
			else if (_currentBaseAnimation->getAnimationName() == "WalkCrouchDown" ||
				_currentBaseAnimation->getAnimationName() == "WalkBackCrouchDown" ||
				_currentBaseAnimation->getAnimationName() == "StrafeRightCrouchDown" ||
				_currentBaseAnimation->getAnimationName() == "StrafeLeftCrouchDown")
			{
				_currentBaseAnimation->addTime(secs * 2.5f);
			}
			else
				_currentBaseAnimation->addTime(secs);
		}
		if (_currentTopAnimation)
		{
			if (_currentTopAnimation->getAnimationName() == "WalkUp" || 
				_currentTopAnimation->getAnimationName() == "WalkBackUp")
			{
				_currentTopAnimation->addTime(secs * 2.5f);
			}
			else if (_currentTopAnimation->getAnimationName() == "RunUp")
				_currentTopAnimation->addTime(secs * 2.5f);
			else if (_currentTopAnimation->getAnimationName() == "StrafeRightUp" ||
				_currentTopAnimation->getAnimationName() == "StrafeLeftUp")
			{
				_currentTopAnimation->addTime(secs * 4.0f);
			}
			else if (_currentTopAnimation->getAnimationName() == "JumpUp")
				_currentTopAnimation->addTime(secs);
			else if (_currentTopAnimation->getAnimationName() == "WalkCrouchUp" ||
				_currentTopAnimation->getAnimationName() == "WalkCrouchDown")
			{
				_currentTopAnimation->addTime(secs * 2.5f);
			}
			else
				_currentTopAnimation->addTime(secs);
		}

		// Comprobamos si la animaci�n ha terminado para avisar y las notificaciones timeevent
		if(_observer)
		{
			if( !_animationFinishedSend && _currentTopAnimation->hasEnded())
			{
				_animationFinishedSend = true;
				_observer->animationFinished(_currentTopAnimation->getAnimationName());
			}
			if (_currentBaseAnimation)
			{
				if( !_animationFinishedSend && _currentBaseAnimation->hasEnded())
				{
					_animationFinishedSend = true;
					_observer->animationFinished(_currentBaseAnimation->getAnimationName());
				}
			}
			if(_checkTimeInfo)
			{
				_currentAnimationTimePosition = _currentTopAnimation->getTimePosition();
				checkTime();
			}
		}


		// HACK::
		if (_currentBaseAnimation && _currentBaseAnimation->getEnabled() && _currentBaseAnimation->getWeight() == 0 &&
			_nextBaseAnimation && _nextBaseAnimation->getEnabled() && _nextBaseAnimation->getWeight() == 0)
		{
			_currentBaseAnimation->setEnabled(false);
			_currentBaseAnimation->setWeight(0.0f);
			_currentBaseAnimation = _nextBaseAnimation;
			_currentBaseAnimation->setEnabled(true);
			_currentBaseAnimation->setWeight(1.0f);
			_nextBaseAnimation = NULL;
		}
		if (_currentTopAnimation && _currentTopAnimation->getEnabled() && _currentTopAnimation->getWeight() == 0 &&
			_nextTopAnimation && _nextTopAnimation->getEnabled() && _nextTopAnimation->getWeight() == 0)
		{
			_currentTopAnimation->setEnabled(false);
			_currentTopAnimation->setWeight(0.0f);
			_currentTopAnimation = _nextTopAnimation;
			_currentTopAnimation->setEnabled(true);
			_currentTopAnimation->setWeight(1.0f);
			_nextTopAnimation = NULL;
		}

	} // tick

	//--------------------------------------------------------

	//bool CAnimatedEntity::setTopAnimation(const std::string &anim, bool loop, bool reset, MASKTYPE tipoMascara)
	//{
	//	if(!_entity->getAllAnimationStates()->hasAnimationState(anim))
	//		return false;

	//	if(_currentTopAnimation != NULL)
	//	{
	//		int pos = (std::find(_animations.begin(),_animations.end(), _currentTopAnimation) - _animations.begin());
	//	}

	//	if(!anim.empty())
	//	{
	//		_currentTopAnimation = _entity->getAnimationState(anim);
	//		_currentTopAnimation->setEnabled(true);
	//		_currentTopAnimation->setWeight(0);
	//		_currentTopAnimation->setLoop(reset);

	//		if(tipoMascara != MASKTYPE::NONE)
	//			setAnimationMask(_currentTopAnimation,tipoMascara);

	//		if(reset) _currentTopAnimation->setTimePosition(0);

	//		int pos = (std::find(_animations.begin(),_animations.end(), _currentTopAnimation) - _animations.begin());
	//	}
	//	else
	//		_currentTopAnimation = NULL;

	//	//_entity->getSkeleton()->setBlendMode(Ogre::ANIMBLEND_AVERAGE);
	//	return true;

	//} // setAnimation

	////--------------------------------------------------------

	//bool CAnimatedEntity::setMaskedAnimations(const std::string &anim1, bool loop1, MASKTYPE tipoMascara1,const std::string &anim2, bool loop2, MASKTYPE tipoMascara2)
	//{
	//	if(!_entity->getAllAnimationStates()->hasAnimationState(anim1) || !_entity->getAllAnimationStates()->hasAnimationState(anim2))
	//		return false;

	//	Ogre::AnimationState * animation1;
	//	Ogre::AnimationState * animation2;

	//	animation1 = _entity->getAnimationState(anim1);
	//	animation2 = _entity->getAnimationState(anim2);

	//	setAnimationMask(animation1,tipoMascara1);
	//	setAnimationMask(animation2,tipoMascara2);

	//	animation1->setEnabled(true);
	//	animation2->setEnabled(true);

	//	animation1->setLoop(loop1);
	//	animation2->setLoop(loop2);

	//	animation1->setTimePosition(0);
	//	animation1->addTime(0);

	//	animation2->setTimePosition(0);
	//	animation2->addTime(0);

	//	/*	_currentAnimations.push_back(animation1);
	//	_currentAnimations.push_back(animation2);*/

	//	_entity->getSkeleton()->setBlendMode(Ogre::ANIMBLEND_CUMULATIVE);
	//	return true;
	//}


	//bool CAnimatedEntity::blendAnimation(const std::string &anim, bool loop, float blendDuration)
	//{
	//	if(!_entity->getAllAnimationStates()->hasAnimationState(anim))
	//		return false;

	//	Ogre::AnimationState * newAnimation = _entity->getAnimationState(anim);

	//	if(_blendTimeLeft > 0) // aun estamos mezclando animaciones
	//	{
	//		if(newAnimation == _nextAnimation)
	//		{
	//			//nada que hacer, la nueva animacion es la misma que _nextAnimation
	//		}
	//		else if(newAnimation == _currentAnimation)
	//		{
	//			//estamos volviendo a la animacion original otra vez

	//			_currentAnimation = _nextAnimation;
	//			_nextAnimation = newAnimation;
	//			_blendTimeLeft = _blendDuration - _blendTimeLeft;
	//		}
	//		else
	//		{
	//			// la nueva animacion es realmente nueva

	//			if(_blendTimeLeft < _blendDuration *  0.5f)
	//			{
	//				//queda menos de la mitad de tiempo de mezcla apagamos _nextAnimation
	//				_nextAnimation->setEnabled(false);
	//				_nextAnimation->setWeight(0);
	//			}
	//			else
	//			{
	//				//queda menos de la mitad de tiempo de mezcla _nextAnimation se convierte en _currntAnimation
	//				//y newAnimation se convierte en nextAnimation
	//				_currentAnimation->setEnabled(false);
	//				_currentAnimation->setWeight(0);
	//				_currentAnimation = _nextAnimation;
	//			}

	//			_nextAnimation = newAnimation;
	//			_nextAnimation->setEnabled(true);
	//			_nextAnimation->setWeight(1.0f - _blendTimeLeft / _blendDuration);
	//			_nextAnimation->setTimePosition(0);
	//			_nextAnimation->setLoop(loop);
	//		}
	//	}
	//	else
	//	{
	//		if(_currentAnimation != newAnimation)
	//		{
	//			_nextAnimation = newAnimation;
	//			_nextAnimation->setEnabled(true);
	//			_nextAnimation->setWeight(0);
	//			_nextAnimation->setTimePosition(0);
	//			_nextAnimation->setLoop(loop);
	//			_blendTimeLeft = _blendDuration = blendDuration;
	//		}
	//	}
	//	return true;

	//} // setAnimation

	//--------------------------------------------------------

	bool CAnimatedEntity::stopAnimation(const std::string &anim)
	{
		if(!_entity->getAllAnimationStates()->hasAnimationState(anim))
			return false;
		Ogre::AnimationState *animation = _entity->getAnimationState(anim);
		animation->setEnabled(false);
		// Si la animaci�n a parar es la animaci�n activa ya no lo estar�.
		if(animation == _currentBaseAnimation)
			_currentBaseAnimation = 0;
		return true;

	} // stopAnimation

	//--------------------------------------------------------

	void CAnimatedEntity::stopAllAnimations()
	{
		if(_entity->getAllAnimationStates()->hasEnabledAnimationState())
		{
			Ogre::ConstEnabledAnimationStateIterator it = 
				_entity->getAllAnimationStates()->getEnabledAnimationStateIterator();
			Ogre::AnimationState *animation;


			//hay que recorrer con el iterador las animaciones una a una y pausarlas
			while(it.hasMoreElements())
			{
				animation = it.getNext();
				animation->setEnabled(false);
			}

			// Si hab�a animaci�n activa ya no lo est�.
			_currentBaseAnimation = 0;
		}

	} // stopAllAnimations



	void CAnimatedEntity::checkTime()
	{
		Ogre::AnimationState* animation = NULL;

		//Recorremos las animaciones
		for(unsigned int i = 0; i < _animations.size(); ++i)
		{
			//Miramos cuales estan activas
			if(_animations[i]->getEnabled())
			{
				//De las activas miramos si de alguna tenemos que avisar de un time event
				if(_animTimeInfo.find(_animations[i]->getAnimationName()) != _animTimeInfo.end())
				{
					animation = _animations[i];
					std::vector<float> timePositions =_animTimeInfo[animation->getAnimationName()];

					for(unsigned int i = 0; i < timePositions.size(); ++i)
					{
						if(Ogre::Math::RealEqual(animation->getTimePosition(),timePositions[i],0.012f) && animation->getWeight() > 0.5f)
						{
							_observer->animationTimeEvent(animation->getAnimationName());
							break;
						}
					}
					break;
				}
			}
		}

		/*if(!animation) return;

		std::vector<float> timePositions =_animTimeInfo[animation->getAnimationName()];

		for(int i =0;i<timePositions.size();++i)
		{
		if(Ogre::Math::RealEqual(animation->getTimePosition(),timePositions[i],0.012f) && animation->getWeight() > 0.5f)
		{
		_observer->animationTimeEvent(animation->getAnimationName());
		break;
		}
		}*/

	}


	//WARRERIAS CON LOS HUESOS
	void CAnimatedEntity::createBoneBlendMaskInfo()
	{
		getMask("Thigh");
		//getMask("Spine");
		for(unsigned int i = 0; i<_legMask.size(); ++i)
		{
			if(_legMask[i] == 1.0f)
				_bodyMask.push_back(0.0f);
			else
				_bodyMask.push_back(1.0f);
		}

		// 2 y 57 pelvis
		/*	_mascaraPiernas[2] = 0.5f;
		_mascaraPiernas[57] = 0.5f;*/

		_bodyMask[2] = 1.0f;
		_bodyMask[57] = 1.0f;

	}

	void CAnimatedEntity::setAnimationMask(Ogre::AnimationState* animationState,MASKTYPE maskType)
	{
		assert(animationState);

		if(animationState->hasBlendMask())
			animationState->destroyBlendMask();

		animationState->createBlendMask(_legMask.size(), -1);

		std::vector<float>* mask;


		if(maskType == MASKTYPE::LEGS)
		{
			mask = &_legMask;
		}
		else if(maskType == MASKTYPE::BODY)
		{
			mask = &_bodyMask;
		}

		for(unsigned int i = 0; i < mask->size(); ++i)
		{
			animationState->setBlendMaskEntry(i,(*mask)[i]);
		}

	}




	void CAnimatedEntity::dumpBones()
	{

		Ogre::Skeleton* skeleton = (Ogre::Skeleton*)_entity->getSkeleton();

		Ogre::Skeleton::BoneIterator boneIt = skeleton->getRootBoneIterator();

		while(boneIt.hasMoreElements())
		{
			Ogre::Bone* bone = boneIt.getNext();
			dumpBoneTree(bone,0);
		}
	}

	void CAnimatedEntity::dumpBoneTree(const Ogre::Bone* bone, unsigned int depth)
	{
		for(unsigned int i =0; i<depth; ++i)
		{
			printf("  ");
		}
		printf("%d:'%s'\n", bone->getHandle(), bone->getName().c_str());

		Ogre::Node::ConstChildNodeIterator it = bone->getChildIterator();
		while(it.hasMoreElements())
		{
			dumpBoneTree((const Ogre::Bone*)it.getNext(), depth +1);
		}
	}

	void CAnimatedEntity::getMask(const std::string& boneName)
	{
		Ogre::Skeleton* skeleton = _entity->getSkeleton();

		float *boneArray = new float [skeleton->getNumBones()];

		Ogre::Skeleton::BoneIterator boneIt = skeleton->getRootBoneIterator();

		while(boneIt.hasMoreElements())
		{
			Ogre::Bone* bone = boneIt.getNext();
			setBoneValue(boneName,bone,boneArray);
		}

		if(boneName == "Thigh")
		{
			_legMask.clear();
			_legMask.assign(boneArray,boneArray + skeleton->getNumBones());
		}
		else if(boneName == "Spine")
		{
			_bodyMask.clear();
			_bodyMask.assign(boneArray,boneArray + skeleton->getNumBones());
		}

		delete [] boneArray;
	}


	void setBoneValue2(const Ogre::Bone* bone,float mask[],float value)
	{
		Ogre::Node::ConstChildNodeIterator it = bone->getChildIterator();
		mask[bone->getHandle()] = value;
		while(it.hasMoreElements())
		{
			const Ogre::Bone* childBone = (const Ogre::Bone*)it.getNext();
			setBoneValue2(childBone,mask,value);
		}
	}

	void CAnimatedEntity::setBoneValue(const std::string& boneName,const Ogre::Bone* bone,float mask[])
	{
		Ogre::Node::ConstChildNodeIterator it = bone->getChildIterator();
		mask[bone->getHandle()] = 1.0f;
		while(it.hasMoreElements())
		{
			const Ogre::Bone* childBone = (const Ogre::Bone*)it.getNext();

			if (bone->getName().find(boneName) != std::string::npos) 
			{
				mask[bone->getHandle()] = 0.0f;
				setBoneValue2(childBone,mask,0.0f);
			}
			else
			{
				setBoneValue(boneName,childBone,mask);
			}
		}
	}


} // namespace Graphics
