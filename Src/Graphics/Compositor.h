/**
@file Compositor.h

Contiene la declaraci�n de la clase que maneja los compositiors (efectos de post-proceso).

@author Alejandro P�rez Alonso

@date Junio, 2014
*/

#ifndef __Graphics_Compositor_H
#define __Graphics_Compositor_H

#include "BaseSubsystems/Math.h"

namespace Ogre
{
	class Viewport;
	class CompositorInstance;
}

namespace Graphics
{
	class CCompositor 
	{
	public:
		CCompositor(const std::string& name);
		~CCompositor();

		bool attachToScene(Ogre::Viewport* viewport);

		void setEnabled(bool value);
		bool isEnabled();
		const std::string& getName() { return _name; }

	protected:
		std::string _name;
		Ogre::Viewport *_viewport;
		Ogre::CompositorInstance* _compositorInstance;
	};

}

#endif //__Graphics_Compositor_H