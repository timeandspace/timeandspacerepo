//---------------------------------------------------------------------------
// SceneCreator.h
//---------------------------------------------------------------------------

/**
@file SceneCreator.h
*/

#ifndef __Graphics_SceneCreator_H
#define __Graphics_SceneCreator_H

#include "stdafx.h"

#include <list>

// Predeclaración de clases para ahorrar tiempo de compilación
namespace Ogre 
{
	class Root;
	class Viewport;
	class SceneManager;
	class StaticGeometry;
	class Light;
}
namespace Graphics 
{
	class CServer;
	class CCamera;
	class CEntity;
	class CStaticEntity;
	class CScene;
}

namespace Graphics 
{
	class CSceneCreator 
	{
	public:

		static CScene* createSceneForMenu();

	}; // class CScene

} // namespace Graphics

#endif // __Graphics_Scene_H
