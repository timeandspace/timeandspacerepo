//---------------------------------------------------------------------------
// AnimatedEntity.h
//---------------------------------------------------------------------------

/**
@file AnimatedEntity.h

Contiene la declaraci�n de la clase que representa una entidad gr�fica 
con animaciones.

@see Graphics::CAnimatedEntity
@see Graphics::CEntity

@author �lvaro Bl�zquez Checa
@date Marzo, 2014
*/

#ifndef __Graphics_AnimatedEntity_H
#define __Graphics_AnimatedEntity_H

#include "Entity.h"


namespace Ogre 
{
	class AnimationState;
}

namespace Graphics 
{
	class CScene;
}

namespace Graphics 
{
	
	class CAnimatedEntityListener 
	{
	public:

		/**
		M�todo que ser� invocado siempre que se termine una animaci�n.
		Las animaciones en c�clicas no invocar�n nunca este m�todo.

		@param animation Nombre de la animaci�n terminada.
		*/
		virtual void animationFinished(const std::string &animation) {}

		virtual void animationTimeEvent(const std::string &animation) {}


	}; // CAnimatedEntityListener

	
	class CAnimatedEntity : public CEntity
	{
	public:

		enum MASKTYPE
		{
			NONE,
			LEGS,
			BODY
		};

		typedef std::map<std::string,std::vector<float>> AnimTimeInfo;

		/**
		Constructor de la clase.

		@param name Nombre de la entidad.
		@param mesh Nombre del modelo que debe cargarse.
		*/
		CAnimatedEntity(const std::string &name, const std::string &mesh);


		/**
		Destructor de la aplicaci�n.
		*/
		virtual ~CAnimatedEntity();

		/**
		Inicializa la entidad gr�fica y establece la animaci�n por defecto
		*/
		void init(const std::string &anim);
		
		/**
		Inicializa la entidad grafica y establece las animaciones por defecto tanto del cuerpo
		como de la piernas
		*/
		void init(const std::string &animBody, const std::string &animLegs);

		/**
		Activa una animaci�n a partir de su nombre.

		@param anim Nombre de la animaci�n a activar.
		@param loop true si la animaci�n debe reproducirse c�clicamente.
		@return true si la animaci�n solicitada fue correctamente activada.
		*/
		bool setAnimation(const std::string &anim, bool loop = true, bool reset = false);

		/**
		Desactiva una animaci�n a partir de su nombre.

		@param anim Nombre de la animaci�n a activar.
		@return true si la animaci�n solicitada fue correctamente desactivada.
		*/
		bool stopAnimation(const std::string &anim);

		/**
		Desactiva todas las animaciones de una entidad.
		*/
		void stopAllAnimations();


		bool setBaseAnimation(const std::string &anim, float blendDuration = 0.5f, bool loop = true, bool reset = false, MASKTYPE tipoMascara = MASKTYPE::NONE);


		//bool setTopAnimation(const std::string &anim, bool loop = true, bool reset = true, MASKTYPE tipoMascara = MASKTYPE::NONE);


		//bool setMaskedAnimations(const std::string &anim1, bool loop1, MASKTYPE tipoMascara1,const std::string &anim2, bool loop2, MASKTYPE tipoMascara2);


		/**
		Funci�n que registra al oyente de la entidad gr�fica. Por 
		simplicidad solo habr� un oyente por entidad.
		*/
		void setObserver(CAnimatedEntityListener *observer, AnimTimeInfo* animTimeInfo = NULL)
		{
			_observer = observer;
			if(animTimeInfo)
			{
				_animTimeInfo = *animTimeInfo;
				_checkTimeInfo = true;
			}
		}

		/**
		Funci�n que quita al oyente de la entidad gr�fica. Por 
		simplicidad solo habr� un oyente por entidad.
		*/
		void removeObserver(CAnimatedEntityListener *observer)
		{if(_observer = observer) _observer = 0;}


		/**
		Afecta a la velocidad de la animacion
		*/
		void setClockModifier(float modifier)
		{
			_clockModifier = modifier;
		}

		float getClockModifier()
		{
			return _clockModifier;
		}


		


		void createBoneBlendMaskInfo();

		/**
		info sobre los huesos
		*/
		void dumpBones();


		void getMask(const std::string& boneName);


	protected:

		void dumpBoneTree(const Ogre::Bone* bone, unsigned int depth);

		void setBoneValue(const std::string& boneName, const Ogre::Bone* bone,float mask[]);

		void setAnimationMask(Ogre::AnimationState* anim,MASKTYPE maskType);

		void checkTime();

		std::vector<float> _legMask;

		std::vector<float> _bodyMask;

		/**
		Objeto oyente que es informado de cambios en la entidad como 
		la terminaci�n de las animaciones. Por simplicidad solo habr�
		un oyente por entidad.
		*/
		CAnimatedEntityListener *_observer;

		// Cada entidad debe pertenecer a una escena. Solo permitimos
		// a la escena actualizar el estado.
		friend class CScene;

		/**
		Actualiza el estado de la entidad cada ciclo.

		@param secs N�mero de milisegundos transcurridos desde la �ltima 
		llamada.
		*/
		virtual void tick(float secs);

		/**
		Animaci�n que tiene la entidad activada.
		*/
		Ogre::AnimationState *_currentTopAnimation;

		Ogre::AnimationState *_currentBaseAnimation;

		Ogre::AnimationState *_nextBaseAnimation;

		Ogre::AnimationState *_nextTopAnimation;
		
		/**
		Siguiente animacion
		*/
		Ogre::AnimationState *_nextAnimation;

		//int _numAnims;

		std::vector<Ogre::AnimationState *> _animations;


		/**
		tiempo que queda de mezcla de animacion
		*/
		float _blendTimeLeft;
		float _topBlendTimeLeft;

		/**
		tiempo de mezcla de animacion
		*/
		float _blendDuration;

		bool _loop;

		bool _complete;

		/**
		modifica el reloj de la entidad grafica, afecta a la velocidad de la animacion
		*/
		float _clockModifier;

		bool _animationFinishedSend;


		float _currentAnimationTimePosition;

		AnimTimeInfo _animTimeInfo;

		bool _checkTimeInfo;


	}; // class CAnimatedEntity

} // namespace Graphics

#endif // __Graphics_AnimatedEntity_H
