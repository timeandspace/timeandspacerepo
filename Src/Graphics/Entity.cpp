//---------------------------------------------------------------------------
// Entity.cpp
//---------------------------------------------------------------------------

/**
@file Entity.cpp

Contiene la implementaci�n de la clase que representa una entidad gr�fica.

@see Graphics::CEntity

@author David Llans�
@date Julio, 2010
*/

#include "Entity.h"
#include "Scene.h"
#include "Server.h"

#include "BaseSubsystems/Server.h"
#include "BaseSubsystems/Math.h"

#include <assert.h>

#include <OgreEntity.h>
#include <OgreSceneNode.h>
#include <OgreSceneManager.h>
#include "AnimatedEntity.h"

namespace Graphics 
{
	CEntity::CEntity(const std::string &name, const std::string &mesh)
		: _entity(0), _entityNode(0), _scene(0), _loaded(false), _dummyEntity(false)
	{
		_name = name;
		_mesh = mesh;
	} // CEntity

	//-------------------------------------------------------

	CEntity::CEntity()
		: _entity(0), _entityNode(0), _scene(0), _loaded(false), _dummyEntity(true)
	{
	}

	//-------------------------------------------------------

	CEntity::~CEntity() 
	{
		assert(!_scene && "��Para destruir una entidad esta no puede pertenecer a una escena!!");

	} // ~CEntity

	//--------------------------------------------------------

	bool CEntity::attachToScene(CScene *scene)
	{
		assert(scene && "��La entidad debe asociarse a una escena!!");
		// Si la entidad est� cargada por otro gestor de escena.
		if(_loaded && (_scene != scene))
			return false;

		// Si no est� cargada forzamos su carga.
		if (!_loaded)
		{
			_scene = scene;
			return load();
		}

		// Si ya estaba cargada en la escena se devuelve cierto.
		return true;

	} // attachToScene

	//--------------------------------------------------------

	bool CEntity::deattachFromScene()
	{
		// Si la entidad no est� cargada no se puede quitar de
		// una escena. Ya que no pertenecer� a ninguna.
		if(!_loaded)
			return false;
		// Si la entidad est� cargada forzamos su descarga.
		else
		{
			assert(_scene && "��La entidad debe estar asociada a una escena!!");
			unload();
			_scene = 0;
		}

		return true;

	} // deattachFromScene

	//--------------------------------------------------------

	bool CEntity::load()
	{
		if(_dummyEntity)
			return true;

		try
		{
			_entity = _scene->getSceneMgr()->createEntity(_name, _mesh);
		}
		catch(std::exception e)
		{
			return false;
		}
		_entityNode = _scene->getSceneMgr()->getRootSceneNode()->
			createChildSceneNode(_name + "_node");
		_entityNode->attachObject(_entity);
		_loaded = true;

		return true;

	} // load

	//--------------------------------------------------------

	void CEntity::unload()
	{
		if(_entityNode)
		{
			// desacoplamos la entidad de su nodo
			_entityNode->detachAllObjects();
			_scene->getSceneMgr()->destroySceneNode(_entityNode);
			_entityNode = 0;
		}
		if(_entity)
		{
			_scene->getSceneMgr()->destroyEntity(_entity);
			_entity = 0;
		}

	} // load

	//--------------------------------------------------------

	void CEntity::attachObjectToBone(std::string bone, std::string entityName, std::string meshName)
	{
		Ogre::Entity *ent = NULL;
		if (! _scene->getSceneMgr()->hasEntity(entityName))
			ent = _scene->getSceneMgr()->createEntity(entityName, meshName);

		if (ent != NULL)
		{
			if (ent->isAttached())
				ent->detachFromParent();
			_entity->attachObjectToBone(bone, ent);
		}

	} // attachObjectToBone

	//--------------------------------------------------------

	void CEntity::attachObjectToBone(std::string bone, std::string entityName, const Ogre::Quaternion &offsetOrientation,
		const Vector3 &offsetPosition)
	{
		Ogre::Entity *ent = NULL;
		if (_scene->getSceneMgr()->hasEntity(entityName))
			ent = _scene->getSceneMgr()->getEntity(entityName);
		if (ent != NULL)
		{
			if (ent->isAttached())
				ent->detachFromParent();
			_entity->attachObjectToBone(bone, ent, offsetOrientation, offsetPosition);
			ent->setVisible(true);
		}

	} // attachObjectToBone

	//--------------------------------------------------------

	void CEntity::detachObjectFromBone(std::string entityName)
	{
		_entity->detachObjectFromBone(entityName);

	} // detachObjectFromBone

	//--------------------------------------------------------

	void CEntity::attachObject(std::string entityName)
	{
		CEntity *ent = CServer::getSingletonPtr()->getEntity(entityName);
		assert(ent);
		if (ent->getEntity()->isAttached())
			ent->getEntity()->detachFromParent();
		_entityNode->attachObject(ent->getEntity());

	} // attachObject

	//--------------------------------------------------------

	void CEntity::detachObject(std::string entityName)
	{
		CEntity *ent = CServer::getSingletonPtr()->getEntity(entityName);
		assert(ent);

		Vector3 pos = ent->getPosition();

		ent->getEntity()->detachFromParent();

		Ogre::SceneNode *sceneNode = NULL;
		if (_scene->getSceneMgr()->hasSceneNode(entityName + "_node"))
		{
			sceneNode = _scene->getSceneMgr()->getSceneNode(entityName + "_node");
		}
		else
			sceneNode = _scene->getSceneMgr()->getRootSceneNode()->createChildSceneNode(entityName + "_node");
			
		sceneNode->attachObject(ent->getEntity());
		sceneNode->setPosition(pos);

		ent->setVisible(false);

	} // attachObject

	//--------------------------------------------------------

	Vector3 CEntity::getAttachedEntityPosition(std::string entityName)
	{
		Ogre::Entity::ChildObjectListIterator::const_iterator it = _entity->getAttachedObjectIterator().begin();
		Ogre::Entity::ChildObjectListIterator::const_iterator end = _entity->getAttachedObjectIterator().end();

		for( ;it != end; ++it)
		{
			if ((*it).first == entityName)
				(*it).second->getParentSceneNode()->getPosition();
		}

		return Vector3::ZERO;

	} //getAttachedEntityPosition

	//--------------------------------------------------------

	void CEntity::tick(float secs)
	{
	} // tick

	//--------------------------------------------------------

	void CEntity::setTransform(const Matrix4 &transform)
	{
		assert(_entityNode && "La entidad no ha sido cargada");
		if(_entityNode)
		{
			_entityNode->setPosition(transform.getTrans());
			_entityNode->setOrientation(transform.extractQuaternion());
		}

	} // setTransform


	Matrix4 CEntity::getTransform()
	{
		return _entityNode->_getFullTransform();
	}

	//--------------------------------------------------------

	void CEntity::setOrientation(const Matrix3 &orientation)
	{
		assert(_entityNode && "La entidad no ha sido cargada");
		if(_entityNode)
			_entityNode->setOrientation(orientation);

	} // setOrientation

	//--------------------------------------------------------

	void CEntity::setVisible(bool visible)
	{
		assert(_entityNode && "La entidad no ha sido cargada");
		if(_entityNode)
			_entityNode->setVisible(visible);

	} // setVisible

	//--------------------------------------------------------

	const bool CEntity::getVisible()
	{
		if(_entityNode)
			return _entity->isVisible();

		throw new std::exception("La entidad no ha sido cargada");

	} // getPosition

	//--------------------------------------------------------

	void CEntity::setPosition(const Vector3 &position)
	{
		assert(_entityNode && "La entidad no ha sido cargada");
		_entityNode->setPosition(position);

	} // setPosition

	//--------------------------------------------------------

	const Vector3& CEntity::getPosition()
	{
		return _entityNode->getPosition();
	}

	//--------------------------------------------------------

	void CEntity::setScale(const Vector3 &scale)
	{
		assert(_entityNode && "La entidad no ha sido cargada");
		_entityNode->setScale(scale);

	} // setScale

	//--------------------------------------------------------

	void CEntity::setScale(const float scale)
	{
		assert(_entityNode && "La entidad no ha sido cargada");
		Vector3 scaleVector(scale,scale,scale);
		_entityNode->setScale(scaleVector);

	} // setScale

	//--------------------------------------------------------

	void CEntity::setCastShadows(bool value)
	{
		assert(_entity && "La entidad no ha sido cargada");
		_entity->setCastShadows(value);

	}// setCastShadows

	//--------------------------------------------------------

	bool CEntity::getCastShadows() 
	{
		return _entity->getCastShadows();
	}

	//--------------------------------------------------------

	void CEntity::setMaterial(std::string materialName)
	{
		assert(_entity);
		_entity->setMaterialName(materialName);

	} // setMaterial

	//--------------------------------------------------------

	void CEntity::changeParameter(std::string materialName, std::string paramName, float value)
	{
		Ogre::MaterialPtr mat = Ogre::MaterialManager::getSingleton().getByName(materialName);

		// Asumimos que siempre va a haber una sola tecnica (technique)
		Ogre::Technique *tech = mat->getTechnique(0);
		for (unsigned int i = 0; i < tech->getNumPasses(); ++i)
		{
			tech->getPass(i)->getVertexProgramParameters()->setNamedConstant(paramName, value);
		}

	} // changeParameter

} // namespace Graphics
