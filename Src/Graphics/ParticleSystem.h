/**
@file ParticleSystem

Contiene la declaraci�n de la clase que representa un sistema de part�culas.

@author Alejandro P�rez Alonso
@date Mayo, 2014
*/
#ifndef __Graphics_Particle_System_H
#define __Graphics_Particle_System_H

#include "BaseSubsystems/Math.h"

#include <ParticleUniverseCommon.h>
#include <ParticleUniverseSystemListener.h>



namespace ParticleUniverse
{
	class ParticleSystem;
}

namespace Ogre
{
	class SceneNode;
}

namespace Graphics
{
	class CScene;
	class IParticleSystemListener;
}

namespace Graphics
{
	class CParticleSystem : public ParticleUniverse::ParticleSystemListener
	{
	public:

		CParticleSystem(const std::string &name, const std::string &templateName);

		virtual ~CParticleSystem();

		void prepare (void);
		void start();
		void start(const float &stopTime);
		void startAndStopFade(const float &stopTime);
		void stop(void);
		void stop(const float &stopTime);
		void stopFade(void);
		void stopFade(const float &stopTime);
		void pause();
		void pause(const float &pauseTime);
		void resume();
		void setScale(const Vector3 &scale);
		void setPosition(const Vector3 &position);
		const Vector3& getPosition();
		void yaw(const float &yaw);
		void pitch(const float &pitch);

		Ogre::SceneNode* getSceneNode();

		void addListener(IParticleSystemListener *listener)
		{
			_listeners.push_back(listener);
		}

		void attachToEntity(std::string parentName, std::string boneName);
	protected:
		// ParticleUniverse::ParticleSystemListener
		virtual void handleParticleSystemEvent(ParticleUniverse::ParticleSystem* particleSystem, 
			ParticleUniverse::ParticleUniverseEvent& particleUniverseEvent);

		bool attachToScene(CScene *scene);

		friend class CScene;

		CScene *_scene;
		/**
		Nodo que contiene la entidad de Ogre.
		*/
		Ogre::SceneNode *_sceneNode;
		/**
		Sistema de part�culas
		*/
		ParticleUniverse::ParticleSystem* _particleSystem;
		/**
		Nombre del sistema de part�culas
		*/
		std::string _name;
		/**
		Nombre del archivo .pu que contiene el script de particulas
		*/
		std::string _templateName;

		/**
		Lista de listeners del sistema de part�culas
		*/
		std::vector<IParticleSystemListener *> _listeners;

	}; // Class CParticleSystem
}

#endif //__Graphics_Particle_System_H