//---------------------------------------------------------------------------
// SkeletonAnimatedEntity.cpp
//---------------------------------------------------------------------------

/**
@file NodeAnimatedEntity.cpp

Contiene la implementaci�n de la clase que representa una entidad gr�fica 
con animaciones.

@see Graphics::CEntity

@author �lvaro Bl�zquez Checa
@date Junio, 2014
*/

#include "NodeAnimatedEntity.h"

#include <OgreAnimationState.h>
#include <OgreAnimation.h>


namespace Graphics 
{
	//-------------------------------------------------------

	CNodeAnimatedEntity::CNodeAnimatedEntity(Ogre::AnimationState& animationState)
	{
		_userDefinedAnimation = &animationState;//new Ogre::AnimationState(animationState);
	}

	//-------------------------------------------------------

	CNodeAnimatedEntity::~CNodeAnimatedEntity() 
	{
		if(_userDefinedAnimation)
			delete _userDefinedAnimation;
	}

	//-------------------------------------------------------

	void CNodeAnimatedEntity::createAnimation(Ogre::SceneNode& node, float duration, const std::vector<NodeTrack>& traks)
	{

		//Create animation
		Ogre::Animation* animation = new Ogre::Animation("animation", duration);

		//Create Track
		Ogre::NodeAnimationTrack* track = animation->createNodeTrack(0);

		//Create Transform keyframe
		Ogre::TransformKeyFrame * kf;


		for(int i=0; i<traks.size(); ++i)
		{
			kf = track->createNodeKeyFrame(traks[i].time);

			if(traks[i].translate != Vector3::ZERO)
				kf->setTranslate(traks[i].translate);

			if(traks[i].rotate != Quaternion::IDENTITY)
				kf->setRotation(traks[i].rotate);

			if(traks[i].scale != Vector3::ZERO)
				kf->setScale(traks[i].scale);
		}

		//track->setAssociatedNode(&node);
	}

	//-------------------------------------------------------

	void CNodeAnimatedEntity::tick(float secs)
	{
		if(_userDefinedAnimation)
			_userDefinedAnimation->addTime(secs);
	} // tick

	//-------------------------------------------------------

} // namespace Graphics
