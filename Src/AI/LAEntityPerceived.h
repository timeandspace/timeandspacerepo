#pragma once

#ifndef __AI_LAEntityPerceived_H
#define __AI_LAEntityPerceived_H


#include "LatentAction.h"

#include "Logic/Entity/Entity.h"

namespace AI
{

	class CLAEntityPerceived : public CLatentAction
	{
	public:
		CLAEntityPerceived(const std::string& entityType, const std::string& varName);
		~CLAEntityPerceived(void);
		/**
		Devuelve true si a la acci�n le interesa el tipo de mensaje
		enviado como par�metro.
		<p>
		Esta acci�n acepta mensajes del tipo PERCEIVED

		@param msg Mensaje que ha recibido la entidad.
		@return true Si la acci�n est� en principio interesada
		por ese mensaje.
		*/
		virtual bool accept(Logic::CMessage *message);
		/**
		Procesa el mensaje recibido. El m�todo es invocado durante la
		ejecuci�n de la acci�n cuando se recibe el mensaje.
		<p>
		Si recibe PERCEIVED comprueba que la entidad percibida coincide con 
		el tipo que es capaz de percibir. Si es as� tiene �xito y si no, falla.

		@param msg Mensaje recibido.
		*/
		virtual void process(Logic::CMessage *message);

		virtual CLatentAction* clone()
		{
			return new CLAEntityPerceived(*this);
		}

	protected:
		std::string _entityType;
		std::string _entityTypeValue;
		CEntity* _entityPerceived;
		std::string _varName;
		bool _perceivedSight;
		bool _perceivedHear;

		/**
		M�todo invocado al principio de la ejecuci�n de la acci�n,
		para que se realicen las tareas que son �nicamente necesarias
		al principio (y no durante toda la vida de la acci�n).
		<p>

		@return Estado de la funci�n; si se indica que la
		acci�n a terminado (LatentAction::Completed), se invocar�
		al OnStop().
		*/
		virtual LAStatus OnStart();

		/**
		M�todo invocado al final de la ejecuci�n de la acci�n,
		para que se realicen las tareas que son �nicamente necesarias
		al final (y no durante toda la vida de la acci�n).

		En la mayor�a de los casos este m�todo no hace nada.
		*/
		virtual void OnStop();

		/**
		M�todo invocado c�clicamente para que se contin�e con la
		ejecuci�n de la acci�n.
		<p>

		@return Estado de la acci�n tras la ejecuci�n del m�todo;
		permite indicar si la acci�n ha terminado o se ha suspendido.
		*/
		virtual LAStatus OnRun() ;

		/**
		M�todo invocado cuando la acci�n ha sido cancelada (el comportamiento
		al que pertenece se ha abortado por cualquier raz�n).

		La tarea puede en este momento realizar las acciones que
		considere oportunas para "salir de forma ordenada".

		@note <b>Importante:</b> el Abort <em>no</em> provoca la ejecuci�n
		de OnStop().
		*/
		virtual LAStatus OnAbort() ;


	}; // class CLAEntityPerceived

} // namespace AI

#endif __AI_LAEntityPerceived_H
