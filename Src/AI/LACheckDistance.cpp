#include "LACheckDistance.h"
#include "Logic\Entity\Entity.h"
#include "BehaviorExecutionContext.h"
#include "Logic\Maps\Map.h"
#include "Logic\Server.h"

namespace AI
{

	CLACheckDistance::CLACheckDistance(const std::string& entityVariableName, std::string minDistanceToCheckVariable, std::string maxDistanceToCheckVariable) 
		: _entityVariableName(entityVariableName), 
		_minSquareDistance(0.0f),
		_maxSquareDistance(0.0f),
		_minDistanceToCheckVariable(minDistanceToCheckVariable),
		_maxDistanceToCheckVariable(maxDistanceToCheckVariable),
		_entityToCheck(NULL),
		_thisEntity(NULL)
	{
		_name = "CheckDistance";
	}

	//---------------------------------------------------------

	CLACheckDistance::~CLACheckDistance(void)
	{
	}

	//---------------------------------------------------------

	CLatentAction::LAStatus CLACheckDistance::OnStart()
	{
		if (_entityVariableName == "isEntityPerceived") // isEntityPerceived es una lista de entidades percibidas
		{
			if (!_context->hasAttribute("isEntityPerceived"))
				return FAIL;
			std::list<std::string> *entitiesPercivedList = 
				(std::list<std::string> *)_context->getUserData("isEntityPerceived");
			_entityName = entitiesPercivedList->front();
		}
		else
		{
			if (!_context->hasAttribute(_entityVariableName))
				return FAIL;
			_entityName = _context->getStringAttribute(_entityVariableName);
		}

		if(_context->hasAttribute(_minDistanceToCheckVariable))
			_minSquareDistance = _context->getFloatAttribute(_minDistanceToCheckVariable);
		else
			_minSquareDistance = 0.0f;

		if(_context->hasAttribute(_maxDistanceToCheckVariable))
			_maxSquareDistance = _context->getFloatAttribute(_maxDistanceToCheckVariable);
		else
			_maxSquareDistance = FLT_MAX;

		_minSquareDistance *= _minSquareDistance;

		if(_maxSquareDistance != FLT_MAX)
			_maxSquareDistance *= _maxSquareDistance;

		_entityToCheck = Logic::CServer::getSingletonPtr()->getMap()->getEntityByName(_entityName);
		_thisEntity = (CEntity*)_context->getUserData("entity");
		return RUNNING;
	}

	//---------------------------------------------------------

	CLatentAction::LAStatus CLACheckDistance::OnRun()
	{
		CLatentAction::LAStatus out = FAIL;
		if(_entityToCheck && _thisEntity)
		{
			float squareDistance = _entityToCheck->getPosition().squaredDistance(_thisEntity->getPosition());

			if (squareDistance >= _minSquareDistance && squareDistance <= _maxSquareDistance
				&& _minSquareDistance != FLT_MAX && _maxSquareDistance != FLT_MAX)
				printf("");
			out = ((squareDistance >= _minSquareDistance && squareDistance <= _maxSquareDistance) ? SUCCESS : FAIL);
		}
		return out;
	}

	//---------------------------------------------------------

	void CLACheckDistance::OnStop()
	{
		//_perceived = false;
	}

	//---------------------------------------------------------

	CLatentAction::LAStatus CLACheckDistance::OnAbort()
	{
		_entityToCheck = NULL;
		_thisEntity = NULL;
		return FAIL;
	}

	//---------------------------------------------------------

}