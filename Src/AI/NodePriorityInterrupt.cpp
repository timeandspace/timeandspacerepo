#include "NodePriorityInterrupt.h"

namespace AI {

	/**
	* El comportamiento de este nodo es muy parecido al del nodo selector (CNodeSelector).<br>
	* Intenta ejecutar todos los nodos desde el m�s prioritario. Si alguno de los nodos
	* no falla, cancela todos los nodos siguientes, si alguno de ellos se estaba ejecutando.
	* <p>
	* � Si el hijo actual devuelve BT_SUCCESS, el nodo termina con �xito (BT_SUCCESS).<br>
	* � Si el hijo actual devuelve BT_RUNNING, el nodo devuelve BT_RUNNING.
	* En el siguiente tick se continuar� ejecutando el mismo hijo.<br>
	* En estos dos casos, si se estaba ejecutando alg�n nodo de menor prioridad se cancela su
	* ejecuci�n.
	* � Si el hijo actual devuelve BT_FAILURE, se ejecuta el siguiente hijo. Si no hay m�s hijos,
	* el nodo termina con fallo.
	*
	* @param agent dato que podemos pasar al �rbol para su ejecuci�n
	* @return estado en que queda el nodo despu�s de la ejecuci�n
	*/
	BEHAVIOR_STATUS CNodePriorityInterrupt::execute(void* agent)
	{

		if (_children.size() == 0)
			return BT_SUCCESS;

		if (currentPosition == -1)
			init(agent);

		// Nodo que vamos a comprobar a continuaci�n
		unsigned int checkedNode = 0;
		BEHAVIOR_STATUS status;
		BehaviorTreeNode* currentlyRunningNode;
		while (checkedNode < _children.size()) //keep trying children until one doesn't fail
		{
			currentlyRunningNode = _children.at(checkedNode);
			status = currentlyRunningNode->execute(agent);
			if (status == BT_FAILURE) {
				// Si el nodo falla al ejecutarse lo reiniciamos y pasamos al siguiente
				currentlyRunningNode->init(agent);
				checkedNode++;
			} else {
				// Si el nodo actual se ejecuta tenemos que comprobar si se ha interrumpido 
				// la ejecuci�n de otro nodo menos prioritario, para poder reinicializarlo.
				// El �ltimo nodo no ejecutado se almacena en currentPosition. 
				// Tendremos que comprobar si tiene menos prioridad, es decir, si su �ndice
				// es mayor que el que ocupa el que acabamos de ejecutar con �xito.
				if (checkedNode < currentPosition)
					_children.at(currentPosition)->init(agent);
				// Si el nodo ha terminado con �xito, el selector tambi�n termina con �xito
				if (status == BT_SUCCESS) {
					// Adem�s, dejamos inicializado el nodo que acaba de terminar para la pr�xima vuelta
					currentlyRunningNode->init(agent);
					currentPosition = 0;
					return BT_SUCCESS;
				} else {
					// S�lo queda que el status sea BT_RUNNING
					// En este caso actualizamos el nodo actual por si ha cambiado
					currentPosition = checkedNode;
					return BT_RUNNING;
				}
				assert(!"BT in an unsupported state");
			}
		}
		// Si ha llegado al final de la lista de nodos y todos han fallado el selector
		// tendr� tambi�n que fallar
		currentPosition = 0;
		return BT_FAILURE;
	}

} // namespace logica
