#include "NodePriorityCondition.h"

namespace AI {

	/**
	 * El comportamiento de este nodo es muy parecido al del nodo selector (CNodeSelector).<br>
	 * Intenta ejecutar todos los nodos desde el m�s prioritario. Si alguno de los nodos
	 * no falla, cancela todos los nodos siguientes, si alguno de ellos se estaba ejecutando.
	 * <p>
	 * � Si el hijo actual devuelve BT_SUCCESS, el nodo termina con �xito (BT_SUCCESS).<br>
	 * � Si el hijo actual devuelve BT_RUNNING, el nodo devuelve BT_RUNNING.
	 * En el siguiente tick se continuar� ejecutando el mismo hijo.<br>
	 * En estos dos casos, si se estaba ejecutando alg�n nodo de menor prioridad se cancela su
	 * ejecuci�n.
	 * � Si el hijo actual devuelve BT_FAILURE, se ejecuta el siguiente hijo. Si no hay m�s hijos,
	 * el nodo termina con fallo.
	 *
	 * @param agent dato que podemos pasar al �rbol para su ejecuci�n
	 * @return estado en que queda el nodo despu�s de la ejecuci�n
	 */
	BEHAVIOR_STATUS CNodePriorityCondition::execute(void* agent)
	{

		if (_children.size() == 0)
			return BT_SUCCESS;

		if (currentPosition == -1)
			init(agent);

		// Condici�n que vamos a comprobar a continuaci�n
		unsigned int checkedCondition = 0;
		CLatentAction* currentCondition;
		while (checkedCondition < _conditions.size()) {
			currentCondition = _conditions.at(checkedCondition);
			CLatentAction::LAStatus status;
			// La condici�n puede ser NULL. En ese caso consideramos que siempre tiene �xito
			if (currentCondition == NULL) {
				status = CLatentAction::SUCCESS;
			} else {
				status = currentCondition->tick();
				currentCondition->reset();
			}
			if (status != CLatentAction::SUCCESS) {
				// Si la condici�n falla pasaremos a la siguiente
				checkedCondition++;
			} else {
				// Si la condici�n tiene �xito tenemos que comprobar si se ha interrumpido 
				// la ejecuci�n de otro nodo para poder reinicializarlo.
				// El �ltimo nodo ejecutado se almacena en currentPosition. 
				// Tendremos que comprobar si no es el mismo que el de la condici�n actual.
				if (checkedCondition != currentPosition) {
					// Como el nodo actual se ha cancelado, lo reiniciamos si es necesario
					if (currentPosition != -1)
						_children.at(currentPosition)->init(agent);
					// Ponemos como nodo actual el nodo correspondiente a la condici�n
					currentPosition = checkedCondition;
				}

				// Ejecutamos el nodo actual (currentPosition)
				BehaviorTreeNode* currentNode = _children.at(currentPosition);
				BEHAVIOR_STATUS nodeStatus = currentNode->execute(agent);
				// Si termina con �xito, el selector termina tambi�n con �xito
				if (nodeStatus == BT_SUCCESS) {
					// Adem�s, dejamos inicializado el nodo que acaba de terminar para la pr�xima vuelta
					currentNode->init(agent);
					currentPosition = -1;
					return BT_SUCCESS;
				} else if (nodeStatus == BT_RUNNING) {
					// Si el nodo actual sigue en ejecuci�n, el selector tambi�n lo hace
					return BT_RUNNING;
				} else {
					// Si el nodo actual falla, tendremos que probar con la siguiente condici�n
					checkedCondition ++;
				}
			}
		}
		// Si fallan todos los nodos o sus condiciones devolveremos un fallo
		currentPosition = -1;
		return BT_FAILURE;
	}


	BehaviorTreeInternalNode* CNodePriorityCondition::addChild(BehaviorTreeNode* newChild, CLatentAction* condition) {
		CNodeSelector::addChild(newChild);
		_conditions.push_back(condition);
		return this;
	}


} // namespace logica
