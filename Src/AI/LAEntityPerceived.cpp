#include "LAEntityPerceived.h"
#include "BehaviorExecutionContext.h"
#include "PerceptionManager.h"

#include "Logic\Server.h"
#include "Logic\Entity\Entity.h"
#include "Logic\Maps\Map.h"

namespace AI
{

	CLAEntityPerceived::CLAEntityPerceived(const std::string & entityType, const std::string & varName) 
		: _entityType(entityType), _varName(varName), _perceivedSight(false), _perceivedHear(false),
		_entityPerceived(NULL)
	{
		_name = "EntityPerceived";
	}

	//---------------------------------------------------------

	CLAEntityPerceived::~CLAEntityPerceived(void)
	{
	}

	//---------------------------------------------------------

	bool CLAEntityPerceived::accept(Logic::CMessage *message)
	{
		/*if (message->getType() == Message::PERCEIVED)
		{
		CPerceivedMessage *msg = static_cast<CPerceivedMessage *>(message);
		if (msg->_entity->getType() == _entityTypeValue)
		{
		return true;
		}
		}*/
		return message->getType() == Message::PERCEIVED;
	}

	//---------------------------------------------------------

	void CLAEntityPerceived::process(Logic::CMessage *message)
	{
		if (message->getType() == Message::PERCEIVED)
		{
			CPerceivedMessage *msg = static_cast<CPerceivedMessage *>(message);
			std::string entityName = msg->_entityName;
			CEntity *entity = Logic::CServer::getSingletonPtr()->getMap()->getEntityByName(entityName);
			if ( msg->_entityType == _entityTypeValue)
			{
				if (msg->_perceivedType == PERCEPTION_SIGHT)
					_perceivedSight = msg->_perceived;
				else 
					_perceivedHear = msg->_perceived;

				if (_perceivedSight || _perceivedHear)
				{
					_entityPerceived = entity;
					//_context->setIntAttribute("isEntityPerceived", 1);
					// Guardar� una lista con todas las entidades percibidas
					if (_context->hasAttribute("isEntityPerceived"))
					{
						std::list<std::string> *entitiesPercivedList = 
							(std::list<std::string> *)_context->getUserData("isEntityPerceived");

						std::list<std::string>::iterator it = entitiesPercivedList->begin();
						std::list<std::string>::iterator end = entitiesPercivedList->end();
						bool flag = false;
						for ( ; it != end; ++it)
						{
							if (entityName == (*it))
							{
								flag = true;
								break;
							}
						}
						if (!flag)
							entitiesPercivedList->push_back(entityName);
					}
					else
					{
						std::list<std::string> *entitiesPercivedList = new std::list<std::string>();
						entitiesPercivedList->push_back(entityName);
						_context->setUserData("isEntityPerceived", entitiesPercivedList);
					}
				}
				else
				{
					_entityPerceived = NULL;
					if (_context->hasAttribute("isEntityPerceived"))
					{
						std::list<std::string> *entitiesPercivedList = 
							(std::list<std::string> *)_context->getUserData("isEntityPerceived");

						std::list<std::string>::iterator it = entitiesPercivedList->begin();
						std::list<std::string>::iterator end = entitiesPercivedList->end();
						for ( ; it != end; )
						{
							if (entityName == (*it))
								it = entitiesPercivedList->erase(it);
							else
								++it;
						}
						if (entitiesPercivedList->size() == 0)
						{
							_context->removeAttribute("isEntityPerceived");
							delete entitiesPercivedList;
							entitiesPercivedList = NULL;
						}
					}
				}
			}
		}
	}

	//---------------------------------------------------------

	CLatentAction::LAStatus CLAEntityPerceived::OnStart()
	{
		_entityTypeValue = _context->getStringAttribute(_entityType);
		return RUNNING;
	}

	//---------------------------------------------------------

	CLatentAction::LAStatus CLAEntityPerceived::OnRun()
	{
		if(_entityPerceived)
			_context->setVector3Attribute(_varName, _entityPerceived->getPosition());

		CLatentAction::LAStatus out = ((_perceivedSight || _perceivedHear) ? SUCCESS : FAIL);

		return out;
	}

	//---------------------------------------------------------

	void CLAEntityPerceived::OnStop()
	{
		//_perceived = false;
	}

	//---------------------------------------------------------

	CLatentAction::LAStatus CLAEntityPerceived::OnAbort()
	{
		_perceivedSight = _perceivedHear = false;
		return FAIL;
	}

	//---------------------------------------------------------

}