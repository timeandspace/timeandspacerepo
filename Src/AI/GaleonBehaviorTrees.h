/**
 * @file GaleonBehaviorTrees.h
 *
 * Distintas clases que implementan comportamientos de ejemplo para los BTs de Gale�n.
 * <p>
 * Las clases incluidas son:<br>
 * � CBTWander: comportamiento que recorre puntos aleatorios del nivel, uno tras otro.<br>
 * � CBTFollow: comportamiento que persigue al jugador.<br>
 * � CBTLastPosition: comportamiento que va hasta la �ltima posici�n conocida del jugador.<br>
 * � CBTTurnAround: comportamiento que mira en 3 direcciones aleatorias.<br>
 * � CBTMainBehavior: comportamiento que combina los anteriores en uno m�s complejo.<br>
 *
 *
 * @author "Gonzalo Fl�rez"
 * @date 11/04/2011
 */

#pragma once

#ifndef AI_GaleonBehaviorTrees_H_
#define AI_GaleonBehaviorTrees_H_

#include "BehaviorTreeBase.h"
#include "NodeParallel.h"
#include "NodeSequential.h"
#include "NodeRepeat.h"
#include "NodePriorityCondition.h"



namespace AI
{
	/**
	 * Comportamiento que recorre puntos aleatorios del nivel, uno tras otro.
	 * <p>
	 * El comportamiento repetir� continuamente los siguientes pasos:<bR>
	 * � Ir hasta un punto aleatorio del mapa.<bR>
	 * � Esperar durante 5 segundos.<bR>
	 *
	 * @author "Gonzalo Fl�rez"
	 * @date 11/04/2011
	 */
	class CBTWander : public CNodeRepeat
	{
	public:
		CBTWander(void);
		~CBTWander(void) {};
	}; //class CBTWander

	/**
	 * Comportamiento que persigue al jugador.
	 * <p>
	 * Es un comportamiento que ejecuta dos acciones al mismo tiempo:<bR>
	 * � Por un lado, comprueba constantemente si se ha percibido la entidad.<bR>
	 * � Al mismo tiempo, se mueve hasta la posici�n de la entidad.<bR>
	 * <p>
	 * Este comportamiento debe fallar si alguna de las dos acciones falla.
	 * <p>
	 * Para realizar el movimiento no es necesario usar el componente de rutas con A*,
	 * ya que si estamos viendo al jugador significa que no hay obst�culos de por medio y
	 * podemos desplazarnos en l�nea recta (en Gale�n no hay cristales ni precipicios).
	 *
	 * @author "Gonzalo Fl�rez"
	 * @date 11/04/2011
	 */
	class CBTFollow : public CNodeParallel 
	{
	public:
		CBTFollow();
		~CBTFollow() {};
	}; // class CBTFollow

	/**
	 * Comportamiento que va hasta la �ltima posici�n conocida del jugador.
	 * <p>
	 * Ejecuta la siguiente secuencia de acciones:<bR>
	 * � Comprueba que existe en el contexto de ejecuci�n el dato con la posici�n del
	 * jugador. Este dato se habr� guardado en el comportamiento CBTFollow.<bR>
	 * � Si es as�, se mueve hasta esta posici�n.<bR>
	 * � Se ejecuta el comportamiento CBTTurnAround (mira en diferentes direcciones aleatoriamente).<bR>
	 * � Por �ltimo se borra la posici�n del contexto (se "olvida").<bR>
	 * <p>
	 * Para realizar el movimiento no es necesario usar el componente de rutas con A*,
	 * ya que si estamos viendo al jugador significa que no hay obst�culos de por medio y
	 * podemos desplazarnos en l�nea recta (en Gale�n no hay cristales ni precipicios).
	 *
	 * @author "Gonzalo Fl�rez"
	 * @date 11/04/2011
	 */
	class CBTLastPosition : public CNodeSequential 
	{
	public :
		CBTLastPosition();
		~CBTLastPosition() { };
	}; // class CBTLastPosition 

	/**
	 * Comportamiento que mira en 3 direcciones aleatorias.
	 * <p>
	 * Repite 3 veces:<bR>
	 * � Mira en una direcci�n aleatoria.<bR>
	 * � Espera durante 1500 ms.<bR>
	 *
	 * @author "Gonzalo Fl�rez"
	 * @date 11/04/2011
	 */
	class CBTTurnAround : public CNodeRepeat 
	{
	public :
		CBTTurnAround();
		~CBTTurnAround() { };
	}; // class CBTLastPosition 

	/**
	 * Comportamiento que combina los anteriores en uno m�s complejo.
	 * <p>
	 * Es un selector con prioridad. Las acciones que realiza, por orden de prioridad son:<br>
	 *
	 * � Intenta seguir al jugador. Si falla (en general, fallar� porque no puede percibirlo)...<bR>
	 * � Va a la �ltima posici�n que recuerda del jugador. Si falla (no recuerda su posici�n)...<br>
	 * � Va hasta un punto aleatorio del mapa.<br>
	 *
	 * @author "Gonzalo Fl�rez"
	 * @date 11/04/2011
	 */
	class CBTMainBehavior : public CNodePriorityCondition
	{
	public:
		CBTMainBehavior ();
		~CBTMainBehavior () {};
	};  // class CBTMainBehavior

} //namespace AI

#endif AI_GaleonBehaviorTrees_H_
