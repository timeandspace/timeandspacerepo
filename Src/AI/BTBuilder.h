/**
* @file NodeRepeat.h
*
* Implementación de un nodo (decorador) repetidor
*
* @author "Alvaro Blazquez Checa"
* @date 11/04/2011
*/

#ifndef AI_BTBuilder_H_
#define AI_BTBuilder_H_

#include <string>
#include <map>
#include <vector>
#include <rapidxml.hpp>

namespace AI
{
	class BehaviorTreeNode;
	class CLatentAction;
	
	class CBTBuilder
	{

	public:
		CBTBuilder();
        virtual ~CBTBuilder();
 
		BehaviorTreeNode* parseBT(const std::string &fileName);
        
		BehaviorTreeNode* getBT(const std::string &btName);

		void addBtToLibrary(const std::string &name, BehaviorTreeNode * behaviourTree);
	
		void parseLibrary(std::string directoryPath);

	private:

		void parseNode(rapidxml::xml_node<>* node, BehaviorTreeNode* &btParentNode);

		void parseCompositeNode(rapidxml::xml_node<>* node, BehaviorTreeNode* &btNode);

		void parseActionNode(rapidxml::xml_node<>* node, BehaviorTreeNode* &btNode);

		void parseDecoratorNode(rapidxml::xml_node<>* node, BehaviorTreeNode* &btNode);

		void parseConditionNode(rapidxml::xml_node<>* node, CLatentAction* &condition);

		void addSubTree(rapidxml::xml_node<>* node, BehaviorTreeNode* &btNode);

		void parsePriorityConditionNode(rapidxml::xml_node<>* node, BehaviorTreeNode* &btNode);

		BehaviorTreeNode* _rootNode;
		
		typedef std::map<std::string,BehaviorTreeNode*> TreeMap;
		typedef std::map<std::string,BehaviorTreeNode*>::iterator TreeMapIter;

		TreeMap _btLibrary;

		std::string _parsedTreeName;

	};
}

#endif AI_BTBuilder_H_
