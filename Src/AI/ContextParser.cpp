
#include "ContextParser.h"

#include <rapidxml_utils.hpp>

#include "BehaviorExecutionContext.h"

using namespace AI;
using namespace std;
using namespace rapidxml;

namespace AI
{

	Vector3 getVector3Attribute(const std::string &attr)
	{
		// Recuperamos la cadena  "x y z"
		
		int space1 = attr.find(',');
		float x = (float)atof(attr.substr(0,space1).c_str());
		int space2 = attr.find(',',space1+1);
		float y = (float)atof(attr.substr(space1+1,space2-(space1+1)).c_str());
		float z = (float)atof(attr.substr(space2+1,attr.size()-(space2+1)).c_str());

		return Vector3(x,y,z);

	} // getVector3Attribute

	float getFloatAttribute(const std::string &attr)
	{
		return (float)atof(attr.c_str());

	}


	CContextParser::CContextParser() : _context(NULL)
	{
	}
 
 
	CContextParser::~CContextParser()
	{
	
	}
 
	CBehaviorExecutionContext* CContextParser::parseContext(const std::string &fileName)
	{
		_context = NULL;

		rapidxml::file<> xmlFile(fileName.c_str());
	
		rapidxml::xml_document<> doc;
		doc.parse<0>(xmlFile.data());
		rapidxml::xml_node<>* XMLRoot;
 
		XMLRoot = doc.first_node();
 
		std::string type = XMLRoot->name();

		_context = new AI::CBehaviorExecutionContext();

		for (xml_node<>* node = XMLRoot->first_node() ; node; node = node->next_sibling())
		{
			type = node->first_attribute("Type")->value();
				
			if(type == "Vector3")
			{
				parseVector3(node);
			}
			else if(type == "Float")
			{
				parseFloat(node);
			}
			else if(type == "String")
			{
				parseString(node);
			}
		}

		return _context;
	}

	void CContextParser::parseVector3(rapidxml::xml_node<>* node)
	{
		std::string variableName;
		std::string vector;
		rapidxml::xml_node<>* childNode = node->first_node();

		variableName = node->first_attribute("Name")->value();
		
		vector = childNode->value();

		Vector3 vector3 = getVector3Attribute(vector);

		_context->setVector3Attribute(variableName, vector3);
		
	}

	void CContextParser::parseFloat(rapidxml::xml_node<>* node)
	{
		std::string variableName;
		std::string floatStringValue;
		rapidxml::xml_node<>* childNode = node->first_node();

		variableName = node->first_attribute("Name")->value();
		
		floatStringValue = childNode->value();

		float floatValue = getFloatAttribute(floatStringValue);

		_context->setFloatAttribute(variableName, floatValue);
		
	}

	void CContextParser::parseString(rapidxml::xml_node<>* node)
	{
		std::string variableName;
		std::string stringValue;

		rapidxml::xml_node<>* childNode = node->first_node();

		variableName = node->first_attribute("Name")->value();
		
		stringValue = childNode->value();

		_context->setStringAttribute(variableName, stringValue);
		
	}


	
}