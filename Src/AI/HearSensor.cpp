/**
@file HearSensor.h

Contiene la implementaci�n de un sensor de sonido simple.

@author Alejandro P�rez Alonso
@date Abril, 2014
*/
#include "HearSensor.h"

#include "PerceptionEntity.h"
#include "PerceptionSignal.h"

#include "BaseSubsystems/Math.h"
#include "Physics/Server.h"

#include "Logic/Entity/Entity.h"

namespace AI 
{

	CHearSensor::~CHearSensor(void)
	{
	}

	/**
	Realiza todas las comprobaciones necesarias para averiguar si la se�al recibida
	es escuchada o no.
	@param perceptible Se�al cuya percepci�n queremos comprobar
	@param time Instante en el que se realiza la comprobaci�n de percepci�n
	@return false si no se percibe, true si s�.
	*/
	bool CHearSensor::perceives(CPerceptionSignal * signal, unsigned long time) 
	{
		// Realizamos las comprobaciones
		// Si alguna de ellas falla tendremos que devolver NULL para indicar que no ha habido percepci�n
		// Si todas tienen �xito devolveremos una instancia de CNotification
		if (signal == NULL) return false;

		// Comprobaciones: 
		// 1. Comprueba si el sensor y la se�al est�n activos 
		// (m�todo isActive de la se�al - se recibe como par�metro- y del sensor)
		if (!signal->isActive() || !this->isActive()) return false;

		// 2. Comprueba si el tipo de la se�al se corresponde con el tipo que percibe el sensor
		// (m�todo getType)
		if (signal->getType() != this->getType()) return false;

		// 3. Comprueba si la intensidad de la se�al est� por encima del threshold del sensor.
		// La intensidad que percibimos, F, es el resultado de usar una funci�n de atenuaci�n:
		// f(F0, dist) = F	donde F0 es la intensidad inicial y dist la distancia.
		// En nuestro caso vamos a simplificar y usaremos una funci�n constante:
		// f(F0, dist) = F0
		// Es decir, que s�lo tendremos que comprobar que el threshold sea mayor que la intensidad
		if (this->getThreshold() > signal->getIntensity()) return false;

		// 4. Comprueba la distancia entre la se�al y el sensor. Se realiza en 2 pasos:
		// 4.1. Primero se comprueba si la distancia en cada dimensi�n es mayor que la distancia m�xima.
		// Sacamos la matriz de transformaci�n de la entidad de percepci�n (getTransform) 
		// y con ella podemos obtener la posici�n del sensor (getTrans)
		// Hacemos lo mismo con la se�al. La diferencia entre estos vectores es el vector de distancia
		// Comparamos cada una de sus dimensiones con la distancia m�xima
		Matrix4 transform = this->getPerceptionEntity()->getTransform();
		Vector3 position = transform.getTrans();
		Vector3 signalPosition = signal->getPerceptionEntity()->getTransform().getTrans();
		Vector3 dist = signalPosition - position;
		if (abs(dist.x) > _maxDistance || abs(dist.z) > _maxDistance) return false;

		// 4.2. A continuaci�n se comprueba si la distancia eucl�dea es mayor que la distancia m�xima.
		// Usamos el vector distancia que hemos calculado antes
		float distMagnitude = dist.length();
		if (distMagnitude > _maxDistance) return false;

		// Aplicamos atenuaci�n del sonido con la distancia
		float intensity = signal->getIntensity();
		intensity -= distMagnitude * DISTANCE_ATTENUATION;
		if (this->getThreshold() > intensity) return false; 

		position.y = 2; //para evitar colisionar con objetos bajos.
		Ray ray = Ray(position, dist / distMagnitude);
		std::vector<Logic::CEntity *> *entities = 
			Physics::CServer::getSingletonPtr()->
			raycastMultiple(ray, distMagnitude, 
			Physics::FilterGroup::EVERYTHING | ~Physics::FilterGroup::PLAYER_GROUP |
			~Physics::FilterGroup::CLONES_GROUP | ~Physics::FilterGroup::ENEMY_GROUP);

		// Por cada entidad que atraviese el rayo reducimos la intensidad de la se�al una 
		//cantidad determinada 
		if (entities != NULL)
		{
			for (unsigned int i = 0; i < entities->size(); ++i)
			{
				intensity -= COLLISION_ATTENUATION; // @TODO:: * grosor del objeto
				if (this->getThreshold() > intensity) {
					delete entities;
					return false;
				}
			}
		}

		delete entities;

		// Si todos los chequeos han tenido �xito tenemos que devolver una nueva instancia de CNotification
		// Los par�metros que necesita son:
		// � El instante en el que hay que entregarla (ahora mismo ==> time)
		// � El sensor que lo ha percibido (this)
		// � La entidad de percepci�n que ha generado la se�al
		// Devolvemos la notificaci�n para que se env�e a la entidad

		return true;
	}

} // namespace AI 
