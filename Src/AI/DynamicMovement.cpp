/**
@file DynamicMovement.cpp

Contiene distintas clases que heredan de IMovement y que 
implementan distintos tipos de movimiento din�mico.

@author Gonzalo Fl�rez
@date Diciembre, 2010

@author Alejandro P�rez Alonso
@date Abril, 2014
*/
#include "DynamicMovement.h"
#include "Server.h"
#include "Physics\Server.h"

namespace AI 
{

	/**
	Efect�a el movimiento.

	@param msecs Tiempo que dura el movimiento.
	@param currentProperties Par�metro de entrada/salida donde se reciben las velocidades actuales y 
	en �l devuelve los nuevos valores de aceleraci�n.
	*/
	void CDynamicSeek::move(unsigned int msecs, DynamicMovement& currentProperties)
	{
		assert(_entity);
		// Implementar el movimiento Seek (din�mico)
		// En _entity tenemos un puntero a la entidad.
		// En _maxLinearAccel tenemos la aceleraci�n m�xima que puede tener la entidad.
		// currentProperties es un par�metro de entrada/salida en el que se recibe las velocidades/aceleraciones
		// actuales y se modifica con los nuevos valores de velocidad/aceleraci�n.
		currentProperties.linearAccel = _target - _entity->getPosition();
		if (currentProperties.linearAccel.squaredLength() > _maxLinearAccel2) {
			currentProperties.linearAccel.normalise();
			currentProperties.linearAccel *= _maxLinearAccel;
		}
	}

	/**
	Efect�a el movimiento.

	@param msecs Tiempo que dura el movimiento.
	@param currentProperties Par�metro de entrada/salida donde se reciben las velocidades actuales y 
	en �l devuelve los nuevos valores de aceleraci�n.
	*/
	void CDynamicFlee::move(unsigned int msecs, DynamicMovement& currentProperties)
	{
		assert(_entity);
		// Implementar el movimiento Seek (din�mico)
		// En _entity tenemos un puntero a la entidad.
		// En _maxLinearAccel tenemos la aceleraci�n m�xima que puede tener la entidad.
		// currentProperties es un par�metro de entrada/salida en el que se recibe las velocidades/aceleraciones
		// actuales y se modifica con los nuevos valores de velocidad/aceleraci�n.
		currentProperties.linearAccel = _entity->getPosition() - _target;
		if (currentProperties.linearAccel.squaredLength() > _maxLinearAccel2) {
			currentProperties.linearAccel.normalise();
			currentProperties.linearAccel *= _maxLinearAccel;
		}
	}

	//---------------------------------------------------------

	/**
	Efect�a el movimiento.

	@param msecs Tiempo que dura el movimiento.
	@param currentProperties Par�metro de entrada/salida donde se reciben las velocidades actuales y 
	en �l devuelve los nuevos valores de aceleraci�n.
	*/
	void CDynamicArrive::move(unsigned int msecs, DynamicMovement &currentProperties)
	{
		assert(_entity);
		// Implementar el movimiento Arrive (din�mico).
		// En _entity tenemos un puntero a la entidad.
		// En _maxLinearSpeed tenemos la velocidad m�xima a la que se puede mover la entidad y 
		// en _maxLinearAccel la aceleraci�n m�xima.
		// Otras constantes �tiles son IMovement::SLOW_RADIUS (radio a partir del cual empezamos a frenar
		// para aproximarnos a un punto) y IMovement::ATTENUATION (atenuaci�n de la aceleraci�n).
		// currentProperties es un par�metro de entrada/salida en el que se recibe las velocidades/aceleraciones
		// actuales y se modifica con los nuevos valores de velocidad/aceleraci�n.
		Vector3 targetSpeed = _target - _entity->getPosition();
		float distance = targetSpeed.length();
		float targetMagnitude;
		if (distance >= IMovement::SLOW_RADIUS) {
			targetMagnitude = _maxLinearSpeed;
		} else {
			targetMagnitude = distance * _maxLinearSpeed / IMovement::SLOW_RADIUS;
		}
		targetSpeed /= distance;
		targetSpeed *= targetMagnitude;
		currentProperties.linearAccel = targetSpeed - currentProperties.linearSpeed;
		currentProperties.linearAccel/= IMovement::ATTENUATION;
		if (currentProperties.linearAccel.squaredLength() > _maxLinearAccel2) {
			currentProperties.linearAccel.normalise();
			currentProperties.linearAccel *= _maxLinearAccel;
		}
	}

	//---------------------------------------------------------

	const float CObstacleAvoidance::MAX_ANGLE = Math::PI / 2;
	const float CObstacleAvoidance::MIN_ANGLE = Math::PI / 18;
	/**
	*/
	void CObstacleAvoidance::move(unsigned int msecs, DynamicMovement &currentProperties, float distToTarget)
	{
		assert(_entity);

		Physics::CServer *physicsServer = Physics::CServer::getSingletonPtr();
		Vector3 newTarget = _target;
		Vector3 entityPos = _entity->getPosition();

		// Comprobar si hay que evitar alg�n obst�culo
		std::vector<Physics::Collision> collisions;
		std::vector<Ray> rayList;
		std::vector<float> maxDistList;
		int groups;
		Vector3 direction = currentProperties.linearSpeed;
		Vector3 entDir = Math::getDirection(_entity->getOrientation());
		direction.y = entDir.y;
		direction.normalise();
		/**
		RAYOS: habr� un rayo principal y dos m�s cortos a los lados para evitar colisiones 
		laterales
		*/
		// grupos a evitar
		groups = Physics::FilterGroup::WORLD | Physics::FilterGroup::PLAYER_GROUP |
			Physics::FilterGroup::CLONES_GROUP;

		// Distancia a la que lanzar los rayos
		float lookAhead = min(distToTarget, IMovement::LOOK_AHEAD);
		// Colision con el rayo central, derecho e izquierdo
		Physics::Collision centralCollision, rightCollision, leftCollision;
		Ray centralRay(entityPos + Vector3(0,0.5,0) + 0.6f * direction, direction);  //@TODO:: quitar el cableo
		bool hasCentralCol = physicsServer->raycastSingle(centralRay, lookAhead, groups, centralCollision);
		// Rayo derecho
		//Ray leftRay = Ray( modifiedEntityPos, Quaternion(Ogre::Degree(rayAngle), Vector3::UNIT_Y) * direction);
		Vector3 direction2 = Quaternion(Ogre::Radian(_angle), Vector3::UNIT_Y) * direction;
		/*Vector3 direction2(direction.x * cos(_angle) - direction.z * sin(_angle), direction.y, 
			direction.x * sin(_angle) + direction.z * cos(_angle));*/
		direction2.normalise();
		Ray rightRay(entityPos + Vector3(0,0.5,0) + 0.6f * direction, direction2);
		bool hasRightCol = physicsServer->raycastSingle(rightRay, lookAhead/2, groups, rightCollision);
		// Rayo izquierdo
		/*Vector3 direction3(direction.x * cos(-_angle) - direction.z * sin(-_angle), direction.y, 
			direction.x * sin(-_angle) + direction.z * cos(-_angle));*/
		Vector3 direction3 = Quaternion(Ogre::Radian(-_angle), Vector3::UNIT_Y) * direction;
		direction3.normalise();
		Ray leftRay(entityPos + Vector3(0,0.5,0) + 0.6f * direction, direction3);
		bool hasLeftCol = physicsServer->raycastSingle(leftRay, lookAhead/2, groups, leftCollision);


		/***
		COMPRABACIONES DE COLISIONES
		*/
		// Si ning�n rayo colisiona entonces no modificamos el target
		if (!hasCentralCol && !hasRightCol && !hasLeftCol)
		{
			currentProperties.linearAccel = 0;
			// Si no hay colision recogemos los �ngulos
			_angle -= Math::PI / 36;
			if (_angle < MIN_ANGLE) _angle = MIN_ANGLE;
			return;
		}
		else if (hasCentralCol && !hasRightCol && !hasLeftCol)
		{
			// Si la normal es igual a la direction pero en sentido contrario
			if (centralCollision.normal.angleBetween(direction).valueDegrees() == 0.0f)
			{
				//@TODO:
				printf("");
			}
			_target = centralCollision.position + centralCollision.normal * IMovement::AVOID_DISTANCE;
			_target.y = 0;
		}
		else if (hasRightCol && !hasLeftCol) // mover a la izquierda
		{
			//Girar hacia el lado que no colisiona
			if (hasCentralCol)
				_target = _entity->getPosition() + direction3 * IMovement::AVOID_DISTANCE;
			else
				_target = _entity->getPosition() + direction * IMovement::AVOID_DISTANCE;
			_target.y = 0;
			// Aumenta el angulo de los radios
			_angle += Math::PI / 36;
			if (_angle > MAX_ANGLE) _angle = MAX_ANGLE;
		}
		else if (!hasRightCol && hasLeftCol) // mover a la derecha
		{
			if (hasCentralCol)
				_target = _entity->getPosition() + direction2 * IMovement::AVOID_DISTANCE;
			else
				_target = _entity->getPosition() + direction * IMovement::AVOID_DISTANCE;
			_target.y = 0;
			// Aumenta el angulo de los radios
			_angle += Math::PI / 36;
			if (_angle > MAX_ANGLE) _angle = MAX_ANGLE;
		}
		else 
		{
			// @TODO: Si colisionan los 3 rayos, habr�a que ampliar el angulo hasta que uno de los dos deje de colisionar
			// Si el angulo es > 90�, elegir alguno de los 2 lados al azar

			// Elegir el de menor distancia de colision, en caso de empate, random
			if (rightCollision.distance > leftCollision.distance) _lastDirection = 0;
			else if (rightCollision.distance < leftCollision.distance) _lastDirection = 1;
			else
				_lastDirection = rand() % 2;

			if (_lastDirection == 0)
			{
				_target = _entity->getPosition() + direction3 * IMovement::AVOID_DISTANCE;
				_target.y = 0;

			}
			else
			{
				_target = _entity->getPosition() + direction2 * IMovement::AVOID_DISTANCE;
				_target.y = 0;
			}
			_angle += Math::PI / 36;
			if (_angle > MAX_ANGLE) _angle = MAX_ANGLE;

		}

		// Llamamos al seek con el nuevo target calculado
		CDynamicSeek::move(msecs, currentProperties);

	}

	//-------------------------------------------------------------------

	const float maxPrediction = 1000.0f;

	void CDynamicPursue::move(unsigned int msecs, DynamicMovement &currentProperties)
	{

		//establecer la prediccion

		Vector3 direction = _entityToPursue->getPosition() - _entity ->getPosition();

		float distance = direction.length();

		float speed = currentProperties.linearSpeed.length();

		float prediction;

		if(speed < distance / maxPrediction)
			prediction = maxPrediction;
		else
			prediction = distance / speed;

		_target = _entityToPursueProperties->linearSpeed * prediction + _entityToPursue->getPosition();

		/*IMovement* movement = IMovement::getMovement(IMovement::MOVEMENT_DYNAMIC_SEEK,_maxLinearSpeed,_maxAngularSpeed,_maxLinearAccel,_maxAngularAccel);

		movement->setEntity(_entity);

		movement->setTarget(_target);

		movement->move(msecs, currentProperties);

		delete movement;*/
		// Llamamos al seek con el nuevo target calculado
		CDynamicSeek::move(msecs, currentProperties);

	}

	//-------------------------------------------------------------------

	void CDynamicEvade::move(unsigned int msecs, DynamicMovement &currentProperties)
	{

		//establecer la prediccion

		Vector3 direction = _entityToEvade->getPosition() - _entity ->getPosition();

		float distance = direction.length();

		float speed = currentProperties.linearSpeed.length();

		float prediction;

		if(speed < distance / maxPrediction)
			prediction = maxPrediction;
		else
			prediction = distance / speed;

		_target = _entityToEvadeProperties->linearSpeed * prediction + _entityToEvade->getPosition();

		// Llamamos al flee con el nuevo target calculado
		CDynamicFlee::move(msecs, currentProperties);

	}

}

