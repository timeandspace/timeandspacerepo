/**
* @file NodeRepeat.h
*
* Implementación de un nodo (decorador) repetidor
*
* @author "Alvaro Blazquez Checa"
* @date 11/04/2011
*/

#ifndef AI_ContextParser_H_
#define AI_ContextParser_H_

#include <string>
#include <rapidxml.hpp>



namespace AI
{
	class CBehaviorExecutionContext;
	
	class CContextParser
	{

	public:
		CContextParser();
        ~CContextParser();
 
		CBehaviorExecutionContext* parseContext(const std::string &fileName);
	

	private:

		void parseVector3(rapidxml::xml_node<>* node);

		void parseFloat(rapidxml::xml_node<>* node);

		void parseString(rapidxml::xml_node<>* node);

		CBehaviorExecutionContext* _context;
	
	};
}

#endif AI_ContextParser_H_
