#pragma once

#ifndef __AI_LAExistsData_H
#define __AI_LAExistsData_H

#include "LatentAction.h"

namespace AI 
{

	class CLARandomTurn : public CLatentAction
	{
	public:
		CLARandomTurn(void) : CLatentAction() {
			_name = "RandomTurn";
		};
		virtual ~CLARandomTurn(void);
		LAStatus OnStart();
		/**
		Devuelve true si a la acci�n le interesa el tipo de mensaje
		enviado como par�metro.
		@param msg Mensaje que ha recibido la entidad.
		@return true Si la acci�n est� en principio interesada
		por ese mensaje.
		*/
		virtual bool accept(CMessage *message) {return false;} ;
		/**
		Procesa el mensaje recibido. El m�todo es invocado durante la
		ejecuci�n de la acci�n cuando se recibe el mensaje.

		@param msg Mensaje recibido.
		*/
		virtual void process(CMessage *message){};

		virtual CLatentAction* clone()
		{
			return new CLARandomTurn(*this);
		}

	};

} // namespace AI 


#endif __AI_LAExistsData_H
