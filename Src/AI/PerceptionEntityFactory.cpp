#include "PerceptionEntityFactory.h"

#include "SightSensor.h"
#include "HearSensor.h"
#include "PerceptionSignal.h"

namespace AI 
{

	/**
	* Devuelve una entidad de percepci�n a partir de una cadena de texto que identifica su tipo.
	*
	* @param type Tipo de entidad de percepci�n. Por ahora s�lo admite "enemy" y "player"
	* @param radius Radio de la entidad de percepci�n
	* @param userData Datos de usuario asociados a la entidad de percepci�n
	* @param listener Listener que ser� notificado cuando la entidad perciba una se�al
	* @return CPerceptionEntity del tipo correspondiente al par�metro type
	*/
	CPerceptionEntity* CPerceptionEntityFactory::getPerceptionEntity(string type, 
		void* userData, IPerceptionListener* listener)
	{
		if (type == "enemy") {
			return new CPerceptionEntityEnemy(type, userData, listener);
		} else if (type == "player") {
			return new CPerceptionEntityPlayer(type, userData, listener);
		} /*else if (type == "allied") {
			return new CPerceptionEntityAllied(type, userData, listener);
		}*/
		else if (type == "object")
		{
			return new CPerceptionEntityObject(type, userData, listener);
		}
		else
			return NULL;
	}

	/**
	* Crea una entidad de percepci�n que representa a un enemigo.
	*
	* Esta clase s�lo tiene un sensor de visi�n b�sico (AI::CSightSensor), inicializado
	* con los siguientes par�metros:
	* � active = true
	* � threshold = 1.0
	* � maxDistance = 200.0
	* � alpha = PI / 4
	*
	* Cuidado: estos par�metros son datos y, por lo tanto, deber�an declararse en un archivo de configuraci�n externo.
	*
	*
	* @author Gonzalo Fl�rez
	* @date 11/04/2011
	*/
	CPerceptionEntityEnemy::CPerceptionEntityEnemy(std::string type, void* userData, 
		IPerceptionListener* listener) :CPerceptionEntity(type, userData, listener)
		
	{
		
		//((std::map<std::string, void*>*)userData)->find("entity")->second
		// Esta clase s�lo tiene un sensor de visi�n b�sico (AI::CSightSensor), inicializado
		// con los siguientes par�metros:
		// � active = true
		// � threshold = 1.0
		// � maxDistance = 200.0
		// � alpha = PI / 4
		//
		// Cuidado: estos par�metros son datos y, por lo tanto, deber�an declararse en un archivo de configuraci�n externo.
		
		
		// Ya se leen algunos parametros de fichero, faltan mas
		std::map<std::string, void*>* perceptionInfo = (std::map<std::string, void*>*)userData;
		float* visionAngle = (float*)(perceptionInfo->find("visionAngle"))->second;
		float* visionDistance = (float*)(perceptionInfo->find("visionDistance"))->second;

		CSightSensor *sight = new CSightSensor(this, true, 1.0f, *visionDistance, Math::fromDegreesToRadians(*visionAngle));
		CHearSensor *hear = new CHearSensor(this, true, 1.0f, 200.0f);
		this->addSensor(sight);
		this->addSensor(hear);

		CPerceptionSignal *sightSignal = new CPerceptionSignal(this, PERCEPTION_SIGHT, 1.0f, 0.0f, true, true);
		this->addSignal(sightSignal);
	}

	/**
	* Clase de entidad de percepci�n que representa al jugador
	*
	* Esta clase s�lo tiene una se�al con los siguientes par�metros:
	* � type = PERCEPTION_SIGHT (es decir, que la se�al es de visibilidad)
	* � intensity = 1.0
	* � delay = 0.0 (instant�nea)
	* � isActive = true
	* � keepAlive = true (la se�al no se destruye despu�s de un ciclo de percepci�n, sino que sigue activa)
	*
	* Cuidado: estos par�metros son datos y, por lo tanto, deber�an declararse en un archivo de configuraci�n aparte.
	*
	*
	* @author Gonzalo Fl�rez
	* @date 11/04/2011
	*/
	CPerceptionEntityObject::CPerceptionEntityObject(std::string type, void* userData, 
		IPerceptionListener* listener) : CPerceptionEntity(type, userData, listener)
	{
		// Esta clase s�lo tiene una se�al con los siguientes par�metros:
		// � type = PERCEPTION_SIGHT (es decir, que la se�al es de visibilidad)
		// � intensity = 1.0
		// � delay = 0.0 (instant�nea)
		// � isActive = true
		// � keepAlive = true (la se�al no se destruye despu�s de un ciclo de percepci�n, sino que sigue activa)
		//
		// Cuidado: estos par�metros son datos y, por lo tanto, deber�an declararse en un archivo de configuraci�n aparte.
		CPerceptionSignal *sightSignal = new CPerceptionSignal(this, PERCEPTION_SIGHT, 1.0f, 0.0f, true, true);

		this->addSignal(sightSignal);
	}


	CPerceptionEntityPlayer::CPerceptionEntityPlayer(std::string type, void* userData, 
		IPerceptionListener* listener) : CPerceptionEntity(type, userData, listener)
	{
		// Esta clase s�lo tiene una se�al con los siguientes par�metros:
		// � type = PERCEPTION_SIGHT (es decir, que la se�al es de visibilidad)
		// � intensity = 1.0
		// � delay = 0.0 (instant�nea)
		// � isActive = true
		// � keepAlive = true (la se�al no se destruye despu�s de un ciclo de percepci�n, sino que sigue activa)
		//
		// Cuidado: estos par�metros son datos y, por lo tanto, deber�an declararse en un archivo de configuraci�n aparte.
		CPerceptionSignal *sightSignal = new CPerceptionSignal(this, PERCEPTION_SIGHT, 1.0f, 0.0f, true, true);
		CPerceptionSignal *walkSignal = new CPerceptionSignal(this, PERCEPTION_HEAR, 1.5f, 0.0f, false, true, "walk");
		CPerceptionSignal *runSignal = new CPerceptionSignal(this, PERCEPTION_HEAR, 2.4f, 0.0f, false, true, "run");
		CPerceptionSignal *shotSignal = new CPerceptionSignal(this, PERCEPTION_HEAR, 10.0f, 0.0f, false, true, "fire");

		this->addSignal(sightSignal);
		this->addSignal(walkSignal);
		this->addSignal(runSignal);
		this->addSignal(shotSignal);

	}
}
