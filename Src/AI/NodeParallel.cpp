#include "NodeParallel.h"

#include <iostream>


using namespace std;

namespace AI 
{

	/**
	 * Constructor. Recibe como par�metros las pol�ticas de fallo y de �xito.
	 * <p>
	 * La pol�tica de fallo indica en qu� casos tiene que fallar este nodo
	 * (si fallan TODOS sus hijos o si falla ALGUNO de sus hijos). La pol�tica
	 * de �xito, en qu� casos tiene �xito (si lo tienen TODOS sus hijos o si lo
	 * tiene ALGUNO sus hijos). En  el caso de que se solapen gana la pol�tica de fallo.
	 *
	 * @param failurePolicy Pol�tica de fallo: FAIL_ON_ALL (valor por defecto) indica que
	 * 						fallar� si fallan todos sus hijos y FAIL_ON_ONE si falla alguno.
	 * @param successPolicy Pol�tica de �xito: SUCCEED_ON_ALL (valor por defecto) indica que
	 * 						tendr� �xito si lo tienen todos sus hijos y SUCCEED_ON_ONE si lo tiene alguno.
	 */
	CNodeParallel::CNodeParallel(FAILURE_POLICY failurePolicy, SUCCESS_POLICY successPolicy)
	{
		_name = "NODE PARALLEL";
		_failPolicy = failurePolicy;
		_succeedPolicy = successPolicy;
		_childrenStatus = NULL;
	}

	/**
	 * Inicializa el nodo parallel y todos sus hijos.
	 *
	 * @param agent dato que podemos pasar al �rbol para su ejecuci�n
	 */
	void CNodeParallel::init(void* agent)
	{
		for (BehaviorTreeListIter iter = _children.begin(); iter!= _children.end(); ++iter)
			(*iter)->init(agent);

		if (_childrenStatus != NULL)
			delete _childrenStatus;
		_childrenStatus = new ChildrenStatusMap();
		for (unsigned int i = 0; i < _children.size(); ++i)
			_childrenStatus->insert( make_pair(_children.at(i),BT_RUNNING));
	}

	/**
	 * Destructor. Libera todos sus hijos.
	 */
	CNodeParallel::~CNodeParallel(void)
	{ 
		if (_childrenStatus != NULL)
			delete _childrenStatus;
	}

	/**
	 * Ejecuta cada uno de los hijos del nodo. Aunque todos se ejecutan
	 * en el mismo tick, dentro del tick se ejecutan seg�n el orden en que
	 * se a�adieron.
	 *
	 * @param agent dato que podemos pasar al �rbol para su ejecuci�n
	 * @return estado en que queda el nodo despu�s de la ejecuci�n
	 */
	BEHAVIOR_STATUS CNodeParallel::execute(void* agent)
	{
		if (_childrenStatus == NULL)
			init(agent);
		// go through all children and update the childrenStatus
		for (unsigned int i = 0 ; i < _children.size(); ++i)
		{
			BehaviorTreeNode* node = _children[i];
			if ((*_childrenStatus)[node] == BT_RUNNING)
			{
				BEHAVIOR_STATUS status = node->execute(agent);
				if (status == BT_FAILURE)
				{
					if (_failPolicy == FAIL_ON_ONE)
					{
						init(agent);
						return BT_FAILURE;
					}
					else
					{
						(*_childrenStatus)[node] = BT_FAILURE;
					}
				}
				if (status == BT_SUCCESS)
					(*_childrenStatus)[node] = BT_SUCCESS;
			}
			if ((*_childrenStatus)[node] == BT_FAILURE && _failPolicy == FAIL_ON_ALL) //theoretically the failPolicy check is not needed
			{
				BEHAVIOR_STATUS status = node->execute(agent);
				(*_childrenStatus)[node] = status;
			}
		}

		//look through the childrenStatus and see if we have met any of our end conditions
		ChildrenStatusMap::iterator iter;
		bool sawSuccess = false;
		bool sawAllFails = true;
		bool sawAllSuccess = true;
		for (iter = _childrenStatus->begin(); iter != _childrenStatus->end() ; ++iter)
		{
			switch((*iter).second)
			{
			case BT_SUCCESS:
				//can't instantly return success for succeedOnOne policy if failOnOne is also true, because failOnOne overrides succeedOnOne
				if (_succeedPolicy == SUCCEED_ON_ONE && _failPolicy != FAIL_ON_ONE)
				{
					init(agent);
					return BT_SUCCESS;
				}
				sawSuccess = true;
				sawAllFails = false;
				break;
			case BT_FAILURE:
				if (_failPolicy == FAIL_ON_ONE)
				{
					init(agent);
					return BT_FAILURE;
				}
				sawAllSuccess = false;
				break;
			case BT_RUNNING:
				sawAllFails = false;
				sawAllSuccess = false;
				//optimization for early exit
				if (_failPolicy == FAIL_ON_ALL && _succeedPolicy == SUCCEED_ON_ALL)
				{
					return BT_RUNNING;
				}
				break;
			}
		}
		if (_failPolicy == FAIL_ON_ALL && sawAllFails)
		{
			init(agent);
			return BT_FAILURE;
		}
		else if (_succeedPolicy == SUCCEED_ON_ALL && sawAllSuccess || _succeedPolicy == SUCCEED_ON_ONE && sawSuccess)
		{
			init(agent);
			return BT_SUCCESS;
		}
		else
		{
			return BT_RUNNING;
		}
	}

} //namespace AI 
