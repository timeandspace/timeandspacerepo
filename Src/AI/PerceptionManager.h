/**
* @file PerceptionManager.h
*
* Gestor de la percepci�n. En este fichero se declara la clase
* encargada de la gesti�n de la percepci�n y algunas clases relacionadas.
*
*
* @author Gonzalo Fl�rez
* @date 11/04/2011
*
* Modifications:
* @author Alejandro P�rez Alonso
* @date Abril, 2014
*/

#pragma once

#ifndef __AI_PerceptionManager_H
#define __AI_PerceptionManager_H

#include <list>

#include "BaseSubsystems/Math.h"

namespace AI 
{

	using namespace std;

	class CPerceptionEntity;
	class CSensor;
	class CPerceptionSignal;

	/**
	* Tipos de percepci�n
	*/
	enum EnmPerceptionType 
	{
		/**
		* Tipo desconocido
		*/
		PERCEPTION_UNKNOWN,
		/**
		* Vista
		*/
		PERCEPTION_SIGHT,
		/**
		* O�do
		*/
		PERCEPTION_HEAR
	}; // enum EnmPerceptionType 

	/**
	* Clase que representa una notificaci�n que hace el gestor de
	* percepci�n a una entidad de percepci�n cuyo sensor ha percibido
	* una se�al.
	*
	* @author Gonzalo Fl�rez
	* @date 11/04/2011
	*/
	class CNotification
	{

	public:
		CNotification(bool perceived, unsigned long time, CSensor* sensor, CPerceptionEntity* pEntity,
			Vector3 position = Vector3::ZERO) : _time(time), _sensor(sensor), 
			_perceived(perceived), _pEntity(pEntity), _position(position) {}

		unsigned long getTime() { return _time; }
		CSensor* getSensor() { return _sensor; }
		CPerceptionEntity* getPerceptionEntity() { return _pEntity; }
		Vector3 getPosition() { return _position; }
		bool isPerceived() { return _perceived; }

	private:
		unsigned long _time;
		CSensor *_sensor;
		Vector3 _position;
		CPerceptionEntity* _pEntity;
		bool _perceived;

	}; // class CNotification

	/**
	* Clase auxiliar que compara dos notificaciones usando su tiempo de entrega.
	* Se utiliza para mantener ordenada la lista de notificaciones pendientes
	*
	* @author Gonzalo Fl�rez
	* @date 11/04/2011
	*/
	class CNotificationComparator 
	{
	public:
		bool operator() (CNotification* a, CNotification* b)
		{
			return ((a->getTime()) > (b->getTime()));
		};

	}; // class CNotificationComparator 


	/**
	* Clase gestora de la percepci�n. En esta clase se registran
	* las entidades interesadas en percibir/ser percibidas. La clase
	* se encarga de ordenar las comprobaciones necesarias cada tick
	* para que se lleve a cabo la percepci�n y de enviar las
	* notificaciones a los sensores correspondientes cuando han
	* percibido algo.
	*
	* @author Gonzalo Fl�rez
	* @date 11/04/2011
	*/
	class CPerceptionManager
	{

	public:
		/**
		* Constructor por defecto
		*/
		CPerceptionManager(void) : _time(0) {};
		/**
		* Destructor por defecto
		*/
		~CPerceptionManager(void) {};

		/**
		* El m�todo update se encarga de actualizar la informaci�n
		* sobre los sensores de cada entidad de percepci�n.
		*
		*
		* @param enclosing_method_arguments
		* @return
		*/
		void update(unsigned int msecs);
		/**
		* Registra una entidad de percepci�n en el gestor. Una vez que
		* la entidad est� registrada, se pueden realizar comprobaciones
		* sobre sus sensores y se�ales y puede recibir notificaciones.
		*
		* @param entity Entidad de percepci�n que se va a registrar.
		*/
		void registerEntity(CPerceptionEntity* entity) { _entities.push_back(entity); };
		/**
		* Elimina una entidad de percepci�n de la lista de entidades gestionadas.
		* Esto evita que se realicen las comprobaciones de percepci�n sobre sus
		* sensores y se�ales y que reciba notificaciones.
		*
		* @param entity Entidad de percepci�n que se va a desregistrar
		*/
		void unregisterEntity(CPerceptionEntity* entity);

		/**
		M�todo que comprueba si un punto de cobertura es bueno o no.
		*/
		bool CPerceptionManager::checkCoverPoint(Vector3 coverPosition, 
			std::string typeToCover, float maxDistance);

	private:
		/**
		* Lista de entidades de percepci�n gestionadas
		*/
		std::list<CPerceptionEntity*> _entities;
		/**
		* Lista de notificaciones de percepci�n pendientes de enviar
		*/
		std::priority_queue<CNotification*, vector<CNotification*>, CNotificationComparator> _notifications;

		/**
		* Comprueba los sensores de las entidades registradas
		*
		* @param time Instante de tiempo en que se realiza la comprobaci�n
		*/
		void checkPerception(unsigned int time);
		/**
		* Recorre la lista de notificaciones pendientes y env�a las
		* que hayan caducado
		*
		* @param time Instante de tiempo en que se realiza la comprobaci�n
		*/
		void notifyEntities(unsigned int time);

		/**
		Lista de pares Sensor-Signal que ha sido percibidos.
		*/
		struct PerceivedPair {
			CSensor *sensor;
			CPerceptionSignal *signal;
		};
		std::list<PerceivedPair> _perceivedList;

		unsigned int _time;
	}; // class CPerceptionManager

} // namespace AI 

#endif __AI_PerceptionManager_H
