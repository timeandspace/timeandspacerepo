#pragma once

#ifndef __AI_LACheckDistance_H
#define __AI_LACheckDistance_H

#include "LatentAction.h"

namespace Logic
{
	class CEntity;
}

namespace AI
{

	class CLACheckDistance : public CLatentAction
	{
	public:
		CLACheckDistance(const std::string& entityVariableName,  std::string minDistanceToCheckVariable, std::string maxDistanceToCheckVariable);
		~CLACheckDistance(void);

		virtual CLatentAction* clone()
		{
			return new CLACheckDistance(*this);
		}

	protected:
		std::string _entityVariableName;
		std::string _entityName;

		CEntity* _entityToCheck;
		CEntity* _thisEntity;

		std::string _minDistanceToCheckVariable;
		std::string _maxDistanceToCheckVariable;
		float _minSquareDistance;
		float _maxSquareDistance;

	
		virtual LAStatus OnStart();


		virtual void OnStop();

	
		virtual LAStatus OnRun() ;

		
		virtual LAStatus OnAbort() ;


	}; 

} // namespace AI

#endif __AI_LACheckDistance_H
