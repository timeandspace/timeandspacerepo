
#include "BehaviorExecutionContext.h"

#include "LAPursue.h"
#include "Logic\Server.h"
#include "Logic\Maps\Map.h"
#include "Movement.h"


namespace AI
{
	CLAPursue::CLAPursue(std::string targetVariableName) : CLatentAction(), _targetVariableName(targetVariableName) 
	{

	}

	CLAPursue::~CLAPursue()
	{

	}

	CLatentAction::LAStatus CLAPursue::OnStart()
	{
		std::string targetName = _context->getStringAttribute(_targetVariableName);
		Logic::CEntity* entityToPursue = Logic::CServer::getSingletonPtr()->getMap()->
			getEntityByName(targetName);

		assert(entityToPursue && "CLAPursue::OnStart: _target = NULL");

		if(entityToPursue)
		{
			CSetSteeringBehaviour *msg = new CSetSteeringBehaviour();
			msg->_otherEntityInvolved = entityToPursue;
			msg->_movementType = AI::IMovement::MOVEMENT_DYNAMIC_PURSUE;
			CEntity* entity = (CEntity*) (_context->getUserData("entity"));
			entity->emitMessage(msg);
			_status = RUNNING;
		}
		else
		{
			_status = FAIL;
		}
		return _status;
	}

	CLatentAction::LAStatus CLAPursue::OnRun()
	{
		return _status;
	}

	void CLAPursue::OnStop()
	{
		printf("");
	}

	CLatentAction::LAStatus CLAPursue::OnAbort()
	{
		std::string targetName = _context->getStringAttribute(_targetVariableName);
		Logic::CEntity* entityToPursue = Logic::CServer::getSingletonPtr()->getMap()->
			getEntityByName(targetName);;
		CSetSteeringBehaviour *msg = new CSetSteeringBehaviour();
		msg->_otherEntityInvolved = entityToPursue;
		msg->_movementType = AI::IMovement::MOVEMENT_NONE;
		CEntity* entity = (CEntity*) (_context->getUserData("entity"));
		entity->emitMessage(msg);
		return SUCCESS;
	}

} // namespace AI
