/**
@file Server.h

Servidor de IA.

@author Gonzalo Fl�rez
@date Diciembre, 2010
*/
#pragma once

#ifndef __AI_Server_H
#define __AI_Server_H

#include "WaypointGraph.h"
#include "AStarFunctionsGaleon.h"
#include "micropather.h"
#include "PerceptionManager.h"

namespace AI {

	class CBTBuilder;
	class CContextParser;
	class BehaviorTreeNode;
	class CBehaviorExecutionContext;

/**
Servidor de IA. De momento s�lo se encarga de mantener el
grafo con los waypoints y de centralizar las llamadas a A*.

La idea es que en un futuro tambi�n se ocupe de gestionar la 
percepci�n.
*/
class CServer
{
	public:

		/**
		Devuelve la �nica instancia de la clase.

		@return Puntero al servidor de IA.
		*/
		static CServer *getSingletonPtr() { return _instance; }
		
		/**
		Inicializa el servidor de IA. Esta operaci�n s�lo es necesario realizarla
		una vez durante la inicializaci�n de la aplicaci�n. 

		@return Devuelve false si no se ha podido inicializar.
		*/
		static bool Init();

		/**
		Libera el servidor de IA. Debe llamarse al finalizar la aplicaci�n.
		*/
		static void Release();

		void reset();

		/**
		Funci�n invocada en cada vuelta para actualizaci�n.

		@param secs Segundos desde que se reenderiz� el �ltimo frame.
		@return Valor booleano indicando si todo fue bien.
		*/
		bool tick(unsigned int msecs){ 
			_pManager->update(msecs);
			return true;
		};

		/**
		A�ade un nuevo nodo al grafo de navegaci�n.
		*/
		void addWaypoint(Vector3 waypoint, float coverPoint);
		/**
		Recalcula el grafo de navegaci�n a partir de los nodos que han sido
		a�adidos con addGraphNode.
		*/
		void computeNavigationGraph();
		/**
		Devuelve el grafo de navegaci�n.
		*/
		CWaypointGraph* getNavigationGraph() {return &_wpg; };
		/**
		Calcula una ruta usando A*.
		*/
		vector<Vector3> *getAStarRoute(Vector3 from, Vector3 to);

		/**
		Calcula un punto de cobertura cercano.

		@param from punto en el que est� la entidad
		@param minDistance distancia m�nima en la que busca un punto de cobertura.
		@param maxDistance distancia m�xima en la que busca punto cobertura.
		@param group grupo o grupos (group1|group2|..) de los que protegerse.

		@return puntero a una posici�n de cobertura o NULL si no hay ning�n punto.
		*/
		Vector3* getCoverPoint(Vector3 from, float minDistance, float maxDistance, unsigned int group);
		/**
		Devuelve el manager de percepci�n
		*/
		CPerceptionManager* getPerceptionManager() { return _pManager; }
		/**
		Dado un �ngulo en radianes lo lleva al intervalo [-PI, PI]
		*/
		static double correctAngle(double angle);


		BehaviorTreeNode* parseTree(const std::string &file);

		CBehaviorExecutionContext* parseContext(const std::string &file);

	private:
		/**
		Constructor de la clase.
		*/
		CServer();

		/**
		Destructor de la clase.
		*/
		virtual ~CServer();
		/**
		Instancia �nica de la clase.
		*/
		static CServer *_instance;

		CWaypointGraph _wpg;
		/**
		Clase que se encarga de calcular la mejor ruta con A*
		*/
		micropather::MicroPather* _aStar;
		/** 
		Funciones de distancia para calcular A*
		*/
		CAStarFunctionsGaleon* _f;
		/**
		Gestor de la percepci�n
		*/
		CPerceptionManager* _pManager;

		/**
		Lista de posibles puntos de cobertura
		*/
		std::vector<CNode *> _coverPoints;

		CBTBuilder* _btBuilder;

		CContextParser* _contextParser;

}; // class CServer

} // namespace AI
#endif