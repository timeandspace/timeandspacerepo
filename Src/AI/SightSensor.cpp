#include "SightSensor.h"

#include "PerceptionEntity.h"
#include "PerceptionSignal.h"

#include "BaseSubsystems/Math.h"
#include "Physics/Server.h"

#include "Graphics\Server.h"

#include "Logic/Entity/Entity.h"
#include "Logic\Entity\Components\PerceptionComponent.h"

namespace AI 
{

	CSightSensor::~CSightSensor(void)
	{
	}

	/**
	* Realiza todas las comprobaciones necesarias para averiguar si la se�al recibida
	* se encuentra dentro del cono de visi�n.
	*
	* Las comprobaciones se realizan de m�s "barata" a m�s "cara"
	* 1. Comprueba si el sensor y la se�al est�n activos
	* 2. Comprueba si el tipo de la se�al se corresponde con el tipo que percibe el sensor.
	* 3. Comprueba si la intensidad de la se�al est� por encima del threshold del sensor.
	* 4. Comprueba la distancia entre la se�al y el sensor. Se realiza en 2 pasos:
	* 4.1. Primero se comprueba si la distancia en cada dimensi�n es mayor que la distancia m�xima.
	* 4.2. A continuaci�n se comprueba si la distancia eucl�dea es mayor que la distancia m�xima.
	* 5. Comprobamos si la se�al se encuentra dentro del �ngulo de amplitud del cono de visi�n.
	* 6. Comprobamos si no existe ning�n objeto f�sico entre el sensor y la se�al. Para eso usamos un rayo f�sico.
	*
	* En el momento en que alguna de estas comprobaciones falle se detiene el proceso y se devuelve
	* false
	*
	* @param perceptible Se�al cuya percepci�n queremos comprobar
	* @param time Instante en el que se realiza la comprobaci�n de percepci�n
	* @return false si no se ha percibido la se�al. True en caso contrario.
	*/
	bool CSightSensor::perceives(CPerceptionSignal * signal, unsigned long time) 
	{
		// Realizamos las comprobaciones
		// Si alguna de ellas falla tendremos que devolver NULL para indicar que no ha habido percepci�n
		// Si todas tienen �xito devolveremos una instancia de CNotification
		if (signal == NULL) return false;

		// Comprobaciones: 
		// 1. Comprueba si el sensor y la se�al est�n activos 
		// (m�todo isActive de la se�al - se recibe como par�metro- y del sensor)
		if (!signal->isActive() || !this->isActive()) return false;

		// 2. Comprueba si el tipo de la se�al se corresponde con el tipo que percibe el sensor
		// (m�todo getType)
		if (signal->getType() != this->getType()) return false;

		// 3. Comprueba si la intensidad de la se�al est� por encima del threshold del sensor.
		// La intensidad que percibimos, F, es el resultado de usar una funci�n de atenuaci�n:
		// f(F0, dist) = F	donde F0 es la intensidad inicial y dist la distancia.
		// En nuestro caso vamos a simplificar y usaremos una funci�n constante:
		// f(F0, dist) = F0
		// Es decir, que s�lo tendremos que comprobar que el threshold sea mayor que la intensidad
		if (this->getThreshold() > signal->getIntensity()) return false;

		// 4. Comprueba la distancia entre la se�al y el sensor. Se realiza en 2 pasos:
		// 4.1. Primero se comprueba si la distancia en cada dimensi�n es mayor que la distancia m�xima.
		// Sacamos la matriz de transformaci�n de la entidad de percepci�n (getTransform) 
		// y con ella podemos obtener la posici�n del sensor (getTrans)
		// Hacemos lo mismo con la se�al. La diferencia entre estos vectores es el vector de distancia
		// Comparamos cada una de sus dimensiones con la distancia m�xima
		Matrix4 transform = this->getPerceptionEntity()->getTransform();

		float height = this->getPerceptionEntity()->getPerceptionComponent()->getSightHeight();
		float signalHeight = signal->getPerceptionEntity()->getPerceptionComponent()->getSightHeight();

		Vector3 position = transform.getTrans();
		position.y += height;
		Vector3 signalPosition = signal->getPerceptionEntity()->getTransform().getTrans();
		signalPosition.y += signalHeight;
		Vector3 dist = signalPosition - position;
		if (abs(dist.x) > _maxDistance[0] || abs(dist.z) > _maxDistance[0]) 
			return false;

		// 4.2. A continuaci�n se comprueba si la distancia eucl�dea es mayor que la distancia m�xima.
		// Usamos el vector distancia que hemos calculado antes
		float distMagnitude = dist.length();
		if (distMagnitude > _maxDistance[0]) return false;

		// 5. Comprobamos si la se�al se encuentra dentro del �ngulo de amplitud del cono de visi�n.
		// Tenemos que calcular el �ngulo que se forma entre la direcci�n hacia la que apunta la entidad (su orientaci�n) y
		// la direcci�n en la que se encuentra el sensor (con respecto a la entidad)
		// Sacamos la orientaci�n de la entidad (getYaw nos da el �ngulo de la entidad con respecto al eje Z)
		// Sacamos la orientaci�n de la se�al = orientaci�n del vector distancia
		// Y sacamos la diferencia 
		// Nos aseguramos de que el �ngulo es menor que PI (nos tenemos que quedar con la parte peque�a de la diferencia)
		// Sacamos la orientaci�n de la entidad 
		// (yaw nos da el �ngulo de la entidad con respecto al eje Z)
		float yaw = Math::getYaw(transform);
		Ogre::Radian angle = Ogre::Math::ATan2(-dist.x, -dist.z);
		float signalAngle = angle.valueRadians();
		float finalAngle = abs(yaw - signalAngle);
		if (finalAngle > Ogre::Math::PI) finalAngle = Ogre::Math::TWO_PI - finalAngle;
		// _alpha es el �ngulo m�ximo del cono
		if (finalAngle > _alpha[0]) return false;

		// 6. Comprobamos si no existe ning�n objeto f�sico entre el sensor y la se�al. Para eso usamos un rayo f�sico.
		// Necesitamos
		// � la posici�n de origen del rayo = posici�n del sensor
		// � la direcci�n (normalizada) = vector distance normalizado
		// � la distancia m�xima = magnitud del vector distance
		// Con estos par�metros creamos una instancia de Ogre::Ray
		// Y lo usamos con raycastClosest. 
		// Si el rayo colisiona con alg�n objeto f�sico es que hay alg�n tipo de pared entre el sensor 
		// y la se�al, por lo que la entidad que ha emitido la se�al no se puede percibir.
		// Si hay alg�n obst�culo ==> raycastClosest nos devuelve la referencia ==> return NULL
		//Vector3 a = dist/distMagnitude;
		Ray ray = Ray(position, dist / distMagnitude);



		Logic::CEntity* signaler = (Logic::CEntity*)signal->getPerceptionEntity()->getUserData();
		
		/*if(Graphics::CServer::getSingletonPtr()->rayCastSingle(ray,signaler->getName()))
			return true;
		else
			return false;*/

		if (Physics::CServer::getSingletonPtr()->
			raycastSingle(ray, distMagnitude, Physics::FilterGroup::WORLD)) 
			return false;

		// Si todos los chequeos han tenido �xito tenemos que devolver una nueva instancia de CNotification
		// Los par�metros que necesita son:
		// � El instante en el que hay que entregarla (ahora mismo ==> time)
		// � El sensor que lo ha percibido (this)
		// � La entidad de percepci�n que ha generado la se�al
		// Devolvemos la notificaci�n para que se env�e a la entidad

		return true;
	}

} // namespace AI 
