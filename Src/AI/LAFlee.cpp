
#include "BehaviorExecutionContext.h"

#include "LAFlee.h"
#include "Logic\Server.h"
#include "Logic\Maps\Map.h"
#include "Movement.h"


namespace AI
{
	CLAFlee::CLAFlee(std::string targetVariableName) : CLatentAction(), _targetVariableName(targetVariableName) 
	{

	}

	CLAFlee::~CLAFlee()
	{
	
	}

	CLatentAction::LAStatus CLAFlee::OnStart()
	{
		std::string targetName = _context->getStringAttribute(_targetVariableName);
		Logic::CEntity* entityToPursue = Logic::CServer::getSingletonPtr()->getMap()->
			getEntityByName(targetName);;

		assert(entityToPursue && "CLAFlee::OnStart: _target = NULL");
		
		if(entityToPursue)
		{
			CPursueMessage *msg = new CPursueMessage();
			msg->_entityToPursue = entityToPursue;
			msg->_movementType = AI::IMovement::MOVEMENT_DYNAMIC_EVADE;
			CEntity* entity = (CEntity*) (_context->getUserData("entity"));
			entity->emitMessage(msg);
			_status = SUCCESS;
		}
		else
		{
			_status = FAIL;
		}
		return _status;
	}

	CLatentAction::LAStatus CLAFlee::OnRun()
	{
		return _status;
	}


} // namespace AI
