
#include "BehaviorExecutionContext.h"

#include "LAFace.h"
#include "Logic\Server.h"
#include "Logic\Maps\Map.h"
#include "Movement.h"


namespace AI
{
	CLAFace::CLAFace(std::string targetVariableName, bool face) : CLatentAction(), _targetVariableName(targetVariableName),
		_face (face), _aligned(false), _targetName("")
	{

	}

	CLAFace::~CLAFace()
	{

	}

	CLatentAction::LAStatus CLAFace::OnStart()
	{
		_aligned = false;
		_targetName = "";
		if (_targetVariableName == "isEntityPerceived")
		{
			if (!_context->hasAttribute("isEntityPerceived"))
			{
				CSetSteeringBehaviour *msg = new CSetSteeringBehaviour();
				msg->_otherEntityInvolved = NULL;
				msg->_movementType = AI::IMovement::MOVEMENT_KINEMATIC_FACE;
				msg->_startStop = false;
				CEntity* entity = (CEntity*) (_context->getUserData("entity"));
				entity->emitMessage(msg);
				_status = FAIL;
				return _status;
			}
			else
			{
				std::list<std::string> *entitiesPercivedList = 
					(std::list<std::string> *)_context->getUserData("isEntityPerceived");
				_targetName = entitiesPercivedList->front();

				//HACK::
				Logic::CEntity* entityToFace = Logic::CServer::getSingletonPtr()->getMap()->
					getEntityByName(_targetName);
				while (entityToFace == NULL && entitiesPercivedList->size() >= 1)
				{
					entitiesPercivedList->pop_front();
					_targetName = entitiesPercivedList->front();
					entityToFace = Logic::CServer::getSingletonPtr()->getMap()->
						getEntityByName(_targetName);
				}
				if (entitiesPercivedList->size() == 0)
				{
					CSetSteeringBehaviour *msg = new CSetSteeringBehaviour();
					msg->_otherEntityInvolved = NULL;
					msg->_movementType = AI::IMovement::MOVEMENT_KINEMATIC_FACE;
					msg->_startStop = false;
					CEntity* entity = (CEntity*) (_context->getUserData("entity"));
					entity->emitMessage(msg);
					_status = FAIL;
					return _status;
				}
			}
		}
		else
			_targetName = _context->getStringAttribute(_targetVariableName);

		Logic::CEntity* entityToFace = Logic::CServer::getSingletonPtr()->getMap()->
			getEntityByName(_targetName);

		//assert(entityToFace && "CLAFace::OnStart: _target = NULL");

		if(entityToFace)
		{
			CSetSteeringBehaviour *msg = new CSetSteeringBehaviour();
			msg->_otherEntityInvolved = entityToFace;
			msg->_movementType = AI::IMovement::MOVEMENT_KINEMATIC_FACE;
			msg->_startStop = _face;
			CEntity* entity = (CEntity*) (_context->getUserData("entity"));
			entity->emitMessage(msg);

			if (! _face) _status = SUCCESS; // Si queremos parar el Face
			else
				_status = RUNNING;
		}
		else
		{
			_status = FAIL;
		}
		return _status;
	}

	CLatentAction::LAStatus CLAFace::OnRun()
	{

		Logic::CEntity* entityToFace = Logic::CServer::getSingletonPtr()->getMap()->
			getEntityByName(_targetName);;

		if (!entityToFace)
		{
			CSetSteeringBehaviour *msg = new CSetSteeringBehaviour();
			msg->_otherEntityInvolved = entityToFace;
			msg->_movementType = AI::IMovement::MOVEMENT_KINEMATIC_FACE;
			msg->_startStop = false;
			CEntity* entity = (CEntity*) (_context->getUserData("entity"));
			entity->emitMessage(msg);
		}

		if (_aligned && entityToFace)
			_status = SUCCESS;
		else if (entityToFace)
			_status = RUNNING;
		else
			_status = FAIL;

		return _status;
	}

	bool CLAFace::accept(Logic::CMessage *message)
	{
		return message->getType() == Message::SET_STEERING_BEHAVIOUR;
	}

	//---------------------------------------------------------

	void CLAFace::process(Logic::CMessage *message)
	{
		if (message->getType() == Message::SET_STEERING_BEHAVIOUR &&
			((CSetSteeringBehaviour *)message)->_movementType == AI::IMovement::FACE_ALIGNED)
		{
			_aligned = true;
		}
	}


} // namespace AI
