//#include "BehaviorTree.h"
#include "NodeUntilFail.h"

#include <assert.h>
using namespace AI;
using namespace std;

/**
* Ejecuta la siguiente repetici�n del hijo.
* <p>
* Cada vez que el hijo tiene �xito, antes de la siguiente ejecuci�n, este se inicializa de nuevo.
*
* @param agent dato que podemos pasar al �rbol para su ejecuci�n
* @return estado en que queda el nodo despu�s de la ejecuci�n
*/
BEHAVIOR_STATUS CNodeUntilFail::execute(void* agent)
{
	if (_children.size() == 0) return BT_SUCCESS;

	BEHAVIOR_STATUS status = _children.at(0)->execute(agent);

	if (status != BT_FAILURE)
	{
		initChildren(agent);
		return BT_RUNNING;
	}

	return status;
}

/**
* Inicializa el nodo y su hijo.
* @param agent dato que podemos pasar al �rbol para su ejecuci�n
*/
void CNodeUntilFail::init( void* agent )
{
	initChildren(agent);
}

void CNodeUntilFail::initChildren(void * agent)
{
	if (_children.size() == 1)
		_children.at(0)->init(agent);
}

/**
* A�ade un hijo al nodo. Al ser un decorador s�lo puede tener 1 hijo. Si se a�ade
* un segundo hijo, saltar� un assert.
*
* @param BehaviorTreeNode* Nodo hijo
* @return Referencia al nodo repetidor
*/
BehaviorTreeInternalNode* CNodeUntilFail::addChild( BehaviorTreeNode* newChild )
{
	assert(_children.size() == 0 && "Error! un nodo until fail debe tener s�lo un hijo");
	BehaviorTreeInternalNode::addChild(newChild);
	return this;
}

/**
* Constructor. Recibe un par�metro que indica el n�mero de repeticiones de su hijo.
*
* @param repeats N�mero de repeticiones. Si este par�metro es -1 el nodo seguir� repiti�ndose hasta que falle.
*/
CNodeUntilFail::CNodeUntilFail()
{
	
};
