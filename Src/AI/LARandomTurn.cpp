#include "LARandomTurn.h"

#include "BehaviorExecutionContext.h"

namespace AI 
{
	
	CLARandomTurn::~CLARandomTurn(void)
	{
	}

	CLatentAction::LAStatus CLARandomTurn::OnStart()
	{
		if (_context->hasAttribute("entity")){
			CEntity* entity = (CEntity*) _context->getUserData("entity");
			float yaw = (float) rand() / RAND_MAX * Ogre::Math::TWO_PI;
			entity->setYaw(yaw);
			return SUCCESS;
		}
		return FAIL;
	}

} // namespace AI 