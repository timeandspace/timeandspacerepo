#include "NodeSelector.h"

using namespace AI;
using namespace std;

/**
* Constructor
*/
CNodeSelector::CNodeSelector()
{
	currentPosition = -1;
}

/**
* Inicializa el nodo y todos sus hijos.
*
* @param agent dato que podemos pasar al �rbol para su ejecuci�n
*/
void CNodeSelector::init(void* agent)
{
	currentPosition = -1;
	for (BehaviorTreeListIter iter = _children.begin(); iter!= _children.end(); ++iter)
		(*iter)->init(agent);
}


/**
* Ejecuta los siguientes hijos del nodo hasta que alguno de ellos
* tenga �xito o fallen todos. Si alguno queda en estado BT_RUNNING,
* el nodo detiene su ejecuci�n y en el siguiente tick contin�a ejecut�ndolo:<bR>
* � Si el hijo actual devuelve BT_SUCCESS, el nodo termina con �xito (BT_SUCCESS).<br>
* � Si el hijo actual devuelve BT_RUNNING, el nodo devuelve BT_RUNNING.
* En el siguiente tick se continuar� ejecutando el mismo hijo.<br>
* � Si el hijo actual devuelve BT_FAILURE, se ejecuta el siguiente hijo. Si no hay m�s hijos,
* el nodo termina con fallo.
*
*
* @param agent dato que podemos pasar al �rbol para su ejecuci�n
* @return estado en que queda el nodo despu�s de la ejecuci�n
*/
BEHAVIOR_STATUS CNodeSelector::execute(void* agent)
{
	if (currentPosition != -1) //there's one still running
	{
		BEHAVIOR_STATUS status = (_children.at(currentPosition))->execute(agent);
		if (status == BT_RUNNING)
			return BT_RUNNING;
		else if (status == BT_SUCCESS)
		{
			currentPosition = -1;
			return BT_SUCCESS;
		}
		else if (status == BT_FAILURE)
		{
			currentPosition++;
			if (currentPosition == _children.size())
			{
				currentPosition = -1;
				return BT_FAILURE;
			}
		}
	}
	else
	{
		init(agent);
		currentPosition = 0;
	}
	if (_children.size() == 0)
		return BT_SUCCESS;

	BehaviorTreeNode* currentlyRunningNode = _children.at(currentPosition);
	BEHAVIOR_STATUS status;
	while ((status = (*currentlyRunningNode).execute(agent)) == BT_FAILURE) //keep trying children until one doesn't fail
	{
		currentPosition++;
		if (currentPosition == _children.size()) //all of the children failed
		{
			currentPosition = -1;
			return BT_FAILURE;
		}
		currentlyRunningNode = _children.at(currentPosition);
	}
	return status;

}

