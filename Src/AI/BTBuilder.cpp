
#include "BTBuilder.h"
#include <rapidxml_utils.hpp>

#include "AI\BehaviorTreeBase.h"

#include "AI\NodeLA.h"
#include "AI\NodeParallel.h"
#include "AI\NodeSelector.h"
#include "AI\NodeSequential.h"
#include "AI\NodeRepeat.h"
#include "AI\NodePriorityCondition.h"
#include "AI\NodeUntilFail.h"

#include "AI\LAMoveToPoint.h"
#include "AI\SimpleLatentActions.h"
#include "AI\LARoutes.h"
#include "AI\LARouteToPoint.h"
#include "LAPursue.h"
#include "LAEvade.h"
#include "LAFace.h"
#include "LACheckDistance.h"
#include "LAShoot.h"

#include "AI\LAEntityPerceived.h"
#include "AI\LARandomTurn.h"

#include "BaseSubsystems\System.h"

#include <tchar.h>
#include <stdio.h>

using namespace AI;
using namespace std;
using namespace rapidxml;


namespace AI
{
	CBTBuilder::CBTBuilder() : _rootNode(NULL)
	{
	}
 
 
	CBTBuilder::~CBTBuilder()
	{
		TreeMapIter itr = _btLibrary.begin();
		TreeMapIter end = _btLibrary.end();

		for(;itr!=end;++itr)
			delete(*itr).second;
	}


	void CBTBuilder::parseLibrary(std::string directoryPath)
	{
		vector<std::string> fileList;
		BaseSubsystems::System::getFilesInDirectory(directoryPath,".xml",fileList);

		for(unsigned int i=0;i<fileList.size();++i)
		{
			std::string file = directoryPath + "/" + fileList[i];
			BehaviorTreeNode* parsedTree = parseBT(file);
			addBtToLibrary(_parsedTreeName,parsedTree);
		}
	}
 
	BehaviorTreeNode* CBTBuilder::parseBT(const std::string &fileName)
	{
		_rootNode = NULL;
		_parsedTreeName = "";

		rapidxml::file<> xmlFile(fileName.c_str());
	
		rapidxml::xml_document<> doc;
		doc.parse<0>(xmlFile.data());
		rapidxml::xml_node<>* XMLRoot;
 
		XMLRoot = doc.first_node();
 
		std::string a = XMLRoot->name();

		int count = 0;
		for (xml_node<>* node = XMLRoot->first_node() ; node; node = node->next_sibling())
		{
			a = node->name();
			count++;	
		}

		BehaviorTreeNode* firstNode = NULL;
		if(count == 1)
			parseNode(XMLRoot->first_node(), firstNode);
		else
		{
			std::printf("Arbol mal formado");
		}

		if(_rootNode)
			_parsedTreeName = XMLRoot->first_attribute("Name")->value();

		return _rootNode;
	}

	void CBTBuilder::parseNode(rapidxml::xml_node<>* node, BehaviorTreeNode* &btParentNode)
	{
		std::string nodeType;

		for (xml_attribute<> *attr = node->first_attribute(); attr; attr = attr->next_attribute())
		{
			nodeType = attr->name();
		}

		BehaviorTreeNode* actualBTNode = NULL;

		nodeType = node->first_attribute("Type")->value();

		if(nodeType == "Composite")
		{
			parseCompositeNode(node,  actualBTNode);
		}
		else if(nodeType == "Action")
		{
			parseActionNode(node,  actualBTNode);
		}
		else if(nodeType == "Condition")
		{
			CLatentAction* condition = NULL;
			parseConditionNode(node, condition);
			actualBTNode = new CNodeLA(condition);	
		}
		else if(nodeType == "Decorator")
		{
			parseDecoratorNode(node, actualBTNode);
		}
		else if(nodeType == "BehaviourTree")
		{
			addSubTree(node, actualBTNode);
		}

		if(btParentNode == NULL)
			_rootNode = actualBTNode;
		else 
			((BehaviorTreeInternalNode*)btParentNode)->addChild(actualBTNode);

		std::string prueba = node->name();
		prueba = node->first_attribute("Name")->value();
		if(prueba != "PriorityCondition")
			for (xml_node<>* childNode = node->first_node() ; childNode; childNode = childNode->next_sibling())
			{
				std::string prueba2 = childNode->name();
				nodeType = childNode->first_attribute("Type")->value();
				std::string a = childNode->first_attribute("Name")->value();
				parseNode(childNode,actualBTNode);
			}


	}

	void CBTBuilder::parseCompositeNode(rapidxml::xml_node<>* node, BehaviorTreeNode* &btNode)
	{
		std::string compositeNodeType;
		compositeNodeType = node->first_attribute("Name")->value();

		if(compositeNodeType == "Sequence")
		{
			btNode = new CNodeSequential();
		}
		else if(compositeNodeType == "Selector")
		{
			btNode = new CNodeSelector();
		}
		else if(compositeNodeType == "PriorityCondition")
		{
			parsePriorityConditionNode(node, btNode);
		}
		else if(compositeNodeType == "Parallel")
		{
			std::string failurePolicy = node->first_attribute("FailurePolicy")->value();
			std::string successPolicy = node->first_attribute("SuccessPolicy")->value();

			FAILURE_POLICY failure;
			if(failurePolicy == "FAIL_ON_ONE") failure =AI::FAIL_ON_ONE;
			else if(failurePolicy =="FAIL_ON_ALL") failure =AI::FAIL_ON_ALL;

			SUCCESS_POLICY succes;
			if(successPolicy == "SUCCED_ON_ONE") succes = AI::SUCCEED_ON_ONE;
			else if(successPolicy =="SUCCED_ON_ALL") succes = AI::SUCCEED_ON_ALL;

			btNode = new CNodeParallel(failure,succes);
		}
	}

	void CBTBuilder::parseActionNode(rapidxml::xml_node<>* node, BehaviorTreeNode* &btNode)
	{
		std::string actionNodeType;
		actionNodeType = node->first_attribute("Name")->value();

		std::string dataName;
		if(node->first_attribute("Data"))
			dataName = node->first_attribute("Data")->value();

		if(actionNodeType == "LAMoveToPoint")
		{
			btNode = new CNodeLA(new CLAMoveToPoint(dataName));
		}
		else if(actionNodeType == "LAWait")
		{
			btNode = new CNodeLA(new CLAWait(dataName));
		}
		else if(actionNodeType == "LARandomTurn")
		{
			btNode = new CNodeLA(new CLARandomTurn());
		}
		else if(actionNodeType == "LARouteToRandom")
		{
			btNode = new CNodeLA(new CLARouteToRandom());
		}
		else if(actionNodeType == "LARouteToPoint")
		{
			btNode = new CNodeLA(new CLARouteToPoint(dataName));
		}
		else if(actionNodeType == "LACover")
		{
			btNode = new CNodeLA(new CLACover());
		}
		else if(actionNodeType == "LAPursue")
		{
			btNode = new CNodeLA(new CLAPursue(dataName));
		}
		else if(actionNodeType == "LAEvade")
		{
			btNode = new CNodeLA(new CLAEvade(dataName));
		}
		else if(actionNodeType == "AlwaysFailure")
		{
			btNode = new CNodeAlwaysFailure();
		}
		else if(actionNodeType == "AlwaysSuccess")
		{
			btNode = new CNodeAlwaysSuccess();
		}
		else if(actionNodeType == "AlwaysRunning")
		{
			btNode = new CNodeAlwaysRunning();
		}
		else if(actionNodeType == "LAGetObject")
		{
			btNode = new CNodeLA(new CLAGetObject());
		}
		else if(actionNodeType == "LAExistsEntity")
		{
			btNode = new CNodeLA(new CLAExistsEntity(dataName));
		}
		else if(actionNodeType == "LAFace")
		{
			std::string entityToFaceVariable;
			std::string boolValue;
			bool face;
			
			entityToFaceVariable = node->first_attribute("Data1")->value();
			boolValue = node->first_attribute("Data2")->value();

			if(boolValue == "true")
				face = true;
			else
				face = false;

			btNode = new CNodeLA(new CLAFace(entityToFaceVariable,face));
		}
		else if (actionNodeType == "LAShoot")
		{
			std::string precision = node->first_attribute("Data1")->value();
			std::string targetName = node->first_attribute("Data2")->value();
			btNode = new CNodeLA(new CLAShoot((float)atof(precision.c_str()), targetName));
		}
		else if (actionNodeType == "LATeletransport")
		{
			btNode = new CNodeLA(new CLATeletransport(dataName));
		}
		
	}

	void CBTBuilder::parseDecoratorNode(rapidxml::xml_node<>* node, BehaviorTreeNode* &btNode)
	{
		std::string decoratorNodeType;
		decoratorNodeType = node->first_attribute("Name")->value();

		if(decoratorNodeType == "UntilFail")
		{
			btNode = new CNodeUntilFail();
		}
		else if(decoratorNodeType == "Limit")
		{
			std::string cycles = node->first_attribute("Cycles")->value();
			btNode = new CNodeRepeat(std::stoi(cycles));
		}
		else if(decoratorNodeType == "Inverter")
		{
			btNode = new CNodeInverter();
		}
		else if(decoratorNodeType == "Semaphore")
		{
			
		}
	}

	void CBTBuilder::parseConditionNode(rapidxml::xml_node<>* node, CLatentAction* &condition)
	{
		std::string conditionNodeType;
		conditionNodeType = node->first_attribute("Name")->value();

		if(conditionNodeType == "LAEntityPerceived")
		{
			std::string entityType = node->first_attribute("Data1")->value();
			std::string perceivedPoint = node->first_attribute("Data2")->value();
			condition = new CLAEntityPerceived(entityType,perceivedPoint);
		}
		else if(conditionNodeType == "LAInvalidateData")
		{
			std::string dataToInvalidate = node->first_attribute("Data")->value();
			condition = new CLAInvalidateData(dataToInvalidate);
		}
		else if(conditionNodeType == "LAExistsData")
		{
			std::string dataToCheck = node->first_attribute("Data")->value();
			condition = new CLAExistsData(dataToCheck);
		}
		else if(conditionNodeType == "LACheckDistance")
		{
			std::string dataToCheck = node->first_attribute("Data1")->value();
			std::string minDistanceToCheckVariable = node->first_attribute("Data2")->value();
			std::string maxDistanceToCheckVariable = node->first_attribute("Data3")->value();
			condition = new CLACheckDistance(dataToCheck,minDistanceToCheckVariable,maxDistanceToCheckVariable);
		}
	}

	void CBTBuilder::addBtToLibrary(const std::string &name,BehaviorTreeNode* behaviourTree)
	{
		if(_btLibrary.count(name))
			_btLibrary.erase(name);
		_btLibrary.insert(std::pair<std::string, BehaviorTreeNode*>(name, behaviourTree));
	}

	BehaviorTreeNode*  CBTBuilder::getBT(const std::string &btName)
	{	
		std::map<std::string,BehaviorTreeNode*>::const_iterator pos = _btLibrary.find(btName);
		if (pos == _btLibrary.end()) 
			return NULL;
		else 
			return pos->second;
	}

	void CBTBuilder::addSubTree(rapidxml::xml_node<>* node, BehaviorTreeNode* &btNode)
	{
		std::string behaviourTreeName;
		behaviourTreeName = node->first_attribute("Name")->value();

		BehaviorTreeNode* tree = getBT(behaviourTreeName);

		assert(tree && "No se ha encontrado el arbol");

		btNode = tree->clone();
	}

	void CBTBuilder::parsePriorityConditionNode(rapidxml::xml_node<>* node, BehaviorTreeNode* &btNode)
	{
		btNode = new CNodePriorityCondition();
		std::string nodeType;
		for (xml_node<>* childNode = node->first_node() ; childNode; childNode = childNode->next_sibling())
		{
			//PARSE <Child>

			CLatentAction* condition = NULL;
			BehaviorTreeNode* btChildNode = NULL;

			for (xml_node<>* secondChildNode = childNode->first_node() ; secondChildNode; secondChildNode = secondChildNode->next_sibling())
			{
				nodeType = secondChildNode->first_attribute("Type")->value();

				if(nodeType == "Condition")
				{
					parseConditionNode(secondChildNode,condition);
				}
				else
				{
					if(nodeType == "Composite")
					{
						parseCompositeNode(secondChildNode,  btChildNode);
					}
					else if(nodeType == "Action")
					{
						parseActionNode(secondChildNode,  btChildNode);
					}
					else if(nodeType == "Decorator")
					{
						parseDecoratorNode(secondChildNode, btChildNode);
					}
					else if(nodeType == "BehaviourTree")
					{
						addSubTree(secondChildNode, btChildNode);
					}

				}
			}

			if(condition != NULL)
				((CNodePriorityCondition*)btNode)->addChild(btChildNode,condition);
			else
				((CNodePriorityCondition*)btNode)->addChild(btChildNode);
		}
	}

}