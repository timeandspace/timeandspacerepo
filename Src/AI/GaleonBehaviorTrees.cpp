#include "GaleonBehaviorTrees.h"

#include "LAEntityPerceived.h"
#include "LAMoveToPoint.h"
#include "SimpleLatentActions.h"
#include "LARandomTurn.h"
#include "NodeLA.h"
#include "NodeRepeat.h"
#include "LARoutes.h"

namespace AI
{

	/**
	* Comportamiento que recorre puntos aleatorios del nivel, uno tras otro.
	* <p>
	* El comportamiento repetir� continuamente los siguientes pasos:<bR>
	* � Ir hasta un punto aleatorio del mapa.<bR>
	* � Esperar durante 5 segundos.<bR>
	*
	* @author "Gonzalo Fl�rez"
	* @date 11/04/2011
	*/
	CBTWander::CBTWander(void) : CNodeRepeat(-1)
	{
		CNodeSequential *seq = new CNodeSequential();
		this->addChild(seq);
		seq->addChild(new CNodeLA(new CLARouteToRandom()));
		seq->addChild(new CNodeLA(new CLAWait(5000)));
		setName("Wander");
	}

	/**
	* Comportamiento que persigue al jugador.
	* <p>
	* Es un comportamiento que ejecuta dos acciones al mismo tiempo:<bR>
	* � Por un lado, comprueba constantemente si se ha percibido la entidad.<bR>
	* � Al mismo tiempo, se mueve hasta la posici�n de la entidad.<bR>
	* <p>
	* Este comportamiento debe fallar si alguna de las dos acciones falla.
	* <p>
	* Para realizar el movimiento no es necesario usar el componente de rutas con A*,
	* ya que si estamos viendo al jugador significa que no hay obst�culos de por medio y
	* podemos desplazarnos en l�nea recta (en Gale�n no hay cristales ni precipicios).
	*
	* @author "Gonzalo Fl�rez"
	* @date 11/04/2011
	*/
	CBTFollow::CBTFollow() : CNodeParallel (AI::FAIL_ON_ONE, AI::SUCCEED_ON_ALL)
	{
		this
			->addChild( (new CNodeSequential() )
				->addChild(new CNodeLA(new CLAWait(500)))
				->addChild(new CNodeAlwaysFailure())
			)
			->addChild( new CNodeLA(new CLAMoveToPoint("pos")));
	}

	/**
	* Comportamiento que va hasta la �ltima posici�n conocida del jugador.
	* <p>
	* Ejecuta la siguiente secuencia de acciones:<bR>
	* � Comprueba que existe en el contexto de ejecuci�n el dato con la posici�n del
	* jugador. Este dato se habr� guardado en el comportamiento CBTFollow.<bR>
	* � Si es as�, se mueve hasta esta posici�n.<bR>
	* � Se ejecuta el comportamiento CBTTurnAround (mira en diferentes direcciones aleatoriamente).<bR>
	* � Por �ltimo se borra la posici�n del contexto (se "olvida").<bR>
	* <p>
	* Para realizar el movimiento no es necesario usar el componente de rutas con A*,
	* ya que si estamos viendo al jugador significa que no hay obst�culos de por medio y
	* podemos desplazarnos en l�nea recta (en Gale�n no hay cristales ni precipicios).
	*
	* @author "Gonzalo Fl�rez"
	* @date 11/04/2011
	*/
	CBTLastPosition::CBTLastPosition(void) : CNodeSequential()
	{
		this->addChild( new CNodeLA(new CLAMoveToPoint("pos")));
		this->addChild( new CBTTurnAround());
		this->addChild( new CNodeLA(new CLAInvalidateData("pos")));
	}

	/**
	* Comportamiento que mira en 3 direcciones aleatorias.
	* <p>
	* Repite 3 veces:<bR>
	* � Mira en una direcci�n aleatoria.<bR>
	* � Espera durante 1500 ms.<bR>
	*
	* @author "Gonzalo Fl�rez"
	* @date 11/04/2011
	*/
	CBTTurnAround::CBTTurnAround(void) : CNodeRepeat(3)
	{
		this->
			addChild( (new CNodeSequential())
				->addChild(new CNodeLA(new CLAWait(1500)) )
				->addChild(new CNodeLA(new CLARandomTurn()))
				);
	}

	/**
	* Comportamiento que combina los anteriores en uno m�s complejo.
	* <p>
	* Es un selector con prioridad. Las acciones que realiza, por orden de prioridad son:<br>
	*
	* � Intenta seguir al jugador. Si falla (en general, fallar� porque no puede percibirlo)...<bR>
	* � Va a la �ltima posici�n que recuerda del jugador. Si falla (no recuerda su posici�n)...<br>
	* � Va hasta un punto aleatorio del mapa.<br>
	*
	* @author "Gonzalo Fl�rez"
	* @date 11/04/2011
	*/
	CBTMainBehavior::CBTMainBehavior() : CNodePriorityCondition()
	{
		this->addChild( new CBTFollow, new CLAEntityPerceived("Player", "pos"));
		this->addChild( new CBTLastPosition(), new CLAExistsData("pos"));
		this->addChild( new CBTWander() );
	}


} // namespace AI
