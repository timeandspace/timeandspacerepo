/**
@file HearSensor.h

Contiene las definiciones de un sensor de sonido simple.

@author Alejandro P�rez Alonso
@date Abril, 2014
*/

#ifndef __AI_HearSensor_H
#define __AI_HearSensor_H

#include "sensor.h"

namespace AI
{

#define DISTANCE_ATTENUATION 0.05f
#define COLLISION_ATTENUATION 0.5f

	/**

	*/
	class CHearSensor : public CSensor
	{
	public:
		/**
		* Constructor. Inicializa los par�metros b�sicos
		*
		* @param pEntity Entidad a la que pertenece el sensor
		* @param active Indica si el sensor est� activo (puede percibir) o no
		* @param threshold Intensidad m�nima de la se�al para que el sensor pueda percibir
		* @param maxDistance Distancia m�xima a la que puede percibirse
		* @param alpha �ngulo del cono.
		* @return return
		*/
		CHearSensor(CPerceptionEntity* pEntity, bool active, float threshold, float maxDistance)
			: CSensor(pEntity, active, threshold), _maxDistance(maxDistance) {} ;
		/**
		* Destructor
		*/
		~CHearSensor(void);

		/**
		* Realiza todas las comprobaciones necesarias para averiguar si la se�al recibida
		* se encuentra dentro del cono de visi�n.
		*
		* Las comprobaciones se realizan de m�s "barata" a m�s "cara"
		* 1. Comprueba si el sensor y la se�al est�n activos
		* 2. Comprueba si el tipo de la se�al se corresponde con el tipo que percibe el sensor.
		* 3. Comprueba si la intensidad de la se�al est� por encima del threshold del sensor.
		* 4. Comprueba la distancia entre la se�al y el sensor. Se realiza en 2 pasos:
		* 4.1. Primero se comprueba si la distancia en cada dimensi�n es mayor que la distancia m�xima.
		* 4.2. A continuaci�n se comprueba si la distancia eucl�dea es mayor que la distancia m�xima.
		* 5. Comprobamos si la se�al se encuentra dentro del �ngulo de amplitud del cono de visi�n.
		* 6. Comprobamos si no existe ning�n objeto f�sico entre el sensor y la se�al. Para eso usamos un rayo f�sico.
		*
		* En el momento en que alguna de estas comprobaciones falle se detiene el proceso y se devuelve
		* NULL,  lo que significa que el sensor no es capaz de detectar la se�al. Por otro lado, si todas se
		* superan con �xito se devuelve una notificaci�n.
		*
		* @param perceptible Se�al cuya percepci�n queremos comprobar
		* @param time Instante en el que se realiza la comprobaci�n de percepci�n
		* @return NULL si no se ha percibido la se�al. Una instancia de CNotification en caso contrario.
		*/
		virtual bool perceives(CPerceptionSignal * perceptible, unsigned long time);
		/**
		* Devuelve el tipo de sensor. En este caso un sensor de visi�n.
		*/
		virtual EnmPerceptionType getType() { return PERCEPTION_HEAR; };

	private:
		/**
		* Distancia m�xima del cono
		*/
		float _maxDistance;

	}; // class CHearSensor

} // namespace AI

#endif //__AI_HearSensor_H