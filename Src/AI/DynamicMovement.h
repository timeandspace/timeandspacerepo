/**
@file DynamicMovement.h

Contiene distintas clases que heredan de IMovement y que 
implementan distintos tipos de movimiento din�mico.

@author Gonzalo Fl�rez
@date Diciembre, 2010

@author Alejandro P�rez Alonso
@date Abril, 2014
*/

#pragma once

#ifndef __AI_DynamicMovement_H
#define __AI_DynamicMovement_H

#include "movement.h"

namespace AI 
{

	/**
	Movimiento Seek. 
	Intenta llegar a un punto de destino empleando siempre la m�xima aceleraci�n posible. 
	En realidad lo m�s probable es que el movimiento nunca llegue al destino sino que se pase de 
	largo, de la vuelta y permanezca oscilando hasta que hagamos que se detenga.
	*/
	class CDynamicSeek : public IMovement
	{
	public:

		/**
		Constructor
		*/
		CDynamicSeek(float maxLinearSpeed, float maxAngularSpeed, float maxLinearAccel, float maxAngularAccel) : 
			IMovement(maxLinearSpeed, maxAngularSpeed, maxLinearAccel, maxAngularAccel) { };
		/**
		Efect�a el movimiento.

		@param msecs Tiempo que dura el movimiento.
		@param currentProperties Par�metro de entrada/salida donde se reciben las velocidades actuales y 
		en �l devuelve los nuevos valores de aceleraci�n.
		*/
		virtual void move(unsigned int msecs, DynamicMovement& currentProperties);

	}; // class CDynamicSeek

	//---------------------------------------------------------

	class CDynamicFlee : public IMovement
	{
	public:
		CDynamicFlee(float maxLinearSpeed, float maxAngularSpeed, float maxLinearAccel, float maxAngularAccel) : 
			IMovement(maxLinearSpeed, maxAngularSpeed, maxLinearAccel, maxAngularAccel) { };
		/**
		Efect�a el movimiento.

		@param msecs Tiempo que dura el movimiento.
		@param currentProperties Par�metro de entrada/salida donde se reciben las velocidades actuales y 
		en �l devuelve los nuevos valores de aceleraci�n.
		*/
		virtual void move(unsigned int msecs, DynamicMovement& currentProperties);
	};

	//---------------------------------------------------------

	/**
	Movimiento Arrive. 
	Intenta llegar a un punto de destino disminuyendo la velocidad seg�n se acerca. De esta 
	manera nos aseguramos de que llegue sin pasarse de largo.
	*/
	class CDynamicArrive : public IMovement
	{
	public:

		/**
		Constructor
		*/
		CDynamicArrive(float maxLinearSpeed, float maxAngularSpeed, float maxLinearAccel, float maxAngularAccel) : 
			IMovement(maxLinearSpeed, maxAngularSpeed, maxLinearAccel, maxAngularAccel) { };
		/**
		Efect�a el movimiento.

		@param msecs Tiempo que dura el movimiento.
		@param currentProperties Par�metro de entrada/salida donde se reciben las velocidades actuales y 
		en �l devuelve los nuevos valores de aceleraci�n.
		*/
		virtual void move(unsigned int msecs, DynamicMovement& currentProperties);

	}; // class CDynamicArrive

	//---------------------------------------------------------


	class CDynamicPursue : public CDynamicSeek
	{
	public:

		DynamicMovement* _entityToPursueProperties;

		Logic::CEntity* _entityToPursue;

		/**
		Constructor
		*/
		CDynamicPursue(float maxLinearSpeed, float maxAngularSpeed, float maxLinearAccel, float maxAngularAccel) : 
			CDynamicSeek(maxLinearSpeed, maxAngularSpeed, maxLinearAccel, maxAngularAccel) { };
		/**
		Efect�a el movimiento.
		
		@param msecs Tiempo que dura el movimiento.
		@param currentProperties Par�metro de entrada/salida donde se reciben las velocidades actuales y 
		en �l devuelve los nuevos valores de aceleraci�n.
		*/
		void move(unsigned int msecs, DynamicMovement& currentProperties);


		void setEntityToPursueProperties(DynamicMovement* properties) { _entityToPursueProperties = properties; }

		void setEntityToPursue(CEntity* entityToPursue) { _entityToPursue = entityToPursue; }

	};

	class CDynamicEvade : public CDynamicFlee
	{
	public:

		/**
		Constructor
		*/
		CDynamicEvade(float maxLinearSpeed, float maxAngularSpeed, float maxLinearAccel, float maxAngularAccel) : 
			CDynamicFlee(maxLinearSpeed, maxAngularSpeed, maxLinearAccel, maxAngularAccel) { };
		/**
		Efect�a el movimiento.
		
		@param msecs Tiempo que dura el movimiento.
		@param currentProperties Par�metro de entrada/salida donde se reciben las velocidades actuales y 
		en �l devuelve los nuevos valores de aceleraci�n.
		*/
		void move(unsigned int msecs, DynamicMovement& currentProperties);


		void setEntityToPursueProperties(DynamicMovement* properties) { _entityToEvadeProperties = properties; }

		void setEntityToPursue(CEntity* entityToPursue) { _entityToEvade = entityToPursue; }

	protected:
		DynamicMovement* _entityToEvadeProperties;

		Logic::CEntity* _entityToEvade;
	};
	/**
	Obstacle Avoidance
	Intenta evitar colisionar con objetos, paredes, etc.
	*/
	class CObstacleAvoidance : public CDynamicSeek
	{
	public: 

		/**
		Constructor
		*/
		CObstacleAvoidance(float maxLinearSpeed, float maxAngularSpeed, float maxLinearAccel, float maxAngularAccel) : 
			_lastDirection(-1), _angle(Math::PI / 18), CDynamicSeek(maxLinearSpeed, maxAngularSpeed, maxLinearAccel, maxAngularAccel) { };

		/**
		Efect�a el movimiento.

		@param msecs Tiempo que dura el movimiento.
		@param currentProperties Par�metro de entrada/salida donde se reciben las velocidades actuales y 
		en �l devuelve los nuevos valores de aceleraci�n.
		*/
		void move(unsigned int msecs, DynamicMovement& currentProperties, float distToTarget);

	protected:
		/**
		Indica �ltima direcci�n tomada, 0 derecha y 1 izquierda
		*/
		int _lastDirection;

		// angulo de inicio 15�, si colisiona va aumentando hasta un m�ximo de 90�
		float _angle;

		const static float MAX_ANGLE;
		const static float MIN_ANGLE;

	}; //class CObstacleAvoidance

} //namespace AI 

#endif // __AI_DynamicMovement_H
