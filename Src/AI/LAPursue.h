#pragma once

#ifndef __AI_LAPursue_H
#define __AI_LAPursue_H

#include "LatentAction.h"

namespace AI 
{
	/**
	Acci�n latente que persigue a una entidad
	*/
	class CLAPursue : public CLatentAction 
	{
	public:
		CLAPursue(std::string targetVariableName);

		~CLAPursue(); 
		
	protected:
		/**
		M�todo invocado al principio de la ejecuci�n de la acci�n,
		para que se realicen las tareas que son �nicamente necesarias
		al principio (y no durante toda la vida de la acci�n).
		<p>
		En este caso, se debe enviar un mensaje al componente 
		CRouteTo y cambiar al estado SUSPENDED.

		@return Estado de la funci�n; si se indica que la
		acci�n a terminado (LatentAction::Completed), se invocar�
		al OnStop().
		*/    
		virtual LAStatus OnStart();

		/**
		M�todo invocado c�clicamente para que se contin�e con la
		ejecuci�n de la acci�n.
		<p>
		En este caso, se va actualizando la posici�n del jugador.

		@return Estado de la acci�n tras la ejecuci�n del m�todo;
		permite indicar si la acci�n ha terminado o se ha suspendido.
		*/
		virtual LAStatus OnRun();

		virtual void OnStop();
		virtual LAStatus OnAbort();

		virtual CLatentAction* clone()
		{
			return new CLAPursue(*this);
		}

	private:

		std::string _targetVariableName;

		CLatentAction::LAStatus _status;
	};

} // namespace AI 

#endif __AI_LAPursue_H
