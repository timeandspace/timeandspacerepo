#include "NodeLA.h"

#include "LatentAction.h"

namespace AI
{

	/**
	* Destructor. Libera tambi�n la acci�n latente asociada.
	*
	*/
	CNodeLA::~CNodeLA(void)
	{
		if (_action != NULL) 
			delete _action;
	}

	/**
	* Ejecuta la acci�n latente asociada al nodo y devuelve el estado correspondiente.
	* <p>
	* Si la acci�n latente devuelve SUCCESS o FAIL, esta funci�n devuelve el valor
	* correspondiente para el nodo. En otro caso, devuelve BT_RUNNING.
	*
	* @param agent dato que podemos pasar al �rbol para su ejecuci�n
	* @return estado en que queda el nodo despu�s de la ejecuci�n
	*/
	BEHAVIOR_STATUS CNodeLA::execute(void* context)
	{
		// Lo primero que tenemos que hacer es comprobar el estado de la acci�n latente (getStatus)
		// para ver si ha terminado (estados SUCCESS o FAIL). 
		// Si ha terminado, salimos del m�todo devolviendo BT_SUCCESS o BT_FAIL
		CLatentAction::LAStatus status = _action->getStatus();
		if (status == CLatentAction::SUCCESS)
			return BT_SUCCESS;
		if (status == CLatentAction::FAIL)
			return BT_FAILURE;

		// Si no ha terminado la LA la ejecutamos
		status = _action->tick();

		// Transformamos el resultado de ejecutar la LA en un resultado de estado del �rbol (BEHAVIOR_STATUS)
		if (status == CLatentAction::SUCCESS)
			return BT_SUCCESS;
		if (status == CLatentAction::FAIL)
			return BT_FAILURE;

		return BT_RUNNING;
	}

	/**
	* Si la acci�n latente no est� lista para ser ejecutada (si el estado no
	* es READY) la resetea.
	*
	* @param agent dato que podemos pasar al �rbol para su ejecuci�n
	*/
	void CNodeLA::init(void* agent)
	{
		// Consultamos el estado de la acci�n latente (_action) con getStatus y la reseteamos si es necesario
		if (_action->getStatus() != CLatentAction::READY)
			_action->reset();
	}

	/**
	* Invoca al m�todo accept de la acci�n latente asociada para comprobar si
	* �sta acepta el mensaje.
	*
	* @param message Mensaje
	* @return true si la acci�n latente acepta el mensaje
	*/
	bool CNodeLA::accept(Logic::CMessage *message)
	{
		return _action->accept(message);
	}

	/**
	* Invoca al m�todo process de la acci�n latente asociada para que procese
	* el mensaje
	*
	* @param message Mensaje
	*/
	void CNodeLA::process(Logic::CMessage *message)
	{
		_action->process(message);
	}

	/**
	* Asigna el contexto de ejecuci�n al nodo actual y tambi�n a la acci�n latente asociada.
	*
	* @param context Contexto de ejecuci�n
	*/
	void CNodeLA::useContext(CBehaviorExecutionContext* context)
	{
		BehaviorTreeNode::useContext(context);
		_action->useContext(context);
	}

}
