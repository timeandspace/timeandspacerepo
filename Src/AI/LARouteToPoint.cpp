#include "LARouteToPoint.h"

#include "BehaviorExecutionContext.h"

namespace AI
{

	/**
	M�todo invocado al principio de la ejecuci�n de la acci�n,
	para que se realicen las tareas que son �nicamente necesarias
	al principio (y no durante toda la vida de la acci�n).
	<p>
	En este caso, se debe enviar un mensaje al componente 
	CRouteTo y cambiar al estado SUSPENDED.

	@return Estado de la funci�n; si se indica que la
	acci�n a terminado (LatentAction::Completed), se invocar�
	al OnStop().
	*/
	CLatentAction::LAStatus CLARouteToPoint::OnStart()
	{	
		// TODO PR�CTICA IA
		// Al comenzar la ejecuci�n, la acci�n env�a un mensaje al 
		// componente que calcula las rutas. Mientras este componente
		// se ejecuta, la acci�n se queda suspendida, esperando recibir
		// el mensaje de que ha llegado a destino o no se ha podido 
		// calcular la ruta.

		// Intenta sacar el punto de destino del contexto
		// Si no existe la acci�n falla
		if (!_context->hasAttribute("pos"))
			return FAIL;
		// Lo mismo para la entidad
		if (!_context->hasAttribute("entity"))
			return FAIL;
		// Saca la posici�n de destino
		Vector3 target = _context->getVector3Attribute("pos");
		// Saca la entidad
		CEntity* entity = (CEntity*) _context->getUserData("entity");
		// Y se la env�a al componente que se ocupa de calcular rutas de la entidad
		sendMoveMessage(entity, true, target);
		return SUSPENDED;
	}

	/**
	M�todo invocado al final de la ejecuci�n de la acci�n,
	para que se realicen las tareas que son �nicamente necesarias
	al final (y no durante toda la vida de la acci�n).

	En la mayor�a de los casos este m�todo no hace nada.
	*/
	void CLARouteToPoint::OnStop()
	{
	}

	/**
	M�todo invocado c�clicamente para que se contin�e con la
	ejecuci�n de la acci�n.
	<p>
	En este caso no hace nada.

	@return Estado de la acci�n tras la ejecuci�n del m�todo;
	permite indicar si la acci�n ha terminado o se ha suspendido.
	*/
	CLatentAction::LAStatus CLARouteToPoint::OnRun() {
		// TODO PR�CTICA IA
		// Si el flujo de ejecuci�n llega a entrar en este m�todo
		// significa que hay algo que no va bien. Se supone que 
		// la acci�n, nada m�s iniciarse se queda suspendida (por 
		// lo que no se llega a llamar a este m�todo) y cuando 
		// recibe los mensajes pasa directamente a terminar con
		// �xito o con fallo.
		return RUNNING;
	}

	/**
	Env�a un mensaje de tipo ROUTE_TO.
	*/
	void CLARouteToPoint::sendMoveMessage(CEntity* entity, bool walk, Vector3 target)
	{
		// Env�a un mensaje para calcular una ruta
		// El bool indica si el receptor debe calcular la ruta 
		// o debe dejar de recorrer la ruta actual y parar
		// Destino de la ruta. Si el par�metro anterior es false
		// este valor no se utiliza
		CRouteToMessage *message2 = new CRouteToMessage(walk, target);

		entity->emitMessage(message2, NULL);
	}

	/**
	M�todo invocado cuando la acci�n ha sido cancelada (el comportamiento
	al que pertenece se ha abortado por cualquier raz�n).

	La tarea puede en este momento realizar las acciones que
	considere oportunas para "salir de forma ordenada".

	@note <b>Importante:</b> el Abort <em>no</em> provoca la ejecuci�n
	de OnStop().
	*/
	CLatentAction::LAStatus CLARouteToPoint::OnAbort() 
	{
		// TODO PR�CTICA IA
		// Cuando se interrumpe una acci�n de seguimiento de una ruta
		// hay que enviar un mensaje al componente de seguimiento de rutas
		// para que se detenga si hay una ruta a medioseguir.

		// Enviamos un mensaje al componente de enrutamiento para que pare.
		if (_context->hasAttribute("entity")) {
			CEntity* entity = (CEntity*) _context->getUserData("entity");
			sendMoveMessage(entity);
		}
		return FAIL;
	}

	/**
	Devuelve true si a la acci�n le interesa el tipo de mensaje
	enviado como par�metro.
	<p>
	Esta acci�n acepta mensajes del tipo FAILED_ROUTE y FINISHED_ROUTE

	@param msg Mensaje que ha recibido la entidad.
	@return true Si la acci�n est� en principio interesada
	por ese mensaje.
	*/

	bool CLARouteToPoint::accept(CMessage *message)
	{
		// TODO PR�CTICA IA
		// Esta acci�n acepta mensajes del tipo FAILED_ROUTE y FINISHED_ROUTE

		return (message->getType() == Message::FINISHED_ROUTE);
	}
	/**
	Procesa el mensaje recibido. El m�todo es invocado durante la
	ejecuci�n de la acci�n cuando se recibe el mensaje.
	<p>
	Si recibe FINISHED_ROUTE la acci�n finaliza con �xito. Si recibe
	FAILED_ROUTE finaliza con fallo.

	@param msg Mensaje recibido.
	*/
	void CLARouteToPoint::process(CMessage *message)
	{
		// TODO PR�CTICA IA
		// Si se recibe un mensaje de fallo de la ruta hay que terminar con fallo.
		// Si es de finalizaci�n de la ruta hay que terminar con �xito.
		// Para terminar una acci�n latente usamos el m�todo finish (ver LatentAction.h)

		if (message->getType() == Message::FINISHED_ROUTE) {
			finish(static_cast<CIAMessage *>(message)->_bool);
		}
	}

} // namespace AI
