/**
* @file PerceptionSignal.h
*
* En este fichero se define la clase que representa una se�al de percepci�n.
* Una se�al representa de manera abstracta a todo aquello que puede ser percibido
* por un sensor.
*
* @author Gonzalo Fl�rez
* @date 11/04/2011
*/

#pragma once

#ifndef __AI_PerceptionSignal_H
#define __AI_PerceptionSignal_H

//#include "PerceptionManager.h"

namespace AI
{

	/**
	* Clase que representa las se�ales de percepci�n.
	* Una se�al representa de manera abstracta a todo aquello que puede ser percibido
	* por un sensor.
	*
	* Las propiedades de la se�al son las que marcan las diferencias entre los tipos de se�ales.
	*
	* @author Gonzalo Fl�rez
	* @date 11/04/2011
	*/
	class CPerceptionSignal
	{
	public:
		CPerceptionSignal(CPerceptionEntity *pEntity, int type, float intensity= 0.0f, float delay = 0.0f, bool active = true, bool keepAlive = true, std::string name="") :
			_pEntity(pEntity), _active(active), _keepAlive(keepAlive), _type(type), 
			_intensity(intensity), _delay(delay), _name(name) {}

		bool isActive() const { return _active; }
		bool keepAlive() const {return _keepAlive; }
		float getIntensity() const { return _intensity; }
		int getType() const { return _type; }
		CPerceptionEntity* getPerceptionEntity() const { return _pEntity; }
		float getDelay() const {return _delay; }
		std::string getName() const { return _name; }

		void setActive (bool active) { _active = active; }

	private:
		CPerceptionEntity* _pEntity;
		bool _active;
		bool _keepAlive;
		int _type;
		float _intensity;
		float _delay;
		std::string _name;

	}; // class CPerceptionSignal

} // namespace AI

#endif __AI_PerceptionSignal_H
