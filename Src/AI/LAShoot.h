/**

*/
#ifndef __AI_LAShoot_H
#define __AI_LAShoot_H

#include "LatentAction.h"

namespace Logic
{
	class CShooter;
}

namespace AI
{
	class CLAShoot : public CLatentAction
	{
	public:
		/**
		Pongo targetName="" por si en un futuro hay que a�adir el nombre de
		la entidad objetivo
		*/
		CLAShoot(float precision, std::string targetName="");
		~CLAShoot();

		virtual bool accept(Logic::CMessage *message);
		
		virtual void process(Logic::CMessage *message);

		virtual CLatentAction* clone()
		{
			return new CLAShoot(*this);
		}
	protected:
		virtual LAStatus OnStart();
		virtual void OnStop();
		virtual LAStatus OnRun();
		virtual LAStatus OnAbort();

		float _precision;
		std::string _targetName;
		CShooter *_shooterComponent;
	}; // class CLAShoot

} // namespce AI

#endif // __AI_LAShoot_H