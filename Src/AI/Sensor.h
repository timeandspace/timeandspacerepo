/**
* @file Sensor.h
*
* En este fichero se define la clase que representa un sensor.
* Los sensores representan la capacidad de percibir determinados tipos de est�mulo
* por parte de las entidades.
*
* @author Gonzalo Fl�rez
* @date 11/04/2011
*/
#pragma once

#ifndef __AI_Sensor_H
#define __AI_Sensor_H

#include "BaseSubsystems/Math.h"

// #include "PerceptionSignal.h"
#include "PerceptionManager.h"

namespace AI 
{

	class CPerceptionSignal;

	/**
	* Clase abstracta que representa un sensor.
	* Los sensores representan la capacidad de percibir determinados tipos de est�mulo
	* por parte de las entidades. Los est�mulos se representan mediante se�ales (AI::CPerceptionSignal).
	*
	* Toda entidad que tiene que percibir un tipo de se�al debe tener al menos un sensor asociado.
	*
	* Cada sensor que herede de esta clase tendr� que implementar al menos el m�todo getType (identifica el
	* tipo de se�al que puede percibir) y perceives. El m�todo perceives ser� invocado por el gestor de
	* percepci�n para cada se�al. En este m�todo es donde se incluye todo el c�digo necesario para
	* comprobar si un sensor detecta una se�al determinada.
	*
	* @author Gonzalo Fl�rez
	* @date 11/04/2011
	*/
	class CSensor
	{

	public:
		CSensor (CPerceptionEntity *pEntity, bool active, float threshold) :
			_pEntity(pEntity), _active(active), _threshold(threshold) {}
		virtual ~CSensor() {}
		bool isActive() { return _active; }
		CPerceptionEntity* getPerceptionEntity() { return _pEntity; }
		float getThreshold() { return _threshold; }

		void setActive(bool active) { _active = active; }

		virtual EnmPerceptionType getType() = 0;
		virtual bool perceives(CPerceptionSignal* signal, unsigned long time) = 0;

	protected:
		bool _active;
		float _threshold;
		CPerceptionEntity* _pEntity;

	};

} // namespace AI 

#endif __AI_Sensor_H
