
#include "BehaviorExecutionContext.h"

#include "LAEvade.h"
#include "Logic\Server.h"
#include "Logic\Maps\Map.h"
#include "Movement.h"


namespace AI
{
	CLAEvade::CLAEvade(std::string targetVariableName) : CLatentAction(), _targetVariableName(targetVariableName) 
	{

	}

	CLAEvade::~CLAEvade()
	{
	
	}

	CLatentAction::LAStatus CLAEvade::OnStart()
	{
		std::string targetName = _context->getStringAttribute(_targetVariableName);
		Logic::CEntity* entityToPursue = Logic::CServer::getSingletonPtr()->getMap()->
			getEntityByName(targetName);;

		assert(entityToPursue && "CLAEvade::OnStart: _target = NULL");
		
		if(entityToPursue)
		{
			CSetSteeringBehaviour *msg = new CSetSteeringBehaviour();
			msg->_otherEntityInvolved = entityToPursue;
			msg->_movementType = AI::IMovement::MOVEMENT_DYNAMIC_EVADE;
			CEntity* entity = (CEntity*) (_context->getUserData("entity"));
			entity->emitMessage(msg);

			_status = RUNNING;
		}
		else
		{
			_status = FAIL;
		}
		return _status;
	}

	CLatentAction::LAStatus CLAEvade::OnRun()
	{
		return _status;
	}

	CLatentAction::LAStatus CLAEvade::OnAbort()
	{
		std::string targetName = _context->getStringAttribute(_targetVariableName);
		Logic::CEntity* entityToPursue = Logic::CServer::getSingletonPtr()->getMap()->
			getEntityByName(targetName);;
		CSetSteeringBehaviour *msg = new CSetSteeringBehaviour();
		msg->_otherEntityInvolved = entityToPursue;
		msg->_movementType = AI::IMovement::MOVEMENT_NONE;
		CEntity* entity = (CEntity*) (_context->getUserData("entity"));
		entity->emitMessage(msg);
		return SUCCESS;
	}

} // namespace AI
