/**
 * @file NodePriorityInterrupt.h
 *
 * Implementaci�n de un nodo selector con prioridad
 *
 * @author "Gonzalo Fl�rez"
 * @date 11/04/2011
 */
#pragma once

#ifndef AI_NodePriorityCondition_H_
#define AI_NodePriorityCondition_H_

#include "NodeSelector.h"
#include "LatentAction.h"

namespace AI {

	/**
	 * Implementaci�n de un nodo selector con prioridad para un BT.
	 * <p>
	 * Este nodo ejecuta sus hijos sucesivamente hasta que uno de ellos tiene �xito,
	 * pero se diferencia del selector en que en cada tick vuelve a intentar ejecutar
	 * todos los nodos desde el principio. Si alguno de los nodos que hab�a fallado antes
	 * se puede ejecutar ahora pasa a ejecutarse, y el que se estaba ejecutando, al tener
	 * menor prioridad, se cancela.
	 *
	 * @author "Gonzalo Fl�rez"
	 * @date 11/04/2011
	 */
	class CNodePriorityCondition : public CNodeSelector
	{
	public:

		/**
		 * Constructor
		 */
		CNodePriorityCondition() : CNodeSelector() { currentPosition = -1; _name = "Priority interrupt";};


		CNodePriorityCondition::CNodePriorityCondition(const CNodePriorityCondition& other)
		{
			_name = other._name;
			_context = NULL;

			for (unsigned int i=0; i<other._children.size();++i)
			{
				addChild(other._children[i]->clone());
			}

			currentPosition = other.currentPosition;

			_conditions.clear();
			for (unsigned int i=0; i<other._conditions.size();++i)
			{
				CLatentAction* copiedAction = NULL;
				if(other._conditions[i] != NULL)
					copiedAction = other._conditions[i]->clone();
				_conditions.push_back(copiedAction);
			}
		}


		/**
		 * El comportamiento de este nodo es muy parecido al del nodo selector (CNodeSelector).<br>
		 * Intenta ejecutar todos los nodos desde el m�s prioritario. Si alguno de los nodos
		 * no falla, cancela todos los nodos siguientes, si alguno de ellos se estaba ejecutando.
		 * <p>
		 * � Si el hijo actual devuelve BT_SUCCESS, el nodo termina con �xito (BT_SUCCESS).<br>
		 * � Si el hijo actual devuelve BT_RUNNING, el nodo devuelve BT_RUNNING.
		 * En el siguiente tick se continuar� ejecutando el mismo hijo.<br>
		 * En estos dos casos, si se estaba ejecutando alg�n nodo de menor prioridad se cancela su
		 * ejecuci�n.
		 * � Si el hijo actual devuelve BT_FAILURE, se ejecuta el siguiente hijo. Si no hay m�s hijos,
		 * el nodo termina con fallo.
		 *
		 * @param agent dato que podemos pasar al �rbol para su ejecuci�n
		 * @return estado en que queda el nodo despu�s de la ejecuci�n
		 */
		BEHAVIOR_STATUS execute(void* agent);

		virtual BehaviorTreeInternalNode* addChild(BehaviorTreeNode* newChild) {
			return addChild(newChild, NULL);
		}

		virtual BehaviorTreeInternalNode* addChild(BehaviorTreeNode* newChild, CLatentAction* condition);

		/**
		 * Devuelve cierto si alguno de sus hijos acepta el mensaje que recibe como par�metro.
		 * Las acciones latentes de las condiciones tambi�n pueden querer recibir mensajes
		 *
		 * @param message Mensaje
		 * @return true si se acepta este mensaje
		 */
		virtual bool accept(Logic::CMessage *message){
			if (CNodeSelector::accept(message))
				return true;

			std::vector<CLatentAction*>::iterator iter;
			for (iter = _conditions.begin(); iter!= _conditions.end(); ++iter)
			{
				if ((*iter) != NULL && (*iter)->accept(message))
					return true;
			}
			return false;
		}

		/**
		 * Procesa un mensaje. Se delega el procesamiento en los hijos del nodo y en las condiciones.
		 *
		 * @param message Mensaje
		 */
		virtual void process(Logic::CMessage *message) {
			CNodeSelector::process(message);
			std::vector<CLatentAction*>::iterator iter;
			for (iter = _conditions.begin(); iter!= _conditions.end(); ++iter)
			{
				if ((*iter) != NULL && (*iter)->accept(message))
					(*iter)->process(message);
			}
		}

		/**
		 * Asigna un contexto de ejecuci�n al nodo. El contexto se asigna tambi�n
		 * recursivamente a todos sus hijos.
		 * <p>
		 * El contexto de ejecuci�n permite
		 * almacenar y recuperar valores y tambi�n comunicarselos a otros nodos.
		 *
		 * @param context Contexto de ejecuci�n del nodo
		 */
		virtual void useContext(CBehaviorExecutionContext* context) {
			CNodeSelector::useContext(context);
			std::vector<CLatentAction*>::iterator iter;
			for (iter = _conditions.begin(); iter!= _conditions.end(); ++iter)
			{
				if (*iter != NULL)
					(*iter)->useContext(context);
			}
		}

		virtual ~CNodePriorityCondition() {
			std::vector<CLatentAction*>::iterator iter;
			for (iter = _conditions.begin(); iter!= _conditions.end(); ++iter)
			{
				delete (*iter);
			}
		}


		virtual BehaviorTreeNode* clone()
		{
			return new CNodePriorityCondition(*this);
		}


	protected:
		/// A standard vector of Behavior Tree nodes. Provided for convenience.
		std::vector<CLatentAction*> _conditions;
	};

} // namespace logica

#endif AI_NodePriorityCondition_H_
