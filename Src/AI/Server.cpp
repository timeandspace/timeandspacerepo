/**
@file Server.cpp

Servidor de IA.

@author Gonzalo Fl�rez
@date Diciembre, 2010
*/
#include "Server.h"
#include "Physics/Server.h"
#include "BTBuilder.h"
#include "ContextParser.h"

#include <assert.h>

#include "Application/ApplicationCommon.h"


namespace AI {
	/////////////////////////////////////////
	/**
	Instancia �nica de la clase.
	*/
	CServer* CServer::_instance = 0;
	/////////////////////////////////////////
	/**
	Constructor de la clase.
	*/
	CServer::CServer(void)
	{
		assert(!_instance && "Segunda inicializaci�n de AI::CServer no permitida!");
		_f = new CAStarFunctionsGaleon();
		_aStar = new micropather::MicroPather(_f);
		_pManager = new CPerceptionManager();
		_btBuilder = new CBTBuilder();
		_contextParser = new CContextParser();
	}
	/////////////////////////////////////////
	/**
	Destructor de la clase.
	*/
	CServer::~CServer(void)
	{
		assert(_instance);
		for (unsigned int i = 0; i < _coverPoints.size(); ++i)
			delete _coverPoints[i];
		delete _pManager;
		delete _aStar;
		delete _f;
		delete _btBuilder;
		delete _contextParser;
	}
	/////////////////////////////////////////
	/**
	Inicializa el servidor de IA. Esta operaci�n s�lo es necesario realizarla
	una vez durante la inicializaci�n de la aplicaci�n. 

	@return Devuelve false si no se ha podido inicializar.
	*/
	bool CServer::Init() 
	{
		_instance = new CServer();
		_instance->_btBuilder->parseLibrary(SIMPLEBTLIBRARYPATH);
		_instance->_btBuilder->parseLibrary(SIMPLE2BTLIBRARYPATH);
		_instance->_btBuilder->parseLibrary(BTLIBRARYPATH);
		return true;
	}
	/////////////////////////////////////////
	/**
	Libera el servidor de IA. Debe llamarse al finalizar la aplicaci�n.
	*/
	void CServer::Release()
	{
		if (_instance)
			delete _instance;
		_instance = 0;
	}

	void CServer::reset()
	{
		for (unsigned int i = 0; i < _coverPoints.size(); ++i)
			delete _coverPoints[i];
		_coverPoints.clear();
		_wpg.reset();
	}

	/////////////////////////////////////////
	/**
	A�ade un nuevo nodo al grafo de navegaci�n.
	*/
	void CServer::addWaypoint(Vector3 waypoint, float coverPoint)
	{
		if (coverPoint >= 0.7f)
		{
			_coverPoints.push_back(new CNode(waypoint, coverPoint));
		}
		_wpg.addWaypoint(waypoint, coverPoint);
	}
	/////////////////////////////////////////
	/**
	Recalcula el grafo de navegaci�n a partir de los nodos que han sido
	a�adidos con addGraphNode.
	*/
	void CServer::computeNavigationGraph() 
	{
		_wpg.computeNavigationGraph();
	}
	/////////////////////////////////////////
	/**
	Calcula una ruta usando A*.
	*/
	std::vector<Vector3> *CServer::getAStarRoute(Vector3 from, Vector3 to)
	{
		// Antes de hacer cualquier c�lculo, comprueba si hay visibilidad
		// desde from a to, evitando calculos del A* innecesarios.
		if (from == to) return 0;
		Vector3 dist = to - from;
		float distMagnitude = dist.normalise();
		Vector3 origin(from.x, from.y+0.2, from.z);
		Ray ray (origin, dist);

		if (!Physics::CServer::getSingletonPtr()->raycastSingle(ray, distMagnitude, 
			Physics::FilterGroup::WORLD))
		{
			vector<Vector3>* out = new vector<Vector3>();
			out->push_back(from);
			out->push_back(to);
			return out;
		}

		// Dadas dos posiciones devuelve una ruta para llegar de una a otra.
		// En primer lugar utilizamos los m�todos del grafo de waypoints para obtener los nodos
		// m�s cercanos al origen y al destino (_wpg.getClosestWaypoint). 
		// A continuaci�n usamos A* para calcular la ruta (_aStar->Solve).
		// Por �ltimo tenemos que devolver la ruta en un vector de posiciones. Para 
		// ello tendremos que convertir los ids de los nodos en sus posiciones (_wpg.getNode) 
		// y a�adir las posiciones de origen y destino.
		int idFrom = _wpg.getClosestWaypoint(from);
		int idTo = _wpg.getClosestWaypoint(to);
		vector<void*>* path = new vector<void*>();
		float cost = 0.0f;
		int solved = _aStar->Solve((void*) idFrom, (void*) idTo, path, &cost);
		if (solved == micropather::MicroPather::NO_SOLUTION || path->size() == 0) {
			delete path;
			return 0;
		}

		vector<Vector3>* out = new vector<Vector3>();
		// A�adimos el punto inicial si no coincide con el primer nodo
		if (!from.positionEquals(_wpg.getNode((int)((*path)[0]))->position, 5.0))
			out->push_back(from);
		for (vector<void*>::iterator it = path->begin(); it != path->end(); ++it) {
			out->push_back(_wpg.getNode((int)(*it))->position);
		}
		// A�adimos el punto final si no coincide con el �ltimo nodo
		if (!to.positionEquals(_wpg.getNode((int) ((*path)[path->size() - 1]))->position, 5.0))
			out->push_back(to);
		delete path;
		return out;
	}

	//--------------------------------------------------------------------------

	/**
	group -> grupo del que cubrirse
	*/
	Vector3* CServer::getCoverPoint(Vector3 from, float minDistance, float maxDistance, unsigned int group)
	{
		// Buscar los puntos de cobertura dentro de un rango definido por maxDistance
		Vector3 *closestCoverPoint = new Vector3(0, 0, 0);
		float closestDist = FLT_MAX;
		for (unsigned int i = 0; i < _coverPoints.size(); ++i)
		{
			float distance = _coverPoints[i]->position.distance(from);
			if ((distance < closestDist) && (distance < maxDistance) && (distance > minDistance))
			{
				//Comprobar que el punto de cobertura sea de verdad un punto de cobertura
				if (_pManager->checkCoverPoint(_coverPoints[i]->position, 
					"player", maxDistance))
				{
					*closestCoverPoint = _coverPoints[i]->position;
					closestDist = distance;
				}
			}
		}
		if (closestDist != FLT_MAX)
			return closestCoverPoint;
		return NULL;
	}

	BehaviorTreeNode* CServer::parseTree(const std::string &file)
	{
		return _btBuilder->parseBT(file);
	}

	CBehaviorExecutionContext* CServer::parseContext(const std::string &file)
	{
		return _contextParser->parseContext(file);
	}

	/////////////////////////////////////////
	/**
	Dado un �ngulo en radianes lo lleva al intervalo [-PI, PI]
	*/
	double CServer::correctAngle(double angle)
	{
		while (angle > Math::PI)
			angle -= 2 * Math::PI;
		while (angle < -Math::PI)
			angle += 2 * Math::PI;
		return angle;
	}

} // namespace AI
