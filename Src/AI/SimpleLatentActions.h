/**
@file CSimpleLatentActions.h

En este fichero se implementan algunas acciones 
latentes b�sicas.

@author Gonzalo Fl�rez
@date Diciembre 2010

*/

#pragma once

#ifndef __AI_SimpleLatentActions_H
#define __AI_SimpleLatentActions_H

#include "LatentAction.h"

#include "Logic/Entity/Entity.h"

using namespace Logic;

namespace AI 
{

	/**
	Esta acci�n espera durante un periodo de tiempo indicado en el constructor.
	*/
	class CLAWait : public CLatentAction
	{
	public:
		/**
		Constructor.

		@param time Tiempo de espera
		*/
		CLAWait(unsigned long time) : CLatentAction(), _time(time), _varName("") {};

		/**
		Constructor.

		@param varName Nombre de la variable que contiene el tiempo de espera en la pizarra
		*/
		CLAWait(const std::string& varName);

		/**
		Destructor
		*/
		~CLAWait() {};

		/**
		M�todo invocado al principio de la ejecuci�n de la acci�n,
		para que se realicen las tareas que son �nicamente necesarias
		al principio (y no durante toda la vida de la acci�n).
		<p>
		Al comenzar se obtiene el tiempo actual y se calcula el 
		tiempo en el que terminar� la acci�n mediante el atributo _time

		@return Estado de la funci�n; si se indica que la
		acci�n a terminado (LatentAction::Completed), se invocar�
		al OnStop().
		*/
		virtual LAStatus OnStart();

		/**
		M�todo invocado al final de la ejecuci�n de la acci�n,
		para que se realicen las tareas que son �nicamente necesarias
		al final (y no durante toda la vida de la acci�n).

		En la mayor�a de los casos este m�todo no hace nada.
		*/
		virtual void OnStop();

		/**
		M�todo invocado c�clicamente para que se contin�e con la
		ejecuci�n de la acci�n.
		<p>
		En cada paso de ejecuci�n se obtiene el tiempo actual 
		y se comprueba si se debe acabar la acci�n.

		@return Estado de la acci�n tras la ejecuci�n del m�todo;
		permite indicar si la acci�n ha terminado o se ha suspendido.
		*/
		virtual LAStatus OnRun() ;

		/**
		M�todo invocado cuando la acci�n ha sido cancelada (el comportamiento
		al que pertenece se ha abortado por cualquier raz�n).

		La tarea puede en este momento realizar las acciones que
		considere oportunas para "salir de forma ordenada".

		@note <b>Importante:</b> el Abort <em>no</em> provoca la ejecuci�n
		de OnStop().
		*/
		virtual LAStatus OnAbort() ;
		/**
		Devuelve true si a la acci�n le interesa el tipo de mensaje
		enviado como par�metro.
		<p>
		Esta acci�n no acepta mensajes de ning�n tipo.

		@param msg Mensaje que ha recibido la entidad.
		@return true Si la acci�n est� en principio interesada
		por ese mensaje.
		*/
		virtual bool accept(CMessage *message);
		/**
		Procesa el mensaje recibido. El m�todo es invocado durante la
		ejecuci�n de la acci�n cuando se recibe el mensaje.

		@param msg Mensaje recibido.
		*/
		virtual void process(CMessage *message);


		virtual CLatentAction* clone()
		{
			return new CLAWait(*this);
		}

	protected:
		/**
		Tiempo de espera
		*/
		unsigned int _time;
		/**
		Tiempo en el que se termina la acci�n
		*/
		unsigned int _endingTime;

		std::string _varName;
	};


	class CLAGoTo : public CLatentAction
	{
	public:
		/**
		Constructor.

		@param time Tiempo de espera
		*/
		CLAGoTo(unsigned int wayPoint, CEntity* entity) : CLatentAction(), 
			_wayPoint(wayPoint)
		{

		}

		/**
		Destructor
		*/
		~CLAGoTo() {};

		/**
		M�todo invocado al principio de la ejecuci�n de la acci�n,
		para que se realicen las tareas que son �nicamente necesarias
		al principio (y no durante toda la vida de la acci�n).
		<p>
		Al comenzar se obtiene el tiempo actual y se calcula el 
		tiempo en el que terminar� la acci�n mediante el atributo _time

		@return Estado de la funci�n; si se indica que la
		acci�n a terminado (LatentAction::Completed), se invocar�
		al OnStop().
		*/
		virtual LAStatus OnStart();

		/**
		M�todo invocado al final de la ejecuci�n de la acci�n,
		para que se realicen las tareas que son �nicamente necesarias
		al final (y no durante toda la vida de la acci�n).

		En la mayor�a de los casos este m�todo no hace nada.
		*/
		virtual void OnStop();

		/**
		M�todo invocado c�clicamente para que se contin�e con la
		ejecuci�n de la acci�n.
		<p>
		En cada paso de ejecuci�n se obtiene el tiempo actual 
		y se comprueba si se debe acabar la acci�n.

		@return Estado de la acci�n tras la ejecuci�n del m�todo;
		permite indicar si la acci�n ha terminado o se ha suspendido.
		*/
		virtual LAStatus OnRun() ;

		/**
		M�todo invocado cuando la acci�n ha sido cancelada (el comportamiento
		al que pertenece se ha abortado por cualquier raz�n).

		La tarea puede en este momento realizar las acciones que
		considere oportunas para "salir de forma ordenada".

		@note <b>Importante:</b> el Abort <em>no</em> provoca la ejecuci�n
		de OnStop().
		*/
		virtual LAStatus OnAbort() ;
		/**
		Devuelve true si a la acci�n le interesa el tipo de mensaje
		enviado como par�metro.
		<p>
		Esta acci�n no acepta mensajes de ning�n tipo.

		@param msg Mensaje que ha recibido la entidad.
		@return true Si la acci�n est� en principio interesada
		por ese mensaje.
		*/
		virtual bool accept(CMessage *message);
		/**
		Procesa el mensaje recibido. El m�todo es invocado durante la
		ejecuci�n de la acci�n cuando se recibe el mensaje.

		@param msg Mensaje recibido.
		*/
		virtual void process(CMessage *message);


		virtual CLatentAction* clone()
		{
			return new CLAGoTo(*this);
		}

	protected:
		/**
		Tiempo de espera
		*/
		int _wayPoint;

	};

	/**
	Acci�n latente que busca un punto de cobertura.
	*/
	class CLACover : public CLatentAction
	{
	public:

		/**
		Constructor.
		*/
		CLACover() : CLatentAction() 
		{
			//this->setEntity(entity);
		}

		/**
		Destructor
		*/
		~CLACover() {}

		/**
		Devuelve true si a la acci�n le interesa el tipo de mensaje
		enviado como par�metro.
		<p>
		Esta acci�n no acepta mensajes de ning�n tipo.

		@param msg Mensaje que ha recibido la entidad.
		@return true Si la acci�n est� en principio interesada
		por ese mensaje.
		*/
		virtual bool accept(CMessage *message);
		/**
		Procesa el mensaje recibido. El m�todo es invocado durante la
		ejecuci�n de la acci�n cuando se recibe el mensaje.

		@param msg Mensaje recibido.
		*/
		virtual void process(CMessage *message);


		virtual CLatentAction* clone()
		{
			return new CLACover(*this);
		}

	protected:
		/**
		M�todo invocado al principio de la ejecuci�n de la acci�n,
		para que se realicen las tareas que son �nicamente necesarias
		al principio (y no durante toda la vida de la acci�n).
		<p>
		Al comenzar se obtiene el tiempo actual y se calcula el 
		tiempo en el que terminar� la acci�n mediante el atributo _time

		@return Estado de la funci�n; si se indica que la
		acci�n a terminado (LatentAction::Completed), se invocar�
		al OnStop().
		*/
		virtual LAStatus OnStart();

		/**
		M�todo invocado al final de la ejecuci�n de la acci�n,
		para que se realicen las tareas que son �nicamente necesarias
		al final (y no durante toda la vida de la acci�n).

		En la mayor�a de los casos este m�todo no hace nada.
		*/
		virtual void OnStop();

		/**
		M�todo invocado c�clicamente para que se contin�e con la
		ejecuci�n de la acci�n.
		<p>
		En cada paso de ejecuci�n se obtiene el tiempo actual 
		y se comprueba si se debe acabar la acci�n.

		@return Estado de la acci�n tras la ejecuci�n del m�todo;
		permite indicar si la acci�n ha terminado o se ha suspendido.
		*/
		virtual LAStatus OnRun() ;

		/**
		M�todo invocado cuando la acci�n ha sido cancelada (el comportamiento
		al que pertenece se ha abortado por cualquier raz�n).

		La tarea puede en este momento realizar las acciones que
		considere oportunas para "salir de forma ordenada".

		@note <b>Importante:</b> el Abort <em>no</em> provoca la ejecuci�n
		de OnStop().
		*/
		virtual LAStatus OnAbort() ;

		float _minDistance;
		float _maxDistance;

	};


	//////////////////////////////
	//////////////////////////////
	//	
	//	Clase CLAExistsData
	//	
	//////////////////////////////
	//////////////////////////////


	class CLAExistsData : public CLatentAction
	{
	public:
		CLAExistsData(std::string destVariable) : CLatentAction(), _destVariable(destVariable) {
			_name = "ExistsData";
		};
		~CLAExistsData(void) {};

	protected:
		std::string _destVariable;

		/**
		M�todo invocado al principio de la ejecuci�n de la acci�n,
		para que se realicen las tareas que son �nicamente necesarias
		al principio (y no durante toda la vida de la acci�n).
		<p>

		@return Estado de la funci�n; si se indica que la
		acci�n a terminado (LatentAction::Completed), se invocar�
		al OnStop().
		*/
		virtual LAStatus OnStart();

		virtual CLatentAction* clone()
		{
			return new CLAExistsData(*this);
		}


	}; // class CLAExistsData 


	//////////////////////////////
	//////////////////////////////
	//	
	//	Clase CLAInvalidateData
	//	
	//////////////////////////////
	//////////////////////////////

	class CLAInvalidateData : public CLatentAction
	{
	public:
		CLAInvalidateData(std::string destVariable) : CLatentAction(), _destVariable(destVariable) {
			_name = "InvalidateData";
		};
		~CLAInvalidateData(void) {};

	protected:
		std::string _destVariable;

		/**
		M�todo invocado al principio de la ejecuci�n de la acci�n,
		para que se realicen las tareas que son �nicamente necesarias
		al principio (y no durante toda la vida de la acci�n).
		<p>

		@return Estado de la funci�n; si se indica que la
		acci�n a terminado (LatentAction::Completed), se invocar�
		al OnStop().
		*/
		virtual LAStatus OnStart();

		virtual CLatentAction* clone()
		{
			return new CLAInvalidateData(*this);
		}


	}; // class CLAInvalidateData  



	////////////////////////////////
	////////////////////////////////
	////	
	////	Clase CLAObjectPerceived
	////	
	////////////////////////////////
	////////////////////////////////
	class CLAGetObject : public CLatentAction
	{
	public:
		CLAGetObject() : CLatentAction() {
			_name = "GetObject";
		};
		~CLAGetObject(void) {};

	protected:
		/**
		M�todo invocado al principio de la ejecuci�n de la acci�n,
		para que se realicen las tareas que son �nicamente necesarias
		al principio (y no durante toda la vida de la acci�n).
		<p>

		@return Estado de la funci�n; si se indica que la
		acci�n a terminado (LatentAction::Completed), se invocar�
		al OnStop().
		*/
		virtual LAStatus OnStart();

		virtual CLatentAction* clone()
		{
			return new CLAGetObject(*this);
		}


	}; // class CLAGetObject 



	/**
	Comprueba si existe una entidad.
	*/
	class CLAExistsEntity : public CLatentAction
	{
	public:
		CLAExistsEntity(std::string destVariable) : CLatentAction(), _destVariable(destVariable) {
			_name = "LAExistsEntity";
		};
		~CLAExistsEntity(void) {};

	protected:
		/**
		*/
		virtual LAStatus OnStart();

		virtual CLatentAction* clone()
		{
			return new CLAExistsEntity(*this);
		}

		std::string _destVariable;

	}; // class CLAGetObject 


	////////////////////////////////
	////////////////////////////////
	////	
	////	Clase CLATeletransport
	////	
	////////////////////////////////
	////////////////////////////////
	class CLATeletransport : public CLatentAction
	{
	public:
		CLATeletransport(std::string destVariable) : CLatentAction(), _destVariable(destVariable) {
			_name = "Teletransport";
		};
		~CLATeletransport(void) {};

	protected:
		/**
		M�todo invocado al principio de la ejecuci�n de la acci�n,
		para que se realicen las tareas que son �nicamente necesarias
		al principio (y no durante toda la vida de la acci�n).
		<p>

		@return Estado de la funci�n; si se indica que la
		acci�n a terminado (LatentAction::Completed), se invocar�
		al OnStop().
		*/
		virtual LAStatus OnStart();

		virtual CLatentAction* clone()
		{
			return new CLATeletransport(*this);
		}

		std::string _destVariable;

	}; // class CLAGetObject 

} //namespace AI 

#endif // __AI_SimpleLatentActions_H