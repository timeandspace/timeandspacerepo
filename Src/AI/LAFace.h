#pragma once

#ifndef __AI_LAFace_H
#define __AI_LAFace_H

#include "LatentAction.h"

namespace AI 
{
	/**
	Acci�n latente que persigue a una entidad
	*/
	class CLAFace : public CLatentAction 
	{
	public:
		CLAFace(std::string targetVariableName, bool face);

		~CLAFace(); 
		
	protected:
		/**
		M�todo invocado al principio de la ejecuci�n de la acci�n,
		para que se realicen las tareas que son �nicamente necesarias
		al principio (y no durante toda la vida de la acci�n).
		<p>
		En este caso, se debe enviar un mensaje al componente 
		CRouteTo y cambiar al estado SUSPENDED.

		@return Estado de la funci�n; si se indica que la
		acci�n a terminado (LatentAction::Completed), se invocar�
		al OnStop().
		*/    
		virtual LAStatus OnStart();

		/**
		M�todo invocado c�clicamente para que se contin�e con la
		ejecuci�n de la acci�n.
		<p>
		En este caso, se va actualizando la posici�n del jugador.

		@return Estado de la acci�n tras la ejecuci�n del m�todo;
		permite indicar si la acci�n ha terminado o se ha suspendido.
		*/
		virtual LAStatus OnRun();


		virtual bool accept(Logic::CMessage *message);
		/**
		Procesa el mensaje recibido. El m�todo es invocado durante la
		ejecuci�n de la acci�n cuando se recibe el mensaje.
		<p>
		Si recibe PERCEIVED comprueba que la entidad percibida coincide con 
		el tipo que es capaz de percibir. Si es as� tiene �xito y si no, falla.

		@param msg Mensaje recibido.
		*/
		virtual void process(Logic::CMessage *message);


		virtual CLatentAction* clone()
		{
			return new CLAFace(*this);
		}

	private:

		std::string _targetVariableName;

		CLatentAction::LAStatus _status;

		std::string _targetName;

		bool _face;

		bool _aligned;
	};

} // namespace AI 

#endif __AI_LAFace_H
