#include "NodeSequential.h"

using namespace AI;
using namespace std;

/**
* Inicializa el nodo y todos sus hijos.
*
* @param agent dato que podemos pasar al �rbol para su ejecuci�n
*/
void CNodeSequential::init(void* agent)
{
	_currentPosition = -1; //-1 indica que no se est� ejecutando ninguno.

	// LLamamos al init de los hijos
	BehaviorTreeListIter it = _children.begin();
	BehaviorTreeListIter end = _children.end();
	for ( ; it != end; ++it)
	{
		(*it)->init(agent);
	}
}

/**
* Constructor
*/
CNodeSequential::CNodeSequential()
{
	_name = "Sequence";
	_currentPosition = -1;
}


/**
* Ejecuta los siguientes nodos de la secuencia.
* <p>
* La ejecuci�n terminar� si alguno de los hijos falla (entonces el nodo
* secuencia falla) o si devuelve BT_RUNNING (el nodo secuencia tambi�n
* devuelve BT_RUNNING). Si el hijo actual devuelve BT_SUCCESS, pasa a
* ejecutarse el siguiente. Si es el �ltimo, entonces el nodo secuencia
* termina con �xito.
*
* @param agent dato que podemos pasar al �rbol para su ejecuci�n
* @return estado en que queda el nodo despu�s de la ejecuci�n
*/
BEHAVIOR_STATUS CNodeSequential::execute(void* agent)
{
	if (_children.size() == 0)
		return BT_SUCCESS;

	if (_currentPosition == -1)
	{
		init(agent);
		_currentPosition = 0;
	}

	BehaviorTreeNode* currentTask = _children.at(_currentPosition);
	BEHAVIOR_STATUS status = currentTask->execute(agent);
	if (status == BT_FAILURE)
	{
		_currentPosition = -1;
	}
	//@Todo: posible cuello de botella si los hijos son muy costosos,
	// ya que se ejecutan todos los hijos por tick.
	while (status == BT_SUCCESS)
	{
		if (_currentPosition == _children.size() - 1)
		{
			_currentPosition = -1;
			return BT_SUCCESS;
		}
		++ _currentPosition;
		currentTask = _children.at(_currentPosition);
		status = currentTask->execute(agent);
	}

	return status;
}
