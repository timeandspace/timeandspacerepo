#include "PerceptionEntity.h"

#include "Sensor.h"
#include "PerceptionSignal.h"

using namespace std;

namespace AI 
{

	/**
	* Destructor.
	* El destructor se responsabiliza de eliminar el contenido de las listas de sensores y de se�ales de la entidad
	*/
	CPerceptionEntity::~CPerceptionEntity(void)
	{
		// Eliminamos los sensores
		for (list<CSensor*>::iterator it = _sensors.begin();
			it != _sensors.end(); ++it)
		{
			delete (*it);
			(*it) = NULL;
		}
		// Y las se�ales
		for (list<CPerceptionSignal*>::iterator it = _signals.begin();
			it != _signals.end(); ++it)
		{
			delete (*it);
			(*it) = NULL;
		}
	}

	/**
	* Elimina un sensor de la entidad
	*/
	void CPerceptionEntity::removeSensor(CSensor* sensor)
	{
		for (list<CSensor*>::iterator it = _sensors.begin();
			it != _sensors.end(); ++it)
		{
			if (sensor == (*it))
				_sensors.erase(it);
			break;
		}
	}

	/**
	* Elimina una se�al de la entidad
	*/
	void CPerceptionEntity::removeSignal(CPerceptionSignal* signal)
	{
		for (list<CPerceptionSignal*>::iterator it = _signals.begin();
			it != _signals.end(); ++it)
		{
			if (signal== (*it))
			{
				_signals.erase(it);
				break;
			}
		}
	}

	/**
	* Elimina una se�al de la entidad
	*/
	void CPerceptionEntity::removeSignal(std::string name)
	{
		for (list<CPerceptionSignal*>::iterator it = _signals.begin();
			it != _signals.end(); ++it)
		{
			if (name == (*it)->getName())
			{
				_signals.erase(it);
				break;
			}
		}
	}

	/**
	* Establece el valor del flag active para todos los sensores
	*/
	void CPerceptionEntity::setSensorsActive(bool active)
	{
		for (list<CSensor*>::iterator it = _sensors.begin();
			it != _sensors.end(); ++it)
		{
			(*it)->setActive(active);
			break;
		}
	}
	/**
	* Establece el valor del flag active para todas las se�ales
	*/
	void CPerceptionEntity::setSignalsActive(bool active)
	{
		for (list<CPerceptionSignal*>::iterator it = _signals.begin();
			it != _signals.end(); ++it)
		{
			(*it)->setActive(active);
		}
	}

	void CPerceptionEntity::setSignalActive(std::string name, bool active)
	{
		for (list<CPerceptionSignal*>::iterator it = _signals.begin();
			it != _signals.end(); ++it)
		{
			if (name == (*it)->getName())
			{
				(*it)->setActive(active);
				break;
			}
		}
	}


} // namespace AI 
