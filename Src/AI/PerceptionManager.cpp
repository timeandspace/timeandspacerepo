#include "PerceptionManager.h"

#include "Application/BaseApplication.h"

#include "BaseSubsystems/Server.h"
#include <OgreTimer.h>

#include "Sensor.h"
#include "PerceptionEntity.h"
#include "PerceptionSignal.h"

#include "Physics/Server.h"

using namespace std;

namespace AI 
{
	/**
	* Elimina una entidad de percepci�n de la lista de entidades gestionadas.
	* Esto evita que se realicen las comprobaciones de percepci�n sobre sus
	* sensores y se�ales y que reciba notificaciones.
	*
	* @param entity Entidad de percepci�n que se va a desregistrar
	*/
	void CPerceptionManager::unregisterEntity(CPerceptionEntity* entity)
	{
		std::list<PerceivedPair>::iterator it = _perceivedList.begin();
		std::list<PerceivedPair>::iterator end = _perceivedList.end();
		for ( ; it != end; )
		{
			if ((*it).sensor->getPerceptionEntity() == entity || 
				(*it).signal->getPerceptionEntity() == entity)
			{
				CNotification *notification = new CNotification(false, _time, (*it).sensor, 
					(*it).signal->getPerceptionEntity());
				//_notifications.push(notification);
				CPerceptionEntity *ent = notification->getSensor()->getPerceptionEntity();
				if (ent)
					ent->sendNotification(notification);

				it = _perceivedList.erase(it);
			}
			else
				++it;
		}

		for (list<CPerceptionEntity*>::iterator iter = _entities.begin();
			iter != _entities.end(); ++iter)
		{
			if (entity == (*iter))
			{
				_entities.erase(iter);
				break;
			}
		}
	}

	/**
	* El m�todo update se encarga de actualizar la informaci�n
	* sobre los sensores de cada entidad de percepci�n.
	*
	* @param msecs duraci�n del �ltimo tick
	*/
	void CPerceptionManager::update(unsigned int msecs)
	{
		// Sacamos del reloj de la aplicaci�n el instante actual para
		// gestionar los instantes de las notificaciones
		unsigned int time = Application::CBaseApplication::getSingletonPtr()->getAppTime();
		_time = time;
		// Fase de agregaci�n y chequeo:
		// Se buscan los sensores potenciales de las se�ales actuales y se comprueba si detectan las se�ales
		// Las variables notIni, notAdded y notRemoved nos indican cu�ntas notificaciones hay en la cola de
		// pendientes al principio, despu�s de a�adir y despu�s de entregar. Podemos usar estos valores para depurar.
		checkPerception(time);

		// Fase de notificaci�n:
		// En esta fase se env�an las notificaciones cuyo tiempo de entrega haya caducado
		notifyEntities(time);

	}

	/**
	* Comprueba los sensores de las entidades registradas
	*
	* @param time Instante de tiempo en que se realiza la comprobaci�n
	*/
	void CPerceptionManager::checkPerception(unsigned int time)
	{
		std::list<PerceivedPair>::iterator it = _perceivedList.begin();
		std::list<PerceivedPair>::iterator end = _perceivedList.end();
		for ( ; it != end; )
		{
			if(!(*it).sensor->perceives((*it).signal, time))
			{
				CNotification *notificacion = new CNotification(false, time, (*it).sensor, 
					(*it).signal->getPerceptionEntity());
				_notifications.push(notificacion);
				it = _perceivedList.erase(it);
			}
			else
				++it;
		}


		list<CPerceptionEntity *>::iterator itSensers = _entities.begin();
		list<CPerceptionEntity *>::iterator endSensers = _entities.end();
		// Para cada entidad que puede percibir (senser)
		for ( ; itSensers != endSensers; ++itSensers)
		{
			// Para cada entidad que puede ser percibida (signaler)
			CPerceptionEntity *senser = (*itSensers);
			list<CPerceptionEntity *>::iterator itSignalers = _entities.begin();
			for ( ; itSignalers != _entities.end(); ++itSignalers)
			{
				CPerceptionEntity *signaler = (*itSignalers);
				// No permitimos que una entidad se perciba a ella misma
				if (senser != signaler)
				{
					list<CPerceptionSignal *> signals = signaler->getSignals();
					list<CPerceptionSignal *>::iterator itSignal = signals.begin();
					// Por cada se�al que emita el signaler
					while (itSignal != signals.end())
					{
						CPerceptionSignal *signal = (*itSignal);
						// Comprobamos si la se�al est� activa
						if (signal->isActive())
						{
							// Por cada sensor del senser
							list<CSensor *>sensors = senser->getSensors();
							list<CSensor *>::iterator itSensor = sensors.begin();
							for ( ; itSensor != sensors.end(); ++itSensor)
							{
								CSensor *sensor = (*itSensor);
								// Comprobamos si el sensor est� activo
								if (sensor->isActive() && 
									sensor->getType() == signal->getType())
								{
									// Comprobamos si ya esta percibido
									bool perceived = false;
									if (_perceivedList.size() > 0)
									{
										it = _perceivedList.begin();
										for ( ; it != end; ++it)
										{
											if ((*it).sensor == sensor &&
												(*it).signal == signal)
											{
												perceived = true;
											}
										}
									}

									// Comprobamos si el sensor puede percibir la se�al invocando al m�todo perceives
									// El m�todo perceives devuelve la notificaci�n que hay que enviar si hay percepci�n o NULL si no la hay
									if (!perceived)
									{
										if (sensor->perceives(signal, time))
										{
											// Si el sensor percibe la se�al, la a�adimos a la lista de notificaciones pendientes
											// La lista de notificaciones es una cola con prioridad, por lo que 
											// las notificaciones se ordenar�n por el tiempo en el que hay que entregarlas
											CNotification *notificacion = new CNotification(true, time, sensor, signal->getPerceptionEntity());
											_notifications.push(notificacion);
											// A�adimos el sensor y la se�al en una lista, para comprobar en el siguiente
											// tick si se siguen percibiendo
											if (! signal->keepAlive())
											{
												CNotification *n = new CNotification(false, time+50, sensor, signal->getPerceptionEntity());
												_notifications.push(n);
											}
											else
											{
												PerceivedPair pair;
												pair.sensor = sensor;
												pair.signal = signal;
												_perceivedList.push_back(pair);
											}

										}
									}
								}
							}
						}
						// Hay 2 tipos de se�ales: temporales y permanentes.
						// Si la se�al es temporal tenemos que borrarla (esto nos lo indica la propiedad keepAlive).
						// Si no es temporal, pasamos directamente a la siguiente incrementando el iterador.
						if (! signal->keepAlive())
						{
							signaler->removeSignal(*itSignal);
							*itSignal = 0;
							itSignal = signals.erase(itSignal);
						}
						else
							++ itSignal;
					}
				}
			}
		}
	}

	/**
	* Recorre la lista de notificaciones pendientes y env�a las
	* que hayan caducado
	*
	* @param time Instante de tiempo en que se realiza la comprobaci�n
	*/
	void CPerceptionManager::notifyEntities(unsigned int time)
	{
		// Si no hay notificaciones pendientes no hacemos nada
		if (_notifications.empty()) return;

		// Procesamos las notificaciones de la cola cuyo tiempo de entrega sea anterior al actual
		// La cola est� ordenada por el tiempo de entrega
		CNotification *notification;
		while (! _notifications.empty() &&
			(notification = _notifications.top())->getTime() <= time)
		{
			_notifications.pop();
			CPerceptionEntity *ent = notification->getSensor()->getPerceptionEntity();
			if (ent)
				ent->sendNotification(notification);
		}
	}

	bool CPerceptionManager::checkCoverPoint(Vector3 coverPosition, 
		std::string typeToCover, float maxDistance)
	{
		std::list<CPerceptionEntity*>::const_iterator it = _entities.begin();
		std::list<CPerceptionEntity*>::const_iterator end = _entities.end();
		Physics::CServer *physicsServer = Physics::CServer::getSingletonPtr();
		for ( ; it != end; ++it)
		{
			if ((*it)->getType() == typeToCover)
			{
				// Lanzar un rayo desde el coverPosition hasta la entidad
				// si colisiona con la entidad no es posible
				Vector3 dist = (*it)->getTransform().getTrans() - coverPosition;
				coverPosition.y = 1;
				float distance = dist.normalise();
				Ray ray;
				ray.setOrigin(coverPosition);
				ray.setDirection(dist);
				int group = 0;
				if (typeToCover == "player")
				{
					group = Physics::FilterGroup::WORLD;
				}
				else if (typeToCover == "enemy")
				{
					//@TODO: cambiar
					group = Physics::FilterGroup::WORLD;
				}	
				Physics::Collision col;
				if (physicsServer->raycastSingle(ray, distance, group, col) == NULL)
					return false;
			}
		}

		return true;
	}

} // namespace AI 
