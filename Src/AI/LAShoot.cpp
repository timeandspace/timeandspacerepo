/**

*/
#include "LAShoot.h"
#include "BehaviorExecutionContext.h"

#include "Logic\Entity\Components\Shooter.h"
#include "Logic\Entity\Components\PerceptionComponent.h"
#include "Logic\Maps\Map.h"

namespace AI
{
	CLAShoot::CLAShoot(float precision, std::string targetName) : CLatentAction(), 
		_precision(precision), _targetName(targetName)
	{
		/*CEntity *ent = ((CEntity*) _context->getUserData("entity"));
		_shooterComponent = static_cast<CShooter *>(ent->getComponent("CShooter"));*/
	}

	//---------------------------------------------------------

	CLAShoot::~CLAShoot()
	{
	}

	//---------------------------------------------------------

	bool CLAShoot::accept(Logic::CMessage *message)
	{
		return false;
	}

	//---------------------------------------------------------

	void CLAShoot::process(Logic::CMessage *message)
	{

	}

	//---------------------------------------------------------

	float getPrecision(const float precision)
	{
		return (1 - precision) * 
			(-1 + 2 * static_cast <float> (rand()) / (static_cast <float> (RAND_MAX)));
	}

	//---------------------------------------------------------

	CLatentAction::LAStatus CLAShoot::OnStart()
	{
		// Movemos el pitch y el yaw un valor aleatorio en función de la precisión.
		CEntity *ent = ((CEntity*) _context->getUserData("entity"));
		float height = ((CPerceptionComponent *)ent->getComponent("CPerceptionComponent"))->getSightHeight();
		CEntity *targetEntity = NULL;
		//obtener objetivo
		if (_targetName == "isEntityPerceived")
		{
			std::list<std::string> *entitiesPercivedList = 
				(std::list<std::string> *)_context->getUserData("isEntityPerceived");
			targetEntity = ent->getMap()->getEntityByName(entitiesPercivedList->front());
		}
		else
			targetEntity = ent->getMap()->getEntityByName(_context->getStringAttribute(_targetName));
		float targetHeight = ((CPerceptionComponent *)targetEntity->getComponent("CPerceptionComponent"))->getSightHeight();
		Vector3 entPos = ent->getPosition();
		entPos.y += height;
		Vector3 targetPos = targetEntity->getPosition();
		targetPos.y += targetHeight * 0.8;
		Vector3 dir = targetPos - entPos;
		//dir += Math::getDirection(ent->getYaw() + Math::PI/2);
		//float offset = getPrecision(_precision);
		// rotamos el vector unos grados aleatorios para simular la precision
		//Math::rotateVectorYAxis(dir, offset);
		dir.x += getPrecision(_precision);
		dir.y += getPrecision(_precision);
		dir.z += getPrecision(_precision);
		dir.normalise(); 

		/*static_cast<CShooter *>(ent->getComponent("CShooter"))->
		fire(entPos, dir);*/
		// animación de disparo
		CControlMessage *msg = new CControlMessage();
		msg->_avatarAction = TAvatarActions::FIRE;
		msg->_vector = dir;
		ent->emitMessage(msg);

		return SUCCESS;
	}

	//---------------------------------------------------------

	CLatentAction::LAStatus CLAShoot::OnRun()
	{
		CControlMessage *msg = new CControlMessage();
		msg->_avatarAction = TAvatarActions::STOP_FIRE;
		((CEntity*) _context->getUserData("entity"))->emitMessage(msg);
		CEntity *ent = ((CEntity*) _context->getUserData("entity"));
		return SUCCESS;
	}

	//---------------------------------------------------------

	void CLAShoot::OnStop()
	{
		/*CControlMessage *msg = new CControlMessage();
		msg->_string = "stopFire";
		((CEntity*) _context->getUserData("entity"))->emitMessage(msg);*/
	}

	//---------------------------------------------------------

	CLatentAction::LAStatus CLAShoot::OnAbort()
	{
		CControlMessage *msg = new CControlMessage();
		msg->_avatarAction = TAvatarActions::STOP_FIRE;
		((CEntity*) _context->getUserData("entity"))->emitMessage(msg);
		return FAIL;
	}

	//---------------------------------------------------------

} // namespace AI
