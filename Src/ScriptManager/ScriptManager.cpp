/**
@file ScriptManager.cpp

Contiene la implementaci�n del servidor de scripts 
de Lua.

@author Alejandro Perez Alonso
@date Marzo, 2014
*/

#include "ScriptManager.h"

#include "Binding.h"

#include <cassert>
#include <string.h>  // Para strdup
#include <iostream>

extern "C" {
#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>
}

#pragma warning( disable: 4251)
#include <luabind/luabind.hpp>

namespace ScriptManager 
{

	// �nica instancia de la clase.
	CScriptManager *CScriptManager::_instance = 0;


	bool CScriptManager::Init()
	{

		assert(_instance == NULL);
		_instance = new CScriptManager();
		if (!_instance->open()) {
			// ��Vaya!!
			Release();
			return false;
		}

		return true;

	} // Init

	//---------------------------------------------------------

	void CScriptManager::Release() 
	{

		assert(_instance);

		delete _instance;
		_instance = NULL;

	} // Release

	//---------------------------------------------------------

	CScriptManager &CScriptManager::getSingleton() 
	{

		assert(_instance);

		return *_instance;

	} // GetSingleton

	//---------------------------------------------------------

	CScriptManager *CScriptManager::getSingletonPtr() 
	{

		assert(_instance);

		return _instance;

	} // GetPtrSingleton

	//---------------------------------------------------------

	bool CScriptManager::loadScript(const char *script) {

		if (luaL_loadfile(_lua, script) != 0)
			return false;

		return true;

	} // loadScript

	//---------------------------------------------------------

	bool CScriptManager::executeScript(const char *script) 
	{

		if (luaL_dofile(_lua, script) != 0)
			if (luaL_dostring(_lua, script) != 0)
				return false;

		return true;

	} // executeScript

	//---------------------------------------------------------

	int CScriptManager::getGlobal(const char *name, int defaultValue) 
	{

		assert(_lua && "CScriptManager::getGlobal -> lua_state no inicializado");

		luabind::object obj = luabind::globals(_lua)[name];

		if (!obj.is_valid() || (luabind::type(obj) != LUA_TNUMBER))
			return defaultValue;
		else
			return luabind::object_cast<int>(obj);

	} // getGlobal(int)

	//---------------------------------------------------------

	bool CScriptManager::getGlobal(const char *name, bool defaultValue) 
	{

		assert(_lua && "CScriptManager::getGlobal -> lua_state no inicializado");

		luabind::object obj = luabind::globals(_lua)[name];

		if (!obj.is_valid() || (luabind::type(obj) != LUA_TBOOLEAN))
			return defaultValue;
		else
			return luabind::object_cast<bool>(obj);

	} // getGlobal(bool)

	//---------------------------------------------------------

	char *CScriptManager::getGlobal(const char *name,
		const char *defaultValue) 
	{
		assert(_lua && "CScriptManager::getGlobal -> lua_state no inicializado");

		luabind::object obj = luabind::globals(_lua)[name];

		if (!obj.is_valid() || (luabind::type(obj) != LUA_TSTRING))
		{

			return const_cast<char*>(defaultValue);
		}
		else
			return 0;

	} // getGlobal(char*)

	//---------------------------------------------------------

	int CScriptManager::getField(const char *table,
		const char *field, int defaultValue) 
	{

		return 0;

	} // getField(int)

	//---------------------------------------------------------

	bool CScriptManager::getField(const char *table,
		const char *field, bool defaultValue) 
	{

		return false;

	} // getField(bool)

	//---------------------------------------------------------


	char *CScriptManager::getField(const char *table,
		const char *field, const char *defaultValue) 
	{

		return 0;

	} // getField(char*)

	//---------------------------------------------------------

	bool CScriptManager::executeProcedure(const char *subroutineName) 
	{
		assert(_lua);

		luabind::object obj = luabind::globals(_lua)[subroutineName];

		if(!obj.is_valid() || (luabind::type(obj) != LUA_TFUNCTION))
			return false;
		else 
		{
			obj();
			return true;
		}

	} // executeProcedure


	//---------------------------------------------------------

	bool CScriptManager::executeProcedure(const char *subroutineName, int param1) 
	{
		assert(_lua);

		try {
			luabind::object obj = luabind::globals(_lua)[subroutineName](param1);
			if(!obj.is_valid() || (luabind::type(obj) != LUA_TFUNCTION))
				return false;
			else 
			{
				obj();
				return true;
			}
		} catch (luabind::error &ex)
		{
#ifdef _DEBUG
			std::cout << ex.what() << "\n";
#endif
			return false;
		}

		return true;

	} // executeProcedure(int)

	//--------------------------------------------------------
	
	bool CScriptManager::executeProcedure(const char *subroutineName, int param1, int param2) 
	{
		assert(_lua);

		try {
			luabind::object obj = luabind::globals(_lua)[subroutineName](param1, param2);
			if(!obj.is_valid() || (luabind::type(obj) != LUA_TFUNCTION))
				return false;
			else 
			{
				obj();
				return true;
			}
		} catch (luabind::error &ex)
		{
#ifdef _DEBUG
			std::cout << ex.what() << "\n";
#endif
			return false;
		}

		return true;

	} // executeProcedure(int)

	//---------------------------------------------------------

	bool CScriptManager::executeFunction(const char *subroutineName,
		int param1, int &result) 
	{
		assert(_lua);

		// If you want to pass a parameter as a reference, you have to wrap it with the Boost.Ref
		// int ret = call_function(L, "fun", boost::ref(val));
		try
		{
			result = luabind::call_function<int>(_lua, subroutineName, param1);
		} catch(luabind::error &ex)
		{
#ifdef _DEBUG
			std::cout << ex.what() << "\n";
#endif
			return false;
		}
		return true;

	} // executeFunction

	//---------------------------------------------------------

	bool CScriptManager::executeFunction(const char *subroutineName, int param1, int param2, bool &result)
	{
		assert(_lua);

		// If you want to pass a parameter as a reference, you have to wrap it with the Boost.Ref
		// int ret = call_function(L, "fun", boost::ref(val));
		try
		{
			result = luabind::call_function<bool>(_lua, subroutineName, param1, param2);
		} catch(luabind::error &ex)
		{
#ifdef _DEBUG
			std::cout << ex.what() << "\n";
#endif
			return false;
		}
		return true;
	} // executeFunction

	//---------------------------------------------------------

	void CScriptManager::registerFunction(lua_CFunction f, const char *luaName) 
	{
		luabind::module(_lua)
			[
				luabind::def(luaName, f)
			];
	} // registerFunction	

	//---------------------------------------------------------

	/*template<class T>
	void CScriptManager::registerClass(T, const char *className)
	{
		luabind::module(_lua)
			[
				luabind::class_<T>(className)
			];
	}*/

	//---------------------------------------------------------
	//                   M�todos protegidos
	//---------------------------------------------------------

	CScriptManager::CScriptManager() : _lua(NULL) 
	{

	} // Constructor

	//---------------------------------------------------------

	CScriptManager::~CScriptManager() 
	{

		close();

	} // Destructor

	//---------------------------------------------------------

	bool CScriptManager::open() 
	{

		_lua = lua_open();
		if (_lua == NULL)
			return false;

		// Abro las librer�as de lua (segunda parte de la inicializaci�n)
		luaL_openlibs(_lua);

		// Abrimos la librer�a base para hacer disponible
		// print() en Lua. (funcion que esta dentro de la libreria lualib.h)
		luaopen_base(_lua);

		luabind::open(_lua);

		bind();

		executeScript(LUA_FUNCTION_AUX);

		return true;

	} // open

	//---------------------------------------------------------

	void CScriptManager::close() 
	{

		if (_lua)
		{
			lua_close(_lua);
			_lua = NULL;
		}

	} // close

	//---------------------------------------------------------

	void CScriptManager::bind()
	{
		CBinding binder;
		binder.bind(_lua);

	} // bind

	//---------------------------------------------------------

	/*template<typename Type>
	void CScriptManager::setInstance(Type *object, const char *className)
	{
		luabind::globals(_lua)[className] = object;
	}*/

	//---------------------------------------------------------

} // namespace ScriptManager
