/**
@file Binding.h

Contiene la declaraci�n de la clase que se encargar de bindear clases a Lua

@author Alejandro Perez Alonso
@date Julio, 2014
*/
#ifndef __ScriptManager_Binding_H
#define __ScriptManager_Binding_H

#include "BaseSubsystems\Math.h"
struct lua_State;

namespace Logic
{
	class CCutScene;
}

namespace Graphics
{
	class CCamera;
}

namespace ScriptManager
{
	class CScriptManager;

	class CBinding 
	{
	public:
		CBinding() {}
		~CBinding() {}

		/**
		M�todo que se encarga de bindear clases
		*/
		void bind(lua_State *luaState);

		void setInstance(Graphics::CCamera *object, const char *className);
		void setInstance(Vector3 *object, const char *className);
		void setInstance(Logic::CCutScene *object, const char *className);
		void deleteInstance(const char *className);
	protected:
		friend class CScriptManager;
		void bindCamera(lua_State* L);
		void bindVector3(lua_State* L);
		void bindCutScene(lua_State* L);
	};
}

#endif 

