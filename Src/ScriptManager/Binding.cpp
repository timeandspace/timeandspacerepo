/**
@file Binding.cpp

Contiene la implementacion de la clase que se encargar de bindear clases a Lua.

@author Alejandro Perez Alonso
@date Julio, 2014
*/

#include "Binding.h"
#include "ScriptManager.h"

extern "C" {
#include <lua.h>
#include <lauxlib.h>
}

#pragma warning( disable: 4251)
#include <luabind/luabind.hpp>
#include <luabind/operator.hpp>
#include "Graphics/Camera.h"
#include "Logic/Entity/Components/CutScene.h"

namespace ScriptManager
{
#define LUA_CONST_START( class ) { luabind::object g = luabind::globals(L); luabind::object table = g[#class]; 
#define LUA_CONST( class, name ) table[#name] = class::name 
#define LUA_CONST_END } 

	//---------------------------------------------------------

	void CBinding::bind(lua_State *luaState)
	{
		bindVector3(luaState);
		bindCamera(luaState);
		bindCutScene(luaState);
	}

	//---------------------------------------------------------

	void CBinding::setInstance(Vector3 *object, const char *className)
	{
		try
		{
			luabind::globals(CScriptManager::getSingletonPtr()->_lua)[className] = object;
		} catch(luabind::error &ex)
		{
#ifdef _DEBUG
			std::cout << ex.what() << "\n";
#endif
		}
	}

	//---------------------------------------------------------

	void CBinding::setInstance(Graphics::CCamera *object, const char *className)
	{
		try
		{
			luabind::globals(CScriptManager::getSingletonPtr()->_lua)[className] = object;
		} catch(luabind::error &ex)
		{
#ifdef _DEBUG
			std::cout << ex.what() << "\n";
#endif
		}
	}

	//---------------------------------------------------------

	void CBinding::setInstance(Logic::CCutScene *object, const char *className)
	{
		try
		{
			luabind::globals(CScriptManager::getSingletonPtr()->_lua)[className] = object;
		} catch(luabind::error &ex)
		{
#ifdef _DEBUG
			std::cout << ex.what() << "\n";
#endif
		}
	}

	//---------------------------------------------------------

	void CBinding::deleteInstance(const char *className)
	{
		try
		{
			luabind::globals(CScriptManager::getSingletonPtr()->_lua)[className] = NULL;
		} catch(luabind::error &ex)
		{
#ifdef _DEBUG
			std::cout << ex.what() << "\n";
#endif
		}
	}

	//---------------------------------------------------------

	void CBinding::bindCamera(lua_State* L)
	{
		luabind::module(L)
			[
				luabind::class_<Graphics::CCamera>( "Camera" )
				.def(luabind::constructor<const std::string &, Graphics::CScene *, float, float>())
				.def("getCameraPosition", &Graphics::CCamera::getCameraPosition)
				.def("getTargetCameraPosition", &Graphics::CCamera::getTargetCameraPosition)
				.def("getCameraOrientation", &Graphics::CCamera::getCameraOrientation)
				.def("getTargetDistance", &Graphics::CCamera::getTargetDistance)
				.def("setCameraPosition", &Graphics::CCamera::setCameraPosition)
				.def("setTargetCameraPosition", &Graphics::CCamera::setTargetCameraPosition)
				//.def("getCamera", &Graphics::CCamera::getCamera)
				//.def("activateCutSceneMode", &Graphics::CCamera::activateCutSceneMode)
				//.def("deactivateCutSceneMode", &Graphics::CCamera::deactivateCutSceneMode)
				//.def("getWorldSpaceCorners", &Graphics::CCamera::getWorldSpaceCorners)
			]; 
	}

	//---------------------------------------------------------

	void CBinding::bindCutScene(lua_State* L)
	{
		luabind::module(L)
			[ 
				luabind::class_<Logic::CCutScene>("CutScene")
				.def(luabind::constructor<>())
				.def("sendSwitchMessage", &Logic::CCutScene::sendSwitchMessage)
			];
	}

	//---------------------------------------------------------

	void CBinding::bindVector3(lua_State* L) 
	{ 
		luabind::module(L)
			[ 
				luabind::class_<Vector3>( "Vector3" )
				.def_readwrite( "x", &Vector3::x )
				.def_readwrite( "y", &Vector3::y )
				.def_readwrite( "z", &Vector3::z ) 
				.def(luabind::constructor<>())
				.def(luabind::constructor<Vector3&>()) 
				.def(luabind::constructor<Ogre::Real, Ogre::Real, Ogre::Real>()) 
				.def("absDotProduct", &Vector3::absDotProduct) 
				.def("crossProduct", &Vector3::crossProduct ) 
				.def("directionEquals", &Vector3::directionEquals )
				.def("distance", &Vector3::distance ) 
				.def("dotProduct", &Vector3::dotProduct ) 
				.def("getRotationTo", &Vector3::getRotationTo )
				.def("isZeroLength", &Vector3::isZeroLength ) 
				.def("length", &Vector3::length ) 
				.def("makeCeil", &Vector3::makeCeil ) 
				.def("makeFloor", &Vector3::makeFloor ) 
				.def("midPoint", &Vector3::midPoint ) 
				.def("normalise", &Vector3::normalise ) 
				.def("nornaliseCopy", &Vector3::normalisedCopy )
				.def("perpendicular", &Vector3::perpendicular )
				.def("positionCloses", &Vector3::positionCloses )
				.def("positionEquals", &Vector3::positionEquals ) 
				//.def("ptr", &Vector3::ptr )
				.def("randomDeviant", &Vector3::randomDeviant )
				.def("reflect", &Vector3::reflect )
				.def("squaredDistance", &Vector3::squaredDistance )
				.def("squaredLength", &Vector3::squaredLength ) 

				// Operators 

				.def(luabind::self + luabind::other<Vector3>() ) 
				.def(luabind::const_self + luabind::other<Vector3>() ) 
				.def(luabind::self - luabind::other<Vector3>() ) 
				.def(luabind::const_self - luabind::other<Vector3>() ) 
				.def(luabind::self * luabind::other<Vector3>() ) 
				.def(luabind::self * Ogre::Real() ) 
				.def(luabind::tostring(luabind::self))
			]; 

		LUA_CONST_START( Vector3 );
		LUA_CONST( Vector3, ZERO);
		LUA_CONST( Vector3, UNIT_X);
		LUA_CONST( Vector3, UNIT_Y);
		LUA_CONST( Vector3, UNIT_Z);
		LUA_CONST( Vector3, NEGATIVE_UNIT_X);
		LUA_CONST( Vector3, NEGATIVE_UNIT_Y);
		LUA_CONST( Vector3, NEGATIVE_UNIT_Z);
		LUA_CONST( Vector3, UNIT_SCALE);
		LUA_CONST_END;
	}

	//---------------------------------------------------------

}