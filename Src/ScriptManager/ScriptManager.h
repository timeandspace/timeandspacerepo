/**
@file ScriptManager.h

Contiene la declaraci�n del servidor de scripts de Lua

@author Alejandro Perez Alonso
@date Marzo, 2014
*/

#ifndef __ScriptManager_Server_H
#define __ScriptManager_Server_H

// Predeclaraci�n de clases para ahorrar tiempo de compilaci�n
// al ahorrarnos la inclusi�n de ficheros .h en otros .h

// Estructura con el contexto (estado) del int�rprete de Lua.
struct lua_State;

typedef int (*lua_CFunction) (lua_State *L);

namespace ScriptManager {

#define LUA_SCRIPTS_PATH "../media/luaScript/"
#define LUA_FUNCTION_AUX "../media/luaScript/functionAux.lua"

	class CBinding;

	class CScriptManager {
	public:

		/**
		M�todo que inicializa el singleton
		*/
		static bool Init();

		/**
		M�todo release, libera los recursos
		*/
		static void Release();

		static CScriptManager &getSingleton();

		/**
		Devuelve un puntero al singleton
		*/
		static CScriptManager *getSingletonPtr();

		/**
		Carga un script
		*/
		bool loadScript(const char *script);

		/**
		Ejecuta un script
		*/
		bool executeScript(const char *script);

		/**
		Accede a variable global de tipo int
		*/
		int getGlobal(const char *name, int defaultValue);

		/**
		Accede a variable global de tipo boolean
		*/
		bool getGlobal(const char *name, bool defaultValue);

		/**
		Accede a variable global de tipo char *
		*/
		char *getGlobal(const char *name, const char *defaultValue);

		int getField(const char *table, const char *field, int defaultValue);

		bool getField(const char *table, const char *field, bool defaultValue);

		char *getField(const char *table, const char *field, const char *defaultValue);


		bool executeProcedure(const char *subroutineName);

		bool executeProcedure(const char *subroutineName, int param1);

		bool executeProcedure(const char *subroutineName, int param1, int param2);

		bool executeFunction(const char *subroutineName, int param1, int &result);

		bool executeFunction(const char *subroutineName, int param1, int param2, bool &result);

		void registerFunction(lua_CFunction f, const char *luaName);

		/*template<typename T>
		void registerClass(T, const char *className);*/

		/*template<typename Type>
		void setInstance(Type *object, const char *className);*/


	protected:
		friend class CBinding;
		CScriptManager();

		~CScriptManager();

		bool open();

		void close();

		void bind();

		static CScriptManager *_instance;

		lua_State *_lua;

	};

} // namespace ScriptManager

#endif // __ScriptManager_Server_H