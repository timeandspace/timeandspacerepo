///*
//** Lua binding: toLuaPackageOther
//** Generated automatically by tolua++-1.0.92 on 07/11/14 21:23:24.
//*/
//
//#ifndef __cplusplus
//#include "stdlib.h"
//#endif
//#include "string.h"
//
//#include "tolua++.h"
//
///* Exported function */
//int tolua_toLuaPackageOther_open (lua_State* tolua_S);
//
//#include "ScriptManager.h"
//#include "Logic/Entity/Components/CutScene.h"
//
///* function to register type */
//static void tolua_reg_types (lua_State* tolua_S)
//{
// tolua_usertype(tolua_S,"Logic::CCutScene::MyVector3");
// tolua_usertype(tolua_S,"Logic::CCutScene");
// tolua_usertype(tolua_S,"ScriptManager::CScriptManager");
//}
//
///* method: getSingleton of class  ScriptManager::CScriptManager */
//#ifndef TOLUA_DISABLE_tolua_toLuaPackageOther_ScriptManager_CScriptManager_getSingleton00
//static int tolua_toLuaPackageOther_ScriptManager_CScriptManager_getSingleton00(lua_State* tolua_S)
//{
//#ifndef TOLUA_RELEASE
// tolua_Error tolua_err;
// if (
// !tolua_isusertable(tolua_S,1,"ScriptManager::CScriptManager",0,&tolua_err) ||
// !tolua_isnoobj(tolua_S,2,&tolua_err)
// )
// goto tolua_lerror;
// else
//#endif
// {
// {
//  ScriptManager::CScriptManager& tolua_ret = (ScriptManager::CScriptManager&)  ScriptManager::CScriptManager::getSingleton();
// tolua_pushusertype(tolua_S,(void*)&tolua_ret,"ScriptManager::CScriptManager");
// }
// }
// return 1;
//#ifndef TOLUA_RELEASE
// tolua_lerror:
// tolua_error(tolua_S,"#ferror in function 'getSingleton'.",&tolua_err);
// return 0;
//#endif
//}
//#endif //#ifndef TOLUA_DISABLE
//
///* method: getSingletonPtr of class  ScriptManager::CScriptManager */
//#ifndef TOLUA_DISABLE_tolua_toLuaPackageOther_ScriptManager_CScriptManager_getSingletonPtr00
//static int tolua_toLuaPackageOther_ScriptManager_CScriptManager_getSingletonPtr00(lua_State* tolua_S)
//{
//#ifndef TOLUA_RELEASE
// tolua_Error tolua_err;
// if (
// !tolua_isusertable(tolua_S,1,"ScriptManager::CScriptManager",0,&tolua_err) ||
// !tolua_isnoobj(tolua_S,2,&tolua_err)
// )
// goto tolua_lerror;
// else
//#endif
// {
// {
//  ScriptManager::CScriptManager* tolua_ret = (ScriptManager::CScriptManager*)  ScriptManager::CScriptManager::getSingletonPtr();
// tolua_pushusertype(tolua_S,(void*)tolua_ret,"ScriptManager::CScriptManager");
// }
// }
// return 1;
//#ifndef TOLUA_RELEASE
// tolua_lerror:
// tolua_error(tolua_S,"#ferror in function 'getSingletonPtr'.",&tolua_err);
// return 0;
//#endif
//}
//#endif //#ifndef TOLUA_DISABLE
//
///* method: printHello of class  ScriptManager::CScriptManager */
//#ifndef TOLUA_DISABLE_tolua_toLuaPackageOther_ScriptManager_CScriptManager_printHello00
//static int tolua_toLuaPackageOther_ScriptManager_CScriptManager_printHello00(lua_State* tolua_S)
//{
//#ifndef TOLUA_RELEASE
// tolua_Error tolua_err;
// if (
// !tolua_isusertype(tolua_S,1,"ScriptManager::CScriptManager",0,&tolua_err) ||
// !tolua_isnoobj(tolua_S,2,&tolua_err)
// )
// goto tolua_lerror;
// else
//#endif
// {
//  ScriptManager::CScriptManager* self = (ScriptManager::CScriptManager*)  tolua_tousertype(tolua_S,1,0);
//#ifndef TOLUA_RELEASE
// if (!self) tolua_error(tolua_S,"invalid 'self' in function 'printHello'",NULL);
//#endif
// {
//  self->printHello();
// }
// }
// return 0;
//#ifndef TOLUA_RELEASE
// tolua_lerror:
// tolua_error(tolua_S,"#ferror in function 'printHello'.",&tolua_err);
// return 0;
//#endif
//}
//#endif //#ifndef TOLUA_DISABLE
//
///* get function: _msecs of class  Logic::CCutScene */
//#ifndef TOLUA_DISABLE_tolua_get_Logic__CCutScene_unsigned__msecs
//static int tolua_get_Logic__CCutScene_unsigned__msecs(lua_State* tolua_S)
//{
//  Logic::CCutScene* self = (Logic::CCutScene*)  tolua_tousertype(tolua_S,1,0);
//#ifndef TOLUA_RELEASE
// if (!self) tolua_error(tolua_S,"invalid 'self' in accessing variable '_msecs'",NULL);
//#endif
// tolua_pushnumber(tolua_S,(lua_Number)self->_msecs);
// return 1;
//}
//#endif //#ifndef TOLUA_DISABLE
//
///* set function: _msecs of class  Logic::CCutScene */
//#ifndef TOLUA_DISABLE_tolua_set_Logic__CCutScene_unsigned__msecs
//static int tolua_set_Logic__CCutScene_unsigned__msecs(lua_State* tolua_S)
//{
//  Logic::CCutScene* self = (Logic::CCutScene*)  tolua_tousertype(tolua_S,1,0);
//#ifndef TOLUA_RELEASE
// tolua_Error tolua_err;
// if (!self) tolua_error(tolua_S,"invalid 'self' in accessing variable '_msecs'",NULL);
// if (!tolua_isnumber(tolua_S,2,0,&tolua_err))
// tolua_error(tolua_S,"#vinvalid type in variable assignment.",&tolua_err);
//#endif
//  self->_msecs = ((unsigned int)  tolua_tonumber(tolua_S,2,0))
//;
// return 0;
//}
//#endif //#ifndef TOLUA_DISABLE
//
///* get function: x of class  MyVector3 */
//#ifndef TOLUA_DISABLE_tolua_get_Logic__CCutScene__MyVector3_x
//static int tolua_get_Logic__CCutScene__MyVector3_x(lua_State* tolua_S)
//{
//  Logic::CCutScene::MyVector3* self = (Logic::CCutScene::MyVector3*)  tolua_tousertype(tolua_S,1,0);
//#ifndef TOLUA_RELEASE
// if (!self) tolua_error(tolua_S,"invalid 'self' in accessing variable 'x'",NULL);
//#endif
// tolua_pushnumber(tolua_S,(lua_Number)self->x);
// return 1;
//}
//#endif //#ifndef TOLUA_DISABLE
//
///* set function: x of class  MyVector3 */
//#ifndef TOLUA_DISABLE_tolua_set_Logic__CCutScene__MyVector3_x
//static int tolua_set_Logic__CCutScene__MyVector3_x(lua_State* tolua_S)
//{
//  Logic::CCutScene::MyVector3* self = (Logic::CCutScene::MyVector3*)  tolua_tousertype(tolua_S,1,0);
//#ifndef TOLUA_RELEASE
// tolua_Error tolua_err;
// if (!self) tolua_error(tolua_S,"invalid 'self' in accessing variable 'x'",NULL);
// if (!tolua_isnumber(tolua_S,2,0,&tolua_err))
// tolua_error(tolua_S,"#vinvalid type in variable assignment.",&tolua_err);
//#endif
//  self->x = ((float)  tolua_tonumber(tolua_S,2,0))
//;
// return 0;
//}
//#endif //#ifndef TOLUA_DISABLE
//
///* get function: y of class  MyVector3 */
//#ifndef TOLUA_DISABLE_tolua_get_Logic__CCutScene__MyVector3_y
//static int tolua_get_Logic__CCutScene__MyVector3_y(lua_State* tolua_S)
//{
//  Logic::CCutScene::MyVector3* self = (Logic::CCutScene::MyVector3*)  tolua_tousertype(tolua_S,1,0);
//#ifndef TOLUA_RELEASE
// if (!self) tolua_error(tolua_S,"invalid 'self' in accessing variable 'y'",NULL);
//#endif
// tolua_pushnumber(tolua_S,(lua_Number)self->y);
// return 1;
//}
//#endif //#ifndef TOLUA_DISABLE
//
///* set function: y of class  MyVector3 */
//#ifndef TOLUA_DISABLE_tolua_set_Logic__CCutScene__MyVector3_y
//static int tolua_set_Logic__CCutScene__MyVector3_y(lua_State* tolua_S)
//{
//  Logic::CCutScene::MyVector3* self = (Logic::CCutScene::MyVector3*)  tolua_tousertype(tolua_S,1,0);
//#ifndef TOLUA_RELEASE
// tolua_Error tolua_err;
// if (!self) tolua_error(tolua_S,"invalid 'self' in accessing variable 'y'",NULL);
// if (!tolua_isnumber(tolua_S,2,0,&tolua_err))
// tolua_error(tolua_S,"#vinvalid type in variable assignment.",&tolua_err);
//#endif
//  self->y = ((float)  tolua_tonumber(tolua_S,2,0))
//;
// return 0;
//}
//#endif //#ifndef TOLUA_DISABLE
//
///* get function: z of class  MyVector3 */
//#ifndef TOLUA_DISABLE_tolua_get_Logic__CCutScene__MyVector3_z
//static int tolua_get_Logic__CCutScene__MyVector3_z(lua_State* tolua_S)
//{
//  Logic::CCutScene::MyVector3* self = (Logic::CCutScene::MyVector3*)  tolua_tousertype(tolua_S,1,0);
//#ifndef TOLUA_RELEASE
// if (!self) tolua_error(tolua_S,"invalid 'self' in accessing variable 'z'",NULL);
//#endif
// tolua_pushnumber(tolua_S,(lua_Number)self->z);
// return 1;
//}
//#endif //#ifndef TOLUA_DISABLE
//
///* set function: z of class  MyVector3 */
//#ifndef TOLUA_DISABLE_tolua_set_Logic__CCutScene__MyVector3_z
//static int tolua_set_Logic__CCutScene__MyVector3_z(lua_State* tolua_S)
//{
//  Logic::CCutScene::MyVector3* self = (Logic::CCutScene::MyVector3*)  tolua_tousertype(tolua_S,1,0);
//#ifndef TOLUA_RELEASE
// tolua_Error tolua_err;
// if (!self) tolua_error(tolua_S,"invalid 'self' in accessing variable 'z'",NULL);
// if (!tolua_isnumber(tolua_S,2,0,&tolua_err))
// tolua_error(tolua_S,"#vinvalid type in variable assignment.",&tolua_err);
//#endif
//  self->z = ((float)  tolua_tonumber(tolua_S,2,0))
//;
// return 0;
//}
//#endif //#ifndef TOLUA_DISABLE
//
///* method: getCameraPosition of class  Logic::CCutScene */
//#ifndef TOLUA_DISABLE_tolua_toLuaPackageOther_Logic_CCutScene_getCameraPosition00
//static int tolua_toLuaPackageOther_Logic_CCutScene_getCameraPosition00(lua_State* tolua_S)
//{
//#ifndef TOLUA_RELEASE
// tolua_Error tolua_err;
// if (
// !tolua_isusertype(tolua_S,1,"Logic::CCutScene",0,&tolua_err) ||
// !tolua_isusertype(tolua_S,2,"Logic::CCutScene::MyVector3",0,&tolua_err) ||
// !tolua_isnoobj(tolua_S,3,&tolua_err)
// )
// goto tolua_lerror;
// else
//#endif
// {
//  Logic::CCutScene* self = (Logic::CCutScene*)  tolua_tousertype(tolua_S,1,0);
//  Logic::CCutScene::MyVector3* tolua_var_1 = ((Logic::CCutScene::MyVector3*)  tolua_tousertype(tolua_S,2,0));
//#ifndef TOLUA_RELEASE
// if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getCameraPosition'",NULL);
//#endif
// {
//  self->getCameraPosition(*tolua_var_1);
// }
// }
// return 0;
//#ifndef TOLUA_RELEASE
// tolua_lerror:
// tolua_error(tolua_S,"#ferror in function 'getCameraPosition'.",&tolua_err);
// return 0;
//#endif
//}
//#endif //#ifndef TOLUA_DISABLE
//
///* method: setCameraPosition of class  Logic::CCutScene */
//#ifndef TOLUA_DISABLE_tolua_toLuaPackageOther_Logic_CCutScene_setCameraPosition00
//static int tolua_toLuaPackageOther_Logic_CCutScene_setCameraPosition00(lua_State* tolua_S)
//{
//#ifndef TOLUA_RELEASE
// tolua_Error tolua_err;
// if (
// !tolua_isusertype(tolua_S,1,"Logic::CCutScene",0,&tolua_err) ||
// !tolua_isusertype(tolua_S,2,"Logic::CCutScene::MyVector3",0,&tolua_err) ||
// !tolua_isnoobj(tolua_S,3,&tolua_err)
// )
// goto tolua_lerror;
// else
//#endif
// {
//  Logic::CCutScene* self = (Logic::CCutScene*)  tolua_tousertype(tolua_S,1,0);
//  Logic::CCutScene::MyVector3* tolua_var_2 = ((Logic::CCutScene::MyVector3*)  tolua_tousertype(tolua_S,2,0));
//#ifndef TOLUA_RELEASE
// if (!self) tolua_error(tolua_S,"invalid 'self' in function 'setCameraPosition'",NULL);
//#endif
// {
//  self->setCameraPosition(*tolua_var_2);
// }
// }
// return 0;
//#ifndef TOLUA_RELEASE
// tolua_lerror:
// tolua_error(tolua_S,"#ferror in function 'setCameraPosition'.",&tolua_err);
// return 0;
//#endif
//}
//#endif //#ifndef TOLUA_DISABLE
//
///* Open function */
//int tolua_toLuaPackageOther_open (lua_State* tolua_S)
//{
// tolua_open(tolua_S);
// tolua_reg_types(tolua_S);
// tolua_module(tolua_S,NULL,0);
// tolua_beginmodule(tolua_S,NULL);
// tolua_module(tolua_S,"ScriptManager",0);
// tolua_beginmodule(tolua_S,"ScriptManager");
//  tolua_cclass(tolua_S,"CScriptManager","ScriptManager::CScriptManager","",NULL);
//  tolua_beginmodule(tolua_S,"CScriptManager");
//   tolua_function(tolua_S,"getSingleton",tolua_toLuaPackageOther_ScriptManager_CScriptManager_getSingleton00);
//   tolua_function(tolua_S,"getSingletonPtr",tolua_toLuaPackageOther_ScriptManager_CScriptManager_getSingletonPtr00);
//   tolua_function(tolua_S,"printHello",tolua_toLuaPackageOther_ScriptManager_CScriptManager_printHello00);
//  tolua_endmodule(tolua_S);
// tolua_endmodule(tolua_S);
// tolua_module(tolua_S,"Logic",0);
// tolua_beginmodule(tolua_S,"Logic");
//  tolua_cclass(tolua_S,"CCutScene","Logic::CCutScene","",NULL);
//  tolua_beginmodule(tolua_S,"CCutScene");
//   tolua_variable(tolua_S,"_msecs",tolua_get_Logic__CCutScene_unsigned__msecs,tolua_set_Logic__CCutScene_unsigned__msecs);
//   tolua_cclass(tolua_S,"MyVector3","Logic::CCutScene::MyVector3","",NULL);
//   tolua_beginmodule(tolua_S,"MyVector3");
//    tolua_variable(tolua_S,"x",tolua_get_Logic__CCutScene__MyVector3_x,tolua_set_Logic__CCutScene__MyVector3_x);
//    tolua_variable(tolua_S,"y",tolua_get_Logic__CCutScene__MyVector3_y,tolua_set_Logic__CCutScene__MyVector3_y);
//    tolua_variable(tolua_S,"z",tolua_get_Logic__CCutScene__MyVector3_z,tolua_set_Logic__CCutScene__MyVector3_z);
//   tolua_endmodule(tolua_S);
//   tolua_function(tolua_S,"getCameraPosition",tolua_toLuaPackageOther_Logic_CCutScene_getCameraPosition00);
//   tolua_function(tolua_S,"setCameraPosition",tolua_toLuaPackageOther_Logic_CCutScene_setCameraPosition00);
//  tolua_endmodule(tolua_S);
// tolua_endmodule(tolua_S);
// tolua_endmodule(tolua_S);
// return 1;
//}
//
//
//#if defined(LUA_VERSION_NUM) && LUA_VERSION_NUM >= 501
// int luaopen_toLuaPackageOther (lua_State* tolua_S) {
// return tolua_toLuaPackageOther_open(tolua_S);
//};
//#endif
//
