//---------------------------------------------------------------------------
// Server.h
//---------------------------------------------------------------------------

/**
@file Server.h

Contiene la declaraci�n de la clase principal de sonido para manejar FMOD y FMOD Studio

@see Sonido::CServer

@author Alejandro P�rez Alonso && Alvaro Blazquez Checa
@date Abril, 2014
*/

#ifndef __Sound_Server_H
#define __Sound_Server_H


#include "BaseSubsystems/Math.h"

namespace FMOD
{
	namespace Studio
	{
		class System;
	}

	class System;
	class ChannelGroup;
	class Sound;
}

struct FMOD_3D_ATTRIBUTES;


namespace Sonido
{
	class CSound;
	class CMusicPlayer;
}

namespace Sonido
{
	class CServer 
	{
	public:

		static CServer* getSingletonPtr() { return _instance; }
		static bool Init();
		static void Release();
		
		FMOD::Studio::System *getSystem() { return _system; }

		void updateAudioListener(const Vector3 &pos, const Vector3 &velocity, const Vector3 &fordward);

		CMusicPlayer* getMusicPlayer() { return _musicPlayer; };
		
		void tick();

	protected:

		CServer();

		virtual ~CServer();


		bool open();

		void close();

		bool loadBanks();

		bool unLoadBanks();

		static CServer *_instance;

		FMOD::Studio::System *_system;

		FMOD::System *_lowLevelSystem;

		CMusicPlayer* _musicPlayer;


		//	LISTENER PROPERTIES
		FMOD_3D_ATTRIBUTES *_listenerAttributes;

	}; // CServer

} //namespace

#endif //__Sound_Server_H