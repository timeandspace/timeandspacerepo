/**

*/

#include "SoundBank.h"
#include "Sound\Server.h"
#include <OgreVector3.h>
#include <assert.h>
#include <string>
#include <fmod_studio.hpp>
#include <fmod_studio_common.h>
#include <fmod_errors.h>

#define SOUNDS_PATH "./media/sounds/"

namespace Sonido
{
	//TODO mirar min max distance
	//TODO mirar loop
	CSoundBank::CSoundBank(const std::string &fileName)
	{
		_system = CServer::getSingletonPtr()->getSystem();
		std::string pathName = SOUNDS_PATH;
		pathName += "Master Bank.bank";


		FMOD::Studio::Bank* bank = NULL;
		FMOD_RESULT result = _system->loadBankFile(pathName.c_str(), FMOD_STUDIO_LOAD_BANK_NORMAL, &bank);

		FMOD::Studio::EventDescription* arrays;

		int count;
		bank->getEventList(&arrays,100,&count);

		for(int i=0; i<count; ++i)
		{
			FMOD::Studio::ID eventID;
			arrays[i].getID(&eventID);

			FMOD::Studio::EventDescription* eventDescription = NULL;
			result = _system->getEvent(&eventID, FMOD_STUDIO_LOAD_BEGIN_NOW, &eventDescription);

			char * paths = new char[100];
			int num;
			result = eventDescription->getPath(paths,100,&num);
			
			std::string str;

			for(int j= 0; j<num;++j)
			{
				str += paths[j];
			}

			result = eventDescription->createInstance(&_sound);
		}

		FMOD::Studio::ID eventID = {0};
		result = _system->lookupID("event:/PasosSpace1", &eventID);
		result = _system->lookupID("MasterBank:/event:/PasosSpace1", &eventID);
		result = _system->lookupID("MasterBank:/event:/PuertaGrande", &eventID);
		result = _system->lookupID("event:/PuertaGrande", &eventID);

		//FMOD::Studio::parseID

		


		

		FMOD::Studio::ParameterInstance* rpm = NULL;
		_sound->getParameter("RPM", &rpm);
 

		if (result != FMOD_OK)
		{
			printf("FMOD error! (%d) %s\n", result, FMOD_ErrorString(result));
			assert(false && "Error al crear CSound");
		}

	}

	CSoundBank::~CSoundBank()
	{
		_sound->release();
	}

	void CSoundBank::play()
	{


	}

	void CSoundBank::stop()
	{
		if(_channel)
		{
			_channel->stop();
			_channel = NULL;
		}
	}

	void CSoundBank::pause(bool pause)
	{
		if(_channel)
		{
			_channel->setPaused(pause);
		}
	}

	void CSoundBank::loop(bool loop)
	{
		_loop = loop;
		if(_channel)
		{
			if(loop)
				_channel->setMode(FMOD_LOOP_NORMAL);
			else
				_channel->setMode(FMOD_LOOP_OFF);
		}
	}


	void CSoundBank::update(unsigned int msecs, const Ogre::Vector3 &pos)
	{
		if(_channel)
		{
			FMOD_RESULT result = _channel->getPaused(&_paused);
			if(result == FMOD_ERR_CHANNEL_STOLEN)
			{
				_channel = NULL;
				if(_lastPos)
				{
					delete _lastPos;
					_lastPos = NULL;
				}
				return;
			}

			FMOD_VECTOR fmodPos = {pos.x,pos.y,pos.z};

			if(!_lastPos)
			{
				_channel->set3DAttributes(&fmodPos, NULL);
				_lastPos = new Ogre::Vector3(pos);
			}
			else
			{
				FMOD_VECTOR fmodVel;

				fmodVel.x = (pos.x - _lastPos->x) * ( 1000 / msecs);
				fmodVel.y = (pos.y - _lastPos->y) * ( 1000 / msecs);
				fmodVel.z = (pos.z - _lastPos->z) * ( 1000 / msecs);

				_lastPos->x = fmodPos.x;
				_lastPos->y = fmodPos.y;
				_lastPos->z = fmodPos.z;

				_channel->set3DAttributes(&fmodPos, &fmodVel);
			}
		}
	}

}
