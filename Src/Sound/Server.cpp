/**
*/

#include "Server.h"
#include "Sound.h"
#include "MusicPlayer.h"
#include "BaseSubsystems/Server.h"
#include "BaseSubsystems/Math.h"
#include "BaseSubsystems/System.h"

#include <assert.h>
#include <fmod_errors.h>
#include <fmod_studio.hpp>
#include <fmod_studio_common.h>
#include <fmod_common.h>

#include "Application/ApplicationCommon.h"


namespace Sonido
{

	const float DISTANCEFACTOR = 1.0f;

	//--------------------------------------------------------


	CServer *CServer::_instance = NULL;


	CServer::CServer() : _musicPlayer(NULL), _system(NULL), _lowLevelSystem(NULL)
	{
		_instance = this;
	}

	//--------------------------------------------------------

	CServer::~CServer() 
	{
		_instance = NULL;
	}

	//--------------------------------------------------------


	bool CServer::Init()
	{
		assert(!_instance && "Segunda inicialización de Sound::CServer no permitida!");
		new CServer();

		if (!_instance->open())
		{
			Release();
			return false;
		}

		return true;

	} // Init

	//--------------------------------------------------------

	void CServer::Release()
	{
		if(_instance)
		{
			_instance->close();
			delete _instance;
		}

	} // Release

	//--------------------------------------------------------

	bool CServer::open()
	{
		FMOD_RESULT result;

		result = FMOD::Studio::System::create(&_system);
		if(result != FMOD_OK)
		{
			printf("FMOD error! (%d) %s\n", result, FMOD_ErrorString(result));
			assert(false && ("Error en el Create de FMod:"));
		}

		result = _system->getLowLevelSystem(&_lowLevelSystem);
		if (result != FMOD_OK)
		{
			printf("FMOD error! (%d) %s\n", result, FMOD_ErrorString(result));
			assert(false && "Error al obtener el sistema de bajo nivel");
		}

		unsigned int version;
		_lowLevelSystem->getVersion(&version);


		_lowLevelSystem->setSoftwareFormat(48000,FMOD_SPEAKERMODE_5POINT1,0);

		result = _lowLevelSystem->set3DSettings(1.0, DISTANCEFACTOR, 1.0f);
		if (result != FMOD_OK)
		{
			printf("FMOD error! (%d) %s\n", result, FMOD_ErrorString(result));
			assert(false && "Error al setear los settings 3D");
		}


		// Initialize FMOD Studio, which will also initialize FMOD Low Level
#ifdef _DEBUG
		// FMOD_STUDIO_INIT_LIVEUPDATE -> sirve para conectar con Fmod studio
		result = _system->initialize(512, FMOD_STUDIO_INIT_NORMAL |  FMOD_STUDIO_INIT_LIVEUPDATE, FMOD_INIT_NORMAL | FMOD_INIT_3D_RIGHTHANDED, 0); // OGRE es RIGHT HANDED
#else
		result = _system->initialize(512, FMOD_STUDIO_INIT_NORMAL, FMOD_INIT_NORMAL | FMOD_INIT_3D_RIGHTHANDED, 0); // OGRE es RIGHT HANDED
#endif
		if (result != FMOD_OK)
		{
			printf("FMOD error! (%d) %s\n", result, FMOD_ErrorString(result));
			assert(false && "Error en el init de FMod");
		}

		loadBanks();

		_listenerAttributes = new FMOD_3D_ATTRIBUTES();

		_musicPlayer = new CMusicPlayer(_system);

		return true;
	} // open

	//--------------------------------------------------------

	void CServer::close()
	{
		delete _musicPlayer;

		delete _listenerAttributes;
		//_system->close();
		_system->release();

		_system = NULL;
		_lowLevelSystem = NULL;
	} // close

	//--------------------------------------------------------

	bool CServer::loadBanks()
	{
		std::string pathName = SOUNDS_PATH;

		std::vector<std::string> fileList;

		BaseSubsystems::System::getFilesInDirectory(pathName,".bank",fileList);

		FMOD::Studio::Bank* stringsBank = NULL;

		pathName += "Master Bank.strings.bank";

		FMOD_RESULT result = _system->loadBankFile(pathName.c_str(), FMOD_STUDIO_LOAD_BANK_NORMAL, &stringsBank);
		pathName = SOUNDS_PATH;
		if (result != FMOD_OK)
		{
			printf("FMOD error! (%d) %s\n", result, FMOD_ErrorString(result));
			assert(false && ("Error al cargar el bank: "));
		}

		for(unsigned int i=0; i<fileList.size() ;++i)
		{
			if(fileList[i] == "Master Bank.strings.bank")
				continue;

			pathName += fileList[i];

			FMOD::Studio::Bank* bank = NULL;


			FMOD_RESULT result = _system->loadBankFile(pathName.c_str(), FMOD_STUDIO_LOAD_BANK_NORMAL, &bank);
			pathName = SOUNDS_PATH;
			if (result != FMOD_OK)
			{
				printf("FMOD error! (%d) %s\n", result, FMOD_ErrorString(result));
				assert(false && ("Error al cargar el bank: "));
			}
#ifdef _DEBUG
			else
			{
				FMOD::Studio::EventDescription** arrays = new FMOD::Studio::EventDescription*[100];

				int eventCount;
				result = bank->getEventList(arrays,100,&eventCount);

				for(int i=0; i<eventCount; ++i)
				{
					FMOD::Studio::ID eventID;
					arrays[i]->getID(&eventID);

					FMOD::Studio::EventDescription* eventDescription = NULL;
					result = _system->getEvent(&eventID, FMOD_STUDIO_LOAD_BEGIN_NOW, &eventDescription);

					char * paths = new char[100];
					int num;
					result = eventDescription->getPath(paths,100,&num);

					delete [100] paths ;

					int paramCount;
					eventDescription->getParameterCount(&paramCount);

					FMOD::Studio::ParameterInstance* paramInstance;
					FMOD_STUDIO_PARAMETER_DESCRIPTION paramDesc;

					for(int j= 0; j<paramCount;++j)
					{
						eventDescription->getParameterByIndex(j,&paramDesc);
					}
				}

				delete [100] arrays;

				FMOD::Studio::ParameterInstance* rpm = NULL;

			}
#endif
		}


		/*	FMOD::Studio::Bank* masterBank = NULL;
		FMOD_RESULT result = _system->loadBankFile(pathName.c_str(), FMOD_STUDIO_LOAD_BANK_NORMAL, &masterBank);
		if (result != FMOD_OK)
		{
		printf("FMOD error! (%d) %s\n", result, FMOD_ErrorString(result));
		assert(false && "Error en el init de FMod");
		}


		pathName = SOUNDS_PATH;

		pathName += "Master Bank.strings.bank";
		FMOD::Studio::Bank* stringsBank = NULL;
		result = _system->loadBankFile(pathName.c_str(), FMOD_STUDIO_LOAD_BANK_NORMAL, &stringsBank);
		if (result != FMOD_OK)
		{
		printf("FMOD error! (%d) %s\n", result, FMOD_ErrorString(result));
		assert(false && "Error en el init de FMod");
		}*/
		return true;
	}

	//--------------------------------------------------------

	bool CServer::unLoadBanks()
	{
		return true;
	}

	//--------------------------------------------------------

	void CServer::updateAudioListener(const Ogre::Vector3 &position, const Ogre::Vector3 &velocity, const Ogre::Vector3 &fordward)
	{
		_listenerAttributes->position.x = position.x;
		_listenerAttributes->position.y = position.y;
		_listenerAttributes->position.z = position.z;

		_listenerAttributes->velocity.x = velocity.x;
		_listenerAttributes->velocity.y = velocity.y;
		_listenerAttributes->velocity.z = velocity.z;

		_listenerAttributes->forward.x = fordward.x;
		_listenerAttributes->forward.y = fordward.y;
		_listenerAttributes->forward.z = fordward.z;
	}

	//--------------------------------------------------------

	void CServer::tick()
	{
		_lowLevelSystem->set3DListenerAttributes(0, &_listenerAttributes->position, &_listenerAttributes->velocity, &_listenerAttributes->forward, 0);
		_system->setListenerAttributes(_listenerAttributes);
		_system->update();
	}

	//--------------------------------------------------------

}
