
#ifndef __Sound_MusicPlayer_H
#define __Sound_MusicPlayer_H

#include <map>
#include <string>
#include <vector>

namespace FMOD
{
	namespace Studio
	{
		class System;
		class Bank;
		class EventInstance;
	}

	class System;
	class ChannelGroup;
	class Sound;
}


namespace Sonido
{
	class CMusicPlayer
	{
	public:
		CMusicPlayer(FMOD::Studio::System *system);
		~CMusicPlayer();

		void loadPlayListFromFile(const std::string &fileName);
		void playSong(const std::string &fileName, bool loop = false, float volume = 1.0);
		void stopMusic();
		void pauseMusic(bool paused);

	private:
		FMOD::System *_system;

		FMOD::ChannelGroup * _channelMusic;

		std::map<std::string, FMOD::Sound*> _playList;

		FMOD::Studio::Bank * _musicBank;

		std::vector<FMOD::Studio::EventInstance*> _studioMusicList;

		int _channel;
	};
}
#endif