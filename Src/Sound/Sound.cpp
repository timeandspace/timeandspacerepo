/**

*/

#include "Sound.h"
#include "Sound\Server.h"
#include <OgreVector3.h>
#include <assert.h>
#include <string>
#include <fmod_studio.hpp>
#include <fmod_studio_common.h>
#include <fmod_errors.h>

#include "Application/ApplicationCommon.h"



namespace Sonido
{
	bool _channelFinnished = false;

	FMOD_RESULT F_CALLBACK channelControlEnd(
		FMOD_CHANNEL *channel,
		FMOD_CHANNELCONTROL_TYPE controltype, 
		FMOD_CHANNELCONTROL_CALLBACK_TYPE type,
		void *commanddata1,
		void *commanddata2
		)
	{
		/*if(type == FMOD_CHANNELCONTROL_CALLBACK_TYPE::FMOD_CHANNELCONTROL_CALLBACK_END)
		_channelFinnished = true;*/
		((FMOD::Channel *)channel)->setCallback(NULL);
		return FMOD_OK;
	}

	FMOD_CHANNELCONTROL_CALLBACK callback = (FMOD_CHANNELCONTROL_CALLBACK)&channelControlEnd;



	//--------------------------------------------------------

	CSound::CSound(const std::string &fileName, bool loop, bool is3D, float min, float max) : 
		_channel(NULL), 
		_paused(false), 
		_lastPos(NULL), 
		_loop(loop),
		_sound(NULL),
		_eventSound(NULL)
	{
		CServer::getSingletonPtr()->getSystem()->getLowLevelSystem(&_lowLevelSystem);
		std::string pathName = SOUNDS_PATH;
		pathName += fileName;

		FMOD_RESULT result = _lowLevelSystem->createSound(pathName.c_str(), FMOD_3D, 0, &_sound);

		if (result != FMOD_OK)
		{
			printf("FMOD error! (%d) %s\n", result, FMOD_ErrorString(result));
			assert(false && "Error al crear CSound");
		}

		if(loop)
			_sound->setMode(FMOD_LOOP_NORMAL);
		else
			_sound->setMode(FMOD_LOOP_OFF);

		//max distance por defecto 10000.0
		float maxDistance = 10000.0f;
		if (min > 0)
			_sound->set3DMinMaxDistance(min, std::max(max, maxDistance));

	}

	//--------------------------------------------------------

	CSound::CSound(const std::string &fileName, const std::string &parameterName) :
		_sound(NULL),
		_eventSound(NULL),
		_parameterInstance(NULL),
		_lastPos(NULL),
		_playing(false)
	{
		_studioSystem = CServer::getSingletonPtr()->getSystem();

		FMOD_RESULT result;

		FMOD::Studio::ID eventID = {0};
		result = _studioSystem->lookupID(fileName.c_str(), &eventID);

		FMOD::Studio::EventDescription* eventDescription = NULL;
		result = _studioSystem->getEvent(&eventID, FMOD_STUDIO_LOAD_BEGIN_NOW, &eventDescription);

		result =  eventDescription->createInstance(&_eventSound);

		bool is3D;
		eventDescription->is3D(&is3D);

		bool isOneShot;
		eventDescription->isOneshot(&isOneShot);

		if(!parameterName.empty())
			_eventSound->getParameter(parameterName.c_str(),&_parameterInstance);


		/*FMOD::Studio::Bank* bank = NULL;
		FMOD_RESULT result = _studioSystem->loadBankFile(pathName.c_str(), FMOD_STUDIO_LOAD_BANK_NORMAL, &bank);

		FMOD::Studio::EventDescription** arrays = new FMOD::Studio::EventDescription*[100];

		int count;
		bank->getEventList(arrays,100,&count);

		for(int i=0; i<count; ++i)
		{
		FMOD::Studio::ID eventID;
		arrays[i]->getID(&eventID);

		FMOD::Studio::EventDescription* eventDescription = NULL;
		result = _studioSystem->getEvent(&eventID, FMOD_STUDIO_LOAD_BEGIN_NOW, &eventDescription);

		char * paths = new char[100];
		int num;
		result = eventDescription->getPath(paths,100,&num);

		std::string str;

		for(int j= 0; j<num;++j)
		{
		str += paths[j];
		}

		result = eventDescription->createInstance(&_eventSound);

		delete [100] paths ;
		}

		delete [100] arrays;

		FMOD::Studio::ParameterInstance* rpm = NULL;
		_eventSound->getParameter("RPM", &rpm);


		if (result != FMOD_OK)
		{
		printf("FMOD error! (%d) %s\n", result, FMOD_ErrorString(result));
		assert(false && "Error al crear CSound");
		}*/
	}

	//--------------------------------------------------------

	CSound::~CSound()
	{
		if(_sound)
			_sound->release();
		if(_eventSound)
			_eventSound->release();
		if(_lastPos)
		{
			delete _lastPos;
			_lastPos = NULL;
		}
	}

	//--------------------------------------------------------

	void CSound::play()
	{
		if(_sound)
		{
			if(_loop) stop();

			_system->playSound(_sound, 0, false, &_channel);

			_channel->setCallback(callback);
		} 
		else if(_eventSound)
		{
			_eventSound->start();
			_playing = true;
		}
	}

	//--------------------------------------------------------

	void CSound::stop()
	{
		if(_sound)
		{
			if(_channel)
			{
				_channel->stop();
				_channel = NULL;
			}
		}
		else if(_eventSound)
		{
			FMOD_STUDIO_PLAYBACK_STATE state;
			_eventSound->getPlaybackState(&state);

			if(_playing)
			{
				_eventSound->stop(FMOD_STUDIO_STOP_MODE::FMOD_STUDIO_STOP_ALLOWFADEOUT);
				_playing = false;
			}
		}
	}

	//--------------------------------------------------------

	void CSound::pause(bool pause)
	{
		if(_sound)
		{
			if(_channel)
			{
				_channel->setPaused(pause);
			}
		}
		else if(_eventSound)
			_eventSound->setPaused(pause);
	}

	//--------------------------------------------------------

	void CSound::setVolume(float volume)
	{
		if(_eventSound)
			_eventSound->setVolume(volume);
	}

	//--------------------------------------------------------

	void CSound::setParameter(float parameterValue)
	{
		if(_eventSound)
			_parameterInstance->setValue(parameterValue);
	}

	//--------------------------------------------------------

	void CSound::loop(bool loop)
	{
		_loop = loop;
		if(_channel)
		{
			if(loop)
				_channel->setMode(FMOD_LOOP_NORMAL);
			else
				_channel->setMode(FMOD_LOOP_OFF);
		}
	}

	//--------------------------------------------------------

	void CSound::update(unsigned int msecs, const Ogre::Vector3 &pos)
	{
		if (msecs == 0) return;

		if(_sound)
		{
			if(_channel)
			{
				FMOD_RESULT result = _channel->getPaused(&_paused);
				if(result == FMOD_ERR_CHANNEL_STOLEN)
				{
					_channel = NULL;
					if(_lastPos)
					{
						delete _lastPos;
						_lastPos = NULL;
					}
					return;
				}

				FMOD_VECTOR fmodPos = {pos.x,pos.y,pos.z};

				if(!_lastPos)
				{
					_channel->set3DAttributes(&fmodPos, NULL);
					_lastPos = new Ogre::Vector3(pos);
				}
				else
				{
					FMOD_VECTOR fmodVel;

					fmodVel.x = (pos.x - _lastPos->x) * ( 1000 / msecs);
					fmodVel.y = (pos.y - _lastPos->y) * ( 1000 / msecs);
					fmodVel.z = (pos.z - _lastPos->z) * ( 1000 / msecs);

					_lastPos->x = fmodPos.x;
					_lastPos->y = fmodPos.y;
					_lastPos->z = fmodPos.z;

					_channel->set3DAttributes(&fmodPos, &fmodVel);
				}
			}
		}
		else if(_eventSound)
		{
			FMOD_3D_ATTRIBUTES attribs;

			attribs.position.x = pos.x;
			attribs.position.y = pos.y;
			attribs.position.z = pos.z;

			if(!_lastPos)
			{
				_lastPos = new Ogre::Vector3(pos);
			}

			attribs.velocity.x = (pos.x - _lastPos->x) * ( 1000 / msecs);
			attribs.velocity.y = (pos.y - _lastPos->y) * ( 1000 / msecs);
			attribs.velocity.y = (pos.z - _lastPos->z) * ( 1000 / msecs);

			_lastPos->x = pos.x;
			_lastPos->y = pos.y;
			_lastPos->z = pos.z;

			_eventSound->set3DAttributes(&attribs);
		}
	}

	//--------------------------------------------------------

}
