/**

*/
#ifndef __Sound_SoundBank_H
#define __Sound_SoundBank_H

#include <string>

namespace Ogre
{
	class Vector3;
}

namespace FMOD
{
	class Sound;
	class System;
	class Channel;
	class ChannelGroup;
}

namespace FMOD
{
	namespace Studio
	{
		class EventInstance;
		class System;
	}
}

namespace Sonido 
{
	class CSoundBank
	{
	public:
		CSoundBank(const std::string &fileName);

		~CSoundBank();

		FMOD::Sound *getSound() { return NULL; }

		void play();

		void stop();

		void pause(bool pause);

		void loop(bool loop);

		void update(unsigned int msecs, const Ogre::Vector3 &pos); 


	private:

	
		FMOD::Studio::EventInstance *_sound;
		FMOD::Channel *_channel;
		FMOD::Studio::System *_system;
		Ogre::Vector3 *_lastPos;
		bool _paused;
		bool _loop;
		int channelId;
	};

} // namespace Sound

#endif;