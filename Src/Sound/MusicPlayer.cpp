
#include "MusicPlayer.h"
#include <fmod_studio.hpp>

#include "Application/ApplicationCommon.h"
#include "BaseSubsystems/System.h"

namespace Sonido
{
	CMusicPlayer::CMusicPlayer(FMOD::Studio::System *system)
	{
		system->getLowLevelSystem(&_system);
		_system->createChannelGroup(NULL, &_channelMusic);
	}

	CMusicPlayer::~CMusicPlayer()
	{
		std::map<std::string, FMOD::Sound*>::iterator it = _playList.begin();
		std::map<std::string, FMOD::Sound*>::iterator end = _playList.end();
		for ( ; it != end; ++it)
		{
			(*it).second->release();
		}
		_playList.clear();
	}

	void CMusicPlayer::loadPlayListFromFile(const std::string &fileName)
	{
		FMOD::Sound *sound;	
		std::string file;

		std::string dataString;
		BaseSubsystems::System::getPathFromResource(fileName,file);

		BaseSubsystems::System::getDataStreamFromResource(fileName,dataString);// HACK de pruebas pra leer datastream de un zip
		_system->createStream(file.c_str(), FMOD_2D,0,&sound);

		_playList.insert(std::pair<std::string,FMOD::Sound*>(fileName,sound));
	}



	void CMusicPlayer::playSong(const std::string &fileName, bool loop, float volume)
	{
		FMOD::Channel *channel;
		FMOD::Sound* sound = _playList[fileName];
		_system->playSound(_playList[fileName], 0, false, &channel);
		channel->setVolume(volume);
		channel->getIndex(&_channel);
		channel->setChannelGroup(_channelMusic);
		channel->setVolumeRamp(true);
	}


	void CMusicPlayer::stopMusic()
	{
		FMOD::Channel* channel;
		if(_system->getChannel(_channel,&channel) != FMOD_OK)
			return;
		channel->stop();
	}

	void CMusicPlayer::pauseMusic(bool paused)
	{
		FMOD::Channel* channel;
		if(_system->getChannel(_channel,&channel) != FMOD_OK)
			return;
		channel->setPaused(paused);
	}

}