/**

*/
#ifndef __Sound_Sound_H
#define __Sound_Sound_H

#include <string>

namespace Ogre
{
	class Vector3;
}

namespace FMOD
{
	class Sound;
	class System;
	class Channel;
	class ChannelGroup;

	namespace Studio
	{
		class EventInstance;
		class System;
		class ParameterInstance;
	}
}

namespace Sonido 
{
	class CSound
	{
	public:
		CSound(const std::string &fileName,  bool loop, bool is3D = true, float min = -1.0, float max = -1.0);

		CSound(const std::string &fileName, const std::string &parameterName = "");

		~CSound();

		FMOD::Sound *getSound() { return _sound; }

		void play();

		void stop();

		void pause(bool pause);

		void setVolume(float volume);

		void setParameter(float parameterValue);

		void loop(bool loop);

		void update(unsigned int msecs, const Ogre::Vector3 &pos); 


	private:

	
		FMOD::Sound *_sound;
		FMOD::Studio::EventInstance *_eventSound;
		FMOD::Studio::ParameterInstance* _parameterInstance;
		FMOD::Studio::System *_studioSystem;
		FMOD::System *_lowLevelSystem;
		FMOD::Channel *_channel;
		FMOD::System *_system;
		Ogre::Vector3 *_lastPos;
		bool _paused;
		bool _playing;
		bool _loop;
		int channelId;
	};

} // namespace Sound

#endif;