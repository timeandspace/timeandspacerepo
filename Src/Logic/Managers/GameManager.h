/**
@file GameManager.h

Contiene la declaraci�n del singleton que controla la l�gica del juego entre niveles.
Mantiene datos entre reinicios de escenas, maneja el n�mero m�ximo de copias por nivel
y almacena datos que no se quieran perder entre niveles.

@author Alejandro P�rez Alonso

@date Julio, 2014
*/

#ifndef __Logic_GameManager_H
#define __Logic_GameManager_H

#include <string>
#include <unordered_map>
#include "Application/ApplicationListener.h"

namespace Logic
{

	class CGameManager : public Application::IApplicationListener
	{
	public:
		/**
		*/
		static CGameManager* getSingletonPtr();
		/**
		*/
		static bool Init();
		/**
		*/
		static void Release();

		/**
		A�adir informaci�n que no se elimine entre reinicios de niveles
		*/
		void addResetInfo(std::string name, void *value)
		{
			_resetLevelInfo[name] = value;
		}

		/**
		Obtener informaci�n
		*/
		void* getResetInfo(std::string name)
		{
			return _resetLevelInfo[name];
		}

		/**
		Comprueba si contiene informaci�n asociada a ese nombre
		*/
		bool hasResetInfo(std::string name)
		{
			std::unordered_map<std::string, void*>::const_iterator it = _resetLevelInfo.find(name);

			return (it != _resetLevelInfo.end());
		}

		/**
		Asignar un n�mero m�ximo de copias para el nivel actual
		*/
		void setMaxCopies(unsigned int value) { _maxCopies = value; }

		/**
		N�mero m�ximo de copias para el nivel actual
		*/
		unsigned int getMaxCopies() { return _maxCopies; }

		/**
		*/
		void onLoadingLevel();

		/**
		M�todo llamado al cambiar de nivel
		*/
		virtual void OnLevelLoaded();

		/**
		M�todo llamado al reiniciar un nivel
		*/
		virtual void OnLevelRestarted();

	private:
		CGameManager();
		~CGameManager();

		bool open();
		void close();

		static CGameManager *_instance;

		// hash map que contiene la informaci�n entre reinicios de nivel al crear copias
		std::unordered_map<std::string, void*> _resetLevelInfo;
		// N�mero m�ximo de copias para el nivel actual
		unsigned int _maxCopies;

	};

} // namespace Logic

#endif //__Logic_GameManager_H