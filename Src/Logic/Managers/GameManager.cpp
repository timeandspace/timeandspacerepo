/**
@file GameManager.cpp

Contiene la implementaci�n del singleton que controla la l�gica del juego entre niveles.
Mantiene datos entre reinicios de escenas, maneja el n�mero m�ximo de copias por nivel
y almacena datos que no se quieran perder entre niveles.

@author Alejandro P�rez Alonso

@date Julio, 2014
*/

#include "GameManager.h"

#include "Application/BaseApplication.h"

namespace Logic
{
	CGameManager* CGameManager::_instance = 0;

	//--------------------------------------------------------

	CGameManager::CGameManager() : _maxCopies(0)
	{
		_instance = this;

	} // CGameManager

	//--------------------------------------------------------

	CGameManager::~CGameManager()
	{

	} // ~CGameManager

	//--------------------------------------------------------

	CGameManager* CGameManager::getSingletonPtr()
	{ 
		return _instance; 
	}

	//--------------------------------------------------------

	bool CGameManager::Init()
	{
		new CGameManager();
		if (!_instance->open())
		{
			_instance->Release();
			return false;
		}
		return true;

	} // Init

	//--------------------------------------------------------

	void CGameManager::Release()
	{
		if(_instance)
		{
			_instance->close();
			delete _instance;
		}
	
	} // Release

	//--------------------------------------------------------

	bool CGameManager::open()
	{
		Application::CBaseApplication::getSingletonPtr()->addApplicationListener(this);
		return true;

	} // open

	//--------------------------------------------------------

	void CGameManager::close()
	{
		Application::CBaseApplication::getSingletonPtr()->removeApplicationListener(this);
		// Borramos toda informaci�n
		std::unordered_map<std::string, void*>::iterator it = _resetLevelInfo.begin();
		std::unordered_map<std::string, void*>::iterator end = _resetLevelInfo.end();
		while (it != end)
		{
			delete (*it).second;
			++ it;
		}
		_resetLevelInfo.clear();

	} // close

	//--------------------------------------------------------

	void CGameManager::onLoadingLevel()
	{
		std::unordered_map<std::string, void*>::iterator it = _resetLevelInfo.begin();
		std::unordered_map<std::string, void*>::iterator end = _resetLevelInfo.end();
		while (it != end)
		{
			delete (*it).second;
			++ it;
		}
		_resetLevelInfo.clear();

	} // onLoadingLevel

	//--------------------------------------------------------

	void CGameManager::OnLevelLoaded()
	{
		
	} // OnLevelLoaded

	//--------------------------------------------------------

	void CGameManager::OnLevelRestarted()
	{

	} // OnLevelRestarted

	//--------------------------------------------------------
	
}