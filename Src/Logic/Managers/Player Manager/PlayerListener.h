/**

*/
#ifndef __Logic_PlayerListener_H
#define __Logic_PlayerListener_H

#include "Logic/Entity/Component.h"
#include "Logic/Entity/Components/Save.h"
#include "GUI/InputManager.h"
#include "Application/ApplicationListener.h"


//declaración de la clase
namespace Logic 
{
	class  IPlayerListener
	{
	public:
		    IPlayerListener(){}
			virtual ~IPlayerListener(){}

			virtual void bulletChange(const std::map<std::string,int> &weapons) = 0;
			virtual void copiesChange(int num) = 0;
			virtual void armadoChange(bool armado) = 0;
			virtual void damage(float actualLife) = 0;

	};
} //namespace Logic
#endif // __Logic_PlayerListener_H