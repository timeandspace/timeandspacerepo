
#include "PlayerManager.h"
#include "PlayerListener.h"
#include "Application/BaseApplication.h"

namespace Logic
{
	CPlayerManager* CPlayerManager::_instance = 0;

	CPlayerManager::CPlayerManager() : _armado(false), _num_copies(0), _life(1.0f)
	{
		_instance = this;
	}

	CPlayerManager::~CPlayerManager()
	{
		_instance = NULL;
		clearAll();
	}

	void CPlayerManager::clearAll()
	{
		_num_bullets.clear();
		_num_copies = 0;
		_life  = 1.0f;
	}

	void CPlayerManager::addListener(IPlayerListener *listener){
		_listeners.push_back(listener);
	}

	void CPlayerManager::clearNum_Bullets()
	{
		_num_bullets.clear();
	}

	void CPlayerManager::clearNum_Copies()
	{
		_num_copies = 0;
	}

	CPlayerManager* CPlayerManager::getSingletonPtr()
	{ 
		return _instance; 
	}

	bool CPlayerManager::Init()
	{
		assert(!_instance && "Segunda inicialización de Logic::CPlayerManager no permitida!");

		new CPlayerManager();

		if(!_instance->open())
		{
			Release();
			return false;
		}
		return true;
	}

	bool CPlayerManager::Release()
	{
		assert(_instance && "Logic::CPlayerManager no inicializado!");

		if(_instance)
		{
			_instance->close();
			delete _instance;
		}
		return true;
	}

	bool CPlayerManager::open()
	{
		Application::CBaseApplication::getSingletonPtr()->addApplicationListener(this);
		setNum_Copies(_num_copies);
		return true;
	}


	void CPlayerManager::close()
	{
		Application::CBaseApplication::getSingletonPtr()->removeApplicationListener(this);
	}


	void CPlayerManager::setArmado(bool armado){
		_armado = armado;
		for(unsigned int i = 0 ; i < _listeners.size();++i){
			_listeners[i]->armadoChange(armado);
		}
	}

	bool CPlayerManager::getArmado(){
		return _armado;
	}



	int CPlayerManager::getNum_bullets(std::string value){

		return _num_bullets[value];
	}

	void CPlayerManager::setNum_bullets(std::string wepon,int bullets){

		_num_bullets.insert(std::make_pair(wepon, bullets));

		//AVISAR A LOS LISTENER
		for(unsigned int i = 0 ; i < _listeners.size();++i){
			_listeners[i]->bulletChange(_num_bullets);
		}
	}

	float CPlayerManager::getLife()
	{
		return _life;
	}

	void CPlayerManager::setLife(float value)
	{
		if(value >= 0.0f && value <= 1.0f)
		{
			_life = value;
			//AVISAR A LOS LISTENER
			for(unsigned int i = 0 ; i < _listeners.size();++i)
				_listeners[i]->damage(_life);
		}
	}



	int CPlayerManager::getNum_Copies()
	{
		return _num_copies;
	}


	void CPlayerManager::setNum_Copies(int num){

		_num_copies = num;

		for(unsigned int i = 0 ; i < _listeners.size();++i){
			_listeners[i]->copiesChange(_num_copies);
		}

	}

	void CPlayerManager::OnLevelLoaded()
	{
		_num_copies = 0;
	}

	void CPlayerManager::OnLevelRestarted()
	{

	}



}