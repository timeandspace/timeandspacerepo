/**

*/
#ifndef __Logic_PlayerManager_H
#define __Logic_PlayerManager_H

#include <map>
#include <string>
#include <vector>
#include "Application/ApplicationListener.h"

//declaración de la clase
namespace Logic 
{
	class IPlayerListener;

	class CPlayerManager : public Application::IApplicationListener
	{
	public:

		//Metodos Singleton
		static CPlayerManager* getSingletonPtr();
		static bool Init();
		static bool Release();

		//Getters
		int getNum_bullets(std::string wepon);
		int getNum_Copies();
		bool getArmado();
		float getLife();

		//Setters
		void setNum_bullets(std::string wepon,int bullets);
		void setArmado(bool armado);
		void setNum_Copies(int num);
		void setLife(float value);

		void clearNum_Copies();
		void clearNum_Bullets();
		void clearAll();
		
		void addListener(IPlayerListener *listener);

		virtual void OnLevelLoaded();

		virtual void OnLevelRestarted();


	private:
		CPlayerManager();
		~CPlayerManager();

		bool open();
		void close();

		static CPlayerManager* _instance;

		std::map<std::string, int> _num_bullets;
		int _num_copies; 
		bool _armado;
		float _life; //0.0f-1.0f

		std::vector<IPlayerListener*> _listeners; 

	};
} //namespace Logic
#endif // __Logic_PlayerManager_H