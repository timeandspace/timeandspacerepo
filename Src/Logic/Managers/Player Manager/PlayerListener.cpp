/**
@file Entity.cpp
*/

#include "PlayerListener.h"

#include "Logic/Maps/EntityFactory.h"
#include "Logic/Entity/Entity.h"
#include "Map/MapEntity.h"
#include "Logic/Server.h"
#include "Logic/Maps/Map.h"
#include "Application/BaseApplication.h"

namespace Logic
{
	CPlayerListener* CPlayerListener::_instance = 0;


	CPlayerListener::CPlayerListener() : _countCopies(0)
	{
		_instance = this;
		GUI::CInputManager::getSingletonPtr()->addKeyListener(this);
	}

	CPlayerListener::~CPlayerListener()
	{
		_instance = NULL;
		GUI::CInputManager::getSingletonPtr()->removeKeyListener(this);

		// Vaciar listas
		clearAll();
	}

	void CPlayerListener::clearAll()
	{
		// Vaciar lista de movimientos
		if (_movementLists.size() > 0)
		{
			int tam = _movementLists.size();
			for(int i = 0; i < tam; ++i)
			{
				if (_movementLists[i].size() > 0)
				{
					std::list<CMessage*>::const_iterator itr = _movementLists[i].begin();
					std::list<CMessage*>::const_iterator end = _movementLists[i].end();

					for(; itr != end; ++itr)
						(*itr)->release();
				}
			}
		}

		std::list<const Map::CEntity*>::const_iterator itr = _savedCopies.begin();
		std::list<const Map::CEntity*>::const_iterator end = _savedCopies.end();

		for(; itr != end; ++itr)
		{
			delete (*itr);
		}
		_savedCopies.clear();


		_movementLists.clear();
		_copiesList.clear();
	}

	void CPlayerListener::clearCopies()
	{
		_copiesList.clear();
		
	}


	unsigned int CPlayerListener::getCountCopies()
	{
		return _copiesList.size();
	}

	CPlayerListener* CPlayerListener::getSingletonPtr()
	{ 
		return _instance; 
	}

	bool CPlayerListener::Init()
	{
		assert(!_instance && "Segunda inicialización de Logic::CPlayerListener no permitida!");

		new CPlayerListener();

		if(!_instance->open())
		{
			Release();
			return false;
		}

		return true;
	}

	bool CPlayerListener::Release()
	{
		assert(_instance && "Logic::CPlayerListener no inicializado!");

		if(_instance)
		{
			_instance->close();
			delete _instance;
		}

		return true;
	}

	bool CPlayerListener::open()
	{
		Application::CBaseApplication::getSingletonPtr()->addApplicationListener(this);
		return true;
	}


	void CPlayerListener::close()
	{
		Application::CBaseApplication::getSingletonPtr()->removeApplicationListener(this);
	}

	void CPlayerListener::addControlledEntity(CSave* entitySaveComponent)
	{
		_controlledEntities.push_back(entitySaveComponent);
	}

	void CPlayerListener::removeControlledEntity(CSave* entitySaveComponent)
	{
		_controlledEntities.remove(entitySaveComponent);
	}

	bool CPlayerListener::keyPressed(GUI::TKey key)
	{
		CClockModifierMessage* message = NULL;
		switch(key.keyId)
		{
		case GUI::Key::C:
			createClone(Logic::CServer::getSingletonPtr()->getPlayer());	
			_controlledEntities.clear();
			Application::CBaseApplication::getSingletonPtr()->restartLevel();
			break;
		case GUI::Key::V:
			message = new CClockModifierMessage(); 
			message->_operation = "SlowDown";
			message->_clockModifier = -0.5f;
			//Logic::CServer::getSingletonPtr()->getPlayer()->emitMessage(message);
			sendMessageToCopies(message);
			break;
		case GUI::Key::B:
			message = new CClockModifierMessage(); 
			message->_operation = "SpeedUp";
			message->_clockModifier = 0.5f;
			//Logic::CServer::getSingletonPtr()->getPlayer()->emitMessage(message);
			sendMessageToCopies(message);
			break;
		case GUI::Key::R:
			backwards(Logic::CServer::getSingletonPtr()->getPlayer());	
			break;
		default:
			return false;
		}
		return true;
	}

	bool CPlayerListener::getControlledEntitySaveComponent(const CEntity* entity, CSave*& saveComponent)
	{
		//Comprobamos si la entidad esta controlada por el PlayerListener
		SavesList::const_iterator itr = _controlledEntities.begin();
		SavesList::const_iterator end = _controlledEntities.end();
		bool found = false;
		while( !found && itr != end)
		{
			if( entity == (*itr)->getEntity() )
			{
				found = true;
				saveComponent = (*itr);
			}
			++itr;
		}
		return found;
	}


	void CPlayerListener::instanceCopies()
	{
		std::list<const Map::CEntity*>::const_iterator itr = _savedCopies.begin();
		std::list<const Map::CEntity*>::const_iterator end = _savedCopies.end();

		for(; itr!=end; ++itr)
		{
			Logic::CEntity* entity = Logic::CEntityFactory::getSingletonPtr()->createEntity((*itr), Logic::CServer::getSingletonPtr()->getMap());
 			_copiesList.push_back(entity);
		}
	}


	void CPlayerListener::restartCopies()
	{
		std::list<CEntity*>::const_iterator itr = _copiesList.begin();
		std::list<CEntity*>::const_iterator end = _copiesList.end();

		int i=0;
		for(; itr != end; ++itr)
		{
			(*itr)->reset();
			CPlayBackMessage* message = new CPlayBackMessage();
			message->_messageList = _movementLists.at(i++);
			(*itr)->emitMessage(message);
		}

	}


	void CPlayerListener::sendMessageToCopies(CMessage* message)
	{
		std::list<CEntity*>::const_iterator itr = _copiesList.begin();
		std::list<CEntity*>::const_iterator end = _copiesList.end();

		for( ; itr != end; ++itr)
		{
			(*itr)->emitMessage(message);
		}
	}

	bool CPlayerListener::backwards(CEntity* entity) 
	{
		CSave* saveComponent;

		if( !getControlledEntitySaveComponent(entity, saveComponent) )
			return false;

		//Enviamos el mensaje de backwards
		CBackwardsMessage* message = new CBackwardsMessage();
		message->_time = saveComponent->getClock();
		message->_duration = 5000; // 2 sec de duracion
		message->_messageList = saveComponent->getMessageList();
		entity->emitMessage(message);
				
		// Desactivamos el componente CSave
		saveComponent->deactivate();

		return true;
	}

	void CPlayerListener::stopBackwards(CEntity* entity, unsigned long clock)
	{
		CSave* saveComponent;

		getControlledEntitySaveComponent(entity, saveComponent);

		// Eliminamos de la lista de mensajes guardado los que tengas _time > clock
		// clock == 'tiempo actual tras rebobinar' 
		int i = 0;
		std::list<CMessage*>::const_iterator itr = saveComponent->getMessageList().begin();
		while (itr != saveComponent->getMessageList().end())
		{
			if (((CControlMessage*)(*itr))->_time > clock)
			{
				(*itr)->release();
				++ i;
			}
			++ itr;
		}
		for(; i != 0; --i)
			saveComponent->getMessageList().pop_back();

		// Asignamos el nuevo reloj
		saveComponent->setClock(clock);
		saveComponent->activate();
	}

	bool CPlayerListener::createClone(CEntity* entity)
	{

		CSave* saveComponent;
		//Se comprueba que la entidad de la que se quiere hacer un clon tiene
		//sus mensajes grabados y esta siendo controlada por el Replay manager
		if( !getControlledEntitySaveComponent(entity, saveComponent) )
			return false;

		// Obtenemos el prefab
		Map::CEntity* entityInfo = Logic::CEntityFactory::getSingletonPtr()->getMapEntityByType(entity->getType()  + "Clone"); 
		if(!entityInfo)
			return false;

		//Hacemos una copia del map
		entityInfo = new Map::CEntity(*entityInfo);

		//Comprobamos si existe
		if(!CEntityFactory::getSingletonPtr()->typeExist(entityInfo->getType()))
		{
			delete entityInfo;
			return false;
		}

		// Cambiamos el nombre a la nueva entidad
		std::stringstream ss;
		ss << entityInfo->getName() << ++_countCopies;	
		entityInfo->setName(ss.str());

		
		entityInfo->setPositionAttribute(entity->getInitialPos());
		entityInfo->setYawAttribute(entity->getInitialYaw());


		_savedCopies.push_back(entityInfo);
		// Guardamos la lista de movimientos, añadimos un mensaje de stopAll
		// para evitar que las copias se queden moviendose indefinidamente
		// cuando se acaba la lista de movimientos.
		CControlMessage *msg = new CControlMessage();
		msg->_string = "stopAll";
		msg->_time = saveComponent->getClock();
		msg->increaseRef();
		saveComponent->getMessageList().push_back(msg);
		_movementLists.push_back(copyMessageList(saveComponent->getMessageList()));

		return true;
	}

	std::list<CMessage*> CPlayerListener::copyMessageList(std::list<CMessage*> tmp)
	{
		std::list<CMessage*>::const_iterator itr = tmp.begin();
		std::list<CMessage*>::const_iterator end = tmp.end();


		std::list<CMessage*> list;

		for(; itr!=end; ++itr)
		{
			CMessage* messageBase = (*itr);
			CControlMessage* message = (CControlMessage*) messageBase;

			message = new CControlMessage(*message);

			list.push_back(message);
		}
		

		return list;
	}

	void CPlayerListener::OnLevelLoaded()
	{
		clearAll();
	}

	void CPlayerListener::OnLevelRestarted()
	{
		clearCopies();
		instanceCopies();
		restartCopies();
	}
}