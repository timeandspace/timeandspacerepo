/**
@file ReplayManager.h

@author �lvaro Bl�zquez Checa
@author Alejandro P�rez Alonso

@date Marzo, 2014
*/
#ifndef __Logic_ReplayManager_H
#define __Logic_ReplayManager_H

#include "Logic/Entity/Component.h"
#include "Logic/Entity/Components/Save.h"
#include "GUI/InputManager.h"
#include "Application/ApplicationListener.h"

//declaraci�n de la clase
namespace Logic 
{
	class CReplayManager :  public GUI::CKeyboardListener ,public Application::IApplicationListener
	{
		public:
			
			//Metodos Singleton
			static CReplayManager* getSingletonPtr();
			static bool Init();
			static bool Release();

			std::list<CEntity*> &getCopiesList() { return _copiesList; }

			bool keyPressed(GUI::TKey key);
			
			void addControlledEntity(CSave*);
			void removeControlledEntity(CSave*);

			virtual void OnLevelLoaded();

			virtual void OnLevelRestarted();

			unsigned int getCountCopies();

			void stopBackwards(CEntity*, unsigned long);

			void replay();
		private:
			CReplayManager();
			~CReplayManager();

			bool open();

			/**
			Segunda fase de la destrucci�n del objeto. Sirve para hacer liberar 
			los recursos de la propia instancia, la liberaci�n de los recursos 
			est�ticos se hace en Release().
			*/
			void close();

			bool getControlledEntitySaveComponent(const CEntity*, CSave*&);
			void restartCopies();
			void sendMessageToCopies(CMessage*);

			std::list<CMessage*> copyMessageList(std::list<CMessage*> tmp);
			bool backwards(CEntity*);

			void clearAll();

			void clearCopies();

			void instanceCopies();

			bool createClone(CEntity*);

			//------------- Atributos ---------------
			typedef std::list<CSave*> SavesList;
			SavesList _controlledEntities;
			

			std::list<CEntity*> _copiesList;
			static CReplayManager* _instance;
			
			std::list<const Map::CEntity*> _savedCopies;

			unsigned int _countCopies;

			typedef std::vector<std::list<CMessage*>> MovementLists;
			MovementLists _movementLists;
	};
} //namespace Logic
#endif // __Logic_ReplayManager_H