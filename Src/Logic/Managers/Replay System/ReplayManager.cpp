/**
@file ReplayManager.cpp

@author �lvaro Bl�zquez Checa
@author Alejandro P�rez Alonso

@date Marzo, 2014
*/

#include "ReplayManager.h"


#include "Logic/Managers/Player Manager/PlayerManager.h"
#include "Logic/Maps/EntityFactory.h"
#include "Logic/Entity/Entity.h"
#include "Map/MapEntity.h"
#include "Logic/Server.h"
#include "Logic/Maps/Map.h"
#include "Application/BaseApplication.h"
#include "Graphics/Server.h"
#include "Logic/Managers/GameManager.h"


namespace Logic
{
	CReplayManager* CReplayManager::_instance = 0;


	CReplayManager::CReplayManager() : _countCopies(0)
	{
		_instance = this;
		GUI::CInputManager::getSingletonPtr()->addKeyListener(this);
	}

	CReplayManager::~CReplayManager()
	{
		_instance = NULL;
		GUI::CInputManager::getSingletonPtr()->removeKeyListener(this);

		// Vaciar listas
		clearAll();
	}

	void CReplayManager::clearAll()
	{
		// Vaciar lista de movimientos
		if (_movementLists.size() > 0)
		{
			int tam = _movementLists.size();
			for(int i = 0; i < tam; ++i)
			{
				if (_movementLists[i].size() > 0)
				{
					std::list<CMessage*>::const_iterator itr = _movementLists[i].begin();
					std::list<CMessage*>::const_iterator end = _movementLists[i].end();

					for(; itr != end; ++itr)
						(*itr)->release();
				}
			}
		}

		std::list<const Map::CEntity*>::const_iterator itr = _savedCopies.begin();
		std::list<const Map::CEntity*>::const_iterator end = _savedCopies.end();

		for(; itr != end; ++itr)
		{
			delete (*itr);
		}
		_savedCopies.clear();


		_movementLists.clear();
		_copiesList.clear();
	}

	void CReplayManager::clearCopies()
	{
		_copiesList.clear();

	}


	unsigned int CReplayManager::getCountCopies()
	{
		return _copiesList.size();
	}

	CReplayManager* CReplayManager::getSingletonPtr()
	{ 
		return _instance; 
	}

	bool CReplayManager::Init()
	{
		assert(!_instance && "Segunda inicializaci�n de Logic::CReplayManager no permitida!");

		new CReplayManager();

		if(!_instance->open())
		{
			Release();
			return false;
		}

		return true;
	}

	bool CReplayManager::Release()
	{
		assert(_instance && "Logic::CReplayManager no inicializado!");

		if(_instance)
		{
			_instance->close();
			delete _instance;
		}

		return true;
	}

	bool CReplayManager::open()
	{
		Application::CBaseApplication::getSingletonPtr()->addApplicationListener(this);
		return true;
	}


	void CReplayManager::close()
	{
		Application::CBaseApplication::getSingletonPtr()->removeApplicationListener(this);
	}

	void CReplayManager::addControlledEntity(CSave* entitySaveComponent)
	{
		_controlledEntities.push_back(entitySaveComponent);
	}

	void CReplayManager::removeControlledEntity(CSave* entitySaveComponent)
	{
		_controlledEntities.remove(entitySaveComponent);
	}

	void CReplayManager::replay()
	{
		if (_countCopies < CGameManager::getSingletonPtr()->getMaxCopies())
		{
			if (_controlledEntities.size() > 0)
			{
				createClone(Logic::CServer::getSingletonPtr()->getPlayer());	
				_controlledEntities.clear();
				Application::CBaseApplication::getSingletonPtr()->restartLevel();
				Logic::CPlayerManager::getSingletonPtr()->setNum_Copies(Logic::CPlayerManager::getSingletonPtr()->getNum_Copies()+1);
			}
		}
		else 
		{
			/* 
			TODO::Si el numero de copias es mayor que el tama�o m�ximo, habr�a dos opciones
			- Eliminar la copia m�s antigua
			- Poner alg�n estado de game over con un bot�n de retry.
			*/
			Application::CBaseApplication::getSingletonPtr()->restartLevel();
		}
	}

	bool CReplayManager::keyPressed(GUI::TKey key)
	{
		//CClockModifierMessage* message = NULL;
		//switch(key.keyId)
		//{
		//case GUI::Key::C:
		//	//replay();
		//	break;
		//case GUI::Key::G:
		//	// Reinicia el nivel sin crear una copia
		//	Application::CBaseApplication::getSingletonPtr()->restartLevel();
		//	break;
		//case GUI::Key::V:
		//	message = new CClockModifierMessage(); 
		//	message->_operation = "SlowDown";
		//	message->_clockModifier = -0.5f;
		//	//Logic::CServer::getSingletonPtr()->getPlayer()->emitMessage(message);
		//	sendMessageToCopies(message);
		//	break;
		//case GUI::Key::B:
		//	message = new CClockModifierMessage(); 
		//	message->_operation = "SpeedUp";
		//	message->_clockModifier = 0.5f;
		//	//Logic::CServer::getSingletonPtr()->getPlayer()->emitMessage(message);
		//	sendMessageToCopies(message);
		//	break;
		//case GUI::Key::R:
		//	//backwards(Logic::CServer::getSingletonPtr()->getPlayer());	
		//	break;
		//default:
			return false;
		//}
		//return true;
	}

	bool CReplayManager::getControlledEntitySaveComponent(const CEntity* entity, CSave*& saveComponent)
	{
		//Comprobamos si la entidad esta controlada por el ReplayManager
		SavesList::const_iterator itr = _controlledEntities.begin();
		SavesList::const_iterator end = _controlledEntities.end();
		bool found = false;
		while( !found && itr != end)
		{
			if( entity == (*itr)->getEntity() )
			{
				found = true;
				saveComponent = (*itr);
			}
			++itr;
		}
		return found;
	}


	void CReplayManager::instanceCopies()
	{
		std::list<const Map::CEntity*>::const_iterator itr = _savedCopies.begin();
		std::list<const Map::CEntity*>::const_iterator end = _savedCopies.end();

		CEntityFactory *entFactory = CEntityFactory::getSingletonPtr();
		CServer *server = CServer::getSingletonPtr();
		for(; itr!=end; ++itr)
		{
			Logic::CEntity* entity = entFactory->createEntity((*itr), server->getMap());
			entity->activate();
			//Graphics::CServer::getSingletonPtr()->setMaterial(entity->getName(), "marineClone");
			_copiesList.push_back(entity);
		}
	}


	void CReplayManager::restartCopies()
	{
		std::list<CEntity*>::const_iterator itr = _copiesList.begin();
		std::list<CEntity*>::const_iterator end = _copiesList.end();

		int i=0;
		for(; itr != end; ++itr)
		{
			(*itr)->reset();
			CPlayBackMessage* message = new CPlayBackMessage();
			message->_messageList = _movementLists.at(i++);
			(*itr)->emitMessage(message);
		}

	}


	void CReplayManager::sendMessageToCopies(CMessage* message)
	{
		std::list<CEntity*>::const_iterator itr = _copiesList.begin();
		std::list<CEntity*>::const_iterator end = _copiesList.end();

		for( ; itr != end; ++itr)
		{
			(*itr)->emitMessage(message);
		}
	}

	bool CReplayManager::backwards(CEntity* entity) 
	{
		CSave* saveComponent;

		if( !getControlledEntitySaveComponent(entity, saveComponent) )
			return false;

		//Enviamos el mensaje de backwards
		CBackwardsMessage* message = new CBackwardsMessage();
		message->_time = saveComponent->getClock();
		message->_duration = 5000; // 2 sec de duracion
		message->_messageList = saveComponent->getMessageList();
		entity->emitMessage(message);

		// Desactivamos el componente CSave
		saveComponent->deactivate();

		return true;
	}

	void CReplayManager::stopBackwards(CEntity* entity, unsigned long clock)
	{
		CSave* saveComponent;

		getControlledEntitySaveComponent(entity, saveComponent);

		// Eliminamos de la lista de mensajes guardado los que tengas _time > clock
		// clock == 'tiempo actual tras rebobinar' 
		int i = 0;
		std::list<CMessage*>::const_iterator itr = saveComponent->getMessageList().begin();
		while (itr != saveComponent->getMessageList().end())
		{
			if (((CControlMessage*)(*itr))->_time > clock)
			{
				(*itr)->release();
				++ i;
			}
			++ itr;
		}
		for(; i != 0; --i)
			saveComponent->getMessageList().pop_back();

		// Asignamos el nuevo reloj
		saveComponent->setClock(clock);
		saveComponent->activate();
	}

	bool CReplayManager::createClone(CEntity* entity)
	{

		CSave* saveComponent;
		//Se comprueba que la entidad de la que se quiere hacer un clon tiene
		//sus mensajes grabados y esta siendo controlada por el Replay manager
		if( !getControlledEntitySaveComponent(entity, saveComponent) )
			return false;

		// Obtenemos el prefab
		Map::CEntity* entityInfo = Logic::CEntityFactory::getSingletonPtr()->getMapEntityByType(entity->getType()  + "Clone"); 
		if(!entityInfo)
			return false;

		//Hacemos una copia del map
		entityInfo = new Map::CEntity(*entityInfo);

		//Comprobamos si existe
		if(!CEntityFactory::getSingletonPtr()->typeExist(entityInfo->getType()))
		{
			delete entityInfo;
			return false;
		}

		// Cambiamos el nombre a la nueva entidad
		std::stringstream ss;
		ss << entityInfo->getName() << ++_countCopies;	
		entityInfo->setName(ss.str());


		entityInfo->setPositionAttribute(entity->getInitialPos());
		entityInfo->setYawAttribute(entity->getInitialYaw());


		_savedCopies.push_back(entityInfo);
		// Guardamos la lista de movimientos, a�adimos un mensaje de stopAll
		// para evitar que las copias se queden moviendose indefinidamente
		// cuando se acaba la lista de movimientos.
		CControlMessage *msg = new CControlMessage();
		msg->_avatarAction = TAvatarActions::STOP_ALL;
		msg->_time = saveComponent->getClock();
		msg->increaseRef();
		saveComponent->getMessageList().push_back(msg);
		_movementLists.push_back(copyMessageList(saveComponent->getMessageList()));

		return true;
	}

	std::list<CMessage*> CReplayManager::copyMessageList(std::list<CMessage*> tmp)
	{
		std::list<CMessage*>::const_iterator itr = tmp.begin();
		std::list<CMessage*>::const_iterator end = tmp.end();


		std::list<CMessage*> list;

		for(; itr!=end; ++itr)
		{
			CMessage* messageBase = (*itr);
			CControlMessage* message = (CControlMessage*) messageBase;

			message = new CControlMessage(*message);

			list.push_back(message);
		}


		return list;
	}

	void CReplayManager::OnLevelLoaded()
	{
		_countCopies = 0;
		clearAll();
	}

	void CReplayManager::OnLevelRestarted()
	{
		clearCopies();
		instanceCopies();
		restartCopies();
	}
}