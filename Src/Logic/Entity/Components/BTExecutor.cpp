#include "BTExecutor.h"

#include "AI/NodeLA.h"

#include "AI/SimpleLatentActions.h"
#include "AI/LAEntityPerceived.h"
#include "AI/LAMoveToPoint.h"
//#include "AI/NodeLACondition.h"
#include "AI/LARandomTurn.h"
#include "AI/LARouteToPoint.h"
#include "AI/NodePriorityInterrupt.h"

#include "AI/Server.h"

#include "AI/BehaviorExecutionContext.h"
#include "AI/GaleonBehaviorTrees.h"

#include "Map/MapEntity.h"

namespace Logic
{

	IMP_FACTORY(CBTExecutor);

	//---------------------------------------------------------

	CBTExecutor::CBTExecutor() : _bt(NULL) , _context(NULL)
	{

	} 

	//---------------------------------------------------------

	CBTExecutor::~CBTExecutor(void)
	{ 
		if(_context != NULL)
			delete _context;

		if (_bt != NULL) 
			delete _bt;

	}

	//---------------------------------------------------------

	bool CBTExecutor::spawn(CEntity* entity, CMap *map, const Map::CEntity *entityInfo)
	{
		if(!IComponent::spawn(entity,map,entityInfo))
			return false;

		// Para que el BT tenga acceso a los datos de la entidad a�adimos una referencia en el contexto
		// Para eso usamos el m�todo setUserData de _context. 
		// El nombre de atributo que usaremos por convenio es "entity"

		if(entityInfo->hasAttribute("behaviour"))
		{
			std::string behaviourFile = entityInfo->getStringAttribute("behaviour");
			_bt = AI::CServer::getSingletonPtr()->parseTree(behaviourFile);

			std::string contextFile = entityInfo->getStringAttribute("contextFile");

			_context = AI::CServer::getSingletonPtr()->parseContext(contextFile);

			_context->setUserData("entity", _entity);

			_bt->useContext(_context);
		}

		return true;

	}

	//---------------------------------------------------------

	void CBTExecutor::tick(unsigned int msecs)
	{
		// Invocar al m�todo de la clase padre (IMPORTANTE)
		IComponent::tick(msecs);


		// Lo primero que hay que hacer es pedir al _bt que se ejecute (con su m�todo execute)
		// Execute recibe un par�metro agent, pero no necesitamos pasarle nada (pasamos NULL) porque
		// toda la informaci�n que usar� el BT est� en el contexto de ejecuci�n
		if (_bt)
		{
			AI::BEHAVIOR_STATUS status = _bt->execute(NULL);

#ifdef _DEBUG
			//std::cout << (status == AI::BT_RUNNING ? "RUNNING" : (status == AI::BT_FAILURE ? "FAILURE" : "SUCCES")) << std::endl;
#endif

			// Puede darse el caso de que despu�s de este tick el BT haya terminado de ejecutarse 
			// (tendremos que consultar el estado que nos ha devuelvto execute)
			// En este caso concreto, queremos que el comportamiento se ejecute en un bucle infinito
			// as� que, si el estado devuelto es de terminaci�n reinicializaremos el �rbol (m�todo init)
			if (status != AI::BT_RUNNING)
				_bt->init(NULL);
		}
	}

	//---------------------------------------------------------

	bool CBTExecutor::accept(CMessage *message)
	{
		// El m�todo accept delega en el accept del BT
		// Los mensajes se aceptan si el �rbol dice que se deben aceptar
		if (_bt == NULL) return false;
		TAvatarActions action = (TAvatarActions)0;
		if (message->getType() == Message::CONTROL)
		{
			action = ((CControlMessage *)message)->_avatarAction;
		}
		return _bt->accept(message) || action == TAvatarActions::DEATH;
	}

	//---------------------------------------------------------

	void CBTExecutor::process(CMessage *message)
	{	
		// El m�todo process delega en el accept del BT
		// Los mensajes los procesa el �rbol
		if (message->getType() == Message::CONTROL)
		{
			_bt->init(NULL);
			delete _bt;
			_bt = NULL;
		}
		if (_bt != NULL)
			_bt->process(message);
	}


} // namespace Logic
