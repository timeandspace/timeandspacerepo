/**
@file LifeTime.h

Contiene la declaraci�n de una clase que elimina la entidad
pasados x segundos definidos en el map.txt

@author Alejandro P�rez Alonso
@date Marzo, 2014
*/

#ifndef __Logic_LifeTime_H
#define __Logic_LifeTime_H

#include "Logic/Entity/Component.h"

namespace Logic
{

	/**
	Clase CLifeTime
	*/
	class CLifeTime : public IComponent
	{
		DEC_FACTORY(CLifeTime);
	public:

		/**
		*/
		CLifeTime() : IComponent(), _lifeTime(5000)
		{
		}

		/**
		*/
		~CLifeTime() {}

		/**
		*/
		virtual bool spawn(CEntity* entity, CMap *map, const Map::CEntity *entityInfo);

		/**
		*/
		virtual void tick(unsigned int msecs);

	protected:
		
		/** 
		N�mero de balas
		*/
		int _lifeTime;

	}; // Class CShooter

	REG_FACTORY(CLifeTime);

} // namespace Logic

#endif // __Logic_LifeTime_H