/**
@file StickyFloor.cpp

Contiene la implementacion del componente que envia un mensaje a otra
entidad cuando recibe un mensaje TOUCHED.

@see Logic::IComponent

@author The Time and The Space
@date Marzo, 2014
*/

#include "StickyFloor.h"

#include "Logic/Entity/Entity.h"
#include "Logic/Maps/Map.h"
#include "Map/MapEntity.h"
#include "Graphics/Server.h"
#include "Graphics/Entity.h"


namespace Logic 
{
	IMP_FACTORY(CStickyFloor);

	//---------------------------------------------------------

	bool CStickyFloor::spawn(CEntity *entity, CMap *map, const Map::CEntity *entityInfo) 
	{
		if(!IComponent::spawn(entity,map,entityInfo))
			return false;

		_targetName = entityInfo->getStringAttribute("target");

		return true;

	} // spawn

	//---------------------------------------------------------

	bool CStickyFloor::activate()
	{
		IComponent::activate();

		_target = _entity->getMap()->getEntityByName(_targetName);
		if (!_target) return false;

		_target->attachEntity(_entity);

		return true;

	} // activate
	
	//---------------------------------------------------------

	void CStickyFloor::deactivate()
	{
		IComponent::deactivate();
		_target = 0;

	} // deactivate
	
	//---------------------------------------------------------

	bool CStickyFloor::accept(CMessage *message)
	{
		return message->getType() == Message::TOUCHED 
			|| message->getType() == Message::UNTOUCHED;

	} // accept

	//---------------------------------------------------------

	void CStickyFloor::process(CMessage *message)
	{
		CEntity *ent = NULL;
		switch(message->getType())
		{
		case Message::TOUCHED: // attach entity
			ent = static_cast<CTouchedMessage *>(message)->_entity;
			if (_entity != ent)
				_target->attachEntity(ent);
			break;
		case Message::UNTOUCHED: // deattach entity
			ent = static_cast<CUnTouchedMessage *>(message)->_entity;
			_target->detachEntity(ent);
			break;
		}

	} // process

	//---------------------------------------------------------

	void CStickyFloor::tick(unsigned int msecs)
	{
		IComponent::tick(msecs);

		Vector3 aux = _target->getPosition();
		Vector3 aux2 = _entity->getPosition();
		_entity->setPosition(_target->getPosition());
		aux2 = _entity->getPosition();
	}


} // namespace Logic
