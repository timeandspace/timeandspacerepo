/**
@file BulletTrigger.cpp

Contiene la implementacion del componente que envia un mensaje a otra
entidad cuando recibe un mensaje TOUCHED.

@see Logic::IComponent

@author The Time & The Space
@date Febrero, 2014
*/

#include "BulletTrigger.h"

#include "Logic/Entity/Entity.h"
#include "Logic/Maps/Map.h"
#include "Map/MapEntity.h"


namespace Logic 
{
	IMP_FACTORY(CBulletTrigger);

	//---------------------------------------------------------

	bool CBulletTrigger::spawn(CEntity *entity, CMap *map, const Map::CEntity *entityInfo) 
	{
		if(!IComponent::spawn(entity,map,entityInfo))
			return false;

		// Tipo de modificador de tiempo que se aplicar�
		std::string operation  = entityInfo->getStringAttribute("operation");
		if (!operation.compare("slow_down"))
			_operationType = TWeaponType::SLOW_DOWN;
		else if (!operation.compare("speed_up"))
			_operationType = TWeaponType::SPEED_UP;
		else if (!operation.compare("backwards"))
			_operationType = TWeaponType::BACKWARD;

		_modifier = entityInfo->getFloatAttribute("clock_modifier");
		_duration = entityInfo->getIntAttribute("duration");

		return true;

	} // spawn

	//---------------------------------------------------------

	bool CBulletTrigger::accept(CMessage *message)
	{
		return message->getType() == Message::TOUCHED;

	} // accept

	//---------------------------------------------------------

	void CBulletTrigger::process(CMessage *message)
	{
		CEntity *ent = NULL;
		switch(message->getType())
		{
		case Message::TOUCHED:

			CEntity *ent = static_cast<CTouchedMessage *>(message)->_entity;
			if (ent)
			{
				CEditTimeMessage *m =  new CEditTimeMessage((TWeaponType)_operationType, 
					_modifier, _duration);
				ent->emitMessage(m);

				std::cout << ent->getName() << "Touched!!" << std::endl;
			}

			// Marcar entidad para que sea borrada
			Logic::CEntityFactory::getSingletonPtr()->deferredDeleteEntity(_entity);
			break;
		}

	} // process


} // namespace Logic

