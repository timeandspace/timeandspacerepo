/**
@file Editable.cpp

Contine la implementaci�n del componente que permite que se
pueda modificar el tick de una entidad (ralentizar, acelerar,
hacia atr�s).

@see Logic::IComponent

@author Alejandro P�rez Alonso
@date Febrero, 2014
*/

#include "Editable.h"

#include "Logic/Entity/Entity.h"
#include "Map/MapEntity.h"
#include "Physics/Server.h"

namespace Logic
{
	IMP_FACTORY(CEditable);

	//---------------------------------------------------------
	bool CEditable::spawn(CEntity *entity, CMap *map, const Map::CEntity *entityInfo)
	{
		if (!IComponent::spawn(entity, map, entityInfo))
			return false;

		_msecsTick = 50; //HACK::

		return true;

	} // spawn

	//---------------------------------------------------------

	bool CEditable::activate()
	{
		return IComponent::activate();

	} // activate

	//---------------------------------------------------------

	void CEditable::deactivate()
	{
		return IComponent::deactivate();

	} // deactivate

	//---------------------------------------------------------

	bool CEditable::accept(CMessage *message)
	{
		return message->getType() == Message::EDIT_TIME;

	} // accept

	//---------------------------------------------------------

	void CEditable::process(CMessage *message)
	{

		if (message->getType() ==  Message::EDIT_TIME)
		{	
			// Duracion del poder
			int duration = static_cast<CEditTimeMessage *>(message)->_duration;

			// Slow Down o Speed Up
			if (static_cast<CEditTimeMessage *>(message)->_operation == TWeaponType::SLOW_DOWN
				|| static_cast<CEditTimeMessage *>(message)->_operation == TWeaponType::SPEED_UP)
			{
				CClockModifierMessage *msg = new CClockModifierMessage();
				msg->_clockModifier = static_cast<CEditTimeMessage *>(message)->_clockModifier;
				_entity->emitMessage(msg);

				//duration del poder
				_clock += duration;
				_modifier += static_cast<CEditTimeMessage *>(message)->_clockModifier;
				if(_modifier <= -1) _modifier = -1.0f;
				
				_isSlowingDown = true;

			}
			// Backwards
			else if (static_cast<CEditTimeMessage *>(message)->_operation == TWeaponType::BACKWARD)
			{
				CBackwardsMessage *msg = new CBackwardsMessage();
				msg->_duration = duration;
			}
		}

	} // process

	//---------------------------------------------------------

	void CEditable::tick(unsigned int msecs)
	{
		// Invocar al m�todo de la clase padre
		IComponent::tick(msecs);
		
		if (_isSlowingDown)
		{
			_clock -= _msecsTick;
			if (_clock <= 0)
			{
				//Restart modificaciones de tiempo.
				CClockModifierMessage *msg = new CClockModifierMessage();
				static_cast<CClockModifierMessage *>(msg)->_clockModifier = -1 * _modifier;
				_entity->emitMessage(msg);
				_isSlowingDown = false;
				_modifier = 0;
			}
		}
		//else _msecsTick = msecs;

	} // tick

	//---------------------------------------------------------

} // namespace Logic
