/**
@file SoundSource.h

@author �lvaro Bl�zquez Checa
@date Marzo, 2014
*/

#ifndef __Logic_SoundSource_H
#define __Logic_SoundSource_H

#include "Logic/Entity/Component.h"

namespace Sonido
{
	class CSound;
}

namespace Logic
{
	/**
	Clase CSoundSource
	*/
	class CSoundSource : public IComponent
	{
		DEC_FACTORY(CSoundSource);
	public:

		/**
		*/
		CSoundSource() : IComponent(), _sound(NULL), _parameterValue(0.0f), _parameterStop(false)
		{
		}

		/**
		*/
		~CSoundSource();

		/**
		*/
		virtual bool spawn(CEntity* entity, CMap *map, const Map::CEntity *entityInfo);

		/**
		*/
		virtual bool activate();

		/**
		*/
		virtual void deactivate();

		/**
		*/
		virtual bool accept(CMessage *message);

		/**
		*/
		virtual void process(CMessage *message);

		/**
		*/
		virtual void tick(unsigned int msecs);


		void play();


		void stop();


		void pause(bool paused);


		void setParameter(float parameterValue);

	protected:

		Sonido::CSound *_sound;

		float _parameterValue;

		bool _parameterStop;

	}; 

	REG_FACTORY(CSoundSource);

} // namespace Logic

#endif 