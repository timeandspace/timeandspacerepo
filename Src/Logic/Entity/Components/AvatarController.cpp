/**
@file AvatarController.cpp

Contiene la implementaci�n del componente que controla el movimiento 
de la entidad.

@see Logic::CAvatarController
@see Logic::IComponent

@author �lvaro Bl�zquez Checa
@author Alejandro P�rez Alonso

@date Marzo, 2014
*/

#include "AvatarController.h"

#include "Logic/Maps/EntityFactory.h"
#include "Logic/Entity/Entity.h"
#include "Logic/Maps/map.h"
#include "Map/MapEntity.h"
#include "Map/MapParser.h"

#include "Weapon.h"
#include "Shooter.h"

#include "Graphics/Server.h"
#include "Graphics/Entity.h"
#include "Graphics/CompositorManager.h"

#include "Application/BaseApplication.h"

#include "AI/PerceptionManager.h"

#include "Logic/Managers/Player Manager/PlayerManager.h"
#include "Logic/Managers/Replay System/ReplayManager.h"


#include "Listeners\AvatarContollerListener.h"

#include "ParticleSystem.h"



namespace Logic 
{
	IMP_FACTORY(CAvatarController);

	//---------------------------------------------------------

	using AvatarActions::TAvatarActions;

	bool CAvatarController::spawn(CEntity *entity, CMap *map, const Map::CEntity *entityInfo) 
	{
		if (!IComponent::spawn(entity,map,entityInfo))
			return false;

		if (entityInfo->hasAttribute("speed"))
		{
			//Para mejor comprension le pasamos por el mapa la velocidad en m/s
			_speed = entityInfo->getFloatAttribute("speed");
			//Luego la convertimos a m/milisegundo
			_speed /= 1000;
		}
		if (entityInfo->hasAttribute("runSpeed"))
		{
			//Para mejor comprension le pasamos por el mapa la velocidad en m/s
			_runSpeed = entityInfo->getFloatAttribute("runSpeed");
			//Luego la convertimos a m/milisegundo
			_runSpeed /= 1000;
		}
		if (entityInfo->hasAttribute("jump_time"))
			_jumpTime = entityInfo->getIntAttribute("jump_time");
		if (entityInfo->hasAttribute("jump_height"))
			_jumpHeight = entityInfo->getFloatAttribute("jump_height");

		// Cadencia de disparo
		if (entityInfo->hasAttribute("fire_rate"))
			_fireRate = entityInfo->getIntAttribute("fire_rate");

		return true;
	} // spawn

	//---------------------------------------------------------

	bool CAvatarController::activate()
	{
		IComponent::activate();

		//Ponemos el punto de mira
		Logic::CPlayerManager::getSingletonPtr()->setArmado(false);

		_specialPowerFlag = false;

		_particleSystem = (CParticleSystem*)_entity->getComponent("CParticleSystem");

		if(_particleSystem)
		{
			Callback callback = MYCALLBACK(&Logic::CAvatarController::eventCallback);
			_particleSystem->setCallBack(callback);
		}

		return true;
	} // activate

	//---------------------------------------------------------

	void CAvatarController::deactivate()
	{
		if (_entity->isPlayer())
			Graphics::CCompositorManager::getSingletonPtr()->setEnabled("motionBlur", false);
		IComponent::deactivate();
	} // deactivate

	//---------------------------------------------------------

	bool CAvatarController::accept(CMessage *message)
	{
		if (_specialPowerFlag 
			&& (message->getType() == Message::CONTROL 
			|| static_cast<CSpecialControlMessage*>(message)->_avatarAction == TAvatarActions::SPECIAL_POWER1))
		{
			return false;
		}

		return ((!_death && (message->getType() == Message::CONTROL 
			|| message->getType() == Message::PLAYBACK
			|| message->getType() == Message::SPECIAL_ACTION 
			|| message->getType() == Message::SPECIAL_CONTROL
			)) 
			|| message->getType() == Message::ANIMATION_FINISHED);

	} // accept

	//---------------------------------------------------------

	void CAvatarController::process(CMessage *message)
	{
		switch(message->getType())
		{
		case Message::CONTROL:
			{
				CControlMessage *controlMessage = static_cast<CControlMessage *>(message);

				switch (controlMessage->_avatarAction)
				{
				case TAvatarActions::WALK:
					walk();
					break;
				case TAvatarActions::WALK_BACK:
					walkBack();
					break;
				case TAvatarActions::STOP_WALK:
					stopWalk();
					break;
				case TAvatarActions::RUN:
					run();
					break;
				case TAvatarActions::STOP_RUN:
					stopRun();
					break;
				case TAvatarActions::STRAFE_RIGHT:
					strafeRight();
					break;
				case TAvatarActions::STRAFE_LEFT:
					strafeLeft();
					break;
				case TAvatarActions::STOP_STRAFE:
					stopStrafe();
					break;
				case TAvatarActions::STOP_ALL:
					stopAllMovement();
					break;
				case TAvatarActions::JUMP:
					jump();
					break;
				case TAvatarActions::STOP_JUMP:
					stopJump();
					break;
				case TAvatarActions::CROUCH:
					crouch();
					break;
				case TAvatarActions::STOP_CROUCH:
					stopCrouch();
					break;
				case TAvatarActions::TURN:
					// Como el rat�n env�a un solo mensaje TURN con el yaw y pitch y lo has separado en dos para que funcione
					// para el joystick, ejecuto lo que hab�a en la funci�n inicial turn porque con esto nuevo no va el rat�n
					if (!controlMessage->_joystick)
					{
						_entity->yaw(controlMessage->_yaw);
						_entity->pitch(controlMessage->_pitch);
						turn(0); pitch(0);
					}
					else
						turn(controlMessage->_yaw);
					break;
				case TAvatarActions::PITCH:
					pitch(controlMessage->_pitch);
					break;
				case TAvatarActions::ZOOM_IN:
					zoom(true);
					break;
				case TAvatarActions::ZOOM_OUT:
					zoom(false);
					break;
				case TAvatarActions::CONTEXTUAL_ACTION:
					contextualAction();
					break;
				case TAvatarActions::FIRE:
					fire();
					break;
				case TAvatarActions::STOP_FIRE:
					stopFire();
					break;


				case TAvatarActions::DEATH:
					death();
					break;
				case TAvatarActions::CHANGE_WEAPON:
					changeWeapon(controlMessage->_scroll);
				default:
					break;
				}
				break;
			}

		case Message::SPECIAL_CONTROL:
			switch(static_cast<CSpecialControlMessage*>(message)->_avatarAction)
			{
			case TAvatarActions::SPECIAL_POWER1:
				specialPower(); // Crear copia
				break;
			case TAvatarActions::SPECIAL_POWER2:
				specialPower2(); // turbo
				break;
			case TAvatarActions::SPECIAL_POWER3:
				specialPower3(); // Reiniciar sin crear copia
				break;
			default:
				break;
			}
			break;

		case Message::RESET:
			stopAllMovement();
			break;

		case Message::SPECIAL_ACTION:
			{
				TSpecialActions specialAction = static_cast<CSpecialActionMessage *>(message)->_specialAction;
				bool enter = static_cast<CSpecialActionMessage *>(message)->_enter;
				std::string otherInfo = static_cast<CSpecialActionMessage *>(message)->_additionalInfo;
				CEntity *onTriggerEntity = static_cast<CSpecialActionMessage *>(message)->_entity;
				onTriggerEnter(specialAction, enter, otherInfo, onTriggerEntity);
				break;
			}
		case Message::ANIMATION_FINISHED:

			onAnimationFinished(static_cast<CAnimationFinishedMessage *>(message)->_animationName);
			break;
		}

	} // process

	//---------------------------------------------------------

	void CAvatarController::changeWeapon(int scroll)
	{
		if (_hasWeapon)
		{
			/*CChangeWeaponMessage *msg = new CChangeWeaponMessage(scroll);
			_entity->emitMessage(msg);*/
			// TODO::Animaci�n de cambio de arma, que no se si habr� en el futuro
			_controllerListener->changeWeapon(true);
		}
	}

	//---------------------------------------------------------

	void CAvatarController::specialPower()
	{
		_particleSystem->start();
		// Cambiamos al material que cambia el alpha
		CChangeMaterialMessage *msg = new CChangeMaterialMessage("marineAlpha");
		msg->_specialAction = true;
		_entity->emitMessage(msg, this);

		_specialPowerFlag = true;
		// Paramos la copia
		stopAllMovement();
		//desactivomos componente CSave
		((CSave *)(_entity->getComponent("CSave")))->pause();
	}

	//---------------------------------------------------------

	void CAvatarController::specialPower2()
	{
		Application::CBaseApplication *app = Application::CBaseApplication::getSingletonPtr();
		app->setMultiplier( (app->getMultiplier() % 2) + 1 );  // _multiplier igual a 1 o 2

		Graphics::CCompositorManager::getSingletonPtr()->setEnabled("motionBlur", 
			! Graphics::CCompositorManager::getSingletonPtr()->isEnabled("motionBlur"));
	}

	void CAvatarController::specialPower3()
	{
		// Reinicia el nivel sin crear una copia
		Application::CBaseApplication::getSingletonPtr()->restartLevel();
	}

	//---------------------------------------------------------

	void CAvatarController::zoom(bool active)
	{
		if (_hasWeapon)
		{
			/*if (!_shooting) 
			{
			if (_walking) walk();
			else if (_walkingBack) walkBack();
			else
			{

			}
			}*/
			_shootingMode = active ? _shootingMode = TCameraType::AIMING :
				TCameraType::WITH_WEAPON;
		if (_hasWeapon && _entity->isPlayer())
		{
			CCameraMessage *msg = new CCameraMessage((TCameraType)_shootingMode);
			_entity->getMap()->getEntityByType("Camera")->emitMessage(msg);
		}
		_controllerListener->aim(active);

		}
	} // zoom

	//---------------------------------------------------------

	void CAvatarController::onAnimationFinished(TAnimations animationType)
	{
		switch(animationType)
		{
		case TAnimations::FIRE:
			stopFire();
			break;
		case TAnimations::GET_OBJECT:
			{
				CEntity *ent = _entity->getMap()->getEntityByName(_onTriggerEntity);
				if (ent)
				{
					// "Bip01 R Hand" ->habr�a que pasarlo por datos 
					// o si no nombrar los huesos de los personajes igual
					/*Graphics::CServer::getSingletonPtr()->getEntity(_entity->getName())->
					attachObjectToBone("Bip01 R Hand", _objectName);*/
					Quaternion offsetOrientation(Ogre::Degree(-110), Vector3::UNIT_Y);
					Quaternion qX(Ogre::Degree(/*45*/10), Vector3::UNIT_X);
					Quaternion qZ(Ogre::Degree(-20), Vector3::UNIT_Z);
					offsetOrientation = offsetOrientation * qX * qZ;
					Vector3 offsetPosition =  Vector3(0.4, 0, -0.05);
					CWeapon *weapon = (CWeapon *)_entity->getMap()->getEntityByName(_objectName)->getComponent("CNormalWeapon");
					if (weapon)
					{
						weapon->attachWeapon(_entity->getName(), "Bip001 R Hand", offsetOrientation, offsetPosition);
					}
					else if (weapon = (CWeapon *)_entity->getMap()->getEntityByName(_objectName)->getComponent("CTimeWeapon"))
					{
						weapon->attachWeapon(_entity->getName(), "Bip001 R Hand", offsetOrientation, offsetPosition);
					}
					// A�adimos el arma al componente Shooter
					((CShooter *)(_entity->getComponent("CShooter")))->addWeapon(weapon);


					_hasWeapon = true;
					// Borramos el trigger que habilitaba la captura del objeto
					Logic::CEntityFactory::getSingletonPtr()->deferredDeleteEntity(ent);
					_onTriggerEntity = "";

					// Desactivamos la opci�n de coger objetos.
					_specialAction = TSpecialActions::NONE;
					_objectName = "";

					// Mensaje para cambiar la camara a modo weapon
					if (_entity->isPlayer())
					{
						CCameraMessage *msg = new CCameraMessage(TCameraType::WITH_WEAPON);
						_entity->getMap()->getEntityByType("Camera")->emitMessage(msg);
					}
					/*CAnimationMessage * message = new CAnimationMessage();
					message->setType(Message::SET_ANIMATION);
					message->_animationName = Animations::IDLE_WEAPON;
					message->_bool = true;
					_entity->emitMessage(message,this);*/
					_controllerListener->hasWeapon(true);

					if (_entity->isPlayer())
						Logic::CPlayerManager::getSingletonPtr()->setArmado(true);
				}
				break;
			}
			//TODO:: Si se agarra una palanca o interruptor
			// case TAnimations:: ...
		case TAnimations::DEATH:

			// Si es el jugador reiniciamos el nivel con una nueva copia
			if (_entity->isPlayer())
			{
				//Application::CBaseApplication::getSingletonPtr()->setState("menu");
				Logic::CReplayManager::getSingletonPtr()->replay();
			}
			else
				Logic::CEntityFactory::getSingletonPtr()->deferredDeleteEntity(_entity);

		}

	} // onAnimationFinished

	//---------------------------------------------------------

	void CAvatarController::onTriggerEnter(TSpecialActions specialAction, bool enter, 
		std::string otherInfo, CEntity *ent)
	{
		if(enter)
		{
			_specialAction = specialAction;
			_objectName = otherInfo;
			_onTriggerEntity = ent->getName();
		}
		else
		{
			_specialAction = TSpecialActions::NONE;
			_objectName = "";
			_onTriggerEntity = "";
		}
	} // onTriggerEnter


	//---------------------------------------------------------

	void CAvatarController::contextualAction()
	{
		if(_specialAction == TSpecialActions::NONE)
			return;

		if (_specialAction == TSpecialActions::GET_OBJECT)
		{
			_controllerListener->getObject(true);
		}
		else if(_specialAction == TSpecialActions::ACTION)
		{
			CSpecialActionMessage* specialActionMessage = new CSpecialActionMessage();
			specialActionMessage->_specialAction = TSpecialActions::ACTION_CONFIRMED;

			(_entity->getMap()->getEntityByName(_onTriggerEntity))->emitMessage(specialActionMessage);

			_controllerListener->pushButton(true);
		}
	} // contextualAction

	//---------------------------------------------------------

	void CAvatarController::death()
	{
		if (!_death)
		{
			_walking = _walkingBack = _strafingLeft = _strafingRight = 
				_shooting = _crouching = false;
			_death = true;

			_controllerListener->death(true);

			if (_entity->getComponent("CSteeringMovement"))
				_entity->getComponent("CSteeringMovement")->deactivate();

			// LLamamos al componente CShooter para que suelte las armas
			if (_hasWeapon)
			{
				((CShooter *)(_entity->getComponent("CShooter")))->releaseWeapons();
			}
		}
	}

	//---------------------------------------------------------

	void CAvatarController::jump()
	{
		if (! _avatarState[AvatarActions::JUMP])
		{
			_avatarState[AvatarActions::JUMP] = true;

			// Enviamos mensaje de salto
			CJumpMessage * message = new CJumpMessage();
			message->_direction = Vector3::UNIT_Y;
			message->_height = _jumpHeight;
			message->_time = _jumpTime;
			_entity->emitMessage(message,this);

			_controllerListener->jumping(true);
		}
	}

	//---------------------------------------------------------

	void CAvatarController::stopJump()
	{
		_avatarState[AvatarActions::JUMP] = false;
		_controllerListener->jumping(false);
	}

	//---------------------------------------------------------

	void CAvatarController::fire()
	{
		if (_hasWeapon && _shootingMode == TCameraType::AIMING)
		{
			if (_rateFire <= 0)
			{
				_shooting = true;

				_controllerListener->shooting(true);

				// Mensaje de percepcion (emite sonido)
				CActivateSignalMessage *msg = new CActivateSignalMessage("fire", true);
				_entity->emitMessage(msg,this);

				// Enviamos mensaje de start Shooting
				CFireMessage *message = new CFireMessage();
				message->_cameraType = (TCameraType)_shootingMode;
				message->_start = true;
				_entity->emitMessage(message);

				//Cadencia de disparo
				_rateFire = 200; //1 disparo cada x msecs
			}
		}
	}

	//---------------------------------------------------------

	void CAvatarController::stopFire()
	{
		if (_hasWeapon)
		{
			_shooting = false;
			// Parar animiacion de disparo.

			_controllerListener->shooting(false);

			// Mensaje de percepcion (emite sonido)
			CActivateSignalMessage *msg = new CActivateSignalMessage("fire", false);
			_entity->emitMessage(msg,this);
		}
	}

	//--------------------------------------------------------

	void CAvatarController::crouch()
	{
		if(!_running)
		{
			_walking = _walkingBack = _strafingLeft = _strafingRight = _running = false;
			_avatarState[AvatarActions::CROUCH] = true;
			_controllerListener->crouch(true);
		}
	}

	//--------------------------------------------------------

	void CAvatarController::stopCrouch()
	{
		_avatarState[AvatarActions::CROUCH] = false;

		// Cambiamos la animaci�n 
		_controllerListener->crouch(false);
	}

	//---------------------------------------------------------

	void CAvatarController::turn(float amountX) 
	{
		_turningValue = amountX;

	} // turn 

	//---------------------------------------------------------

	void CAvatarController::pitch(float amountY)
	{
		_pitchValue = amountY;

	} // pitch

	//---------------------------------------------------------

	void CAvatarController::walk() 
	{
		//_walking = true;
		_avatarState[AvatarActions::WALK] = true;

		_controllerListener->walking(true);

		CActivateSignalMessage *msg = new CActivateSignalMessage("walk", true);
		_entity->emitMessage(msg, this);

	} // walk

	//---------------------------------------------------------

	void CAvatarController::walkBack() 
	{
		//_walkingBack = true;
		_avatarState[AvatarActions::WALK_BACK] = true;

		_controllerListener->walkingBack(true);

		CActivateSignalMessage *msg = new CActivateSignalMessage("walk", true);
		_entity->emitMessage(msg, this);

	} // walkBack

	//---------------------------------------------------------

	void CAvatarController::stopWalk() 
	{
		//_walking = _walkingBack = false;
		_avatarState[AvatarActions::WALK] = _avatarState[AvatarActions::WALK_BACK] = 
			_avatarState[AvatarActions::RUN] = false;

		_controllerListener->walking(false);
		_controllerListener->walkingBack(false);

		CActivateSignalMessage *msg = new CActivateSignalMessage("walk", false);
		_entity->emitMessage(msg, this); 
	} // stopWalk

	//---------------------------------------------------------

	void CAvatarController::strafeLeft() 
	{

		_avatarState[AvatarActions::STRAFE_LEFT] = true;

		if(_avatarState[AvatarActions::RUN]) return;

		_controllerListener->strafingLeft(true);

		CActivateSignalMessage *msg = new CActivateSignalMessage("walk", true);
		_entity->emitMessage(msg, this);

	} // walk

	//---------------------------------------------------------

	void CAvatarController::strafeRight() 
	{

		_avatarState[AvatarActions::STRAFE_RIGHT] = true;
		
		if(_avatarState[AvatarActions::RUN]) return;

		_controllerListener->strafingRight(true);

		CActivateSignalMessage *msg = new CActivateSignalMessage("walk", true);
		_entity->emitMessage(msg, this);

	} // walkBack

	//---------------------------------------------------------

	void CAvatarController::stopStrafe() 
	{
		_avatarState[AvatarActions::STRAFE_LEFT] = _avatarState[AvatarActions::STRAFE_RIGHT] = false;

		_controllerListener->strafingLeft(false);
		//_controllerListener->strafingRight(false);

		CActivateSignalMessage *msg = new CActivateSignalMessage("walk", false);
		_entity->emitMessage(msg, this);
	} // stopWalk

	//---------------------------------------------------------

	void CAvatarController::stopAllMovement()
	{
		_avatarState[AvatarActions::WALK] = _avatarState[AvatarActions::WALK_BACK] = 
			_avatarState[AvatarActions::STRAFE_LEFT] = _avatarState[AvatarActions::STRAFE_RIGHT] = 
			_avatarState[AvatarActions::RUN] = _shooting = false;

		_controllerListener->idle(true);

		CActivateSignalMessage *msg = new CActivateSignalMessage("walk", false);
		_entity->emitMessage(msg, this);

		msg = new CActivateSignalMessage("run", false);
		_entity->emitMessage(msg, this);
	}

	//---------------------------------------------------------

	void CAvatarController::run()
	{
		/*if(_avatarState[AvatarActions::STRAFE_LEFT] || _avatarState[AvatarActions::STRAFE_RIGHT])
		return;*/

		_avatarState[AvatarActions::RUN] = true;

		_controllerListener->running(true);

		CActivateSignalMessage *msg = new CActivateSignalMessage("run", true);
		_entity->emitMessage(msg, this);
	}

	//---------------------------------------------------------

	void CAvatarController::stopRun()
	{
		_avatarState[AvatarActions::RUN] = false;

		_controllerListener->running(false);

		CActivateSignalMessage *msg = new CActivateSignalMessage("run", false);
		_entity->emitMessage(msg, this);

	}

	//---------------------------------------------------------

	void CAvatarController::tick(unsigned int msecs)
	{
		IComponent::tick(msecs);

		if (_rateFire > 0)
			_rateFire -= msecs;

		if (_turningValue != 0)
		{
			_entity->yaw(_turningValue * msecs);
		}

		if (_pitchValue != 0)
		{
			_entity->pitch(Math::fromDegreesToRadians(_pitchValue * msecs * 0.02f));
		}


		if (_avatarState[AvatarActions::WALK] || _avatarState[AvatarActions::WALK_BACK] || 
			_avatarState[AvatarActions::STRAFE_LEFT] || _avatarState[AvatarActions::STRAFE_RIGHT] || 
			_avatarState[AvatarActions::RUN])
		{
			Vector3 direction(Vector3::ZERO);
			Vector3 directionStrafe(Vector3::ZERO);

			if (_avatarState[AvatarActions::WALK] || _avatarState[AvatarActions::WALK_BACK] || 
				_avatarState[AvatarActions::RUN])
			{
				direction = Math::getDirection(_entity->getYaw());
				if (_avatarState[AvatarActions::WALK_BACK])
					direction *= -1;
			}

			if (!_avatarState[AvatarActions::RUN] && 
				(_avatarState[AvatarActions::STRAFE_LEFT] || _avatarState[AvatarActions::STRAFE_RIGHT]))
			{
				directionStrafe = 
					Math::getDirection(_entity->getYaw() + Math::PI/2);
				if (_avatarState[AvatarActions::STRAFE_RIGHT])
					directionStrafe *= -1;
			}

			direction += directionStrafe;
			direction.normalise();

			setCurrentSpeed(msecs);

			direction *= msecs * _currentSpeed;

			CAvatarWalkMessage* message = new CAvatarWalkMessage();
			message->_movement = direction;
			_entity->emitMessage(message, this);
		}
		else
			_currentSpeed = 0;

	} // tick

	//---------------------------------------------------------

	void CAvatarController::setCurrentSpeed(float msecs)
	{
		float secondsElapsed = msecs / 1000.0f;
		if (_shootingMode == TCameraType::AIMING)
		{
			_currentSpeed  = _speed / 3;
		}
		else if (_avatarState[TAvatarActions::CROUCH] && 
			(_avatarState[TAvatarActions::WALK] || _avatarState[TAvatarActions::WALK_BACK] ||
			_avatarState[TAvatarActions::STRAFE_LEFT] || _avatarState[TAvatarActions::STRAFE_RIGHT]))
		{
			_currentSpeed  = _speed / 2;
		}
		else if(!_avatarState[TAvatarActions::RUN]|| _avatarState[TAvatarActions::WALK_BACK])
		{
			_currentSpeed = _speed;
		}
		else 
		{
			if(_currentSpeed < _runSpeed)
			{
				_currentSpeed += 0.03f * secondsElapsed;
				if(_currentSpeed > _runSpeed)
					_currentSpeed = _runSpeed;
			}
			else if(_currentSpeed > _runSpeed)
			{
				_currentSpeed -= 0.03f * secondsElapsed;
				if(_currentSpeed > _runSpeed)
					_currentSpeed = _runSpeed;
			}
		}		
	}

	//---------------------------------------------------------

	void CAvatarController::eventCallback(CallbackEvent callbackEvent)
	{
		switch (callbackEvent)
		{
		case PARTICLE_FINISHED:
			printf("CAvatarController particle finished");
			CReplayManager::getSingletonPtr()->replay();
			break;
		default:
			break;
		}
	}

	//---------------------------------------------------------


} // namespace Logic

