/**
@file BulletTrigger.h

Contiene la declaraci�n del componete que envia un mensaje cuando su 
entidad es tocada. El mensaje se env�a a la entidad que se ha tocado.

@see Logic::IComponent

@author Alejandro P�rez Alonso
@date Marzo, 2014
*/
#ifndef __Logic_Bullet_Trigger_H
#define __Logic_Bullet_Trigger_H

#include "Logic/Entity/Component.h"
#include "Logic/Entity/Entity.h"
#include "Logic/Maps/EntityFactory.h"

//declaraci�n de la clase
namespace Logic 
{

	class CBulletTrigger : public IComponent
	{
		DEC_FACTORY(CBulletTrigger);
	public:

		/**
		Constructor por defecto.
		*/
		CBulletTrigger() : IComponent(), _duration(0), _modifier(1.0f){}

		/**
		Inicializaci�n del componente usando la descripci�n de la entidad que hay en 
		el fichero de mapa.
		*/
		virtual bool spawn(CEntity* entity, CMap *map, const Map::CEntity *entityInfo);

		/**
		Este componente s�lo acepta mensaje de tipos TOUCHED y UNTOUCHED.
		*/
		virtual bool accept(CMessage *message);

		/**
		Al recibir mensaje TOUCHED se marca la entidad para que sea borrada.
		*/
		virtual void process(CMessage *message);

	protected:
		int _operationType; //Tipo de operacion(slowDown, SpeedUp, Backwards, etc.)
		unsigned int _duration;
		float _modifier; //Modificador para slowDown o SpeedUp

	}; // class CBulletTrigger

	REG_FACTORY(CBulletTrigger);

} // namespace Logic

#endif // __Logic_Bullet_Trigger_H
