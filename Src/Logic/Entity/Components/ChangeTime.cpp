/**

*/

#include "ChangeTime.h"

#include "Logic/Entity/Entity.h"
#include "Logic/Maps/Map.h"
#include "Logic/Maps/Entityfactory.h"
#include "Map/MapEntity.h"
#include "Application/BaseApplication.h"

namespace Logic 
{
	IMP_FACTORY(CChangeTime);

	bool CChangeTime::spawn(CEntity* entity, CMap *map, const Map::CEntity *entityInfo)
	{
		if(!IComponent::spawn(entity,map,entityInfo))
			return false;

		return true;
	}

	//---------------------------------------------------------

	bool CChangeTime::accept(CMessage *message)
	{
		return message->getType() == Message::CHANGE_TIME;
	}

	//---------------------------------------------------------

	void CChangeTime::process(CMessage *message)
	{
		if (message->getType() == Message::CHANGE_TIME)
		{
			_isActived = true;
			_timeModifier = static_cast<CChangeTimeMessage *>(message)->_modifier;
			_duration = static_cast<CChangeTimeMessage *>(message)->_duration;
			_slowDown = static_cast<CChangeTimeMessage *>(message)->_slowDown;
		}
	}

	//---------------------------------------------------------

	void CChangeTime::updateTime(unsigned int msecs)
	{
		if (_slowDown)
			_duration -= (msecs * _timeModifier);
		else
			_duration -= (msecs / _timeModifier);
	}

	//---------------------------------------------------------

	void CChangeTime::tick(unsigned int msecs)
	{
		IComponent::tick(msecs);

		if (_isActived) {
			updateTime(msecs);

			if (_duration <= 0)
			{
				_isActived = _slowDown = false;
				_timeModifier = 1;
			}

			_entity->setTimeModifier(_timeModifier);
			_entity->setSlowDown(_slowDown);
		}
	}

}
