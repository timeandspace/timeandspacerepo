/**
@file AudioListener.cpp

Contine la implementaci�n del componente AudioListener

@see Logic::IComponent

@author �lvaro Bl�zquez Checa
@date Marzo, 2014
*/

#include "AudioListener.h"

#include "Logic/Entity/Entity.h"
#include "Map/MapEntity.h"
#include "Sound/Sound.h"
#include "Sound/Server.h"

namespace Logic
{
	IMP_FACTORY(CAudioListener);

	//---------------------------------------------------------
	bool CAudioListener::spawn(CEntity *entity, CMap *map, const Map::CEntity *entityInfo)
	{
		if (!IComponent::spawn(entity, map, entityInfo))
			return false;

		_soundServer = Sonido::CServer::getSingletonPtr();

		_lastPos = _entity->getPosition();

		return true;

	} // spawn

	//---------------------------------------------------------

	bool CAudioListener::activate()
	{
		return IComponent::activate();
	} // activate

	//---------------------------------------------------------

	void CAudioListener::deactivate()
	{
		IComponent::deactivate();
	} // deactivate

	//---------------------------------------------------------

	bool CAudioListener::accept(CMessage *message)
	{
		return false;
	} // accept

	//---------------------------------------------------------

	void CAudioListener::process(CMessage *message)
	{
		

	} // process

	//---------------------------------------------------------

	void CAudioListener::tick(unsigned int msecs)
	{
		// Invocar al m�todo de la clase padre
		IComponent::tick(msecs);

		//FMOD trabaja en uds/s, en nuestro caso en metros -> m/s (NO frames/s, NO m/ms)
		Ogre::Vector3 _velocity;

		_velocity.x = (_lastPos.x - _entity->getPosition().x) * (1000 / msecs);
		_velocity.y = (_lastPos.y - _entity->getPosition().y) * (1000 / msecs);
		_velocity.z = (_lastPos.z - _entity->getPosition().z) * (1000 / msecs);


		_soundServer->updateAudioListener(_entity->getPosition(), _velocity,
			Math::getDirection(_entity->getOrientation()));

		_lastPos = _entity->getPosition();


	} // tick

	//---------------------------------------------------------

} // namespace Logic
