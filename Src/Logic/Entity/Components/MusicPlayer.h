/**
@file SoundSource.h

@author �lvaro Bl�zquez Checa
@date Marzo, 2014
*/

#ifndef __Logic_MusicPlayer_H
#define __Logic_MusicPlayer_H

#include "Logic/Entity/Component.h"

namespace Sonido
{
	class CSound;
}

namespace Logic
{
	/**
	Clase CMusicPlayer
	*/
	class CMusicPlayer : public IComponent
	{
		DEC_FACTORY(CMusicPlayer);
	public:

		/**
		*/
		CMusicPlayer() : IComponent(), _sound(NULL)
		{
		}

		/**
		*/
		~CMusicPlayer() 
		{
			if(_sound)
				delete _sound;
		}

		/**
		*/
		virtual bool spawn(CEntity* entity, CMap *map, const Map::CEntity *entityInfo);

		/**
		*/
		virtual bool activate();

		/**
		*/
		virtual void deactivate();

		/**
		*/
		virtual bool accept(CMessage *message);

		/**
		*/
		virtual void process(CMessage *message);

		/**
		*/
		virtual void tick(unsigned int msecs);


		void play();


		void stop();


		void pause(bool paused);


		void setParameter(float parameterValue);


		void setVolume(float volume);

	protected:

		Sonido::CSound *_sound;

		std::string _soundName;

	}; 

	REG_FACTORY(CMusicPlayer);

} // namespace Logic

#endif 