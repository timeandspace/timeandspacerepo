/**
@file SwitchTrigger.cpp

Contiene la declaraci�n del componente que envia un mensaje SWITCH a otra
entidad cuando recibe un mensaje TOUCHED / UNTOUCHED.

@see Logic::CSwitchTrigger
@see Logic::IComponent

@author David Llans�
@date Octubre, 2010
*/

#include "SwitchTrigger2.h"

#include "Logic/Entity/Entity.h"
#include "Logic/Maps/Map.h"
#include "Map/MapEntity.h"


namespace Logic 
{
	IMP_FACTORY(CSwitchTrigger2);

	//---------------------------------------------------------

	bool CSwitchTrigger2::spawn(CEntity *entity, CMap *map, const Map::CEntity *entityInfo) 
	{
		if(!IComponent::spawn(entity,map,entityInfo))
			return false;

		std::stringstream ss;
		int aux = 1;
		// A�adimos la posibilidad de tener mas de 1 target
		ss << "target" << aux;
		while (entityInfo->hasAttribute(ss.str()))
		{
			_targetNames.push_back(entityInfo->getStringAttribute(ss.str()));
			ss.str(""); ss.clear();
			++ aux;
			ss << "target" << aux;
		}

		ss.str("");
		ss.clear();
		
		aux = 1;
		// Opposite target, son objetivos que realizan el efecto contrario
		ss << "oppositeTarget" << aux;
		while (entityInfo->hasAttribute(ss.str()))
		{
			_opositeTargets.push_back(entityInfo->getStringAttribute(ss.str()));
			ss.str(""); ss.clear();
			++ aux;
			ss << "oppositeTarget" << aux;
		}
		ss.str(""); ss.clear(); 
		return true;

	} // spawn

	//---------------------------------------------------------

	bool CSwitchTrigger2::activate()
	{
		IComponent::activate();
		return true;

	} // activate

	//---------------------------------------------------------

	void CSwitchTrigger2::deactivate()
	{
		IComponent::deactivate();
	} // deactivate

	//---------------------------------------------------------

	bool CSwitchTrigger2::accept(CMessage *message)
	{
		return message->getType() == Message::ACTION; 

	} // accept

	//---------------------------------------------------------

	void CSwitchTrigger2::process(CMessage *message)
	{
		switch(message->getType())
		{
		case Message::ACTION:
			onTriggerEnter();
			break;
		}

	} // process

	//---------------------------------------------------------

	void CSwitchTrigger2::onTriggerEnter()
	{
		if (_targetNames.size() > 0)
		{
			CSwitchMessage* m = new CSwitchMessage();
			m->_int = 1;
			m->_activate = true;
			CEntity *ent = 0;
			for (unsigned int i = 0; i < _targetNames.size(); ++i)
			{
				ent = _entity->getMap()->getEntityByName(_targetNames[i]);
				if (ent)
					ent->emitMessage(m);
			}
		}
		// Si existe targets opuestos, se les env�a la acci�n de UnTouched
		if (_opositeTargets.size() > 0)
		{
			CSwitchMessage* msg = new CSwitchMessage();
			msg->_int = -1;
			msg->_activate = true;
			CEntity *ent = 0;
			for (unsigned int i = 0; i < _opositeTargets.size(); ++i)
			{
				ent = _entity->getMap()->getEntityByName(_opositeTargets[i]);
				if(ent)
					ent->emitMessage(msg);
			}
		}
	} // onTriggerEnter

	//---------------------------------------------------------

	void CSwitchTrigger2::tick(unsigned int msecs)
	{
		IComponent::tick(msecs);
	}

} // namespace Logic

