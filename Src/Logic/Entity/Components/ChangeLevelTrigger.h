/**
@file ForceTrigger.h

Contiene la declaraci�n del componente que envia un mensaje a otra
entidad cuando recibe un mensaje TOUCHED. Cambia de nivel cuando el
player entra en contacto.

@see Logic::CForceTrigger
@see Logic::IComponent

@author Alejandro P�rez Alonso
@date Febrero, 2014
*/
#ifndef __Logic_Change_Level_Trigger_H
#define __Logic_Change_Level_Trigger_H

#include "Logic/Entity/Component.h"

//declaraci�n de la clase
namespace Logic 
{

	class CChangeLevelTrigger : public IComponent
	{
		DEC_FACTORY(CChangeLevelTrigger);
	public:

		/**
		Constructor por defecto.
		*/
		CChangeLevelTrigger() : IComponent() {}

		/**
		Inicializaci�n del componente usando la descripci�n de la entidad que hay en 
		el fichero de mapa.
		*/
		virtual bool spawn(CEntity* entity, CMap *map, const Map::CEntity *entityInfo);

		/**
		M�todo que se invoca para activar el componente.
		*/
		virtual bool activate();

		/**
		M�todo que se invoca al desactivar el componente.
		*/
		virtual void deactivate();

		/**
		Este componente s�lo acepta mensaje de tipos TOUCHED.
		*/
		virtual bool accept(CMessage *message);

		/**
		Al recibir mensaje TOUCHED se realizar� un cambio de nivel.
		*/
		virtual void process(CMessage *message);

	protected:
		/**
		Indica el nivel que se cargar� cuando el player entre en contacto con �l.
		*/
		std::string _nextLevel;

	}; // class CChangeLevelTrigger

	REG_FACTORY(CChangeLevelTrigger);

} // namespace Logic

#endif // __Logic_Change_Level_Trigger_H
