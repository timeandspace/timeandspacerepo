/**
@file Playback.h

Contiene la declaraci�n del componente que reproduce una lista
de movimientos que recibe por mensaje.

@see Logic::CSave
@see Logic::IComponent

@author �lvaro Bl�zquez Checa
@author Alejandro P�rez Alonso

@date Febrero, 2014
*/
#ifndef __Logic_Playback_H
#define __Logic_Playback_H

#include "Logic/Entity/Component.h"

//declaraci�n de la clase
namespace Logic 
{
/**
*/
	class CPlayback : public IComponent
	{
		DEC_FACTORY(CPlayback);
	public:

		/**
		Constructor por defecto; en la clase base no hace nada.
		*/
		CPlayback() : IComponent(), _clock(0),_numMessages(0), _playing(false){}

		/**
		Destructor de la clase, libera la memoria de la lista de mensajes
		*/
		~CPlayback() 
		{
			destroyList();
		}

		/**
		Inicializaci�n del componente usando la descripci�n de la entidad que hay en 
		el fichero de mapa.
		*/
		virtual bool spawn(CEntity* entity, CMap *map, const Map::CEntity *entityInfo);

		/**
		*/
		virtual bool accept(CMessage *message);

		/**
		*/
		virtual void process(CMessage *message);

		/**
		*/
		virtual void tick(unsigned int msecs);

		/**
		*/
		void play();

		/**
		*/
		void stop();
		
		/**
		*/
		void restart();

	protected:

		void initList();

		void destroyList();

		unsigned int _clock;

		int _numMessages;

		Matrix4 _initialTransform;
		Vector3 _initialPosition;
		CMessage* _initialMessage;
		CMessageList _messageList;
		bool _playing;
		
		
	}; // class CMove

	REG_FACTORY(CPlayback);

} // namespace Logic

#endif // __Logic_Save_H
