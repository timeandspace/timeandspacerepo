/**
@file ScriptComponent.cpp

Contiene la implementación del componente que ejecuta un script
de lua.

@author Alejandro Pérez Alonso
@date Marzo, 2014
*/

#include "ScriptComponent.h"

#include "Logic/Entity/Entity.h"
#include "Logic/Maps/Map.h"
#include "Map/MapEntity.h"
#include "Application/BaseApplication.h"
#include "ScriptManager/ScriptManager.h"

namespace Logic 
{
	IMP_FACTORY(CScriptComponent);
	
	//---------------------------------------------------------

	bool CScriptComponent::spawn(CEntity *entity, CMap *map, const Map::CEntity *entityInfo) 
	{
		if(!IComponent::spawn(entity,map,entityInfo))
			return false;

		if (entityInfo->hasAttribute("scriptName"))
			_scriptName = entityInfo->getStringAttribute("scriptName");

		return true;

	} // spawn
	
	//---------------------------------------------------------

	bool CScriptComponent::activate()
	{
		IComponent::activate();

		// Ejecutamos el script al activar el componente
		ScriptManager::CScriptManager::getSingletonPtr()->executeScript(_scriptName.c_str());

		return true;
	} // activate

	//---------------------------------------------------------

	void CScriptComponent::deactivate()
	{
		IComponent::deactivate();
	} // deactivate

	//---------------------------------------------------------

	bool CScriptComponent::accept(CMessage *message)
	{
		return false;

	} // accept
	
	//---------------------------------------------------------

	void CScriptComponent::process(CMessage *message)
	{

	} // process

	//---------------------------------------------------------

	void CScriptComponent::tick(unsigned int msecs)
	{
		IComponent::tick(msecs);
	}

} // namespace Logic
