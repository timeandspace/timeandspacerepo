/**
@file ParticleSystem.cpp

Implementacion del componente que controla el comportamiento de un sistema de 
part�culas

@author Alejandro P�rez Alonso
@date Mayo, 2014
*/
#include "Graphics/ParticleSystem.h"
#include "ParticleSystem.h"

#include "Logic/Entity/Entity.h"
#include "Logic/Maps/Map.h"
#include "Map/MapEntity.h"

#include "Graphics/Scene.h"
#include "Logic/Managers/Replay System/ReplayManager.h"


namespace Logic
{
	IMP_FACTORY(CParticleSystem);

	//---------------------------------------------------------

	CParticleSystem::CParticleSystem() : IComponent(), Graphics::IParticleSystemListener(), _particleSystem(0), _oneShot(false), 
		_autoStart(false), _attached(false) , _yOffset(0.0f)
	{
	}

	//---------------------------------------------------------

	CParticleSystem::~CParticleSystem()
	{
		if (_particleSystem)
		{
			delete _particleSystem;
			_particleSystem = NULL;
		}
	}

	//---------------------------------------------------------

	bool CParticleSystem::spawn(CEntity* entity, CMap *map, const Map::CEntity *entityInfo)
	{
		if(!IComponent::spawn(entity,map,entityInfo))
			return false;

		_scene = _entity->getMap()->getScene();
		if (entityInfo->hasAttribute("oneShot"))
		{
			_oneShot = entityInfo->getBoolAttribute("oneShot");

		}
		// Creamos el sistema de part�culas
		_particleSystem = _scene->createParticleSystem(_entity->getName(), entityInfo->getStringAttribute("template"));

		assert(_particleSystem && "Error! CParticleSystem::spawn: _particleSystem es NULL\n");

		// Nos registramos como listeners
		_particleSystem->addListener(this);

		// Guardamos la duracion del efecto
		_duration = entityInfo->getFloatAttribute("duration");

		// Comprobamos si se desea attachar el efecto a otra entidad
		if (entityInfo->hasAttribute("parentName"))
		{
			_parentName = entityInfo->getStringAttribute("parentName");
			_boneName = entityInfo->getStringAttribute("boneName");
			_attached = true;
		}

		// Posicionamos el efecto
		_particleSystem->setPosition(entityInfo->getVector3Attribute("position"));
		// AutoStart
		_autoStart = entityInfo->getBoolAttribute("autoStart");

		// Scale
		if (entityInfo->hasAttribute("scale"))
		{
			_particleSystem->setScale(entityInfo->getVector3Attribute("scale"));
		}

		// Orientaci�n
		_particleSystem->yaw(entityInfo->getFloatAttribute("orientation"));

		if (entityInfo->hasAttribute("yOffset"))
		{
			_yOffset = entityInfo->getFloatAttribute("yOffset");
		}

		return true;
	}

	//---------------------------------------------------------

	bool CParticleSystem::activate()
	{
		if (_attached)
		{
			_particleSystem->attachToEntity(_parentName, _boneName);
		}

		return IComponent::activate();
	}

	//---------------------------------------------------------

	bool CParticleSystem::accept(CMessage *message)
	{

		return _particleSystem && message->getType() == Message::PARTICLE_SYSTEM;
	}

	//---------------------------------------------------------

	void CParticleSystem::process(CMessage *message)
	{
		if (message->getType() == Message::PARTICLE_SYSTEM)
		{
			float duration = static_cast<CParticleSystemMessage *>(message)->_duration;
			if (static_cast<CParticleSystemMessage *>(message)->_activate)
			{
				if (duration <= 0)
				{
					_particleSystem->start();
					CSoundMessage* message1 = new CSoundMessage();
					message1->setType(Message::PLAY_SOUND);
					_entity->emitMessage(message1, this);
				}

				else
				{
					_particleSystem->startAndStopFade(duration);
				}
			}
			else
			{
				if (duration <= 0)
					_particleSystem->stopFade();
				else
					_particleSystem->stopFade(duration);
				CSoundMessage* message1 = new CSoundMessage();
				message1->setType(Message::STOP_SOUND);
				_entity->emitMessage(message1, this);
			}
		}
	}

	//---------------------------------------------------------

	void CParticleSystem::tick(unsigned int msecs)
	{
		IComponent::tick(msecs);

		if (_particleSystem)
		{
			if (_oneShot && CReplayManager::getSingletonPtr()->getCountCopies() != 0)
			{
				_scene->destroyParticleSystem(_entity->getName());
				_particleSystem = NULL;
				return;
			}
			if (_autoStart)
			{
				CSoundMessage* message1 = new CSoundMessage();
				message1->setType(Message::PLAY_SOUND);
				_entity->emitMessage(message1, this);
				if (_duration <= 0.0f)
				{
					_particleSystem->start();
				}
				else
					_particleSystem->startAndStopFade(_duration);
				_autoStart = false;
			}

			_particleSystem->setPosition(_entity->getPosition() + Vector3(0.0f,_yOffset,0.0f));
		}
	}

	//---------------------------------------------------------

	void CParticleSystem::onStart()
	{

	}

	//---------------------------------------------------------

	void CParticleSystem::onStop()
	{
		printf("particle finished");
		if((!_callback._Empty()))
			_callback(PARTICLE_FINISHED);

	}

	//---------------------------------------------------------

	void CParticleSystem::start()
	{
		_particleSystem->startAndStopFade(_duration);
	}

	//---------------------------------------------------------

	void CParticleSystem::setCallBack(Callback callback)
	{
		_callback = callback;
	}
}