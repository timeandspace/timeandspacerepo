/**
@file EntitySpawner.h

@author �lvaro Bl�zquez Checa
@date Marzo, 2014
*/

#ifndef __Logic_EntitySpawner_H
#define __Logic_EntitySpawner_H

#include "Logic/Entity/Component.h"

namespace Logic
{

	/**
	Clase CEntitySpawner
	*/
	class CEntitySpawner : public IComponent
	{
		DEC_FACTORY(CEntitySpawner);
	public:

		/**
		*/
		CEntitySpawner() : IComponent(),_entityToSpawn(""),_initialized(false),
			_id(0),_entityOrientation(0.0f)
		{
		}

		/**
		*/
		~CEntitySpawner() {}

		/**
		*/
		virtual bool spawn(CEntity* entity, CMap *map, const Map::CEntity *entityInfo);

		/**
		*/
		virtual bool activate();

		/**
		*/
		virtual void deactivate();

		/**
		*/
		virtual bool accept(CMessage *message);

		/**
		*/
		virtual void process(CMessage *message);

		/**
		*/
		virtual void tick(unsigned int msecs);

	protected:

		float _entityOrientation;

		std::string _entityToSpawn;

		bool _initialized;

		int _id;

	}; // Class CShooter

	REG_FACTORY(CEntitySpawner);

} // namespace Logic

#endif // __Logic_Shooter_H