/**
@file FadeOut.h

Contiene la implementación del componente que hace que una entidad se desvanezca con el tiempo.
Modifica el parametro alpha del material.

@author Alejandro Pérez Alonso

@date Junio, 2014
*/
#include "FadeOut.h"

#include "Logic/Entity/Entity.h"
#include "Logic/Maps/EntityFactory.h"
#include "Logic/Maps/map.h"
#include "Map/MapEntity.h"

#include "Graphics/Server.h"
#include "Graphics/Entity.h"


namespace Logic
{
	IMP_FACTORY(CFadeOut);

	bool CFadeOut::spawn(CEntity* entity, CMap *map, const Map::CEntity *entityInfo)
	{
		return IComponent::spawn(entity, map, entityInfo);
	} // spawn

	//--------------------------------------------------------

	bool CFadeOut::activate()
	{
		_graphicsEntity = Graphics::CServer::getSingletonPtr()->getEntity(_entity->getName());

		return IComponent::activate();

	} //activate

	//--------------------------------------------------------

	void CFadeOut::deactivate()
	{
		return IComponent::deactivate();

	} //deactivate

	//--------------------------------------------------------

	void CFadeOut::tick(unsigned int msecs)
	{
		IComponent::tick(msecs);

		if (_enabled)
		{
			//std::stringstream ss;
			//ss << _value;
			_graphicsEntity->changeParameter(_materialName, "Alpha", _value);
			_value += float(msecs / 3000.0f); // 4000 -> 4seg que durara el efecto
			if (_value > 1)
				_enabled = false;
		}

	} // tick

	//--------------------------------------------------------

	bool CFadeOut::accept(CMessage *message)
	{
		return message->getType() == Message::CHANGE_MATERIAL;

	} //accept

	//--------------------------------------------------------

	void CFadeOut::process(CMessage *message)
	{
		if (message->getType() == Message::CHANGE_MATERIAL)
		{
			_materialName = static_cast<CChangeMaterialMessage *>(message)->_materialName;
			_enabled = static_cast<CChangeMaterialMessage *>(message)->_specialAction;
			_value = 0;
		}

	} //process

	//--------------------------------------------------------

}

