/**
@file DynamicEntity.cpp

@author Alvaro Blazquez Checa
@date Diciembre, 2010
*/
#include "DynamicEntity.h"

#include "Map/MapEntity.h"

#include "AI/Movement.h"

namespace Logic 
{

	IMP_FACTORY(CDynamicEntity);

	//---------------------------------------------------------
	
	/**
	Constructor
	*/
	CDynamicEntity::CDynamicEntity() : IComponent()
	{ 
		_currentProperties = new AI::IMovement::DynamicMovement();
	}
	
	//---------------------------------------------------------
	/**
	Destructor
	*/
	CDynamicEntity::~CDynamicEntity()
	{
		delete _currentProperties;
	}

	//---------------------------------------------------------
	
	bool CDynamicEntity::spawn(CEntity *entity, CMap *map, const Map::CEntity *entityInfo) 
	{
		if(!IComponent::spawn(entity,map,entityInfo))
			return false;

		_lastPosition = _entity->getPosition();

		return true;

	} // spawn

	//---------------------------------------------------------
	
	void CDynamicEntity::tick(unsigned int msecs)
	{
		IComponent::tick(msecs);

		_currentProperties->linearSpeed = (_entity->getPosition() - _lastPosition) / msecs;

		_lastPosition = _entity->getPosition();
	} // tick


	//---------------------------------------------------------

	AI::IMovement::DynamicMovement* CDynamicEntity::getCurrentProperties()
	{
		return _currentProperties;
	}
	
	//---------------------------------------------------------

	Vector3 CDynamicEntity::getVelocity()
	{
		return _currentProperties->linearSpeed;
	}
}