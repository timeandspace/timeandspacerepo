/**
@file SwitchTrigger.cpp

Contiene la declaraci�n del componente que envia un mensaje SWITCH a otra
entidad cuando recibe un mensaje TOUCHED / UNTOUCHED.

@see Logic::CSwitchTrigger
@see Logic::IComponent

@author David Llans�
@date Octubre, 2010
*/

#include "SwitchTrigger.h"

#include "Logic/Entity/Entity.h"
#include "Logic/Maps/Map.h"
#include "Map/MapEntity.h"


namespace Logic 
{
	IMP_FACTORY(CSwitchTrigger);

	//---------------------------------------------------------

	bool CSwitchTrigger::spawn(CEntity *entity, CMap *map, const Map::CEntity *entityInfo) 
	{
		if(!IComponent::spawn(entity,map,entityInfo))
			return false;

		// TODO: leer atributo del mapa "target"
		// Leer atributo del mapa "target" y almacenarlo en el atributo privado _targetName
		std::stringstream ss;
		int aux = 2;
		if (entityInfo->hasAttribute("target"))
		{
			_targetNames.push_back(entityInfo->getStringAttribute("target"));

			// A�adimos la posibilidad de tener mas de 1 target
			ss << "target" << aux;
			while (entityInfo->hasAttribute(ss.str()))
			{
				_targetNames.push_back(entityInfo->getStringAttribute(ss.str()));
				ss.str(""); ss.clear();
				++ aux;
				ss << "target" << aux;
			}

			ss.str("");
			ss.clear();
			aux = 1;
		}
		// Opposite target, son objetivos que realizan el efecto contrario
		ss << "oppositeTarget";
		while (entityInfo->hasAttribute(ss.str()))
		{
			_opositeTargets.push_back(entityInfo->getStringAttribute(ss.str()));
			ss.str(""); ss.clear();
			++ aux;
			ss << "oppositeTarget" << aux;
		}
		ss.str(""); ss.clear(); 

		/**
		Guardamos los target de sistemas de part�culas:
		- particleSystemTarget -> nombre del sistema de particulas afectado
		- particleSystemActivate -> acci�n al entrar en el trigger, true activar� las particulas
		false las desactiva.
		*/
		if (entityInfo->hasAttribute("particleSystemTarget"))
		{
			ParticleSystemTarget psTarget;
			psTarget._name = entityInfo->getStringAttribute("particleSystemTarget");
			psTarget._activate = entityInfo->getBoolAttribute("particleSystemActivate");
			_particleSystemTargets.push_back(psTarget);
		}
		aux = 2;
		ss << "particleSystemTarget" << aux;
		std::stringstream ss2;
		ss2 << "particleSystemActivate" << aux;
		while (entityInfo->hasAttribute(ss.str()) && 
			entityInfo->hasAttribute(ss2.str()))
		{
			ParticleSystemTarget psTarget;
			psTarget._name = entityInfo->getStringAttribute(ss.str());
			psTarget._activate = entityInfo->getBoolAttribute(ss2.str());
			_particleSystemTargets.push_back(psTarget);
			ss.str(""); ss.clear(); ss2.str(""); ss2.clear();
			++ aux;
			ss << "particleSystemTarget" << aux;
			ss2 << "particleSystemActivate" << aux;
		}


		return true;

	} // spawn

	//---------------------------------------------------------

	bool CSwitchTrigger::activate()
	{
		IComponent::activate();
		// TODO: obtener un puntero a la entidad target
		// Ojo: necesitamos hacerlo en el activate, porque en el spawn de este componente
		// puede que a�n no exista la entidad target
		// Pedir al mapa un puntero a la entidad
		/*_target = _entity->getMap()->getEntityByName(_targetName);
		if (!_target) return false;*/
		return true;

	} // activate

	//---------------------------------------------------------

	void CSwitchTrigger::deactivate()
	{
		IComponent::deactivate();

	} // deactivate

	//---------------------------------------------------------

	bool CSwitchTrigger::accept(CMessage *message)
	{
		// TODO: recibir mensajes de tipo TOUCHED y UNTOUCHED
		return message->getType() == Message::TOUCHED 
			|| message->getType() == Message::UNTOUCHED;

	} // accept

	//---------------------------------------------------------

	void CSwitchTrigger::process(CMessage *message)
	{
		switch(message->getType())
		{
		case Message::TOUCHED:
			if (_count == 0)
				onTriggerEnter();

			_entityList.push_back(static_cast<CTouchedMessage *>(message)->_entity->getName());

			_count ++;
			break;

		case Message::UNTOUCHED:
			_count --;
			if (_count == 0)
				onTriggerExit();

			// Buscamos la entidad en la lista de entidades para darla de baja
			std::list<std::string>::iterator it = _entityList.begin();
			std::list<std::string>::iterator end = _entityList.end();
			while (it != end)
			{
				if ((*it) == static_cast<CUnTouchedMessage *>(message)->_entity->getName())
				{
					it = _entityList.erase(it);
					break;
				}
				++it;
			}

			break;
		}

	} // process

	//---------------------------------------------------------

	void CSwitchTrigger::onTriggerEnter()
	{
		if (_targetNames.size() > 0)
		{
			CSwitchMessage* m = new CSwitchMessage();
			m->_int = 1;
			m->_activate = true;
			CEntity *ent = 0;
			for (unsigned int i = 0; i < _targetNames.size(); ++i)
			{
				ent = _entity->getMap()->getEntityByName(_targetNames[i]);
				if (ent)
					ent->emitMessage(m);
			}
		}
		// Si existe targets opuestos, se les env�a la acci�n de UnTouched
		if (_opositeTargets.size() > 0)
		{
			CSwitchMessage* msg = new CSwitchMessage();
			msg->_int = -1;
			msg->_activate = false;
			for (unsigned int i = 0; i < _opositeTargets.size(); ++i)
			{
				_entity->getMap()->getEntityByName(_opositeTargets[i])->
					emitMessage(msg);
			}
		}
		if (_particleSystemTargets.size() > 0)
		{
			for (unsigned int i = 0; i < _particleSystemTargets.size(); ++i)
			{
				CParticleSystemMessage* msg = new CParticleSystemMessage(_particleSystemTargets[i]._activate);
				if (!msg->_activate)
					msg->_duration = 0.5; // duraccion del fade out en segundos
				_entity->getMap()->getEntityByName(_particleSystemTargets[i]._name)->
					emitMessage(msg);
			}
		}

	} // onTriggerEnter

	//---------------------------------------------------------

	void CSwitchTrigger::onTriggerExit()
	{
		if (_targetNames.size() > 0)
		{
			CSwitchMessage* m = new CSwitchMessage();
			m->_int = -1;
			m->_activate = false;
			CEntity *ent = 0;
			for (unsigned int i = 0; i < _targetNames.size(); ++i)
			{
				ent = _entity->getMap()->getEntityByName(_targetNames[i]);
				if (ent)
					ent->emitMessage(m);
			}
		}
		// Si existe targets opuestos, se les env�a la acci�n de Touched
		if (_opositeTargets.size() > 0)
		{
			for (unsigned int i = 0; i < _opositeTargets.size(); ++i)
			{
				CSwitchMessage* msg = new CSwitchMessage();
				msg->_int = 1;
				msg->_activate = true;
				_entity->getMap()->getEntityByName(_opositeTargets[i])->
					emitMessage(msg);
			}
		}
		if (_particleSystemTargets.size() > 0)
		{
			for (unsigned int i = 0; i < _particleSystemTargets.size(); ++i)
			{
				CParticleSystemMessage* msg = new CParticleSystemMessage(!_particleSystemTargets[i]._activate);
				_entity->getMap()->getEntityByName(_particleSystemTargets[i]._name)->
					emitMessage(msg);
			}
		}

	} // onTriggerExit

	//---------------------------------------------------------

	void CSwitchTrigger::tick(unsigned int msecs)
	{
		IComponent::tick(msecs);

		if (_entityList.size() > 0)
		{
			std::list<std::string>::iterator it = _entityList.begin();
			std::list<std::string>::iterator end = _entityList.end();
			while (it != end)
			{
				//TODO:: cuando se elimina una entidad no se pq pero nunca es NULL
				if (_entity->getMap()->getEntityByName((*it)) == NULL)
				{
					it = _entityList.erase(it);
					continue;
				}

				++it;
			}
			_count = _entityList.size();
			if (_count == 0)
				onTriggerExit();
		}
	}

} // namespace Logic

