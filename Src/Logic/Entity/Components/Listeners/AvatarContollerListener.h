

#ifndef __Logic_AvatarControllerListenr_H
#define ____Logic_AvatarControllerListenr_H

namespace Logic
{
	class IAvatarControllerListener
	{
	public:
		virtual void jumping(bool active)=0;
		virtual void crouch(bool active)=0;
		virtual void walking(bool active)=0;
		virtual void walkingBack(bool active)=0;
		virtual void idle(bool active)=0;
		virtual void running(bool active)=0;
		virtual void shooting(bool active)=0;
		virtual void strafingLeft(bool active)=0;
		virtual void strafingRight(bool active)=0;
		virtual void death(bool active)=0;
		virtual void getObject(bool active)=0;
		virtual void hasWeapon(bool active) = 0;
		virtual void damage(bool active) = 0;
		virtual void changeWeapon(bool active) = 0;
		virtual void aim(bool active) = 0;
		virtual void pushButton(bool active) = 0;
	};
}


#endif