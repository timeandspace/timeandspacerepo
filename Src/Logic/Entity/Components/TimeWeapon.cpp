/**
@file NormalWeapon.cpp

@see Logic::CWeapon

@author Alejandro P�rez Alonso
@date Mayo, 2014
*/
#include "TimeWeapon.h"

#include "Logic/Entity/Entity.h"
#include "Map/MapEntity.h"
#include "Physics/Server.h"
#include "Logic/Maps/EntityFactory.h"
#include "Logic/Maps/map.h"
#include "Graphics/Server.h"
#include "SimpleAnimatedGraphics.h"
#include "ParticleSystem.h"

namespace Logic
{

	IMP_FACTORY(CTimeWeapon);

	//---------------------------------------------------------

	CTimeWeapon::CTimeWeapon() : 
		CWeapon(),
		_bulletEntityInfo(NULL),
		_bulletEntityName(""),
		_modifier(0.0f),
		_duration(0),
		_id(0)
	{
	}

	//---------------------------------------------------------

	CTimeWeapon::~CTimeWeapon()
	{
		if(_bulletEntityInfo)
		{
			delete _bulletEntityInfo;
			_bulletEntityInfo = NULL;
		}
	}

	//---------------------------------------------------------

	bool CTimeWeapon::spawn(CEntity* entity, CMap *map, const Map::CEntity *entityInfo)
	{
		if (!CWeapon::spawn(entity, map, entityInfo))
			return false;

		assert(entityInfo->hasAttribute("bulletEntityName") && "Falta atributo");
		_bulletEntityName = entityInfo->getStringAttribute("bulletEntityName");

		assert(entityInfo->hasAttribute("clock_modifier") && "Falta atributo");
		_modifier = entityInfo->getFloatAttribute("clock_modifier");

		assert(entityInfo->hasAttribute("duration") && "Falta atributo");
		_duration = entityInfo->getIntAttribute("duration");

		return true;
	}


	//---------------------------------------------------------

	bool CTimeWeapon::activate()
	{
		CWeapon::activate();
		if(!_init)
		{
			_bulletEntityInfo = Logic::CEntityFactory::getSingletonPtr()->getMapEntityByName(_bulletEntityName);
			_bulletEntityInfo = new Map::CEntity(*_bulletEntityInfo);
			_init = true;
		}
		
		return IComponent::activate();
	}

	//---------------------------------------------------------

	void CTimeWeapon::deactivate()
	{
		CWeapon::deactivate();

		return IComponent::deactivate();
	}

	//---------------------------------------------------------

	bool CTimeWeapon::accept(CMessage *message)
	{
		return CWeapon::accept(message);
	}

	//---------------------------------------------------------

	void CTimeWeapon::process(CMessage *message)
	{
		CWeapon::process(message);
	}

	//---------------------------------------------------------

	void CTimeWeapon::tick(unsigned int msecs)
	{
		CWeapon::tick(msecs);

		// Rate fire
	}

	//---------------------------------------------------------

	void CTimeWeapon::attachWeapon(const std::string &entityName, const std::string &boneName,
		Quaternion offsetOrientation = Quaternion::IDENTITY, Vector3 offsetPosition = Vector3::ZERO)
	{
		// Orientacion del arma
		/*Ogre::Quaternion offsetOrientation(Ogre::Degree(-70), Vector3::UNIT_Y);
		Quaternion qX(Ogre::Degree(45), Vector3::UNIT_X);
		Quaternion qZ(Ogre::Degree(20), Vector3::UNIT_Z);
		offsetOrientation = offsetOrientation * qX * qZ;*/
		if (offsetOrientation !=  Quaternion::IDENTITY)
			_offsetOrientation = offsetOrientation;
		if (offsetPosition != Vector3::ZERO)
			_offsetPosition = offsetPosition;

		// Attach
		Graphics::CServer::getSingletonPtr()->getEntity(entityName)->
			attachObjectToBone(boneName, _entity->getName(), _offsetOrientation, _offsetPosition);
	}

	//---------------------------------------------------------

	void CTimeWeapon::detachWeapon()
	{
		Graphics::CServer::getSingletonPtr()->getEntity(_entity->getName())->detachObject(_entity->getName());
	}

	//---------------------------------------------------------

	void CTimeWeapon::fire(const Vector3 &impactPoint, const std::string &hitEntityName)
	{
		// He cambiado esto aqui en lugar de en el tick porque petaba al matar a una copia que tenia un arma enganchada
		_shootingPoint = Graphics::CServer::getSingletonPtr()->getBonePosition(_entity->getName(), _shootingPointBone);

		// El arma normal, las balas son instant�neas
		CEditTimeMessage *m =  new CEditTimeMessage(TWeaponType::SLOW_DOWN, -_modifier, _duration);
		CEntity *hitEntity = _entity->getMap()->getEntityByName(hitEntityName);

		if (hitEntity)
			hitEntity->emitMessage(m);
		else
			delete m;

		Vector3 direction = impactPoint - _shootingPoint;
		direction.normalise();

		// Instanciamos bala
		createBullet(impactPoint, direction);

		// Activamos animaci�n
		CMessage* animationMessage = new CMessage();
		animationMessage->setType(Message::SET_ANIMATION);
		_entity->emitMessage(animationMessage);
	}

	//---------------------------------------------------------

	void CTimeWeapon::createBullet(const Vector3 &impactPoint, const Vector3 &direction)
	{
		std::stringstream ss;
		ss << _shootingPoint.x << " " << _shootingPoint.y << " " << _shootingPoint.z; 
		_bulletEntityInfo->setAttribute("position", ss.str());

		// Vaciar stringstream
		ss.str("");
		ss.clear();

		ss << impactPoint.x << " " << impactPoint.y << " " << impactPoint.z; 
		_bulletEntityInfo->setAttribute("finalPosition", ss.str());

		// Vaciar stringstream
		ss.str("");
		ss.clear();

		// Asignamos la direcci�n de movimiento. Ya que las balas no son instantaneas
		ss << direction.x << " " << direction.y << " " << direction.z; 
		_bulletEntityInfo->setAttribute("direction", ss.str());

		// Asignamos el nombre del template del efecto de particulas
		/*_bulletEntityInfo->setAttribute("template", "NormalWeapon");*/

		// Vaciar stringstream
		ss.str("");
		ss.clear();

		// Asigamos nombre �nico
		//TODO:: HACER UNA ESPECIE DE EntityID.h PARA TODAS LAS ENTIDADES INSTANCIADAS
		// O MODIDIFICARLO PARA QUE PUEDAN ACCEDER TODOS.
		// Hack:: ej: SlowBullet_David_1 or BackBullet_Peter2_4
		ss << _bulletEntityName << "_" << _entity->getName() << "_" << ++_id;	
		_bulletEntityInfo->setName(ss.str());

		// Creamos una nueva entidad con el prefab
		Logic::CEntity* bulletEntity = CEntityFactory::getSingletonPtr()->
			createEntity(_bulletEntityInfo, _entity->getMap());
		// Llamamos al activate de la entidad (IMPORTANTE: si no no funcionar� en el .exe en release)
		bulletEntity->activate();

		// Avisamos a las particulas de la bala TODO esto se puede hacer en la bala
		// y asi puede haber balas con efecto de particulas o sin el
		CParticleSystemMessage* particleMessage = new CParticleSystemMessage();
		particleMessage->_activate = true;
		bulletEntity->emitMessage(particleMessage);
	}

} //namespace Logic
