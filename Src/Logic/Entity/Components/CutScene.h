/**
@file CutScene.h

Contiene la declaraci�n del componente que realiza un cut scene. Las c�maras al 
igual que los mesh van adjuntos a un scene node, por lo que se puede crear un
scene node a parte, hacer todas las cosas del cut scene, y volver a adjuntarla
al scene node original.

@author Alejandro P�rez Alonso

@date Mayo, 2014
*/
#ifndef __Logic_Cut_Scene_H
#define __Logic_Cut_Scene_H

#include "Logic/Entity/Component.h"
#include "GUI/InputManager.h"


namespace Graphics 
{
	class CCamera;
	class CScene;
}

namespace Logic
{
	class CMusicPlayer;
}

namespace GUI
{
	class CHudManager;
}

//declaraci�n de la clase
namespace Logic 
{

	class CCutScene : public IComponent, public GUI::CKeyboardListener 
	{
		DEC_FACTORY(CCutScene);
	public:

		/**
		Constructor 
		*/
		CCutScene() : IComponent(), _graphicsCamera(0), _count(0), 
			_cutSceneActivated(false), _cutSceneNumber(0), _init(false),
			_isCinemaMode(true), _musicPlayer(0),_hudManager(0),_sceneTitle(""),
			_scriptName(""), _functionName(""), _stopPlayer(true)
		{}

		/**
		Inicializaci�n del componente, utilizando la informaci�n extra�da de
		la entidad le�da del mapa (Maps::CEntity). Se accede a los atributos 
		necesarios como la c�mara gr�fica y se leen atributos del mapa.

		@param entity Entidad a la que pertenece el componente.
		@param map Mapa L�gico en el que se registrar� el objeto.
		@param entityInfo Informaci�n de construcci�n del objeto le�do del
		fichero de disco.
		@return Cierto si la inicializaci�n ha sido satisfactoria.
		*/
		virtual bool spawn(CEntity* entity, CMap *map, const Map::CEntity *entityInfo);

		/**
		M�todo que activa el componente; invocado cuando se activa
		el mapa donde est� la entidad a la que pertenece el componente.
		<p>
		Se coge el jugador del mapa, la entidad que se quiere "seguir".

		@return true si todo ha ido correctamente.
		*/
		virtual bool activate();

		/**
		M�todo que desactiva el componente; invocado cuando se
		desactiva el mapa donde est� la entidad a la que pertenece el
		componente. Se invocar� siempre, independientemente de si estamos
		activados o no.
		<p>
		Se pone el objetivo a seguir a NULL.
		*/
		virtual void deactivate();

		/**
		M�todo llamado en cada frame que actualiza el estado del componente.
		<p>
		Se encarga de mover la c�mara siguiendo al jugador.

		@param msecs Milisegundos transcurridos desde el �ltimo tick.
		*/
		virtual void tick(unsigned int msecs);


		virtual bool accept(CMessage *message);


		virtual void process(CMessage *message);


		virtual bool keyPressed(GUI::TKey key);

		/**
		Envia un mensaje CSwitchMessage, en el init del cutscene hacemos
		getEntitiesByType("PuertaPhysics", _actors); por lo que index indica
		la posicion del vector y 'activate' indica si activa o desactiva
		la plataforma o puerta
		*/
		void sendSwitchMessage(int index, bool activate);

	protected:

		void initCutScene();

		void finishCutScene();

		void cutScene1(unsigned int msecs);

		void cutScene2(unsigned int msecs);

		void finishCutScene2();

		/**
		C�mara gr�fica.
		*/
		Graphics::CCamera *_graphicsCamera;

		/**
		Componente music player
		*/
		Logic::CMusicPlayer *_musicPlayer;


		/**
		Componente music player
		*/
		GUI::CHudManager* _hudManager;

		int _count;
		int _cutSceneNumber;
		bool _cutSceneActivated;
		std::string _sceneTitle;
		
		// Nombre del script que se ejecuta
		std::string _scriptName;
		// Nombre de la funcion que se ejecuta
		std::string _functionName;

		// Indica si al activarse el cutScene activa el modo cine (barras negras)
		bool _isCinemaMode;

		// Indica si el cutScene ha sido inicializado o no
		bool _init;

		// Contiene todas las entidades de tipo PuertaPhysics
		std::vector<Logic::CEntity*> _actors;

		// Indica si se debe parar o no los movimientos del player al activar el cutscene
		bool _stopPlayer;

	}; // class CCutScene

	REG_FACTORY(CCutScene);

} // namespace Logic

#endif // __Logic_Cut_Scene_H
