/**
@file Compositor.h

Declaraci�n del componente que controla el comportamiento de un compositor

@author Alejandro P�rez Alonso
@date Junio, 2014
*/

#ifndef __Logic_Compositor_H
#define __Logic_Compositor_H

#include "Logic/Entity/Component.h"

namespace Graphics
{
	class CScene;
	class CCompositor;
}
//declaraci�n de la clase
namespace Logic 
{

	class CCompositor : public IComponent
	{
		DEC_FACTORY(CCompositor);
	public:

		/**
		Constructor por defecto; inicializa los atributos a su valor por 
		defecto.
		*/
		CCompositor();

		/**
		Destructor (virtual); Quita de la escena y destruye la entidad gr�fica.
		*/
		virtual ~CCompositor();
		
		/**
		Inicializaci�n del componente, utilizando la informaci�n extra�da de
		la entidad le�da del mapa (Maps::CEntity). Toma del mapa el atributo
		model con el modelo que se deber� cargar e invoca al m�todo virtual
		createGraphicsEntity() para generar la entidad gr�fica.

		@param entity Entidad a la que pertenece el componente.
		@param map Mapa L�gico en el que se registrar� el objeto.
		@param entityInfo Informaci�n de construcci�n del objeto le�do del
			fichero de disco.
		@return Cierto si la inicializaci�n ha sido satisfactoria.
		*/
		virtual bool spawn(CEntity* entity, CMap *map, const Map::CEntity *entityInfo);


		virtual void tick(unsigned int msecs);


		virtual bool accept(CMessage *message);


		virtual bool activate();

		virtual void deactivate();
		

		virtual void process(CMessage *message);

	protected:

		std::string _compositorName;

		bool _autoStart;


	}; // class CCompositor

	REG_FACTORY(CCompositor);

} // namespace Logic

#endif // __Logic_Compositor_H
