/**
@file AvatarController.h

Contiene la declaraci�n del componente que controla el movimiento 
de la entidad.

@see Logic::CAvatarController
@see Logic::IComponent

@author David Llans�
@author �lvaro Bl�zquez Checa
@author Alejandro P�rez Alonso

@date Agosto, 2010
@date Marzo, 2014
*/
#ifndef __Logic_AvatarController_H
#define __Logic_AvatarController_H

#include "Logic/Entity/Component.h"

namespace Logic
{
	class CParticleSystem;
}

//declaraci�n de la clase
namespace Logic 
{
	class IAvatarControllerListener;


	class CAvatarController : public IComponent 
	{
		DEC_FACTORY(CAvatarController);
	public:

		//typedef std::tr1::function<int (void)> Callback;

		/**
		Constructor por defecto; inicializa los atributos a su valor por 
		defecto.
		*/
		CAvatarController() : IComponent(), _walking(false), _walkingBack(false), 
			_strafingLeft(false), _strafingRight(false), _jumping(false), _crouching(false), 
			_speed(0.055f), _shooting(false), _fireRate(150), _fireRateAux(0), 
			_objectName(""), _hasWeapon(false), _onTriggerEntity(""),
			_runSpeed(0.02f), _running(false), _shitfPressed(false),
			_currentSpeed(0.0f), _shootingMode(0), _death(false),
			_specialAction(TSpecialActions::NONE), _specialPowerFlag(false),
			_particleSystem(NULL),_turningValue(0.0f),_pitchValue(0.0f),
			_avatarState(false), _rateFire(0)
		{
			
		}
		
		/**
		Inicializaci�n del componente, utilizando la informaci�n extra�da de
		la entidad le�da del mapa (Maps::CEntity). Toma del mapa el atributo
		speed que indica a la velocidad a la que se mover� el jugador.

		@param entity Entidad a la que pertenece el componente.
		@param map Mapa L�gico en el que se registrar� el objeto.
		@param entityInfo Informaci�n de construcci�n del objeto le�do del
			fichero de disco.
		@return Cierto si la inicializaci�n ha sido satisfactoria.
		*/
		virtual bool spawn(CEntity* entity, CMap *map, const Map::CEntity *entityInfo);

		/**
		M�todo que activa el componente; invocado cuando se activa
		el mapa donde est� la entidad a la que pertenece el componente.
		<p>
		Si el componente pertenece a la entidad del jugador, el componente
		se registra as� mismo en el controlador del GUI para que las ordenes 
		se reciban a partir de los eventos de teclado y rat�n.

		@return true si todo ha ido correctamente.
		*/
		virtual bool activate();
		
		/**
		M�todo que desactiva el componente; invocado cuando se
		desactiva el mapa donde est� la entidad a la que pertenece el
		componente. Se invocar� siempre, independientemente de si estamos
		activados o no.
		<p>
		Si el componente pertenece a la entidad del jugador, el componente
		se deregistra as� mismo en el controlador del GUI para dejar de
		recibir las ordenes dadas a partir de los eventos de teclado y rat�n.
		*/
		virtual void deactivate();

		/**
		M�todo llamado en cada frame que actualiza el estado del componente.
		<p>
		Se encarga de mover la entidad en cada vuelta de ciclo cuando es
		necesario (cuando est� andando o desplaz�ndose lateralmente).

		@param msecs Milisegundos transcurridos desde el �ltimo tick.
		*/
		virtual void tick(unsigned int msecs);

		/**
		M�todo virtual que elige que mensajes son aceptados. Son v�lidos
		CONTROL.

		@param message Mensaje a chequear.
		@return true si el mensaje es aceptado.
		*/
		virtual bool accept(CMessage *message);

		/**
		M�todo virtual que procesa un mensaje.

		@param message Mensaje a procesar.
		*/
		virtual void process(CMessage *message);


		void setListener(IAvatarControllerListener* listener) { _controllerListener = listener; }

		/**
		Callback al que se llamara desde otras clases para notificar eventos

		@param CallbackEvent evento a procesar.
		*/
		void eventCallback(CallbackEvent callbackEvent);

		/**
		Para todo movimiento poniendo los booleanos a false, se
		fuerza a pesar de que no llegue el mensaje correspondiente 
		de stopstrafe, stopwalk.. etc.
		*/
		void stopAllMovement();

	protected:

		/**
		Metodo que se invoca cuando una entidad entra en un trigger
		de special action. en funci�n del string y el boolean realiza
		una serie de acciones. Por ejemplo adjuntar.
		*/
		void onTriggerEnter(TSpecialActions specialAction, bool enter, 
			std::string otherInfo, CEntity *ent);

		/**
		Funci�n envocada cuando una animacion que no se reproduzca en loop
		termina. En funci�n del nombre el avatar realizar� una acci�n o
		descartar� el mensaje.
		*/
		void onAnimationFinished(TAnimations animationName);
		
		/**
		Provoca que la entidad avance.
		*/
		void walk();

		/**
		Provoca que la entidad retroceda.
		*/
		void walkBack();

		/**
		Provoca que la entidad cese el avance.
		*/
		void stopWalk();

		/**
		Provoca que la entidad se desplace lateralmente a la izquierda.
		*/
		void strafeLeft();

		/**
		Provoca que la entidad se desplace lateralmente a la derecha.
		*/
		void strafeRight();

		/**
		Provoca que la entidad cese el desplazamiento lateral.
		*/
		void stopStrafe();
		
		/**
		Provoca que la entidad se agache.
		*/
		void crouch();

		/**
		Provoca que la entidad deje de agacharse.
		*/
		void stopCrouch();

		/**
		Provoca que la entidad gire. N�meros Positivos para	giro a 
		derechas, negativos para giro izquierdas.

		@param amount Cantidad de giro. Positivos giro a derechas,
		negativos a izquierdas.
		*/
		void turn(float amountX);

		void pitch(float amountY);

		/**
		Provoca que la entidad salte.
		*/
		void jump();

		void stopJump();

		/**
		Provoca que la entidad dispare
		*/
		void fire();

		/**
		Provoca que la entidad pare de dispare
		*/
		void stopFire();

		


		void run();


		void stopRun();


		/**
		Provoca que la entidad coja un objeto, para poder usar esta acci�n 
		debe ser activada previamente.
		*/
		void contextualAction();


		void zoom(bool active);


		void death();


		void specialPower();
		void specialPower2();
		void specialPower3();

		void changeWeapon(int scroll);


		IAvatarControllerListener* _controllerListener;

		void setCurrentSpeed(float msecs);



		std::bitset<32> _avatarState;

		/**
		Atributo para saber si la entidad est� avanzando.
		*/
		bool _walking;

		/**
		Atributo para saber si la entidad est� retrocediendo.
		*/
		bool _walkingBack;

		/**
		Atributo para saber si la entidad est� moviendose lateralmente
		a la izquierda.
		*/
		bool _strafingLeft;

		/**
		Atributo para saber si la entidad est� moviendose lateralmente
		a la derecha.
		*/
		bool _strafingRight;

		bool _shooting;

		/**
		Atributo que indica la magnitud de velocidad de la entidad.
		*/
		float _speed;

		float _runSpeed;

		float _currentSpeed;

		/** 
		Indica si el character controller est� saltando
		*/
		bool _jumping;

		/**
		Indica si el character controller est� agachado
		*/
		bool _crouching;

		float _turningValue;
		float _pitchValue;

		// Duracion del salto
		int _jumpTime;

		// Altura de cada salto por tick (_movement += Vector3::INIT_Y * _jumpHeight)
		float _jumpHeight;

		bool _shitfPressed;

		bool _running;

		/**
		Indica la cadencia de disparo
		*/
		int _fireRate;

		int _fireRateAux;

		
		/**
		Posible objeto a recoger
		*/
		std::string _objectName;

		/**
		Trigger especial, sobre el que se encuentra el avatar
		*/
		//CEntity *_onTriggerEntity;
		std::string _onTriggerEntity;

		TSpecialActions _specialAction;

		/**
		Indica si el avatar lleva un arma o no
		*/
		bool _hasWeapon;

		int _shootingMode;

		bool _death;

		bool _specialPowerFlag;
		/**
		ParticleSystem del avatar
		*/
		Logic::CParticleSystem* _particleSystem;

		//Cadencia de disparo 1 disparo cada x msecs
		int _rateFire;


	}; // class CAvatarController

	REG_FACTORY(CAvatarController);

} // namespace Logics

#endif // __Logic_AvatarController_H
