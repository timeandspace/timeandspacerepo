/**
@file CutScene.cpp

Contiene la implementaci�n del componente que controla los cut scenes

@see GUI::CCamera
@see Logic::IComponent

@author Alejandro P�rez Alonso
@date Mayo, 2014
*/

#include "CutScene.h"
#include "MusicPlayer.h"

#include "Logic/Server.h"
#include "Logic/Entity/Entity.h"
#include "Logic/Maps/Map.h"
#include "Map/MapEntity.h"

#include "Graphics/Scene.h"
#include "Graphics/Camera.h"
#include "Graphics/Server.h"
#include "Graphics/CompositorManager.h"

#include "Application/BaseApplication.h"

#include "GUI/Server.h"
#include "GUI/PlayerController.h"
#include "GUI/HudManager.h"

#include "ScriptManager/Binding.h"
#include "ScriptManager/ScriptManager.h"

#include "Logic/Managers/GameManager.h"


namespace Logic 
{
	IMP_FACTORY(CCutScene);

	//---------------------------------------------------------

	bool CCutScene::spawn(CEntity *entity, CMap *map, const Map::CEntity *entityInfo) 
	{
		if(!IComponent::spawn(entity,map,entityInfo))
			return false;

		// Nombre de la escena
		if(entityInfo->hasAttribute("sceneTitle"))
			_sceneTitle = entityInfo->getStringAttribute("sceneTitle");
		// Indica si se activa el modo cine (bandas negras y nombre)
		if(entityInfo->hasAttribute("cinemaMode"))
			_isCinemaMode = entityInfo->getBoolAttribute("cinemaMode");
		// Script a ejecutar
		if (entityInfo->hasAttribute("scriptName"))
			_scriptName = entityInfo->getStringAttribute("scriptName");
		// Nombre de la funcion del script a ejecutar
		if (entityInfo->hasAttribute("functionName"))
			_functionName = entityInfo->getStringAttribute("functionName");

		// Indica si se debe parar los mov. del player al activar el cutScene
		if (entityInfo->hasAttribute("stopPlayer"))
			_stopPlayer = entityInfo->getBoolAttribute("stopPlayer");

		return true;
	} // spawn

	//---------------------------------------------------------

	bool CCutScene::activate()
	{
		// Consultamos GameManager para saber si el cutScene ha sido activado o no
		if (CGameManager::getSingletonPtr()->hasResetInfo(_entity->getName()))
		{
			this->_activated = *(bool*) (CGameManager::getSingletonPtr()->
				getResetInfo(_entity->getName()));
			return true;
		}
		// Obtenemos la camara
		_graphicsCamera = _entity->getMap()->getScene()->getCamera();

		Logic::CEntity* ent = CServer::getSingletonPtr()->getMap()->getEntityByName("MusicPlayer");

		if(ent)
			_musicPlayer = static_cast<Logic::CMusicPlayer*>(ent->getComponent("CMusicPlayer"));

		_hudManager = GUI::CHudManager::getSingletonPtr();

		return IComponent::activate();

	} // activate

	//---------------------------------------------------------

	void CCutScene::deactivate()
	{
		_graphicsCamera = NULL;

		_musicPlayer = NULL;

		_hudManager = NULL;

		GUI::CInputManager::getSingletonPtr()->removeKeyListener(this);

		IComponent::deactivate();

	} // deactivate

	//---------------------------------------------------------

	void CCutScene::tick(unsigned int msecs)
	{
		IComponent::tick(msecs);
		if (_cutSceneActivated)
		{
			_count += msecs;
			switch (_cutSceneNumber)
			{
			case 1:
				cutScene1(msecs);
				break;
			case 2:
				cutScene2(msecs);
				break;
			default:
				_cutSceneActivated = false;
				break;
			}
		}

	} // tick

	//---------------------------------------------------------

	bool CCutScene::accept(CMessage *message)
	{
		return message->getType() == Message::ACTIVATE_CUTSCENE && !_cutSceneActivated;
	}

	//---------------------------------------------------------

	void CCutScene::process(CMessage *message)
	{
		// Mensaje de activar un cierto cutScene
		if (message->getType() == Message::ACTIVATE_CUTSCENE)
		{

			// bindeo objeto camara
			ScriptManager::CBinding binder;
			binder.setInstance(_graphicsCamera, "camera");
			binder.setInstance(this, "cutScene");

			// Carga funci�n lua cutScene
			ScriptManager::CScriptManager::getSingletonPtr()->
				executeScript(_scriptName.c_str());

			// Paramos los movimientos del player
			if (_stopPlayer)
			{
				CControlMessage *msg= new CControlMessage();
				msg->_avatarAction = TAvatarActions::STOP_ALL;
				Logic::CServer::getSingletonPtr()->getPlayer()->emitMessage(msg);
				// Desactivamos el control del player
				GUI::CServer::getSingletonPtr()->deactivatePlayerController();
				// Desactivamos el guardado de movimeintos
				Logic::CServer::getSingletonPtr()->getPlayer()->getComponent("CSave")->deactivate();
			}

			// Desactivamos el turbo
			Application::CBaseApplication *app = Application::CBaseApplication::getSingletonPtr();
			app->setMultiplier(1);  // _multiplier igual a 1 o 2
			Graphics::CCompositorManager::getSingletonPtr()->setEnabled("motionBlur", false);

			_count = 0;
			_cutSceneActivated = true;
			_cutSceneNumber = static_cast<CCutSceneMessage *>(message)->_cutSceneNumber;

			// Nos registramos como key listener para poder parar el cutScene en cualquier momento
			GUI::CInputManager::getSingletonPtr()->addKeyListener(this);
		}
	}

	//---------------------------------------------------------

	void CCutScene::cutScene1(unsigned int msecs)
	{
		if(!_init)
		{
			initCutScene();
			// Obtenemos todas las entidades de tipo 'PuertaPhysics' (puertas y plataformas)
			Logic::CServer::getSingletonPtr()->getMap()->getEntitiesByType("PuertaPhysics", _actors);
			_init = true;
		}

		/* Ejecuta un script con el cutScene, si executeFunction modifica result
		a true, indica que el cutscena ha terminado */
		bool result = false;
		ScriptManager::CScriptManager::getSingletonPtr()->
			executeFunction(_functionName.c_str(), _count, msecs, result);

		if (result) finishCutScene();

	} // cutScene1

	//---------------------------------------------------------

	void CCutScene::sendSwitchMessage(int index, bool activate)
	{
		if (index >= _actors.size()) return;

		CSwitchMessage* message = new CSwitchMessage();
		message->_activate = activate;
		(activate) ? message->_int = 1 : message->_int = -1;
		_actors[index]->emitMessage(message);
	}

	//---------------------------------------------------------

	void CCutScene::cutScene2(unsigned int msecs)
	{

	}

	//---------------------------------------------------------

	void CCutScene::finishCutScene2()
	{
		for (unsigned int i = 0; i < _actors.size(); ++i)
		{
			CSwitchMessage* message = new CSwitchMessage();
			message->_activate = false;
			message->_int = -1;
			_actors[i]->emitMessage(message);
		}
	}

	//---------------------------------------------------------

	void CCutScene::finishCutScene()
	{
		// FinishCutScene espec�ficos
		switch (_cutSceneNumber)
		{
		case 1:
			//finishCutScene2();
			break;
		case 2:
			finishCutScene2();
			break;
		default:
			break;
		}
		// Ejecutamos la funcion de finish cutScene de lua
		// las funciones de finish tendr� el nombre finish_*
		// donde * es _functionName
		std::string str = "finish_"; str.append(_functionName);
		ScriptManager::CScriptManager::getSingletonPtr()->
			executeProcedure(str.c_str());

		// Indicamos al GameManager que ya ha sido activado 
		// y que no se vuelva a activar la proxima vez
		bool *value = new bool(); *value = false;
		CGameManager::getSingletonPtr()->addResetInfo(_entity->getName(), value);

		// FinishCutScene global
		GUI::CServer::getSingletonPtr()->activatePlayerController();
		_graphicsCamera->deactivateCutSceneMode();
		Logic::CServer::getSingletonPtr()->getMap()->getEntityByType("Camera")->getComponent("CCamera")->activate();
		Logic::CServer::getSingletonPtr()->getPlayer()->getComponent("CSave")->activate();
		_cutSceneActivated = false;
		_cutSceneNumber = 0;
		_count = 0;
		GUI::CInputManager::getSingletonPtr()->removeKeyListener(this);
		_hudManager->cinemaMode(false);
		if(_musicPlayer) _musicPlayer->setVolume(0.3f);
		//Application::CHudManager::getSingletonPtr()->hudVisible(true);

		// UnBind
		ScriptManager::CBinding binder;
		binder.deleteInstance("camera");
		binder.deleteInstance("cutScene");

	} //finishCutScene

	//---------------------------------------------------------

	void CCutScene::initCutScene()
	{
		Logic::CServer::getSingletonPtr()->getMap()->getEntityByType("Camera")->getComponent("CCamera")->deactivate();
		// Activamos cut scene mode
		_graphicsCamera->activateCutSceneMode();
		if (_isCinemaMode)
		{
			_hudManager->cinemaMode(true, _sceneTitle);
			if(_musicPlayer) _musicPlayer->setVolume(0.7f);
		}
	}

	//---------------------------------------------------------

	bool CCutScene::keyPressed(GUI::TKey key)
	{
		if (_cutSceneActivated)
		{
			if (key.keyId == GUI::Key::RETURN)
			{
				finishCutScene();
				return true;
			}
		}
		return false;
	}

	//----------------------------------------------------------

} // namespace Logic

