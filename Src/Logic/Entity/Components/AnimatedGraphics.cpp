/**
@file AnimatedGraphics.cpp

Contiene la implementaci�n del componente que controla la representaci�n
gr�fica de una entidad est�tica.

@see Logic::CAnimatedGraphics
@see Logic::IComponent

@author Alvaro Blazquez Checa
@author Alejandro P�rez Alonso
@date Septiembre 2014
*/

#include "AnimatedGraphics.h"

#include "Logic/Entity/Entity.h"
#include "Logic/Maps/Map.h"
#include "Map/MapEntity.h"
#include "AvatarController.h"
#include "NPCController.h"

#include "Graphics/Scene.h"

using namespace Logic::Animations;

namespace Logic 
{
	IMP_FACTORY(CAnimatedGraphics);

	//---------------------------------------------------------

	Graphics::CEntity* CAnimatedGraphics::createGraphicsEntity(const Map::CEntity *entityInfo)
	{
		_animatedGraphicsEntity = new Graphics::CAnimatedEntity(_entity->getName(),_model);
		if (!_scene->addEntity(_animatedGraphicsEntity))
			return 0;

		_animatedGraphicsEntity->setTransform(_entity->getTransform());

		if (entityInfo->hasAttribute("setCastShadows"))
		{
			bool value = entityInfo->getBoolAttribute("setCastShadows");
			_animatedGraphicsEntity->setCastShadows(value);

		}

		if (entityInfo->hasAttribute("defaultAnimation")) // Unica animaci�n
		{
			_animatedGraphicsEntity->init(entityInfo->getStringAttribute("defaultAnimation"));
		}
		else // Animaciones separadas en parte de arriba y abajo
		{
			if (entityInfo->hasAttribute("defaultAnimationUp"))
			{
				_defaultAnimation.topName = entityInfo->getStringAttribute("defaultAnimationUp");
			}

			if (entityInfo->hasAttribute("defaultAnimationDown"))
			{
				_defaultAnimation.legsName = entityInfo->getStringAttribute("defaultAnimationDown");
			}
			_animatedGraphicsEntity->init(_defaultAnimation.topName, _defaultAnimation.legsName);
		}

		Graphics::CAnimatedEntity::AnimTimeInfo animTimeInfo;
		std::vector<float> times;

		times.push_back(0.40f);
		times.push_back(0.90f);
		animTimeInfo.insert(std::make_pair("Walk", times));
		times.clear();

		times.push_back(0.0f);
		times.push_back(0.5f);
		//times.push_back(0.99f);

		animTimeInfo.insert(std::make_pair("Run", times));

		times.clear();
		times.push_back(0.315f);
		animTimeInfo.insert(std::make_pair("GetWeaponUp", times));

		_animatedGraphicsEntity->setObserver(this,&animTimeInfo);

		// Guardamos las animaciones
		AnimNames animNames;
		animNames.topName = animNames.legsName = "";

		// IDLE
		if (entityInfo->hasAttribute("idle_up_animation_name"))
			animNames.topName = entityInfo->getStringAttribute("idle_up_animation_name");
		if (entityInfo->hasAttribute("idle_down_animation_name"))
			animNames.legsName = entityInfo->getStringAttribute("idle_down_animation_name");
		_animationNames.insert(std::make_pair(IDLE, animNames));

		// WALK
		animNames.topName = animNames.legsName = "";
		if (entityInfo->hasAttribute("walk_up_animation_name"))
			animNames.topName = entityInfo->getStringAttribute("walk_up_animation_name");
		if (entityInfo->hasAttribute("walk_down_animation_name"))
			animNames.legsName = entityInfo->getStringAttribute("walk_down_animation_name");
		_animationNames.insert(std::make_pair(WALK, animNames));

		// WALK_BACK
		animNames.topName = animNames.legsName = "";
		if (entityInfo->hasAttribute("walkBack_up_animation_name"))
			animNames.topName = entityInfo->getStringAttribute("walkBack_up_animation_name");
		if (entityInfo->hasAttribute("walkBack_down_animation_name"))
			animNames.legsName = entityInfo->getStringAttribute("walkBack_down_animation_name");
		_animationNames.insert(std::make_pair(WALK_BACK, animNames));

		// STRAFE_RIGHT
		animNames.topName = animNames.legsName = "";
		if (entityInfo->hasAttribute("strafeRight_up_animation_name"))
			animNames.topName = entityInfo->getStringAttribute("strafeRight_up_animation_name");
		if (entityInfo->hasAttribute("strafeRight_down_animation_name"))
			animNames.legsName = entityInfo->getStringAttribute("strafeRight_down_animation_name");
		_animationNames.insert(std::make_pair(STRAFE_RIGHT, animNames));

		// STRAFE_LEFT
		animNames.topName = animNames.legsName = "";
		if (entityInfo->hasAttribute("strafeLeft_up_animation_name"))
			animNames.topName = entityInfo->getStringAttribute("strafeLeft_up_animation_name");
		if (entityInfo->hasAttribute("strafeLeft_down_animation_name"))
			animNames.legsName = entityInfo->getStringAttribute("strafeLeft_down_animation_name");
		_animationNames.insert(std::make_pair(STRAFE_LEFT, animNames));

		// RUN
		animNames.topName = animNames.legsName = "";
		if (entityInfo->hasAttribute("run_up_animation_name"))
			animNames.topName = entityInfo->getStringAttribute("run_up_animation_name");
		if (entityInfo->hasAttribute("run_down_animation_name"))
			animNames.legsName = entityInfo->getStringAttribute("run_down_animation_name");
		_animationNames.insert(std::make_pair(RUN, animNames));

		// RUN RIGHT
		animNames.topName = animNames.legsName = "";
		if (entityInfo->hasAttribute("run_right_up_animation_name"))
			animNames.topName = entityInfo->getStringAttribute("run_right_up_animation_name");
		if (entityInfo->hasAttribute("run_right_down_animation_name"))
			animNames.legsName = entityInfo->getStringAttribute("run_right_down_animation_name");
		_animationNames.insert(std::make_pair(RUN_RIGHT, animNames));

		// RUN LEFT
		animNames.topName = animNames.legsName = "";
		if (entityInfo->hasAttribute("run_left_up_animation_name"))
			animNames.topName = entityInfo->getStringAttribute("run_left_up_animation_name");
		if (entityInfo->hasAttribute("run_left_down_animation_name"))
			animNames.legsName = entityInfo->getStringAttribute("run_left_down_animation_name");
		_animationNames.insert(std::make_pair(RUN_LEFT, animNames));

		// JUMP
		animNames.topName = animNames.legsName = "";
		if (entityInfo->hasAttribute("jump_up_animation_name"))
			animNames.topName = entityInfo->getStringAttribute("jump_up_animation_name");
		if (entityInfo->hasAttribute("jump_down_animation_name"))
			animNames.legsName = entityInfo->getStringAttribute("jump_down_animation_name");
		_animationNames.insert(std::make_pair(JUMP, animNames));

		// CROUCH
		animNames.topName = animNames.legsName = "";
		if (entityInfo->hasAttribute("crouch_up_animation_name"))
			animNames.topName = entityInfo->getStringAttribute("crouch_up_animation_name");
		if (entityInfo->hasAttribute("crouch_down_animation_name"))
			animNames.legsName = entityInfo->getStringAttribute("crouch_down_animation_name");
		_animationNames.insert(std::make_pair(CROUCH, animNames));

		// CROUCH WALK
		animNames.topName = animNames.legsName = "";
		if (entityInfo->hasAttribute("walk_crouch_up_animation_name"))
			animNames.topName = entityInfo->getStringAttribute("walk_crouch_up_animation_name");
		if (entityInfo->hasAttribute("walk_crouch_down_animation_name"))
			animNames.legsName = entityInfo->getStringAttribute("walk_crouch_down_animation_name");
		_animationNames.insert(std::make_pair(WALK_CROUCH, animNames));

		// CROUCH WALK BACK
		animNames.topName = animNames.legsName = "";
		if (entityInfo->hasAttribute("walkBack_crouch_up_animation_name"))
			animNames.topName = entityInfo->getStringAttribute("walkBack_crouch_up_animation_name");
		if (entityInfo->hasAttribute("walkBack_crouch_down_animation_name"))
			animNames.legsName = entityInfo->getStringAttribute("walkBack_crouch_down_animation_name");
		_animationNames.insert(std::make_pair(WALK_BACK_CROUCH, animNames));

		// CROUCH STRAFE RIGHT
		animNames.topName = animNames.legsName = "";
		if (entityInfo->hasAttribute("strafe_right_crouch_up_animation_name"))
			animNames.topName = entityInfo->getStringAttribute("strafe_right_crouch_up_animation_name");
		if (entityInfo->hasAttribute("strafe_right_crouch_down_animation_name"))
			animNames.legsName = entityInfo->getStringAttribute("strafe_right_crouch_down_animation_name");
		_animationNames.insert(std::make_pair(STRAFE_RIGHT_CROUCH, animNames));

		// CROUCH STRAFE LEFT
		animNames.topName = animNames.legsName = "";
		if (entityInfo->hasAttribute("strafe_left_crouch_up_animation_name"))
			animNames.topName = entityInfo->getStringAttribute("strafe_right_crouch_up_animation_name");
		if (entityInfo->hasAttribute("strafe_left_crouch_down_animation_name"))
			animNames.legsName = entityInfo->getStringAttribute("strafe_left_crouch_down_animation_name");
		_animationNames.insert(std::make_pair(STRAFE_LEFT_CROUCH, animNames));

		// FIRE
		animNames.topName = animNames.legsName = "";
		if (entityInfo->hasAttribute("fire_animation_name"))
			animNames.topName = entityInfo->getStringAttribute("fire_animation_name");
		animNames.legsName = "";
		_animationNames.insert(std::make_pair(FIRE, animNames));

		// IDLE_WEAPON
		animNames.topName = animNames.legsName = "";
		if (entityInfo->hasAttribute("idleWeapon_animation_name"))
			animNames.topName = entityInfo->getStringAttribute("idleWeapon_animation_name");
		animNames.legsName = "";
		_animationNames.insert(std::make_pair(IDLE_WEAPON, animNames));

		// DEATH
		animNames.topName = animNames.legsName = "";
		if (entityInfo->hasAttribute("death_up_animation_name"))
			animNames.topName = entityInfo->getStringAttribute("death_up_animation_name");
		if (entityInfo->hasAttribute("death_down_animation_name"))
			animNames.legsName = entityInfo->getStringAttribute("death_down_animation_name");
		_animationNames.insert(std::make_pair(DEATH, animNames));

		// DAMAGE
		animNames.topName = animNames.legsName = "";
		if (entityInfo->hasAttribute("damage_animation_name"))
			animNames.topName = entityInfo->getStringAttribute("damage_animation_name");
		animNames.legsName = "";
		_animationNames.insert(std::make_pair(DAMAGE, animNames));

		// PUSH_BUTTON
		animNames.topName = animNames.legsName = "";
		if (entityInfo->hasAttribute("pushButton_animation_name"))
			animNames.topName = entityInfo->getStringAttribute("pushButton_animation_name");
		animNames.legsName = "";
		_animationNames.insert(std::make_pair(PUSH_BUTTON, animNames));

		// CHANGE WEAPON
		animNames.topName = animNames.legsName = "";
		if (entityInfo->hasAttribute("changeWeapon_animation_name"))
			animNames.topName = entityInfo->getStringAttribute("changeWeapon_animation_name");
		animNames.legsName = "";
		_animationNames.insert(std::make_pair(CHANGE_WEAPON, animNames));

		// GET_OBJECT
		animNames.topName = animNames.legsName = "";
		if (entityInfo->hasAttribute("getObject_up_animation_name"))
			animNames.topName = entityInfo->getStringAttribute("getObject_up_animation_name");
		if (entityInfo->hasAttribute("getObject_down_animation_name"))
			animNames.legsName = entityInfo->getStringAttribute("getObject_down_animation_name");
		_animationNames.insert(std::make_pair(GET_OBJECT, animNames));

		// AIM
		animNames.topName = animNames.legsName = "";
		if (entityInfo->hasAttribute("aim_animation_name"))
			animNames.topName = entityInfo->getStringAttribute("aim_animation_name");
		animNames.legsName = "";
		_animationNames.insert(std::make_pair(AIM, animNames));

		return _animatedGraphicsEntity;

	} // createGraphicsEntity

	//---------------------------------------------------------

	bool CAnimatedGraphics::activate()
	{
		CAvatarController* avatarController = (CAvatarController*)_entity->getComponent("CAvatarController");
		if (avatarController)
			avatarController->setListener(this);

		CNPCController* npcController = (CNPCController*)_entity->getComponent("CNPCController");
		if (npcController)
		{
			npcController->setListener(this);
		}

		return CGraphics::activate();
	}

	//---------------------------------------------------------

	void CAnimatedGraphics::deactivate()
	{
		return CGraphics::deactivate();
	}

	//---------------------------------------------------------

	bool CAnimatedGraphics::accept(CMessage *message)
	{
		return CGraphics::accept(message) || 
			message->getType() == Message::SET_ANIMATION ||
			message->getType()== Message::STOP_ANIMATION ||
			message->getType()== Message::SET_ENTITY_CLOCK_MODIFIER;

	} // accept

	//---------------------------------------------------------

	void CAnimatedGraphics::process(CMessage *message)
	{
		CGraphics::process(message);

		switch(message->getType())
		{
		case Message::SET_ANIMATION:
			/*_animationName = static_cast<CAnimationMessage *>(message)->_animationName;
			if(_animationNames.count(_animationName)) {
			setAnimation(_animationName, static_cast<CAnimationMessage *>(message)->_bool);
			}*/
			break;

		case Message::STOP_ANIMATION:
			//_animatedGraphicsEntity->stopAnimation(_animationNames.at(_animationName));
			_animatedGraphicsEntity->stopAllAnimations();
			break;

		case Message::SET_ENTITY_CLOCK_MODIFIER:
			CClockModifierMessage * clockModifierMessage = static_cast<CClockModifierMessage *>(message);
			if(clockModifierMessage->_operation == "SlowDown" 
				|| clockModifierMessage->_operation == "SpeedUp") 
			{
				float time = _animatedGraphicsEntity->getClockModifier() 
					+ clockModifierMessage->_clockModifier;
				if (time < 0) time = 0;
				_animatedGraphicsEntity->setClockModifier(time);
			}
			/*if(clockModifierMessage->_operation == "SlowDown") 
			{
			float time = _animatedGraphicsEntity->getClockModifier() - 0.5;
			if (time < 0) time = 0;
			_animatedGraphicsEntity->setClockModifier(time);
			}
			else if(clockModifierMessage->_operation == "SpeedUp")
			_animatedGraphicsEntity->setClockModifier(_animatedGraphicsEntity->getClockModifier() + 0.5);*/
			else
				_animatedGraphicsEntity->setClockModifier(clockModifierMessage->_clockModifier);

		}

	} // process

	//---------------------------------------------------------

	void CAnimatedGraphics::setAnimation(TAnimations &animation, bool loop)
	{
		if(_animationNames.count(animation)) 
		{
			_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(animation).topName);
		}
	}

	//---------------------------------------------------------

	void CAnimatedGraphics::animationFinished(const std::string &animation)
	{
		//Enviar mensaje de Animation finished
		TAnimations animationName = TAnimations::ANY;

		std::map<TAnimations, AnimNames>::const_iterator itr = _animationNames.begin();
		std::map<TAnimations, AnimNames>::const_iterator end = _animationNames.end();

		for( ; itr != end; ++itr)
		{
			if((*itr).second.topName == animation)
			{
				animationName = (*itr).first;
				break;
			}
		}

		if (animationName == TAnimations::DEATH || animationName == TAnimations::GET_OBJECT || 
			animationName == TAnimations::FIRE)
		{
			CAnimationFinishedMessage *msg = new CAnimationFinishedMessage(animationName);
			_entity->emitMessage(msg, this);
		}
		// Poner animaci�n por defect

		if (_state[DEATH])
			return;
		else if (_state[CHANGE_WEAPON])
			changeWeapon(false);
		else if (_state[DAMAGE])
			damage(false);
		else if (_state[AIM])
			return;
		else if (_state[GET_OBJECT])
			getObject(false);
		else if (_state[PUSH_BUTTON])
			pushButton(false);
		else if (_state[FIRE])
			shooting(false);
		else if(_state[RUN])
			running(true);
		else if(_state[WALK])
			walking(true);
		else if(_state[WALK_BACK])
			walkingBack(true);
		else
			idle(true);



		/******************************************************

		TODO:: Si la animacion == getWeapon se podr�a poner un flag en
		_state[HAS_WEAPON]=1 o si no una nueva variable _hasWeapon. Y notificarle
		de alguna manera al avatarController para que este se encarge de adjuntar
		el arma.

		Con la animacion de muerte yo pondr�a _state[DEATH]=1 y en al principio de
		cada funcion comprobaria que no se cumpliese esto.

		*****************************************************/
	}

	//---------------------------------------------------------

	void CAnimatedGraphics::animationTimeEvent(const std::string &animation)
	{
		TAnimations animationName = TAnimations::ANY;
		std::map<TAnimations, AnimNames>::const_iterator itr = _animationNames.begin();
		std::map<TAnimations, AnimNames>::const_iterator end = _animationNames.end();

		for( ; itr != end; ++itr)
		{
			if((*itr).second.topName == animation)
			{
				animationName = (*itr).first;
				break;
			}
		}

		if(animationName == TAnimations::WALK || animationName == TAnimations::RUN)
		{
			CSoundMessage* msg2 = new CSoundMessage();
			_entity->emitMessage(msg2, this);
#ifdef _DEBUG
			std::cout << "----------------" << std::endl;
			std::cout << "PASO" << std::endl;
			std::cout << "----------------" << std::endl;
			std::cout << "" << std::endl;
#endif
		}

		else if(animationName == TAnimations::GET_OBJECT)
		{
			CAnimationFinishedMessage *msg = new CAnimationFinishedMessage(animationName);
			_entity->emitMessage(msg, this);
		}
		else if (animationName == TAnimations::CHANGE_WEAPON)
		{
			CChangeWeaponMessage *msg = new CChangeWeaponMessage();
			_entity->emitMessage(msg);
		}
	}

	//---------------------------------------------------------

	void CAnimatedGraphics::hasWeapon(bool active)
	{
		_hasWeapon = active;
		if (active)
		{
			if (_state[WALK])
			{
				walking(true);
			}
			else if (_state[WALK_BACK])
			{
				walkingBack(true);
			}
			else if (_state[RUN])
			{
				running(true);
			}
			/*else if (_state[STRAFE_RIGHT])
			{
			_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(STRAFE_RIGHT_WEAPON));
			}
			else if (_state[STRAFE_LEFT])
			{
			_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(STRAFE_LEFT_WEAPON));
			}
			*/
			else if (_state[CROUCH])
			{
				crouch(true);
			}
			else 
			{
				idle(true);
			}
		}
	}

	//---------------------------------------------------------

	void CAnimatedGraphics::damage(bool active)
	{
		_state[DAMAGE] = active;
		if (active)
		{
			_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(DEATH).topName, 0.1, false,  true,
				Graphics::CAnimatedEntity::BODY);
		}
		else if (_state[RUN])
			running(true);
		else if (_state[WALK])
			walking(true);
		else if (_state[WALK_BACK])
			walkingBack(true);
		else if (_state[STRAFE_LEFT])
			strafingLeft(true);
		else if (_state[STRAFE_RIGHT])
			strafingRight(true);
		else if (_state[CROUCH])
			crouch(true);
		else
			idle(true);
	}

	//---------------------------------------------------------

	void CAnimatedGraphics::jumping(bool active)
	{
		if (_state[JUMP] && active) return;
		_state[JUMP] = active;
		if (active)
		{
			if (_state[AIM])
			{
				aim(true);
			}
			else if (_hasWeapon)
			{
				_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(IDLE_WEAPON).topName,
					0.7f, true, false, Graphics::CAnimatedEntity::BODY);
			}
			else
			{

				_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(JUMP).topName,
					0.3f, false, true, Graphics::CAnimatedEntity::BODY);
			}
			_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(JUMP).legsName,
				0.3f, false, true, Graphics::CAnimatedEntity::LEGS);
		}
		else
		{

			if(_state[RUN])
			{
				if (_state[AIM])
				{
					aim(true);
				}
				else if (_hasWeapon)
				{
					_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(IDLE_WEAPON).topName,
						0.3f, true, false, Graphics::CAnimatedEntity::BODY);
				}
				else
				{
					_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(RUN).topName,
						0.3f, true, false, Graphics::CAnimatedEntity::BODY);
				}
				_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(RUN).legsName,
					0.3f, true, false, Graphics::CAnimatedEntity::LEGS);
			}
			else if(_state[WALK])
			{
				if (_state[AIM])
				{
					aim(true);
				}
				else if (_hasWeapon)
				{
					_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(IDLE_WEAPON).topName,
						0.3f, true, false, Graphics::CAnimatedEntity::BODY);
				}
				else
				{

					_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(WALK).topName,
						0.3f, true, false, Graphics::CAnimatedEntity::BODY);
				}
				_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(WALK).legsName,
					0.3f, true, false, Graphics::CAnimatedEntity::LEGS);
			}
			else if(_state[WALK_BACK])
			{
				if (_state[AIM])
				{
					aim(true);
				}
				else if (_hasWeapon)
				{
					_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(IDLE_WEAPON).topName,
						0.3f, true, false, Graphics::CAnimatedEntity::BODY);
				}
				else
				{

					_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(WALK_BACK).topName,
						0.3f, true, false, Graphics::CAnimatedEntity::BODY);
				}
				_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(WALK_BACK).legsName,
					0.3f, true, false, Graphics::CAnimatedEntity::LEGS);
			}
			else
			{
				if (_state[AIM])
				{
					aim(true);
				}
				else if (_hasWeapon)
				{
					_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(IDLE_WEAPON).topName,
						0.7f, true, false, Graphics::CAnimatedEntity::BODY);

				}
				else
				{
					_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(IDLE).topName,
						0.7f, true, false, Graphics::CAnimatedEntity::BODY);
				}
				_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(IDLE).legsName,
					0.7f, true, false, Graphics::CAnimatedEntity::LEGS);
			}

		}
	}

	//---------------------------------------------------------

	void CAnimatedGraphics::walking(bool active)
	{
		_state[WALK] = active;
		if(active)
		{
			if (_state[JUMP]) return;

			//activar las animaciones con sus duraciones de mezcla y demas par�metros
			if (_state[AIM])
			{
				aim(true);
			}
			else if (_state[CROUCH])
			{
				_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(WALK_CROUCH).topName,
					0.3f, true, false, Graphics::CAnimatedEntity::BODY);
				_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(WALK_CROUCH).legsName,
					0.3f, true, false, Graphics::CAnimatedEntity::LEGS);
				return;
			}
			else if (_hasWeapon)
			{
				_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(IDLE_WEAPON).topName,
					0.7f, true, false, Graphics::CAnimatedEntity::BODY);
			}
			else
			{
				_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(WALK).topName,
					0.3f, true, false, Graphics::CAnimatedEntity::BODY);
			}
			_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(WALK).legsName,
				0.3f, true, false, Graphics::CAnimatedEntity::LEGS);
		}
		else
		{
			_state[WALK_BACK] = active;
			if (_state[JUMP]) return;
			if (_state[CROUCH])
			{
				if (_state[AIM])
				{
					_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(IDLE).legsName,
						0.7f, true, false, Graphics::CAnimatedEntity::LEGS);
				}
				crouch(true);
			}
			else if (_state[STRAFE_LEFT])
				strafingLeft(true);
			else if (_state[STRAFE_RIGHT])
				strafingRight(true);
			else
			{
				idle(true);
			}
		}
	}

	//---------------------------------------------------------

	void CAnimatedGraphics:: walkingBack(bool active)
	{
		_state[WALK_BACK] = active;
		if(active)
		{
			if (_state[JUMP]) return;
			if (_state[AIM])
			{
				aim(true);
			}
			else if (_state[CROUCH])
			{
				_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(WALK_BACK_CROUCH).topName,
					0.3f, true, false, Graphics::CAnimatedEntity::BODY);
				_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(WALK_BACK_CROUCH).legsName,
					0.3f, true, false, Graphics::CAnimatedEntity::LEGS);
				return;
			}
			else if (_hasWeapon)
			{
				_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(IDLE_WEAPON).topName,
					0.7f, true, false, Graphics::CAnimatedEntity::BODY);
			}
			else
			{

				_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(WALK_BACK).topName,
					0.4f, true, false, Graphics::CAnimatedEntity::BODY);
			}
			_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(WALK_BACK).legsName,
				0.4f, true, false, Graphics::CAnimatedEntity::LEGS);
		}
		else
		{
			if (_state[JUMP]) return;
			if (_state[CROUCH])
			{
				if (_state[AIM])
				{
					_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(IDLE).legsName,
						0.7f, true, false, Graphics::CAnimatedEntity::LEGS);
				}
				crouch(true);
			}
			else if (_state[STRAFE_LEFT])
				strafingLeft(true);
			else if (_state[STRAFE_RIGHT])
				strafingRight(true);
			else
			{
				idle(true);
			}
		}
	}

	//---------------------------------------------------------

	void CAnimatedGraphics:: idle(bool active)
	{
		_state[IDLE] = active;
		if (_state[AIM])
		{
			aim(true);
		}
		else if (_hasWeapon)
		{
			_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(IDLE_WEAPON).topName,
				0.7f, true, false, Graphics::CAnimatedEntity::BODY);

		}
		else
		{
			_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(IDLE).topName,
				0.7f, true, false, Graphics::CAnimatedEntity::BODY);
		}
		_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(IDLE).legsName,
			0.7f, true, false, Graphics::CAnimatedEntity::LEGS);
	}

	//---------------------------------------------------------

	void CAnimatedGraphics:: running(bool active)
	{
		_state[RUN] = active;

		if(active)
		{
			if (_state[JUMP]) return;
			if (_state[AIM])
			{
				aim(true);
				return;
			}
			else if (_hasWeapon)
			{
				_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(IDLE_WEAPON).topName,
					0.7f, true, false, Graphics::CAnimatedEntity::BODY);
			}
			else
			{
				_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(RUN).topName,
					0.3f, true, false, Graphics::CAnimatedEntity::BODY);
			}
			_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(RUN).legsName,
				0.3f, true, false, Graphics::CAnimatedEntity::LEGS);
		}
		else 
		{
			if (_state[JUMP]) return;
			if(_state[WALK])
			{
				walking(true);
			}
			else
				idle(true);
		}
	}

	//---------------------------------------------------------

	void CAnimatedGraphics:: shooting(bool active)
	{
		_state[FIRE] = active;

		if (active)
		{
			_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(FIRE).topName, 0.7f, false, true,
				Graphics::CAnimatedEntity::BODY);
		}
		else
		{
			if (_state[AIM])
			{
				aim(true);
			}
			else
			{
				_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(IDLE_WEAPON).topName, 0.7f, false, true,
					Graphics::CAnimatedEntity::BODY);
			}
		}
	}

	//---------------------------------------------------------

	void CAnimatedGraphics:: strafingLeft(bool active)
	{
		_state[STRAFE_LEFT] = active;

		if(active)
		{
			if (_state[JUMP]) return;
			if (_state[AIM])
			{
				aim(true);
				return;
			}
			else if (_state[CROUCH])
			{
				_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(STRAFE_LEFT_CROUCH).topName,
					0.3f, true, false, Graphics::CAnimatedEntity::BODY);
				_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(STRAFE_LEFT_CROUCH).legsName,
					0.3f, true, false, Graphics::CAnimatedEntity::LEGS);
				return;
			}
			else if (_hasWeapon)
			{
				_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(IDLE_WEAPON).topName,
					0.7f, true, false, Graphics::CAnimatedEntity::BODY);
			}
			else
			{
				_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(STRAFE_LEFT).topName,
					0.7f, true, false, Graphics::CAnimatedEntity::BODY);

			}
			_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(STRAFE_LEFT).legsName,
				0.7f, true, false, Graphics::CAnimatedEntity::LEGS);

		}
		else
		{
			_state[STRAFE_RIGHT] = _state[STRAFE_LEFT];
			if (_state[JUMP]) return;
			if (_state[CROUCH])
			{
				if (_state[AIM])
				{
					_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(IDLE).legsName,
						0.7f, true, false, Graphics::CAnimatedEntity::LEGS);
				}
				crouch(true);
			}
			else if (_state[RUN])
				running(true);
			else if(_state[WALK])
				walking(true);
			else if(_state[WALK_BACK])
				walkingBack(true);
			else
				idle(true);
		}
	}

	//---------------------------------------------------------

	void CAnimatedGraphics:: strafingRight(bool active)
	{
		_state[STRAFE_RIGHT] = active;

		if(active)
		{
			if (_state[JUMP]) return;
			if (_state[AIM])
			{
				aim(true);
				return;
			}
			else if (_state[CROUCH])
			{
				_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(STRAFE_RIGHT_CROUCH).topName,
					0.3f, true, false, Graphics::CAnimatedEntity::BODY);
				_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(STRAFE_RIGHT_CROUCH).legsName,
					0.3f, true, false, Graphics::CAnimatedEntity::LEGS);
				return;
			}
			else if (_hasWeapon)
			{
				_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(IDLE_WEAPON).topName,
					0.7f, true, false, Graphics::CAnimatedEntity::BODY);
			}
			else
			{

				_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(STRAFE_RIGHT).topName,
					0.7f, true, false, Graphics::CAnimatedEntity::BODY);

			}
			_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(STRAFE_RIGHT).legsName,
				0.7f, true, false, Graphics::CAnimatedEntity::LEGS);
		}
		else
		{
			if (_state[CROUCH])
			{
				if (_state[AIM])
				{
					_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(IDLE).legsName,
						0.7f, true, false, Graphics::CAnimatedEntity::LEGS);
				}
				crouch(true);
			}
			else if (_state[RUN])
				running(true);
			else if(_state[WALK])
				walking(true);
			else if(_state[WALK_BACK])
				walkingBack(true);
			else
				idle(true);
		}
	}

	//---------------------------------------------------------

	void CAnimatedGraphics:: death(bool active)
	{
		_state[DEATH] = active;
		_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(DEATH).topName, 0.1, false,  true,
			Graphics::CAnimatedEntity::BODY);
		_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(DEATH).legsName, 1.0f, false, true,
			Graphics::CAnimatedEntity::LEGS);
	}

	//---------------------------------------------------------

	void CAnimatedGraphics:: crouch(bool active)
	{
		_state[CROUCH] = active;
		if(active)
		{
			if(_state[RUN]) return;
			if (_state[AIM])
			{
				//aim(true);
				return;
			}
			if(_state[WALK])
				walking(true);
			else if(_state[WALK_BACK])
				walkingBack(true);
			else if(_state[STRAFE_LEFT])
				strafingLeft(true);
			else if(_state[STRAFE_RIGHT])
				strafingRight(true);
			else 
			{
				_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(CROUCH).topName,
					0.7f, true, false, Graphics::CAnimatedEntity::BODY);
				_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(CROUCH).legsName,
					0.7f, true, false, Graphics::CAnimatedEntity::LEGS);
			}
		}
		else if(_state[WALK])
			walking(true);
		else if(_state[WALK_BACK])
			walkingBack(true);
		else if(_state[STRAFE_LEFT])
			strafingLeft(true);
		else if(_state[STRAFE_RIGHT])
			strafingRight(true);
		else
			idle(true);
	}

	//---------------------------------------------------------

	void CAnimatedGraphics:: getObject(bool active)
	{
		//_state = 0x0; // Resetamos todos los valores
		_state[GET_OBJECT] = active;

		if (active)
		{
			_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(GET_OBJECT).topName, 1.0f, false, true,
				Graphics::CAnimatedEntity::BODY);
			_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(GET_OBJECT).legsName, 1.0f, false, true,
				Graphics::CAnimatedEntity::LEGS);
		}
		else
		{
			_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(IDLE_WEAPON).topName, 0.7f, true, true,
				Graphics::CAnimatedEntity::BODY);
			_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(IDLE).legsName,
				0.7f, true, true, Graphics::CAnimatedEntity::LEGS);
		}
	}

	//---------------------------------------------------------

	void CAnimatedGraphics::pushButton(bool active)
	{
		_state[PUSH_BUTTON] = active;

		if (active)
		{
			_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(PUSH_BUTTON).topName, 1.0f, false, true,
				Graphics::CAnimatedEntity::BODY);
		}
		else
		{
			if (_hasWeapon)
			{
				_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(IDLE_WEAPON).topName, 0.7f, true, true,
					Graphics::CAnimatedEntity::BODY);
			}
			else
			{
				_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(IDLE).topName, 0.7f, true, true,
					Graphics::CAnimatedEntity::BODY);
			}
		}
	}

	//---------------------------------------------------------

	void CAnimatedGraphics::changeWeapon(bool active)
	{
		_state[CHANGE_WEAPON] = active;
		if (active)
		{
			_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(CHANGE_WEAPON).topName,
				0.7f, false, true, Graphics::CAnimatedEntity::BODY);
		}
		else
		{
			if (_state[AIM])
			{
				aim(true);
			}
			else
			{
				_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(IDLE_WEAPON).topName, 0.7f, true, true,
					Graphics::CAnimatedEntity::BODY);
			}
		}
	}
	//---------------------------------------------------------

	void  CAnimatedGraphics::aim(bool active)
	{
		_state[AIM] = active;
		if (active)
		{
			_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(AIM).topName,
				0.7f, false, true, Graphics::CAnimatedEntity::BODY);
			if (_state[CROUCH])
			{
				_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(IDLE).legsName,
					0.7f, true, false, Graphics::CAnimatedEntity::LEGS);
			}
			if (_state[WALK] || _state[RUN] || _state[STRAFE_RIGHT] || _state[STRAFE_LEFT])
			{
				_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(WALK).legsName,
					0.7f, true, false, Graphics::CAnimatedEntity::LEGS);
			}
			else if (_state[WALK_BACK])
			{
				_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(WALK_BACK).legsName,
					0.7f, true, false, Graphics::CAnimatedEntity::LEGS);
			}
		}
		else
		{
			_animatedGraphicsEntity->setBaseAnimation(_animationNames.at(IDLE_WEAPON).topName, 0.7f, true, true,
				Graphics::CAnimatedEntity::BODY);
			if (_state[RUN])
				running(true);
			else if(_state[CROUCH])
				crouch(true);
			else if(_state[WALK])
				walking(true);
			else if(_state[WALK_BACK])
				walkingBack(true);
			else if(_state[STRAFE_RIGHT])
				strafingRight(true);
			else if(_state[STRAFE_LEFT])
				strafingLeft(true);
			else
				idle(true);
		}
	}

} // namespace Logic
