/**

*/

#include "HudTrigger.h"

#include "Logic/Entity/Entity.h"
#include "Logic/Maps/Map.h"
#include "Map/MapEntity.h"
#include "GUI/HudManager.h"

namespace Logic
{
	IMP_FACTORY(CHudTrigger);

	//---------------------------------------------------------

	bool CHudTrigger::spawn(CEntity *entity, CMap *map, const Map::CEntity *entityInfo) 
	{
		if(!IComponent::spawn(entity,map,entityInfo))
			return false;

		_text =  entityInfo->getStringAttribute("text");

		_showTime  = entityInfo->getFloatAttribute("showTime");

		_character  = (CHARACTER)entityInfo->getIntAttribute("character");

		return true;

	} // spawn

	//---------------------------------------------------------

	bool CHudTrigger::accept(CMessage *message)
	{
		return message->getType() == Message::TOUCHED;

	} // accept

	//---------------------------------------------------------

	void CHudTrigger::process(CMessage *message)
	{
		CTouchedMessage* touchedMessage = static_cast<CTouchedMessage*>(message);
		
		if(!touchedMessage->_entity->isPlayer())
			return;

		switch(touchedMessage->getType())
		{
		case Message::TOUCHED:
			{
				if(_character == CHARACTER::TIME)
				{
					_hudManager->historiaHudTime(true,_text);
				}
				else if(_character == CHARACTER::SPACE)
				{
					_hudManager->historiaHudSpace(true,_text);
				}
				else if(_character == CHARACTER::CREDITS)
				{
					_hudManager->showCredits();
				}
				_showing = true;
				_showTimeLeft = _showTime;
				break;
			}
		}
	} // process

	//---------------------------------------------------------

	void CHudTrigger::tick(unsigned int msecs)
	{
		IComponent::tick(msecs);

		if(_showing && _character != CHARACTER::CREDITS)
		{
			_showTimeLeft -= (msecs/1000.0f);
			if(_showTimeLeft <= 0)
			{
				_showing = false;
				_showTimeLeft = 0;
				if(_character == CHARACTER::TIME)
				{
					_hudManager->historiaHudTime(false);
				}
				else if(_character == CHARACTER::SPACE)
				{
					_hudManager->historiaHudSpace(false);
				}
			}
		}

	} // tick

	//---------------------------------------------------------


	bool CHudTrigger::activate()
	{
		_hudManager = GUI::CHudManager::getSingletonPtr();
		return IComponent::activate();

	} // tick

	//---------------------------------------------------------

	void CHudTrigger::deactivate()
	{
		_hudManager = 0;
		IComponent::deactivate();

	} // tick

	//---------------------------------------------------------
}