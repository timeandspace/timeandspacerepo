/**
@file DynamicEntity.h

Contiene la declaración de la clase DynamicEntity. Se encarga de
recoger y actualizar las propiedades dinamicas de una entidad

@author Alvaro Blazquez Checa
@date Diciembre, 2010
*/
#pragma once

#ifndef __Logic_DynamicEntity_H
#define __Logic_DynamicEntity_H

#include "Logic/Entity/Component.h"
#include "AI/Movement.h"
#include "AI/Server.h"
#include "AI/KinematicMovement.h"

namespace AI
{
	class IMovement;
}

namespace Logic 
{

	class CDynamicEntity : public IComponent
	{
		DEC_FACTORY(CDynamicEntity);

	public:
		
		CDynamicEntity();
		
		~CDynamicEntity(void);
		
		virtual bool spawn(CEntity* entity, CMap *map, const Map::CEntity *entityInfo);

		virtual void tick(unsigned int msecs);

		AI::IMovement::DynamicMovement* getCurrentProperties();

		Vector3 getVelocity();

	private:
		
		AI::IMovement::DynamicMovement* _currentProperties;

		Vector3 _lastPosition;

	};

	REG_FACTORY(CDynamicEntity);

} //namespace Logic

#endif //__Logic_SteeringMovement_H
