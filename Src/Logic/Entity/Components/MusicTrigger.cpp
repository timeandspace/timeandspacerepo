/**

*/

#include "MusicTrigger.h"

#include "Logic/Entity/Entity.h"
#include "Logic/Maps/Map.h"
#include "Map/MapEntity.h"
#include "Logic/Server.h"
#include "MusicPlayer.h"

namespace Logic
{
	IMP_FACTORY(CMusicTrigger);

	//---------------------------------------------------------

	CMusicTrigger::CMusicTrigger() : IComponent(), 
		_musicPlayer(NULL),
		_parameterValue(0.0f)
	{
	}

	//---------------------------------------------------------

	bool CMusicTrigger::spawn(CEntity *entity, CMap *map, const Map::CEntity *entityInfo) 
	{
		if(!IComponent::spawn(entity,map,entityInfo))
			return false;

		assert(entityInfo->hasAttribute("parameterValue") && "Falta parametro para el MusicTrigger");
		_parameterValue = entityInfo->getFloatAttribute("parameterValue");

		assert(entityInfo->hasAttribute("musicPlayerEntityName") && "Falta nombre del MusicPlayer");
		_musicPlayerEntityName = entityInfo->getStringAttribute("musicPlayerEntityName");

		return true;

	} // spawn

	//---------------------------------------------------------

	bool CMusicTrigger::activate()
	{
		Logic::CEntity* ent = Logic::CServer::getSingletonPtr()->getMap()->getEntityByName(_musicPlayerEntityName);
		assert(ent && "Falta music Player en la escena");
		_musicPlayer = (CMusicPlayer*) ent->getComponent("CMusicPlayer");

		return IComponent::activate();
	}

	//---------------------------------------------------------

	void CMusicTrigger::deactivate()
	{
		return IComponent::deactivate();
	}

	//---------------------------------------------------------

	bool CMusicTrigger::accept(CMessage *message)
	{
		return message->getType() == Message::TOUCHED 
			|| message->getType() == Message::UNTOUCHED;

	} // accept

	//---------------------------------------------------------

	void CMusicTrigger::process(CMessage *message)
	{
		switch(message->getType())
		{
		case Message::TOUCHED: // attach entity
			{
				_musicPlayer->setParameter(_parameterValue);
				break;
			}
		case Message::UNTOUCHED: // deattach entity
			{
				
				break;
			}
		}
	} // process

	//---------------------------------------------------------
}