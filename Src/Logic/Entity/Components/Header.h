/**

*/
#ifndef __Logic_Header_H
#define __Logic_Header_H

#include "Logic/Entity/Component.h"

//declaración de la clase
namespace Logic 
{
/**
*/
	class CHeader : public IComponent
	{
		DEC_FACTORY(CHeader);
	public:

		/**
		Constructor por defecto; en la clase base no hace nada.
		*/
		CHeader() : IComponent() {}

		/**
		Destructor de la clase, libera la memoria de la lista de mensajes
		*/
		~CHeader() {}

		/**
		Inicialización del componente usando la descripción de la entidad que hay en 
		el fichero de mapa.
		*/
		virtual bool spawn(CEntity* entity, CMap *map, const Map::CEntity *entityInfo);

		/**
		*/
		virtual bool accept(CMessage *message);

		/**
		*/
		virtual void process(CMessage *message);

		/**
		*/
		virtual void tick(unsigned int msecs);

	protected:
	}; // class CMove

	REG_FACTORY(CHeader);

} // namespace Logic

#endif // __Logic_Save_H
