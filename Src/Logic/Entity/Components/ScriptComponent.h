/**
@file ScriptComponent.h

Contiene la declaraci�n del componente que ejecuta un script
de lua.

@author Alejandro P�rez Alonso
@date Marzo, 2014
*/
#ifndef __Logic_ScriptManager_H
#define __Logic_ScriptManager_H

#include "Logic/Entity/Component.h"

namespace Logic
{
	class CScriptComponent : public IComponent
	{
		DEC_FACTORY(CScriptComponent);
	public:

		/**
		Constructor por defecto
		*/
		CScriptComponent() : IComponent() {}

		/**
		Inicializaci�n del componente usando la descripci�n de la entidad que hay en 
		el fichero de mapa.
		*/
		virtual bool spawn(CEntity* entity, CMap *map, const Map::CEntity *entityInfo);

		/**
		*/
		virtual bool activate();

		/**
		*/
		virtual void deactivate();

		/**
		Este componente s�lo acepta mensajes de tipo ...
		*/
		virtual bool accept(CMessage *message);

		/**
		*/
		virtual void process(CMessage *message);

		/**
		*/
		virtual void tick(unsigned int msecs);

	protected:
		std::string _scriptName;

	}; // class CScriptManager

	REG_FACTORY(CScriptComponent);

} // namespace Logic

#endif //__Logic_ScriptManager_H