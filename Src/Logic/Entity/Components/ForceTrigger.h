/**
@file ForceTrigger.h

Contiene la declaraci�n del componente que envia un mensaje a otra
entidad cuando recibe un mensaje TOUCHED. Le aplica una fuerza
a la otra entidad.

@see Logic::CForceTrigger
@see Logic::IComponent

@author Alejandro Perez Alonso
@date Enero, 2014
*/
#ifndef __Logic_Force_Trigger_H
#define __Logic_Force_Trigger_H

#include "Logic/Entity/Component.h"

//declaraci�n de la clase
namespace Logic 
{
	/**
	Este componente procesa mensajes de tipo TOUCHED o UNTOUCHED (indican que la 
	entidad ha sido tocada o dejada de ser tocada) para enviar un mensaje JUMP a 
	una entidad objetivo.

	@ingroup logicGroup

	@author Alejandro Perez Alonso
	@date Enero, 2014
	*/
	class CForceTrigger : public IComponent
	{
		DEC_FACTORY(CForceTrigger);
	public:

		/**
		Constructor por defecto.
		*/
		CForceTrigger() : IComponent() {}

		/**
		Inicializaci�n del componente usando la descripci�n de la entidad que hay en 
		el fichero de mapa.
		*/
		virtual bool spawn(CEntity* entity, CMap *map, const Map::CEntity *entityInfo);

		/**
		M�todo que se invoca para activar el componente.
		*/
		virtual bool activate();

		/**
		M�todo que se invoca al desactivar el componente.
		*/
		virtual void deactivate();

		/**
		Este componente s�lo acepta mensaje de tipos TOUCHED y UNTOUCHED.
		*/
		virtual bool accept(CMessage *message);

		/**
		Al recibir mensaje TOUCHED emite mensajes para aplicarle fuerzas
		a la entidad objetivo.
		*/
		virtual void process(CMessage *message);

	protected:
		Vector3 _direction; // Direccion hacia la que se aplicar� la fuerza
		float _forceTime;
		float _forceHeight;

	}; // class CSwitchTrigger

	REG_FACTORY(CForceTrigger);

} // namespace Logic

#endif // __Logic_SwitchTrigger_H
