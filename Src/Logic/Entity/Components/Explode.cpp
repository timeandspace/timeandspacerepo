/**
@file Explode.cpp

Contine la implementaci�n del componente que hace "explotar" una entidad y 
aplica un da�o a las entidades que se encuentren dentro de un radio determinado.

@see Logic::IComponent

@author Alejandro P�rez Alonso
@date Mayo, 2014
*/

#include "Explode.h"

#include "Logic/Entity/Entity.h"
#include "Map/MapEntity.h"
#include "Logic/Maps/EntityFactory.h"
#include "Logic/Maps/map.h"
#include "Graphics/Server.h"
#include "Graphics/Entity.h"
#include "ParticleSystem.h"

namespace Logic
{
	IMP_FACTORY(CExplode);

	CExplode::CExplode() : IComponent(), _damage(0.0f), _radius(0.0f), _count(0), _active(false), _force(0.0f)
	{
	}

	//---------------------------------------------------------

	CExplode::~CExplode()
	{

	}

	//---------------------------------------------------------

	bool CExplode::spawn(CEntity *entity, CMap *map, const Map::CEntity *entityInfo)
	{
		if (!IComponent::spawn(entity, map, entityInfo))
			return false;

		_damage = entityInfo->getFloatAttribute("damage");
		_radius = entityInfo->getFloatAttribute("radius");

		if (entityInfo->hasAttribute("radius2"))
		{
			_radius2 = std::max(_radius, entityInfo->getFloatAttribute("radius2"));
			_force = entityInfo->getFloatAttribute("force");
		}

		return true;
	}

	//---------------------------------------------------------

	bool CExplode::activate()
	{
		return IComponent::activate();

	} // activate

	//---------------------------------------------------------

	void CExplode::deactivate()
	{
		IComponent::deactivate();
	} // deactivate

	//---------------------------------------------------------

	bool CExplode::accept(CMessage *message)
	{
		return (message->getType() == Message::CONTROL && 
			(((CControlMessage*)message)->_avatarAction == TAvatarActions::DEATH));

	} // accept

	//---------------------------------------------------------

	void CExplode::process(CMessage *message)
	{
		if (message->getType() ==  Message::CONTROL)
		{
			// Play sistema de part�culas
			CParticleSystem* p = (CParticleSystem*)_entity->getComponent("CParticleSystem");
			Callback callback = MYCALLBACK(&Logic::CExplode::eventCallback);
			p->setCallBack(callback);
			p->start();

			//Ocultamos la entidad
			Graphics::CServer::getSingletonPtr()->getEntity(_entity->getName())->setVisible(false);

			_active = true;
		}

	} // process

	//---------------------------------------------------------

	void CExplode::tick(unsigned int msecs)
	{
		IComponent::tick(msecs);

		if (_active)
		{
			_count += msecs;
			if (_count == 300)
			{
				// Quitar da�o a las entidades que se encuentren dentro de un determinado radio
				std::map<TEntityID, CEntity*>::const_iterator it = _entity->getMap()->getEntities().begin();
				std::map<TEntityID, CEntity*>::const_iterator end = _entity->getMap()->getEntities().end();
				CDamagedMessage *msg = NULL;
				CAddForceMessage *forceMsg = NULL;
				for ( ; it != end; ++it)
				{
					// Entidades dentro del radio exterior, se aplica una fuerza en sentido contrario
					if (_entity->getPosition().positionEquals((*it).second->getPosition(), _radius) &&
						_entity != (*it).second)
					{
						if (forceMsg == NULL)
						{
							forceMsg = new CAddForceMessage();
							forceMsg->_force = _force;
						}
						forceMsg->_direction = (*it).second->getPosition() - _entity->getPosition();
						forceMsg->_direction.normalise();
						(*it).second->emitMessage(forceMsg, this);

						// Entidades dentro del radio interior, se le aplica un da�o
						if (_entity->getPosition().positionEquals((*it).second->getPosition(), _radius) &&
							_entity != (*it).second)
						{
							if (msg == NULL)
							{
								msg = new CDamagedMessage();
								msg->_damage = _damage;
							}
							(*it).second->emitMessage(msg, this);
						}
					}
				}
			}
		}

	} // tick

	//---------------------------------------------------------

	void CExplode::eventCallback(CallbackEvent callbackEvent)
	{
		switch (callbackEvent)
		{
		case PARTICLE_FINISHED:
			{
				// Eliminamos la entidad
				Logic::CEntityFactory::getSingletonPtr()->deferredDeleteEntity(_entity);
				break;
			}
		default:
			break;
		}

	}

	//---------------------------------------------------------
	//---------------------------------------------------------

}