/**
@file AnimatedGraphics.h

Contiene la declaración del componente que controla la representación
gráfica de una entidad estática.

@see Logic::CAnimatedGraphics
@see Logic::CGraphics

@author Alvaro Blazquez
@date Agosto, 2010
*/
#ifndef __Logic_AnimatedGraphics_H
#define __Logic_AnimatedGraphics_H

#include "Graphics.h"
#include "Graphics/AnimatedEntity.h"
#include "Listeners\AvatarContollerListener.h"

namespace Graphics 
{
	class CAnimatedEntity;
}

namespace Logic 
{
	struct AnimNames {
		std::string topName;
		std::string legsName;
	};

	class CAnimatedGraphics : public CGraphics, public Graphics::CAnimatedEntityListener, public IAvatarControllerListener
	{
		DEC_FACTORY(CAnimatedGraphics);
	public:
		/**
		Constructor por defecto; inicializa los atributos a su valor por 
		defecto.
		*/
		CAnimatedGraphics() : 
			CGraphics(), 
			_animatedGraphicsEntity(0),
			//_defaultAnimation("") ,
			_hasWeapon(false),
			_state(0)
		{
		}

		virtual bool accept(CMessage *message);

		virtual void process(CMessage *message);
		
		void animationFinished(const std::string &animation);

		void animationTimeEvent(const std::string &animation);

		virtual bool activate();
		
		virtual void deactivate();


		virtual void jumping(bool active);
		virtual void walking(bool active);
		virtual void idle(bool active);
		virtual void running(bool active);
		virtual void shooting(bool active);
		virtual void strafingLeft(bool active);
		virtual void strafingRight(bool active);
		virtual void death(bool active);
		virtual void walkingBack(bool active);
		virtual void crouch(bool active);
		virtual void getObject(bool active);
		virtual void hasWeapon(bool active);
		virtual void damage(bool active);
		virtual void changeWeapon(bool active);
		virtual void aim(bool active);
		virtual void pushButton(bool active);
	protected:

		std::bitset<32> _state;

		virtual Graphics::CEntity* createGraphicsEntity(const Map::CEntity *entityInfo);

		void setAnimation(TAnimations &animation, bool loop);

		void stopAnimation(); 

		void setEntityClockModifier(); 
		
	
		Graphics::CAnimatedEntity *_animatedGraphicsEntity;

		AnimNames _defaultAnimation;

		std::map<TAnimations, AnimNames> _animationNames;

		TAnimations _animationName;
		bool _hasWeapon;

	}; // class CAnimatedGraphics

	REG_FACTORY(CAnimatedGraphics);

} // namespace Logic

#endif // __Logic_AnimatedGraphics_H
