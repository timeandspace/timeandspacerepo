

#ifndef __Logic_BTExecutor_H
#define __Logic_BTExecutor_H

#include "Logic/Entity/Component.h"

#include "AI/BehaviorTreeBase.h"

namespace AI
{
	class CLatentAction;
	class CBehaviorExecutionContext;
}

namespace Logic
{
	class CBTExecutor : public IComponent
	{
		DEC_FACTORY(CBTExecutor);

	public:
		CBTExecutor();
		~CBTExecutor(void);
		/**
		Inicializaci�n del componente, utilizando la informaci�n extra�da de
		la entidad le�da del mapa (Maps::CEntity). Toma del mapa el atributo
		behavior, que indica el nombre de la m�quina de estado a ejecutar.

		@param entity Entidad a la que pertenece el componente.
		@param map Mapa L�gico en el que se registrar� el objeto.
		@param entityInfo Informaci�n de construcci�n del objeto le�do del
			fichero de disco.
		@return Cierto si la inicializaci�n ha sido satisfactoria.
		*/
		virtual bool spawn(CEntity* entity, CMap *map, const Map::CEntity *entityInfo);
		/**
		M�todo llamado en cada frame que actualiza el estado del componente.
		<p>
		Este m�todo actualiza la m�quina de estado. Si hay un cambio de estado, 
		se actualiza el valor del atributo _currentAction, que es el que contiene
		la acci�n latente que se est� ejecutando. Por �ltimo, se llama al tick de
		la acci�n latente para que avance su ejecuci�n.

		@param msecs Milisegundos transcurridos desde el �ltimo tick.
		*/
		virtual void tick(unsigned int msecs);
		/**
		Este m�todo delega en el m�todo accept de la acci�n latente que se 
		est� ejecutando (_currentAction).
		*/
		virtual bool accept(CMessage *message);
		/**
		Este m�todo delega en el m�todo process de la acci�n latente que se 
		est� ejecutando (_currentAction).
		*/
		virtual void process(CMessage *message);


	private:
		AI::BehaviorTreeNode * _bt;

		AI::CBehaviorExecutionContext * _context;

	}; //class CBTExecutor

	REG_FACTORY(CBTExecutor);

} // namespace Logic

#endif __Logic_BTExecutor_H