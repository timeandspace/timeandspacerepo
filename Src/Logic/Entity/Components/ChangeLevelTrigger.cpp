/**
@file ForceTrigger.cpp

Contiene la implementacion del componente que envia un mensaje a otra
entidad cuando recibe un mensaje TOUCHED. Le aplica una fuerza
a la otra entidad.

@see Logic::CForceTrigger
@see Logic::IComponent

@author The Time & The Space
@date Febrero, 2014
*/

#include "ChangeLevelTrigger.h"

#include "Logic/Entity/Entity.h"
#include "Logic/Maps/Map.h"
#include "Map/MapEntity.h"
#include "Application/BaseApplication.h"
#include "Application/LevelLoader.h"


namespace Logic 
{
	IMP_FACTORY(CChangeLevelTrigger);

	//---------------------------------------------------------

	bool CChangeLevelTrigger::spawn(CEntity *entity, CMap *map, const Map::CEntity *entityInfo) 
	{
		if(!IComponent::spawn(entity,map,entityInfo))
			return false;

		_nextLevel  = entityInfo->getStringAttribute("nextLevel");

		return true;

	} // spawn

	//---------------------------------------------------------

	bool CChangeLevelTrigger::activate()
	{
		return IComponent::activate();;
	} // activate

	//---------------------------------------------------------

	void CChangeLevelTrigger::deactivate()
	{
		IComponent::deactivate();
	} // deactivate

	//---------------------------------------------------------

	bool CChangeLevelTrigger::accept(CMessage *message)
	{
		return message->getType() == Message::TOUCHED;

	} // accept

	//---------------------------------------------------------

	void CChangeLevelTrigger::process(CMessage *message)
	{
		switch(message->getType())
		{
		case Message::TOUCHED:
			// Change Level
			Application::CBaseApplication::getSingletonPtr()->loadLevel(_nextLevel);
			break;
		}

	} // process


} // namespace Logic

