/**
@file Life.cpp

Contiene la implementación del componente que controla la vida de una entidad.

@see Logic::CLife
@see Logic::IComponent

@author David Llansó
@date Octubre, 2010
*/

#include "Life.h"

#include "Logic/Entity/Entity.h"
#include "Logic/Managers/Player Manager/PlayerManager.h"
#include "Logic/Maps/EntityFactory.h"
#include "Map/MapEntity.h"
#include "Application/BaseApplication.h"

namespace Logic 
{
	IMP_FACTORY(CLife);

	//---------------------------------------------------------

	bool CLife::spawn(CEntity *entity, CMap *map, const Map::CEntity *entityInfo) 
	{
		if(!IComponent::spawn(entity,map,entityInfo))
			return false;

		// TODO: 1. Leer atributo "life" del fichero de mapa y almacenar el valor
		// en el atributo privado _life
		if (entityInfo->hasAttribute("life"))
		{
			_life = _maxLife = entityInfo->getIntAttribute("life");
		}


		return true;

	} // spawn

	//---------------------------------------------------------

	bool CLife::activate()
	{
		IComponent::activate();
		if(_entity->isPlayer())
		{
			Logic::CPlayerManager::getSingletonPtr()->setLife(1.0f);
		}
		return true;
	}

	//---------------------------------------------------------

	void CLife::deactivate()
	{
		IComponent::deactivate();
	}

	//---------------------------------------------------------

	bool CLife::accept(CMessage *message)
	{
		
		// Aceptar mensajes del tipo DAMAGED
		return message->getType() == Message::DAMAGED;

	} // accept

	//---------------------------------------------------------

	void CLife::process(CMessage *message)
	{
		switch(message->getType())
		{
			// TODO: 3. Procesar mensajes de tipo DAMAGED
			// Disminuir la vida y mostrar un mensaje de herido en la consola.
			// Si el jugador pierde toda la vida volver al estado de juego "menu".
		case Message::DAMAGED:
			_life -= static_cast<CDamagedMessage *>(message)->_damage;
			CSoundMessage *soundmsg = new CSoundMessage();
			_entity->emitMessage(soundmsg);
#ifdef _DEBUG
			printf("Herido!\n");
#endif
			if(_entity->isPlayer())
			{
				Math::fromDegreesToRadians(1);
				Logic::CPlayerManager::getSingletonPtr()->setLife( Math::clamp(_life / _maxLife,0.0f,1.0f));
			}

			if (_life <= 0)
			{
				//Application::CBaseApplication::getSingletonPtr()->setState("menu");
				CControlMessage *msg = new CControlMessage();
				msg->_avatarAction = TAvatarActions::DEATH;
				_entity->emitMessage(msg,this);

			}
			else
			{
				/*CAnimationMessage * message2 = new CAnimationMessage();
				message2->setType(Message::SET_ANIMATION);
				message2->_animationName = Animations::DAMAGE;
				message2->_bool = false;
				_entity->emitMessage(message2,this);*/
			}
		}

	} // process


} // namespace Logic

