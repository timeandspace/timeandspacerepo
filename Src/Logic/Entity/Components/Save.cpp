/**
@file Save.cpp

@author �lvaro Bl�zquez Checa
@author Alejandro P�rez Alonso

@date Febrero, 2014
*/

#include "Save.h"

#include "Logic/Entity/Entity.h"
#include "Logic/Maps/Map.h"
#include "Logic/Maps/Entityfactory.h"
#include "Map/MapEntity.h"
#include "Application/BaseApplication.h"
#include "Logic/Managers/Replay System/ReplayManager.h"


namespace Logic 
{
	IMP_FACTORY(CSave);

	bool CSave::spawn(CEntity* entity, CMap *map, const Map::CEntity *entityInfo)
	{
		if(!IComponent::spawn(entity,map,entityInfo))
			return false;

		_initialTransform = _entity->getTransform();
		_initialPos = _entity->getPosition();
		_initialYaw = _entity->getYaw();

		return true;
	}

	//---------------------------------------------------------

	bool CSave::accept(CMessage *message)
	{
		if (!isActivated) return false;

		if (message->getType() == Message::CONTROL && ((CControlMessage *)message)->_avatarAction == TAvatarActions::DEATH)
			return false;

		return message->getType() == Message::CONTROL || message->getType() == Message::RESET;
	}

	//---------------------------------------------------------

	void CSave::process(CMessage *message)
	{
		if (message->getType() == Message::CONTROL)
		{
			static_cast<CControlMessage *>(message)->_time = _clock;
			_messageList.push_back(message);
			message->increaseRef();
		}

		if (message->getType() == Message::RESET)
		{
			//_entity->setYaw(_initialYaw);
			_messageList.clear();
			_clock = 0;
		}
	}

	//--------------------------------------------------------- 

	bool CSave::activate() 
	{ 
		CReplayManager::getSingletonPtr()->addControlledEntity(this);
		isActivated = true;
		return IComponent::activate();
	}



	//--------------------------------------------------------- 

	void CSave::deactivate() 
	{ 
		CReplayManager::getSingletonPtr()->removeControlledEntity(this);
		isActivated = false; 
		return IComponent::deactivate();
	}

	//---------------------------------------------------------

	void CSave::pause()
	{
		isActivated = false; 
	}

	//--------------------------------------------------------- 

	void CSave::tick(unsigned int msecs)
	{
		if (isActivated) 
		{
			IComponent::tick(msecs);

			_clock += msecs;
		}
	}

	//---------------------------------------------------------

	void CSave::destroyList()
	{
		if (_messageList.size() > 0)
		{
			CMessageList::const_iterator itr = _messageList.begin();
			CMessageList::const_iterator end = _messageList.end();

			for(; itr != end; ++itr)
			{
				if ((*itr))
					(*itr)->release();
			}
			_messageList.clear();
		}
	}
}
