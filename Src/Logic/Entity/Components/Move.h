/**

*/
#ifndef __Logic_Move_H
#define __Logic_Move_H

#include "Logic/Entity/Component.h"

//declaración de la clase
namespace Logic 
{
	struct Movement {
		unsigned int _numTicks;
		Vector3 _dir;
	};
/**
*/
	class CMove : public IComponent
	{
		DEC_FACTORY(CMove);
	public:

		/**
		Constructor por defecto; en la clase base no hace nada.
		*/
		CMove() : IComponent(), _speed(0.05f) {}
		
		/**
		Inicialización del componente usando la descripción de la entidad que hay en 
		el fichero de mapa.
		*/
		virtual bool spawn(CEntity* entity, CMap *map, const Map::CEntity *entityInfo);

		/**
		*/
		virtual bool accept(CMessage *message);

		/**
		*/
		virtual void process(CMessage *message);

		/**
		*/
		virtual void tick(unsigned int msecs);

	protected:
		float _speed;
		std::list<Movement> _movList;

	}; // class CMove

	REG_FACTORY(CMove);

} // namespace Logic

#endif // __Logic_Move_H
