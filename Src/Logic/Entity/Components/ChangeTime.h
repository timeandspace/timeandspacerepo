/**

*/
#ifndef __Logic_ChangeTime_H
#define __Logic_ChangeTime_H

#include "Logic/Entity/Component.h"

//declaración de la clase
namespace Logic 
{
/**
*/
	class CChangeTime : public IComponent
	{
		DEC_FACTORY(CChangeTime);
	public:

		/**
		Constructor por defecto; en la clase base no hace nada.
		*/
		CChangeTime() : IComponent(), _timeModifier(1.0), _duration(0), _isActived(false) {}

		/**
		Destructor de la clase.
		*/
		~CChangeTime() {}

		/**
		Inicialización del componente usando la descripción de la entidad que hay en 
		el fichero de mapa.
		*/
		virtual bool spawn(CEntity* entity, CMap *map, const Map::CEntity *entityInfo);

		/**
		*/
		virtual bool accept(CMessage *message);

		/**
		*/
		virtual void process(CMessage *message);

		/**
		*/
		virtual void tick(unsigned int msecs);

	protected:
		void updateTime(unsigned int msecs);

		/** Modificador de tiempo (msecs *= _timeModifier) Si > 1 acelera y entre 0 y 1 reduce el tiempo */
		int _timeModifier;

		/** Tiempo que dura el efecto, se ira decrementando y cuando llegue a 0 _timeModifier=1 */
		float _duration;

		bool _isActived;

		bool _slowDown;
	}; // class CMove

	REG_FACTORY(CChangeTime);

} // namespace Logic

#endif // __Logic_ChangeTime_H
