/**
@file Camera.cpp

Contiene la implementaci�n del componente que controla la c�mara gr�fica
de una escena.
 
@see Logic::CCamera
@see Logic::IComponent

@author David Llans�
@date Agosto, 2010
*/

#include "Light.h"

#include "Logic/Server.h"
#include "Logic/Entity/Entity.h"
#include "Logic/Maps/Map.h"
#include "Map/MapEntity.h"

#include "Graphics/Server.h"


namespace Logic 
{
	IMP_FACTORY(CLight);
	
	//---------------------------------------------------------

	bool CLight::spawn(CEntity *entity, CMap *map, const Map::CEntity *entityInfo) 
	{
		if(!IComponent::spawn(entity,map,entityInfo))
			return false;
		

		
		return true;

	} // spawn
	
	//---------------------------------------------------------

	bool CLight::activate()
	{
		
		Graphics::CServer::getSingletonPtr()->createSpotLight(_entity->getName(),"lait");
		return IComponent::activate();

	} // activate
	
	//---------------------------------------------------------

	void CLight::deactivate()
	{

		return IComponent::deactivate();
		
	} // deactivate
	
	//---------------------------------------------------------

	void CLight::tick(unsigned int msecs)
	{
		IComponent::tick(msecs);
		Logic::CEntity* player = Logic::CServer::getSingletonPtr()->getPlayer();
		Vector3 direction = Math::getDirection(player->getOrientation());

		direction *= 40;//

		direction += Vector3(0,10,0); 

		Vector3 playerPos= player->getPosition();
		Vector3 linternaPos = playerPos + direction;

		

		_entity->setPosition( linternaPos);

		Matrix3 orientation = player->getOrientation();

		Matrix4 a = orientation;
		
		
		_entity->setOrientation(orientation);

	} // tick

} // namespace Logic

