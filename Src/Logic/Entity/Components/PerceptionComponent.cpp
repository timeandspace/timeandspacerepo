#include "PerceptionComponent.h"

#include "Logic/Entity/Entity.h"
#include "Map/MapEntity.h"

#include "AI/PerceptionSignal.h"
#include "AI/PerceptionEntity.h"
#include "AI/Sensor.h"
#include "AI/Server.h"
#include "AI/PerceptionEntityFactory.h"


namespace Logic 
{
	IMP_FACTORY(CPerceptionComponent);

	CPerceptionComponent::~CPerceptionComponent(void)
	{
		// Eliminamos la entidad de percepci�n que hemos creado en el spawn
		if (_pEntity)
		{
			delete _pEntity;
			_pEntity = NULL;
		}
	}

	bool CPerceptionComponent::spawn(CEntity* entity, CMap *map, const Map::CEntity *entityInfo)
	{
		// Invocar al m�todo de la clase padre
		if(!IComponent::spawn(entity,map,entityInfo))
			return false;

		// Crear una nueva entidad de percepci�n y a�adir los sensores y se�ales permanentes
		// En este caso utilizamos una factor�a de entidades de percepci�n, a la que le pasamos el
		// tipo de entidad que leemos del mapa
		string entityType = entityInfo->getStringAttribute("perception_entity_type");

		std::map<std::string,void*>* perceptionInfo = new std::map<std::string,void*>();
		perceptionInfo->insert(std::pair<std::string, void*>("entity",_entity));

		float visionAngle;
		float visionDistance;

		if(entityType == "enemy" || entityType == "player")
		{
			//_standHeight = entityInfo->getFloatAttribute("standHeight");
			//_crouchHeight = entityInfo->getFloatAttribute("crouchHeight");

			_standHeight = 2.0;
			_crouchHeight = 1.0;
		}

		if(entityType == "enemy")
		{
			

			//en el txt pasarlo en grados luego el perception entity lo pasa a radianes
			visionAngle = entityInfo->getFloatAttribute("visionAngle");
			visionDistance = entityInfo->getFloatAttribute("visionDistance");
			perceptionInfo->insert(std::pair<std::string, void*>("visionAngle", &visionAngle));
			perceptionInfo->insert(std::pair<std::string, void*>("visionDistance", &visionDistance));
		}
		_pEntity = AI::CPerceptionEntityFactory::getPerceptionEntity(entityType, perceptionInfo, this);

		delete perceptionInfo;

		return true;
	}

	/**
	M�todo que activa el componente; invocado cuando se activa
	el mapa donde est� la entidad a la que pertenece el componente.
	<p>
	En este caso, se registra el componente en el gestor de percepci�n (AI::PerceptionManager)

	@return true si todo ha ido correctamente.
	*/
	bool CPerceptionComponent::activate()
	{

		// Obtenemos la matriz de transformaci�n inicial de la entidad asociada y se la
		// pasamos a la entidad de percepci�n
		// Y registramos la entidad de percepci�n en el gestor
		_transform = _entity->getTransform();
		_pEntity->setTransform(_transform);
		AI::CServer::getSingletonPtr()->getPerceptionManager()->registerEntity(_pEntity);

		return IComponent::activate();
	}

	/**
	M�todo que desactiva el componente; invocado cuando se
	desactiva el mapa donde est� la entidad a la que pertenece el
	componente. Se invocar� siempre, independientemente de si estamos
	activados o no.
	<p>
	Al desactivar el componente la entidad de percepci�n se "desregistra"
	del gestor de percepci�n.
	*/
	void CPerceptionComponent::deactivate()
	{
		// Desregistramos la entidad de percepci�n en el gestor
		AI::CServer::getSingletonPtr()->getPerceptionManager()->unregisterEntity(_pEntity);
		IComponent::deactivate();
	}

	/**
	M�todo llamado en cada frame que actualiza el estado del componente.
	<p>
	En cada tick se comprueba si la entidad l�gica ha modificado su matriz de transformaci�n
	y, si es as�, se actualiza la matriz de transformaci�n de la entidad de percepci�n.

	@param msecs Milisegundos transcurridos desde el �ltimo tick.
	*/
	void CPerceptionComponent::tick(unsigned int msecs)
	{
		// Invocar al m�todo de la clase padre (IMPORTANTE)
		IComponent::tick(msecs);

		// Establecemos la matriz de transformaci�n de la entidad de percepci�n
		// para que sea la misma que la de la entidad l�gica
		if (_updateTransform)
		{
			_pEntity->setTransform(_transform);
			_updateTransform = false;
		}
	}

	/**
	* M�todo invocado por el gestor de percepci�n cuando recibe una notificaci�n de un
	* sensor de la entidad de percepci�n.
	*
	* En este m�todo se incluir�n las acciones que queremos que el componente realice cuando
	* alguno de los sensores asociados percibe una se�al.
	*
	* @param notification Notificaci�n recibida
	*/
	void CPerceptionComponent::notificationPerceived(AI::CNotification* notification)
	{

		// Cuando percibimos algo tenemos que enviar un mensaje de tipo PERCEIVED
		// Enviamos el tipo de sensor/se�al en el campo _int del mensaje
		// Enviamos la entidad l�gica percibida en el campo _entity
		// La entidad percibida est� en el user data de la entidad de percepci�n
		int type = notification->getSensor()->getType();
		CEntity *entity = static_cast<CEntity *>(notification->
			getPerceptionEntity()->getUserData());

		CPerceivedMessage *message = new CPerceivedMessage(type, entity->getName(), entity->getType(),
			notification->isPerceived());
		_entity->emitMessage(message);

#ifdef _DEBUG
		std::string perceptionType;
		if(type==1)
			perceptionType = "VE";
		else if(type==2)
			perceptionType = "OYE";
		else
			perceptionType = "PERCIBE";

		if (notification->isPerceived())
			cout << _entity->getName() << perceptionType <<" A " << entity->getName() << " en " << 
			entity->getPosition() << endl;
		else
			cout << _entity->getName() << " NO " << perceptionType <<" a "<< entity->getName() << endl;
#endif

		// El gestor de percepci�n se desentiende de las notificaciones una vez que las 
		// env�a. Es responsabilidad del receptor eliminarlas.
		delete notification;
	}

	float CPerceptionComponent::getSightHeight()
	{
		if(_crouched)
			return _crouchHeight;
		return _standHeight;
	}


	/**
	M�todo virtual que elige qu� mensajes son aceptados.
	<p>
	Los mensajes que acepta este componente son:
	� SET_TRANSFORM: si se modifica la matriz de transformaci�n de la entidad
	l�gica hay que actualizar tambi�n la de la entidad de percepci�n.
	� ADD_SIGNAL: a�ade una se�al a la entidad de percepci�n. De esta forma
	podemos a�adir se�ales que responden a determinados eventos (por ejemplo, al disparar)
	� ACTIVATE_PERCEPTION: activa o desactiva la percepci�n

	@param message Mensaje a chequear.
	@return true si el mensaje es aceptado.
	*/
	bool CPerceptionComponent::accept(CMessage * message)
	{
		// TODO PR�CTICA IA
		return (message->getType() == Message::SET_TRANSFORM ||
			message->getType() == Message::ADD_SIGNAL ||
			message->getType() == Message::REMOVE_SIGNAL ||
			message->getType() == Message::ACTIVATE_SIGNAL ||
			message->getType() == Message::ACTIVATE_PERCEPTION ||
			message->getType() == Message::CONTROL);
	}

	/**
	M�todo virtual que procesa un mensaje.
	<p>
	Los mensajes que puede procesar este componente son:
	� SET_TRANSFORM: si se modifica la matriz de transformaci�n de la entidad
	l�gica hay que actualizar tambi�n la de la entidad de percepci�n.
	� ADD_SIGNAL: a�ade una se�al a la entidad de percepci�n. De esta forma
	podemos a�adir se�ales que responden a determinados eventos (por ejemplo, al disparar)
	� ACTIVATE_PERCEPTION: activa o desactiva la percepci�n

	@param message Mensaje a procesar.
	*/
	void CPerceptionComponent::process(CMessage * message)
	{

		// SET_TRANSFORM
		// Almacenamos la matriz de transformaci�n para establecerla en la entidad
		// de percepci�n durante su tick.
		// ADD_SIGNAL
		// A�adimos una se�al a la entidad de percepci�n. Las propiedades de la se�al est�n
		// especificadas en los par�metros del mensaje. Con un sistema de mensajes m�s sofisticado
		// tendr�amos m�s flexibilidad.
		// ACTIVATE_PERCEPTION
		// Activamos o desactivamos los sensores y las se�ales de la entidad de percepci�n
		// dependiendo del _bool del mensaje
		if (message->getType() == Message::SET_TRANSFORM)
		{
			_updateTransform = true;
			_transform = static_cast<CSetTransformMessage *>(message)->_transform;
		}
		else if (message->getType() == Message::ADD_SIGNAL)
		{
			CAddSignalMessage *msg = static_cast<CAddSignalMessage *>(message);
			_pEntity->addSignal(new AI::CPerceptionSignal(_pEntity, msg->_signalType, 
				msg->_intensity, 0.0f, true, msg->_keepAlive, msg->_name));
		}
		else if (message->getType() == Message::REMOVE_SIGNAL)
		{
			CRemoveSignalMessage *msg = static_cast<CRemoveSignalMessage *>(message);
			_pEntity->removeSignal(msg->_name);
		}
		else if (message->getType() == Message::ACTIVATE_PERCEPTION)
		{
			CActivatePerceptionMessage *msg = 
				static_cast<CActivatePerceptionMessage *>(message);

			_pEntity->setSensorsActive(msg->_active);
			_pEntity->setSignalsActive(msg->_active);
		}
		else if (message->getType() == Message::ACTIVATE_SIGNAL)
		{
			CActivateSignalMessage *msg = static_cast<CActivateSignalMessage *>(message);
			_pEntity->setSignalActive(msg->_name, msg->_activate);
		}
		else if (message->getType() == Message::CONTROL)
		{
			CControlMessage *msg = static_cast<CControlMessage *>(message);
			if(msg->_avatarAction == TAvatarActions::CROUCH)
				_crouched = true;
			else if(msg->_avatarAction == TAvatarActions::STOP_CROUCH)
				_crouched = false;
		}

	}
} // namespace Logic 

