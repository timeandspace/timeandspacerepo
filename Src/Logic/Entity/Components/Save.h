/**
@file Save.h

@author �lvaro Bl�zquez Checa
@author Alejandro P�rez Alonso

@date Febrero, 2014
*/
#ifndef __Logic_Save_H
#define __Logic_Save_H

#include "Logic/Entity/Component.h"

//declaraci�n de la clase
namespace Logic 
{
/**
*/

	class CSave : public IComponent
	{
		DEC_FACTORY(CSave);
	public:

		/**
		Constructor por defecto; en la clase base no hace nada.
		*/
		CSave() : IComponent(), _clock(0), isActivated(true) {}

		/**
		Destructor de la clase, libera la memoria de la lista de mensajes
		*/
		~CSave() 
		{
			destroyList();
		}

		/**
		Inicializaci�n del componente usando la descripci�n de la entidad que hay en 
		el fichero de mapa.
		*/
		virtual bool spawn(CEntity* entity, CMap *map, const Map::CEntity *entityInfo);

		/**
		*/
		virtual bool activate();

		/**
		*/
		virtual void deactivate();

		void pause();

		/**
		*/
		virtual bool accept(CMessage *message);

		/**
		*/
		virtual void process(CMessage *message);

		/**
		*/
		virtual void tick(unsigned int msecs);

		/**
		*/
		Matrix4 getInitialTransform() { return _initialTransform;}

		/**
		*/
		Vector3 getInitialPosition() { return _initialPos;}

		/**
		*/
		CMessageList& getMessageList() { return _messageList;}

		/**
		*/
		unsigned long getClock() { return _clock; }

		/**
		*/
		void setClock(unsigned long clock) { _clock = clock; }

	protected:

		/**
		Libera la lista de mensajes.
		*/
		void destroyList();

		CMessageList _messageList;
		Matrix4 _initialTransform; 
		Vector3 _initialPos;
		float _initialYaw;
		unsigned long _clock;
	
		TMessageType _typeToAccept;

		bool isActivated;
		
	}; // class CMove

	REG_FACTORY(CSave);

} // namespace Logic

#endif // __Logic_Save_H
