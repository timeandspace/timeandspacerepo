/**
@file FadeOut.h

Contiene la declaraci�n del componente que hace que una entidad se desvanezca con el tiempo.
Modifica el parametro alpha del material.

@author Alejandro P�rez Alonso

@date Junio, 2014
*/
#ifndef __Logic_Fade_Out_H
#define __Logic_Fade_Out_H

#include "Logic/Entity/Component.h"

namespace Graphics
{
	class CEntity;
}

namespace Logic
{

	class CFadeOut : public IComponent
	{
		DEC_FACTORY(CExplode);
	public:
		CFadeOut() : IComponent(), _value(0), _enabled(false), _graphicsEntity(NULL)
		{
		}
		
		virtual ~CFadeOut() {}

		virtual bool spawn(CEntity* entity, CMap *map, const Map::CEntity *entityInfo);

		virtual bool activate();

		virtual void deactivate();

		virtual void tick(unsigned int msecs);

		virtual bool accept(CMessage *message);

		virtual void process(CMessage *message);

	private:
		/**
		Puntero a la entida gr�fica
		*/
		Graphics::CEntity *_graphicsEntity;
		/**
		Bool auxiliar para contralar si el efecto se ha activado o no
		*/
		bool _enabled;
		/**
		Valor [0, 1]
		*/
		float _value;

		std::string _materialName;

	}; // class CFadeOut

	REG_FACTORY(CFadeOut);

} // namespace Logic

#endif //__Logic_Fade_Out_H