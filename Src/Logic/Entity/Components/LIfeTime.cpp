/**
@file LifeTime.h

Contiene la implemtacion de una clase que elimina la entidad
pasados x segundos definidos en el map.txt

@author Alejandro P�rez Alonso
@date Marzo, 2014
*/

#include "LifeTime.h"

#include "Logic/Maps/EntityFactory.h"
#include "Map/MapEntity.h"

namespace Logic 
{
	IMP_FACTORY(CLifeTime);

	bool CLifeTime::spawn(CEntity* entity, CMap *map, const Map::CEntity *entityInfo)
	{
		if (!IComponent::spawn(entity,map,entityInfo))
			return false;

		if (entityInfo->hasAttribute("lifetime"))
			_lifeTime = entityInfo->getIntAttribute("lifetime");

		return true;
	}

	//---------------------------------------------------------

	void CLifeTime::tick(unsigned int msecs)
	{
		// Invocar al m�todo de la clase padre
		IComponent::tick(msecs);
		_lifeTime -= msecs;
		if (_lifeTime <= 0)
		{
			// Marcar entidad para que sea borrada
			Logic::CEntityFactory::getSingletonPtr()->deferredDeleteEntity(_entity);
		}
	}

	//---------------------------------------------------------
}