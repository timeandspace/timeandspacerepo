
#ifndef __Logic_Special_Action_Trigger_H
#define __Logic_Special_Action_Trigger_H

#include "Logic/Entity/Component.h"

namespace GUI
{
	class CHudManager;
}

namespace Logic 
{
	
	class CHudTrigger : public IComponent
	{
		DEC_FACTORY(CHudTrigger);
	public:

		/**
		Constructor por defecto.
		*/
		CHudTrigger() : IComponent(), _showTime(0.0f), _showing(false), _text(""),
			_hudManager(0), _showTimeLeft(0.0f) {}

		/**
		Inicializaci�n del componente usando la descripci�n de la entidad que hay en 
		el fichero de mapa.
		*/
		virtual bool spawn(CEntity* entity, CMap *map, const Map::CEntity *entityInfo);

		/**
		Este componente s�lo acepta mensaje de tipos TOUCHED y UNTOUCHED.
		*/
		virtual bool accept(CMessage *message);

		/**
		Al recibir mensaje TOUCHED
		*/
		virtual void process(CMessage *message);

		virtual void tick(unsigned int msecs);

		virtual bool activate();

		virtual void deactivate();

	protected:

		enum CHARACTER
		{
			TIME,
			SPACE,
			CREDITS
		};
		
		float _showTime;

		float _showTimeLeft;

		std::string _text;

		bool _showing;

		CHARACTER _character; // TIME o SPACE

		GUI::CHudManager* _hudManager;

	}; // class CSwitchTrigger

	REG_FACTORY(CHudTrigger);

} // namespace Logic

#endif // __Logic_Sticky_Floor_H
