/**
@file CutSceneTrigger.cpp

Contiene la implementacion del componente que activa una cut scene

@see Logic::IComponent

@author Alejandro P�rez Alonso
@date Mayo, 2014
*/

#include "CutSceneTrigger.h"

#include "Logic/Entity/Entity.h"
#include "Logic/Maps/Map.h"
#include "Map/MapEntity.h"
#include "Application/BaseApplication.h"
#include "Application/LevelLoader.h"
#include "Logic/Managers/Replay System/ReplayManager.h"
#include "Logic/Managers/GameManager.h"

namespace Logic 
{
	IMP_FACTORY(CCutSceneTrigger);

	//---------------------------------------------------------

	bool CCutSceneTrigger::spawn(CEntity *entity, CMap *map, const Map::CEntity *entityInfo) 
	{
		if(!IComponent::spawn(entity,map,entityInfo))
			return false;

		_cutSceneNumber  = entityInfo->getIntAttribute("cutScene_number");

		return true;

	} // spawn

	//---------------------------------------------------------

	bool CCutSceneTrigger::activate()
	{
		// Comprobamos si ya fue activado
		if (CGameManager::getSingletonPtr()->hasResetInfo(_entity->getName()))
		{
			this->_activated = *(bool*) (CGameManager::getSingletonPtr()->
				getResetInfo(_entity->getName()));
			return true;
		}

		return IComponent::activate();;
	} // activate

	//---------------------------------------------------------

	void CCutSceneTrigger::deactivate()
	{
		IComponent::deactivate();
	} // deactivate

	//---------------------------------------------------------

	bool CCutSceneTrigger::accept(CMessage *message)
	{
		return message->getType() == Message::TOUCHED;

	} // accept

	//---------------------------------------------------------

	void CCutSceneTrigger::process(CMessage *message)
	{
		switch(message->getType())
		{
		case Message::TOUCHED:
			Logic::CEntity *ent = static_cast<CTouchedMessage *>(message)->_entity;
			if (ent->isPlayer() || ent->getType() == "PlayerClone")
			{
				CCutSceneMessage *msg = new CCutSceneMessage(_cutSceneNumber);
				_entity->emitMessage(msg);
				deactivate();
				break;
			}
		}

	} // process


} // namespace Logic

