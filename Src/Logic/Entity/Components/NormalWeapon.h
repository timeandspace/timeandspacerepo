/**
@file NormalWeapon.h

@see Logic::CWeapon

@author Alejandro P�rez Alonso
@date Mayo, 2014
*/

#ifndef __Logic_NormalWeapon_H
#define __Logic_NormalWeapon_H

#include "Weapon.h"

namespace Logic
{

	class CNormalWeapon : public CWeapon
	{
		DEC_FACTORY(CNormalWeapon);

	public:
		/**
		*/
		CNormalWeapon();
		
		/**
		*/
		virtual ~CNormalWeapon();

		/**
		*/
		virtual bool spawn(CEntity* entity, CMap *map, const Map::CEntity *entityInfo);

		/**
		*/
		virtual bool activate();

		/**
		*/
		virtual void deactivate();

		/**
		*/
		virtual bool accept(CMessage *message);

		/**
		*/
		virtual void process(CMessage *message);

		/**
		*/
		virtual void tick(unsigned int msecs);

		/**
		*/
		virtual void fire(const Vector3 &impactPoint, const std::string &hitEntityName);

		/**
		*/
		virtual void attachWeapon(const std::string &entityName, const std::string &boneName, 
			Quaternion offsetOrientation, Vector3 offsetPosition);

		/**
		*/
		virtual void detachWeapon();

	protected:
		/**
		*/
		void createBullet(const Vector3 &impactPoint, const Vector3 &direction);

		Map::CEntity* _bulletEntityInfo;

		std::string _bulletEntityName;

		float _damage;

		int _id;

	}; // Class Normal CWeapon

	REG_FACTORY(CNormalWeapon);

} // namespace Logic

#endif //__Logic_NormalWeapon_H