/**
@file Camera.cpp

Contiene la implementaci�n del componente que controla la c�mara gr�fica
de una escena.

@see Logic::CCamera
@see Logic::IComponent

@author Alejandro P�rez Alonso
@date Junio, 2014
*/

#include "Camera.h"

#include "Logic/Server.h"
#include "Logic/Entity/Entity.h"
#include "Logic/Maps/Map.h"
#include "Map/MapEntity.h"

#include "Graphics/Scene.h"
#include "Graphics/Camera.h"

#include "Physics/Server.h"


namespace Logic 
{
	IMP_FACTORY(CCamera);

	//---------------------------------------------------------

	bool CCamera::spawn(CEntity *entity, CMap *map, const Map::CEntity *entityInfo) 
	{
		if(!IComponent::spawn(entity,map,entityInfo))
			return false;

		_graphicsCamera = _entity->getMap()->getScene()->getCamera();
		if(!_graphicsCamera)
			return false;

		float near = -1;
		float far = -1;

		if(entityInfo->hasAttribute("nearClip"))
			near = entityInfo->getFloatAttribute("nearClip");

		if(entityInfo->hasAttribute("farClip"))
			far = entityInfo->getFloatAttribute("farClip");

		if(near != -1 && far != -1)
			_graphicsCamera->setClipDistance(near,far);

		if(entityInfo->hasAttribute("isStatic"))
			_isStatic = entityInfo->getBoolAttribute("isStatic");

		if(entityInfo->hasAttribute("lookAt"))
			_lookAt = entityInfo->getVector3Attribute("lookAt");

		if(entityInfo->hasAttribute("distance"))
			_distance = entityInfo->getFloatAttribute("distance");

		if(entityInfo->hasAttribute("height"))
			_height = entityInfo->getFloatAttribute("height");

		if(entityInfo->hasAttribute("targetDistance"))
			_targetDistance = entityInfo->getFloatAttribute("targetDistance");

		if(entityInfo->hasAttribute("targetHeight"))
			_targetHeight = entityInfo->getFloatAttribute("targetHeight");

		if(entityInfo->hasAttribute("offset"))
		{
			_offset = entityInfo->getFloatAttribute("offset");
			_offsetAux = 0;
		}
		if(entityInfo->hasAttribute("offsetSpeed"))
		{
			_offsetSpeed = entityInfo->getFloatAttribute("offsetSpeed");
		}


		if(entityInfo->hasAttribute("zoom"))
		{
			_zoomDistance = entityInfo->getFloatAttribute("zoom");
			if (_zoomDistance > _distance) _zoomDistance = _distance;
			_zoomDistanceAux = _distance;
		}

		if(entityInfo->hasAttribute("zoomSpeed"))
		{
			_zoomSpeed = entityInfo->getFloatAttribute("zoomSpeed");
		}

		// Puntero al servidor de f�sica para ahorrar llamar a getSinglentonPtr en el tick
		_physicsServer = Physics::CServer::getSingletonPtr();

		return true;

	} // spawn

	//---------------------------------------------------------

	bool CCamera::activate()
	{
		if(!_isStatic)
			_target = CServer::getSingletonPtr()->getPlayer();
		else
		{
			_target = 0;
			_graphicsCamera->setCameraPosition(_entity->getPosition());
			_graphicsCamera->setTargetCameraPosition(_lookAt);
		}

		const Vector3* worldSpaceCorners = _graphicsCamera->getWorldSpaceCorners();
		// top-right near, top-left near, bottom-left near, bottom-right near, top-right far, top-left far, bottom-left far, bottom-right far.
		_nearFrustrum.x = (worldSpaceCorners[0] - worldSpaceCorners[1]).length();
		_nearFrustrum.y = (worldSpaceCorners[0] - worldSpaceCorners[2]).length();
		_nearFrustrum.z = 0.2;

		return IComponent::activate();

	} // activate

	//---------------------------------------------------------

	void CCamera::deactivate()
	{
		_target = 0;
		IComponent::deactivate();

	} // deactivate

	//---------------------------------------------------------

	void CCamera::tick(unsigned int msecs)
	{
		IComponent::tick(msecs);

		if(_target)
		{
			// Actualizamos la posici�n de la c�mara.
			Vector3 position = _target->getPosition();
			Vector3 direction;
			if (_cameraType == TCameraType::AIMING)
			{
				_zoomDistanceAux -= _zoomSpeed;
				if (_zoomDistanceAux < _zoomDistance) 
					_zoomDistanceAux = _zoomDistance;
			}
			else
			{
				_zoomDistanceAux += _zoomSpeed;
				if (_zoomDistanceAux > _distance) 
					_zoomDistanceAux = _distance;
			}

			direction = -_zoomDistanceAux * (Math::getDirection(_target->getOrientation()));

			if (_cameraType == TCameraType::WITH_WEAPON || 
				_cameraType == TCameraType::AIMING)
			{
				_offsetAux -= _offsetSpeed;
				if (_offsetAux < _offset) _offsetAux = _offset;
				position += _offsetAux * Math::getDirection(_target->getYaw() + Math::PI/2);
			}

			// Aplicamos pitch
			float pitch = _target->getPitch();
			if (_cameraType == TCameraType::AIMING) 
			{
				//pitch /= 2; // Reducimos el rango de movimiento vertical
				/*if (pitch > Math::PI / 8) pitch = Math::PI / 8;
				else if(pitch < -Math::PI / 12) pitch = -Math::PI / 12;*/
			}
			//direction.y = _height;
			direction.y = _height - pitch * _height;

			_graphicsCamera->setCameraPosition(position + direction);

			// Y la posici�n hacia donde mira la c�mara.
			direction = _targetDistance * Math::getDirection(_target->getOrientation());
			//direction.y = _targetHeight;
			direction.y = _targetHeight + pitch / 2 * _targetHeight;
			_graphicsCamera->setTargetCameraPosition(position + direction);

			// Lanzamos rayos para detectar posibles obst�culos entre la c�mara y el target
			Vector3 origin = _graphicsCamera->getTargetCameraPosition();
			direction = _graphicsCamera->getCameraPosition() - origin;
			float maxDist = direction.normalise();

			Ray ray(origin, direction);
			Physics::Collision collision;
			if (_physicsServer->sweepSingle(ray, maxDist, Physics::FilterGroup::WORLD, collision, _nearFrustrum))
			//if (_physicsServer->raycastSingle(ray, maxDist, Physics::FilterGroup::WORLD, collision))
			{
				if (collision.distance > _targetDistance)
					_graphicsCamera->setCameraPosition(origin + collision.distance * direction);
				//_graphicsCamera->setCameraPosition(collision.position);
			}
		}

	} // tick

	//---------------------------------------------------------

	bool CCamera::accept(CMessage *message)
	{
		return message->getType() == Message::CHANGE_CAMERA;
	}

	//---------------------------------------------------------

	void CCamera::process(CMessage *message)
	{
		if (message->getType() == Message::CHANGE_CAMERA)
		{
			CCameraMessage *msg = static_cast<CCameraMessage *>(message);
			_cameraType = msg->_cameraType;
			if (_cameraType == TCameraType::MAP)
			{
				_target = 0;
				_graphicsCamera->setCameraPosition(_entity->getPosition());
				_graphicsCamera->setTargetCameraPosition(_lookAt);
			}
		}
	}

} // namespace Logic

