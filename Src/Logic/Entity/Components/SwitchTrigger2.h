/**
@file SwitchTrigger2.h

Contiene la declaraci�n del componente que envia un mensaje SWITCH a otra
entidad cuando recibe un mensaje TOUCHED / UNTOUCHED.

@see Logic::CSwitchTrigger
@see Logic::IComponent

@author David Llans�
@date Octubre, 2010
*/
#ifndef __Logic_SwitchTrigger2_H
#define __Logic_SwitchTrigger2_H

#include "Logic/Entity/Component.h"

//declaraci�n de la clase
namespace Logic 
{
/**
	Este componente procesa mensajes de tipo TOUCHED o UNTOUCHED (indican que la 
	entidad ha sido tocada o dejada de ser tocada) para enviar un mensaje SWITCH a 
	una entidad objetivo.
	<p>
	La entidad objetivo se especifica en el mapa con el atributo "target". Este 
	atributo <em>debe</em> ser especificado.
	
    @ingroup logicGroup

	@author David Llans� Garc�a
	@date Octubre, 2010
*/
	class CSwitchTrigger2 : public IComponent
	{
		DEC_FACTORY(CSwitchTrigger2);
	public:

		/**
		Constructor por defecto.
		*/
		CSwitchTrigger2() : IComponent() {}
		
		/**
		Inicializaci�n del componente usando la descripci�n de la entidad que hay en 
		el fichero de mapa.
		*/
		virtual bool spawn(CEntity* entity, CMap *map, const Map::CEntity *entityInfo);

		/**
		M�todo que se invoca para activar el componente.
		*/
		virtual bool activate();
		
		/**
		M�todo que se invoca al desactivar el componente.
		*/
		virtual void deactivate();

		/**
		Este componente s�lo acepta mensaje de tipos TOUCHED y UNTOUCHED.
		*/
		virtual bool accept(CMessage *message);

		/**
		Al recibir mensaje TOUCHED y UNTOUCHED emite mensajes SWITCH
		a la entidad objetivo para que cambie de posici�n.
		*/
		virtual void process(CMessage *message);

		virtual void tick(unsigned int msecs);

	protected:

		void onTriggerEnter();

		/**
		Nombre de la entidad a la que se desea enviar un SWITCH cuando se 
		recibe un mensaje ACTION.
		*/
		std::vector<std::string> _targetNames;
		
		/**
		Lista de targets que realizar�n la acci�n opuesta al target
		*/
		std::vector<std::string> _opositeTargets;


	}; // class CSwitchTrigger2

	REG_FACTORY(CSwitchTrigger2);

} // namespace Logic

#endif // __Logic_SwitchTrigger2_H
