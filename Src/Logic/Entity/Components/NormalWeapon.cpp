/**
@file NormalWeapon.cpp

@see Logic::CWeapon

@author Alejandro P�rez Alonso
@date Mayo, 2014
*/
#include "NormalWeapon.h"

#include "Logic/Entity/Entity.h"
#include "Map/MapEntity.h"
#include "Physics/Server.h"
#include "Logic/Maps/EntityFactory.h"
#include "Logic/Maps/map.h"
#include "Graphics/Server.h"
#include "SimpleAnimatedGraphics.h"
#include "ParticleSystem.h"

namespace Logic
{

	IMP_FACTORY(CNormalWeapon);

	//---------------------------------------------------------

	CNormalWeapon::CNormalWeapon() : 
		CWeapon(),
		_bulletEntityInfo(NULL),
		_bulletEntityName(""),
		_damage(0.0f),
		_id(0)
	{
	}

	//---------------------------------------------------------

	CNormalWeapon::~CNormalWeapon()
	{
		if(_bulletEntityInfo)
		{
			delete _bulletEntityInfo;
			_bulletEntityInfo = NULL;
		}
	}

	//---------------------------------------------------------

	bool CNormalWeapon::spawn(CEntity* entity, CMap *map, const Map::CEntity *entityInfo)
	{
		if (!CWeapon::spawn(entity, map, entityInfo))
			return false;

		assert(entityInfo->hasAttribute("bulletEntityName") && "Falta atributo");
		_bulletEntityName = entityInfo->getStringAttribute("bulletEntityName");

		assert(entityInfo->hasAttribute("damage") && "Falta atributo");
		_damage = entityInfo->getFloatAttribute("damage");

		return true;
	}


	//---------------------------------------------------------

	bool CNormalWeapon::activate()
	{
		CWeapon::activate();
		if(!_init)
		{
			_bulletEntityInfo = Logic::CEntityFactory::getSingletonPtr()->getMapEntityByName(_bulletEntityName);
			_bulletEntityInfo = new Map::CEntity(*_bulletEntityInfo);
			_init = true;
		}

		return IComponent::activate();
	}

	//---------------------------------------------------------

	void CNormalWeapon::deactivate()
	{
		CWeapon::deactivate();
		
		return IComponent::deactivate();
	}

	//---------------------------------------------------------

	bool CNormalWeapon::accept(CMessage *message)
	{
		return CWeapon::accept(message);
	}

	//---------------------------------------------------------

	void CNormalWeapon::process(CMessage *message)
	{
		CWeapon::process(message);
	}

	//---------------------------------------------------------

	void CNormalWeapon::tick(unsigned int msecs)
	{
		CWeapon::tick(msecs);

		// Rate fire
	}

	//---------------------------------------------------------

	void CNormalWeapon::attachWeapon(const std::string &entityName, const std::string &boneName, 
		Quaternion offsetOrientation = Quaternion::IDENTITY, Vector3 offsetPosition = Vector3::ZERO)
	{
		// Orientacion del arma
		/*Ogre::Quaternion offsetOrientation(Ogre::Degree(-70), Vector3::UNIT_Y);
		Quaternion qX(Ogre::Degree(45), Vector3::UNIT_X);
		Quaternion qZ(Ogre::Degree(20), Vector3::UNIT_Z);
		offsetOrientation = offsetOrientation * qX * qZ;*/
		// Attach
		if (offsetOrientation !=  Quaternion::IDENTITY)
			_offsetOrientation = offsetOrientation;
		if (offsetPosition != Vector3::ZERO)
			_offsetPosition = offsetPosition;

		// Attach
		Graphics::CServer::getSingletonPtr()->getEntity(entityName)->
			attachObjectToBone(boneName, _entity->getName(), _offsetOrientation, _offsetPosition);
	}

	//---------------------------------------------------------

	void CNormalWeapon::detachWeapon()
	{
		Graphics::CServer::getSingletonPtr()->getEntity(_entity->getName())->detachObject(_entity->getName());
	}

	//---------------------------------------------------------

	void CNormalWeapon::fire(const Vector3 &impactPoint, const std::string &hitEntityName)
	{
		// He cambiado esto aqui en lugar de en el tick porque petaba al matar a una copia que tenia un arma enganchada
		_shootingPoint = Graphics::CServer::getSingletonPtr()->getBonePosition(_entity->getName(), _shootingPointBone);

		// El arma normal, las balas son instant�neas
		CDamagedMessage *m = new CDamagedMessage();
		m->_damage = _damage;
		CEntity *hitEntity = _entity->getMap()->getEntityByName(hitEntityName);


		// Aparte de enviar da�o enviamos un mensaje de aplicar fuerza
		// para reventar barriles basicamente.
		CAddForceMessage *forceMessage = new CAddForceMessage();
		Vector3 direction = impactPoint - _shootingPoint;
		direction.normalise();
		forceMessage->_direction = direction;
		forceMessage->_force = 1000.0f;

		if (hitEntity)
		{
			hitEntity->emitMessage(m);
			hitEntity->emitMessage(forceMessage);

		}
		else
		{
			delete m;
			delete forceMessage;
		}

		// Instanciamos bala
		// TODO:: Quitar las balas f�sicas y poner s�lo un efecto de part�culas en la punta del arma
		createBullet(impactPoint, direction);
		/*CParticleSystemMessage* particleMessage = new CParticleSystemMessage();
		particleMessage->_activate = true;
		_entity->emitMessage(particleMessage);*/

		// Activamos animaci�n
		CMessage* animationMessage = new CMessage();
		animationMessage->setType(Message::SET_ANIMATION);
		_entity->emitMessage(animationMessage);
	}

	//---------------------------------------------------------

	void CNormalWeapon::createBullet(const Vector3 &impactPoint, const Vector3 &direction)
	{
		std::stringstream ss;
 		ss << _shootingPoint.x << " " << _shootingPoint.y << " " << _shootingPoint.z; 
		_bulletEntityInfo->setAttribute("position", ss.str());

		// Vaciar stringstream
		ss.str("");
		ss.clear();

		ss << impactPoint.x << " " << impactPoint.y << " " << impactPoint.z; 
		_bulletEntityInfo->setAttribute("finalPosition", ss.str());

		// Vaciar stringstream
		ss.str("");
		ss.clear();

		// Asignamos la direcci�n de movimiento. Ya que las balas no son instantaneas
		ss << direction.x << " " << direction.y << " " << direction.z; 
		_bulletEntityInfo->setAttribute("direction", ss.str());

		// Asignamos el nombre del template del efecto de particulas
		/*_bulletEntityInfo->setAttribute("template", "NormalWeapon");*/

		// Vaciar stringstream
		ss.str("");
		ss.clear();

		// Asigamos nombre �nico
		//TODO:: HACER UNA ESPECIE DE EntityID.h PARA TODAS LAS ENTIDADES INSTANCIADAS
		// O MODIDIFICARLO PARA QUE PUEDAN ACCEDER TODOS.
		// Hack:: ej: SlowBullet_David_1 or BackBullet_Peter2_4
		ss << _bulletEntityName << "_" << _entity->getName() << "_" << ++_id;	
		_bulletEntityInfo->setName(ss.str());

		// Creamos una nueva entidad con el prefab
		Logic::CEntity* bulletEntity = CEntityFactory::getSingletonPtr()->
			createEntity(_bulletEntityInfo, _entity->getMap());
		// Llamamos al activate de la entidad (IMPORTANTE: si no no funcionar� en el .exe en release)
		bulletEntity->activate();

		// Avisamos a las particulas de la bala TODO esto se puede hacer en la bala
		// y asi puede haber balas con efecto de particulas o sin el
		CParticleSystemMessage* particleMessage = new CParticleSystemMessage();
		particleMessage->_activate = true;
		bulletEntity->emitMessage(particleMessage);
	}

} //namespace Logic
