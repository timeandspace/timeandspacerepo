/**
@file SteeringMovement.cpp

Contiene la declaraci�n de la clase CSteringMovement. Se encarga de
mover un avatar utilizando movimientos cin�ticos o din�micos.

@author Gonzalo Fl�rez
@date Diciembre, 2010
*/
#include "SteeringMovement.h"

#include "Map/MapEntity.h"

#include "AI/Movement.h"

#include "AI/DynamicMovement.h"

#include "DynamicEntity.h"

namespace Logic 
{

	IMP_FACTORY(CSteeringMovement);

	//---------------------------------------------------------
	/**
	Destructor
	*/
	CSteeringMovement::~CSteeringMovement()
	{
		if (_currentMovement != 0)
			delete _currentMovement;
		if (_yaw != 0)
			delete _yaw;
		if(_face != 0)
			delete _face;
		if (_obstacleAvoidance != 0)
			delete _obstacleAvoidance;
	}

	//---------------------------------------------------------
	/**
	Inicializaci�n del componente, utilizando la informaci�n extra�da de
	la entidad le�da del mapa (Maps::CEntity). Toma del mapa los atributos
	speed, angularSpeed, accel y angularAccel, que indican los valores m�ximos 
	de velocidad y aceleraci�n lineales y angulares. El atributo tolerance indica
	la distancia a la que se considera que se ha alcanzado una posici�n.

	@param entity Entidad a la que pertenece el componente.
	@param map Mapa L�gico en el que se registrar� el objeto.
	@param entityInfo Informaci�n de construcci�n del objeto le�do del
	fichero de disco.
	@return Cierto si la inicializaci�n ha sido satisfactoria.
	*/
	bool CSteeringMovement::spawn(CEntity *entity, CMap *map, const Map::CEntity *entityInfo) 
	{
		if(!IComponent::spawn(entity,map,entityInfo))
			return false;

		if(entityInfo->hasAttribute("speed"))
			_maxLinearSpeed = entityInfo->getFloatAttribute("speed");
		if(entityInfo->hasAttribute("angularSpeed"))
			_maxAngularSpeed = entityInfo->getFloatAttribute("angularSpeed");
		if(entityInfo->hasAttribute("accel"))
			_maxLinearAccel = entityInfo->getFloatAttribute("accel");
		if(entityInfo->hasAttribute("angularAccel"))
			_maxAngularAccel = entityInfo->getFloatAttribute("angularAccel");
		if (entityInfo->hasAttribute("movementTolerance"))
			_tolerance = entityInfo->getFloatAttribute("movementTolerance");

		_yaw = AI::IMovement::getMovement(AI::IMovement::MOVEMENT_KINEMATIC_ALIGN_TO_SPEED, 
			_maxLinearSpeed, _maxAngularSpeed, _maxLinearAccel, _maxAngularAccel); 
		_yaw->setEntity(entity);

		_obstacleAvoidance = AI::IMovement::getMovement(AI::IMovement::MOVEMENT_OBSTACLE_AVOIDANCE,
			_maxLinearSpeed, _maxAngularSpeed, _maxLinearAccel, _maxAngularAccel);
		_obstacleAvoidance->setEntity(entity);

		return true;

	} // spawn

	//---------------------------------------------------------

	void CSteeringMovement::tick(unsigned int msecs)
	{
		IComponent::tick(msecs);

		//antes de ver si finalizamos realizamos el face si lo hay NO BORRES ALEX
		if(_face)
			faceEntity(msecs);

		if (_finished) 
			return;

		// Si hay un nuevo movimiento
		if (_currentMovement != NULL) 
		{
			// Ajustamos el target
			_arrived = false;
			_currentMovement->setEntity(_entity);
			_currentMovement->setTarget(_currentTarget);
			// Y la animaci�n
			//sendAnimationMessage(TAnimations::WALK);

		}

		_arrived = _entity->getPosition().positionEquals(_currentTarget, _tolerance);
		if (_arrived) 
		{
			// Enviar un mensaje para informar de que hemos llegado a destino
			CIAMessage *message = new CIAMessage(Message::FINISHED_MOVE);
			message->_bool = true;
			_entity->emitMessage(message, this);
			_finished = true;
		}

		// Si nos estamos desplazando calculamos la pr�xima posici�n
		if (!_arrived && _currentMovement != 0) 
		{
			_currentMovement->move(msecs, _currentProperties);

			AI::IMovement::DynamicMovement _auxProperties(_currentProperties);
			float distToTarget = _currentTarget.distance(_entity->getPosition());
			static_cast<AI::CObstacleAvoidance *>(_obstacleAvoidance)->move(msecs, _currentProperties, distToTarget);
			// Sumamos las fuerzas
			_currentProperties.linearAccel += _auxProperties.linearAccel;

			//Limitar el m�ximo de aceleraci�n
			if (_currentProperties.linearAccel.length() > _maxLinearAccel)
			{
				_currentProperties.linearAccel.normalise();
				_currentProperties.linearAccel *= _maxLinearAccel;
			}

			// Enviar un mensaje para que el componente f�sico mueva el personaje
			/*CAvatarWalkMessage *message = new CAvatarWalkMessage();
			message->_movement = _currentProperties.linearSpeed * msecs;
			_entity->emitMessage(message, this);*/

			// Enviar mensaje al NPC controller
			CControlMessage *ctrlMsg = new CControlMessage();
			ctrlMsg->_avatarAction = TAvatarActions::WALK;
			ctrlMsg->_vector = _currentProperties.linearSpeed * msecs;
			_entity->emitMessage(ctrlMsg, this);

			// Calculamos la rotaci�n
			if(!_face)
			{
				_yaw->move(msecs, _currentProperties);
				// Aplicar la rotaci�n
				CControlMessage *message2 = new CControlMessage();
				message2->_avatarAction = TAvatarActions::TURN;
				message2->_yaw = _currentProperties.angularSpeed * msecs;
				_entity->emitMessage(message2, this);
			}

			// Acelerar
			_currentProperties.linearSpeed += _currentProperties.linearAccel * msecs;
			// Clipping
			double speedValue = _currentProperties.linearSpeed.length();
			if (speedValue > _maxLinearSpeed) {
				_currentProperties.linearSpeed *= (_maxLinearSpeed / speedValue);
			}

			_currentProperties.angularSpeed += _currentProperties.angularAccel * msecs;
			if (_currentProperties.angularSpeed > _maxAngularSpeed) 
				_currentProperties.angularSpeed = Ogre::Math::Sign(_currentProperties.angularSpeed) * _maxAngularSpeed;
		}

	} // tick

	//--------------------------------------------------------- 

	void CSteeringMovement::faceEntity(unsigned int msecs)
	{
		if(_face)
		{
			_face->move(msecs, _currentProperties);

			// Aplicar la rotaci�n
			CControlMessage *message = new CControlMessage();
			message->_avatarAction = TAvatarActions::TURN;
			message->_yaw = _currentProperties.angularSpeed * msecs;
			_entity->emitMessage(message, this);
		}
	}

	//---------------------------------------------------------
	/**
	Env�a un mensaje para cambiar la animaci�n actual.

	@param animation Nueva animaci�n.
	*/
	/*void CSteeringMovement::sendAnimationMessage(TAnimations animation) 
	{
		CAnimationMessage *message = new CAnimationMessage();

		message->setType(Message::SET_ANIMATION);
		message->_animationName = animation;
		message->_bool = true;
		_entity->emitMessage(message,this);
	}*/

	//---------------------------------------------------------
	/**
	Este componente s�lo acepta mensajes de tipo MOVE_TO.
	*/
	bool CSteeringMovement::accept(CMessage *message)
	{
		return message->getType() == Message::MOVE_TO ||
			message->getType() == Message::SET_STEERING_BEHAVIOUR;
	} // accept

	//---------------------------------------------------------
	/**
	Si se recibe un mensaje de tipo MOVE_TO, almacena el vector como 
	destino del movimiento y el tipo de movimiento.
	*/
	void CSteeringMovement::process(CMessage *message)
	{
		if (message->getType() == Message::MOVE_TO) 
		{
			Vector3 target = static_cast<CMoveToMessage *>(message)->_target;
			int movType = static_cast<CMoveToMessage *>(message)->_movementType;	


			if (movType != _currentMovementType || _currentTarget != target) 
			{
				if (_currentMovement != 0) {
					delete _currentMovement;
				}

				_currentMovementType = movType;
				_currentTarget = target;

				_currentMovement = AI::IMovement::getMovement(_currentMovementType, _maxLinearSpeed, _maxAngularSpeed, _maxLinearAccel, _maxAngularAccel);
				if (_currentMovement == NULL)
				{
					CControlMessage *ctrlMsg = new CControlMessage();
					ctrlMsg->_avatarAction= TAvatarActions::STOP_WALK;
					_entity->emitMessage(ctrlMsg, this);
				}
				_finished = false;
			}

		}
		else if (message->getType() == Message::SET_STEERING_BEHAVIOUR) 
		{
			CSetSteeringBehaviour* steerMessage = static_cast<CSetSteeringBehaviour *>(message);

			CEntity* otherEntityInvolved = steerMessage->_otherEntityInvolved;
			int movType = steerMessage->_movementType;		

			switch (movType)
			{
			case AI::IMovement::MOVEMENT_NONE:
				{
					if (_currentMovement != 0) {
						delete _currentMovement;
						_currentMovement = NULL;
					}

					CControlMessage *ctrlMsg = new CControlMessage();
					ctrlMsg->_avatarAction = TAvatarActions::STOP_WALK;
					_entity->emitMessage(ctrlMsg, this);
					break;
				}
			case AI::IMovement::MOVEMENT_DYNAMIC_PURSUE:
			case AI::IMovement::MOVEMENT_DYNAMIC_EVADE:
				{
					if (_currentMovement != 0) {
						delete _currentMovement;
					}
					_currentMovementType = movType;
					_currentMovement = AI::IMovement::getMovement(_currentMovementType, _maxLinearSpeed, _maxAngularSpeed, _maxLinearAccel, _maxAngularAccel);

					CDynamicEntity* dinEnt  = (CDynamicEntity*) otherEntityInvolved->getComponent("CDynamicEntity");

					if(movType == AI::IMovement::MOVEMENT_DYNAMIC_PURSUE)
					{
						((AI::CDynamicPursue*) _currentMovement)->setEntityToPursue(otherEntityInvolved);
						((AI::CDynamicPursue*) _currentMovement)->setEntityToPursueProperties(dinEnt->getCurrentProperties());
					}
					else
					{
						((AI::CDynamicEvade*) _currentMovement)->setEntityToPursue(otherEntityInvolved);
						((AI::CDynamicEvade*) _currentMovement)->setEntityToPursueProperties(dinEnt->getCurrentProperties());
					}
				}
				break;
			case AI::IMovement::MOVEMENT_KINEMATIC_FACE:
				{
					if (steerMessage->_startStop && !_face)
					{
						_face = AI::IMovement::getMovement(movType, _maxLinearSpeed, _maxAngularSpeed, _maxLinearAccel, _maxAngularAccel);
						_face->setEntity(_entity);
						((AI::CKinematicFace*) _face)->setEntityToFace(steerMessage->_otherEntityInvolved);	
					}
					// Si existe face movement pero el target es distinto cambiamos el target
					else if (steerMessage->_startStop &&  _face /*&&
						((AI::CKinematicFace*) _face)->getEntityToFace() != steerMessage->_otherEntityInvolved*/ )
					{
						((AI::CKinematicFace*) _face)->setEntityToFace(steerMessage->_otherEntityInvolved);	
					}
					else if (!steerMessage->_startStop && _face)
					{
						delete _face;
						_face = NULL;
					}
				}
				break;
			default:
				break;
			}

		}

	} // process

	//---------------------------------------------------------

}