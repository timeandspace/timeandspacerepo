/**
@file SoundSource.cpp

Contine la implementaci�n del componente

@see Logic::IComponent

@author �lvaro Bl�zquez Checa
@date Marzo, 2014

Update:
@author Alejandro P�rez Alonso
@date Abril, 2014
*/

#include "SoundSource.h"

#include "Logic/Entity/Entity.h"
#include "Map/MapEntity.h"
#include "Sound/Sound.h"
#include "Sound/SoundBank.h"
#include "Sound/Server.h"

namespace Logic
{
	IMP_FACTORY(CSoundSource);

	//---------------------------------------------------------

	CSoundSource::~CSoundSource() 
	{
		if(_sound)
			delete _sound;
	}

	//---------------------------------------------------------

	bool CSoundSource::spawn(CEntity *entity, CMap *map, const Map::CEntity *entityInfo)
	{
		if (!IComponent::spawn(entity, map, entityInfo))
			return false;

		bool loop = false;
		if(entityInfo->hasAttribute("loop"))
		{
			loop = entityInfo->getBoolAttribute("loop");
		}

		if(entityInfo->hasAttribute("sound_name"))
		{
			std::string soundName = entityInfo->getStringAttribute("sound_name");

			_sound = new Sonido::CSound(soundName , loop);
		}
		else if(entityInfo->hasAttribute("sound_name_studio"))
		{
			std::string soundName = entityInfo->getStringAttribute("sound_name_studio");
			std::string parameterName = "";

			if(entityInfo->hasAttribute("parameterName"))
			{
				parameterName = entityInfo->getStringAttribute("parameterName");
				_parameterStop = true;
			}

			if(parameterName.empty())
				_sound = new Sonido::CSound(soundName);
			else 
				_sound = new Sonido::CSound(soundName,parameterName);
		}
		if(entityInfo->hasAttribute("volume"))
		{
			float volume = entityInfo->getFloatAttribute("volume");

			if(_sound)
				_sound->setVolume(volume);
		}


		//assert(_sound && "Falta el archivo de sonido");

		return true;

	} // spawn

	//---------------------------------------------------------

	bool CSoundSource::activate()
	{
		return IComponent::activate();
	} // activate

	//---------------------------------------------------------

	void CSoundSource::deactivate()
	{
		IComponent::deactivate();

		if(_sound)
			_sound->stop();
	} // deactivate

	//---------------------------------------------------------

	bool CSoundSource::accept(CMessage *message)
	{
		return message->getType() == Message::PLAY_SOUND ||
			message->getType() == Message::STOP_SOUND  ||
			message->getType() == Message::CHANGE_PARAMETER;
	} // accept

	//---------------------------------------------------------

	void CSoundSource::process(CMessage *message)
	{
		CSoundMessage* soundMessage = static_cast<CSoundMessage*>(message);

		if (soundMessage->getType() ==  Message::PLAY_SOUND)
		{	
			play();
		}
		else if (soundMessage->getType() ==  Message::STOP_SOUND)
		{	
			stop();
		}
		else if (soundMessage->getType() ==  Message::CHANGE_PARAMETER)
		{	
			setParameter(soundMessage->_soundParameter);
		}

	} // process

	//---------------------------------------------------------

	void CSoundSource::tick(unsigned int msecs)
	{
		IComponent::tick(msecs);

		if(_sound)
			_sound->update(msecs, _entity->getPosition());

	} // tick

	//---------------------------------------------------------

	void CSoundSource::play()
	{
		if(_sound)
		{
			_sound->setParameter(0.1f);
			_sound->play();
		}
	}

	//---------------------------------------------------------

	void CSoundSource::stop()
	{
		if(_sound)
		{
			if(_parameterStop)
				_sound->setParameter(0.7f);
			else
				_sound->stop();
		}
	}

	//---------------------------------------------------------

	void CSoundSource::pause(bool paused)
	{
		if(_sound);
	}

	//--------------------------------------------------------

	void CSoundSource::setParameter(float parameterValue)
	{
		if(_sound)
			_sound->setParameter(parameterValue);
	}

} // namespace Logic
