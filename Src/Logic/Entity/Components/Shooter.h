/**
@file Shooter.h

Contiene la declaraci�n del componente que permite a una
entidad disparar. Instancia un objeto bala al disparar.

@author Alejandro P�rez Alonso
@date Febrero, 2014
*/

#ifndef __Logic_Shooter_H
#define __Logic_Shooter_H

#include "Logic/Entity/Component.h"

namespace Sonido
{
	class CSound;
}
namespace Logic
{
	class CCamera;
	class CWeapon;
}

namespace Logic
{

	/**
	Clase CShooter
	*/
	class CShooter : public IComponent
	{
		DEC_FACTORY(CShooter);
	public:

		/**
		*/
		CShooter();

		/**
		*/
		~CShooter();

		/**
		*/
		virtual bool spawn(CEntity* entity, CMap *map, const Map::CEntity *entityInfo);

		/**
		*/
		virtual bool activate();

		/**
		*/
		virtual void deactivate();

		/**
		*/
		virtual bool accept(CMessage *message);

		/**
		*/
		virtual void process(CMessage *message);

		/**
		*/
		virtual void tick(unsigned int msecs);

		/**
		A�ade un nuevo arma
		*/
		void addWeapon(CWeapon *ent)
		{
			_weapons.push_back(ent);
			if (_weapons.size() > 1)
			{
				removeWeapon(_weapons[_currentWeapon]);
				_currentWeapon = (TWeaponType)(_weapons.size() - 1);
			}
		}

		/**
		Quita  un arma, la quita del hueso pero no la elimina, no se sueltan las armas
		*/
		void removeWeapon(CWeapon *ent);

		/**
		Suelta las armas
		*/
		void releaseWeapons();

		//void fire(const Vector3 &position, const Vector3 &direction);

	protected:


		//void fire(CFireMessage* message);

		/**
		Modificador de tiempo del poder
		*/
		float _modifier;


		//Lista de armas
		std::vector<CWeapon *>_weapons;

		int _currentWeapon;

		CCamera *_cam;

	}; // Class CShooter

	REG_FACTORY(CShooter);

} // namespace Logic

#endif // __Logic_Shooter_H