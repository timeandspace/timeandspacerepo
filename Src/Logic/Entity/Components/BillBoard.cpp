/**
@file BillBoard.cpp


*/

#include "BillBoard.h"

#include "Logic/Entity/Entity.h"
#include "Logic/Maps/Map.h"
#include "Map/MapEntity.h"

#include "Graphics/Server.h"
#include "Graphics/Scene.h"
#include "Graphics/Entity.h"
#include "Graphics/StaticEntity.h"

namespace Logic 
{
	IMP_FACTORY(CBillBoard);

	//---------------------------------------------------------

	CBillBoard::~CBillBoard() 
	{
		

	} // ~CGraphics

	//---------------------------------------------------------

	bool CBillBoard::spawn(CEntity *entity, CMap *map, const Map::CEntity *entityInfo) 
	{
		if(!IComponent::spawn(entity,map,entityInfo))
			return false;

		_billboardMaterial = entityInfo->getStringAttribute("billboardMaterial");

		if(entityInfo->hasAttribute("dimension"))
			_dim = entityInfo->getVector2Attribute("dimension");

		return true;

	} // spawn

	//---------------------------------------------------------

	bool CBillBoard::activate()
	{
		Graphics::CServer::getSingletonPtr()->getActiveScene()->
			createBillBoardSet(_entity->getName(), _billboardMaterial, _dim);
		Graphics::CServer::getSingletonPtr()->getActiveScene()->
			createBillBoard(_entity->getName(), _entity->getPosition());

		return IComponent::activate();
	}

	//---------------------------------------------------------

	bool CBillBoard::accept(CMessage *message)
	{
		return message->getType() == Message::SET_TRANSFORM;

	} // accept

	//---------------------------------------------------------

	void CBillBoard::process(CMessage *message)
	{

	} // process

	//---------------------------------------------------------

	 void CBillBoard::tick(unsigned int msecs)
	 {
		 IComponent::tick(msecs);
	 }

} // namespace Logic

