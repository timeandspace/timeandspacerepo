/**
@file ForceTrigger.cpp

Contiene la implementacion del componente que envia un mensaje a otra
entidad cuando recibe un mensaje TOUCHED. Le aplica una fuerza
a la otra entidad.

@see Logic::CForceTrigger
@see Logic::IComponent

@author Alejandro Perez Alonso
@date Enero, 2014
*/

#include "ForceTrigger.h"

#include "Logic/Entity/Entity.h"
#include "Logic/Maps/Map.h"
#include "Map/MapEntity.h"


namespace Logic 
{
	IMP_FACTORY(CForceTrigger);

	//---------------------------------------------------------

	bool CForceTrigger::spawn(CEntity *entity, CMap *map, const Map::CEntity *entityInfo) 
	{
		if(!IComponent::spawn(entity,map,entityInfo))
			return false;

		_forceTime = 10; _forceHeight = 4;
		if(entityInfo->hasAttribute("force_time"))
			_forceTime = entityInfo->getFloatAttribute("force_time");

		if(entityInfo->hasAttribute("force_height"))
			_forceHeight = entityInfo->getFloatAttribute("force_height");

		_direction = Vector3::UNIT_Y;
		if(entityInfo->hasAttribute("force_direction"))
			_direction = entityInfo->getVector3Attribute("force_direction");

		return true;

	} // spawn

	//---------------------------------------------------------

	bool CForceTrigger::activate()
	{
		return IComponent::activate();
	} // activate

	//---------------------------------------------------------

	void CForceTrigger::deactivate()
	{
		IComponent::deactivate();
	} // deactivate

	//---------------------------------------------------------

	bool CForceTrigger::accept(CMessage *message)
	{
		return message->getType() == Message::TOUCHED;

	} // accept

	//---------------------------------------------------------

	void CForceTrigger::process(CMessage *message)
	{
		CEntity *ent = NULL;
		switch(message->getType())
		{
		case Message::TOUCHED:
			ent = static_cast<CTouchedMessage *>(message)->_entity;
			/* Si la entidad es un character controller, aplicar una fuerza es igual a 
			realizar un salto.*/
			std::string type = ent->getType();
			if (type == "Player" || type == "PlayerClone")
			{
				CJumpMessage *m = new CJumpMessage();
				m->_direction = _direction;
				m->_height = _forceHeight;
				m->_time = _forceTime;
				m->_jump = false;
				ent->emitMessage(m);
			}
			// Si no aplicamos una fuerza al objeto din�mico
			//else {
			//	//Evitar mensaje de aplicar fuerza
			//	ent->emitMessage(m, this);
			//}
			break;
		}

	} // process


} // namespace Logic

