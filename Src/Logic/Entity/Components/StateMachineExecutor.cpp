#include "StateMachineExecutor.h"

#include "Map/MapEntity.h"
#include "AI/WaypointGraph.h"
#include "AI/Server.h"

namespace Logic 
{

	//---------------------------------------------------------

	IMP_FACTORY(CStateMachineExecutor);

	//---------------------------------------------------------
	/**
	Inicializaci�n del componente, utilizando la informaci�n extra�da de
	la entidad le�da del mapa (Maps::CEntity). Toma del mapa el atributo
	behavior, que indica el nombre de la m�quina de estado a ejecutar.

	@param entity Entidad a la que pertenece el componente.
	@param map Mapa L�gico en el que se registrar� el objeto.
	@param entityInfo Informaci�n de construcci�n del objeto le�do del
	fichero de disco.
	@return Cierto si la inicializaci�n ha sido satisfactoria.
	*/
	bool CStateMachineExecutor::spawn(CEntity* entity, CMap *map, const Map::CEntity *entityInfo)
	{
		if(!IComponent::spawn(entity,map,entityInfo))
			return false;
		// Obtiene el nombre de la m�quina de estado 
		if(entityInfo->hasAttribute("behavior")) {
			std::string smName = entityInfo->getStringAttribute("behavior");
			if (_currentStateMachine != 0) delete _currentStateMachine;
			// Saca una instancia de la m�quina de estado de la factor�a
			//_currentStateMachine = new AI::CSMPatrol(_entity);
			
			AI::CStateMachineFactory::buildStateMachine(entity,"");
			if(smName == "pursue")
			{
				std::string name = "Player";
				_currentStateMachine = AI::CStateMachineFactory::getStateMachine(smName, entity, &name);
			}

			else if(smName == "patrol")
			{
				std::vector<int> patrolList;

				patrolList.push_back(0);
				patrolList.push_back(1);
				patrolList.push_back(2);
				patrolList.push_back(3);

				_currentStateMachine = AI::CStateMachineFactory::getStateMachine(smName, entity, &patrolList);
			}
			else
				_currentStateMachine = AI::CStateMachineFactory::getStateMachine(smName, entity, NULL);

		}

		return true;
	}
	//---------------------------------------------------------
	/**
	M�todo llamado en cada frame que actualiza el estado del componente.
	<p>
	Este m�todo actualiza la m�quina de estado. Si hay un cambio de estado, 
	se actualiza el valor del atributo _currentAction, que es el que contiene
	la acci�n latente que se est� ejecutando. Por �ltimo, se llama al tick de
	la acci�n latente para que avance su ejecuci�n.

	@param msecs Milisegundos transcurridos desde el �ltimo tick.
	*/
	void CStateMachineExecutor::tick(unsigned int msecs)
	{
		IComponent::tick(msecs);

		// TODO PR�CTICA IA
		// En cada tick del ejecutor de m�quinas de estado tenemos que 
		// realizar 2 acciones principales:
		// 1. Actualizar la m�quina de estado.
		// 1.1. Si la m�quina cambia de estado reseteamos la acci�n que 
		//		est�bamos ejecutando hasta ahora para que se pueda volver
		//		a ejecutar y cambiamos la acci�n latente actual por la del 
		//		nuevo estado.
		// 2. Ejecutar la acci�n latente correspondiente al estado actual
		if (_currentStateMachine != NULL)
		{
			// Para int's
			/*if (_currentStateMachine->update())
			{

			int* currentarget = _currentStateMachine->getCurrentNode();
			std::cout << "moving to " << (*currentarget) << std::endl;

			AI::CWaypointGraph *wpg = AI::CServer::getSingletonPtr()->getNavigationGraph();
			Vector3 position = wpg->getWaypoint(*currentarget);
			TMessage message;
			message._type = Message::ROUTE_TO;
			message._vector3 = position;
			_entity->emitMessage(message, this);
			}*/

			// Para LatentAction's
			if (_currentStateMachine->update())
			{
				if (_currentAction != NULL)
					_currentAction->reset();
				_currentAction = _currentStateMachine->getCurrentNode();
			}
		}
		if (_currentAction != NULL)
			_currentAction->tick();
	}
	//---------------------------------------------------------
	/**
	Este m�todo delega en el m�todo accept de la acci�n latente que se 
	est� ejecutando (_currentAction).
	*/
	bool CStateMachineExecutor::accept(CMessage *message)
	{
		// TODO PR�CTICA IA
		// El m�todo accept delega en el m�todo accept de la acci�n latente actual
		/*if (_currentStateMachine != NULL)
		return _currentStateMachine->accept(message);*/
		if (_currentStateMachine != NULL && _currentStateMachine->accept(message))
			return true;
		if (_currentAction != NULL)
			return _currentAction->accept(message);
		return false;
	}
	//---------------------------------------------------------
	/**
	Este m�todo delega en el m�todo process de la acci�n latente que se 
	est� ejecutando (_currentAction).
	*/
	void CStateMachineExecutor::process(CMessage *message)
	{
		// TODO PR�CTICA IA
		// El m�todo process delega en el m�todo process de la acci�n latente actual
		if (_currentStateMachine != NULL)
			_currentStateMachine->process(message);
		if (_currentAction != NULL)
			_currentAction->process(message);
	}
	//---------------------------------------------------------

}