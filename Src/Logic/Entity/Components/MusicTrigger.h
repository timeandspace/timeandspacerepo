/**
@file MusicTrigger.h

Contiene la declaración del componente que modifica el parametro
de la musica dinamica para cambiar la intensidad.

@see Logic::IComponent

@author Alvaro Blazquez Checa
@date Mayo, 2014
*/
#ifndef __Logic_Music_Trigger_H
#define __Logic_Music_Trigger_H

#include "Logic/Entity/Component.h"

namespace Logic
{
	class CMusicPlayer;
}

//declaración de la clase
namespace Logic 
{
	/**
	@ingroup logicGroup

	@author Alvaro Blazquez Checa
	@date Mayo, 2014
	*/
	class CMusicTrigger : public IComponent
	{
		DEC_FACTORY(CSpecialActionTrigger);
	public:

		/**
		Constructor por defecto.
		*/
		CMusicTrigger();

		/**
		Inicialización del componente usando la descripción de la entidad que hay en 
		el fichero de mapa.
		*/
		virtual bool spawn(CEntity* entity, CMap *map, const Map::CEntity *entityInfo);

		/**
		*/
		virtual bool activate();

		/**
		*/
		virtual void deactivate();

		/**
		Este componente sólo acepta mensaje de tipos TOUCHED y UNTOUCHED.
		*/
		virtual bool accept(CMessage *message);

		/**
		Al recibir mensaje TOUCHED
		*/
		virtual void process(CMessage *message);


	protected:
		Logic::CMusicPlayer* _musicPlayer;

		float _parameterValue;

		std::string _musicPlayerEntityName;

	}; // class CMusicTrigger

	REG_FACTORY(CMusicTrigger);

} // namespace Logic

#endif // __Logic_Sticky_Floor_H
