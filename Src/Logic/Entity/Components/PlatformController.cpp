/**
Versi�n mejorada del componente CSwitchPosition, este mueve un objeto (plataforma, puerta, etc.)
entre una lista de puntos. Adem�s se permite el movimiento en loop y la opci�n de autoStart.

@author Alejandro P�rez Alonso

@date Marzo, 2014
*/

#include "PlatformController.h"

#include "Logic/Entity/Entity.h"
#include "Logic/Maps/Map.h"
#include "Map/MapEntity.h"


namespace Logic 
{
	IMP_FACTORY(CPlatformController);

	//---------------------------------------------------------

	bool CPlatformController::spawn(CEntity *entity, CMap *map, const Map::CEntity *entityInfo) 
	{
		if(!IComponent::spawn(entity,map,entityInfo))
			return false;

		// Leer atributos del map
		_tolerance = 0.1;
		if (entityInfo->hasAttribute("tolerance"))
			_tolerance = entityInfo->getFloatAttribute("tolerance");

		_speed = entityInfo->getFloatAttribute("speed");
		_speed /= 1000.0f; 
		_loop = entityInfo->getBoolAttribute("loop");
		_nextLoop = _loop;
		_isActivated = entityInfo->getBoolAttribute("autoStart");

		// Leer lista de movimientos del map
		_vectList.push_back(entityInfo->getVector3Attribute("position"));
		// �APA:: buscamos position[0-9]
		std::stringstream ss;
		int aux = 2;
		ss << "position" << aux;

		while (entityInfo->hasAttribute(ss.str()))
		{
			_vectList.push_back(entityInfo->getVector3Attribute(ss.str()));
			ss.str("");
			ss.clear();
			++ aux;
			ss << "position" << aux;
		}

		return true;

	} // spawn

	//---------------------------------------------------------

	bool CPlatformController::accept(CMessage *message)
	{
		return message->getType() == Message::SWITCH;

	} // accept

	//---------------------------------------------------------

	void CPlatformController::process(CMessage *message)
	{
		switch(message->getType())
		{
		case Message::SWITCH:
			_direction = static_cast<CSwitchMessage*>(message)->_int;
			//_nextTarget += _direction;
			if (static_cast<CSwitchMessage*>(message)->_activate)
			{
				_isActivated = true;
				_nextLoop = _loop;
			}
			else
			{
				// Vuelve a la posicion inicial y se desactiva el movimiento en loop
				_nextTarget = 0;
				if (_loop) _nextLoop = false;
				_isActivated = true;
			}
			break;
		case Message::BACKWARDS:
			_direction *= -1;
			//_nextTarget += _direction;
			_isActivated = true;
			break;
		}

	} // process

	//---------------------------------------------------------

	Vector3 CPlatformController::getDirection()
	{
		Vector3 dir(Vector3::ZERO);

		if (_vectList.size() > 0 && (_vectList.size() > _nextTarget))
		{
			dir = _vectList[_nextTarget] - _entity->getPosition();
			dir.normalise();
		}
		
		return dir;
	}

	//---------------------------------------------------------

	void CPlatformController::tick(unsigned int msecs)
	{
		// Invocar al m�todo de la clase padre (IMPORTANTE)
		IComponent::tick(msecs);

		if (_isActivated && _vectList.size() > 0)
		{
			if(!_running)
			{
				CSoundMessage* message = new CSoundMessage();
				message->setType(Message::PLAY_SOUND);
				_entity->emitMessage(message, this);
				_running = true;
			}
			// Comprobamos si se ha alcanzado el objetivo
			if (_entity->getPosition().positionEquals(_vectList[_nextTarget], _tolerance)) 
			{
				// Si llega al final de la lista
				if ((_direction > 0 && ((_nextTarget + 1) == _vectList.size()))
					|| (_direction < 0 && _nextTarget == 0))
				{
					_direction *= -1; // invertimos direccion

					if (!_nextLoop) { // Si no loop paramos
						CSoundMessage* message = new CSoundMessage();
						message->setType(Message::STOP_SOUND);
						_entity->emitMessage(message, this);

						_running = false;
						_isActivated = false;
						return;
					}
				}
				_nextTarget += _direction;
			} 

			// Calcular la direcci�n en la que debemos mover la entidad
			Vector3 dir = _vectList[_nextTarget] - _entity->getPosition();

			// Calcular la distancia a la que est� el objetivo
			float distance = dir.length();

			// Calcular desplazamiento en funci�n del tiempo transcurrido
			dir.normalise();
			dir *= msecs * _speed;

			// Si nos pasamos del objetivo ajustamos para llegar justo hasta �l 
			if (dir.length() > distance) 
			{
				dir.normalise();
				dir *= distance;
			}

			CKinematicMoveMessage* message = new CKinematicMoveMessage();
			message->_displ = dir;
			_entity->emitMessage(message, this);
		}
		else
		{

		}

	} // tick


} // namespace Logic
