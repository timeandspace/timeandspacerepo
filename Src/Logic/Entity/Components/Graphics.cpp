/**
@file Graphics.cpp

Contiene la implementación del componente que controla la representación
gráfica de la entidad.

@see Logic::CGraphics
@see Logic::IComponent

@author David Llansó
@date Agosto, 2010
*/

#include "Graphics.h"

#include "Logic/Entity/Entity.h"
#include "Logic/Maps/Map.h"
#include "Map/MapEntity.h"

#include "Graphics/Server.h"
#include "Graphics/Scene.h"
#include "Graphics/Entity.h"
#include "Graphics/StaticEntity.h"

namespace Logic 
{
	IMP_FACTORY(CGraphics);

	//---------------------------------------------------------

	CGraphics::~CGraphics() 
	{
		// Eliminamos attached objects
		Graphics::CServer::getSingletonPtr()->destroyParticleSystem(_entity->getName()+"_p");
		if(_graphicsEntity)
		{
			_scene->removeEntity(_graphicsEntity);
			delete _graphicsEntity;
			_graphicsEntity = 0;
		}

	} // ~CGraphics

	//---------------------------------------------------------

	bool CGraphics::spawn(CEntity *entity, CMap *map, const Map::CEntity *entityInfo) 
	{
		if(!IComponent::spawn(entity,map,entityInfo))
			return false;

		_scene = _entity->getMap()->getScene();

		if(entityInfo->hasAttribute("model"))
			_model = entityInfo->getStringAttribute("model");

		_graphicsEntity = createGraphicsEntity(entityInfo);

		if(!_graphicsEntity)
			return false;

		if(entityInfo->hasAttribute("setCastShadows"))
		{
			bool value = entityInfo->getBoolAttribute("setCastShadows");
			_graphicsEntity->setCastShadows(value);

		}

		if (entityInfo->hasAttribute("material"))
		{
			_graphicsEntity->setMaterial(entityInfo->getStringAttribute("material"));
		}

		//bool value = _graphicsEntity->getCastShadows();

		return true;

	} // spawn

	//---------------------------------------------------------

	Graphics::CEntity* CGraphics::createGraphicsEntity(const Map::CEntity *entityInfo)
	{
		bool isStatic = false;
		bool isPlayer = false;

		if(entityInfo->hasAttribute("static"))
			isStatic = entityInfo->getBoolAttribute("static");

		if(entityInfo->hasAttribute("isPlayer"))
			isPlayer = entityInfo->getBoolAttribute("isPlayer");

		if(isStatic)
		{
			_graphicsEntity = new Graphics::CStaticEntity(_entity->getName(),_model);

			if(!_scene->addStaticEntity((Graphics::CStaticEntity*)_graphicsEntity))
				return 0;
		}
		else
		{
			_graphicsEntity = new Graphics::CEntity(_entity->getName(),_model);

			if(!_scene->addEntity(_graphicsEntity))
				return 0;
		}

		Ogre::Radian yaw = _entity->getTransform().extractQuaternion().getYaw();

		_graphicsEntity->setTransform(_entity->getTransform());

		return _graphicsEntity;

	} // createGraphicsEntity

	//---------------------------------------------------------

	bool CGraphics::accept(CMessage *message)
	{
		return message->getType() == Message::SET_TRANSFORM || 
			message->getType() == Message::CHANGE_MATERIAL;

	} // accept

	//---------------------------------------------------------

	void CGraphics::process(CMessage *message)
	{
		switch(message->getType())
		{
		case Message::SET_TRANSFORM:
			_graphicsEntity->setTransform(static_cast<CSetTransformMessage *>(message)->_transform);
			break;

		case Message::CHANGE_MATERIAL:
			_graphicsEntity->setMaterial(static_cast<CChangeMaterialMessage *>(message)->_materialName);
			break;
		}

	} // process

	//---------------------------------------------------------

} // namespace Logic

