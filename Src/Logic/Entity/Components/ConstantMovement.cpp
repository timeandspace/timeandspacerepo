/**
@file ConstantMovement.h

Contiene la implementación de una clase que realiza un 
movimiento constante en la direccion dada.

@author Alejandro Pérez Alonso
@date Abril, 2014
*/

#include "ConstantMovement.h"

#include "Logic/Entity/Entity.h"
#include "Logic/Maps/EntityFactory.h"
#include "Map/MapEntity.h"

namespace Logic 
{
	IMP_FACTORY(CConstantMovement);

	//---------------------------------------------------------

	bool CConstantMovement::spawn(CEntity *entity, CMap *map, const Map::CEntity *entityInfo) 
	{
		if(!IComponent::spawn(entity,map,entityInfo))
			return false;

		_direction = entityInfo->getVector3Attribute("direction");

		_speed = entityInfo->getFloatAttribute("speed");

		_finalPosition = entityInfo->getVector3Attribute("finalPosition");

		return true;

	} // spawn

	//---------------------------------------------------------

	void CConstantMovement::tick(unsigned int msecs)
	{
		IComponent::tick(msecs);

		Vector3 dist = _finalPosition - _entity->getPosition();
		float distance = dist.normalise();

		if (distance < 0.1 || !dist.directionEquals(_direction, Ogre::Radian(0.1f)))
			Logic::CEntityFactory::getSingletonPtr()->deferredDeleteEntity(_entity);

		Vector3 dir = _direction * _speed * msecs;
		CKinematicMoveMessage* message = new CKinematicMoveMessage();
		message->_displ = dir;
		if (distance < dir.normalise())
		{
			dir *= distance;
			message->_displ = dir;
		}
		
		_entity->emitMessage(message, this);
	}

} // namespace Logic
