/**
@file SoundSource.h
@author �lvaro Bl�zquez Checa
@date Febrero, 2014
*/

#ifndef __Logic_AudioListener_H
#define __Logic_AudioListener_H

#include "Logic/Entity/Component.h"


namespace Sonido
{
	class CServer;
}

namespace Logic
{
	/**
	Clase CAudioListener
	*/
	class CAudioListener : public IComponent
	{
		DEC_FACTORY(CAudioListener);
	public:

		/**
		*/
		CAudioListener() : IComponent()
		{
		}

		/**
		*/
		~CAudioListener() {}

		/**
		*/
		virtual bool spawn(CEntity* entity, CMap *map, const Map::CEntity *entityInfo);

		/**
		*/
		virtual bool activate();

		/**
		*/
		virtual void deactivate();

		/**
		*/
		virtual bool accept(CMessage *message);

		/**
		*/
		virtual void process(CMessage *message);

		/**
		*/
		virtual void tick(unsigned int msecs);

	private:

		Sonido::CServer *_soundServer;

		Ogre::Vector3 _lastPos;

	}; 

	REG_FACTORY(CAudioListener);

} // namespace Logic

#endif 