/**
@file Shooter.h

Contiene la declaraci�n del componente que describe
el comportamiento general de un arma.

@author Alvaro Blazquez Checa
@date Mayo, 2014
*/

#ifndef __Logic_Weapon_H
#define __Logic_Weapon_H

#include "Logic/Entity/Component.h"


namespace Logic
{

	/**
	Clase CWeapon
	*/
	class CWeapon : public IComponent
	{
	public:

		/**
		*/
		CWeapon();

		/**
		*/
		virtual ~CWeapon();

		/**
		*/
		virtual bool spawn(CEntity* entity, CMap *map, const Map::CEntity *entityInfo);

		/**
		*/
		virtual bool activate();

		/**
		*/
		virtual void deactivate();

		/**
		*/
		virtual bool accept(CMessage *message);

		/**
		*/
		virtual void tick(unsigned int msecs);

		/**
		*/
		virtual unsigned int getAmmo() { return _ammo; }

		/**
		*/
		virtual void setAmmo(unsigned int ammo) { _ammo = ammo; }

		/**
		Funcion que devuelve el punto del arma donde se instancian las balas o el efecto de particulas,
		ser� especifica para cada arma
		*/
		virtual const Vector3 &getShootingPoint() { return _shootingPoint;}

		/**
		Funcion que dispara el arma, ser� especifica para cada arma
		*/
		virtual void fire(const Vector3 &impactPoint, const std::string &hitEntityName) = 0;

		/**
		Funcion que adjunta un arma a un personaje, ser� especifica para cada arma
		*/
		virtual void attachWeapon(const std::string &entityName, const std::string &boneName, 
			Quaternion offsetOrientation = Quaternion::IDENTITY, Vector3 offsetPosition = Vector3::ZERO) = 0;

		virtual void detachWeapon() = 0;
		
		/**
		Funci�n que suelta el arma para que pueda ser utilizada de nuevo
		*/
		virtual void throwWeapon();

	protected:

		/** 
		N�mero de balas
		*/
		unsigned int _ammo;

		/**
		Identificador del tipo de arma/poder seleccionado
		*/
		int _currentWeapon;

		/**
		Posici�n relativa la entidad desde la que se instanciar�n las balas.
		*/
		Vector3 _shootingPoint;

		/**
		Nombre del hueso donde se quiere attachar el arma
		*/
		std::string _shootingPointBone;

		bool _init;

		Quaternion _offsetOrientation;
		Vector3 _offsetPosition;

	}; // Class CWeapon
} // namespace Logic

#endif // __Logic_Weapon_H