/**
@file Explode.h

Contiene la declaraci�n del componente que hace "explotar" una entidad y aplicar da�o
a las entidades que se encuentren en un determinado radio.

@author Alejandro P�rez Alonso

@date Mayo, 2014
*/
#ifndef __Logic_Explode_H
#define __Logic_Explode_H

#include "Logic/Entity/Component.h"

//declaraci�n de la clase
namespace Logic 
{

	class CExplode : public IComponent
	{
		DEC_FACTORY(CExplode);
	public:

		/**
		Constructor por defecto; en la clase base no hace nada.
		*/
		CExplode();

		virtual ~CExplode();

		/**
		Inicializaci�n del componente, utilizando la informaci�n extra�da de
		la entidad le�da del mapa (Maps::CEntity). Se accede a los atributos 
		necesarios como la c�mara gr�fica y se leen atributos del mapa.

		@param entity Entidad a la que pertenece el componente.
		@param map Mapa L�gico en el que se registrar� el objeto.
		@param entityInfo Informaci�n de construcci�n del objeto le�do del
		fichero de disco.
		@return Cierto si la inicializaci�n ha sido satisfactoria.
		*/
		virtual bool spawn(CEntity* entity, CMap *map, const Map::CEntity *entityInfo);

		/**
		M�todo que activa el componente; invocado cuando se activa
		el mapa donde est� la entidad a la que pertenece el componente.
		<p>
		Se coge el jugador del mapa, la entidad que se quiere "seguir".

		@return true si todo ha ido correctamente.
		*/
		virtual bool activate();

		/**
		M�todo que desactiva el componente; invocado cuando se
		desactiva el mapa donde est� la entidad a la que pertenece el
		componente. Se invocar� siempre, independientemente de si estamos
		activados o no.
		<p>
		Se pone el objetivo a seguir a NULL.
		*/
		virtual void deactivate();

		/**
		M�todo llamado en cada frame que actualiza el estado del componente.
		<p>
		Se encarga de mover la c�mara siguiendo al jugador.

		@param msecs Milisegundos transcurridos desde el �ltimo tick.
		*/
		virtual void tick(unsigned int msecs);

		virtual bool accept(CMessage *message);

		virtual void process(CMessage *message);

	protected:
		void eventCallback(CallbackEvent callbackEvent);

		/**
		Da�o de la explosi�n
		*/
		float _damage;
		/**
		Radio donde se aplica el da�o de la explosi�n
		*/
		float _radius;
		/**
		Radio mayor o igual que radius que aplica una fuerza a las entidades que se encuentre dentro de este
		*/
		float _radius2;

		/**
		fuerza que se le aplicar� a las entidades cercanas tras la explosi�n
		*/
		float _force;

		unsigned int _count;
		bool _active;


	}; // class CExplode

	REG_FACTORY(CExplode);

} // namespace Logic

#endif // __Logic_Explode_H
