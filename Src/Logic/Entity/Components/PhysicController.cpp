/**
@file PhysicController.cpp

Contiene la implementaci�n del componente que se utiliza para representar jugadores y enemigos en
el mundo f�sico usando character controllers.

@see Logic::CPhysicController
@see Logic::CPhysicEntity
@see Logic::IPhysics

@author Antonio S�nchez Ruiz-Granados
@date Noviembre, 2012
*/

#include "PhysicController.h"

#include "Logic/Entity/Entity.h"
#include "Map/MapEntity.h"
#include "Physics/Server.h"
#include "Physics/CharacterController.h"

#include <PxPhysicsAPI.h>

using namespace Logic;
using namespace Physics;
using namespace physx; 

IMP_FACTORY(CPhysicController);

//---------------------------------------------------------

CPhysicController::CPhysicController() : IPhysics(), _controller(NULL), 
	_movement(0,0,0), _falling(true), _jumping(false), _stopEntity(false),
	_gravityAcceleration(0,0,0), _gravityValue(9.8),_jumpDirection(Vector3::UNIT_Y),
	_verticalOffset(0,0.1f,0), _fallingDamage(INT_MAX), _heightCounter(0.0f), _maxHeight(10.0f)
{

}

//---------------------------------------------------------

CPhysicController::~CPhysicController() 
{
	if (_controller) {
		delete _controller;
	}
} 

//---------------------------------------------------------

bool CPhysicController::spawn(CEntity* entity, CMap *map, const Map::CEntity *entityInfo)
{
	// Invocar al m�todo de la clase padre
	if(!IComponent::spawn(entity,map,entityInfo))
		return false;

	if(entityInfo->hasAttribute("maxFallingHeight"))
		_maxHeight = entityInfo->getFloatAttribute("maxFallingHeight");

	// Crear el character controller asociado al componente
	_controller = createController(entityInfo);

	return true;
}

//---------------------------------------------------------

bool CPhysicController::accept(CMessage *message)
{
	// TODO: recibir mensajes de tipo AVATAR_WALK
	return message->getType() == Message::AVATAR_WALK
		|| message->getType() == Message::RESET
		|| message->getType() == Message::JUMP
		|| message->getType() == Message::STOP_ENTITY
		|| message->getType() == Message::KINEMATIC_MOVE
		|| message->getType() == Message::TELETRANSPORT;

} 

//---------------------------------------------------------

void CPhysicController::process(CMessage *message)
{
	switch(message->getType())
	{
		// TODO: Procesar mensajes de tipo AVATAR_WALK
		// Anotamos el vector de desplazamiento recibido en el mensaje en el atributo
		// privado _movement para aplicar el movimiento en el tick
	case Message::KINEMATIC_MOVE:
		_movement += static_cast<CKinematicMoveMessage *>(message)->_displ;
		break;
	case Message::AVATAR_WALK :
		_movement += static_cast<CAvatarWalkMessage *>(message)->_movement;
		break;
		// Si se actualiza la posicion logica, hay que actualizar la fisica para no estar descuadrados
	case Message::RESET :
		_controller->setPosition(static_cast<CResetMessage*>(message)->_initialPos);
		break;
	case Message::JUMP :
		if (!_falling || !static_cast<CJumpMessage *>(message)->_jump) 
		{
			_jumpHeight = static_cast<CJumpMessage *>(message)->_height;
			_jumpTime = static_cast<CJumpMessage *>(message)->_time;
			_jumpDirection = static_cast<CJumpMessage *>(message)->_direction;
			_jumping = true;
			if (static_cast<CJumpMessage *>(message)->_jump)
				_heightCounter = 0;
		}

		break;
	case Message::STOP_ENTITY :
		_stopEntity = !_stopEntity;//static_cast<CBooleanMessage*>(message)->_value;
		break;

	case Message::TELETRANSPORT :
		_controller->setPosition(static_cast<CTeletransportMessage*>(message)->_position);
		break;
	}

} 

//---------------------------------------------------------

void CPhysicController::tick(unsigned int msecs) 
{
	// Llamar al m�todo de la clase padre (IMPORTANTE).
	IComponent::tick(msecs);


	// TODO: actualizar la posici�n de la entidad l�gica
	// 1. Recuperar la posici�n del controller f�sico usando el servidor
	// 2. Actualizar la posici�n de la entidad f�sica
	_entity->setPosition(_controller->getPosition() - _verticalOffset, this);

	if(!_stopEntity) {

		if (_falling) {
			_gravityAcceleration = Vector3(0, -9.8 * msecs * 0.001f * 1.2, 0);
			if (_gravityAcceleration.y < (- 55.0f * msecs * 0.001f) )
				_gravityAcceleration.y = - 55.0f * msecs * 0.001f; // Velocidad Terminal es de aproximadamente 55 m/s
		} 
		else
			_gravityAcceleration = Vector3(0, -9.8 * msecs * 0.001f * 1.2, 0);

		_movement += _gravityAcceleration;
		// Calculamos el salto, el metodo jumpController actualiza el vector _movement en 
		// funcion de _jumpTime, _jumpHeight y _falling
		unsigned jumpingFlag = ~ControllerFlag::JUMPING;
		if (_jumping)
			jumpingFlag = _controller->jump(_movement, _jumpTime, _jumpHeight, _jumpDirection, _falling, msecs);

		_jumping = (jumpingFlag & ControllerFlag::JUMPING) && true;

		if (_falling) 
		{
			if (jumpingFlag == ControllerFlag::STOP_JUMPING
				|| jumpingFlag == ~ControllerFlag::JUMPING)
			{
				_heightCounter -= _gravityAcceleration.y;
			}
		}
		else
		{
			if (_heightCounter >= _maxHeight)
			{
				CDamagedMessage* m = new CDamagedMessage();
				m->_damage = _fallingDamage;
				_entity->emitMessage(m);
			}
			_heightCounter = 0.0f;
		}
		// TODO: pedir al controller que se mueva
		// 1. Mover el controller usando el servidor de f�sica
		unsigned flags = _controller->move(_movement, msecs);//msecs

		if (flags == ControllerFlag::COLLISION_UP)
			_jumping = false;
		// TODO: comprobar si el controller toca el suelo o est� cayendo
		// Comprobar si est� activo el flag PxControllerFlag::eCOLLISION_DOWN en la
		// m�scara de bits que devuelve el m�todo que mueve el controller, y actualizar
		// el valor de _falling
		if ((flags & ControllerFlag::COLLISION_DOWN) && _jumpTime > 0 && _jumping && 
			_jumpDirection.y < 0)
		{
			// Comentado porque da problemas en algunos casos como puertas horizontales
			/*CDamagedMessage *msg = new CDamagedMessage();
			msg->_damage = 1000;
			_entity->emitMessage(msg);*/
		}

		bool flag = false;
		if (_falling) flag = true;
		_falling = !(flags & ControllerFlag::COLLISION_DOWN);
		if (! _falling && flag)
		{
			Logic::CControlMessage* msg = new Logic::CControlMessage();
			msg->_avatarAction = TAvatarActions::STOP_JUMP;
			_entity->emitMessage(msg, this);
		}

		// Ponemos el movimiento a cero
		_movement = Vector3::ZERO;
	}
}

//---------------------------------------------------------

Physics::CCharacterController* CPhysicController::createController(const Map::CEntity *entityInfo)
{
	// Obtenemos la posici�n de la entidad. Inicialmente colocaremos el controller
	// un poco por encima del suelo, porque si lo ponemos justo en el suelo a veces
	// lo atraviesa en el primer ciclo de simulaci�n.
	Vector3 position = _entity->getPosition() + Vector3(0, 1.0f, 0);

	// Leer el volumen de colisi�n del controller. Por ahora s�lo admitimos c�psulas.
	std::string shape = "capsule";
	if (entityInfo->hasAttribute("physic_shape")) {
		shape = entityInfo->getStringAttribute("physic_shape");
		assert(shape == "capsule");
	}

	// TODO: Crear el controller
	// 1. Leer radio y altura de la capsula
	// 2. Crear el controller usando el servidor de f�sica (pasar como componente this)
	float radius = entityInfo->getFloatAttribute("physic_radius");
	float height = entityInfo->getFloatAttribute("physic_height");

	// Grupo de colision entre character controllers
	int collisionGroup = FilterGroup::PLAYER_GROUP;
	if (entityInfo->hasAttribute("physic_cct_group")) 
		collisionGroup = entityInfo->getIntAttribute("physic_cct_group");

	unsigned int filterData = FilterGroup::EVERYTHING;
	if (entityInfo->hasAttribute("filterData"))
	{
		filterData = entityInfo->getIntAttribute("filterData");
		if (filterData == -1)
			filterData = FilterGroup::EVERYTHING;
		else
			filterData = ~filterData;
	}

	return new Physics::CCharacterController(position, radius, height, collisionGroup, filterData, this);
} 

//---------------------------------------------------------

void CPhysicController::onTrigger(IPhysics *otherComponent, bool enter)
{

}

//---------------------------------------------------------

void CPhysicController::onContact(IPhysics *otherComponent)
{

}

//---------------------------------------------------------

void CPhysicController::onShapeHit (const PxControllerShapeHit &hit)
{
	// TODO: aplicar una fuerza a la entidad din�mica contra la que chocamos
	// 1. Obtener el actor del objeto contra el que hemos colisionado
	// 2. Si es un actor est�tico no hacemos nada (usar m�todo de PxActor para comprobar)
	// 3. Si es un actor cinem�tico no hacemos nada (usar m�todo de CServer para comprobar)
	// 4. En otro caso aplicamos una fuerza a la entidad en la direcci�n del golpe
	/*if (hit.shape->getActor().userData)
	{
	CPhysicEntity *component = (CPhysicEntity *)hit.shape->getActor().userData;
	}*/
	PxRigidDynamic *actor =  hit.shape->getActor().isRigidDynamic();
	if (!actor) 
		return;

	if (Physics::CServer::getSingletonPtr()->isKinematic(actor)) 
		return;

	actor->addForce(hit.dir * hit.length * 1000.0f);
}

//---------------------------------------------------------

void CPhysicController::onControllerHit (const PxControllersHit &hit)
{

}

//---------------------------------------------------------



