/**
@file Backwards.cpp

Contiene la implementacion del componente que hace que una copia
"retroceda en el tiempo" reproduce la lista de movimientos al
rev�s.

@see Logic::CSave
@see Logic::ReplayManager
@see Logic::IComponent

@author Alejandro Perez Alonso
@date Febrero, 2014
*/

#include "Backwards.h"

#include "Logic/Entity/Entity.h"
#include "Logic/Maps/Map.h"
#include "Map/MapEntity.h"
#include "Logic/Managers/Replay System/ReplayManager.h"

namespace Logic 
{
	IMP_FACTORY(CBackwards);

	//---------------------------------------------------------

	bool CBackwards::spawn(CEntity *entity, CMap *map, const Map::CEntity *entityInfo) 
	{
		if(!IComponent::spawn(entity,map,entityInfo))
			return false;

		return true;

	} // spawn

	//---------------------------------------------------------

	bool CBackwards::accept(CMessage *message)
	{
		return message->getType() == Message::BACKWARDS && !_isPlaying;

	} // accept

	//---------------------------------------------------------

	std::list<CMessage*>& CBackwards::parserList(CMessageList &list)
	{
		CMessageList *out = new CMessageList();

		CMessageList::const_reverse_iterator it = list.rbegin();
		CMessageList::const_reverse_iterator end = list.rend();

		// Recorremos la lista de mensajes 
		for (; it != end; ++it)
		{
			// Giro
			if (((CControlMessage *)(*it))->_avatarAction == TAvatarActions::TURN)
			{
				CControlMessage* msg = new CControlMessage();
				msg->_avatarAction = TAvatarActions::TURN;
				msg->_time = ((CControlMessage *)(*it))->_time;
				msg->_yaw = -1 * ((CControlMessage *)(*it))->_yaw;
				msg->increaseRef();
				out->push_back(msg);
				//list.pop_back();
			}
			// Stop walk
			else if (((CControlMessage *)(*it))->_avatarAction == TAvatarActions::STOP_WALK)
			{
				// Buscar hacia abajo el tipo de walk
				CMessageList::const_reverse_iterator it2 = it;
				for ( ; it2 != end ; ++it2)
				{
					if (((CControlMessage *)(*it2))->_avatarAction == TAvatarActions::WALK ||
						((CControlMessage *)(*it2))->_avatarAction == TAvatarActions::STOP_WALK)
					{
						CControlMessage* msg = new CControlMessage();
						if (((CControlMessage *)(*it2))->_avatarAction == TAvatarActions::WALK)
							msg->_avatarAction = TAvatarActions::WALK_BACK;
						else
							msg->_avatarAction = TAvatarActions::WALK;
						msg->_time = ((CControlMessage *)(*it))->_time;
						msg->increaseRef();
						out->push_back(msg);
						break;
					}

				}
			}
			else if (((CControlMessage *)(*it))->_avatarAction == TAvatarActions::STOP_STRAFE)
			{
				// Buscar hacia abajo el tipo de walk
				CMessageList::const_reverse_iterator it2 = it;
				for ( ; it2 != end ; ++it2)
				{
					if (((CControlMessage *)(*it2))->_avatarAction == TAvatarActions::STRAFE_RIGHT ||
						((CControlMessage *)(*it2))->_avatarAction == TAvatarActions::STRAFE_LEFT)
					{
						CControlMessage* msg = new CControlMessage();
						if (((CControlMessage *)(*it2))->_avatarAction == TAvatarActions::STRAFE_RIGHT)
							msg->_avatarAction = TAvatarActions::STRAFE_LEFT;
						else
							msg->_avatarAction = TAvatarActions::STRAFE_RIGHT;
						msg->_time = ((CControlMessage *)(*it))->_time;
						msg->increaseRef();
						out->push_back(msg);
						break;
					}

				}
			}
			// walk o walkBack
			else if (((CControlMessage *)(*it))->_avatarAction == TAvatarActions::WALK ||
				((CControlMessage *)(*it))->_avatarAction == TAvatarActions::WALK_BACK)
			{

				// Buscar si hay un stop mas arriba
				CMessageList::const_iterator it2 = it.base();
				for (; it2 != list.end(); ++it2)
				{
					if (((CControlMessage *)(*it2))->_avatarAction == TAvatarActions::STOP_WALK)
					{
						CControlMessage* msg = new CControlMessage();
						msg->_avatarAction = ((CControlMessage *)(*it2))->_avatarAction;
						msg->_time = ((CControlMessage *)(*it))->_time;
						msg->increaseRef();
						out->push_back(msg);
						break;
					}
				}
				if (it2 == list.end())
				{
					CControlMessage* msg = new CControlMessage();
					msg->_avatarAction = ((CControlMessage *)(*it))->_avatarAction;
					msg->_time = _clock;
					msg->increaseRef();
					out->push_front(msg);
				}

			}
			else if ( ((CControlMessage *)(*it))->_avatarAction == TAvatarActions::STRAFE_RIGHT ||
				((CControlMessage *)(*it))->_avatarAction == TAvatarActions::STRAFE_LEFT)
			{

				// Buscar si hay un stop mas arriba+
				CMessageList::const_iterator it2 = it.base();
				for (; it2 != list.end(); ++it2)
				{
					if (! ((CControlMessage *)(*it2))->_avatarAction == TAvatarActions::STOP_STRAFE)
					{
						CControlMessage* msg = new CControlMessage();
						msg->_avatarAction = ((CControlMessage *)(*it2))->_avatarAction;
						msg->_time = ((CControlMessage *)(*it))->_time;
						msg->increaseRef();
						out->push_back(msg);
						break;
					}
				}
				if (it2 == list.end())
				{
					CControlMessage* msg = new CControlMessage();
					msg->_avatarAction = ((CControlMessage *)(*it))->_avatarAction;
					msg->_time = _clock;
					msg->increaseRef();
					out->push_front(msg);
				}

			}
		}

		return *out;

	} //parserList

	//---------------------------------------------------------

	void CBackwards::process(CMessage *message)
	{
		if (message->getType() == Message::BACKWARDS)
		{

			// Procesamos el mensaje
			CBackwardsMessage* backwardsMessage = static_cast<CBackwardsMessage *>(message);

			_messageList = &backwardsMessage->_messageList; //puntero a la lista de mensajes
			_duration = backwardsMessage->_duration;
			_clock = backwardsMessage->_time;

			// Crear una nueva lista con los movimientos invertidos
			_messageList = &parserList(*_messageList);

			_isPlaying = true;
		}

	} // process

	//---------------------------------------------------------

	void CBackwards::tick(unsigned int msecs)
	{
		IComponent::tick(msecs);

		if (_isPlaying)
		{
			if (_duration > 0 && _messageList->size() > 0)
			{
				while (_messageList->size() > 0 && static_cast<CControlMessage *>(_messageList->front())->_time == _clock)
				{
					_entity->emitMessage(_messageList->front());
					_messageList->front()->release();
					_messageList->pop_front();
				}
			}	
			else
			{
				// Liberamos lo que queda de lista
				if (_messageList && _messageList->size() > 0)
				{
					CMessageList::const_iterator itr = _messageList->begin();
					CMessageList::const_iterator end = _messageList->end();
					for ( ; itr != end; ++itr)
					{
						(*itr)->release();
					}
					_messageList->clear();
				}
				delete _messageList;
				_messageList = NULL;

				// Enviamos mensaje de StopAll para parar todo y evitar errores
				CControlMessage* msg = new CControlMessage();
				msg->_avatarAction = TAvatarActions::STOP_ALL;
				msg->_time = _clock;
				_entity->emitMessage(msg);

				// Llamamos al metodo de stopBackwards del replay manager para que vuelva a activar
				// el componente que guarda los movimientos.
				CReplayManager::getSingletonPtr()->stopBackwards(_entity, _clock);
				_isPlaying = false;
			}
			_duration -= msecs;
			_clock -= msecs;
		}
	} // tick


} // namespace Logic

