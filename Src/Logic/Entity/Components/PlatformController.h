/**
@file PlatformController.h

Contiene la declaraci�n del componente que controla el movimiento de una
plataforma. AutoStart, Loop, etc.

@author Alejandro P�rez Alonso
@date Marzo, 2014
*/
#ifndef __Logic_Platform_Controller_H
#define __Logic_Platform_Controller_H

#include "Logic/Entity/Component.h"


//declaraci�n de la clase
namespace Logic 
{
/**

*/
	class CPlatformController : public IComponent
	{
		DEC_FACTORY(CPlatformController);
	public:

		/**
		Constructor por defecto; en la clase base no hace nada.
		*/
		CPlatformController() : IComponent(), _loop(true), _isActivated(true), _nextTarget(0), 
			_direction(1), _speed(0), _running(false) {}
		
		/**
		Inicializaci�n del componente usando la descripci�n de la entidad que hay en 
		el fichero de mapa.
		*/
		virtual bool spawn(CEntity* entity, CMap *map, const Map::CEntity *entityInfo);

		/**
		
		*/
		virtual bool accept(CMessage *message);

		/**
		
		*/
		virtual void process(CMessage *message);

		/**

		*/
		void tick(unsigned int msecs);

		Vector3 getDirection();

	protected:

		// Vector de posiciones sobre los que se desplazar� la entidad
		std::vector<Vector3> _vectList;

		// Indica siguiente posicion del vector de posiciones hacia la que se dirigira la entidad.
		int _nextTarget;

		// Indica si el movimiento ser� en loop
		bool _loop; 

		// Bolean que indica que en la siguiente iteracion. Sirve para evitar que una plataforma
		// continue en bucle al ser desactivada. Regresa al punto inicial y se detiene.
		bool _nextLoop;

		// Indica si el objeto ha sido activado por un trigger o no.
		bool _isActivated;

		bool _running;

		float _tolerance;

		// Indica la direcci�n a la que se movera la entidad
		int _direction;

		float _speed;

	}; // class CPlatformController

	REG_FACTORY(CPlatformController);

} // namespace Logic

#endif // __Logic_Platform_Controller_H
