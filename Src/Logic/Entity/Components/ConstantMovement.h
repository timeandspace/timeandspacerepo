/**
@file ConstantMovement.h

Contiene la declaraci�n de una clase que realiza un 
movimiento constante en la direccion dada.

@author Alejandro P�rez Alonso
@date Abril, 2014
*/

#ifndef __Logic_ConstantMovement_H
#define __Logic_ConstantMovement_H

#include "Logic/Entity/Component.h"

namespace Logic
{

	/**
	Clase CConstantMovement
	*/
	class CConstantMovement : public IComponent
	{
		DEC_FACTORY(CConstantMovement);
	public:

		/**
		*/
		CConstantMovement() : IComponent(), _direction(Vector3::ZERO), _speed(0.0f), 
			_finalPosition(Vector3::ZERO)
		{
		}

		/**
		*/
		~CConstantMovement() {}

		/**
		*/
		virtual bool spawn(CEntity* entity, CMap *map, const Map::CEntity *entityInfo);

		/**
		*/
		virtual void tick(unsigned int msecs);

	protected:
		
		Vector3 _direction;
		Vector3 _finalPosition;
		float _speed;

	}; // Class CShooter

	REG_FACTORY(CConstantMovement);

} // namespace Logic

#endif // __Logic_LifeTime_H