/**
@file Shooter.cpp

Contine la implementaci�n del componente

@see Logic::IComponent

@author Alejandro P�rez Alonso
@date Febrero, 2014
*/

#include "Shooter.h"

#include "Logic/Entity/Entity.h"
#include "Map/MapEntity.h"
#include "Physics/Server.h"
#include "Logic/Maps/EntityFactory.h"
#include "Logic/Maps/map.h"
#include "PerceptionComponent.h"
#include "Camera.h"
#include "Graphics/Server.h"

#include "Weapon.h"

#include "Sound/Sound.h"
#include "Sound/Server.h"

namespace Logic
{
	IMP_FACTORY(CShooter);


	//---------------------------------------------------------

	CShooter::CShooter() : IComponent(), _currentWeapon(TWeaponType::NORMAL), 
		_modifier(0), _cam(0)
	{
	}

	//---------------------------------------------------------

	CShooter::~CShooter() 
	{
	}

	//---------------------------------------------------------

	bool CShooter::spawn(CEntity *entity, CMap *map, const Map::CEntity *entityInfo)
	{
		if (!IComponent::spawn(entity, map, entityInfo))
			return false;

		_modifier = entityInfo->getFloatAttribute("clock_modifier");

		return true;

	} // spawn

	//---------------------------------------------------------

	bool CShooter::activate()
	{
		_cam = (CCamera *)_entity->getMap()->getEntityByName("Camera")->
			getComponent("CCamera");

		return IComponent::activate();
	} // activate

	//---------------------------------------------------------

	void CShooter::deactivate()
	{
		IComponent::deactivate();
	} // deactivate

	//---------------------------------------------------------

	bool CShooter::accept(CMessage *message)
	{
		return message->getType() == Message::FIRE 
			|| message->getType() == Message::CHANGE_WEAPON;

	} // accept

	//---------------------------------------------------------

	void CShooter::process(CMessage *message)
	{

		if (message->getType() ==  Message::FIRE)
		{	
			// Accedemos al componente CWeapon
			const Vector3 shootingPoint = _weapons[_currentWeapon]->getShootingPoint();

			Vector3 result; std::string hitEntityName = "";
			if (_entity->isPlayer())
			{
				Graphics::CServer::getSingletonPtr()->raycastClosest(shootingPoint, result, 
					hitEntityName);
			}
			else
			{
				CFireMessage *msg = static_cast<CFireMessage *>(message);
				if (!msg->_direction)
				{
					_cam = (CCamera *)_entity->getMap()->getEntityByName("Camera")->
						getComponent("CCamera");
					float offset = _cam->getOffset();
					float zoomdistance = _cam->getDistance();
					if (msg->_cameraType == TCameraType::AIMING)
						zoomdistance = _cam->getZoomDistance();
					Vector3 position = _entity->getPosition() + 
						offset * Math::getDirection(_entity->getYaw() + Math::PI/2);
					Vector3 dir = -zoomdistance * 
						(Math::getDirection(_entity->getOrientation()));
					dir.y = _cam->getHeight() - _entity->getPitch() * _cam->getHeight();
					Vector3 camPos = position + dir;
					dir = _cam->getTargetDistance() * Math::getDirection(_entity->getOrientation());
					dir.y = _cam->getTargetHeight() + _entity->getPitch()/2 * _cam->getTargetHeight();
					Vector3 targetPos = position + dir;
					dir = targetPos - camPos;
					float maxDist = dir.normalise();
					// Lanzamos rayos para detectar posibles obst�culos entre la c�mara y el target
					Ray ray(targetPos, -dir);
					Physics::Collision collision;
					if (Physics::CServer::getSingletonPtr()->
						raycastSingle(ray, maxDist, Physics::FilterGroup::WORLD, collision))
					{
						if (collision.distance > _cam->getTargetDistance())
							camPos = targetPos + collision.distance * -dir;
					}
					// Calculamos disparo
					Graphics::CServer::getSingletonPtr()->raycastClosest(shootingPoint, 
						Ray(camPos, dir), result, hitEntityName);
				}
				else
				{
					Vector3 entPos = _entity->getPosition();
					float height = ((CPerceptionComponent *)_entity->getComponent("CPerceptionComponent"))->getSightHeight();
					entPos.y += height;
					Graphics::CServer::getSingletonPtr()->raycastClosest(shootingPoint, 
						Ray(entPos, *msg->_direction), result, hitEntityName);
				}
			}
			_weapons[_currentWeapon]->fire(result, hitEntityName);
		}

		// Cambia el tipo de arma
		else if (message->getType() ==  Message::CHANGE_WEAPON)
		{
			if (_weapons.size() > 1)
			{
				// Rotacion y posici�n para colocar el arma a la espalda
				_weapons[_currentWeapon]->detachWeapon();
				Quaternion offsetOrientation(Ogre::Degree(90), Vector3::UNIT_Y);
				Quaternion qX(Ogre::Degree(90), Vector3::UNIT_X);
				Quaternion qZ(Ogre::Degree(-25), Vector3::UNIT_Z);
				offsetOrientation = offsetOrientation * qX * qZ;
				Vector3 offsetPosition =  Vector3(-0.2, 0.2, -0.2);
				// Attachamos arma a la espalda
				_weapons[_currentWeapon]->attachWeapon(_entity->getName(), "Bip001 R Clavicle", offsetOrientation, offsetPosition);

				// Elegimos siguiente arma (indice array)
				_currentWeapon = (_currentWeapon + static_cast<CChangeWeaponMessage *>(message)->_weaponType) % _weapons.size();

				// Deacoplamos arma de la espalda
				_weapons[_currentWeapon]->detachWeapon();
				// Rotacion y posici�n mano
				offsetOrientation = Quaternion(Ogre::Degree(-110), Vector3::UNIT_Y);
				qX = Quaternion(Ogre::Degree(/*45*/10), Vector3::UNIT_X);
				qZ = Quaternion(Ogre::Degree(-20), Vector3::UNIT_Z);
				offsetOrientation = offsetOrientation * qX * qZ;
				offsetPosition =  Vector3(0.4, 0, -0.05);
				// Attachamos arma elegida a la mu�eca
				_weapons[_currentWeapon]->attachWeapon(_entity->getName(), "Bip001 R Hand", offsetOrientation, offsetPosition);
			}
		}

	} // process

	//---------------------------------------------------------

	void CShooter::removeWeapon(CWeapon *ent)
	{
		ent->detachWeapon();

		Quaternion offsetOrientation(Ogre::Degree(90), Vector3::UNIT_Y);
		Quaternion qX(Ogre::Degree(90), Vector3::UNIT_X);
		Quaternion qZ(Ogre::Degree(-25), Vector3::UNIT_Z);
		offsetOrientation = offsetOrientation * qX * qZ;
		Vector3 offsetPosition =  Vector3(-0.2, 0.2, -0.2);
		// Attachamos arma a la espalda
		_weapons[_currentWeapon]->attachWeapon(_entity->getName(), "Bip001 R Clavicle", offsetOrientation, offsetPosition);

	} //removeWeapon

	//---------------------------------------------------------

	void CShooter::releaseWeapons()
	{
		if (_weapons.size() > 0)
		{
			for (unsigned int i = 0; i < _weapons.size(); ++i)
			{
				_weapons[i]->throwWeapon();
			}
		}
	} // releaseWeapons


	//---------------------------------------------------------
	//void CShooter::fire(const Vector3 &position, const Vector3 &direction)
	//{
	//	Vector3 origin;
	//	Vector3 entityPosition = _entity->getPosition();

	//	origin.x = entityPosition.x + _shootingPoint.x * Math::getDirection(_entity->getYaw() + Math::PI/2).x;
	//	origin.y = _shootingPoint.y;
	//	origin.z = entityPosition.z + _shootingPoint.z * Math::getDirection(_entity->getYaw()).z;

	//	Vector3 result; std::string hitEntityName = "";
	//	if (_entity->isPlayer())
	//	{
	//		Graphics::CServer::getSingletonPtr()->raycastClosest(origin, result, 
	//			hitEntityName);
	//	}
	//	else
	//	{
	//		Graphics::CServer::getSingletonPtr()->raycastClosest(origin, 
	//			Ray(position, direction), result, hitEntityName);
	//	}
	//	// En funci�n del tipo de arma seleccionada, instanciamos una entidad u otra.
	//	Map::CEntity* entityInfo = NULL;
	//	entityInfo = CEntityFactory::getSingletonPtr()->getMapEntityByType("Bullet");
	//	assert(entityInfo && "CShooter: Error al obtener el prefab Bullet");
	//	if ( !hitEntityName.empty() )
	//	{
	//		CEntity *hitEntity = _entity->getMap()->getEntityByName(hitEntityName);
	//		if (hitEntity)
	//		{
	//			if (_currentWeapon == TWeaponType::NORMAL)
	//			{
	//				CDamagedMessage *m = new CDamagedMessage();
	//				m->_damage = _damage;
	//				hitEntity->emitMessage(m);
	//				// Aparte de enviar da�o enviamos un mensaje de aplicar fuerza
	//				// para reventar barriles basicamente.

	//				CAddForceMessage *forceMessage = new CAddForceMessage();
	//				forceMessage->_direction = direction;
	//				forceMessage->_force = 1000.0f;
	//				hitEntity->emitMessage(forceMessage);

	//			}
	//			else if (_currentWeapon == TWeaponType::SLOW_DOWN)
	//			{
	//				CEditTimeMessage *m =  new CEditTimeMessage(TWeaponType::SLOW_DOWN,
	//					-_modifier, _duration);
	//				hitEntity->emitMessage(m);

	//			}
	//			else if (_currentWeapon == TWeaponType::SPEED_UP)
	//			{
	//				CEditTimeMessage *m =  new CEditTimeMessage(TWeaponType::SPEED_UP, 
	//					_modifier, _duration);
	//				hitEntity->emitMessage(m);

	//			}
	//			else if (_currentWeapon == TWeaponType::BACKWARD)
	//			{
	//				CEditTimeMessage *m =  new CEditTimeMessage(TWeaponType::BACKWARD, 
	//					_modifier, _duration);
	//				hitEntity->emitMessage(m);
	//			}
	//		}
	//	}

	//	std::stringstream ss;

	//	/*origin = result - entityPosition;
	//	origin.normalise();
	//	origin *= 20;
	//	origin += entityPosition;
	//	origin.y = _shootingPoint.y;*/
	//	origin = Graphics::CServer::getSingletonPtr()->
	//		getBonePosition(_entity->getName(), "Bip01 L Hand");
	//	/*Graphics::CServer::getSingletonPtr()->createParticleSystem(_entity->getName(),
	//	"fire", "disparo", origin);	*/

	//	// Asignamos la posicion de inicio
	//	ss << origin.x << " " << origin.y << " " << origin.z; 
	//	entityInfo->setAttribute("position", ss.str());

	//	// Vaciar stringstream
	//	ss.str("");
	//	ss.clear();

	//	ss << result.x << " " << result.y << " " << result.z; 
	//	entityInfo->setAttribute("finalPosition", ss.str());

	//	// Vaciar stringstream
	//	ss.str("");
	//	ss.clear();

	//	// Asignamos la direcci�n de movimiento. Ya que las balas no son instantaneas
	//	result -= origin;
	//	result.normalise();
	//	ss << result.x << " " << result.y << " " << result.z; 
	//	entityInfo->setAttribute("direction", ss.str());

	//	// Vaciar stringstream
	//	ss.str("");
	//	ss.clear();

	//	// Asigamos nombre �nico
	//	//TODO:: HACER UNA ESPECIE DE EntityID.h PARA TODAS LAS ENTIDADES INSTANCIADAS
	//	// O MODIDIFICARLO PARA QUE PUEDAN ACCEDER TODOS.
	//	// Hack:: ej: SlowBullet_David_1 or BackBullet_Peter2_4
	//	entityInfo = new Map::CEntity(*entityInfo);
	//	ss << entityInfo->getName() << "_" << _entity->getName() << "_" << ++_id;	
	//	entityInfo->setName(ss.str());

	//	// Creamos una nueva entidad con el prefab
	//	Logic::CEntity* bulletEntity = CEntityFactory::getSingletonPtr()->
	//		createEntity(entityInfo, _entity->getMap());
	//	delete entityInfo;

	//	assert(bulletEntity && "Error al crear la entidad bala!");

	//	// Creamos efecto de particulas
	//	Graphics::CServer::getSingletonPtr()->createParticleSystem(bulletEntity->getName(),
	//		bulletEntity->getName() + "_p", "MisParticulas");
	//	Graphics::CServer::getSingletonPtr()->setParticleSystemDirection(bulletEntity->getName() + "_p", result);
	//	// Sonido
	//	CSoundMessage *soundmsg = new CSoundMessage();
	//	_entity->emitMessage(soundmsg);


	//}


	//void CShooter::fire(CFireMessage* message)
	//{
	//	// Si es de tipo Enemy
	//	if (!message->_direction)
	//	{
	//		// �apa para que las copias repitan los disparos, ya que no pueden lanzar un rayo
	//		// de la c�mara al centro del viewport.
	//		cam = (CCamera *)_entity->getMap()->getEntityByName("Camera")->
	//			getComponent("CCamera");
	//		float offset = cam->getOffset();
	//		float zoomdistance = cam->getDistance();
	//		if (message->_cameraType == TCameraType::AIMING)
	//			zoomdistance = cam->getZoomDistance();
	//		Vector3 position = _entity->getPosition() + 
	//			offset * Math::getDirection(_entity->getYaw() + Math::PI/2);
	//		Vector3 dir = -zoomdistance * 
	//			(Math::getDirection(_entity->getOrientation()));
	//		dir.y = cam->getHeight() - _entity->getPitch() * cam->getHeight();
	//		Vector3 camPos = position + dir;
	//		dir = cam->getTargetDistance() * Math::getDirection(_entity->getOrientation());
	//		dir.y = cam->getTargetHeight() + _entity->getPitch()/2 * cam->getTargetHeight();
	//		Vector3 targetPos = position + dir;
	//		dir = targetPos - camPos;
	//		dir.normalise();
	//		fire(camPos, dir);
	//	}
	//	else
	//	{
	//		Vector3 entPos = _entity->getPosition();
	//		float height = ((CPerceptionComponent *)_entity->getComponent("CPerceptionComponent"))->getSightHeight();
	//		entPos.y += height;
	//		fire(entPos, *message->_direction);
	//	}


	//}
	//---------------------------------------------------------

	void CShooter::tick(unsigned int msecs)
	{
		IComponent::tick(msecs);
	} // tick


} // namespace Logic
