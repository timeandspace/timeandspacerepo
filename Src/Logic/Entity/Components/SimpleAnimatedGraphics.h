/**
@file SimpleAnimatedGraphics.h

Contiene la declaración del componente que controla la representación
gráfica de una entidad estática.

@see Logic::CSimpleAnimatedGraphics
@see Logic::CGraphics

@author Alvaro Blazquez
@date Agosto, 2010
*/
#ifndef __Logic_SimpleAnimatedGraphics_H
#define __Logic_SimpleAnimatedGraphics_H

#include "Graphics.h"
#include "Graphics/AnimatedEntity.h"


namespace Graphics 
{
	class CAnimatedEntity;
}

namespace Logic 
{
	class CSimpleAnimatedGraphics : public CGraphics, public Graphics::CAnimatedEntityListener
	{
		DEC_FACTORY(CSimpleAnimatedGraphics);
	public:

		/**
		Constructor por defecto; inicializa los atributos a su valor por 
		defecto.
		*/
		CSimpleAnimatedGraphics();

		~CSimpleAnimatedGraphics();

		virtual bool accept(CMessage *message);

		virtual void process(CMessage *message);

		virtual bool activate();
		
		virtual void deactivate();

		void animationFinished(const std::string &animation);

	protected:

		void playAnimation();

		virtual Graphics::CEntity* createGraphicsEntity(const Map::CEntity *entityInfo);

		void stopAnimation(); 

		void setEntityClockModifier(); 
		
		Graphics::CAnimatedEntity *_animatedGraphicsEntity;

		std::string _defaultAnimationName;

		std::string _animationName;

		bool _loop;

	}; // class CSimpleAnimatedGraphics

	REG_FACTORY(CSimpleAnimatedGraphics);

} // namespace Logic

#endif // __Logic_SimpleAnimatedGraphics_H
