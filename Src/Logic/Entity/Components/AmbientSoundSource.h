/**
@file SoundSource.h

@author �lvaro Bl�zquez Checa
@date Marzo, 2014
*/

#ifndef __Logic_AmbientSoundSource_H
#define __Logic_AmbientSoundSource_H

#include "Logic/Entity/Component.h"

namespace Sonido
{
	class CSound;
}

namespace Logic
{
	/**
	Clase CAmbientSoundSource
	*/
	class CAmbientSoundSource : public IComponent
	{
		DEC_FACTORY(CAmbientSoundSource);
	public:

		/**
		*/
		CAmbientSoundSource() : IComponent(), _sound(NULL)
		{
		}

		/**
		*/
		~CAmbientSoundSource() 
		{
			if(_sound)
				delete _sound;
		}

		/**
		*/
		virtual bool spawn(CEntity* entity, CMap *map, const Map::CEntity *entityInfo);

		/**
		*/
		virtual bool activate();

		/**
		*/
		virtual void deactivate();

		/**
		*/
		virtual bool accept(CMessage *message);

		/**
		*/
		virtual void process(CMessage *message);

		/**
		*/
		virtual void tick(unsigned int msecs);


		void play();


		void stop();


		void pause(bool paused);

	protected:

		Sonido::CSound *_sound;


	}; 

	REG_FACTORY(CAmbientSoundSource);

} // namespace Logic

#endif 