/**

*/

#include "SpecialActionTrigger.h"

#include "Logic/Entity/Entity.h"
#include "Logic/Maps/Map.h"
#include "Map/MapEntity.h"

namespace Logic
{
	IMP_FACTORY(CSpecialActionTrigger);

	//---------------------------------------------------------

	bool CSpecialActionTrigger::spawn(CEntity *entity, CMap *map, const Map::CEntity *entityInfo) 
	{
		if(!IComponent::spawn(entity,map,entityInfo))
			return false;

		std::string specialActionString = entityInfo->getStringAttribute("specialAction");

		_object = entityInfo->getStringAttribute("object");

		if(!specialActionString.compare("getObject"))
		{
			_specialAction = TSpecialActions::GET_OBJECT;
		}
		else if(!specialActionString.compare("action"))
		{
			_specialAction = TSpecialActions::ACTION;
		}

		return true;

	} // spawn

	//---------------------------------------------------------

	bool CSpecialActionTrigger::accept(CMessage *message)
	{
		return message->getType() == Message::TOUCHED 
			|| message->getType() == Message::UNTOUCHED
			|| message->getType() == Message::SPECIAL_ACTION;

	} // accept

	//---------------------------------------------------------

	void CSpecialActionTrigger::process(CMessage *message)
	{
		//Si la colision del trigger es contra el mundo el mensaje recibido contendra NULL en su atributo _entity
		//Por lo tanto no enviamos mensaje de CSpecialActionMessage ya que petaria.
		switch(message->getType())
		{
		case Message::TOUCHED: // attach entity
			{
				CTouchedMessage *touchedMessage = static_cast<CTouchedMessage *>(message);
				CEntity *otherEntity = touchedMessage->_entity;
				if(!otherEntity)
					return;
				//if (_count == 0)
				//{
					CSpecialActionMessage* msg = new CSpecialActionMessage(_specialAction, true, _entity, _object);
					otherEntity->emitMessage(msg);
				//}

				_entityList.push_back(static_cast<CTouchedMessage *>(message)->_entity->getName());

				_count++;
				break;
			}
		case Message::UNTOUCHED: // deattach entity
			{
				CTouchedMessage *touchedMessage = static_cast<CTouchedMessage *>(message);
				CEntity *otherEntity = touchedMessage->_entity;
				if(!otherEntity)
					return;
				_count --;
				CSpecialActionMessage* msg = new CSpecialActionMessage(_specialAction, false, _entity);
				otherEntity->emitMessage(msg);

				// Buscamos la entidad en la lista de entidades para darla de baja
				std::list<std::string>::iterator it = _entityList.begin();
				std::list<std::string>::iterator end = _entityList.end();
				while (it != end)
				{
					if ((*it) == static_cast<CUnTouchedMessage *>(message)->_entity->getName())
					{
						it = _entityList.erase(it);
						break;
					}
					++it;
				}

				break;
			}

		case Message::SPECIAL_ACTION:
			{
				CSpecialActionMessage *specialMessage = static_cast<CSpecialActionMessage *>(message);
				TSpecialActions specialAction = specialMessage->_specialAction;

				if(specialAction == TSpecialActions::ACTION_CONFIRMED)
				{
					CActionMessage *actionMessage = new CActionMessage();
					_entity->emitMessage(actionMessage,this);
				}
				break;
			}
		}
	} // process

	//---------------------------------------------------------

	void CSpecialActionTrigger::tick(unsigned int msecs)
	{
		IComponent::tick(msecs);

		if (_entityList.size() > 0)
		{
			std::list<std::string>::iterator it = _entityList.begin();
			std::list<std::string>::iterator end = _entityList.end();
			while (it != end)
			{
				//TODO:: cuando se elimina una entidad no se pq pero nunca es NULL
				if (_entity->getMap()->getEntityByName((*it)) == NULL)
				{
					it = _entityList.erase(it);
					continue;
				}

				++it;
			}
			_count = _entityList.size();
		}
	} // tick

	//---------------------------------------------------------
}