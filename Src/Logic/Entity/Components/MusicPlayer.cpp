/**
@file MusicPlayer.cpp

Contine la implementaci�n del componente

@see Logic::IComponent

@author �lvaro Bl�zquez Checa
@date Marzo, 2014

*/

#include "MusicPlayer.h"

#include "Logic/Entity/Entity.h"
#include "Map/MapEntity.h"
#include "Sound/Sound.h"
#include "Sound/SoundBank.h"
#include "Sound/Server.h"
#include "Sound/MusicPlayer.h"

namespace Logic
{
	IMP_FACTORY(CMusicPlayer);

	//---------------------------------------------------------
	bool CMusicPlayer::spawn(CEntity *entity, CMap *map, const Map::CEntity *entityInfo)
	{
		if (!IComponent::spawn(entity, map, entityInfo))
			return false;

		bool loop = false;
		if(entityInfo->hasAttribute("loop"))
		{
			loop = entityInfo->getBoolAttribute("loop");
		}

		float volume = 1.0f;
		if(entityInfo->hasAttribute("volume"))
		{
			volume = entityInfo->getFloatAttribute("volume");
		}

		if(entityInfo->hasAttribute("sound_name"))
		{
			std::string soundName = entityInfo->getStringAttribute("sound_name");

			_sound = new Sonido::CSound("music/"+soundName , loop);

			Sonido::CServer::getSingletonPtr()->getMusicPlayer()->loadPlayListFromFile("RockingLoop1.ogg");
		}

		
		if(entityInfo->hasAttribute("sound_name_studio"))
		{
			_soundName = entityInfo->getStringAttribute("sound_name_studio");

			_sound = new Sonido::CSound(_soundName);

			_sound->setVolume(volume);
		}
		
		assert(_sound && "Falta el archivo de sonido");

		return true;

	} // spawn

	//---------------------------------------------------------

	bool CMusicPlayer::activate()
	{
		_sound->play();
		return IComponent::activate();
		
	} // activate

	//---------------------------------------------------------

	void CMusicPlayer::deactivate()
	{
		IComponent::deactivate();
		assert(_sound);
		_sound->stop();
	} // deactivate

	//---------------------------------------------------------

	bool CMusicPlayer::accept(CMessage *message)
	{
		return message->getType() == Message::PLAY_SOUND ||
			message->getType() == Message::STOP_SOUND ||
			message->getType() == Message::CHANGE_PARAMETER;
	} // accept

	//---------------------------------------------------------

	void CMusicPlayer::process(CMessage *message)
	{
		if (message->getType() ==  Message::PLAY_SOUND)
		{	
			play();
		}
		else if (message->getType() ==  Message::STOP_SOUND)
		{	
			stop();
		}
		else if (message->getType() ==  Message::CHANGE_PARAMETER)
		{	
			stop();
		}

	} // process

	//---------------------------------------------------------

	void CMusicPlayer::tick(unsigned int msecs)
	{
		IComponent::tick(msecs);

	} // tick

	//---------------------------------------------------------

	void CMusicPlayer::play()
	{
		assert(_sound);
		_sound->play();
	}

	//---------------------------------------------------------

	void CMusicPlayer::stop()
	{
		assert(_sound);
		_sound->stop();
	}

	//---------------------------------------------------------

	void CMusicPlayer::pause(bool paused)
	{
		assert(_sound);
	}

	//---------------------------------------------------------

	void CMusicPlayer::setParameter(float parameterValue)
	{
		_sound->setParameter(parameterValue);
	}

	//---------------------------------------------------------

	void CMusicPlayer::setVolume(float volume)
	{
		_sound->setVolume(volume);
	}

} // namespace Logic
