/**
@file AvatarController.cpp

Contiene la implementaci�n del componente que controla el movimiento 
de la entidad.

@see Logic::CAvatarController
@see Logic::IComponent

@author David Llans�
@author �lvaro Bl�zquez Checa
@author Alejandro P�rez Alonso

@date Agosto, 2010
@date Marzo, 2014
*/

#include "NPCController.h"

#include "Logic/Maps/EntityFactory.h"
#include "Logic/Entity/Entity.h"
#include "Logic/Maps/map.h"
#include "Map/MapEntity.h"

#include "Map/MapParser.h"

#include "Weapon.h"
#include "Shooter.h"

#include "Graphics/Server.h"
#include "Graphics/Entity.h"

#include "Application/BaseApplication.h"

#include "AI/PerceptionManager.h"

#include "Logic/Managers/Player Manager/PlayerManager.h"

namespace Logic 
{
	IMP_FACTORY(CNPCController);

	//---------------------------------------------------------

	bool CNPCController::spawn(CEntity *entity, CMap *map, const Map::CEntity *entityInfo) 
	{
		if (!IComponent::spawn(entity,map,entityInfo))
			return false;

		if (entityInfo->hasAttribute("speed"))
			_speed = entityInfo->getFloatAttribute("speed");
		if (entityInfo->hasAttribute("runSpeed"))
			_runSpeed = entityInfo->getFloatAttribute("runSpeed");
		if (entityInfo->hasAttribute("jump_time"))
			_jumpTime = entityInfo->getIntAttribute("jump_time");
		if (entityInfo->hasAttribute("jump_height"))
			_jumpHeight = entityInfo->getFloatAttribute("jump_height");

		// Cadencia de disparo
		if (entityInfo->hasAttribute("fire_rate"))
			_fireRate = entityInfo->getIntAttribute("fire_rate");

		if (entityInfo->hasAttribute("hasWeapon"))
		{
			_hasWeapon = entityInfo->getBoolAttribute("hasWeapon");
			// Que tipo de Arma tiene la IA
			_weaponName = entityInfo->getStringAttribute("weaponName");
		}

		return true;
	} // spawn

	//---------------------------------------------------------

	bool CNPCController::activate()
	{
		IComponent::activate();

		/* Si el enemigo tiene arma de inicio se la asignamos */
		if (_hasWeapon)
		{
			Map::CEntity *weaponEntityInfo = new Map::CEntity( *Logic::CEntityFactory::getSingletonPtr()->
				getMapEntityByName(_weaponName) );
			weaponEntityInfo->setName(weaponEntityInfo->getName() + _entity->getName());
			// Creamos una nueva entidad con el prefab
			Logic::CEntity* weaponEntity = CEntityFactory::getSingletonPtr()->
				createEntity(weaponEntityInfo, _entity->getMap());
			CWeapon *weapon = (CWeapon *)weaponEntity->getComponent("CNormalWeapon");
			if (weapon)
			{
				weapon->attachWeapon(_entity->getName(), "Bip01 R Hand");
			}

			// A�adimos el arma al componente Shooter
			((CShooter *)(_entity->getComponent("CShooter")))->addWeapon(weapon);

			delete weaponEntityInfo;
		}

		return true;
	} // activate

	//---------------------------------------------------------

	void CNPCController::deactivate()
	{
		IComponent::deactivate();
	} // deactivate

	//---------------------------------------------------------

	bool CNPCController::accept(CMessage *message)
	{
		return ((!_death && (message->getType() == Message::CONTROL 
			|| message->getType() == Message::SPECIAL_ACTION 
			)) 
			|| message->getType() == Message::ANIMATION_FINISHED);

	} // accept

	//---------------------------------------------------------

	void CNPCController::process(CMessage *message)
	{
		switch(message->getType())
		{
		case Message::CONTROL:
			{
				if (_death)  return;
				CControlMessage *controlMessage = static_cast<CControlMessage *>(message);

				switch (controlMessage->_avatarAction)
				{
				case TAvatarActions::WALK:
					walk(controlMessage->_vector);
					break;
				case TAvatarActions::WALK_BACK:
					walkBack();
					break;
				case TAvatarActions::STOP_WALK:
					stopWalk();
					break;
				case TAvatarActions::STRAFE_LEFT:
					strafeLeft();
					break;
				case TAvatarActions::STRAFE_RIGHT:
					strafeRight();
					break;
				case TAvatarActions::STOP_STRAFE:
					stopStrafe();
					break;
				case TAvatarActions::TURN:
					turn(controlMessage->_yaw,controlMessage->_pitch);
					break;
				case TAvatarActions::JUMP:
					jump();
					break;
				case TAvatarActions::CROUCH:
					crouch();
					break;
				case TAvatarActions::STOP_CROUCH:
					stopCrouch();
					break;
				case TAvatarActions::STOP_ALL:
					stopAllMovement();
					break;
				case TAvatarActions::FIRE:
					fire(controlMessage->_vector);
					break;
				case TAvatarActions::STOP_FIRE:
					stopFire();
					break;
				case TAvatarActions::CONTEXTUAL_ACTION:
					contextualAction();
					break;
				case TAvatarActions::RUN:
					run();
					break;
				case TAvatarActions::STOP_RUN:
					stopRun();
					break;
				case TAvatarActions::ZOOM_IN:
					zoom(true);
					break;
				case TAvatarActions::ZOOM_OUT:
					zoom(false);
					break;
				case TAvatarActions::DEATH:
					death();
					break;
				default:
					break;
				}	
				break;
			}

		case Message::RESET:
			stopAllMovement();
			break;

		case Message::SPECIAL_ACTION:
			{
				TSpecialActions specialAction = static_cast<CSpecialActionMessage *>(message)->_specialAction;
				bool enter = static_cast<CSpecialActionMessage *>(message)->_enter;
				std::string otherInfo = static_cast<CSpecialActionMessage *>(message)->_additionalInfo;
				CEntity *onTriggerEntity = static_cast<CSpecialActionMessage *>(message)->_entity;
				onTriggerEnter(specialAction, enter, otherInfo, onTriggerEntity);
				break;
			}
		case Message::ANIMATION_FINISHED:

			//onAnimationFinished(static_cast<CAnimationFinishedMessage *>(message)->_animationName);
			break;
		}

	} // process

	//---------------------------------------------------------

	void CNPCController::death()
	{
		if (!_death)
		{
			_walking = _walkingBack = _strafingLeft = _strafingRight = 
				_shooting = _crouching = false;
			_death = true;

			/*CAnimationMessage * message2 = new CAnimationMessage();
			message2->setType(Message::STOP_ANIMATION);
			_entity->emitMessage(message2, this);*/
			/*CAnimationMessage * message = new CAnimationMessage();
			message->setType(Message::SET_ANIMATION);
			message->_animationName = Animations::DEATH;
			message->_bool = false;
			_entity->emitMessage(message, this);*/
			if (_entity->getComponent("CSteeringMovement"))
				_entity->getComponent("CSteeringMovement")->deactivate();
			/*_entity->deactivateAllComponents(this);
			_entity->getComponent("CAnimatedGraphics")->activate();*/
		}
	}

	//---------------------------------------------------------

	void CNPCController::zoom(bool active)
	{
		/*if (_hasWeapon)
		{

		if (!_shooting) 
		{
		if (_walking) walk();
		else if (_walkingBack) walkBack();
		else
		{
		CAnimationMessage * message = new CAnimationMessage();
		message->setType(Message::SET_ANIMATION);
		message->_animationName = Animations::IDLE_WEAPON;
		_entity->emitMessage(message);
		}
		}

		_shootingMode = active ? _shootingMode = TCameraType::AIMING :
		TCameraType::WITH_WEAPON;
		if (_hasWeapon && _entity->isPlayer())
		{
		CCameraMessage *msg = new CCameraMessage((TCameraType)_shootingMode);
		_entity->getMap()->getEntityByType("Camera")->emitMessage(msg);
		}

		}*/
	} // zoom

	//---------------------------------------------------------

	//void CNPCController::onAnimationFinished(TAnimations animationType)
	//{
	//	switch(animationType)
	//	{
	//	case TAnimations::GET_OBJECT:
	//		{
	//			if (_onTriggerEntity)
	//			{
	//				// "Bip01 R Hand" ->habr�a que pasarlo por datos 
	//				// o si no nombrar los huesos de los personajes igual
	//				Graphics::CServer::getSingletonPtr()->getEntity(_entity->getName())->
	//					attachObjectToBone("Bip01 R Hand", _objectName);
	//				_hasWeapon = true;
	//				// Borramos el trigger que habilitaba la captura del objeto
	//				Logic::CEntityFactory::getSingletonPtr()->deferredDeleteEntity(_onTriggerEntity);

	//				// Desactivamos la opci�n de coger objetos.
	//				_getObjectEnabled = false;
	//				_objectName = "";

	//				// Mensaje para cambiar la camara a modo weapon
	//				if (_entity->isPlayer())
	//				{
	//					CCameraMessage *msg = new CCameraMessage(TCameraType::WITH_WEAPON);
	//					_entity->getMap()->getEntityByType("Camera")->emitMessage(msg);
	//				}
	//				CAnimationMessage * message = new CAnimationMessage();
	//				message->setType(Message::SET_ANIMATION);
	//				message->_animationName = Animations::IDLE_WEAPON;
	//				message->_bool = true;
	//				_entity->emitMessage(message,this);

	//				if (_entity->isPlayer())
	//					Logic::CPlayerManager::getSingletonPtr()->setArmado(true);
	//			}
	//			break;
	//		}
	//		//TODO:: Si se agarra una palanca o interruptor
	//		// case TAnimations:: ...
	//	case TAnimations::DEATH:

	//		if (_entity->isPlayer())
	//			Application::CBaseApplication::getSingletonPtr()->setState("menu");
	//		Logic::CEntityFactory::getSingletonPtr()->deferredDeleteEntity(_entity);

	//	}

	//} // onAnimationFinished

	//---------------------------------------------------------

	void CNPCController::onTriggerEnter(TSpecialActions specialAction, bool enter, 
		std::string otherInfo, CEntity *ent)
	{
		// Caso en el que la acci�n es coger un objeto
		if (enter && (specialAction == TSpecialActions::GET_OBJECT))
		{
			//GetObject
			_getObjectEnabled = true;
			_objectName = otherInfo;
			_onTriggerEntity = ent;
		}
		else if(!enter && (specialAction == TSpecialActions::GET_OBJECT))
		{
			_getObjectEnabled = false;
			_objectName = "";
			_onTriggerEntity = NULL;
		}

		// TODO::Otros casos
	} // onTriggerEnter

	//---------------------------------------------------------

	void CNPCController::contextualAction()
	{
		if (_getObjectEnabled)
		{
			/*CAnimationMessage * message2 = new CAnimationMessage();
			message2->setType(Message::SET_ANIMATION);
			message2->_animationName = Animations::GET_OBJECT;
			message2->_bool = false;
			_entity->emitMessage(message2,this);*/

		}

	} // getObject

	//---------------------------------------------------------

	void CNPCController::jump()
	{
		// Enviamos mensaje de salto
		CJumpMessage * message = new CJumpMessage();
		message->_direction = Vector3::UNIT_Y;
		message->_height = _jumpHeight;
		message->_time = _jumpTime;
		_entity->emitMessage(message,this);

		// Ponemos animacion
		/*CAnimationMessage * message2 = new CAnimationMessage();
		message2->setType(Message::SET_ANIMATION);
		if (_hasWeapon)
		message2->_animationName = Animations::JUMP;
		else
		message2->_animationName = Animations::JUMP;
		message2->_bool = false;
		_entity->emitMessage(message2,this);*/
	}

	//---------------------------------------------------------

	void CNPCController::fire(const Vector3 &movement)
	{
		if (_hasWeapon)
		{
			_shooting = true;

			// Animiacion de disparo.
			/*CAnimationMessage * message2 = new CAnimationMessage();
			message2->setType(Message::SET_ANIMATION);
			message2->_animationName = Animations::FIRE;
			message2->_bool = true;
			_entity->emitMessage(message2,this);*/
			// Enviamos mensaje de disparo
			CFireMessage *message = new CFireMessage();
			message->_cameraType = (TCameraType)_shootingMode;
			message->_direction = new Vector3(movement);
			_entity->emitMessage(message);
			// Mensaje de percepcion (emite sonido)
			CActivateSignalMessage *msg = new CActivateSignalMessage("fire", true);
			_entity->emitMessage(msg,this);
		}

	}

	//---------------------------------------------------------

	void CNPCController::stopFire()
	{
		if (_hasWeapon)
		{
			_shooting = false;
			// Parar animiacion de disparo.
			/*	CAnimationMessage * message2 = new CAnimationMessage();
			message2->setType(Message::SET_ANIMATION);
			message2->_animationName = Animations::IDLE_WEAPON;
			message2->_bool = true;
			_entity->emitMessage(message2,this);*/
			// Mensaje de percepcion (emite sonido)
			CActivateSignalMessage *msg = new CActivateSignalMessage("fire", false);
			_entity->emitMessage(msg,this);
		}
	}

	//--------------------------------------------------------

	void CNPCController::crouch()
	{
		_crouching = true;

		//// Cambiamos la animaci�n
		//CAnimationMessage * message = new CAnimationMessage();
		//message->setType(Message::SET_ANIMATION);
		//if (_hasWeapon)
		//	message->_animationName = Animations::CROUCH_WEAPON;
		//else
		//	message->_animationName = Animations::CROUCH;
		//message->_bool = true;
		//_entity->emitMessage(message,this);
	}

	//--------------------------------------------------------

	void CNPCController::stopCrouch()
	{
		_crouching = false;

		// Cambiamos la animaci�n 
		/*CAnimationMessage * message = new CAnimationMessage();
		message->setType(Message::SET_ANIMATION);
		if (_hasWeapon)
		message->_animationName = Animations::IDLE_WEAPON;
		else
		message->_animationName = Animations::IDLE;
		message->_bool = true;*/
		//_entity->emitMessage(message,this);
	}

	//---------------------------------------------------------

	void CNPCController::turn(float amountX, float amountY) 
	{
		_entity->yaw(amountX);
		_entity->pitch(amountY);

	} // turn

	//---------------------------------------------------------

	void CNPCController::walk(const Vector3 &movement) 
	{

		// Enviamos mensaje de movimiento
		CAvatarWalkMessage* message = new CAvatarWalkMessage();
		message->_movement = movement;
		_entity->emitMessage(message, this);

		// Comparamos la direcci�n de la entidad con el vector de movimiento para saber si
		// el NPC se mueve de lado o no.
		if (Math::getDirection(_entity->getYaw() + Math::PI/2).directionEquals(movement, 
			Ogre::Radian()))
		{
			strafeLeft();
		}
		else if (Math::getDirection(_entity->getYaw() - Math::PI/2).directionEquals(movement, 
			Ogre::Radian()))
		{
			strafeRight();
		}
		else if (!Math::getDirection(_entity->getYaw()).directionEquals(movement, 
			Ogre::Radian(Math::PI / 2)))
		{
			walkBack();
		}
		else
		{
			//if (!_walking){
			_walking = true;
			_walkingBack = _strafingLeft = _strafingRight = false;
			/*if (_shootingMode != TCameraType::AIMING && !_shooting)
			{*/
			// Cambiamos la animaci�n
			/*CAnimationMessage * message = new CAnimationMessage();
			message->setType(Message::SET_ANIMATION);
			if (_hasWeapon)
			message->_animationName = Animations::WALK_WEAPON;
			else
			message->_animationName = Animations::WALK;
			message->_bool = true;*/
			_entity->emitMessage(message,this);
			//}
			// Mensaje de percepcion (emite sonido)
			//CAddSignalMessage *msg = new CAddSignalMessage("walk", AI::PERCEPTION_HEAR, 5.0f, true);
			CActivateSignalMessage *msg = new CActivateSignalMessage("walk", true);
			_entity->emitMessage(msg, this);
			//}
		}


	} // walk

	//---------------------------------------------------------

	void CNPCController::walkBack() 
	{
		if (!_walkingBack)
		{
			_walkingBack = true;
			_walking = _strafingLeft = _strafingRight = false;

			/*if (_shootingMode != TCameraType::AIMING && !_shooting)
			{*/
			// Cambiamos la animaci�n
			/*CAnimationMessage * message = new CAnimationMessage();
			message->setType(Message::SET_ANIMATION);
			if (_hasWeapon)
			message->_animationName = Animations::WALK_BACK_WEAPON;
			else
			message->_animationName = Animations::WALK_BACK;
			message->_bool = true;
			_entity->emitMessage(message,this);*/
			//}
			// Mensaje de percepcion (emite sonido)
			//CAddSignalMessage *msg = new CAddSignalMessage("walk", AI::PERCEPTION_HEAR, 5.0f, true);
			CActivateSignalMessage *msg = new CActivateSignalMessage("walk", true);
			_entity->emitMessage(msg, this);
		}
	} // walkBack

	//---------------------------------------------------------

	void CNPCController::stopWalk() 
	{
		_walking = _walkingBack = false;

		// Cambiamos la animaci�n si no seguimos desplaz�ndonos
		// lateralmente
		if(!(_strafingLeft || _strafingRight))
		{
			/*CAnimationMessage * message = new CAnimationMessage();
			message->setType(Message::SET_ANIMATION);
			if (_hasWeapon)
			message->_animationName = Animations::IDLE_WEAPON;
			else
			message->_animationName = Animations::IDLE;
			message->_bool = true;
			_entity->emitMessage(message,this);*/
		}

		//CRemoveSignalMessage *msg = new CRemoveSignalMessage("walk");
		CActivateSignalMessage *msg = new CActivateSignalMessage("walk", false);
		_entity->emitMessage(msg, this);

	} // stopWalk

	//---------------------------------------------------------

	void CNPCController::strafeLeft() 
	{
		_strafingLeft = true;

		if (_shootingMode != TCameraType::AIMING && !_shooting)
		{
			// Cambiamos la animaci�n
			/*CAnimationMessage * message = new CAnimationMessage();
			message->setType(Message::SET_ANIMATION);

			message->_animationName = Animations::STRAFE_LEFT;
			message->_bool = true;
			_entity->emitMessage(message,this);*/
		}

		//CAddSignalMessage *msg = new CAddSignalMessage("walk", AI::PERCEPTION_HEAR, 5.0f, true);
		CActivateSignalMessage *msg = new CActivateSignalMessage("walk", true);
		_entity->emitMessage(msg, this);

	} // walk

	//---------------------------------------------------------

	void CNPCController::strafeRight() 
	{
		_strafingRight = true;

		if (_shootingMode != TCameraType::AIMING && !_shooting)
		{
			// Cambiamos la animaci�n
			/*CAnimationMessage * message = new CAnimationMessage();
			message->setType(Message::SET_ANIMATION);
			message->_animationName = Animations::STRAFE_RIGHT;
			message->_bool = true;
			_entity->emitMessage(message,this);*/
		}

		//CAddSignalMessage *msg = new CAddSignalMessage("walk", AI::PERCEPTION_HEAR, 5.0f, true);
		CActivateSignalMessage *msg = new CActivateSignalMessage("walk", true);
		_entity->emitMessage(msg, this);

	} // walkBack

	//---------------------------------------------------------

	void CNPCController::stopStrafe() 
	{
		_strafingLeft = _strafingRight = false;
		if (_shootingMode != TCameraType::AIMING && !_shooting)
		{
			// Cambiamos la animaci�n si no seguimos andando
			if(!(_walking || _walkingBack))
			{
				/*CAnimationMessage * message = new CAnimationMessage();
				message->setType(Message::SET_ANIMATION);
				if (_hasWeapon)
				message->_animationName = Animations::IDLE_WEAPON;
				else
				message->_animationName = Animations::IDLE;
				message->_bool = true;
				_entity->emitMessage(message,this);*/
			}
			else
			{
				/*CAnimationMessage * message = new CAnimationMessage();
				message->setType(Message::SET_ANIMATION);
				if(_walking)
				{
				message->_animationName = Animations::WALK;
				}
				else if(_walkingBack)
				{
				message->_animationName = Animations::WALK_BACK;
				}
				_entity->emitMessage(message,this);*/
			}
		}
		//CRemoveSignalMessage *msg = new CRemoveSignalMessage("walk");
		CActivateSignalMessage *msg = new CActivateSignalMessage("walk", false);
		_entity->emitMessage(msg, this);
	} // stopWalk

	void CNPCController::stopAllMovement()
	{
		_walking = _walkingBack = _strafingLeft = _strafingRight = _shooting = false;
		/*CAnimationMessage * message = new CAnimationMessage();
		message->setType(Message::SET_ANIMATION);
		if (_hasWeapon)
		message->_animationName = Animations::IDLE_WEAPON;
		else
		message->_animationName = Animations::IDLE;
		message->_bool = true;
		_entity->emitMessage(message,this);*/

		//CRemoveSignalMessage *msg = new CRemoveSignalMessage("walk");
		CActivateSignalMessage *msg = new CActivateSignalMessage("walk", false);
		_entity->emitMessage(msg, this);
		//CRemoveSignalMessage *msg2 = new CRemoveSignalMessage("run");
		CActivateSignalMessage *msg2 = new CActivateSignalMessage("run", false);
		_entity->emitMessage(msg2, this);
	}

	//---------------------------------------------------------

	void CNPCController::run()
	{
		if (_shootingMode != TCameraType::AIMING && !_shooting)
		{
			if(_walking)
			{
				_running = true;
				// Mensaje de animacion
				/*CAnimationMessage * message = new CAnimationMessage();
				message->setType(Message::SET_ANIMATION);
				message->_animationName = Animations::RUN;
				message->_bool = true;
				_entity->emitMessage(message,this);*/
				// Mensaje de percepcion (emite sonido)
				/*CRemoveSignalMessage *msg = new CRemoveSignalMessage("walk");
				_entity->emitMessage(msg, this);
				CAddSignalMessage *msg2 = new CAddSignalMessage("run", AI::PERCEPTION_HEAR, 10.0f, true);*/
				CActivateSignalMessage *msg2 = new CActivateSignalMessage("run", true);
				_entity->emitMessage(msg2,this);

			}
		}
	}

	//---------------------------------------------------------

	void CNPCController::stopRun()
	{
		_running = false;

		if (_shootingMode != TCameraType::AIMING && !_shooting)
		{
			if(_walking)
			{
				/*CAnimationMessage * message = new CAnimationMessage();
				message->setType(Message::SET_ANIMATION);
				message->_animationName = Animations::WALK;
				message->_bool = true;
				_entity->emitMessage(message,this);*/
			}
			else
			{
				stopWalk();
			}
		}

		//CRemoveSignalMessage *msg = new CRemoveSignalMessage("run");
		CActivateSignalMessage *msg = new CActivateSignalMessage("run", false);
		_entity->emitMessage(msg, this);
	}

	//---------------------------------------------------------
	//int count = 0;
	void CNPCController::tick(unsigned int msecs)
	{
		IComponent::tick(msecs);

		// Disparo en r�faga
		/*_fireRateAux += msecs;
		if (_shooting && (_fireRateAux >= _fireRate))
		{
		CFireMessage *message = new CFireMessage();
		message->_cameraType = (TCameraType)_shootingMode;
		_entity->emitMessage(message);
		_fireRateAux = 0;
		}*/
	}

} // namespace Logic

