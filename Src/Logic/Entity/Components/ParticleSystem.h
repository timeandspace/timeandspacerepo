/**
@file ParticleSystem.h

Implementacion del componente que controla el comportamiento de un sistema de 
part�culas

@author Alejandro P�rez Alonso
@date Mayo, 2014
*/

#ifndef __Logic_Particle_System_H
#define __Logic_Particle_System_H

#include "Logic/Entity/Component.h"
#include "Graphics/ParticleListener.h"

namespace Graphics
{
	class CScene;
	class CParticleSystem;
}
//declaraci�n de la clase
namespace Logic 
{

	class CParticleSystem : public IComponent, public Graphics::IParticleSystemListener
	{
		DEC_FACTORY(CParticleSystem);
	public:

		/**
		Constructor por defecto; inicializa los atributos a su valor por 
		defecto.
		*/
		CParticleSystem();

		/**
		Destructor (virtual); Quita de la escena y destruye la entidad gr�fica.
		*/
		virtual ~CParticleSystem();
		
		/**
		Inicializaci�n del componente, utilizando la informaci�n extra�da de
		la entidad le�da del mapa (Maps::CEntity). Toma del mapa el atributo
		model con el modelo que se deber� cargar e invoca al m�todo virtual
		createGraphicsEntity() para generar la entidad gr�fica.

		@param entity Entidad a la que pertenece el componente.
		@param map Mapa L�gico en el que se registrar� el objeto.
		@param entityInfo Informaci�n de construcci�n del objeto le�do del
			fichero de disco.
		@return Cierto si la inicializaci�n ha sido satisfactoria.
		*/
		virtual bool spawn(CEntity* entity, CMap *map, const Map::CEntity *entityInfo);


		virtual void tick(unsigned int msecs);


		virtual bool accept(CMessage *message);


		virtual bool activate();
		

		virtual void process(CMessage *message);

		/*** Graphics::IParticleSystemListener ***/
		virtual void onStart();

		virtual void onStop();

		void start();

		void setCallBack(Callback callback);


	protected:
		
		/**
		Entidad gr�fica.
		*/
		Graphics::CParticleSystem *_particleSystem;


		Callback _callback;

		/**
		Escena gr�fica donde se encontrar�n las representaciones gr�ficas de
		las entidades. La guardamos para la destrucci�n de la entidad gr�fica.
		*/
		Graphics::CScene* _scene;

		// Duracion del efecto de part�culas
		float _duration;

		bool _oneShot;
		bool _autoStart;

		/**
		Flag utilizado para saber si un efecto est� adjunto a otra entidad. Se utiliza porque en el spawn aun no se 
		han creado todas las entidades.
		*/
		bool _attached;

		std::string _parentName;
		std::string _boneName;

		float _yOffset;

	}; // class CGraphics

	REG_FACTORY(CParticleSystem);

} // namespace Logic

#endif // __Logic_Particle_System_H
