/**
@file CutSceneTrigger.h

Contiene la declaraci�n del componente que activa una cut scene

@see Logic::IComponent

@author Alejandro P�rez Alonso
@date Mayo, 2014
*/
#ifndef __Logic_CutScene_Trigger_H
#define __Logic_CutScene_Trigger_H

#include "Logic/Entity/Component.h"

//declaraci�n de la clase
namespace Logic 
{

	class CCutSceneTrigger : public IComponent
	{
		DEC_FACTORY(CCutSceneTrigger);
	public:

		/**
		Constructor por defecto.
		*/
		CCutSceneTrigger() : IComponent() {}

		/**
		Inicializaci�n del componente usando la descripci�n de la entidad que hay en 
		el fichero de mapa.
		*/
		virtual bool spawn(CEntity* entity, CMap *map, const Map::CEntity *entityInfo);

		/**
		M�todo que se invoca para activar el componente.
		*/
		virtual bool activate();

		/**
		M�todo que se invoca al desactivar el componente.
		*/
		virtual void deactivate();

		/**
		Este componente s�lo acepta mensaje de tipos TOUCHED.
		*/
		virtual bool accept(CMessage *message);

		/**
		Al recibir mensaje TOUCHED se realizar� un cambio de nivel.
		*/
		virtual void process(CMessage *message);

	protected:
		/**
		Indica el nivel que se cargar� cuando el player entre en contacto con �l.
		*/
		int _cutSceneNumber;

	}; // class CCutSceneTrigger

	REG_FACTORY(CCutSceneTrigger);

} // namespace Logic

#endif // __Logic_CutScene_Trigger_H
