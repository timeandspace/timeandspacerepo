/**
@file PhysicEntity.h

Contiene la implementaci�n del componente encargado de representar entidades f�sicas simples,
que son aquellas representadas mediante un �nico actor de PhysX. Este componente no sirve
para representar character controllers.

@see Logic::CPhysicEntity
@see Logic::IComponent
@see Logic::CPhysicController

@author Antonio S�nchez Ruiz-Granados
@date Noviembre, 2012
*/

#include "PhysicEntity.h"
#include "Logic/Entity/Entity.h"
#include "Map/MapEntity.h"
#include "Physics/Server.h"
#include "Physics/Conversions.h"

#include "Logic/Entity/Components/PlatformController.h"

#define _USE_MATH_DEFINES
#include <math.h>

#include <PxPhysicsAPI.h>

using namespace Logic;
using namespace Physics;
using namespace physx; 

IMP_FACTORY(CPhysicEntity);

//---------------------------------------------------------

CPhysicEntity::CPhysicEntity() : IPhysics(), _actor(NULL), _movement(0,0,0)
{
	_server = CServer::getSingletonPtr();
}

//---------------------------------------------------------

CPhysicEntity::~CPhysicEntity() 
{
	if (_actor) {
		_server->destroyActor(_actor);
		_actor = NULL;
	}

	_server = NULL;
} 

//---------------------------------------------------------

bool CPhysicEntity::spawn(CEntity *entity, CMap *map, const Map::CEntity *entityInfo) 
{
	// Invocar al m�todo de la clase padre
	if(!IComponent::spawn(entity,map,entityInfo))
		return false;

	// Crear el objeto f�sico asociado al componente
	_actor = createActor(entityInfo);

	PxRigidDynamic *dinActor = _actor->isRigidDynamic();
	if(dinActor && _server->isKinematic(dinActor))
	{
		PxTransform trans = dinActor->getGlobalPose();
		PxVec3 rotation (0.0,90.0,0);
		trans.rotate(rotation);
		dinActor->setGlobalPose(trans);
		dinActor->setKinematicTarget(trans);
	}

	return true;
} 

//---------------------------------------------------------

bool CPhysicEntity::accept(CMessage *message)
{
	return message->getType() == Message::KINEMATIC_MOVE
		|| message->getType() == Message::ADD_FORCE;
}

//---------------------------------------------------------

void CPhysicEntity::process(CMessage *message)
{
	switch(message->getType()) {
		// TODO: procesar mensaje de tipo KINEMATIC_MOVE
		// Acumular el vector de desplazamiento recibido en el mensaje en el 
		// atributo privado _movement para posteriormente utilizarlo en el
		// m�todo tick.
	case Message::KINEMATIC_MOVE:
		_movement = static_cast<CKinematicMoveMessage *>(message)->_displ;
		break;
	case Message::ADD_FORCE:
		// Aplicar una fuerza a entidades de tipo dinamico.

		PxRigidDynamic *dinActor = _actor->isRigidDynamic();
		if (!dinActor) 
			return;
		Vector3 dir = static_cast<CAddForceMessage *>(message)->_direction;
		int force = static_cast<CAddForceMessage *>(message)->_force;
		dinActor->addForce(Physics::Vector3ToPxVec3(dir) * force, PxForceMode::eFORCE);
		break;
	}
}

//---------------------------------------------------------

void CPhysicEntity::tick(unsigned int msecs) 
{
	// Invocar al m�todo de la clase padre (IMPORTANTE)
	IComponent::tick(msecs);

	// Si es una entidad est�tica no hacemos nada
	PxRigidDynamic *dinActor = _actor->isRigidDynamic();
	if (!dinActor) 
	{
		// �APA:: para que los triggers estaticos(sticky floor) se muevan al 
		// recibir un mensaje KINEMATIC_MOVE de la clase padre.
		// Se queja el PhysX por consola constantemente.
		if (_movement != Vector3::ZERO)
		{
			PxTransform t = _actor->getGlobalPose();
			t.p += Vector3ToPxVec3(_movement);
			_actor->setGlobalPose(t);
		}
		return;
	}

	// TODO: actualizar la posici�n y orientaci�n de la entidad l�gica
	// usando la informaci�n proporcionada por el motor de f�sica
	Matrix4 matrix = _server->getActorTransform(_actor);
	_entity->setTransform(matrix);

	// TODO: mover objetos cinem�ticos de acuerdo a la l�gica
	// 1. Comprobar si el componente representa a un objeto cinem�tico usando
	//    la interfaz del servidor de f�sica
	// 2. Mover el objeto usando la interfaz del servidor de f�sica
	// 3. Poner el atributo _movement a cero para el siguiente tick
	if (_server->isKinematic(dinActor))
	{
		_server->moveKinematicActor(dinActor, _movement);
		_movement = Vector3::ZERO;
	}
}

//---------------------------------------------------------

PxRigidActor* CPhysicEntity::createActor(const Map::CEntity *entityInfo)
{
	// Leer el tipo de entidad
	assert(entityInfo->hasAttribute("physic_entity"));
	const std::string physicEntity = entityInfo->getStringAttribute("physic_entity");
	assert((physicEntity == "rigid") || (physicEntity == "plane") || (physicEntity == "fromFile"));

	// Crear el tipo de entidad adecuada
	if (physicEntity == "plane") 
		return createPlane(entityInfo);

	if (physicEntity == "rigid") 
		return createRigid(entityInfo);

	if (physicEntity == "fromFile")
		return createFromFile(entityInfo);

	return NULL;
}

//---------------------------------------------------------

PxRigidStatic* CPhysicEntity::createPlane(const Map::CEntity *entityInfo)
{
	// La posici�n de la entidad es un punto del plano
	const Vector3 point = _entity->getPosition();

	// TODO: Leer el vector normal al plano del fichero de mapa
	const Vector3 normal = entityInfo->getVector3Attribute("physic_normal");;

	// TODO: leer atributo de grupo de colisi�n (por defecto 0)
	int group = entityInfo->getIntAttribute("physic_group");

	// Filtro de colisi�n
	unsigned int filterData = FilterGroup::EVERYTHING;
	if (entityInfo->hasAttribute("filterData"))
	{
		filterData = entityInfo->getIntAttribute("filterData");
		if (filterData == -1)
			filterData = FilterGroup::EVERYTHING;
		else
			filterData = ~filterData;
	}
	// TODO: Usar la interfaz del servidor de f�sica para crear un plano est�tico
	// Pasar como componente this
	return _server->createPlane(point, normal, group, filterData, this);
}

//---------------------------------------------------------

PxRigidActor* CPhysicEntity::createRigid(const Map::CEntity *entityInfo)
{
	// Leer la posici�n de la entidad
	const Vector3 position = _entity->getPosition();

	// Leer la orintacion de la entidad
	float orientation;

	if(entityInfo->hasAttribute("orientation"))
		orientation =  entityInfo->getFloatAttribute("orientation");
	else 
		orientation = 0;

	// Leer el tipo de entidad: est�ticos, din�mico o cinem�tico
	assert(entityInfo->hasAttribute("physic_type"));
	const std::string physicType = entityInfo->getStringAttribute("physic_type");
	assert((physicType == "static") || (physicType == "dynamic") || (physicType == "kinematic"));

	// TODO: Leer la forma (shape) 
	const std::string physicShape = entityInfo->getStringAttribute("physic_shape");

	// TODO: Leer atributo trigger del mapa (por defecto no es un trigger)
	bool trigger = false;
	if (entityInfo->hasAttribute("physic_trigger"))
		trigger = entityInfo->getBoolAttribute("physic_trigger");

	// TODO: leer atributo de grupo de colisi�n (por defecto 0)
	int group = entityInfo->getIntAttribute("physic_group");

	if (physicType == "static") {
		if (physicShape == "box") {
			// TODO: leer las dimensiones de la caja
			const Vector3 physicDimensions = entityInfo->getVector3Attribute("physic_dimensions");

			// Filtro de colisi�n
			unsigned int filterData = FilterGroup::EVERYTHING;
			if (entityInfo->hasAttribute("filterData"))
			{
				filterData = entityInfo->getIntAttribute("filterData");
				if (filterData == -1)
					filterData = FilterGroup::EVERYTHING;
				else
					filterData = ~filterData;
			}

			// Crear una caja est�tica
			return _server->createStaticBox(position, physicDimensions, trigger, 
				group, filterData, this);
		}

	} else {
		// TODO: Leer la masa (por defecto 0)
		float mass = entityInfo->getFloatAttribute("physic_mass");

		// Leer si se trata de un actor cinem�tico
		bool kinematic = (physicType == "kinematic");

		if (physicShape == "box") {
			// Leer las dimensiones de la caja
			assert(entityInfo->hasAttribute("physic_dimensions"));
			const Vector3 physicDimensions = entityInfo->getVector3Attribute("physic_dimensions");

			// Filtro de colisi�n
			unsigned int filterData = FilterGroup::EVERYTHING;
			if (entityInfo->hasAttribute("filterData"))
			{
				filterData = entityInfo->getIntAttribute("filterData");
				if (filterData == -1)
					filterData = FilterGroup::EVERYTHING;
				else
					filterData = ~filterData;
			}

			// TODO: Crear y devolver una caja din�mica usando el servidor de f�sica
			// Pasar como componente this
			return _server->createDynamicBox(position, orientation, physicDimensions, mass, kinematic, 
				trigger, group, filterData, this);
		}
	}

	return NULL;
}

//---------------------------------------------------------

PxRigidActor* CPhysicEntity::createFromFile(const Map::CEntity *entityInfo)
{
	// Leer la posici�n de la entidad
	const Vector3 position = _entity->getPosition();

	// Leer la orintacion de la entidad
	float orientation;

	if(entityInfo->hasAttribute("orientation"))
		orientation =  entityInfo->getFloatAttribute("orientation");
	else 
		orientation = 0;

	// Leer la ruta del fichero RepX
	assert(entityInfo->hasAttribute("physic_file"));
	const std::string file = entityInfo->getStringAttribute("physic_file");

	// Leer el grupo de colisi�n (por defecto 0)
	int group = 0;
	if (entityInfo->hasAttribute("physic_group"))
		group = entityInfo->getIntAttribute("physic_group");

	// Filtro de colisi�n
	unsigned int filterData = FilterGroup::EVERYTHING;
	if (entityInfo->hasAttribute("filterData"))
	{
		filterData = entityInfo->getIntAttribute("filterData");
		if (filterData == -1)
			filterData = FilterGroup::EVERYTHING;
		else
			filterData = ~filterData;
	}

	// TODO: crear el actor a partir del fichero RepX usando el servidor de f�sica
	return _server->createFromFile(file, position, orientation, group, filterData, this);
}

//---------------------------------------------------------

void CPhysicEntity::onTrigger(IPhysics *otherComponent, bool enter)
{
	// TODO: notificar que entramos o salimos de un trigger
	// Construir un mensaje de tipo TOUCHED si entramos en el trigger 
	// o UNTOUCHED si salimos del trigger y enviarlo al resto de 
	// componentes de la entidad.
	if (enter)
	{
		CTouchedMessage* m = new CTouchedMessage();
		if (otherComponent)
			m->_entity = otherComponent->getEntity();
		else 
			m->_entity = NULL;
		_entity->emitMessage(m);
	}	
	else
	{
		CUnTouchedMessage* m = new CUnTouchedMessage();
		if (otherComponent)
			m->_entity = otherComponent->getEntity();
		else 
			m->_entity = NULL;
		_entity->emitMessage(m);
	}


}

//---------------------------------------------------------

void CPhysicEntity::addForce(const Vector3 &forceDir, float strenght)
{
	if(_actor->isRigidDynamic() && !_server->isKinematic((PxRigidDynamic*)_actor))
	{
		((PxRigidDynamic*)_actor)->addForce(Vector3ToPxVec3(forceDir * strenght));
	}
}

//---------------------------------------------------------

void CPhysicEntity::onContact(IPhysics *otherComponent)
{
	CEntity *ent = 0;
	if (otherComponent)
	{
		ent = otherComponent->getEntity();
		CJumpMessage *msg = new CJumpMessage();
		// HACK::Asumimos que solo van a aplicar una fuerza las plataformas
		if (_entity->getComponent("CPlatformController"))
		{
			msg->_direction = static_cast<CPlatformController *>(_entity->getComponent("CPlatformController"))
				->getDirection();
		}
		else
			msg->_direction = Math::getDirection(_entity->getOrientation());
		msg->_height = 10;
		msg->_time = 200;
		msg->_jump = false;
		ent->emitMessage(msg);
	}
}