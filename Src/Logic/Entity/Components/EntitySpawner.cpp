/**
@file EntitySpawner.cpp


@see Logic::IComponent

@author �lvaro Bl�zquez Checa
@date Febrero, 2014
*/

#include "EntitySpawner.h"

#include "Logic/Entity/Entity.h"
#include "Map/MapEntity.h"
#include "Physics/Server.h"
#include "Logic/Maps/EntityFactory.h"
#include "Graphics/Server.h"

namespace Logic
{
	IMP_FACTORY(CEntitySpawner);

	//---------------------------------------------------------
	bool CEntitySpawner::spawn(CEntity *entity, CMap *map, const Map::CEntity *entityInfo)
	{
		if (!IComponent::spawn(entity, map, entityInfo))
			return false;

		if (entityInfo->hasAttribute("entityToSpawn")) {
			_entityToSpawn = entityInfo->getStringAttribute("entityToSpawn");
			_initialized = true;
		}

		if(_initialized) {		

			Map::CEntity* mapEntity = CEntityFactory::getSingletonPtr()->getMapEntityByName(_entityToSpawn);

			mapEntity = new Map::CEntity(*mapEntity);

			mapEntity->setPositionAttribute(_entity->getPosition());

			float orientation = Math::fromRadiansToDegrees(_entity->getYaw());

			mapEntity->setYawAttribute(_entity->getYaw());

			/*std::stringstream ss;

			ss << mapEntity->getName() << _id++;
			mapEntity->setName(ss.str());*/

			Logic::CEntity* entity = CEntityFactory::getSingletonPtr()->createEntity(mapEntity,_entity->getMap());
	
			delete mapEntity;
		}



		return true;

	} // spawn

	//---------------------------------------------------------

	bool CEntitySpawner::activate()
	{
		return IComponent::activate();
	} // activate

	//---------------------------------------------------------

	void CEntitySpawner::deactivate()
	{
		IComponent::deactivate();
	} // deactivate

	//---------------------------------------------------------

	bool CEntitySpawner::accept(CMessage *message)
	{
		return false;
	} // accept

	//---------------------------------------------------------

	void CEntitySpawner::process(CMessage *message)
	{

		
	} // process

	//---------------------------------------------------------

	void CEntitySpawner::tick(unsigned int msecs)
	{
		// Invocar al m�todo de la clase padre
		IComponent::tick(msecs);
	} // tick

	//---------------------------------------------------------

} // namespace Logic
