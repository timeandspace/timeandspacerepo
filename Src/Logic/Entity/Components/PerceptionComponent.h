/**
* @file PerceptionComponent.h
*
* Declaraci�n del componente que permite a la entidad
* percibir y/o ser percibida
*
* @author Gonzalo Fl�rez
* @date 12/04/2011
*/

#pragma once

#ifndef __Logic_PerceptionComponent_H
#define __Logic_PerceptionComponent_H

#include "Logic/Entity/Component.h"
#include "AI/PerceptionListener.h"

namespace AI {
	class CPerceptionEntity;
}

namespace Logic {

	/**
	* Este componente se encarga de las tareas relacionadas con la percepci�n.
	* <p>
	* Al cargar las propiedades del mapa, se ocupa de crear una entidad de percepci�n
	* (AI::CPerceptionEntity), que estar� asociada a la entidad l�gica. En esta entidad
	* de percepci�n se definen los sensores que posee la entidad y las se�ales que
	* genera en cada tick.
	* <p>
	* La entidad de percepci�n se registra en el gestor de percepci�n (AI::CPerceptionManager)
	* para poder percibir y ser percibida.
	* <p>
	* Cuando uno de los sensores de la entidad percibe una se�al apropiada, se invoca
	* al m�todo notificationPerceived que, en este caso, env�a un mensaje con los
	* detalles de la percepci�n realizada.
	*
	* @author Gonzalo Fl�rez
	* @date 12/04/2011
	*/
	class CPerceptionComponent: public IComponent, public AI::IPerceptionListener {

		DEC_FACTORY(CPerceptionComponent);

	public:
		/**
		* Constructor
		*/
		CPerceptionComponent(void) 
			: _pEntity(NULL), _updateTransform(false),
			_standHeight(0.0f), _crouchHeight(0.0f),
			_crouched(false)
		{}

		/**
		* Destructor
		*/
		~CPerceptionComponent(void);

		/**
		Inicializaci�n del componente, utilizando la informaci�n extra�da de
		la entidad le�da del mapa (Maps::CEntity).

		En el spawn, se crea una entidad de percepci�n (AI::CPerceptionEntity) y se
		registra en el gestor de percepci�n (AI::CPerceptionManager).

		En este caso utilizaremos los atributos
		� perception_entity_type: tipo de la entidad de percepci�n. Es una manera
		r�pida y sencilla de definir qu� sensores y se�ales tiene la entidad y cu�les
		son sus propiedades, pero no es muy flexible.
		� physic_radius: utilizamos el radio de la c�psula f�sica como radio de la entidad
		de percepci�n.

		@param entity Entidad a la que pertenece el componente.
		@param map Mapa L�gico en el que se registrar� el objeto.
		@param entityInfo Informaci�n de construcci�n del objeto le�do del
		fichero de disco.
		@return Cierto si la inicializaci�n ha sido satisfactoria.
		*/
		virtual bool spawn(CEntity* entity, CMap *map,
			const Map::CEntity *entityInfo);

		/**
		M�todo que activa el componente; invocado cuando se activa
		el mapa donde est� la entidad a la que pertenece el componente.
		<p>
		En este caso, se registra el componente en el gestor de percepci�n (AI::PerceptionManager)

		@return true si todo ha ido correctamente.
		*/
		virtual bool activate();

		/**
		M�todo que desactiva el componente; invocado cuando se
		desactiva el mapa donde est� la entidad a la que pertenece el
		componente. Se invocar� siempre, independientemente de si estamos
		activados o no.
		<p>
		Al desactivar el componente la entidad de percepci�n se "desregistra"
		del gestor de percepci�n.
		*/
		virtual void deactivate();

		/**
		M�todo llamado en cada frame que actualiza el estado del componente.
		<p>
		En cada tick se comprueba si la entidad l�gica ha modificado su matriz de transformaci�n
		y, si es as�, se actualiza la matriz de transformaci�n de la entidad de percepci�n.

		@param msecs Milisegundos transcurridos desde el �ltimo tick.
		*/
		virtual void tick(unsigned int msecs);

		/**
		M�todo virtual que elige que mensajes son aceptados.
		<p>
		Los mensajes que acepta este componente son:
		� SET_TRANSFORM: si se modifica la matriz de transformaci�n de la entidad
		l�gica hay que actualizar tambi�n la de la entidad de percepci�n.
		� ADD_SIGNAL: a�ade una se�al a la entidad de percepci�n. De esta forma
		podemos a�adir se�ales que responden a determinados eventos (por ejemplo, al disparar)

		@param message Mensaje a chequear.
		@return true si el mensaje es aceptado.
		*/
		virtual bool accept(CMessage *message);

		/**
		M�todo virtual que procesa un mensaje.
		<p>
		Los mensajes que puede procesar este componente son:
		� SET_TRANSFORM: si se modifica la matriz de transformaci�n de la entidad
		l�gica hay que actualizar tambi�n la de la entidad de percepci�n.
		� ADD_SIGNAL: a�ade una se�al a la entidad de percepci�n

		@param message Mensaje a procesar.
		*/
		virtual void process(CMessage *message);

		/**
		* M�todo invocado por el gestor de percepci�n cuando recibe una notificaci�n de un
		* sensor de la entidad de percepci�n.
		*
		* En este m�todo se incluir�n las acciones que queremos que el componente realice cuando
		* alguno de los sensores asociados percibe una se�al.
		*
		* @param notification Notificaci�n recibida
		*/
		virtual void notificationPerceived(AI::CNotification* notification);


		float getSightHeight();

	private:
		/**
		* Entidad de percepci�n que se registra en el CPerceptionManager
		*/
		AI::CPerceptionEntity* _pEntity;
		/**
		* Matriz de transformaci�n de la entidad
		*/
		Matrix4 _transform;
		/**
		* Indica si hay que actualizar la matriz de transformaci�n en el pr�ximo tick
		*/
		bool _updateTransform;

		float _standHeight;
		float _crouchHeight;

		bool _crouched;

	}; // class CPerceptionComponent

	REG_FACTORY(CPerceptionComponent)
		;

} // namespace Logic


#endif __Logic_PerceptionComponent_H
