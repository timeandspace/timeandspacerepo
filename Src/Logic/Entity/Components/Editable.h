/**
@file Editable.h

Contiene la declaraci�n del componente que permite que pueda
ser ralentizado/acelerado un objeto.

@author Alejandro P�rez Alonso
@date Marzo, 2014
*/

#ifndef __Logic_Editable_H
#define __Logic_Editable_H

#include "Logic/Entity/Component.h"

namespace Logic
{

	class CEditable : public IComponent
	{
		DEC_FACTORY(CEditable);
	public:

		/**
		*/
		CEditable() : IComponent(), _clock(0), _isSlowingDown(false), _modifier(0) {}

		/**
		*/
		~CEditable() {}

		/**
		*/
		virtual bool spawn(CEntity* entity, CMap *map, const Map::CEntity *entityInfo);

		/**
		*/
		virtual bool activate();

		/**
		*/
		virtual void deactivate();

		/**
		*/
		virtual bool accept(CMessage *message);

		/**
		*/
		virtual void process(CMessage *message);

		/**
		*/
		virtual void tick(unsigned int msecs);

	protected:
		unsigned int _clock;
		bool _isSlowingDown;
		float _modifier;
		int _msecsTick; // sirve para guardar en el spawn el tick inicial

	}; // Class CEditable

	REG_FACTORY(CEditable);

} // namespace Logic

#endif // __Logic_Editable_H