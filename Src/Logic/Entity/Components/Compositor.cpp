/**
@file Compositor.cpp

Declaraci�n del componente que controla el comportamiento de un compositor

@author Alejandro P�rez Alonso
@date Junio, 2014
*/

#include "Compositor.h"

#include "Logic/Entity/Entity.h"
#include "Logic/Maps/Map.h"
#include "Map/MapEntity.h"

#include "Graphics/CompositorManager.h"

namespace Logic
{

	IMP_FACTORY(CCompositor);

	//---------------------------------------------------------

	CCompositor::CCompositor() : IComponent(), _autoStart(false), _compositorName("")
	{
	}

	//---------------------------------------------------------

	CCompositor::~CCompositor()
	{

	}

	//---------------------------------------------------------

	bool CCompositor::spawn(CEntity* entity, CMap *map, const Map::CEntity *entityInfo)
	{
		if(!IComponent::spawn(entity,map,entityInfo))
			return false;

		// Creamos el sistema de part�culas
		_compositorName = entityInfo->getStringAttribute("template");

		// AutoStart
		_autoStart = entityInfo->getBoolAttribute("autoStart");

		return true;
	}

	//---------------------------------------------------------

	bool CCompositor::activate()
	{
		if (_autoStart)
			Graphics::CCompositorManager::getSingletonPtr()->setEnabled(_compositorName, true);

		return IComponent::activate();
	}

	//---------------------------------------------------------

	void CCompositor::deactivate()
	{
		Graphics::CCompositorManager::getSingletonPtr()->setEnabled(_compositorName, false);

		return IComponent::deactivate();
	}

	//---------------------------------------------------------

	bool CCompositor::accept(CMessage *message)
	{
		return false;
	}

	//---------------------------------------------------------

	void CCompositor::process(CMessage *message)
	{

		return;
	}

	//---------------------------------------------------------

	void CCompositor::tick(unsigned int msecs)
	{
		IComponent::tick(msecs);

	}

} // namespace Logic
