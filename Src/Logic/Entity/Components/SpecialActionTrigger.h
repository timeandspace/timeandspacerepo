/**
@file SpecialActionTrigger.h

Contiene la declaraci�n del componente que envia un mensaje a otra
entidad cuando recibe un mensaje TOUCHED. Env�a un string con la 
acci�n special que la entidad afectada se encargar� de procesar o no.

@see Logic::IComponent

@author Alejandro P�rez Alonso
@date Marzo, 2014
*/
#ifndef __Logic_Special_Action_Trigger_H
#define __Logic_Special_Action_Trigger_H

#include "Logic/Entity/Component.h"

//declaraci�n de la clase
namespace Logic 
{
	/**
	Este componente procesa mensajes de tipo TOUCHED o UNTOUCHED (indican que la 
	entidad ha sido tocada o dejada de ser tocada) para enviar un mensaje de tipo 
	SPECIAL_ACTION con la acci�n, un puntero a la entidad y un string con informaci�n
	adicional, como por ejemplo si la acci�n es "getObject", este string ser�a el 
	nombre de la entidad que coger� el player.

	@ingroup logicGroup

	@author Alejandro Perez Alonso
	@date Marzo, 2014
	*/
	class CSpecialActionTrigger : public IComponent
	{
		DEC_FACTORY(CSpecialActionTrigger);
	public:

		/**
		Constructor por defecto.
		*/
		CSpecialActionTrigger() : IComponent(), _count(0), _specialAction(TSpecialActions::NONE) {}

		/**
		Inicializaci�n del componente usando la descripci�n de la entidad que hay en 
		el fichero de mapa.
		*/
		virtual bool spawn(CEntity* entity, CMap *map, const Map::CEntity *entityInfo);

		/**
		Este componente s�lo acepta mensaje de tipos TOUCHED y UNTOUCHED.
		*/
		virtual bool accept(CMessage *message);

		/**
		Al recibir mensaje TOUCHED
		*/
		virtual void process(CMessage *message);

		virtual void tick(unsigned int msecs);

	protected:
		TSpecialActions _specialAction;
		std::string _object;
		int _count;
		/**
		Lista de las entidades que se encuantran dentro del trigger
		*/
		std::list<std::string> _entityList;

	}; // class CSwitchTrigger

	REG_FACTORY(CSpecialActionTrigger);

} // namespace Logic

#endif // __Logic_Sticky_Floor_H
