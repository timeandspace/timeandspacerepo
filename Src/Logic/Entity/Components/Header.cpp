/**

*/

#include "Header.h"

#include "Logic/Entity/Entity.h"
#include "Logic/Maps/Map.h"
#include "Logic/Maps/Entityfactory.h"
#include "Map/MapEntity.h"
#include "Application/BaseApplication.h"

namespace Logic 
{
	IMP_FACTORY(CHeader);

	bool CHeader::spawn(CEntity* entity, CMap *map, const Map::CEntity *entityInfo)
	{
		if(!IComponent::spawn(entity,map,entityInfo))
			return false;

		return true;
	}

	//---------------------------------------------------------

	bool CHeader::accept(CMessage *message)
	{
		return false;
	}

	//---------------------------------------------------------

	void CHeader::process(CMessage *message)
	{
		
	}

	//---------------------------------------------------------

	void CHeader::tick(unsigned int msecs)
	{
		IComponent::tick(msecs);
	}
}
