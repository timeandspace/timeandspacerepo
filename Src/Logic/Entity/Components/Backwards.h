/**
@file Backwards.h

Contiene la declaración del componente que hace que una copia
"retroceda en el tiempo" reproduce la lista de movimientos al
revés.

@see Logic::CSave
@see Logic::ReplayManager
@see Logic::IComponent

@author Alejandro Perez Alonso
@date Febrero, 2014
*/
#ifndef __Logic_Force_Trigger_H
#define __Logic_Force_Trigger_H

#include "Logic/Entity/Component.h"

//declaración de la clase
namespace Logic 
{
	/**

	@date Enero, 2014
	*/
	class CBackwards : public IComponent
	{
		DEC_FACTORY(CBackwards);
	public:

		/**
		Constructor por defecto.
		*/
		CBackwards() : IComponent(), _duration(0), _isPlaying(false), _messageList(NULL) {}

		~CBackwards() 
		{
			if (_messageList && _messageList->size() > 0)
			{
				CMessageList::const_iterator itr = _messageList->begin();
				CMessageList::const_iterator end = _messageList->end();
				for ( ; itr != end; ++itr)
				{
					(*itr)->release();
				}
				_messageList->clear();
			}
			delete _messageList;
		}

		/**
		Inicialización del componente usando la descripción de la entidad que hay en 
		el fichero de mapa.
		*/
		virtual bool spawn(CEntity* entity, CMap *map, const Map::CEntity *entityInfo);

		/**
		*/
		virtual bool accept(CMessage *message);

		/**
		*/
		virtual void process(CMessage *message);

		/**
		*/
		virtual void tick(unsigned int msecs);

		//void setMessageList(CMessageList &list) { _messageList = list; }

		//void setBackwardTime(unsigned int &time) { _duration = time; }

	protected:
		/**
		Funcion que transforma una lista de mensajes, en una lista de mensajes inversa.
		*/
		CMessageList& parserList(CMessageList &list);

		unsigned long _duration;
		unsigned long _clock;
		CMessageList* _messageList;
		bool _isPlaying;

	}; // class CSwitchTrigger

	REG_FACTORY(CBackwards);

} // namespace Logic

#endif // __Logic_SwitchTrigger_H
