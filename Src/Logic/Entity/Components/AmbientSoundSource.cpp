/**
@file SoundSource.cpp

Contine la implementaci�n del componente

@see Logic::IComponent

@author �lvaro Bl�zquez Checa
@date Marzo, 2014

Update:
@author Alejandro P�rez Alonso
@date Abril, 2014
*/

#include "AmbientSoundSource.h"

#include "Logic/Entity/Entity.h"
#include "Map/MapEntity.h"
#include "Sound/Sound.h"
#include "Sound/SoundBank.h"
#include "Sound/Server.h"

namespace Logic
{
	IMP_FACTORY(CAmbientSoundSource);

	//---------------------------------------------------------
	bool CAmbientSoundSource::spawn(CEntity *entity, CMap *map, const Map::CEntity *entityInfo)
	{
		if (!IComponent::spawn(entity, map, entityInfo))
			return false;

		bool loop = false;
		if(entityInfo->hasAttribute("loop"))
		{
			loop = entityInfo->getBoolAttribute("loop");
		}

		if(entityInfo->hasAttribute("sound_name"))
		{
			std::string soundName = entityInfo->getStringAttribute("sound_name");

			_sound = new Sonido::CSound(soundName , loop);
		}

		
		if(entityInfo->hasAttribute("sound_name_studio"))
		{
			std::string soundName = entityInfo->getStringAttribute("sound_name_studio");

			_sound = new Sonido::CSound(soundName);
		}

		if(entityInfo->hasAttribute("volume"))
		{
			float volume = entityInfo->getFloatAttribute("volume");
			_sound->setVolume(volume);
		}
		
		assert(_sound && "Falta el archivo de sonido");

		return true;

	} // spawn

	//---------------------------------------------------------

	bool CAmbientSoundSource::activate()
	{
		_sound->play();
		return IComponent::activate();
		
	} // activate

	//---------------------------------------------------------

	void CAmbientSoundSource::deactivate()
	{
		IComponent::deactivate();
		
		assert(_sound);
		_sound->stop();
	} // deactivate

	//---------------------------------------------------------

	bool CAmbientSoundSource::accept(CMessage *message)
	{
		return message->getType() == Message::PLAY_SOUND ||
			message->getType() == Message::STOP_SOUND;
	} // accept

	//---------------------------------------------------------

	void CAmbientSoundSource::process(CMessage *message)
	{
		if (message->getType() ==  Message::PLAY_SOUND)
		{	
			play();
		}
		else if (message->getType() ==  Message::STOP_SOUND)
		{	
			stop();
		}

	} // process

	//---------------------------------------------------------

	void CAmbientSoundSource::tick(unsigned int msecs)
	{
		IComponent::tick(msecs);

	} // tick

	//---------------------------------------------------------

	void CAmbientSoundSource::play()
	{
		assert(_sound);
		_sound->play();
	}

	//---------------------------------------------------------

	void CAmbientSoundSource::stop()
	{
		assert(_sound);
		_sound->stop();
	}

	//---------------------------------------------------------

	void CAmbientSoundSource::pause(bool paused)
	{
		assert(_sound);
	}

} // namespace Logic
