/**

*/



#include "Logic/Entity/Entity.h"
#include "Logic/Maps/Map.h"
#include "Map/MapEntity.h"
#include "Application/BaseApplication.h"

namespace Logic 
{
	IMP_FACTORY(CMove);

	//---------------------------------------------------------

	bool CMove::spawn(CEntity *entity, CMap *map, const Map::CEntity *entityInfo) 
	{
		if(!IComponent::spawn(entity,map,entityInfo))
			return false;

		/*_movList.push_back(Vector3(0,0,0));
		_movList.push_back(Vector3(10,0,0));
		_movList.push_back(Vector3(10,0,10));
		_movList.push_back(Vector3(0,0,10));*/
		Movement m;

		m._dir = Vector3(1, 0, 0);
		m._numTicks = 100;
		_movList.push_back(m);

		m._dir = Vector3(0, 0, -1);
		m._numTicks = 100;
		_movList.push_back(m);

		m._dir = Vector3(-1, 0, 0);
		m._numTicks = 100;
		_movList.push_back(m);

		m._dir = Vector3(0, 0, 1);
		m._numTicks = 100;
		_movList.push_back(m);

		return true;

	} // spawn

	//---------------------------------------------------------

	bool CMove::accept(CMessage *message)
	{
		return false;

	} // accept

	//---------------------------------------------------------

	void CMove::process(CMessage *message)
	{
		switch(message->getType())
		{
		default:
			break;
		}

	} // process

	//---------------------------------------------------------

	void CMove::tick(unsigned int msecs)
	{
		IComponent::tick(msecs);

		//_entity->setPosition(_movList.back());
		if (_movList.size() != 0)
		{
			Movement m = _movList.back();
			if (m._numTicks > 0)
			{
				-- _movList.back()._numTicks;
				Vector3 v = _movList.back()._dir * _speed * msecs;
				Vector3 v2 = _entity->getPosition();
				_entity->setPosition(v + v2);
			}
			else {
				std::cout <<  _entity->getPosition().x << ", " << _entity->getPosition().y << ", " << _entity->getPosition().z << std::endl;
				m._numTicks = 100;
				_movList.push_front(m);
				_movList.pop_back();
			}

			for (unsigned int i = 0; i < 5000000; ++i)
				i = i;
		}
	}


} // namespace Logic

