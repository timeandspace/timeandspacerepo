/**
@file Shooter.cpp

Contine la implementación del componente

@see Logic::IComponent

@author Alvaro Blazquez Checa
@date Mayo, 2014
*/

#include "Weapon.h"

#include "Logic/Entity/Entity.h"
#include "Map/MapEntity.h"
#include "Physics/Server.h"
#include "Logic/Maps/EntityFactory.h"
#include "Logic/Maps/map.h"
#include "PerceptionComponent.h"
#include "Camera.h"
#include "Graphics/Server.h"
#include "SimpleAnimatedGraphics.h"
#include "ParticleSystem.h"


namespace Logic
{
	//---------------------------------------------------------

	CWeapon::CWeapon() : 
		IComponent(), 
		_currentWeapon(TWeaponType::NORMAL), 
		_shootingPoint(Vector3::ZERO),
		_shootingPointBone(""),
		_init(false),
		_ammo(0), _offsetOrientation (Quaternion::IDENTITY), _offsetPosition(Vector3::ZERO)
	{
	}

	//---------------------------------------------------------

	CWeapon::~CWeapon() 
	{
	}

	//---------------------------------------------------------

	bool CWeapon::spawn(CEntity *entity, CMap *map, const Map::CEntity *entityInfo)
	{
		if (!IComponent::spawn(entity, map, entityInfo))
			return false;

		if(entityInfo->hasAttribute("shooting_point"))
			_shootingPoint = entityInfo->getVector3Attribute("shooting_point");

		if(entityInfo->hasAttribute("shooting_point_bone"))
			_shootingPointBone = entityInfo->getStringAttribute("shooting_point_bone");

		/*if(entityInfo->hasAttribute("offsetPosition"))
			_offsetPosition = entityInfo->getVector3Attribute("offsetPosition");

		if(entityInfo->hasAttribute("offsetRotationX"))
		{
			Quaternion qX(Ogre::Degree(entityInfo->getFloatAttribute("offsetRotationX")), Vector3::UNIT_X);
			_offsetOrientation = _offsetOrientation * qX;
		}
		if(entityInfo->hasAttribute("offsetRotationY"))
		{
			Quaternion qY(Ogre::Degree(entityInfo->getFloatAttribute("offsetRotationY")), Vector3::UNIT_Y);
			_offsetOrientation = _offsetOrientation * qY;
		}
		if(entityInfo->hasAttribute("offsetRotationZ"))
		{
			Quaternion qZ(Ogre::Degree(entityInfo->getFloatAttribute("offsetRotationZ")), Vector3::UNIT_Z);
			_offsetOrientation = _offsetOrientation * qZ;
		}*/

		assert(!(_shootingPoint == Vector3::ZERO) || (!_shootingPointBone.empty()) && "Falta shootingPoint");

		return true;

	} // spawn

	//---------------------------------------------------------

	bool CWeapon::activate()
	{
		if(!_init)
		{
			_shootingPoint = Graphics::CServer::getSingletonPtr()->getBonePosition(_entity->getName(), _shootingPointBone);
		}
		return IComponent::activate();
	}

	//---------------------------------------------------------

	void CWeapon::deactivate()
	{
		return IComponent::deactivate();
	}

	//---------------------------------------------------------

	bool CWeapon::accept(CMessage *message)
	{
		return message->getType() == Message::FIRE;
	} // accept

	//---------------------------------------------------------

	void CWeapon::tick(unsigned int msecs)
	{
		//_shootingPoint = Graphics::CServer::getSingletonPtr()->getBonePosition(_entity->getName(), _shootingPointBone);
	}

	//---------------------------------------------------------

	void CWeapon::throwWeapon()
	{
		Graphics::CEntity * ent = Graphics::CServer::getSingletonPtr()->getEntity(_entity->getName());
		Vector3 pos = ent->getPosition();

		ent->detachObject(_entity->getName());
		ent->setPosition(pos);
		ent->setVisible(true);

		// Crear un special trigger para poder coger el arma
		Map::CEntity* specialActionTriggerInfo = Logic::CEntityFactory::getSingletonPtr()->getMapEntityByName("SpecialActionTrigger");
		std::stringstream ss;
		ss << pos.x << " " << pos.y << " " << pos.z; 
		specialActionTriggerInfo->setAttribute("position", ss.str());
		specialActionTriggerInfo->setAttribute("object", _entity->getName());
		// Creamos una nueva entidad con el prefab
		Logic::CEntity* specialActionTrigger = CEntityFactory::getSingletonPtr()->
			createEntity(specialActionTriggerInfo, _entity->getMap());
		// Llamamos al activate de la entidad (IMPORTANTE: si no no funcionará en el .exe en release)
		specialActionTrigger->activate();
	}

} // namespace Logic
