/**
@file AnimatedGraphics.cpp

Contiene la implementación del componente que controla la representación
gráfica de una entidad estática.

@see Logic::CAnimatedGraphics
@see Logic::IComponent

@author David Llansó
@date Agosto, 2010
*/

#include "SimpleAnimatedGraphics.h"

#include "Logic/Entity/Entity.h"
#include "Logic/Maps/Map.h"
#include "Map/MapEntity.h"
#include "AvatarController.h"
#include "NPCController.h"

#include "Graphics/Scene.h"

using namespace Logic::Animations;

namespace Logic 
{
	IMP_FACTORY(CSimpleAnimatedGraphics);


	CSimpleAnimatedGraphics::CSimpleAnimatedGraphics() : 
		CGraphics(), 
		_animatedGraphicsEntity(0),
		_defaultAnimationName(""),
		_animationName(""),
		_loop(false)
	{
	}

	//---------------------------------------------------------

	CSimpleAnimatedGraphics::~CSimpleAnimatedGraphics()
	{
	}

	//---------------------------------------------------------

	Graphics::CEntity* CSimpleAnimatedGraphics::createGraphicsEntity(const Map::CEntity *entityInfo)
	{
		_animatedGraphicsEntity = new Graphics::CAnimatedEntity(_entity->getName(),_model);
		if(!_scene->addEntity(_animatedGraphicsEntity))
			return 0;

		_animatedGraphicsEntity->setTransform(_entity->getTransform());

		_animatedGraphicsEntity->setObserver(this);

		if(entityInfo->hasAttribute("setCastShadows"))
		{
			bool value = entityInfo->getBoolAttribute("setCastShadows");
			_animatedGraphicsEntity->setCastShadows(value);

		}

		if(entityInfo->hasAttribute("defaultAnimation"))
		{
			_defaultAnimationName = entityInfo->getStringAttribute("defaultAnimation");
		}

		if(entityInfo->hasAttribute("animation"))
		{
			_animationName = entityInfo->getStringAttribute("animation");
		}

		if(entityInfo->hasAttribute("loop"))
		{
			_loop = entityInfo->getBoolAttribute("animation");
		}

		_animatedGraphicsEntity->init(_defaultAnimationName);

		return _animatedGraphicsEntity;

	} // createGraphicsEntity

	//---------------------------------------------------------

	bool CSimpleAnimatedGraphics::activate()
	{
		return IComponent::activate();
	}

	//---------------------------------------------------------

	void CSimpleAnimatedGraphics::deactivate()
	{
		return IComponent::deactivate();
	}

	//---------------------------------------------------------

	bool CSimpleAnimatedGraphics::accept(CMessage *message)
	{
		return CGraphics::accept(message) || 
			message->getType() == Message::SET_ANIMATION ||
			message->getType()== Message::STOP_ANIMATION ||
			message->getType()== Message::SET_ENTITY_CLOCK_MODIFIER;

	} // accept

	//---------------------------------------------------------

	void CSimpleAnimatedGraphics::process(CMessage *message)
	{
		CGraphics::process(message);

		switch(message->getType())
		{
		case Message::SET_ANIMATION:
			_animatedGraphicsEntity->setAnimation(_animationName, _loop);
			break;

		case Message::STOP_ANIMATION:
			_animatedGraphicsEntity->stopAllAnimations();
			break;

		case Message::SET_ENTITY_CLOCK_MODIFIER:
			CClockModifierMessage * clockModifierMessage = static_cast<CClockModifierMessage *>(message);
			if(clockModifierMessage->_operation == "SlowDown" 
				|| clockModifierMessage->_operation == "SpeedUp") 
			{
				float time = _animatedGraphicsEntity->getClockModifier() 
					+ clockModifierMessage->_clockModifier;
				if (time < 0) time = 0;
				_animatedGraphicsEntity->setClockModifier(time);
			}
			else
				_animatedGraphicsEntity->setClockModifier(clockModifierMessage->_clockModifier);
			break;

		}

	} // process

	//---------------------------------------------------------

	void CSimpleAnimatedGraphics::playAnimation()
	{
		_animatedGraphicsEntity->setBaseAnimation(_animationName);
	}

	//---------------------------------------------------------

	void CSimpleAnimatedGraphics::animationFinished(const std::string &animation)
	{

		if(animation == _animationName)
		{
			//Enviar mensaje de Animation finished
			CAnimationFinishedMessage *msg = new CAnimationFinishedMessage(TAnimations::ANY);
			_entity->emitMessage(msg, this);
		}
		_animatedGraphicsEntity->setAnimation(_defaultAnimationName);
	}


	//---------------------------------------------------------





} // namespace Logic

