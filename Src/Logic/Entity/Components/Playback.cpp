/**
@file Playback.cpp

Contiene la implementaci�n del componente que reproduce una lista
de movimientos que recibe por mensaje.

@author �lvaro Bl�zquez Checa
@author Alejandro P�rez Alonso

@date Febrero, 2014
*/

#include "Playback.h"

#include "Logic/Entity/Entity.h"
#include "Logic/Maps/Map.h"
#include "Map/MapEntity.h"
#include "Application/BaseApplication.h"

namespace Logic 
{
	IMP_FACTORY(CPlayback);

	bool CPlayback::spawn(CEntity* entity, CMap *map, const Map::CEntity *entityInfo)
	{
		if(!IComponent::spawn(entity,map,entityInfo))
			return false;

		return true;
	}

	//--------------------------------------------------------

	bool CPlayback::accept(CMessage *message)
	{
		return message->getType() == Message::PLAYBACK || message->getType() == Message::STOP_ENTITY ;
	}

	//--------------------------------------------------------

	void CPlayback::process(CMessage *message)
	{
		if(message->getType() == Message::PLAYBACK)
		{
			CPlayBackMessage* playBackMessage = static_cast<CPlayBackMessage *>(message);
			destroyList();
			_messageList = playBackMessage->_messageList;
			initList();
			play();
			_clock = 0;
		}
		else if(message->getType() == Message::STOP_ENTITY)
		{
			stop();
		}
		
	}

	//--------------------------------------------------------

	void CPlayback::initList()
	{
		if (_messageList.size() > 0) 
		{
			CMessageList::const_iterator itr = _messageList.begin();
			CMessageList::const_iterator end = _messageList.end();

			_initialMessage = (*itr);
			for(; itr != end; ++itr)
			{
				(*itr)->increaseRef();
			}
		}
	}

	//--------------------------------------------------------

	void CPlayback::destroyList()
	{
		if (_messageList.size() > 0)
		{
			CMessageList::const_iterator itr = _messageList.begin();
			CMessageList::const_iterator end = _messageList.end();

			for(; itr != end; ++itr)
			{
				(*itr)->release();
			}
		}
	}

	//--------------------------------------------------------

	void CPlayback::play()
	{
		_playing = true;
	}

	//--------------------------------------------------------

	void CPlayback::stop()
	{
		_playing = false;
	}

	//--------------------------------------------------------

	void CPlayback::restart()
	{
		_clock = 0;
		_numMessages = 0;

		if (_messageList.size() > 0)
		{
			CMessageList::const_iterator itr = _messageList.begin();
			
			while((*itr) != _initialMessage)
			{
				_messageList.push_back(_messageList.front());
				_messageList.pop_front();
				itr = _messageList.begin();
			}
		}
	}

	void CPlayback::tick(unsigned int msecs)
	{
		IComponent::tick(msecs);

		if ( _playing &&_messageList.size() > 0)
		{
			while (_messageList.size() > 0 && static_cast<CControlMessage *>(_messageList.front())->_time <= _clock)
			{
				_entity->emitMessage(_messageList.front());
				_messageList.front()->release();
				_messageList.pop_front();
				//_numMessages++;
			}
			_clock += msecs;
		}
	}
}



