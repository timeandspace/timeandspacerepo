/**
@file Entity.cpp

Contiene la implementaci�n de la clase Entity, que representa una entidad
de juego. Es una colecci�n de componentes.

@see Logic::CEntity
@see Logic::IComponent

@author David Llans�
@date Julio, 2010
*/

#include "Entity.h"

// Componentes
#include "Component.h"

#include "Logic/Server.h"
#include "Logic/Maps/Map.h"
#include "Map/MapEntity.h"

#include "Components\Shooter.h"

#include "GUI/Server.h"
#include "GUI/PlayerController.h"


namespace Logic 
{
	CEntity::CEntity(TEntityID entityID) : _entityID(entityID), 
		_map(0), _type(""), _name(""), _parentEntity(""), 
		_transform(Matrix4::IDENTITY),_isPlayer(false), _activated(false), 
		_clockModifier(1.0f),_aux(0), _pitch(0.0f)
	{

	} // CEntity

	//---------------------------------------------------------

	CEntity::~CEntity()
	{
		assert(!_map && "��Antes de destruir la entidad debe desacoplarse del mapa!!");

		destroyAllComponents();
		_map = 0;

	} // ~CEntity

	//---------------------------------------------------------

	bool CEntity::spawn(CMap *map, const Map::CEntity *entityInfo) 
	{
		// Leemos las propiedades comunes
		_map = map;
		_type = entityInfo->getType();

		if(entityInfo->hasAttribute("name"))
			_name = entityInfo->getStringAttribute("name");

		if(entityInfo->hasAttribute("position"))
		{
			Vector3 position = entityInfo->getVector3Attribute("position");
			_transform.setTrans(position);
		}

		// Por comodidad en el mapa escribimos los �ngulos en grados.
		if(entityInfo->hasAttribute("orientation"))
		{
			float yaw = Math::fromDegreesToRadians(entityInfo->getFloatAttribute("orientation"));
			Math::yaw(yaw,_transform);
		}

		if(entityInfo->hasAttribute("isPlayer"))
			_isPlayer = entityInfo->getBoolAttribute("isPlayer");

		if(entityInfo->hasAttribute("AttachTo"))
		{
			_parentEntity = entityInfo->getStringAttribute("AttachTo");
		}

		// Inicializamos los componentes
		TComponentList::const_iterator it;

		bool correct = true;

		for( it = _components.begin(); it != _components.end() && correct; ++it )
			correct = (*it)->spawn(this,map,entityInfo) && correct;

		// Posicion y orientacion inicial
		_initialPos = this->getPosition();
		_initialYaw = this->getYaw();

		return correct;

	} // spawn

	//---------------------------------------------------------

	bool CEntity::activate() 
	{
		// Si somos jugador, se lo decimos al servidor
		// y nos registramos para que nos informen
		// de los movimientos que debemos realizar
		if (isPlayer())
		{
			CServer::getSingletonPtr()->setPlayer(this);
			GUI::CServer::getSingletonPtr()->getPlayerController()->setControlledAvatar(this);
		}

		// Activamos los componentes
		TComponentList::const_iterator it;

		// Solo si se activan todos los componentes correctamente nos
		// consideraremos activados.
		_activated = true;

		for( it = _components.begin(); it != _components.end(); ++it )
			_activated = (*it)->activate() && _activated;

		if(!_parentEntity.empty())
		{
			CEntity* entity = CServer::getSingletonPtr()->getMap()->getEntityByName(_parentEntity);
			assert(entity && "No existe la entidad padre");
			entity->attachEntity(this);
		}


		return _activated;

	} // activate

	//---------------------------------------------------------

	void CEntity::reset()
	{
		this->setYaw(_initialYaw);
		this->setPosition(_initialPos);

		// Emite un mensaje de reset a los componentes
		// Lo procesar� el componente de fisica para actualizar la posicion fisica
		CResetMessage* message = new CResetMessage();
		message->_initialPos = this->_initialPos;
		this->emitMessage(message);
	}

	//--------------------------------------------------------

	void CEntity::deactivate() 
	{
		// Si �ramos el jugador, le decimos al servidor que ya no hay.
		// y evitamos que se nos siga informando de los movimientos que 
		// debemos realizar
		if (isPlayer())
		{
			GUI::CServer::getSingletonPtr()->getPlayerController()->setControlledAvatar(0);
			CServer::getSingletonPtr()->setPlayer(0);
		}

		TComponentList::const_iterator it;

		// Desactivamos los componentes
		for( it = _components.begin(); it != _components.end(); ++it )
			(*it)->deactivate();

		if(!_parentEntity.empty())
		{
			CEntity* entity = CServer::getSingletonPtr()->getMap()->getEntityByName(_parentEntity);
			assert(entity && "No existe la entidad padre");
			entity->detachEntity(this);
		}

		_activated = false;

	} // deactivate

	void CEntity::deactivateAllComponents(IComponent *component)
	{
		TComponentList::const_iterator it;
		for( it = _components.begin(); it != _components.end(); ++it )
		{
			if (component != (*it))
				(*it)->deactivate();
		}
	}

	//---------------------------------------------------------
	void CEntity::tick(unsigned int msecs) 
	{
		TComponentList::const_iterator it;

		for( it = _components.begin(); it != _components.end(); ++it )
		{
			if ((*it)->isActivated())
				(*it)->tick(msecs * _clockModifier);
		}


	} // tick

	//---------------------------------------------------------

	IComponent *CEntity::getComponentById(std::string* component_name)
	{

		TComponentList::const_iterator it;

		for( it = _components.begin(); it != _components.end(); ++it )
		{
			//if ((*it)->getEntity()->getName()==)

			//if (dynamic_cast<CAvatarController*>(*it))
			CShooter *s = dynamic_cast<CShooter *>(*it);
			if (s != NULL) return (*it);
			/*if (typeid((*it)).name()== typeid(CShooter)){


			}*/

		}


	} // addComponent

	//---------------------------------------------------------

	void CEntity::addComponent(IComponent* component)
	{
		_components.push_back(component);
		component->setEntity(this);

	} // addComponent

	//---------------------------------------------------------

	IComponent* CEntity::getComponent(std::string componentClass)
	{
		TComponentList::const_iterator it;

		for( it = _components.begin(); it != _components.end(); ++it )
		{
			std::string a = ((*it)->name);
			if ((*it)->name == componentClass)
				return (*it);
		}

		return NULL;
	} // getComponent

	//---------------------------------------------------------

	bool CEntity::removeComponent(IComponent* component)
	{
		TComponentList::const_iterator it = _components.begin();

		bool removed = false;
		// Buscamos el componente hasta el final, por si aparec�a
		// m�s de una vez... (no tendr�a mucho sentido, pero por si
		// acaso).
		while (it != _components.end()) 
		{
			if (*it == component)
			{
				it = _components.erase(it);
				removed = true;
			}
			else
				++it;
		}
		if (removed)
			component->setEntity(0);
		return removed;

	} // removeComponent

	//---------------------------------------------------------

	void CEntity::destroyAllComponents()
	{
		IComponent* c;
		while(!_components.empty())
		{
			c = _components.back();
			_components.pop_back();
			delete c;
		}

	} // destroyAllComponents

	//---------------------------------------------------------

	bool CEntity::emitMessage(CMessage *message, IComponent* emitter)
	{
		// Interceptamos los mensajes que adem�s de al resto de los
		// componentes, interesan a la propia entidad.
		switch(message->getType())
		{
		case Message::SET_TRANSFORM:
			_transform = static_cast<CSetTransformMessage*>(message)->_transform;
			break;
		case Message::SET_ENTITY_CLOCK_MODIFIER:
			CClockModifierMessage* clockMofifierMessage = static_cast<CClockModifierMessage*>(message);
			//if(clockMofifierMessage->_operation == "SlowDown")
			//{
			//	_clockModifier -= 0.5f;
			//	if (_clockModifier < 0) _clockModifier = 0;
			//}
			//else if(clockMofifierMessage->_operation == "SpeedUp")
			//{
			//	_clockModifier += 0.5f;
			//	// TODO:: limitar la aceleracion
			//}
			_clockModifier += clockMofifierMessage->_clockModifier;
			if (_clockModifier < 0) _clockModifier = 0;
			break;
		}

		TComponentList::const_iterator it;
		// Para saber si alguien quiso el mensaje.
		bool anyReceiver = false;
		for( it = _components.begin(); it != _components.end(); ++it )
		{
			// Al emisor no se le envia el mensaje.
			if( emitter != (*it) )
				anyReceiver = (*it)->set(message) || anyReceiver;
		}

		TAttachedEntityList::const_iterator iter = _attachedEntities.begin();
		TAttachedEntityList::const_iterator end = _attachedEntities.end();
		bool anyReceiver2 = false;
		for (; iter != end; ++iter)
		{
			if (message->getType() == Message::AVATAR_WALK ||
				message->getType() == Message::KINEMATIC_MOVE)
			{
				if ((*iter))
					anyReceiver2 = (*iter)->emitMessage(message, emitter);
			}
		}

		if (!anyReceiver && !anyReceiver2)
		{
			if (message->getRefCount() == 0)
			{
				delete message;
				message = NULL;
			}
		}

		return anyReceiver || anyReceiver2;

	} // emitMessage

	//---------------------------------------------------------

	void CEntity::setTransform(const Matrix4& transform) 
	{
		_transform = transform;

		// Avisamos a los componentes del cambio.
		CSetTransformMessage* message = new CSetTransformMessage();
		message->_transform = _transform;
		emitMessage(message);

	} // setTransform

	//---------------------------------------------------------

	void CEntity::setPosition(const Vector3 &position, IComponent* invoker)  
	{
		_transform.setTrans(position);

		// Avisamos a los componentes del cambio.
		CSetTransformMessage* message = new CSetTransformMessage();
		message->_transform = _transform;
		emitMessage(message,invoker);

	} // setPosition

	//---------------------------------------------------------

	void CEntity::setOrientation(const Matrix3& orientation) 
	{
		_transform = orientation;

		// Avisamos a los componentes del cambio.
		CSetTransformMessage* message = new CSetTransformMessage();
		message->_transform = _transform;
		emitMessage(message);

	} // setOrientation

	//---------------------------------------------------------

	Matrix3 CEntity::getOrientation() const
	{
		Matrix3 orientation;
		_transform.extract3x3Matrix(orientation);
		return orientation;

	} // getOrientation

	//---------------------------------------------------------

	Vector3 CEntity::getInitialPos()
	{
		return _initialPos;
	}

	//---------------------------------------------------------

	float CEntity::getInitialYaw()
	{
		return _initialYaw;
	}

	//---------------------------------------------------------

	void CEntity::setYaw(float yaw) 
	{
		Math::setYaw(yaw,_transform);

		// Avisamos a los componentes del cambio.
		CSetTransformMessage* message = new CSetTransformMessage();
		message->_transform = _transform;
		emitMessage(message);

	} // setYaw

	//---------------------------------------------------------

	void CEntity::yaw(float yaw) 
	{
		Math::yaw(yaw,_transform);

		// Avisamos a los componentes del cambio.
		CSetTransformMessage* message = new CSetTransformMessage();
		message->_transform = _transform;
		emitMessage(message);

	} // yaw

	//---------------------------------------------------------

	void CEntity::attachEntity(CEntity *ent)
	{
		if (this != ent)
		{
			ent->setParent(_name);
			_attachedEntities.push_back(ent);
		}
	}

	//---------------------------------------------------------

	void CEntity::detachEntity(CEntity *ent)
	{
		for (int i = 0; i < _attachedEntities.size(); ++i)
		{
			if (_attachedEntities[i] == ent)
			{
				CEntity *auxEnt = _attachedEntities[i];
				_attachedEntities[i] = _attachedEntities[_attachedEntities.size() - 1];
				_attachedEntities.pop_back();
				ent->setParent("");
				return;
			}
		}
	}

	//---------------------------------------------------------


} // namespace Logic
