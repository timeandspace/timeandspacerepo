/**
@file CommunicationPort.cpp

Contiene la implementaci�n de la clase que se encarga del intercambio 
de los mensajes.

@see Logic::CCommunicationPort

@author David Llans� Garc�a
@date Julio, 2010
*/

#include "CommunicationPort.h"

namespace Logic {

	CCommunicationPort::~CCommunicationPort()
	{
		CMessageList::iterator it = _messages.begin();
		for(; it != _messages.end(); ++it)
		{
			(*it)->release();
		}
		_messages.clear();

	} // ~CCommunicationPort

	//---------------------------------------------------------

	bool CCommunicationPort::set(CMessage *message)
	{
		bool accepted = accept(message);
		if(accepted)
		{
			_messages.push_back(message);
			message->increaseRef();
		}

		return accepted;

	} // set

	//---------------------------------------------------------

	void CCommunicationPort::processMessages()
	{
		CMessageList::iterator it = _messages.begin();

		for(; it != _messages.end(); ++it) 
		{
			process(*it);
			(*it)->release();
		}
		_messages.clear();

	} // processMessages

} // namespace Logic
