/**
@file Message.h

Contiene el tipo de datos de un mensaje.

@see Logic::CMessage

@author David Llans� Garc�a
*/
#ifndef __Logic_Message_H
#define __Logic_Message_H

#include <string>

#include "BaseSubsystems/Math.h"

// Predeclaraciones
namespace Logic {
	class CEntity;
};

namespace Logic
{
	/**
	Namespace para los tipos de mensajes posibles.
	*/
	namespace Message
	{
		enum TMessageType
		{
			UNASSIGNED = 0xFFFFFFFF,

			SET_TRANSFORM,
			SET_ANIMATION,
			STOP_ANIMATION,
			ANIMATION_FINISHED,

			CONTROL,
			AVATAR_WALK,
			KINEMATIC_MOVE,
			TOUCHED,
			UNTOUCHED,
			SWITCH,
			DAMAGED,
			RECORDING,
			PLAYBACK,
			JUMP,
			STOP_ENTITY,
			SET_ENTITY_CLOCK_MODIFIER,
			BACKWARDS,
			RESET,
			TELETRANSPORT,

			//SpecialControl
			SPECIAL_CONTROL,

			// Shooter
			FIRE,
			CHANGE_WEAPON,
			EDIT_TIME,
			ADD_FORCE,
			ACTIVATE,
			DEACTIVATE,
			//IA
			FINISHED_ROUTE,
			FINISHED_MOVE,
			ENTITY_ENGAGED,
			
			ROUTE_TO,
			MOVE_TO,
			SET_STEERING_BEHAVIOUR,
			SPECIAL_ACTION,

			ACTION,

			// Sistema de percepci�n
			PERCEIVED,
			STOP_PERCEIVED,
			ADD_SIGNAL,
			REMOVE_SIGNAL,
			ACTIVATE_SIGNAL,
			ACTIVATE_PERCEPTION,
			//SOUND
			PLAY_SOUND,
			STOP_SOUND,
			CHANGE_PARAMETER,
			// Camera
			CHANGE_CAMERA,
			ACTIVATE_CUTSCENE,
			// Sistema de particulas
			PARTICLE_SYSTEM,
			//Graphics
			CHANGE_MATERIAL
		};
	}

	/**
	Tipo copia para los mensajes. Por simplicidad.
	*/
	typedef Message::TMessageType TMessageType;

	/**
	Namespace para los tipos de animaciones posibles
	*/
	namespace Animations {
		enum TAnimations
		{
			WALK = 0x0,
			WALK_BACK,
			RUN,
			RUN_RIGHT,
			RUN_LEFT,
			STRAFE_RIGHT,
			STRAFE_LEFT,
			IDLE,
			JUMP,
			CROUCH,
			WALK_CROUCH,
			WALK_BACK_CROUCH,
			STRAFE_RIGHT_CROUCH,
			STRAFE_LEFT_CROUCH,
			GET_OBJECT,
			WALK_WEAPON,
			WALK_BACK_WEAPON,
			RUN_WEAPON,
			STRAFE_RIGHT_WEAPON,
			STRAFE_LEFT_WEAPON,
			IDLE_WEAPON,
			CROUCH_WEAPON,
			FIRE,
			DEATH,
			DAMAGE,
			CHANGE_WEAPON,
			AIM,
			PUSH_BUTTON,
			ANY
		};
	};
	typedef Animations::TAnimations TAnimations;


	/**
	Namespace para los tipos de animaciones posibles
	*/
	namespace AvatarActions {
		enum TAvatarActions
		{
			IDLE,
			WALK,
			WALK_BACK,
			STOP_WALK,
			RUN,
			STOP_RUN,
			STRAFE_RIGHT,
			STRAFE_LEFT,
			STOP_STRAFE,
			STOP_ALL,
			JUMP,
			STOP_JUMP,
			CROUCH,
			STOP_CROUCH,
			TURN,
			PITCH,
			ZOOM_IN,
			ZOOM_OUT,
			
			CONTEXTUAL_ACTION,
			WALK_WEAPON,
			WALK_BACK_WEAPON,
			RUN_WEAPON,
			STRAFE_RIGHT_WEAPON,
			STRAFE_LEFT_WEAPON,
			IDLE_WEAPON,
			CROUCH_WEAPON,
			FIRE,
			STOP_FIRE,
			DEATH,
			SPECIAL_POWER1, // Copias
			SPECIAL_POWER2, // Turbo
			SPECIAL_POWER3, // Reset sin crear copias
			CHANGE_WEAPON
		};
	};
	typedef AvatarActions::TAvatarActions TAvatarActions;


	/**
	Namespace para los tipos de actiones especiales
	*/
	namespace SpecialActions {
		enum TSpecialActions
		{
			GET_OBJECT,
			ACTION,
			ACTION_CONFIRMED,
			NONE
		};
	};
	typedef SpecialActions::TSpecialActions TSpecialActions;

	/**
	Namespace para los tipos de armas disponibles
	*/
	namespace Weapons {
		enum TWeaponType
		{
			NORMAL = 0x0,
			SLOW_DOWN,
			SPEED_UP,
			BACKWARD
		};

	};

	typedef Weapons::TWeaponType TWeaponType;

	namespace Camera 
	{
		enum TCameraType
		{
			NORMAL = 0x0,
			WITH_WEAPON,
			AIMING,
			MAP
		};
	}
	typedef Camera::TCameraType TCameraType;


	//CALLBACKS

	#define MYCALLBACK(functionAddress) std::tr1::bind(functionAddress, this, std::tr1::placeholders::_1); 
	
	typedef enum
	{
		PARTICLE_FINISHED
	}
	CallbackEvent;

	namespace Callbacks
	{
		typedef std::tr1::function<void (CallbackEvent)> Callback;
		
	}
	typedef Callbacks::Callback Callback;

	//CALLBACKS
	

	/**
	Clase padre de los distintos mensajes. Contiene el tipo de mensaje y un contador
	de referencias.
	<p>
	@remarks <b>NOTA:</b> Un mismo mensaje puede ser apuntado por varias colas de mensajes,
	por lo que esta clase tiene un contador de referencias. Cuando un mensaje es a�adido a 
	una cola se incrementar� el contador y cuando este sea procesado se decrementar� el
	contador. Si este llega a 0, el mensaje ser� eliminado.
	*/
	class CMessage {
	public:

		/**
		Constructor de la clase, inicializa los atributos
		*/
		CMessage() : _type(Message::UNASSIGNED), _refCount(0) {}

		/**
		Destructor virtual, se deber� redefinir en las clases que hereden de �sta.
		*/
		virtual ~CMessage() {}

		/**
		Devuelve el tipo de mensaje
		*/
		const TMessageType getType() 
		{ 
			return _type;
		}

		/**
		Establece el tipo de mensaje
		*/
		void setType(TMessageType type) 
		{ 
			_type = type;
		}

		/**
		Devuelve el numero de referencias
		*/
		const int getRefCount() 
		{
			return _refCount;
		}

		/**
		Decrementa el numero de referencias, si llega a cero se destruye el objeto
		*/
		void release()
		{
			-- _refCount;
			if (_refCount <= 0)
			{
				delete this;
			}
		}

		/**
		Incrementa el contador de referencias, se dever� llamar a este m�todo cuando
		el mensaje sea apuntado.
		*/
		void increaseRef()
		{
			++ _refCount;
		}


	protected:
		/**
		Tipo del mensaje.
		*/
		TMessageType _type;

		/**
		Numero de referencias que tiene el mensaje
		*/
		int _refCount;
	};




	/***************************************************************************************************/

	class CSetTransformMessage : public CMessage
	{
	public:
		CSetTransformMessage() : CMessage(), _transform(Matrix4::ZERO)
		{
			_type = Message::SET_TRANSFORM;
		}

		~CSetTransformMessage(){}

		Matrix4 _transform;
	};


	/***************************************************************************************************/

	/*class CAnimationMessage : public CMessage
	{
	public:
		CAnimationMessage() : CMessage()
		{
		}

		void setType(TMessageType type)
		{
			_type = type;
		}

		~CAnimationMessage(){}

		Logic::CAnimatedGraphics::TAnimations _animationName;
		bool _bool;
	};*/

	/***************************************************************************************************/



	/***************************************************************************************************/

	/**
	Mensaje de tipo Control
	*/
	class CControlMessage : public CMessage
	{
	public:
		CControlMessage() : CMessage(), _pitch(0.0f), _scroll(0), _yaw(0.0f), _joystick(false)
		{
			_type = Message::CONTROL;
		}

		~CControlMessage(){}

		TAvatarActions _avatarAction;
		float _yaw;
		float _pitch;
		int _scroll;
		unsigned long _time;
		bool _joystick;
		/**
		Vector que se utiliza en los NPC para mandar el vector de movimiento o
		la direcci�n a la que disparar.
		*/
		Vector3 _vector;
	};


	/***************************************************************************************************/
	/**
	Mensaje de tipo SpecialControl solo para el jugador no queremos que se graben para las copias
	*/
	class CSpecialControlMessage : public CMessage
	{
	public:
		CSpecialControlMessage() : CMessage()
		{
			_type = Message::SPECIAL_CONTROL;
		}

		~CSpecialControlMessage(){}

		TAvatarActions _avatarAction;
		
	};


	/***************************************************************************************************/

	class CAvatarWalkMessage : public CMessage
	{
	public:
		CAvatarWalkMessage() : CMessage()
		{
			_type = Message::AVATAR_WALK;
		}

		~CAvatarWalkMessage(){}

		Vector3 _movement;
	};

	/***************************************************************************************************/

	class CKinematicMoveMessage : public CMessage
	{
	public:
		CKinematicMoveMessage() : CMessage()
		{
			_type = Message::KINEMATIC_MOVE;
		}

		~CKinematicMoveMessage(){}

		Vector3 _displ;
	};

	class CActionMessage : public CMessage
	{
	public:
		CActionMessage() : CMessage()
		{
			_type = Message::ACTION;
		}

		~CActionMessage(){}
	};

	/***************************************************************************************************/

	class CTouchedMessage : public CMessage
	{
	public:
		CTouchedMessage() : CMessage()
		{
			_type = Message::TOUCHED;
		}

		~CTouchedMessage(){}

		CEntity *_entity;
	};

	/***************************************************************************************************/

	class CUnTouchedMessage : public CMessage
	{
	public:
		CUnTouchedMessage() : CMessage()
		{
			_type = Message::UNTOUCHED;
		}

		~CUnTouchedMessage(){}

		CEntity *_entity;
	};

	/***************************************************************************************************/

	class CSwitchMessage : public CMessage
	{
	public:
		CSwitchMessage() : CMessage(), _int(0), _activate(true)
		{
			_type = Message::SWITCH;
		}

		~CSwitchMessage(){}

		int _int;
		bool _activate;
	};


	/***************************************************************************************************/

	class CDamagedMessage : public CMessage
	{
	public:
		CDamagedMessage() : CMessage()
		{
			_type = Message::DAMAGED;
		}

		~CDamagedMessage(){}

		float _damage;
	};


	/***************************************************************************************************/

	/**
	Mensaje de tipo Recording
	*/
	class CRecordingMessage : public CMessage
	{
	public:
		CRecordingMessage() : CMessage(), _initialPos(Vector3::ZERO) //TODO : quitar vector si no hace falta
		{
			_type = Message::RECORDING;
		}

		~CRecordingMessage(){}

		Vector3 _initialPos;
	};

	/***************************************************************************************************/

	/**
	Mensaje de tipo Playback, contiene la posicion inicial, el viraje y una lista de mensajes a reproducir.
	*/
	class CPlayBackMessage : public CMessage
	{
	public:
		CPlayBackMessage() : CMessage(), _initialTransform(Matrix4::ZERO)
		{
			_type = Message::PLAYBACK;
		}

		~CPlayBackMessage(){}

		Matrix4 _initialTransform;
		Vector3 _initialPos;
		std::list<CMessage*> _messageList;
		std::string _string;
	};

	/***************************************************************************************************/

	class CClockModifierMessage : public CMessage
	{
	public:
		CClockModifierMessage() : CMessage(), _clockModifier(0.0f)
		{
			_type = Message::SET_ENTITY_CLOCK_MODIFIER;
		}
		~CClockModifierMessage(){}

		float _clockModifier;
		std::string _operation;
	};

	/***************************************************************************************************/

	class CJumpMessage : public CMessage
	{
	public:
		CJumpMessage() : CMessage(), _direction(Vector3::ZERO), _height(0), _time(0), _jump(true)
		{
			_type = Message::JUMP;
		}
		~CJumpMessage(){}

		Vector3 _direction;
		float _height;
		int _time;
		bool _jump; // true salto, false si simula un impulso
	};

	/***************************************************************************************************/

	class CBooleanMessage : public CMessage
	{
	public:
		CBooleanMessage() : CMessage()
		{

		}
		~CBooleanMessage(){}

		bool _value;
	};

	/***************************************************************************************************/

	class CBackwardsMessage : public CMessage
	{
	public:
		CBackwardsMessage() : CMessage(), _duration(1000)
		{
			_type = Message::BACKWARDS;
		}
		~CBackwardsMessage(){}

		std::list<CMessage*> _messageList;
		unsigned long _time; // msecs del componente CSave
		unsigned long _duration; // msecs de rebobinado
	};

	/***************************************************************************************************/

	class CResetMessage : public CMessage
	{
	public:
		CResetMessage() : CMessage()
		{
			_type = Message::RESET;
		}
		~CResetMessage(){}
		Vector3 _initialPos;
	};

	/***************************************************************************************************/

	class CFireMessage : public CMessage
	{

	public:
		CFireMessage() : CMessage(), _weaponType(TWeaponType::SLOW_DOWN), _direction(NULL)
		{
			_type = Message::FIRE;
		}
		CFireMessage(TWeaponType weaponType) : CMessage(), _weaponType(weaponType), _direction(NULL)
		{
			_type = Message::FIRE;
		}
		~CFireMessage() 
		{
			if (_direction)
				delete _direction;
		}
		TWeaponType _weaponType;
		TCameraType _cameraType;
		bool _start;
		Vector3* _direction;
	};

	/***************************************************************************************************/

	class CChangeWeaponMessage : public CMessage
	{
	public:
		CChangeWeaponMessage() : CMessage(), _weaponType(1)
		{
			_type = Message::CHANGE_WEAPON;
		}
		CChangeWeaponMessage(int weaponType) : CMessage(), _weaponType(weaponType)
		{
			_type = Message::CHANGE_WEAPON;
		}
		~CChangeWeaponMessage() {}
		int _weaponType;
	};

	/***************************************************************************************************/

	class CEditTimeMessage : public CMessage
	{
	public:
		CEditTimeMessage() : CMessage(), _clockModifier(0)
		{
			_type = Message::EDIT_TIME;
		}
		CEditTimeMessage(TWeaponType operation, float modifier, int duration) : CMessage(), 
			_operation(operation), _clockModifier(modifier), _duration(duration)
		{
			_type = Message::EDIT_TIME;
		}
		~CEditTimeMessage(){}
		float _clockModifier;
		TWeaponType _operation;
		int _duration;
	};

	/***************************************************************************************************/

	class CAddForceMessage : public CMessage
	{
	public:
		CAddForceMessage() : CMessage(), _force(0), _direction(Vector3::ZERO)
		{
			_type = Message::ADD_FORCE;
		}
		CAddForceMessage(Vector3 dir, int force) : CMessage(), _force(force), _direction(dir)
		{
			_type = Message::ADD_FORCE;
		}
		~CAddForceMessage() {}
		Vector3 _direction;
		int _force;
	};

	/***************************************************************************************************/

	class CRouteToMessage : public CMessage
	{
	public:
		CRouteToMessage() : CMessage()
		{
			_type = Message::ROUTE_TO;
		}
		CRouteToMessage(bool boolean, Vector3 target) : CMessage(), _bool(boolean), _target(target)
		{
			_type = Message::ROUTE_TO;
		}
		/**
		El bool indica si el receptor debe calcular la ruta o debe dejar de recorrer 
		la ruta actual y parar.
		*/
		bool _bool;
		/**
		Destino de la ruta. Si el par�metro _bool es false este valor no se utiliza
		*/
		Vector3 _target;
	};

	/***************************************************************************************************/

	class CIAMessage : public CMessage
	{
	public:
		CIAMessage(TMessageType type) : CMessage()
		{
			_type = type;
		}
		bool _bool;
	};

	/***************************************************************************************************/

	class CMoveToMessage : public CMessage
	{
	public:
		CMoveToMessage() : CMessage()
		{
			_type = Message::MOVE_TO;
		}
		Vector3 _target;
		int _movementType;
		std::string _name;
	};


	/***************************************************************************************************/

	class CSetSteeringBehaviour : public CMessage
	{
	public:
		CSetSteeringBehaviour() : CMessage(), _otherEntityInvolved(NULL), _isAligned(false)
		{
			_type = Message::SET_STEERING_BEHAVIOUR;
		}
		CEntity* _otherEntityInvolved;
		int _movementType;
		bool _startStop;
		bool _isAligned; // Flag que indica si est� alineado.
	};

	/***************************************************************************************************/

	class CSpecialActionMessage : public CMessage
	{
	public:
		CSpecialActionMessage() : CMessage()
		{
			_type = Message::SPECIAL_ACTION;
		}
		CSpecialActionMessage(TSpecialActions actionName, bool enter, CEntity *ent, 
			std::string additionalInfo = "") : CMessage(), _specialAction(actionName), _enter(enter), 
			_entity(ent), _additionalInfo(additionalInfo)
		{
			_type = Message::SPECIAL_ACTION;
		}

		TSpecialActions _specialAction;
		bool _enter;
		//String auxiliar por si se desea pasar algun otro tipo de informacion. Ej. el arma que recoge.
		std::string _additionalInfo;
		// Puntero a la entidad que env�a el mensaje(trigger)
		CEntity *_entity;
	};

	/***************************************************************************************************/

	/**
	Mensaje que se env�a al terminar de reproducir una animacion que no se reproduce en loop
	*/
	class CAnimationFinishedMessage : public CMessage
	{
	public:
		CAnimationFinishedMessage() : CMessage()
		{
			_type = Message::ANIMATION_FINISHED;
		}
		CAnimationFinishedMessage(TAnimations animationName) : CMessage(), _animationName(animationName)
		{
			_type = Message::ANIMATION_FINISHED;
		}

		TAnimations _animationName;
	};

	/***************************************************************************************************/

	class CSoundMessage : public CMessage
	{
	public:
		CSoundMessage() : CMessage()
		{
			_type = Message::PLAY_SOUND;
		}

		void setType(TMessageType type)
		{
			_type = type;
		}

		float _soundParameter;
	};

	/***************************************************************************************************/

	class CAddSignalMessage : public CMessage
	{
	public:
		CAddSignalMessage() : CMessage()
		{
			_type = Message::ADD_SIGNAL;
		}
		CAddSignalMessage(std::string name, int type, float intensity, bool keepAlive) : 
			_signalType(type), _intensity(intensity), _keepAlive(keepAlive), _name(name),
			CMessage()
		{
			_type = Message::ADD_SIGNAL;
		}

		int _signalType;
		float _intensity;
		bool _keepAlive;
		std::string _name;
	};

	/***************************************************************************************************/

	class CRemoveSignalMessage : public CMessage
	{
	public:
		CRemoveSignalMessage() : CMessage()
		{
			_type = Message::REMOVE_SIGNAL;
		}
		CRemoveSignalMessage(std::string name) : _name(name), CMessage()
		{
			_type = Message::REMOVE_SIGNAL;
		}

		std::string _name;
	};

	class CActivateSignalMessage : public CMessage
	{
	public:
		CActivateSignalMessage() : CMessage()
		{
			_type = Message::ACTIVATE_SIGNAL;
		}
		CActivateSignalMessage(std::string name, bool activate) : _name(name), 
			_activate(activate), CMessage()
		{
			_type = Message::ACTIVATE_SIGNAL;
		}

		// Nombre de la se�al
		std::string _name;
		// Indica si se activa o desactiva una se�al
		bool _activate;
	};

	/***************************************************************************************************/

	class CActivatePerceptionMessage : public CMessage
	{
	public:
		CActivatePerceptionMessage() : CMessage()
		{
			_type = Message::ACTIVATE_PERCEPTION;
		}
		CActivatePerceptionMessage(bool active) : _active(active), CMessage()
		{
			_type = Message::ACTIVATE_PERCEPTION;
		}
		bool _active;
	};

	/***************************************************************************************************/

	class CPerceivedMessage : public CMessage
	{
	public:
		CPerceivedMessage() : CMessage()
		{
			_type = Message::PERCEIVED;
		}
		CPerceivedMessage(int type, std::string entityName, std::string entityType, bool perceived) : 
			_perceivedType(type), _perceived(perceived), _entityName(entityName), _entityType(entityType), CMessage()
		{
			_type = Message::PERCEIVED;
		}
		int _perceivedType; // Tipo de sentido que lo ha percibido.
		//CEntity *_entity; // Lo quito porque da problemas cuando mueres y quieres enviar un mensaje de no percibido
		std::string _entityName;
		std::string _entityType;
		bool _perceived; // Indica si ha sido percibido o se ha dejado de percibir.
	};

	/***************************************************************************************************/

	class CCameraMessage : public CMessage
	{
	public:
		CCameraMessage() : CMessage()
		{
			_type = Message::CHANGE_CAMERA;
		}

		CCameraMessage(TCameraType type) : _cameraType(type), CMessage()
		{
			_type = Message::CHANGE_CAMERA;
		}
		TCameraType _cameraType;
	};

	/***************************************************************************************************/

	class CTeletransportMessage : public CMessage
	{
	public:
		CTeletransportMessage() : CMessage()
		{
			_type = Message::TELETRANSPORT;
		}
		CTeletransportMessage(Vector3 position) : CMessage(), _position(position)
		{
			_type = Message::TELETRANSPORT;
		}
		~CTeletransportMessage(){}
		Vector3 _position;
	};

	/***************************************************************************************************/

	class CCutSceneMessage : public CMessage
	{
	public:
		CCutSceneMessage() : CMessage(), _cutSceneNumber(0)
		{
			_type = Message::ACTIVATE_CUTSCENE;
		}
		CCutSceneMessage(int cutSceneNumber) : CMessage(), _cutSceneNumber(cutSceneNumber)
		{
			_type = Message::ACTIVATE_CUTSCENE;
		}
		~CCutSceneMessage(){}
		
		int _cutSceneNumber;
	};

	/***************************************************************************************************/

	class CParticleSystemMessage : public CMessage
	{
	public:
		CParticleSystemMessage() : CMessage(), _duration(0.0f)
		{
			_type = Message::PARTICLE_SYSTEM;
		}
		CParticleSystemMessage(bool activate, float duration = 0) : CMessage(), _activate(activate), _duration(duration)
		{
			_type = Message::PARTICLE_SYSTEM;
		}
		~CParticleSystemMessage(){}

		bool _activate;
		//duracion del efecto (stopTime )
		float _duration; 
	};

	/***************************************************************************************************/

	class CChangeMaterialMessage : public CMessage
	{
	public:
		CChangeMaterialMessage() : CMessage(), _materialName(""), _specialAction(false)
		{
			_type = Message::CHANGE_MATERIAL;
		}
		CChangeMaterialMessage(std::string materialName) : CMessage(), _materialName(materialName), _specialAction(false)
		{
			_type = Message::CHANGE_MATERIAL;
		}
		std::string _materialName;
		bool _specialAction; // Bool utilizado para hacer el fadeOut de una entidad
	};
	
} // namespace Logic

#endif // __Logic_Message_H
