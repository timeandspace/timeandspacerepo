/**
@file EntityFactory.cpp

Contiene la implementaci�n de la clase factor�a de entidades
del juego.

@see Logic::CEntityFactory

@author David Llans� Garc�a, Marco Antonio G�mez Mart�n
@date Agosto, 2010
*/
#include "EntityFactory.h"
#include "ComponentFactory.h"
#include "Logic/Entity/Entity.h"
#include "Logic/Entity/Component.h"
#include "Map.h"

#include "Map/MapEntity.h"
#include "Map/MapParser.h"

#include "Application/ApplicationCommon.h"

#include <iostream>
#include <fstream>
#include <cassert>



/**
Sobrecargamos el operador >> para la lectura de blueprints.
Cada l�nea equivaldr� a una entidad donde la primera palabra es el tipo
y las siguientes son los componentes que tiene.
*/
std::istream& operator>>(std::istream& is, Logic::CEntityFactory::TBluePrint& blueprint) 
{
	is >> blueprint.type;
	std::string aux;
	getline(is,aux,'\n');
	std::istringstream components(aux);
	while(!components.eof())
	{
		aux.clear();
		components >> aux;
		if(!aux.empty())
			blueprint.components.push_back(aux);
	}
	return is;
}

namespace Logic
{

	CEntityFactory *CEntityFactory::_instance = 0;

	//---------------------------------------------------------

	CEntityFactory::CEntityFactory()
	{
		_instance = this;

	} // CEntityFactory

	//---------------------------------------------------------

	CEntityFactory::~CEntityFactory()
	{
		_instance = 0;

	} // ~CEntityFactory

	//---------------------------------------------------------

	bool CEntityFactory::Init() 
	{
		assert(!_instance && "Segunda inicializaci�n de Logic::CEntityFactory no permitida!");

		new CEntityFactory();

		if (!_instance->open())
		{
			Release();
			return false;
		}

		return true;

	} // Init

	//---------------------------------------------------------

	void CEntityFactory::Release() 
	{
		assert(_instance && "Logic::CEntityFactory no est� inicializado!");

		if(_instance)
		{
			_instance->close();
			delete _instance;
		}

	} // Release

	//--------------------------------------------------------

	bool CEntityFactory::open()
	{
		// Cargamos el archivo con las definiciones de las entidades del nivel.
		if (!Logic::CEntityFactory::getSingletonPtr()->loadBluePrints("blueprints.txt"))
			return false;

		// Cargamos los arqueotipos(prefabs). 
		if (!Logic::CEntityFactory::getSingletonPtr()->loadPrefabs("prefabs.txt"))
			return false;

		return true;
	} // open

	//--------------------------------------------------------

	void CEntityFactory::close() 
	{
		unloadBluePrints();
		destroyPrefabs();

	} // close

	//---------------------------------------------------------

	typedef std::pair<std::string,CEntityFactory::TBluePrint> TStringBluePrintPair;

	bool CEntityFactory::loadBluePrints(const std::string &filename)
	{
		// Completamos la ruta con el nombre proporcionado
		std::string completePath(BLUEPRINTS_FILE_PATH);
		completePath = completePath + filename;
		// Abrimos el fichero
		std::ifstream in(completePath.c_str());        
		if(!in)
			return false;

		while(!in.eof())
		{
			// Se lee un TBluePrint del fichero
			TBluePrint b;
			in >> b;
			// Si no era una l�nea en blanco
			if(!b.type.empty())
			{
				// Si el tipo ya estaba definido lo eliminamos.
				if(_bluePrints.count(b.type))
					_bluePrints.erase(b.type);
				// A�adimos a la tabla
				TStringBluePrintPair elem(b.type,b);
				_bluePrints.insert(elem);
			}
		}

		return true;

	} // loadBluePrints

	//---------------------------------------------------------

	void CEntityFactory::unloadBluePrints()
	{
		_bluePrints.clear();

	} // unloadBluePrints

	//---------------------------------------------------------

	void CEntityFactory::destroyPrefabs()
	{
		Map::CMapParser::TEntityList::const_iterator itr = _prefabMap.begin();
		Map::CMapParser::TEntityList::const_iterator end = _prefabMap.end();

		while( itr != end )
		{	
			delete (*itr);
			++itr;
		}
	}

	//---------------------------------------------------------

	Logic::CEntity *CEntityFactory::assembleEntity(const std::string &type) 
	{
		TBluePrintMap::const_iterator it;

		it = _bluePrints.find(type);
		// si el tipo se encuentra registrado.
		if (it != _bluePrints.end()) 
		{
			CEntity* ent = new CEntity(EntityID::NextID());
			std::list<std::string>::const_iterator itc;
			// A�adimos todos sus componentes.
			IComponent* comp;
			CComponentFactory *compFactory = CComponentFactory::getSingletonPtr();
			for(itc = (*it).second.components.begin(); 
				itc !=(*it).second.components.end(); itc++)
			{
				if(compFactory->has((*itc)))
				{
					comp = compFactory->create((*itc));
					comp->setName((*itc));
				}
				else
				{
					assert(!"Nombre erroneo de un componente, Mira a ver si est�n todos bien escritos en el fichero de blueprints");
					delete ent;
					return 0;
				}
				if(comp)
					ent->addComponent(comp);
			}

			return ent;
		}
		return 0;

	} // assembleEntity

	//---------------------------------------------------------


	Logic::CEntity *CEntityFactory::createEntity(
		const Map::CEntity *entityInfo,
		Logic::CMap *map)
	{
		CEntity *ret = assembleEntity(entityInfo->getType());

		if (!ret)
			return 0;

		// A�adimos la nueva entidad en el mapa antes de inicializarla.
		map->addEntity(ret);

		// Y lo inicializamos
		if (ret->spawn(map, entityInfo))
			return ret;
		else {
			map->removeEntity(ret);
			delete ret;
			return 0;
		}

	} // createEntity

	//---------------------------------------------------------

	void CEntityFactory::deleteEntity(Logic::CEntity *entity)
	{
		assert(entity);
		// Si la entidad estaba activada se desactiva al sacarla
		// del mapa.
		entity->getMap()->removeEntity(entity);
		delete entity;
		entity = 0;

	} // deleteEntity

	//---------------------------------------------------------

	void CEntityFactory::deferredDeleteEntity(Logic::CEntity *entity)
	{
		assert(entity);
		TEntityList::const_iterator it = _pendingEntities.begin();
		TEntityList::const_iterator end = _pendingEntities.end();

		for (; it != end; ++it)
		{
			if ((*it) == entity) return;
		}
		_pendingEntities.push_back(entity);

	} // deferredDeleteEntity

	//---------------------------------------------------------

	void CEntityFactory::deleteDefferedEntities()
	{
		TEntityList::iterator it(_pendingEntities.begin());
		TEntityList::const_iterator end(_pendingEntities.end());

		while(it != end)
		{
			if (*it)
				deleteEntity(*it);
			(*it) = NULL;
			++it;
		}

		if (!_pendingEntities.empty())
			_pendingEntities.clear();

	} // deleteDefferedObjects

	//--------------------------------------------------------

	bool CEntityFactory::loadPrefabs(const std::string &filename)
	{
		// Completamos la ruta con el nombre proporcionado
		std::string completePath(BLUEPRINTS_FILE_PATH);
		completePath = completePath + filename;
		// Parseamos el fichero
		if(!Map::CMapParser::getSingletonPtr()->parseFile(completePath))
		{
			assert(!"No se ha podido parsear el mapa.");
			return false;
		}
		// Extraemos las entidades del parseo.
		Map::CMapParser::TEntityList entityList = Map::CMapParser::getSingletonPtr()->getEntityList();
		Map::CMapParser::TEntityList::const_iterator itr = entityList.begin();
		Map::CMapParser::TEntityList::const_iterator end = entityList.end();

		//Creamos nuestra propia copia de cada entidad
		for( ; itr != end; ++itr )
		{
			Map::CEntity* mapEntity = new Map::CEntity(*(*itr));
			_prefabMap.push_back(mapEntity);
		}
		return true;
	}

	//--------------------------------------------------------

	Map::CEntity* CEntityFactory::getMapEntityByType(const std::string &entityName)
	{
		Map::CEntity* entityInfo = NULL;
		Map::CMapParser::TEntityList::const_iterator itr = _prefabMap.begin();
		Map::CMapParser::TEntityList::const_iterator end = _prefabMap.end();
		bool found = false;

		while( !found && itr != end )
		{	
			if( (*itr)->getType().compare(entityName) == 0 )
			{
				entityInfo = *itr;
				found = true;
			}
			else
			{
				++itr;
			}
		}
		return entityInfo;
	}

	//--------------------------------------------------------

	Map::CEntity* CEntityFactory::getMapEntityByName(const std::string &entityName)
	{
		Map::CEntity* entityInfo = NULL;
		Map::CMapParser::TEntityList::const_iterator itr = _prefabMap.begin();
		Map::CMapParser::TEntityList::const_iterator end = _prefabMap.end();
		bool found = false;

		while( !found && itr != end )
		{	
			if( (*itr)->getName().compare(entityName) == 0 )
			{
				entityInfo = *itr;
				found = true;
			}
			else
			{
				++itr;
			}
		}
		return entityInfo;
	}

	//--------------------------------------------------------

	bool CEntityFactory::typeExist(const std::string &entityType)
	{
		Map::CMapParser::TEntityList::const_iterator itr = _prefabMap.begin();
		Map::CMapParser::TEntityList::const_iterator end = _prefabMap.end();
		
		while( itr != end )
		{	
			if( (*itr)->getType().compare(entityType) == 0 )
			{
				return true;
			}
			++itr;
		}
		return false;
	}



} // namespace Logic
