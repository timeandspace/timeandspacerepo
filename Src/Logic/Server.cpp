/**
@file Server.cpp

Contiene la implementaci�n de la clase CServer, Singleton que se encarga de
la gesti�n de la l�gica del juego.

@see Logic::CServer

@author David Llans�
@date Agosto, 2010
*/

#include "Server.h"
#include "Logic/Maps/Map.h"

#include "Logic/Maps/EntityFactory.h"
#include "AI/Server.h"

#include "Map/MapParser.h"

#include "Managers/Replay System/ReplayManager.h"
#include "Managers/Player Manager/PlayerManager.h"
#include "Managers/GameManager.h"

#include <cassert>

namespace Logic {

	CServer* CServer::_instance = 0;

	//--------------------------------------------------------

	CServer::CServer() : _map(0)
	{
		_instance = this;

	} // CServer

	//--------------------------------------------------------

	CServer::~CServer()
	{
		_instance = 0;

	} // ~CServer
	
	//--------------------------------------------------------

	bool CServer::Init()
	{
		assert(!_instance && "Segunda inicializaci�n de Logic::CServer no permitida!");

		new CServer();

		if (!_instance->open())
		{
			Release();
			return false;
		}

		return true;

	} // Init

	//--------------------------------------------------------

	void CServer::Release()
	{
		assert(_instance && "Logic::CServer no est� inicializado!");

		if(_instance)
		{
			_instance->close();
			delete _instance;
		}

	} // Release

	//--------------------------------------------------------

	bool CServer::open()
	{
		// Inicializamos el parser de mapas.
		if (!Map::CMapParser::Init())
			return false;

		// Inicializamos la factor�a de entidades.
		if (!Logic::CEntityFactory::Init())
			return false;

		if (!Logic::CReplayManager::Init())
			return false;

		if (!Logic::CPlayerManager::Init())
			return false;

		if (!Logic::CGameManager::Init())
			return false;

		return true;
	} // open

	//--------------------------------------------------------

	void CServer::close() 
	{
		unLoadLevel();

		Logic::CEntityFactory::Release();
		
		Map::CMapParser::Release();

		Logic::CReplayManager::Release();

		Logic::CGameManager::Release();

	} // close

	//--------------------------------------------------------

	bool CServer::loadLevel(const std::string &filename, Graphics::CScene* scene)
	{
		// solo admitimos un mapa cargado, si iniciamos un nuevo nivel 
		// se borra el mapa anterior.
		unLoadLevel();

		onLoadLevelActions();

		if(_map = CMap::createMapFromFile(filename, scene))
		{
			return true;
		}

		return false;

	} // loadLevel

	//--------------------------------------------------------

	void CServer::restartLevel(const std::string &filename, Graphics::CScene* scene)
	{
		unLoadLevel();

		onRestartLevelActions();

		_map = CMap::createMapFromFile(filename, scene);
		_map->activate();
	}

	//-------------------------------------------------------

	void CServer::unLoadLevel()
	{
		if(_map)
		{
			_map->deactivate();
			delete _map;
			_map = 0;
		}
		_player = 0;

	} // unLoadLevel

	//---------------------------------------------------------

	void CServer::onLoadLevelActions()
	{
		Logic::CGameManager::getSingletonPtr()->onLoadingLevel();
	}

	//-------------------------------------------------------
	
	void CServer::onRestartLevelActions()
	{
		AI::CServer::getSingletonPtr()->reset();
	}

	//--------------------------------------------------------

	bool CServer::activateMap() 
	{
		return _map->activate();

	} // activateMap

	//---------------------------------------------------------

	void CServer::deactivateMap() 
	{
		_map->deactivate();

	} // deactivateMap

	//---------------------------------------------------------

	void CServer::tick(unsigned int msecs) 
	{
		// Eliminamos las entidades que se han marcado para ser eliminadas.
		Logic::CEntityFactory::getSingletonPtr()->deleteDefferedEntities();

		_map->tick(msecs);

	} // tick

	//---------------------------------------------------------

	unsigned int CServer::getNumCopies(){
		return Logic::CReplayManager::getSingletonPtr()->getCountCopies();
	}

} // namespace Logic
