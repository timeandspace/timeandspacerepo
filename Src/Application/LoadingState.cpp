//---------------------------------------------------------------------------
// LoadingState.cpp
//--------------------------------------------------------------------------

/**
@file LoadingState.cpp

Contiene la implementaci�n del estado de carga

@see Application::CApplicationState
@see Application::CLoadingState

@author �lvaro Bl�zquez Checa
@date Febrero 2014
*/

#include "LoadingState.h"
#include "Graphics\Server.h"


#include <CEGUI/CEGUI.h>

namespace Application {


	CLoadingState::CLoadingState(CBaseApplication * app) : CApplicationState(app)
	{
	}

	CLoadingState::~CLoadingState()
	{
	}


	bool CLoadingState::init() 
	{
		CApplicationState::init();


		// Cargamos la ventana que muestra el men�
		_loadingWindow = CEGUI::WindowManager::getSingletonPtr()->loadLayoutFromFile("Loading.layout");


		/* ProgressBar 
		progressBar = static_cast<ProgressBar*>(winMgr.getWindow("ProgressBar"));
		progressBar->setProgress(0.25f); // Initial progress of 25%
		progressBar->setStepSize(0.10f); // Calling step() will increase the progress by 10%
		progressBar->step(); // Advance the progress by the size specified in setStepSize()
		progressBar->adjustProgress(-0.05f); // Adjust the progress by a delta value rather than setting a new value through setProgress
		float valueProgressBar = progressBar->getProgress(); // initial 0.25f + step 0.10f - adjustment 0.05f = 0.30f
		*/

		//progressBar = _loadingWindow->getChildElement("ProgressBar");


		return true;

	} // init

	//--------------------------------------------------------

	void CLoadingState::release() 
	{
		CApplicationState::release();

	} // release

	//--------------------------------------------------------

	void CLoadingState::activate() 
	{
		CApplicationState::activate();

		Ogre::ResourceGroupManager::getSingletonPtr()->addResourceGroupListener(this);

		CEGUI::System::getSingletonPtr()->getDefaultGUIContext().setRootWindow(_loadingWindow);
		_loadingWindow->setVisible(true);
		CEGUI::System::getSingletonPtr()->getDefaultGUIContext().getMouseCursor().hide();
		_loadingWindow->activate();
		// �apa al canto para que se pinte (autor: A. P�rez)
		Graphics::CServer::getSingletonPtr()->tick(0.001);
	} // activate

	//--------------------------------------------------------

	void CLoadingState::deactivate() 
	{		
		_loadingWindow->deactivate();
		_loadingWindow->setVisible(false);

		Ogre::ResourceGroupManager::getSingletonPtr()->removeResourceGroupListener(this);

		CApplicationState::deactivate();

	} // deactivate

	//--------------------------------------------------------

	void CLoadingState::tick(unsigned int msecs) 
	{
		CApplicationState::tick(msecs);
	} // tick

	//--------------------------------------------------------

	bool CLoadingState::keyPressed(GUI::TKey key)
	{
		return false;

	} // keyPressed

	//--------------------------------------------------------

	bool CLoadingState::keyReleased(GUI::TKey key)
	{
		return false;
	} // keyPressed


	void CLoadingState::resourceCreated (const Ogre::ResourcePtr &resource)
	{

	}


	void CLoadingState::resourceGroupLoadEnded (const std::string &groupName)
	{
	}


	void CLoadingState::resourceGroupLoadStarted (const std::string &groupName, size_t resourceCount)
	{
	}


	void CLoadingState::resourceGroupPrepareEnded (const std::string &groupName)
	{
	}


	void CLoadingState::resourceGroupPrepareStarted (const std::string &groupName, size_t resourceCount)
	{
	}


	void CLoadingState::resourceGroupScriptingEnded (const std::string &groupName)
	{
	}


	void CLoadingState::resourceGroupScriptingStarted (const std::string &groupName, size_t scriptCount)
	{
	}


	void CLoadingState::resourceLoadEnded (void)
	{
	}


	void CLoadingState::resourceLoadStarted (const Ogre::ResourcePtr &resource)
	{
	}


	void CLoadingState:: resourcePrepareEnded (void)
	{
	}


	void CLoadingState::resourcePrepareStarted (const Ogre::ResourcePtr &resource)
	{
	}


	void CLoadingState::resourceRemove (const Ogre::ResourcePtr &resource)
	{
	}


	void CLoadingState::scriptParseEnded (const std::string &scriptName, bool skipped)
	{
	}


	void CLoadingState::scriptParseStarted (const std::string &scriptName, bool &skipThisScript)
	{
	}


	void CLoadingState::worldGeometryPrepareStageEnded (void)
	{
	}


	void CLoadingState::worldGeometryPrepareStageStarted (const std::string &description)
	{
	}


	void CLoadingState::worldGeometryStageEnded (void)
	{
	}


	void CLoadingState::worldGeometryStageStarted (const std::string &description)
	{
	}
}