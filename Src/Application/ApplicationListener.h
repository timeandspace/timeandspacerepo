//---------------------------------------------------------------------------
// ApplicationListener.h
//---------------------------------------------------------------------------

/**
@file ApplicationListener.h

Contiene la declaración de la interfaz ApplicationListener

@see Application::CBaseApplication
@see Application::CApplicationState

@author Alvaro Blazquez Checa
@date Marzo, 2014
*/

#ifndef __Application_ApplicationListener_H
#define __Application_ApplicationListener_H



namespace Application 
{
	/**
	@remarks 
	
	@ingroup applicationGroup

	@author Alvaro Blazquez Checa
	@date Marzo, 2014
	*/
	class IApplicationListener
	{
	public:

		virtual void OnLevelLoaded() = 0;

		virtual void OnLevelRestarted() = 0;
		

	}; // class ApplicationListener

} // namespace Application

#endif // __Application_BaseApplication_H
