//---------------------------------------------------------------------------
// PauseState.h
//---------------------------------------------------------------------------

/**
@file PauseState.h

Contiene la declaraci�n del estado de pausa.

@see Application::CApplicationState
@see Application::CPauseState

@author Alejandro P�rez Alonso
@date Abril, 2014
*/

#ifndef __Application_PauseState_H
#define __Application_PauseState_H

#include "ApplicationState.h"

// Predeclaraci�n de clases para ahorrar tiempo de compilaci�n
namespace Application 
{
	class CBaseApplication;
}


namespace Application 
{
	class CPauseState : public CApplicationState 
	{
	public:
		/** 
		Constructor de la clase 
		*/
		CPauseState(CBaseApplication *app) : CApplicationState(app)
				{}

		/**
		Funci�n llamada por la aplicaci�n cuando se activa
		el estado.
		*/
		virtual void activate();

		/**
		Funci�n llamada por la aplicaci�n cuando se desactiva
		el estado.
		*/
		virtual void deactivate();
		
		/**
		M�todo que ser� invocado siempre que se termine la pulsaci�n
		de una tecla. Ser� la aplicaci�n qui�n llame a este m�todo 
		cuando el estado est� activo. Esta clase NO se registra en
		el InputManager sino que es la aplicaci�n quien lo hace y 
		delega en los estados.

		@param key C�digo de la tecla pulsada.
		@return true si el evento ha sido procesado. En este caso 
		el gestor no llamar� a otros listeners.
		*/
		virtual bool keyReleased(GUI::TKey key);

		virtual bool buttonReleased(GUI::TJoystickButton button);

		virtual void tick(unsigned int msecs);

		void resumeReleased();
		void restartLevelReleased();
		void menuReleased();
		

	}; // CPauseState

} // namespace Application

#endif //  __Application_PauseState_H
