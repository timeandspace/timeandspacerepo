//---------------------------------------------------------------------------
// LevelLoader.h
//--------------------------------------------------------------------------

/**
@file LevelLoader.h

Maneja el concepto de nivel en el juego, carga los niveles a partir de 
su descripcion grafica, fisica y logica.
Se usa un parseador de ficheros dotScene para crear el mundo grafico estatico

@see Application::CLevelLoader
@see Application::CDotSceneLoader

@author �lvaro Bl�zquez Checa
@date Febrero 2014
*/

#ifndef __Application_LevelLoader_H
#define __Application_LevelLoader_H

#include <string> 
#include <vector>
#include <list>

// Predeclaraci�n de clases para ahorrar tiempo de compilaci�n
namespace Application
{
	class DotSceneLoader;
}

/**
Namespace con todo lo relacionado con la aplicaci�n: clase abstracta, 
estados, etc. Es el coraz�n del juego, el encargado de inicializar
el resto de sistemas necesarios para la ejecuci�n del juego.
(para m�s informaci�n ver @ref applicationGroup).

@author Marco Antonio G�mez Mart�n & David Llans�
@date Julio, 2010
*/
namespace Application
{
	class CLevelLoader 
	{

		friend class CBaseApplication;

	protected:
		CLevelLoader();

		~CLevelLoader();

		bool init();

		void release();

		/**
		M�todo principal de la clase, se usa para cargar un nivel a partir de su descripcion
		en el fichero ./media/levels/levels.txt
		*/
		bool loadLevel(const std::string &);

		bool loadLevel(short i);

		bool unLoadLevel();

		void restartLevel();

		bool isLevel(const std::string&);		

		std::string getName()
		{
			return _levelLoaded->levelName;
		}

	private:

		struct Level {

			std::string levelName;
			std::string logicMapFile;
			std::list<std::string> resourceGroups;
			std::string sceneResourceGroup;
			std::string dotSceneFile;
			std::string physicFile;

			Level() : levelName(""),logicMapFile (""), resourceGroups(), sceneResourceGroup(""), dotSceneFile(""), physicFile("")
			{
			}
		};

		typedef struct Level Level;

		bool parseLevels(const std::string&);

		void parseString(const std::string &str, char delim, std::list<std::string> &list);

		Level* getLevel(const std::string&);

		void loadResourceGroups(const std::list<std::string> &groupsList);

		void unLoadResourceGroups(const std::list<std::string> &groupsList);

		std::vector<Level*> _levelList;

		Level* _levelLoaded;

		DotSceneLoader* _dotSceneLoader;
	};

}
#endif