
#include "LevelLoader.h"
#include "BaseApplication.h"
#include "Logic/Server.h"
#include "Physics/Server.h"
#include "Graphics/Server.h"
#include "Graphics/Scene.h"
#include "AI/Server.h"
#include "DotSceneLoader.h"
#include <OgreConfigFile.h>
#include "Application/ApplicationCommon.h"


namespace Application
{

	CLevelLoader::CLevelLoader() : _levelLoaded (NULL), _dotSceneLoader (NULL)
	{
		
	}

	CLevelLoader::~CLevelLoader()
	{
	}

	bool CLevelLoader::init()
	{
		_dotSceneLoader = new DotSceneLoader();

		return parseLevels(LEVELS_FILE);
	}

	void CLevelLoader::release()
	{
		//unLoadLevel();

		delete _dotSceneLoader;

		for(unsigned short i = 0; i < _levelList.size(); ++i) 
			delete(_levelList[i]);
	}

	bool CLevelLoader::loadLevel(const std::string &levelName)
	{
		Level* levelToload = NULL;

		//Si esta el nivel lo cargamos
		//Si no esta el nivel a cargar no hacemos nada y devolvemos false
		if((levelToload = getLevel(levelName)) != NULL)
		{
			//Cambiamos al estado loading y forzamos el pintado de CGUI
			/*Application::CBaseApplication::getSingletonPtr()->setState("loading");
			Application::CBaseApplication::getSingletonPtr()->changeState();
			Graphics::CServer::getSingletonPtr()->tick(10);*/
		
			//Si hay un nivel cargado destruimos la escena logica, grafica y fisica
			if(_levelLoaded)
			{
				unLoadLevel();
			}
		
			//Se cargan los grupos de recursos a memoria
			//loadResourceGroups(levelToload->resourceGroups); 

			//Se crea la escena fisica
			Physics::CServer::getSingletonPtr()->createScene();

			//Se crea el mundo fisico
			Physics::CServer::getSingletonPtr()->createWorldFromFile(levelToload->physicFile);

			//Se crea la escena grafica
			Graphics::CScene* graphicScene = Graphics::CServer::getSingletonPtr()->createScene(levelToload->levelName);

			/*AI::CServer::getSingletonPtr()->Release();

			AI::CServer::getSingletonPtr()->Init();*/
			AI::CServer::getSingletonPtr()->reset();
			
			//Creamos el mundo a partir del fichero dotScene
			_dotSceneLoader->parseDotScene(levelToload->dotSceneFile, levelToload->sceneResourceGroup,graphicScene->getSceneMgr());

			// Cargamos el nivel a partir del nombre del mapa. 
			if (!Logic::CServer::getSingletonPtr()->loadLevel(levelToload->logicMapFile, graphicScene))
				return false;

			//Application::CBaseApplication::getSingletonPtr()->setState("game");

			_levelLoaded = levelToload;
			
			return true;
		}
		else return false;
	}

	void CLevelLoader::restartLevel()
	{
		if(_levelLoaded)
		{
			// Cargamos el nivel a partir del nombre del mapa. 
			Logic::CServer::getSingletonPtr()->restartLevel(_levelLoaded->logicMapFile, Graphics::CServer::getSingletonPtr()->getActiveScene());	

		}
	}

	bool CLevelLoader::unLoadLevel()
	{
		//Si hay un nivel cargado destruimos la escena logica y fisica
		if(_levelLoaded)
		{
			Logic::CServer::getSingletonPtr()->unLoadLevel();
			Physics::CServer::getSingletonPtr()->destroyScene();
			unLoadResourceGroups(_levelLoaded->resourceGroups);
			Graphics::CServer::getSingletonPtr()->removeScene(_levelLoaded->levelName);
			_levelLoaded = NULL;
			return true;
		}
		return false;
	}


	bool CLevelLoader::isLevel(const std::string &levelName)
	{
		bool found = false;

		for(unsigned short i = 0; i < _levelList.size() && !found; ++i)
		{
			if(_levelList[i]->levelName == levelName)
				found = true;
		}
		return found;
	}


	CLevelLoader::Level* CLevelLoader::getLevel(const std::string &levelName)
	{
		for(unsigned short i = 0; i < _levelList.size(); ++i)
		{
			if(_levelList[i]->levelName == levelName)
			{
				return _levelList[i];
			}
		}
		return NULL;
	}


	bool CLevelLoader::parseLevels(const std::string &levelsFile)
	{
		Ogre::ConfigFile cf;
		cf.load(levelsFile);

		Ogre::ConfigFile::SectionIterator itSection = cf.getSectionIterator();

		std::string sLevelName,sKey, sValue;
		while(itSection.hasMoreElements()) 
		{
			if((sLevelName = itSection.peekNextKey()) != "")
			{
				Level* level = new Level();
				level->levelName = sLevelName;
				Ogre::ConfigFile::SettingsMultiMap *mapSettings = itSection.getNext();
				Ogre::ConfigFile::SettingsMultiMap::const_iterator itSetting = mapSettings->begin();
				while(itSetting != mapSettings->end()) 
				{
					sKey = itSetting->first;
					sValue = itSetting->second;

					if(sKey == "ResourceGroups")
						parseString(sValue,',',level->resourceGroups);
					else if(sKey == "LogicMapFile")
						level->logicMapFile = sValue;
					else if(sKey == "DotSceneFile")
						level->dotSceneFile = sValue;
					else if(sKey == "SceneResourceGroup")
						level->sceneResourceGroup = sValue;
					else if(sKey == "PhysicFile")
						level->physicFile = sValue;
					++itSetting;
				}
				_levelList.push_back(level);
			}
			else
				itSection.getNext();
		}
		return _levelList.size() != 0;
	}


	void CLevelLoader::parseString(const std::string &str, char delim, std::list<std::string> &list)
	{
		std::stringstream ss(str);
		std::string item;
		while (std::getline(ss, item, delim)) 
		{
			list.push_back(item);
		}
	}


	void CLevelLoader::loadResourceGroups(const std::list<std::string> &groupsList)
	{
		std::list<std::string>::const_iterator itr = groupsList.begin();
		std::list<std::string>::const_iterator end = groupsList.end();
		Graphics::CServer *server = Graphics::CServer::getSingletonPtr();
		for( ; itr != end; ++itr)
		{
			server->loadResourceGroup((*itr));
		}

	}


	void CLevelLoader::unLoadResourceGroups(const std::list<std::string> &groupsList)
	{
		std::list<std::string>::const_iterator itr = groupsList.begin();
		std::list<std::string>::const_iterator end = groupsList.end();
		Graphics::CServer *server = Graphics::CServer::getSingletonPtr();
		for( ; itr != end; ++itr)
		{
			server->unLoadResourceGroup((*itr));
		}
	}

}