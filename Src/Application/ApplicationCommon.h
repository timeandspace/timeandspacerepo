//DIRECTORIES

#ifndef __Application_ApplicationCommon_H
#define __Application_ApplicationCommon_H

/**
@file ApplicationCommon

define rutas y ficheros de configuracion

@author Alvaro Blazquez Checa
@date Julio 2014
*/

#ifdef _DEBUG
#define LEVELS_FILE "../media/Levels/maps/levels.txt"
#define SOUNDS_PATH "../media/sounds/"
#define MUSIC_PATH "../media/music/"
#define BTLIBRARYPATH "../media/behaviourTrees/BTLibrary"
#define SIMPLEBTLIBRARYPATH "../media/behaviourTrees/BTLibrary/Simple"
#define SIMPLE2BTLIBRARYPATH "../media/behaviourTrees/BTLibrary/Simple2"
#define BLUEPRINTS_FILE_PATH "../media/levels/maps/"
#else
#define LEVELS_FILE "../media/Levels/maps/levels.txt"
#define SOUNDS_PATH "../media/sounds/"
#define MUSIC_PATH "../media/music/"
#define BTLIBRARYPATH "../media/behaviourTrees/BTLibrary"
#define SIMPLEBTLIBRARYPATH "../media/behaviourTrees/BTLibrary/Simple"
#define SIMPLE2BTLIBRARYPATH "../media/behaviourTrees/BTLibrary/Simple2"
#define BLUEPRINTS_FILE_PATH "../media/levels/maps/"
#endif





#ifdef _DEBUG
#define OGRE_CFG "ogre_d.cfg"
#define OGRE_PLUGINS "plugins_d.cfg"
#define OGRE_LOG "Ogre_d.log"
#else
#define OGRE_CFG "ogre.cfg"
#define OGRE_PLUGINS "plugins.cfg"
#define OGRE_LOG "Ogre.log"
#endif


#endif