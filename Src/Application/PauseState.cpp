//---------------------------------------------------------------------------
// PauseState.cpp
//---------------------------------------------------------------------------

/**
@file PauseState.cpp

Contiene la implementación del estado de pausa.

@see Application::CApplicationState
@see Application::CPauseState

@author Alejandro Pérez Alonso
@date Abril, 2014
*/

#include "PauseState.h"

//#include "Graphics/Server.h"
//#include "GUI/Server.h"
#include "GUI/HudManager.h"

namespace Application {

	void CPauseState::activate() 
	{
		GUI::CHudManager::getSingletonPtr()->setState(this);
		CApplicationState::activate();

	} // activate

	//--------------------------------------------------------

	void CPauseState::deactivate() 
	{		
		CApplicationState::deactivate();

	} // deactivate

	//--------------------------------------------------------

	bool CPauseState::keyReleased(GUI::TKey key)
	{
		switch(key.keyId)
		{
		case GUI::Key::ESCAPE:
		case GUI::Key::P:
			_app->setState("game",POP_STATE);
			break;
		default:
			return false;
		}
		return true;

	} // keyReleased

	//--------------------------------------------------------

	bool CPauseState::buttonReleased(GUI::TJoystickButton button)
	{
		switch(button)
		{
		case GUI::TJoystickButton::BUTTON_B:
		case GUI::TJoystickButton::BUTTON_START:
			_app->setState("game",POP_STATE);
			break;
		default:
			return false;
		}
		return true;
	}

	//--------------------------------------------------------

	void CPauseState::tick(unsigned int msecs)
	{
		CApplicationState::tick(msecs);

	}

	//--------------------------------------------------------

	void CPauseState::resumeReleased()
	{
		_app->setState("game",POP_STATE);
	}

	//--------------------------------------------------------

	void CPauseState::restartLevelReleased()
	{
		//_app->restartLevel();
		_app->setState("game",POP_STATE);
		_app->restartLevel(true);
	}

	//--------------------------------------------------------
	void CPauseState::menuReleased()
	{
		_app->setState("menu");
	}

} // namespace Application
