//---------------------------------------------------------------------------
// LoadingState.h
//--------------------------------------------------------------------------

/**
@file LoadingState.h
@see Application::CApplicationState
@see Application::CLoadingState

@author �lvaro Bl�zquez Checa
@date Febrero 2014
*/


#ifndef __Application_LoadingState_H
#define __Application_LoadingState_H

#include "ApplicationState.h"
#include <OgreResourceGroupManager.h>

// Predeclaraci�n de clases para ahorrar tiempo de compilaci�n
namespace CEGUI
{
	class Window;
}

namespace Application
{
	class CLoadingState : public CApplicationState , Ogre::ResourceGroupListener
	{
	public:
		/**
		Constructor de la clase
		*/
		CLoadingState(CBaseApplication * app);

		/**
		Destructor de la clase
		*/
		virtual ~CLoadingState();

		/**
		*/
		virtual bool init();

		/**
		*/
		virtual void release();
		
		/**
		*/
		virtual void activate();

		/**
		*/
		virtual void deactivate();

		/**
		*/
		virtual void tick(unsigned int msecs);

		/**
		*/
		virtual bool keyPressed(GUI::TKey key);

		/**
		*/
		virtual bool keyReleased(GUI::TKey key);


		virtual void 	resourceCreated (const Ogre::ResourcePtr &resource);
 	
 
		virtual void 	resourceGroupLoadEnded (const std::string &groupName);
 		
 
		virtual void 	resourceGroupLoadStarted (const std::string &groupName, size_t resourceCount);
 			
 
		virtual void 	resourceGroupPrepareEnded (const std::string &groupName);
 			
 
		virtual void 	resourceGroupPrepareStarted (const std::string &groupName, size_t resourceCount);
 			

		virtual void 	resourceGroupScriptingEnded (const std::string &groupName);
 			
 
		virtual void 	resourceGroupScriptingStarted (const std::string &groupName, size_t scriptCount);
 
 
		virtual void 	resourceLoadEnded (void);
	
 
		virtual void 	resourceLoadStarted (const Ogre::ResourcePtr &resource);
 
 
		virtual void 	resourcePrepareEnded (void);
 		
 
		virtual void 	resourcePrepareStarted (const Ogre::ResourcePtr &resource);
 	
 
		virtual void 	resourceRemove (const Ogre::ResourcePtr &resource);
 	
 
		virtual void 	scriptParseEnded (const std::string &scriptName, bool skipped);
 	
 
		virtual void 	scriptParseStarted (const std::string &scriptName, bool &skipThisScript);
 			
 
		virtual void 	worldGeometryPrepareStageEnded (void);
 			
 
		virtual void 	worldGeometryPrepareStageStarted (const std::string &description); 			
 

		virtual void 	worldGeometryStageEnded (void);
 			
 
		virtual void 	worldGeometryStageStarted (const std::string &description);

	private:

		/**
		Ventana CEGUI que muestra el estado de carga.
		*/
		CEGUI::Window* _loadingWindow;

		/* ProgressBar */
		//CEGUI::ProgressBar* progressBar;



	};
}

#endif