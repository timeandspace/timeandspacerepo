//---------------------------------------------------------------------------
// MenuState.cpp
//---------------------------------------------------------------------------

/**
@file MenuState.cpp

Contiene la implementaci�n del estado de men�.

@see Application::CApplicationState
@see Application::CMenuState

@author David Llans�
@date Agosto, 2010
*/

#include "MenuState.h"
#include "LevelLoader.h"
#include "GUI/HudManager.h"
#include "Graphics/Server.h"
#include "Graphics/Scene.h"
#include "Graphics/SceneCreator.h"
#include "Sound/Server.h"
#include "Sound/MusicPlayer.h"




namespace Application {


	CMenuState::~CMenuState() 
	{
		
	} // ~CMenuState

	//--------------------------------------------------------

	bool CMenuState::init() 
	{
		CApplicationState::init();

		configMenuGraphicScene();

		Sonido::CServer::getSingletonPtr()->getMusicPlayer()->loadPlayListFromFile("TimeAndSpace21Junio.mp3");

		return true;

	} // init

	//--------------------------------------------------------

	void CMenuState::release() 
	{
		CApplicationState::release();

	} // release
	

	//--------------------------------------------------------

	void CMenuState::configMenuGraphicScene()
	{
		_scene = Graphics::CSceneCreator::createSceneForMenu();
	}

	//--------------------------------------------------------

	void CMenuState::activate() 
	{
		CApplicationState::activate();

		Graphics::CServer::getSingletonPtr()->setScene(_scene);

		Sonido::CServer::getSingletonPtr()->getMusicPlayer()->playSong("TimeAndSpace21Junio.mp3");

		GUI::CHudManager::getSingletonPtr()->setState(this);

	} // activate

	//--------------------------------------------------------

	void CMenuState::deactivate() 
	{		
		Sonido::CServer::getSingletonPtr()->getMusicPlayer()->stopMusic();

		CApplicationState::deactivate();

	} // deactivate

	//--------------------------------------------------------

	void CMenuState::tick(unsigned int msecs) 
	{
		CApplicationState::tick(msecs);
	} // tick

	//--------------------------------------------------------

	bool CMenuState::keyPressed(GUI::TKey key)
	{
		return false;

	} // keyPressed

	//--------------------------------------------------------

	bool CMenuState::keyReleased(GUI::TKey key)
	{
		switch(key.keyId)
		{
		case GUI::Key::ESCAPE:
			_app->exitRequest();
			break;
		case GUI::Key::RETURN:
			_app->loadLevel("Level1");
			break;
		case GUI::Key::NUMBER1:
			_app->loadLevel("Level1");
			break;
		case GUI::Key::NUMBER2:
			_app->loadLevel("Level5");
			break;
		case GUI::Key::NUMBER3:
			_app->loadLevel("Level4");
			break;
		case GUI::Key::NUMBER4:
			_app->loadLevel("Level2");
			break;
		case GUI::Key::NUMBER5:
			_app->loadLevel("Level3");
			break;
		case GUI::Key::NUMBER6:
			_app->loadLevel("Level6");
			break;
		case GUI::Key::NUMBER7:
			_app->loadLevel("Level7");
			break;
	/*	case GUI::Key::NUMBER8:
			_app->loadLevel("Level8");
			break;*/
		default:
			return false;
		}
		return true;

	} // keyReleased

	//--------------------------------------------------------

	bool CMenuState::mouseMoved(const GUI::CMouseState &mouseState)
	{
		return false;

	} // mouseMoved

	//--------------------------------------------------------

	bool CMenuState::mousePressed(const GUI::CMouseState &mouseState)
	{
		return false;

	} // mousePressed

	//--------------------------------------------------------


	bool CMenuState::mouseReleased(const GUI::CMouseState &mouseState)
	{
		return false;

	} // mouseReleased

	//--------------------------------------------------------

	bool CMenuState::buttonPressed(int button)
	{
		switch (button)
		{
		case GUI::TJoystickButton::BUTTON_A:
			_app->loadLevel("Level1");
			break;

		case GUI::TJoystickButton::BUTTON_B:
			_app->exitRequest();
			break;

		case GUI::TJoystickButton::BUTTON_START:
			_app->loadLevel("Level1");
			break;

		default:
			return false;
		}

		return true;
	}

	//--------------------------------------------------------

	void CMenuState::startReleased()
	{
		_app->loadLevel("Level1");

	} // startReleased

	//--------------------------------------------------------

	bool CMenuState::optionsReleased()
	{
		return true;
	}

	//--------------------------------------------------------

	bool CMenuState::exitReleased()
	{
		_app->exitRequest();
		return true;

	} // exitReleased

	//--------------------------------------------------------

} // namespace Application
