//---------------------------------------------------------------------------
// GameState.cpp
//---------------------------------------------------------------------------

/**
@file GameState.cpp

Contiene la implementaci�n del estado de juego.

@see Application::CApplicationState
@see Application::CGameState

@author David Llans�
@date Agosto, 2010
*/

#include "GameState.h"

#include "Logic/Server.h"
#include "Logic/Entity/Entity.h"
#include "Logic/Maps/EntityFactory.h"
#include "Logic/Maps/Map.h"
#include "LevelLoader.h"

#include "GUI/Server.h"
#include "GUI/PlayerController.h"
#include "GUI/HudManager.h"
#include <CEGUI/System.h>

#include "Graphics/Server.h"

#include "Physics/Server.h"

#include "AI/Server.h"

#include "Logic/Managers/Player Manager/PlayerManager.h"


namespace Application {

	CGameState::~CGameState()
	{
		// Dejamos de ser listener de BaseApplication
		Application::CBaseApplication::getSingletonPtr()->removeApplicationListener((IApplicationListener*)this);

	} // ~CGameState

	//--------------------------------------------------------

	bool CGameState::init() 
	{
		CApplicationState::init();



		/*Logic::CPlayerManager::getSingletonPtr()->
		addListener(CHudManager::getSingletonPtr());*/

		// Nos registramos como listener para que que se nos notifique si se ha cambiado
		// de nivel.
		Application::CBaseApplication::getSingletonPtr()->addApplicationListener((IApplicationListener*)this);

		return true;

	} // init

	//--------------------------------------------------------

	void CGameState::release() 
	{
		CApplicationState::release();

		Logic::CPlayerManager::Release();

		_app->unLoadLevel();
	} // release

	//--------------------------------------------------------

	void CGameState::activate() 
	{
		CApplicationState::activate();

		GUI::CHudManager::getSingletonPtr()->setState(this);


		_physicsServer = Physics::CServer::getSingletonPtr();
		_logicServer = Logic::CServer::getSingletonPtr();
		_aiServer = AI::CServer::getSingletonPtr();
		_graphicsServer = Graphics::CServer::getSingletonPtr();
		// Activamos el mapa que ha sido cargado para la partida.
		_logicServer->activateMap();

		Logic::CPlayerManager::getSingletonPtr()->clearAll();

		// Queremos que el GUI maneje al jugador.
		GUI::CServer::getSingletonPtr()->getPlayerController()->activate();
	} // activate

	//--------------------------------------------------------

	void CGameState::deactivate() 
	{
		// Desactivamos la clase que procesa eventos de entrada para 
		// controlar al jugador.
		GUI::CServer::getSingletonPtr()->getPlayerController()->deactivate();

		// Desactivamos el mapa de la partida.
		Logic::CServer::getSingletonPtr()->deactivateMap();

		CApplicationState::deactivate();

	} // deactivate

	//-------------------------------------------------------

	void CGameState::pause()
	{
		// Desactiva la escena
		_graphicsServer->pauseScene();
		// Desactivamos Player Controller
		GUI::CServer::getSingletonPtr()->deactivatePlayerController();
	}

	//-------------------------------------------------------

	void CGameState::resume()
	{
		// Activa la escena
		_graphicsServer->resumeScene();

		GUI::CHudManager::getSingletonPtr()->setState(this);
		// Activamos Player Controller
		GUI::CServer::getSingletonPtr()->activatePlayerController();
	}
	//-------------------------------------------------------

	//bool CGameState::changeScene(const std::string sceneName)
	//{
	//	//_app->setState("menu");
	//	if (!Logic::CServer::getSingletonPtr()->loadLevel(sceneName))
	//		return false;
	//	//_app->setState("game");
	//	return true;
	//}

	//-------------------------------------------------------

	void CGameState::tick(unsigned int msecs) 
	{
		CApplicationState::tick(msecs);

		//CHudManager::getSingletonPtr()->tick(msecs/*HUD_TICK*/);

		/***
		Fixed update, todo lo que est� dentro del bucle se ejecuta a un ritmo
		constante.
		*/
		_tickLogico += msecs;
		int numVeces = 0;
		while ((_tickLogico >= LOGIC_TICK) && (numVeces < 10)) 
		{
			// Modo turbo: se ejecuta por cada tick 2 veces
			for (unsigned int i = 0; i < _app->getMultiplier(); ++i)
			{
				// Simulacion de la f�sica
				_physicsServer->tick(LOGIC_TICK);
				// Simulaci�n de la l�gica
				_logicServer->tick(LOGIC_TICK);
				// Realizamos la percepci�n
				_aiServer->tick(LOGIC_TICK);
			}

			_tickLogico -= LOGIC_TICK;
			++ numVeces;
		}

	} // tick

	//-------------------------------------------------------

	void CGameState::OnLevelLoaded()
	{
		_app->setMultiplier(1);
	}

	//-------------------------------------------------------

	void CGameState::OnLevelRestarted()
	{ 
		_app->setMultiplier(1);
	}

	//--------------------------------------------------------

	bool CGameState::keyPressed(GUI::TKey key)
	{
		return false;

	} // keyPressed

	//--------------------------------------------------------

	bool CGameState::keyReleased(GUI::TKey key)
	{
		switch(key.keyId)
		{
		case GUI::Key::ESCAPE:
#ifdef _DEBUG
			_app->setState("menu");
#else
			_app->setState("pause",PUSH_STATE);
#endif
			break;
		case GUI::Key::P:
			_app->setState("pause",PUSH_STATE);
			break;
			//case GUI::Key::T:
			//	_app->setMultiplier( (_app->getMultiplier() % 2) + 1 );  // _multiplier igual a 1 o 2
		default:
			return false;
		}
		return true;

	} // keyReleased

	//--------------------------------------------------------

	bool CGameState::buttonReleased(GUI::TJoystickButton button)
	{
		switch(button)
		{
		case GUI::TJoystickButton::BUTTON_START:
			_app->setState("pause",PUSH_STATE);
			break;
		default:
			return false;
		}
		return true;
	}

	//--------------------------------------------------------

	bool CGameState::mouseMoved(const GUI::CMouseState &mouseState)
	{
		return false;

	} // mouseMoved

	//--------------------------------------------------------

	bool CGameState::mousePressed(const GUI::CMouseState &mouseState)
	{
		return false;

	} // mousePressed

	//--------------------------------------------------------

	bool CGameState::mouseReleased(const GUI::CMouseState &mouseState)
	{
		return false;

	} // mouseReleased

	//--------------------------------------------------------

} // namespace Application
