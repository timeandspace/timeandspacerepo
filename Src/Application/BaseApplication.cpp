//---------------------------------------------------------------------------
// BaseApplication.cpp
//---------------------------------------------------------------------------

/**
@file BaseApplication.cpp

Contiene la implementaci�n de la clase aplicacion, que maneja la ejecuci�n
de todo el juego.

@see Application::CBaseApplication
@see Application::CApplicationState

@author Marco Antonio G�mez Mart�n & David Llans�
@date Julio, 2010
*/

#include "BaseApplication.h"
#include "ApplicationState.h"
#include "Clock.h"
#include "LevelLoader.h"
#include "ApplicationListener.h"

#include <assert.h>

namespace Application {

	CBaseApplication *CBaseApplication::_instance = 0;

	CBaseApplication::CBaseApplication() : 
		_initialized(false),
		/*_currentState(0),*/
		_nextState(0),
		_nextMode(PUSH_STATE),
		_exit(false),
		_clock(0),
		_levelLoader(0),
		_levelLoadingRequest(false),
		_levelRestartingRequest(false),
		_levelToLoad(""),
		_multiplier(1)

	{
		assert(!_instance && "No puede crearse m�s de una aplicaci�n");
		_instance = this;

	} // CBaseApplication

	//--------------------------------------------------------

	CBaseApplication::~CBaseApplication()
	{
		delete _levelLoader;
		_instance = 0;

	} // ~CBaseApplication

	//--------------------------------------------------------

	bool CBaseApplication::init() 
	{
		assert(!_initialized && "La aplicaci�n ya est� inicializada");
		_levelLoader = new CLevelLoader();
		_levelLoader->init();
		_initialized = true;

		return true;

	} // init

	//--------------------------------------------------------

	void CBaseApplication::release()
	{
		assert(_initialized && "La aplicaci�n no est� inicializada");

		// Desactivamos y eliminamos todos los estados.
		releaseAllStates();

		_initialized = false;

		_levelLoader->release();

	} // release

	//--------------------------------------------------------

	void CBaseApplication::releaseAllStates()
	{
		// Desactivamos el estado actual
		/*if (!_currentStates.empty()) 
		{
		_currentStates.top()->deactivate();
		}*/

		// Eliminamos los estados

		TStateTable::const_iterator it, end;

		for (it = _states.begin(), end = _states.end(); 
			it != end; ++it) 
		{
			it->second->release();
			delete it->second;
		}
		_states.clear();

	} // releaseAllStates

	//--------------------------------------------------------

	bool CBaseApplication::addState(const std::string &name,
		CApplicationState *newState) 
	{
		TStateTable::const_iterator it;

#ifdef _DEBUG
		// Comprobamos que no existe un estado con ese nombre.
		// Otra posibilidad es no hacerlo en Debug, sino siempre,
		// y, en caso de que ya exista, eliminarlo (pues la aplicaci�n
		// acepta la responsabilidad de borrar los estados que contiene).
		// Sin embargo, en ese caso, habr�a que comprobar que no es
		// el estado actual, ni el estado siguiente al que se espera ir...
		it = _states.find(name);
		assert(it == _states.end());
#endif
		_states[name] = newState;
		return newState->init();

	} // addState

	//--------------------------------------------------------

	bool CBaseApplication::setState(const std::string &name, int mode)
	{
		// Buscamos el estado.
		TStateTable::const_iterator it;

		it = _states.find(name);

		// Si no hay ning�n estado con ese nombre, no hacemos nada
		if (it == _states.end())
			return false;

		_nextState = it->second;

		if (mode == SET_STATE) // Cambio de contexto
		{
			while (!_currentStates.empty())
				popState();
			_nextMode = PUSH_STATE;
		}
		else if (mode != PUSH_STATE && mode != POP_STATE)
			_nextMode = PUSH_STATE;
		else
			_nextMode = mode;
		return true;

	} // setState

	//--------------------------------------------------------

	void CBaseApplication::run() 
	{
		assert(_clock && "Asegurate de haber creado un reloj en el init de la clase de tu aplicacion!");

		// Actualizamos una primera vez el tiempo, antes de
		// empezar, para que el primer frame tenga un tiempo
		// de frame razonable.
		_clock->updateTime();

		// Ejecuci�n del bucle principal. Simplemente miramos si
		// tenemos que hacer una transici�n de estado, y si no hay que
		// hacerla, ejecutamos la vuelta
		while (!exitRequested()) 
		{
			if (_currentStates.empty())
				changeState();
			else if (_nextState && (_nextState != _currentStates.top()))
				changeState();

			_clock->updateTime();

			/* Si retornamos de un break point o de una parada superior a 1 seg */
#ifdef _DEBUG
			if (_clock->getLastFrameDuration() > 1000)
				_clock->setLastFrameDuration(50);
#endif

			tick(_clock->getLastFrameDuration());
		}

	} // run

	//--------------------------------------------------------

	unsigned int CBaseApplication::getAppTime() 
	{
		return _clock->getTime();

	} // getAppTime

	//--------------------------------------------------------

	void CBaseApplication::changeState() 
	{
		// Avisamos al estado actual que le vamos a eliminar
		// Avisamos al estado actual que le vamos a eliminar
		if (_nextMode == PUSH_STATE)
		{
			pushState(_nextState);
		}
		else
		{
			if (!_currentStates.empty()) 
				popState();
		}

		_nextState = 0;

	} // changeState

	//--------------------------------------------------------

	void CBaseApplication::tick(unsigned int msecs) 
	{
		// Aparentemente esta funci�n es sencilla. Aqu� se pueden
		// a�adir otras llamadas que sean comunes a todos los estados
		// de todas las aplicaciones.
		// El m�todo es virtual. Si para una aplicaci�n concreta, se
		// identifican cosas comunes a todos los estados, se pueden
		// a�adir en la implementaci�n del m�todo de esa aplicaci�n.

		if(_levelLoadingRequest)
			loadLevel();

		if(_levelRestartingRequest)
		{
			_levelRestartingRequest = false;
			_levelLoader->restartLevel();
			notifyLevelRestarted();
		}


		if (!_currentStates.empty()) 
			_currentStates.top()->tick(msecs);



	} // tick

	//--------------------------------------------------------

	void CBaseApplication::pushState(CApplicationState *newState) 
	{
		if (!_currentStates.empty())
			_currentStates.top()->pause();
		_currentStates.push(newState);
		_currentStates.top()->activate();
	}

	//--------------------------------------------------------

	void CBaseApplication::popState() 
	{
		if (!_currentStates.empty()) 
		{
			_currentStates.top()->deactivate();
			_currentStates.pop();
			if (!_currentStates.empty())
				_currentStates.top()->resume();
		}
	}

	//--------------------------------------------------------

	bool CBaseApplication::keyPressed(GUI::TKey key)
	{
		// Avisamos al estado actual de la pulsaci�n.
		if (!_currentStates.empty())
			return _currentStates.top()->keyPressed(key);

		return false;

	} // keyPressed

	//--------------------------------------------------------

	bool CBaseApplication::keyReleased(GUI::TKey key)
	{
		// Avisamos al estado actual del fin de la pulsaci�n.
		if (!_currentStates.empty())
			return _currentStates.top()->keyReleased(key);

		return false;

	} // keyReleased

	//--------------------------------------------------------

	bool CBaseApplication::mouseMoved(const GUI::CMouseState &mouseState)
	{
		// Avisamos al estado actual del movimiento.
		if (!_currentStates.empty())
			return _currentStates.top()->mouseMoved(mouseState);

		return false;

	} // mouseMoved

	//--------------------------------------------------------

	bool CBaseApplication::mousePressed(const GUI::CMouseState &mouseState)
	{
		// Avisamos al estado actual de la pulsaci�n.
		if (!_currentStates.empty())
			return _currentStates.top()->mousePressed(mouseState);

		return false;

	} // mousePressed

	//--------------------------------------------------------

	bool CBaseApplication::mouseReleased(const GUI::CMouseState &mouseState)
	{
		// Avisamos al estado actual del fin de la pulsaci�n.
		if (!_currentStates.empty())
			return _currentStates.top()->mouseReleased(mouseState);

		return false;

	} // mouseReleased

	//--------------------------------------------------------

	bool CBaseApplication::buttonPressed(GUI::TJoystickButton button)
	{
		if (!_currentStates.empty())
			return _currentStates.top()->buttonPressed(button);

		return false;

	} // buttonPressed

	//--------------------------------------------------------

	bool CBaseApplication::buttonReleased(GUI::TJoystickButton button)
	{
		if (!_currentStates.empty())
			return _currentStates.top()->buttonReleased(button);

		return false;

	} // buttonReleased

	//--------------------------------------------------------

	bool CBaseApplication::axisMoved()
	{
		if (!_currentStates.empty())
			return _currentStates.top()->axisMoved();

		return false;

	} // axisMoved

	//--------------------------------------------------------

	bool CBaseApplication::povMoved(int direction)
	{
		if (!_currentStates.empty())
			return _currentStates.top()->povMoved(direction);

		return false;

	} // povMoved

	//--------------------------------------------------------

	bool CBaseApplication::loadLevel(const std::string& levelToLoad)
	{
		if(_levelLoader->isLevel(levelToLoad))
		{
			setState("loading");
			_levelLoadingRequest = true;
			_levelToLoad = levelToLoad;
			return true;
		}
		return false;

	} // loadLevel

	//--------------------------------------------------------

	bool CBaseApplication::loadLevel()
	{
		bool loaded = _levelLoader->loadLevel(_levelToLoad);
		setState("game");
		_levelLoadingRequest = false;
		_levelToLoad = "";
		notifyLevelLoaded();
		return loaded;
	}

	//--------------------------------------------------------

	bool CBaseApplication::restartLevel(bool complete)
	{
		if (! complete)
			_levelRestartingRequest = true;
		else
		{
			loadLevel(_levelLoader->getName());
		}
		return true;
	}

	//--------------------------------------------------------

	void CBaseApplication::unLoadLevel()
	{
		_levelLoader->unLoadLevel();
	}

	//--------------------------------------------------------

	void CBaseApplication::notifyLevelLoaded()
	{
		for(unsigned int i=0; i<_applicationListeners.size(); ++i)
		{
			_applicationListeners[i]->OnLevelLoaded();
		}
	}

	//--------------------------------------------------------

	void CBaseApplication::notifyLevelRestarted()
	{
		for(unsigned int i=0; i<_applicationListeners.size(); ++i)
		{
			_applicationListeners[i]->OnLevelRestarted();
		}
	}

	//--------------------------------------------------------

	void CBaseApplication::addApplicationListener(IApplicationListener* listener)
	{
		_applicationListeners.push_back(listener);
	}

	//-------------------------------------------------------

	void  CBaseApplication::removeApplicationListener(IApplicationListener* listener)
	{
		int index = -1;
		for(unsigned int i=0; i<_applicationListeners.size(); ++i)
		{
			if(_applicationListeners[i] == listener)
				index = i;
		}
		if(index >= 0)
			_applicationListeners.erase(_applicationListeners.begin() + index);
	}

	//--------------------------------------------------------

} // namespace Application
