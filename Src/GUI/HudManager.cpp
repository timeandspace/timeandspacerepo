/**
@file Entity.cpp
*/

#include "HudManager.h"

#include <CEGUI/System.h>
#include <CEGUI/WindowManager.h>
#include <CEGUI/Window.h>
#include <CEGUI/EventSet.h>


#include "Logic/Maps/EntityFactory.h"
#include "Logic/Entity/Entity.h"
#include "Map/MapEntity.h"
#include "Logic/Server.h"
#include "Logic/Maps/Map.h"
#include "Application/BaseApplication.h"
#include "Logic/Managers/Player Manager/PlayerListener.h"
#include "Logic/Managers/Player Manager/PlayerManager.h"


#include "GUI/Server.h"
#include "GUI/PlayerController.h"

#include "Application/MenuState.h"
#include "Application/GameState.h"
#include "Application/PauseState.h"

#include <CEGUI/ScriptModules/Lua/ScriptModule.h>
#include <tolua++.h>
#include <lstate.h>

#include "LuaInterface.hpp"
#include "Sound/Server.h"
#include "Sound/MusicPlayer.h"

#include "Logic/Server.h"
#include "Logic/Entity/Entity.h"
#include "Logic/Maps/Map.h"
#include "Logic/Entity/Components/MusicPlayer.h"
#include "Logic/Entity/Components/AvatarController.h"

#define	COPIAS_EVENT "copias"
#define	CROSSHAIR_EVENT "crosshair"
#define	DAMAGE_EVENT "damage"
#define	TIMECHARACTER_EVENT "timeEvent"
#define	SPACECHARACTER_EVENT "spaceEvent"
#define	CINEMAMODE_EVENT "cinemaEvent"

namespace GUI
{
	CHudManager* CHudManager::_instance = 0;

	//--------------------------------------------------------

	CHudManager::CHudManager()  : _eventSet(0), _currentState(0)
	{
		_instance = this;
	}

	//--------------------------------------------------------

	CHudManager::~CHudManager()
	{
		_instance = NULL;
	}
	
	//--------------------------------------------------------

	CHudManager* CHudManager::getSingletonPtr()
	{ 
		return _instance; 
	}

	//--------------------------------------------------------

	bool CHudManager::Init()
	{
		assert(!_instance && "Segunda inicialización de Logic::CHudManager no permitida!");

		new CHudManager();

		if(!_instance->open())
		{
			Release();
			return false;
		}

		return true;
	}

	//--------------------------------------------------------

	bool CHudManager::Release()
	{
		assert(_instance && "Logic::CHudManager no inicializado!");

		if(_instance)
		{
			_instance->close();
			delete _instance;
		}

		return true;
	}

	//--------------------------------------------------------

	bool CHudManager::open()
	{
		Application::CBaseApplication::getSingletonPtr()->addApplicationListener(this);

		CEGUI::LuaScriptModule* script_module = (CEGUI::LuaScriptModule*)CEGUI::System::getSingletonPtr()->getScriptingModule();
		lua_State* luaState = script_module->getLuaState();
		
		tolua_toLuaPackage_open(luaState);

		Logic::CPlayerManager::getSingletonPtr()->addListener(this);

		_eventSet = new CEGUI::EventSet();

		_eventSet->addEvent(COPIAS_EVENT);
		_eventSet->addEvent(CROSSHAIR_EVENT);
		_eventSet->addEvent(TIMECHARACTER_EVENT);
		_eventSet->addEvent(SPACECHARACTER_EVENT);
		_eventSet->addEvent(CINEMAMODE_EVENT);

		return true;
	}

	//--------------------------------------------------------

	void CHudManager::close()
	{
		Application::CBaseApplication::getSingletonPtr()->removeApplicationListener(this);

		delete _eventSet;
	}

	//--------------------------------------------------------

	void CHudManager::setState(Application::CApplicationState* state)
	{
		_currentState = state; 

		//TODO externalizar nombre de los scripts

		if(dynamic_cast<Application::CMenuState*>(state))
			CEGUI::System::getSingletonPtr()->executeScriptFile("MenuGuiInitScript.lua");	
		else if(dynamic_cast<Application::CGameState*>(state))
		{
			CEGUI::System::getSingletonPtr()->executeScriptFile("GameGuiInitScript.lua");	
			int copies = Logic::CPlayerManager::getSingletonPtr()->getNum_Copies();
			copiesChange(copies);
		}
		else if(dynamic_cast<Application::CPauseState*>(state))
		{
			CEGUI::System::getSingletonPtr()->executeScriptFile("PauseGuiInitScript.lua");	
		}
	}

	//--------------------------------------------------------

	void CHudManager::tick(unsigned int msecs)
	{
		CEGUI::System::getSingletonPtr()->injectTimePulse(msecs/1000.0f);
	}

	//--------------------------------------------------------

	Application::CApplicationState* CHudManager::getState()
	{
		return _currentState;
	}

	//--------------------------------------------------------

	void CHudManager::hudVisible(bool visible)
	{
	
	}

	//--------------------------------------------------------

	void CHudManager::bulletChange(const std::map<std::string,int> &weapons)
	{
	
	}

	//--------------------------------------------------------

	void CHudManager::copiesChange(int num)
	{
		CustomEventArg arg;
		arg.num = num;
		CEGUI::System::getSingletonPtr()->fireEvent(COPIAS_EVENT,arg);
	}

	//--------------------------------------------------------

	void CHudManager::armadoChange(bool armado)
	{
		CustomEventArg arg;
		arg.boolValue = armado;
		CEGUI::System::getSingletonPtr()->fireEvent(CROSSHAIR_EVENT,arg);
	}

	//--------------------------------------------------------

	void CHudManager::damage(float actualLife)
	{
		CustomEventArg arg;
		arg.floatValue = actualLife;
		CEGUI::System::getSingletonPtr()->fireEvent(DAMAGE_EVENT,arg);
	}

	//--------------------------------------------------------

	void CHudManager::showTime(bool mostrar)
	{
	
	}

	//--------------------------------------------------------

	void CHudManager::showSpace(bool mostrar)
	{

	}

	//--------------------------------------------------------

	void CHudManager::historiaHudTime(bool show, const std::string &text)
	{
		CustomEventArg arg;

		arg.boolValue = show;

		if(show)
		{
			arg.str = text;
		}
		CEGUI::System::getSingletonPtr()->fireEvent(TIMECHARACTER_EVENT,arg);
	}

	//--------------------------------------------------------

	void CHudManager::historiaHudSpace(bool show, const std::string &text)
	{
		CustomEventArg arg;

		arg.boolValue = show;

		if(show)
		{
			arg.str = text;
		}
		CEGUI::System::getSingletonPtr()->fireEvent(SPACECHARACTER_EVENT,arg);
	}

	//--------------------------------------------------------

	void CHudManager::cinemaMode(bool activate,const std::string sceneTitle)
	{
		CustomEventArg arg;

		arg.boolValue = activate;

		if(activate)
		{
			arg.str = sceneTitle;
		}

		CEGUI::System::getSingletonPtr()->fireEvent(CINEMAMODE_EVENT,arg);
	}

	//--------------------------------------------------------

	void CHudManager::OnLevelLoaded()
	{

	}

	//--------------------------------------------------------
	
	void CHudManager::OnLevelRestarted()
	{
		setState(_currentState);
	}

	//--------------------------------------------------------
	
	void CHudManager::showCredits()
	{
		CEGUI::Window* win = CEGUI::WindowManager::getSingletonPtr()->loadLayoutFromFile("Credits.layout");
		CEGUI::System::getSingletonPtr()->getDefaultGUIContext().setRootWindow(win);
		GUI::CServer::getSingletonPtr()->getPlayerController()->deactivate();

		Logic::CEntity* ent = Logic::CServer::getSingletonPtr()->getMap()->getEntityByName("MusicPlayer");

		if(ent)
		{
			Logic::CMusicPlayer* musicPlayer = static_cast<Logic::CMusicPlayer*>(ent->getComponent("CMusicPlayer"));
			if(musicPlayer)
				musicPlayer->stop();
		}

		ent = Logic::CServer::getSingletonPtr()->getPlayer();

		if(ent)
		{
			Logic::CAvatarController* player = static_cast<Logic::CAvatarController*>(ent->getComponent("CAvatarController"));
			if(player)
				player->stopAllMovement();
		}

		Sonido::CServer::getSingletonPtr()->getMusicPlayer()->playSong("TimeAndSpace21Junio.mp3");

	}


}