/**

*/
#ifndef __GUI_HudManager_H
#define __GUI_HudManager_H

#include "Logic/Managers/Player Manager/PlayerListener.h"
#include <CEGUI/EventArgs.h>


namespace CEGUI
{
	class Window;
	class EventSet;
}

namespace Application
{
	class CApplicationState;
}

namespace GUI 
{
	class CustomEventArg : public CEGUI::EventArgs
	{
	public:
		std::string str;
		int num;
		bool boolValue;
		float floatValue;
	};

	class CHudManager :  public Logic::IPlayerListener, public Application::IApplicationListener
	{
	public:

		//Metodos Singleton
		static CHudManager* getSingletonPtr();
		static bool Init();
		static bool Release();

		void tick(unsigned int msecs);

		void setState(Application::CApplicationState* state);

		Application::CApplicationState* getState();

		
		void hudVisible(bool visible);
		void historiaHudSpace(bool show, const std::string &text = "");
		void historiaHudTime(bool show, const std::string &text = "");
		void showTime(bool mostrar);
		void showSpace(bool mostrar);
		void cinemaMode(bool activate, const std::string sceneTitle = "");
		void showCredits();

		//PLAYER LISTENER METHODS
		void bulletChange(const std::map<std::string,int> &weapons);
		void copiesChange(int num);
		void armadoChange(bool armado);
		void damage(float actualLife);
		//PLAYER LISTENER METHODS

		//APPLICATION LISTENER METHODS
		virtual void OnLevelLoaded();
		virtual void OnLevelRestarted();
		//APPLICATION LISTENER METHODS

	private:

		CHudManager();

		~CHudManager();

		bool open();

		void close();

		static CHudManager* _instance;

		CEGUI::EventSet* _eventSet;

		Application::CApplicationState* _currentState;
	};
} //namespace GUI
#endif 