//---------------------------------------------------------------------------
// InputManager.cpp
//---------------------------------------------------------------------------

/**
@file InputManager.cpp

Contiene la implementaci�n del gestor de perif�ricos de entrada
as� como las clases oyentes que deben extender las clases que
quieran ser avisadas de los eventos de dichos perif�ricos:
rat�n y teclado.

@see GUI::CInputManager
@see GUI::CKeyboardListener
@see GUI::CMouseListener

@author David Llans�
@date Julio, 2010
*/

#include "InputManager.h"

#include "BaseSubsystems/Server.h"

#include <OISInputManager.h>

#include <sstream>
#include <cassert>


namespace GUI{

	CInputManager *CInputManager::_instance = 0;

	//--------------------------------------------------------

	CInputManager::CInputManager() :
		_mouse(0),
		_keyboard(0),
		_joystick(0),
		_inputSystem(0),
		_controllerMode(DIRECT_INPUT)
	{
		assert(!_instance && "�Segunda inicializaci�n de GUI::CInputManager no permitida!");
		_instance = this;

	} // CInputManager

	//--------------------------------------------------------

	CInputManager::~CInputManager() 
	{
		assert(_instance);

		_instance = 0;

	} // ~CInputManager

	//--------------------------------------------------------

	bool CInputManager::Init() 
	{
		assert(!_instance && "�Segunda inicializaci�n de GUI::CInputManager no permitida!");

		new CInputManager();

		if (!_instance->open())
		{
			Release();
			return false;
		}

		return true;

	} // Init

	//--------------------------------------------------------

	void CInputManager::Release()
	{
		if(_instance)
		{
			_instance->close();
			delete _instance;
		}

	} // Release

	//--------------------------------------------------------

	bool CInputManager::open() 
	{
		if(!BaseSubsystems::CServer::getSingletonPtr())
			return false;

		// Cogemos el sistema de entrada.
		_inputSystem = BaseSubsystems::CServer::getSingletonPtr()->getInputSystem();

		// Cogemos el buffer del teclado y nos hacemos oyentes.
		_keyboard = BaseSubsystems::CServer::getSingletonPtr()->getBufferedKeyboard();
		if(_keyboard)
			_keyboard->setEventCallback(this);

		// Cogemos el buffer del rat�n y nos hacemos oyentes.
		_mouse = BaseSubsystems::CServer::getSingletonPtr()->getBufferedMouse();
		if(_mouse)
			_mouse->setEventCallback(this);

		_joystick = BaseSubsystems::CServer::getSingletonPtr()->getBufferedJoystick();
		if (_joystick)
		{
			_joystick->setEventCallback(this);
			for(int i=0; i<6; ++i)
				_joystickState.axisValues.push_back(0);
		}
		return true;

	} // open

	//--------------------------------------------------------

	void CInputManager::close()
	{
		// No somos responsables de la destrucci�n de los objetos.
		_mouse = 0;
		_keyboard = 0;
		_inputSystem = 0;

	} // close

	//--------------------------------------------------------

	void CInputManager::tick() 
	{
		// Se necesita capturar todos los dispositivos.
		if(_mouse) {
			_mouse->capture();
		}

		if(_keyboard) {
			_keyboard->capture();
		}

		if(_joystick) {
			checkJoyStickAxis();
		}

	} // capture

	//--------------------------------------------------------

	void CInputManager::addKeyListener(CKeyboardListener *keyListener) 
	{
		if(_keyboard)
			_keyListeners.push_front(keyListener);

	} // addKeyListener

	//--------------------------------------------------------

	void CInputManager::addMouseListener(CMouseListener *mouseListener) 
	{
		if(_mouse)
			_mouseListeners.push_front(mouseListener);

	} // addMouseListener

	//--------------------------------------------------------

	void CInputManager::addJoystickListener(CJoystickListener *joystickListener) 
	{
		if(_joystick)
			_joystickListeners.push_front(joystickListener);

	} // addMouseListener

	//--------------------------------------------------------

	void CInputManager::removeKeyListener(CKeyboardListener *keyListener) 
	{
		_keyListeners.remove(keyListener);

	} // removeKeyListener

	//--------------------------------------------------------

	void CInputManager::removeMouseListener(CMouseListener *mouseListener) 
	{
		_mouseListeners.remove(mouseListener);

	} // removeMouseListener

	//--------------------------------------------------------

	void CInputManager::removeJoystickListener(CJoystickListener *joystickListener) 
	{
		_joystickListeners.remove(joystickListener);

	} // removeJoystickListener

	//--------------------------------------------------------

	void CInputManager::removeAllListeners() 
	{
		_keyListeners.clear();
		_mouseListeners.clear();
		_joystickListeners.clear();

	} // removeAllListeners

	//--------------------------------------------------------

	void CInputManager::removeAllKeyListeners() 
	{
		_keyListeners.clear();

	} // removeAllKeyListeners

	//--------------------------------------------------------

	void CInputManager::removeAllMouseListeners() 
	{
		_mouseListeners.clear();

	} // removeAllMouseListeners

	//--------------------------------------------------------

	void CInputManager::removeAllJoystickListeners() 
	{
		_joystickListeners.clear();

	} // removeAllMouseListeners

	//--------------------------------------------------------

	bool CInputManager::keyPressed(const OIS::KeyEvent &e) 
	{
		bool flag = false;
		if (!_keyListeners.empty()) 
		{
			std::list<CKeyboardListener*>::const_iterator it;
			it = _keyListeners.begin();
			for (; it != _keyListeners.end(); ++it) 
			{
				if ((*it)->keyPressed(ois2galeon(e)))
				{
					flag = true;
					break;
				}
			}
		}

		return flag;

	} // keyPressed

	//--------------------------------------------------------

	bool CInputManager::keyReleased(const OIS::KeyEvent &e) 
	{
		bool flag = false;
		if (!_keyListeners.empty()) 
		{
			std::list<CKeyboardListener*>::const_iterator it;
			it = _keyListeners.begin();
			for (; it != _keyListeners.end(); ++it) 
			{
				if (*it)
				{
					if ((*it)->keyReleased(ois2galeon(e)))
					{
						flag = true;
						break;
					}
				}
			}
		}

		return flag;

	} // keyReleased

	//--------------------------------------------------------

	TKey CInputManager::ois2galeon(const OIS::KeyEvent &e)
	{
		unsigned int text = e.text;
		//Las teclas del numpad no vienen con el texto. Lo metemos a mano.
		switch (e.key)
		{
		case OIS::KC_DECIMAL:
			text = 46;
			break;
		case OIS::KC_DIVIDE:
			text = 47;
			break;
		case OIS::KC_NUMPAD0:
			text = 48;
			break;
		case OIS::KC_NUMPAD1:
			text = 49;
			break;
		case OIS::KC_NUMPAD2:
			text = 50;
			break;
		case OIS::KC_NUMPAD3:
			text = 51;
			break;
		case OIS::KC_NUMPAD4:
			text = 52;
			break;
		case OIS::KC_NUMPAD5:
			text = 53;
			break;
		case OIS::KC_NUMPAD6:
			text = 54;
			break;
		case OIS::KC_NUMPAD7:
			text = 55;
			break;
		case OIS::KC_NUMPAD8:
			text = 56;
			break;
		case OIS::KC_NUMPAD9:
			text = 57;
			break;
		}

		return TKey(text,(const Key::TKeyID)e.key);

	}


	//--------------------------------------------------------

	bool CInputManager::mouseMoved(const OIS::MouseEvent &e) 
	{
		if (!_mouseListeners.empty()) 
		{
			// Actualizamos el estado antes de enviarlo
			_mouseState.setExtents(e.state.width, e.state.height);
			_mouseState.setPosition(e.state.X.abs,e.state.Y.abs);
			_mouseState.movX = e.state.X.rel;
			_mouseState.movY = e.state.Y.rel;
			_mouseState.scrool = e.state.Z.rel;
			_mouseState.button = Button::UNASSIGNED;

			std::list<CMouseListener*>::const_iterator it;
			it = _mouseListeners.begin();
			for (; it != _mouseListeners.end(); ++it) 
			{
				if ((*it)->mouseMoved(_mouseState))
					return true;
			}
		}

		return false;

	} // mouseMoved

	//--------------------------------------------------------

	bool CInputManager::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID button) 
	{
		if (!_mouseListeners.empty()) 
		{
			// Actualizamos el estado antes de enviarlo
			_mouseState.setExtents(e.state.width, e.state.height);
			_mouseState.setPosition(e.state.X.abs,e.state.Y.abs);
			_mouseState.movX = e.state.X.rel;
			_mouseState.movY = e.state.Y.rel;
			_mouseState.scrool = e.state.Z.rel;
			_mouseState.button = (TButton)button;

			std::list<CMouseListener*>::const_iterator it;
			it = _mouseListeners.begin();
			for (; it != _mouseListeners.end(); ++it) 
			{
				if ((*it)->mousePressed(_mouseState))
					break;
			}
		}

		return false;

	} // mousePressed

	//--------------------------------------------------------

	bool CInputManager::mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID button) 
	{
		if (!_mouseListeners.empty()) 
		{
			// Actualizamos el estado antes de enviarlo
			_mouseState.setExtents(e.state.width, e.state.height);
			_mouseState.setPosition(e.state.X.abs,e.state.Y.abs);
			_mouseState.movX = e.state.X.rel;
			_mouseState.movY = e.state.Y.rel;
			_mouseState.scrool = e.state.Z.rel;
			_mouseState.button = (TButton)button;

			std::list<CMouseListener*>::const_iterator it;
			it = _mouseListeners.begin();
			for (; it != _mouseListeners.end(); ++it) 
			{
				if ((*it)->mouseReleased(_mouseState))
					break;
			}
		}

		return false;

	} // mouseReleased

	//---------------------
	// JOYSTICK LISTENER
	//---------------------

	//this is meant to translate between the DIRECTINP/XINP diferences
	TJoystickButton CInputManager::returnButton(int button)
	{
		if(_controllerMode == DIRECT_INPUT)
		{
			return TJoystickButton(button);
		}
		else //X_INPUT
		{
			switch(button)
			{
			case 0:
				return TJoystickButton::BUTTON_A;
				break;
			case 1:
				return TJoystickButton::BUTTON_A;
				break;
			case 2:
				break;
			case 3:
				break;
			case 4:
				break;
			case 5:
				break;
			case 6:
				break;
			case 7:
				break;

			}
		}
	}


	bool CInputManager::buttonPressed( const OIS::JoyStickEvent &arg, int button)
	{
		printf("button pressed %d\n", button);
		bool flag = false;
		std::list<CJoystickListener *>::const_iterator it;
		it = _joystickListeners.begin();
		for (; it != _joystickListeners.end(); ++it) 
		{
			if ((*it)->buttonPressed(TJoystickButton(button)))
			{
				flag = true;
				break;
			}
		}
		return flag;
	}

	/** @remarks Joystick button up event */
	bool CInputManager::buttonReleased( const OIS::JoyStickEvent &arg, int button)
	{
		bool flag = false;
		std::list<CJoystickListener *>::const_iterator it;
		it = _joystickListeners.begin();
		for (; it != _joystickListeners.end(); ++it) 
		{
			if ((*it)->buttonReleased(TJoystickButton(button)))
			{
				flag = true;
				break;
			}
		}
		return flag;
	}


	//if (!_mouseListeners.empty()) 
	//	{
	//		// Actualizamos el estado antes de enviarlo
	//		_mouseState.setExtents(e.state.width, e.state.height);
	//		_mouseState.setPosition(e.state.X.abs,e.state.Y.abs);
	//		_mouseState.movX = e.state.X.rel;
	//		_mouseState.movY = e.state.Y.rel;
	//		_mouseState.scrool = e.state.Z.rel;
	//		_mouseState.button = Button::UNASSIGNED;

	//		std::list<CMouseListener*>::const_iterator it;
	//		it = _mouseListeners.begin();
	//		for (; it != _mouseListeners.end(); ++it) 
	//		{
	//			if ((*it)->mouseMoved(_mouseState))
	//				return true;
	//		}
	//	}

	//	return false;

	/** @remarks Joystick axis moved event */
	bool CInputManager::axisMoved( const OIS::JoyStickEvent &arg, int axis)
	{

		//		int x = arg.state.mAxes[axis].abs;
		//
		//
		//		//x / OIS::JoyStick::MAX_AXIS;
		//#ifdef _DEBUG
		//		if(axis == 1)
		//			printf("Axis %d valor = %d\n", axis, x);
		//#endif
		//
		//		bool flag = false;
		//		std::list<CJoystickListener *>::const_iterator it;
		//		it = _joystickListeners.begin();
		//		for (; it != _joystickListeners.end(); ++it) 
		//		{
		//			if ((*it)->axisMoved(x,axis))
		//			{
		//				flag = true;
		//				break;
		//			}
		//		}

		return false;
	}

	bool CInputManager::sliderMoved( const OIS::JoyStickEvent &e, int sliderID)
	{
		return false;
	}

	bool CInputManager::povMoved( const OIS::JoyStickEvent &e, int pov)
	{
		//printf("POV[%d] = %d\n", pov, e.state.mPOV[pov].direction);
		bool flag = false;
		std::list<CJoystickListener *>::const_iterator it;
		it = _joystickListeners.begin();
		for (; it != _joystickListeners.end(); ++it) 
		{
			if ((*it)->povMoved(e.state.mPOV[pov].direction))
			{
				flag = true;
				break;
			}
		}
		return flag;
	}

	bool CInputManager::vector3Moved( const OIS::JoyStickEvent &arg, int index)
	{
		return false;
	}

	void CInputManager::checkJoyStickAxis()
	{
		if (_joystick)
		{
			_joystick->capture();

			std::vector<OIS::Axis,std::allocator<OIS::Axis>>::const_iterator itr;

			int axis =0;

			for(itr =  _joystick->getJoyStickState().mAxes.begin(); itr !=  _joystick->getJoyStickState().mAxes.end(); ++itr)
			{
				if(true)//(_joystickState.axisValues[axis] != itr ->abs)
				{
					std::list<CJoystickListener *>::const_iterator it;

					for (it = _joystickListeners.begin(); it != _joystickListeners.end(); ++it) 
					{
						if ((*it)->axisMoved(itr->abs,axis))
							break;
					}
				}

				axis++;
			}
		}
	}


} // namespace GUI
