/**
@file PlayerController.cpp

Contiene la implementación de la clase CPlayerController. Se encarga de
recibir eventos del teclado y del ratón y de interpretarlos para
mover al jugador.

@see GUI::CPlayerController

@author David Llansó
@date Agosto, 2010
*/

#include "PlayerController.h"
#include "InputManager.h"

#include "Logic/Entity/Entity.h"
#include "Logic/Entity/Message.h"

#include <cassert>

#define TURN_FACTOR 0.001f

using namespace Logic::AvatarActions;

namespace GUI {

	CPlayerController::CPlayerController() : _controlledAvatar(0), _shiftPressed(false), _wPressed(false)
	{
		for(int i=0; i<6; ++i)
			_axisValues.push_back(0);
	} // CPlayerController

	//--------------------------------------------------------

	CPlayerController::~CPlayerController()
	{
		deactivate();

	} // ~CPlayerController

	//--------------------------------------------------------

	void CPlayerController::activate()
	{		
		CInputManager::getSingletonPtr()->addKeyListener(this);
		CInputManager::getSingletonPtr()->addMouseListener(this);
		CInputManager::getSingletonPtr()->addJoystickListener(this);

	} // activate

	//--------------------------------------------------------

	void CPlayerController::deactivate()
	{
		CInputManager::getSingletonPtr()->removeKeyListener(this);
		CInputManager::getSingletonPtr()->removeMouseListener(this);
		CInputManager::getSingletonPtr()->removeJoystickListener(this);

	} // deactivate

	//--------------------------------------------------------

	bool CPlayerController::keyPressed(TKey key)
	{
		if(_controlledAvatar)
		{
			Logic::CControlMessage* m = NULL;

			Logic::CSpecialControlMessage* m2 = NULL;

			switch(key.keyId)
			{
			case GUI::Key::W:
				_wPressed = true;
				m = new Logic::CControlMessage();

				if(_shiftPressed)
				{
					m->_avatarAction = RUN;
				}
				else
				{
					m->_avatarAction = WALK;
				}
				_controlledAvatar->emitMessage(m);
				break;

			case GUI::Key::S:
				m = new Logic::CControlMessage();
				m->_avatarAction = WALK_BACK;
				_controlledAvatar->emitMessage(m);
				break;

			case GUI::Key::A:
				m = new Logic::CControlMessage();
				m->_avatarAction = STRAFE_LEFT;
				_controlledAvatar->emitMessage(m);
				break;

			case GUI::Key::D:
				m = new Logic::CControlMessage();
				m->_avatarAction = STRAFE_RIGHT;
				_controlledAvatar->emitMessage(m);
				break;

			case GUI::Key::SPACE:
				m = new Logic::CControlMessage();
				m->_avatarAction = JUMP;
				_controlledAvatar->emitMessage(m);
				break;

			case GUI::Key::LCONTROL:
				m = new Logic::CControlMessage();
				m->_avatarAction = CROUCH;
				_controlledAvatar->emitMessage(m);
				break;

			case GUI::Key::LSHIFT:
				_shiftPressed = true;
				if(_wPressed)
				{
					Logic::CControlMessage* m2 = new Logic::CControlMessage();
					m2->_avatarAction = RUN;
					_controlledAvatar->emitMessage(m2);
				}
				break;

			case GUI::Key::E:
				m = new Logic::CControlMessage();
				m->_avatarAction = CONTEXTUAL_ACTION;
				_controlledAvatar->emitMessage(m);
				break;

			case GUI::Key::C:
				m2 = new Logic::CSpecialControlMessage();
				m2->_avatarAction = SPECIAL_POWER1;
				_controlledAvatar->emitMessage(m2);
				break;

			case GUI::Key::T:
				m2 = new Logic::CSpecialControlMessage();
				m2->_avatarAction = SPECIAL_POWER2;
				_controlledAvatar->emitMessage(m2);
				break;

			case GUI::Key::G:
				m2 = new Logic::CSpecialControlMessage();
				m2->_avatarAction = SPECIAL_POWER3;
				_controlledAvatar->emitMessage(m2);
				break;
			default:
				return false;
			}

			return true;
		}
		return false;

	} // keyPressed

	//--------------------------------------------------------

	bool CPlayerController::keyReleased(TKey key)
	{
		if(_controlledAvatar)
		{
			Logic::CControlMessage* m = NULL;
			switch(key.keyId)
			{
			case GUI::Key::W:
				_wPressed = false;
				m = new Logic::CControlMessage();
				m->_avatarAction = STOP_WALK;
				if(_shiftPressed)
				{
					Logic::CControlMessage* m2 = new Logic::CControlMessage();
					m2->_avatarAction = STOP_RUN;
					_controlledAvatar->emitMessage(m2);
				}
				break;

			case GUI::Key::S:
				m = new Logic::CControlMessage();
				m->_avatarAction = STOP_WALK;
				break;

			case GUI::Key::A:
			case GUI::Key::D:
				m = new Logic::CControlMessage();
				m->_avatarAction = STOP_STRAFE;
				break;

			case GUI::Key::LCONTROL:
				m = new Logic::CControlMessage();
				m->_avatarAction = STOP_CROUCH;
				break;

			case GUI::Key::LSHIFT:
				_shiftPressed = false;
				if(_wPressed)
				{
					Logic::CControlMessage* m2 = new Logic::CControlMessage();
					m2->_avatarAction = STOP_RUN;
					_controlledAvatar->emitMessage(m2);
					return true;
				}
				return true;

			default:
				delete m;
				return false;
			}
			_controlledAvatar->emitMessage(m);
			return true;
		}
		return false;

	} // keyReleased

	//--------------------------------------------------------

	bool CPlayerController::mouseMoved(const CMouseState &mouseState)
	{
		if(_controlledAvatar)
		{
			Logic::CControlMessage* m = new Logic::CControlMessage();
			m->_avatarAction = TURN;
			m->_yaw = -(float)mouseState.movX * TURN_FACTOR;
			m->_pitch = -(float)mouseState.movY * TURN_FACTOR;
			_controlledAvatar->emitMessage(m);

			// Si mueve la rueda del mouse cambiamos de arma
			if (mouseState.scrool != 0)
			{
				Logic::CControlMessage* msg = new Logic::CControlMessage();
				msg->_avatarAction = CHANGE_WEAPON;
				msg->_scroll = (mouseState.scrool < 0) ? 1 : -1;
				_controlledAvatar->emitMessage(msg);
			}

			return true;
		}
		return false;

	} // mouseMoved

	//--------------------------------------------------------

	bool CPlayerController::mousePressed(const CMouseState &mouseState)
	{
		if (mouseState.button == TButton::LEFT)
		{
			Logic::CControlMessage* m = new Logic::CControlMessage();
			m->_avatarAction = FIRE;
			_controlledAvatar->emitMessage(m);
			return true;
		}
		else if (mouseState.button == TButton::RIGHT)
		{
			Logic::CControlMessage* m = new Logic::CControlMessage();
			m->_avatarAction = ZOOM_IN;
			_controlledAvatar->emitMessage(m);
			return true;
		}
		return false;

	} // mousePressed

	//--------------------------------------------------------

	bool CPlayerController::mouseReleased(const CMouseState &mouseState)
	{
		if (mouseState.button == TButton::LEFT)
		{
			/*Logic::CControlMessage* m = new Logic::CControlMessage();
			m->_avatarAction = STOP_FIRE;
			_controlledAvatar->emitMessage(m);*/
			return true;
		}
		else if (mouseState.button == TButton::RIGHT)
		{
			Logic::CControlMessage* m = new Logic::CControlMessage();
			m->_avatarAction = ZOOM_OUT;
			_controlledAvatar->emitMessage(m);
			return true;
		}
		return false;

	} // mouseReleased

	//--------------------------------------------------------

	bool CPlayerController::buttonPressed(TJoystickButton button)
	{
		if(_controlledAvatar)
		{
			switch (button)
			{
			case TJoystickButton::BUTTON_A:
				{
					Logic::CControlMessage *m = new Logic::CControlMessage();
					m->_avatarAction = JUMP;
					_controlledAvatar->emitMessage(m);
					break;
				}

			case TJoystickButton::BUTTON_B:
				{
					Logic::CSpecialControlMessage* m2 = new Logic::CSpecialControlMessage();
					m2->_avatarAction = SPECIAL_POWER3;
					_controlledAvatar->emitMessage(m2);
					break;
				}
			case TJoystickButton::BUTTON_X:
				{
					Logic::CSpecialControlMessage* m2 = new Logic::CSpecialControlMessage();
					m2->_avatarAction = SPECIAL_POWER1;
					_controlledAvatar->emitMessage(m2);
					break;
				}
				break;
			case TJoystickButton::BUTTON_Y:
				{
					Logic::CControlMessage *m = new Logic::CControlMessage();
					m->_avatarAction = CONTEXTUAL_ACTION;
					_controlledAvatar->emitMessage(m);
					break;
				}
				break;
			case TJoystickButton::BUTTON_LB:
				{
					Logic::CSpecialControlMessage* m2 = new Logic::CSpecialControlMessage();
					m2->_avatarAction = SPECIAL_POWER2;
					_controlledAvatar->emitMessage(m2);
					break;
				}
			case TJoystickButton::BUTTON_RB:
				_shiftPressed = true;
				if(_wPressed)
				{
					Logic::CControlMessage* m2 = new Logic::CControlMessage();
					m2->_avatarAction = RUN;
					_controlledAvatar->emitMessage(m2);
				}
				break;
			case TJoystickButton::BUTTON_RT:
				{

					Logic::CControlMessage* m2 = new Logic::CControlMessage();
					m2->_avatarAction = FIRE;
					_controlledAvatar->emitMessage(m2);

					break;
				}
			case TJoystickButton::BUTTON_LT:
				{
					Logic::CControlMessage* m2 = new Logic::CControlMessage();
					m2->_avatarAction = ZOOM_IN;
					_controlledAvatar->emitMessage(m2);
				}
				break;
			case TJoystickButton::BUTTON_BACK:
				break;
			case TJoystickButton::BUTTON_START:
				break;
			default:
				return false;
			}
		}

		return false;

	} // buttonPressed

	//--------------------------------------------------------

	bool CPlayerController::buttonReleased(TJoystickButton button)
	{
		if(_controlledAvatar)
		{
			switch (button)
			{
			case TJoystickButton::BUTTON_A:
				break;
			case TJoystickButton::BUTTON_B:
				break;
			case TJoystickButton::BUTTON_X:
				break;
			case TJoystickButton::BUTTON_Y:
				break;
			case TJoystickButton::BUTTON_LB:
				break;
			case TJoystickButton::BUTTON_RB:
				_shiftPressed = false;
				if(_wPressed)
				{
					Logic::CControlMessage* m2 = new Logic::CControlMessage();
					m2->_avatarAction = STOP_RUN;
					_controlledAvatar->emitMessage(m2);
				}
				break;
			case TJoystickButton::BUTTON_LT:
				{
					Logic::CControlMessage* m2 = new Logic::CControlMessage();
					m2->_avatarAction = ZOOM_OUT;
					_controlledAvatar->emitMessage(m2);
				}
				break;
			case TJoystickButton::BUTTON_BACK:
				break;
			case TJoystickButton::BUTTON_START:
				break;
			default:
				return false;
			}
		}

		return false;

	} // mouseReleased

	//--------------------------------------------------------

	bool CPlayerController::axisMoved(int x, int axis)
	{
		if(_controlledAvatar && _axisValues[axis] != x)
		{
#ifdef _DEBUG
			{
				if(axis < 4)
					printf("Axis %d valor = %d\n", axis, x);
			}
#endif	

			switch (axis)
			{
			case 0:
				{
					Logic::CControlMessage* m = new Logic::CControlMessage();
					m->_avatarAction = PITCH;
					m->_joystick = true;
					if(x >= PITCH_VALUE || x <= -PITCH_VALUE)
					{
						m->_pitch = -(float)Math::fromDegreesToRadians(x * 360 / 32000) ;
					}
					else
					{
						m->_pitch = 0;
					}

					_controlledAvatar->emitMessage(m);
				}
				break;
			case 1:
				{
					Logic::CControlMessage* m = new Logic::CControlMessage();
					m->_avatarAction = TURN;
					m->_joystick = true;

					if(x >= TURN_VALUE|| x <= -TURN_VALUE)
					{		
						m->_yaw = -(float)Math::fromDegreesToRadians((x) * (360.0f / (32000.0f))) * 0.00099f;
					}
					else
					{
						m->_yaw = 0;
					}

					_controlledAvatar->emitMessage(m);
				}
				break;

			case 2:
				//LEFT AXIS
				{
					Logic::CControlMessage* m = new Logic::CControlMessage(); 
					m->_avatarAction = TAvatarActions::IDLE;

					// RUNNING	
					if(x <= RUNNING_VALUE && _axisValues[axis] > RUNNING_VALUE)
					{
						m->_avatarAction = RUN;
					}
					// Si el valor anterior era de RUN mandamos STOPRUN si no ,no.
					else if(x > RUNNING_VALUE &&_axisValues[axis] <= RUNNING_VALUE)
					{
						m->_avatarAction = STOP_RUN;
					}
					// WALKING
					else if((x <= -WALK_VALUE && x > RUNNING_VALUE) && (_axisValues[axis] < RUNNING_VALUE || _axisValues[axis] > -WALK_VALUE))
					{
						m->_avatarAction = WALK;
					}
					else if(x >= WALK_VALUE && _axisValues[axis] < WALK_VALUE)
						m->_avatarAction = WALK_BACK;
					else if((x < WALK_VALUE && x > -WALK_VALUE) && ( _axisValues[axis] >= WALK_VALUE ||  _axisValues[axis] <= -WALK_VALUE))
					{
						m->_avatarAction = STOP_WALK;
					}

					if(m->_avatarAction != TAvatarActions::IDLE)
						_controlledAvatar->emitMessage(m);
					else
						m->release();
				}
				break;

			case 3:
				{
					Logic::CControlMessage* m2 = new Logic::CControlMessage(); 
					m2->_avatarAction = TAvatarActions::IDLE;

					if(x <= -STRAFE_VALUE && _axisValues[axis] > -STRAFE_VALUE)
						m2->_avatarAction = STRAFE_LEFT;
					else if(x >= STRAFE_VALUE && _axisValues[axis] < STRAFE_VALUE)
						m2->_avatarAction = STRAFE_RIGHT;
					else if(x > -STRAFE_VALUE && x < STRAFE_VALUE && (_axisValues[axis] <= -STRAFE_VALUE || _axisValues[axis] >= STRAFE_VALUE))
						m2->_avatarAction = STOP_STRAFE;

					if(m2->_avatarAction != TAvatarActions::IDLE)
						_controlledAvatar->emitMessage(m2);
					else
						m2->release();
				}
				break;
			case 4:
				{
					//TEST
					Logic::CControlMessage* m2 = new Logic::CControlMessage(); 
					if(x <= -STRAFE_VALUE)
						m2->_avatarAction = STRAFE_LEFT;
					else if(x >= STRAFE_VALUE)
						m2->_avatarAction = STRAFE_RIGHT;
					else
						m2->_avatarAction = STOP_STRAFE;
				}
				break;
			}

			_axisValues[axis] = x;
			return true;
		}

		return false;

	} // axisMoved

	//--------------------------------------------------------

	bool CPlayerController::povMoved(int direction)
	{
		if(_controlledAvatar)
		{
			switch (direction)
			{
			case Button::TPovButton::POV_UP:
				break;
			case Button::TPovButton::POV_DOWN:
				break;
			case Button::TPovButton::POV_RIGHT:
				break;
			case Button::TPovButton::POV_LEFT:
				break;
			default:
				break;
			}
		}
		return false;
	}

} // namespace GUI
