/*
** Lua binding: toLuaPackage
** Generated automatically by tolua++-1.0.92 on 09/29/14 01:27:57.
*/

#ifndef __cplusplus
#include "stdlib.h"
#endif
#include "string.h"

#include "tolua++.h"

/* Exported function */
int tolua_toLuaPackage_open (lua_State* tolua_S);

#include "HudManager.h"
#include "Application/MenuState.h"
#include "Application/PauseState.h"
#include "Application/BaseApplication.h"

/* function to register type */
static void tolua_reg_types (lua_State* tolua_S)
{
 tolua_usertype(tolua_S,"Application::CBaseApplication");
 tolua_usertype(tolua_S,"GUI::CHudManager");
 tolua_usertype(tolua_S,"Application::CApplicationState");
 tolua_usertype(tolua_S,"GUI::CustomEventArg");
 tolua_usertype(tolua_S,"Application::CMenuState");
 tolua_usertype(tolua_S,"Application::CPauseState");
}

/* method: getSingletonPtr of class  GUI::CHudManager */
#ifndef TOLUA_DISABLE_tolua_toLuaPackage_GUI_CHudManager_getSingletonPtr00
static int tolua_toLuaPackage_GUI_CHudManager_getSingletonPtr00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertable(tolua_S,1,"GUI::CHudManager",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
 {
  GUI::CHudManager* tolua_ret = (GUI::CHudManager*)  GUI::CHudManager::getSingletonPtr();
 tolua_pushusertype(tolua_S,(void*)tolua_ret,"GUI::CHudManager");
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getSingletonPtr'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: setState of class  GUI::CHudManager */
#ifndef TOLUA_DISABLE_tolua_toLuaPackage_GUI_CHudManager_setState00
static int tolua_toLuaPackage_GUI_CHudManager_setState00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"GUI::CHudManager",0,&tolua_err) ||
 !tolua_isusertype(tolua_S,2,"Application::CApplicationState",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  GUI::CHudManager* self = (GUI::CHudManager*)  tolua_tousertype(tolua_S,1,0);
  Application::CApplicationState* state = ((Application::CApplicationState*)  tolua_tousertype(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'setState'",NULL);
#endif
 {
  self->setState(state);
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'setState'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: getState of class  GUI::CHudManager */
#ifndef TOLUA_DISABLE_tolua_toLuaPackage_GUI_CHudManager_getState00
static int tolua_toLuaPackage_GUI_CHudManager_getState00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"GUI::CHudManager",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  GUI::CHudManager* self = (GUI::CHudManager*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getState'",NULL);
#endif
 {
  Application::CApplicationState* tolua_ret = (Application::CApplicationState*)  self->getState();
 tolua_pushusertype(tolua_S,(void*)tolua_ret,"Application::CApplicationState");
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getState'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* get function: str of class  GUI::CustomEventArg */
#ifndef TOLUA_DISABLE_tolua_get_GUI__CustomEventArg_str
static int tolua_get_GUI__CustomEventArg_str(lua_State* tolua_S)
{
  GUI::CustomEventArg* self = (GUI::CustomEventArg*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in accessing variable 'str'",NULL);
#endif
 tolua_pushcppstring(tolua_S,(const char*)self->str);
 return 1;
}
#endif //#ifndef TOLUA_DISABLE

/* set function: str of class  GUI::CustomEventArg */
#ifndef TOLUA_DISABLE_tolua_set_GUI__CustomEventArg_str
static int tolua_set_GUI__CustomEventArg_str(lua_State* tolua_S)
{
  GUI::CustomEventArg* self = (GUI::CustomEventArg*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (!self) tolua_error(tolua_S,"invalid 'self' in accessing variable 'str'",NULL);
 if (!tolua_iscppstring(tolua_S,2,0,&tolua_err))
 tolua_error(tolua_S,"#vinvalid type in variable assignment.",&tolua_err);
#endif
  self->str = ((std::string)  tolua_tocppstring(tolua_S,2,0))
;
 return 0;
}
#endif //#ifndef TOLUA_DISABLE

/* get function: num of class  GUI::CustomEventArg */
#ifndef TOLUA_DISABLE_tolua_get_GUI__CustomEventArg_num
static int tolua_get_GUI__CustomEventArg_num(lua_State* tolua_S)
{
  GUI::CustomEventArg* self = (GUI::CustomEventArg*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in accessing variable 'num'",NULL);
#endif
 tolua_pushnumber(tolua_S,(lua_Number)self->num);
 return 1;
}
#endif //#ifndef TOLUA_DISABLE

/* set function: num of class  GUI::CustomEventArg */
#ifndef TOLUA_DISABLE_tolua_set_GUI__CustomEventArg_num
static int tolua_set_GUI__CustomEventArg_num(lua_State* tolua_S)
{
  GUI::CustomEventArg* self = (GUI::CustomEventArg*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (!self) tolua_error(tolua_S,"invalid 'self' in accessing variable 'num'",NULL);
 if (!tolua_isnumber(tolua_S,2,0,&tolua_err))
 tolua_error(tolua_S,"#vinvalid type in variable assignment.",&tolua_err);
#endif
  self->num = ((int)  tolua_tonumber(tolua_S,2,0))
;
 return 0;
}
#endif //#ifndef TOLUA_DISABLE

/* get function: boolValue of class  GUI::CustomEventArg */
#ifndef TOLUA_DISABLE_tolua_get_GUI__CustomEventArg_boolValue
static int tolua_get_GUI__CustomEventArg_boolValue(lua_State* tolua_S)
{
  GUI::CustomEventArg* self = (GUI::CustomEventArg*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in accessing variable 'boolValue'",NULL);
#endif
 tolua_pushboolean(tolua_S,(bool)self->boolValue);
 return 1;
}
#endif //#ifndef TOLUA_DISABLE

/* set function: boolValue of class  GUI::CustomEventArg */
#ifndef TOLUA_DISABLE_tolua_set_GUI__CustomEventArg_boolValue
static int tolua_set_GUI__CustomEventArg_boolValue(lua_State* tolua_S)
{
  GUI::CustomEventArg* self = (GUI::CustomEventArg*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (!self) tolua_error(tolua_S,"invalid 'self' in accessing variable 'boolValue'",NULL);
 if (!tolua_isboolean(tolua_S,2,0,&tolua_err))
 tolua_error(tolua_S,"#vinvalid type in variable assignment.",&tolua_err);
#endif
  self->boolValue = ((bool)  tolua_toboolean(tolua_S,2,0))
;
 return 0;
}
#endif //#ifndef TOLUA_DISABLE

/* get function: floatValue of class  GUI::CustomEventArg */
#ifndef TOLUA_DISABLE_tolua_get_GUI__CustomEventArg_floatValue
static int tolua_get_GUI__CustomEventArg_floatValue(lua_State* tolua_S)
{
  GUI::CustomEventArg* self = (GUI::CustomEventArg*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in accessing variable 'floatValue'",NULL);
#endif
 tolua_pushnumber(tolua_S,(lua_Number)self->floatValue);
 return 1;
}
#endif //#ifndef TOLUA_DISABLE

/* set function: floatValue of class  GUI::CustomEventArg */
#ifndef TOLUA_DISABLE_tolua_set_GUI__CustomEventArg_floatValue
static int tolua_set_GUI__CustomEventArg_floatValue(lua_State* tolua_S)
{
  GUI::CustomEventArg* self = (GUI::CustomEventArg*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (!self) tolua_error(tolua_S,"invalid 'self' in accessing variable 'floatValue'",NULL);
 if (!tolua_isnumber(tolua_S,2,0,&tolua_err))
 tolua_error(tolua_S,"#vinvalid type in variable assignment.",&tolua_err);
#endif
  self->floatValue = ((float)  tolua_tonumber(tolua_S,2,0))
;
 return 0;
}
#endif //#ifndef TOLUA_DISABLE

/* method: startReleased of class  Application::CMenuState */
#ifndef TOLUA_DISABLE_tolua_toLuaPackage_Application_CMenuState_startReleased00
static int tolua_toLuaPackage_Application_CMenuState_startReleased00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Application::CMenuState",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Application::CMenuState* self = (Application::CMenuState*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'startReleased'",NULL);
#endif
 {
  self->startReleased();
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'startReleased'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: optionsReleased of class  Application::CMenuState */
#ifndef TOLUA_DISABLE_tolua_toLuaPackage_Application_CMenuState_optionsReleased00
static int tolua_toLuaPackage_Application_CMenuState_optionsReleased00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Application::CMenuState",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Application::CMenuState* self = (Application::CMenuState*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'optionsReleased'",NULL);
#endif
 {
  bool tolua_ret = (bool)  self->optionsReleased();
 tolua_pushboolean(tolua_S,(bool)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'optionsReleased'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: exitReleased of class  Application::CMenuState */
#ifndef TOLUA_DISABLE_tolua_toLuaPackage_Application_CMenuState_exitReleased00
static int tolua_toLuaPackage_Application_CMenuState_exitReleased00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Application::CMenuState",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Application::CMenuState* self = (Application::CMenuState*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'exitReleased'",NULL);
#endif
 {
  bool tolua_ret = (bool)  self->exitReleased();
 tolua_pushboolean(tolua_S,(bool)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'exitReleased'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: resumeReleased of class  Application::CPauseState */
#ifndef TOLUA_DISABLE_tolua_toLuaPackage_Application_CPauseState_resumeReleased00
static int tolua_toLuaPackage_Application_CPauseState_resumeReleased00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Application::CPauseState",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Application::CPauseState* self = (Application::CPauseState*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'resumeReleased'",NULL);
#endif
 {
  self->resumeReleased();
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'resumeReleased'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: restartLevelReleased of class  Application::CPauseState */
#ifndef TOLUA_DISABLE_tolua_toLuaPackage_Application_CPauseState_restartLevelReleased00
static int tolua_toLuaPackage_Application_CPauseState_restartLevelReleased00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Application::CPauseState",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Application::CPauseState* self = (Application::CPauseState*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'restartLevelReleased'",NULL);
#endif
 {
  self->restartLevelReleased();
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'restartLevelReleased'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: menuReleased of class  Application::CPauseState */
#ifndef TOLUA_DISABLE_tolua_toLuaPackage_Application_CPauseState_menuReleased00
static int tolua_toLuaPackage_Application_CPauseState_menuReleased00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Application::CPauseState",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Application::CPauseState* self = (Application::CPauseState*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'menuReleased'",NULL);
#endif
 {
  self->menuReleased();
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'menuReleased'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: getSingletonPtr of class  Application::CBaseApplication */
#ifndef TOLUA_DISABLE_tolua_toLuaPackage_Application_CBaseApplication_getSingletonPtr00
static int tolua_toLuaPackage_Application_CBaseApplication_getSingletonPtr00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertable(tolua_S,1,"Application::CBaseApplication",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
 {
  Application::CBaseApplication* tolua_ret = (Application::CBaseApplication*)  Application::CBaseApplication::getSingletonPtr();
 tolua_pushusertype(tolua_S,(void*)tolua_ret,"Application::CBaseApplication");
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getSingletonPtr'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: restartLevel of class  Application::CBaseApplication */
#ifndef TOLUA_DISABLE_tolua_toLuaPackage_Application_CBaseApplication_restartLevel00
static int tolua_toLuaPackage_Application_CBaseApplication_restartLevel00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Application::CBaseApplication",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Application::CBaseApplication* self = (Application::CBaseApplication*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'restartLevel'",NULL);
#endif
 {
  bool tolua_ret = (bool)  self->restartLevel();
 tolua_pushboolean(tolua_S,(bool)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'restartLevel'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* Open function */
int tolua_toLuaPackage_open (lua_State* tolua_S)
{
 tolua_open(tolua_S);
 tolua_reg_types(tolua_S);
 tolua_module(tolua_S,NULL,0);
 tolua_beginmodule(tolua_S,NULL);
 tolua_module(tolua_S,"GUI",0);
 tolua_beginmodule(tolua_S,"GUI");
  tolua_cclass(tolua_S,"CHudManager","GUI::CHudManager","",NULL);
  tolua_beginmodule(tolua_S,"CHudManager");
   tolua_function(tolua_S,"getSingletonPtr",tolua_toLuaPackage_GUI_CHudManager_getSingletonPtr00);
   tolua_function(tolua_S,"setState",tolua_toLuaPackage_GUI_CHudManager_setState00);
   tolua_function(tolua_S,"getState",tolua_toLuaPackage_GUI_CHudManager_getState00);
  tolua_endmodule(tolua_S);
 tolua_endmodule(tolua_S);
 tolua_module(tolua_S,"GUI",0);
 tolua_beginmodule(tolua_S,"GUI");
  tolua_cclass(tolua_S,"CustomEventArg","GUI::CustomEventArg","",NULL);
  tolua_beginmodule(tolua_S,"CustomEventArg");
   tolua_variable(tolua_S,"str",tolua_get_GUI__CustomEventArg_str,tolua_set_GUI__CustomEventArg_str);
   tolua_variable(tolua_S,"num",tolua_get_GUI__CustomEventArg_num,tolua_set_GUI__CustomEventArg_num);
   tolua_variable(tolua_S,"boolValue",tolua_get_GUI__CustomEventArg_boolValue,tolua_set_GUI__CustomEventArg_boolValue);
   tolua_variable(tolua_S,"floatValue",tolua_get_GUI__CustomEventArg_floatValue,tolua_set_GUI__CustomEventArg_floatValue);
  tolua_endmodule(tolua_S);
 tolua_endmodule(tolua_S);
 tolua_module(tolua_S,"Application",0);
 tolua_beginmodule(tolua_S,"Application");
  tolua_cclass(tolua_S,"CMenuState","Application::CMenuState","",NULL);
  tolua_beginmodule(tolua_S,"CMenuState");
   tolua_function(tolua_S,"startReleased",tolua_toLuaPackage_Application_CMenuState_startReleased00);
   tolua_function(tolua_S,"optionsReleased",tolua_toLuaPackage_Application_CMenuState_optionsReleased00);
   tolua_function(tolua_S,"exitReleased",tolua_toLuaPackage_Application_CMenuState_exitReleased00);
  tolua_endmodule(tolua_S);
 tolua_endmodule(tolua_S);
 tolua_module(tolua_S,"Application",0);
 tolua_beginmodule(tolua_S,"Application");
  tolua_cclass(tolua_S,"CPauseState","Application::CPauseState","",NULL);
  tolua_beginmodule(tolua_S,"CPauseState");
   tolua_function(tolua_S,"resumeReleased",tolua_toLuaPackage_Application_CPauseState_resumeReleased00);
   tolua_function(tolua_S,"restartLevelReleased",tolua_toLuaPackage_Application_CPauseState_restartLevelReleased00);
   tolua_function(tolua_S,"menuReleased",tolua_toLuaPackage_Application_CPauseState_menuReleased00);
  tolua_endmodule(tolua_S);
 tolua_endmodule(tolua_S);
 tolua_module(tolua_S,"Application",0);
 tolua_beginmodule(tolua_S,"Application");
  tolua_cclass(tolua_S,"CBaseApplication","Application::CBaseApplication","",NULL);
  tolua_beginmodule(tolua_S,"CBaseApplication");
   tolua_function(tolua_S,"getSingletonPtr",tolua_toLuaPackage_Application_CBaseApplication_getSingletonPtr00);
   tolua_function(tolua_S,"restartLevel",tolua_toLuaPackage_Application_CBaseApplication_restartLevel00);
  tolua_endmodule(tolua_S);
 tolua_endmodule(tolua_S);
 tolua_endmodule(tolua_S);
 return 1;
}


#if defined(LUA_VERSION_NUM) && LUA_VERSION_NUM >= 501
 int luaopen_toLuaPackage (lua_State* tolua_S) {
 return tolua_toLuaPackage_open(tolua_S);
};
#endif

