

/**
@file Server.h

@author Alvaro Blazquez Chaca
@date Julio, 2014
*/

#ifndef __BaseSubsystems_System_H
#define __BaseSubsystems_System_H

#include <string>
#include <vector>




namespace BaseSubsystems 
{

	class System
	{
	public:
		static void getFilesInDirectory(const std::string &directoryPath, const std::string &fileExt, std::vector<std::string> &fileList);

		static void getPathFromResource(const std::string &resourceName, std::string& resourcePath);

		static void getDataStreamFromResource(const std::string &resourceName, std::string& dataString);
	};

} // namespace BaseSubsystems

#endif // __BaseSubsystems_Server_H
