

#include "System.h"

#include <assert.h>

#include <iostream>
#include <boost/filesystem.hpp>
#include <OgreResourceManager.h>
#include <OgreArchive.h>
#include <OgreFileSystem.h>
using namespace boost::filesystem;
using namespace std;

namespace BaseSubsystems 
{
	//--------------------------------------------------------

	void System::getFilesInDirectory(const std::string &directoryPath, const std::string &fileExt, std::vector<std::string> &fileList)
	{
		path p (directoryPath);   // p reads clearer than argv[1] in the following code

		if (exists(p))    // does p actually exist?
		{
			if (is_regular_file(p))        // is p a regular file?
			{
				cout << p << " size is " << file_size(p) << '\n';
			}
			else
				if (is_directory(p))      // is p a directory?
				{
					cout << p << " is a directory containing:\n";

					typedef vector<path> vec;             // store paths,
					vec v;                                // so we can sort them later

					copy(directory_iterator(p), directory_iterator(), back_inserter(v));


					sort(v.begin(), v.end());             // sort, since directory iteration

					// is not ordered on some file systems

					for (vec::const_iterator it(v.begin()), it_end(v.end()); it != it_end; ++it)
					{

						cout << "   " << (*it).filename() << " extension: "<< (*it).extension() <<'\n';

						if((*it).extension().generic_string() == fileExt)
						{
							fileList.push_back((*it).filename().generic_string());
						}
					}
				}

				else
					cout << p << " exists, but is neither a regular file nor a directory\n";
		}
		else
			cout << p << " does not exist\n";

	}


	void System::getPathFromResource(const std::string &resourceName, std::string& resourcePath)
	{
		//HACK puede petar si existe el recurso
		std::string group;

		group = Ogre::ResourceGroupManager::getSingleton().findGroupContainingResource(resourceName);
		Ogre::FileInfoListPtr fileInfoList(Ogre::ResourceGroupManager::getSingletonPtr()->findResourceFileInfo(group, Ogre::String(resourceName)));

		Ogre::FileInfo file = (*fileInfoList->begin());
		#ifdef _Debug
		cout << file.archive->getName().c_str()<<file.filename.c_str() ;
		#endif
		resourcePath = file.archive->getName() + file.filename;
	}


	void System::getDataStreamFromResource(const std::string &resourceName, std::string& dataString)
	{
		//HACK puede petar si existe el recurso
		std::string group;

		group = Ogre::ResourceGroupManager::getSingleton().findGroupContainingResource(resourceName);
		Ogre::FileInfoListPtr fileInfoList(Ogre::ResourceGroupManager::getSingletonPtr()->findResourceFileInfo(group, Ogre::String(resourceName)));

		Ogre::FileInfo file = (*fileInfoList->begin());
		#ifdef _Debug
		cout << file.archive->getName().c_str()<<file.filename.c_str() ;
		#endif

		Ogre::DataStreamPtr data = file.archive->open(file.filename);

		dataString =  data->getAsString();
	}

} // namespace BaseSubsystems
