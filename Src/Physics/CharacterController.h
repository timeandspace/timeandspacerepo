/**
@file ControllerManager.h

Contiene la declaraci�n del character controller.

@author Alejandro P�rez Alonso
@date Enero, 2014
*/

#ifndef __Physics_Controller_Controller_H_
#define __Physics_Controller_Controller_H_

#include "BaseSubsystems/Math.h"

// Predeclaraci�n de tipos
namespace Logic {
	class CEntity;
	class CPhysicController;
	class IPhysics;
};

namespace Physics {
	class CServer;
};

namespace physx {
	class PxCapsuleController;
}

// Namespace que contiene las clases relacionadas con la parte f�sica. 
namespace Physics {

	struct ControllerFlag
	{
		enum Enum
		{
			COLLISION_SIDES = (1<<0),	//!< Character is colliding to the sides.
			COLLISION_UP = (1<<1),	//!< Character has collision above.
			COLLISION_DOWN = (1<<2),	//!< Character has collision below.
			JUMPING	= (1<<3),	//!< Character is jumping.
			STOP_JUMPING	= (1<<4)//0x06 
		};
	};

	//----------------------------------
	// Gesti�n de character controllers
	//----------------------------------
	class CCharacterController
	{
	public:
		/**
		Constructor para character Controllers
		*/
		CCharacterController(const Vector3 &position, float radius, 
			float height, int collisionGroup, unsigned int filterData,
			const Logic::CPhysicController *component);

		~CCharacterController();

		/**
		Mueve el controller por la escena.

		@param movement Movimiento que se quiere realizar.
		@param msecs Millisegundos transcurridos desde la �ltima actualizaci�n.
		@return Flags de colisi�n, un conjunto de physic::ControllerFlag.
		*/
		unsigned move(const Vector3 &movement, unsigned int msecs);

		/**
		Devuelve la posici�n del controller.

		@return posici�n del controller en coordenadas l�gicas.
		*/
		const Vector3 getPosition();

		/**
		Establece la posici�n del controller.

		@param posici�n del controller en coordenadas l�gicas.
		*/
		void setPosition(const Vector3 &position);

		/**
		Hace que el controller salte.

		@param movement Movimiento que se quiere realizar
		*/
		unsigned jump(Vector3 &movement, int &jumpTime, const float &jumpHeight, 
			const Vector3& direction, const bool &isFalling, unsigned int msecs);

	private:
		// Servidor de f�sica
		Physics::CServer *_server;

		// Character controller que representa la entidad f�sica en PhysX
		physx::PxCapsuleController *_controller;

		unsigned int _filterData;
	};

} // namespace Physics

#endif // __Physics_Controller_Manager_H_