/**
@file ControllerController.h

Contiene la implementacion del character controller.

@date Enero, 2014
*/
#include "CharacterController.h"
#include "Physics/Server.h"
#include "Physics/Conversions.h"

#include <assert.h>

#include <PxPhysicsAPI.h>


using namespace physx;

namespace Physics
{
	CCharacterController::CCharacterController(const Vector3 &position, float radius, 
		float height, int collisionGroup, unsigned int filterData,
		const Logic::CPhysicController *component)
	{
		_server = CServer::getSingletonPtr();
		_filterData = filterData;
		_controller = _server->createCapsuleController(position, radius, height, collisionGroup, filterData, component);
	}

	//--------------------------------------------------------

	CCharacterController::~CCharacterController()
	{
		if (_controller) {
			_controller->release();
			_controller = NULL;
		}

		_server = NULL;
	}

	//--------------------------------------------------------

	unsigned CCharacterController::move(const Vector3 &movement, unsigned int msecs)
	{
		PxVec3 m = Vector3ToPxVec3(movement);
		float  minDist = 0.01f;
		float elapsedTime = msecs / 1000.0f;
		PxObstacleContext *obstacles = NULL;	
		PxControllerFilters filters;

		/***
		CONFIGURACION DE FILTRO DE COLISIONES
		- Collide based on a group bitmask stored in the controller.
		if (filters.mActiveGroups & controller->getGroupsBitmask != 0)
		collision
		*/
		_controller->setInteraction(PxCCTInteractionMode::eUSE_FILTER);

		PxFilterData data;

		// Filtro de colision del player con el resto de character controllers
		if (_controller->getGroupsBitmask() == FilterGroup::PLAYER_GROUP)
		{
			filters.mActiveGroups = FilterGroup::EVERYTHING & ~FilterGroup::CLONES_GROUP;
			/* 
			data.word0 hace referencia a las colisiones Character Controller vs World
			Si word0 != 0 entonces el player solo colisionará con los objetos que 
			cumplan (objeto->grupoColision & data.word0) != 0
			En este caso quiere decir que si word0 = 0x0f entonces el player
			colisionará con los groups [1-15]
			*/
			data.word0 = FilterGroup::COLLISION_PLAYER;
		}

		// Filtro de colision de los clones, no colisiona con el jugador ni con el resto de copias 
		else if (_controller->getGroupsBitmask() == FilterGroup::CLONES_GROUP)
		{
			filters.mActiveGroups = FilterGroup::EVERYTHING & ~FilterGroup::PLAYER_GROUP & ~FilterGroup::CLONES_GROUP;
			data.word0 = FilterGroup::COLLISION_CLONES;
		}

		
		filters.mFilterData = &data;
		/*** 
		FIN Configuracion de filtro de colisiones 
		*/

		return _controller->move(Vector3ToPxVec3(movement), minDist, elapsedTime, filters, obstacles);
	}

	//--------------------------------------------------------

	const Vector3 CCharacterController::getPosition()
	{
		// Transformar entre coordenadas de PhysX y coordenadas lógicas
		//	- offsetY = altura del controller / 2 + radio del controller
		float offsetY = _controller->getHeight() / 2.0f + _controller->getRadius();
		PxExtendedVec3 pos = Vector3ToPxExtendedVec3(Vector3(0, -offsetY, 0));

		return PxExtendedVec3ToVector3(_controller->getPosition() + pos);
	}

	//--------------------------------------------------------

	void CCharacterController::setPosition(const Vector3 &position)
	{
		// Mover el character controller y devolver los flags de colisión
		float offsetY = _controller->getHeight() / 2.0f + _controller->getRadius();
		PxExtendedVec3 pos = Vector3ToPxExtendedVec3(position+Vector3(0,offsetY,0));
		_controller->setPosition(pos);
	}

	//--------------------------------------------------------

	unsigned CCharacterController::jump(Vector3 &movement, int &jumpTime, const float &jumpHeight, 
			const Vector3& direction, const bool &isFalling, unsigned int msecs)
	{
		if (jumpTime > 0) 
		{
			movement += direction * jumpHeight * msecs * 0.001;
			jumpTime -= msecs;
			//-- jumpTime; //TODO:: hacer que jumpTime -= msecs para que se pueda ralentizar
		}
		// Solo permitiremos saltar de nuevo cuando el controller toque el suelo
		else/* if (! isFalling) */
			return ControllerFlag::STOP_JUMPING;

		return ControllerFlag::JUMPING;
	}

	//--------------------------------------------------------
}