/**
@file Server.cpp

Contiene la implementaci�n del servidor de f�sica. 

@see Physics::CServer

@author Antonio S�nchez Ruiz-Granados
@date Noviembre, 2012

@author Alejandro P�rez Alonso
@date Marzo, 2014
*/

#include "Server.h"
#include "Conversions.h"
#include "ErrorManager.h"
#include "CollisionManager.h"
#include "Logic/Entity/Components/Physics.h"

#include <assert.h>
#include <algorithm>

#include <PxPhysicsAPI.h>
#include <extensions\PxExtensionsAPI.h>
#include <extensions\PxVisualDebuggerExt.h> 
#include <RepX\RepX.h>
#include <RepX\RepXUtility.h>

using namespace Logic;
using namespace physx;

namespace Physics
{
	// �nica instancia del servidor
	CServer *CServer::_instance = NULL;

	//--------------------------------------------------------

	CServer::CServer() : _cudaContextManager(NULL), _scene(NULL), _cpuDispatcher(NULL)
	{
		// Crear gestor de errores
		_errorManager = new CErrorManager();

		// Crear gestor de memoria
		_allocator = new PxDefaultAllocator();

		// TODO: crear gestor de colisiones
		_collisionManager = new CCollisionManager();

		// TODO: Crear PxFoundation
		// Usar nuestro gestor de memoria y nuestro gestor de errores
		_foundation  = PxCreateFoundation(PX_PHYSICS_VERSION, *_allocator, *_errorManager);
		assert(_foundation);

		// TODO: Crear PxProfileZoneManager
		// Es necesario para habitiar algunas opciones de profiling de rendimiento en el PhysX Visual Debugger 
		_profileZoneManager = &PxProfileZoneManager::createProfileZoneManager(_foundation);
		assert(_profileZoneManager);

		// Crear CudaContextManager. Permite aprovechar la GPU para hacer parte de la simulaci�n f�sica.
		// Se utiliza posteriormente al crear la escena f�sica.
		// S�lo Windows
#ifdef PX_WINDOWS
		pxtask::CudaContextManagerDesc cudaContextManagerDesc;
		_cudaContextManager = pxtask::createCudaContextManager(*_foundation, cudaContextManagerDesc, 
			_profileZoneManager);
		if( _cudaContextManager )
		{
			if( !_cudaContextManager->contextIsValid() )
			{
				_cudaContextManager->release();
				_cudaContextManager = NULL;
			}
		}
#endif

		// TODO: Crear PxPhysics
		// Es el punto de entrada al SDK de PhysX
		PxTolerancesScale toleranceScale;
		bool recordMemoryAllocations = true;
		_physics = PxCreatePhysics(PX_PHYSICS_VERSION, *_foundation,
			toleranceScale, recordMemoryAllocations, _profileZoneManager);
		assert(_physics);

		// TODO: crear controller manager (PxCreateControllerManager)
		_controllerManager = PxCreateControllerManager(*_foundation);
		assert(_controllerManager);

		// TODO: inicializar el m�dulo PxCooking
		// Es necesario para cocinar mallas y para deserializar actores a partir de ficheros RepX
		// Usar la funci�n PxCreateCooking
		PxCookingParams params;
		_cooking = PxCreateCooking(PX_PHYSICS_VERSION, *_foundation, params);
		assert(_cooking);

		// TODO: crear un material por defecto
		// Usar el m�todo adecuado del objeto PxPhysics
		float staticFriction = 0.5f;
		float dynamicFriction = 0.5f;
		float restitution = 0.1f;
		_defaultMaterial = _physics->createMaterial(staticFriction, dynamicFriction, restitution);
		assert(_defaultMaterial);

		// Intentar conectar con PhysX Visual Debugger (PVD)
		_pvdConnection = NULL;

		// S�lo en modo DEBUG
#ifdef _DEBUG
		debugger::comm::PvdConnectionManager *pvdConnectionManager = _physics->getPvdConnectionManager();
		assert(pvdConnectionManager && "Error en PxPhysics::getPvdConnectionManager");

		// Configurar m�quina, puerto y tiempo de espera (en millisegundos)
		const char *ip = "127.0.0.1";
		int port = 5425;						
		unsigned int timeout = 100;				

		// Configurar qu� informaci�n queremos mandar al PVD (debug, profile, memory)
		PxVisualDebuggerConnectionFlags connectionFlags = PxVisualDebuggerExt::getAllConnectionFlags();

		// Intentar establecer la conexi�n
		_pvdConnection = PxVisualDebuggerExt::createConnection(pvdConnectionManager, ip, port, timeout, connectionFlags);

#endif
	} 

	//--------------------------------------------------------

	CServer::~CServer() 
	{
		// Destruir objetos en orden inverso a como fueron creados
		if (_pvdConnection) {
			_pvdConnection->release();
			_pvdConnection = NULL;
		}

		// TODO: Liberar material por defecto (release)
		if (_defaultMaterial) {
			_defaultMaterial->release();
			_defaultMaterial = NULL;
		}

		// TODO: Liberar el m�dulo de cooking (release)
		if (_cooking) {
			_cooking->release();
			_cooking = NULL;
		}

		// TODO: Liberar el controller manager (release)
		if (_controllerManager) {
			_controllerManager->release();
			_controllerManager = NULL;
		}

		if (_cudaContextManager) {
			_cudaContextManager->release();
			_cudaContextManager = NULL;
		}

		// TODO: Liberar objeto PxPhysics (release)
		if (_physics) {

			_physics->release();
			_physics = NULL;
		}

		// TODO: Liberar objeto PxProfileZoneManager (release)
		if (_profileZoneManager) {
			_profileZoneManager->release();
			_profileZoneManager = NULL;
		}

		// TODO: Liberar objeto PxFoundation (release)
		if (_foundation) {
			_foundation->release();
			_foundation = NULL;
		}

		// TODO: destruir gestor de colisiones
		if (_collisionManager){
			delete _collisionManager;
			_collisionManager = NULL;
		}

		if (_allocator) {
			delete _allocator;
			_allocator = NULL;
		}

		if (_errorManager) {
			delete _errorManager;
			_errorManager = NULL;
		}
	} 

	//--------------------------------------------------------

	bool CServer::Init() 
	{
		if (!_instance) {
			_instance = new CServer();
		}

		return true;
	} 

	//--------------------------------------------------------

	void CServer::Release()
	{
		if(_instance) {
			delete _instance;
			_instance = NULL;
		}
	} 

	//--------------------------------------------------------

	/***
	Filter Shader que sirve para controlar que elementos colisionan.
	*/
	PxFilterFlags customFilterShader(
		PxFilterObjectAttributes attributes0, PxFilterData filterData0,
		PxFilterObjectAttributes attributes1, PxFilterData filterData1,
		PxPairFlags& pairFlags, const void* constantBlock, PxU32 constantBlockSize)
	{

		// word0 guarda el grupo de colision, word1 una bitmask para determinar con quien colisiona
		if ((filterData0.word0 & filterData1.word1) && (filterData1.word0 & filterData0.word1) ||
			(filterData0.word0 == 0 || filterData1.word0 == 0) || 
			(filterData0.word1 == 0 || filterData1.word1 == 0))
		{
			// let triggers through
			if(PxFilterObjectIsTrigger(attributes0) || PxFilterObjectIsTrigger(attributes1))
			{
				pairFlags = PxPairFlag::eTRIGGER_DEFAULT;
				return PxFilterFlag::eDEFAULT;
			}
			pairFlags = PxPairFlag::eCONTACT_DEFAULT;
			//pairFlags |= PxPairFlag::eSWEPT_INTEGRATION_LINEAR;
			pairFlags |= PxPairFlag::eNOTIFY_TOUCH_FOUND;
			/*pairFlags |= PxPairFlag::eNOTIFY_THRESHOLD_FORCE_FOUND;
			pairFlags |= PxPairFlag::eNOTIFY_THRESHOLD_FORCE_PERSISTS;
			pairFlags |= PxPairFlag::eNOTIFY_THRESHOLD_FORCE_LOST;*/
			pairFlags |= PxPairFlag::eNOTIFY_CONTACT_POINTS;
		}
		else
		{
			return PxFilterFlag::eSUPPRESS;
		}


		return PxFilterFlag::eDEFAULT;
	}

	//--------------------------------------------------------

	void CServer::createScene ()
	{
		assert(_instance);

		// Crear el descriptor de la escena
		PxSceneDesc sceneDesc(_physics->getTolerancesScale());


		// Establecer la gravedad en el eje Y
		sceneDesc.gravity = PxVec3(0.0f, -9.81f, 0.0f);

		// TODO: establecer el gestor de colisiones
		// Asignar el gestor de colisiones al atributo simulationEventCallback
		sceneDesc.simulationEventCallback = _collisionManager;

		// Establecer un gestor de tareas por CPU
		if (!sceneDesc.cpuDispatcher) {
			int mNbThreads = 1; // hilos de ejecuci�n
			_cpuDispatcher  = PxDefaultCpuDispatcherCreate(mNbThreads);
			assert (_cpuDispatcher && "Error en PxDefaultCpuDispatcherCreate");

			sceneDesc.cpuDispatcher = _cpuDispatcher;
		}

		// Establecer el shader que controla las colisiones entre entidades.
		sceneDesc.filterShader = customFilterShader;
		//sceneDesc.filterShader = PxDefaultSimulationFilterShader;

		// Intentar establecer un gestor de tareas por GPU 
		// S�lo Windows
#ifdef PX_WINDOWS
		if (!sceneDesc.gpuDispatcher && _cudaContextManager)
		{
			sceneDesc.gpuDispatcher = _cudaContextManager->getGpuDispatcher();
		}
#endif

		// TODO: Crear la escena f�sica
		// Usando el m�todo adecuado del objeto PxPhysics
		_scene = _physics->createScene(sceneDesc);
		_scene->setFlag(PxSceneFlag::eENABLE_KINEMATIC_PAIRS, true);
		assert(_scene);

		//Configuramos colisiones. El grupo 1 y 2 no podr�n colisionar
		setGroupCollisions(2, 22, false);
	}

	//--------------------------------------------------------

	void CServer::destroyScene ()
	{
		assert(_instance);

		// TODO: Liberar la escena (m�todo release)
		if(_cpuDispatcher) {
			_cpuDispatcher->release();
			_cpuDispatcher = NULL;
		}

		if (_scene) {
			_scene->release();
			_scene = NULL;
		}
	}

	//--------------------------------------------------------

	bool CServer::tick(unsigned int msecs) 
	{
		assert(_scene);

		// Empezar la simulaci�n f�sica. Actualmente usamos intervalos de tiempo variables,
		// debemos tener cuidado porque PhysX puede no comportarse bien si el tiempo 
		// transcurrido es demasiado grande.
		_scene->simulate(msecs / 1000.0f);

		// Se podrian hacer cosas siempre que no toque los objetos fisicos (multihilo)

		// Esperamos a que la simulaci�n f�sica termine. En principio podr�amos utilizar
		// este intervalo de tiempo para hacer algo m�s �til. Existe una versi�n de este
		// m�todo no bloqueante.
		return _scene->fetchResults(true);
	} 

	//--------------------------------------------------------

	PxRigidStatic* CServer::createPlane(const Vector3 &point, const Vector3 &normal, int group, 
		unsigned int filterData, const IPhysics *component)
	{
		assert(_scene);

		// TODO: Crear un plano est�tico
		// 1. Transformar el punto y la normal a los tipos de PhysX
		// 2. Usar el material por defecto del servidor
		// 3. Crear el actor usando PxCreatePlane
		PxVec3 p = Vector3ToPxVec3(point);
		PxVec3 n = Vector3ToPxVec3(normal);
		PxPlane plane(p, n);
		PxRigidStatic *actor = PxCreatePlane(*_physics, plane, *_defaultMaterial);
		assert(actor);

		// TODO: enlazar el actor f�sico al componente l�gico
		// Usamos el atributo userData del actor para guardar la direcci�n del componente
		// l�gico encargado de la f�sica. 
		actor->userData = (void *) component;

		// TODO: Establecer el grupo de colisi�n
		// Usar la funci�n PxSetGroup
		PxSetGroup(*actor, group);

		// TODO: A�adir el actor a la escena
		_scene->addActor(*actor);

		setupFiltering(actor, group, filterData);

		return actor;
	}

	//--------------------------------------------------------

	PxRigidStatic* CServer::createStaticBox(const Vector3 &position, const Vector3 &dimensions, bool trigger, 
		int group, unsigned int filterData, const IPhysics *component)
	{
		assert(_scene);

		// Nota: PhysX coloca el sistema de coordenadas local en el centro de la caja, mientras
		// que la l�gica asume que el origen del sistema de coordenadas est� en el centro de la 
		// cara inferior. Para unificar necesitamos realizar una traslaci�n en el eje Y.
		// Afortunadamente, el descriptor que se usa para crear el actor permite definir esta 
		// transformaci�n local, por lo que la conversi�n entre sistemas de coordenadas es transparente. 

		// TODO: Crear un cubo est�tico
		// 0. Recuerda que siempre debes convertir entre los tipos de la l�gica y los de PhysX (vectores, etc).
		// 1. Crear pose (PxTransform) a partir de la posici�n
		// 2. Crear geometr�a de la caja (PxBoxGeometry) a partir de las dimensiones
		// 3. Usar el material por defecto del servidor de f�sica
		// 4. Aplicar una transformaci�n local (localPose) que desplace dimensions.y hacia arriba (eje Y positivo)
		// 5. Crear el actor usando la funci�n PxCreateStatic
		PxVec3 p = Vector3ToPxVec3(position);
		PxVec3 d = Vector3ToPxVec3(dimensions);
		PxTransform pose(p);
		d.x *= 0.5; d.y *= 0.5; d.z *= 0.5;
		PxBoxGeometry geom(d); 
		PxTransform localPose(PxVec3(0, d.y, 0));
		PxRigidStatic *actor = PxCreateStatic(*_physics, pose, geom, *_defaultMaterial, localPose);
		assert(actor);

		// TODO: Transformar el objeto est�tico en un trigger si es necesario
		// 1. Obtener la primera shape del actor
		// 2. Desactivar el flag PxShapeFlag::eSIMULATION_SHAPE
		// 3. Activar el flag PxShapeFlag::eTRIGGER_SHAPE
		if (trigger) {
			PxShape *shape;
			actor->getShapes(&shape, 1, 0);
			shape->setFlag(PxShapeFlag::eSIMULATION_SHAPE, false);
			shape->setFlag(PxShapeFlag::eTRIGGER_SHAPE, true);
			shape->setFlag(PxShapeFlag::eSCENE_QUERY_SHAPE,false);

		}

		// TODO: enlazar el actor f�sico al componente l�gico
		// Usamos el atributo userData del actor para guardar la direcci�n del componente
		// l�gico encargado de la f�sica. 
		actor->userData = (void *) component;

		// TODO: Establecer el grupo de colisi�n
		// Usar la funci�n PxSetGroup
		PxSetGroup(*actor, group);

		// TODO: A�adir el actor a la escena
		_scene->addActor(*actor);

		setupFiltering(actor, group, filterData);

		return actor;
	}

	//--------------------------------------------------------

	PxRigidDynamic* CServer::createDynamicBox(const Vector3 &position, float orientation, const Vector3 &dimensions, 
		float mass, bool kinematic, bool trigger, int group, unsigned int filterData,
		const IPhysics *component)
	{
		assert(_scene);

		// Nota: PhysX coloca el sistema de coordenadas local en el centro de la caja, mientras
		// que la l�gica asume que el origen del sistema de coordenadas est� en el centro de la 
		// cara inferior. Para unificar necesitamos realizar una traslaci�n en el eje Y.
		// Afortunadamente, el descriptor que se usa para crear el actor permite definir esta 
		// transformaci�n local, por lo que la conversi�n entre sistemas de coordenadas es transparente. 

		// Crear un cubo din�mico
		PxTransform pose(Vector3ToPxVec3(position));
		PxQuat quaternion(Math::fromDegreesToRadians(orientation), Vector3ToPxVec3(Vector3::UNIT_Y));
		pose.q =  quaternion;
		float density = mass / (dimensions.x * dimensions.y * dimensions.z);
		PxVec3 d = Vector3ToPxVec3(dimensions);
		d.x *= 0.5; d.y *= 0.5; d.z *= 0.5;
		PxBoxGeometry geom(d);
		PxMaterial *material = _defaultMaterial;
		PxTransform localPose(PxVec3(0, d.y, 0)); // Transformaci�n de coordenadas l�gicas a coodenadas de PhysX

		// TODO: Crear cubo din�mico o cinem�tico (seg�n el par�metro kinematic)
		// - Para din�mico usar PxCreateDynamic
		// - Para cinematico usar PxCreateKinematic
		// Asignarlo al actor;
		PxRigidDynamic *actor;
		if (kinematic)
			actor = PxCreateKinematic(*_physics, pose, geom, *material, density, localPose);
		else
			actor = PxCreateDynamic(*_physics, pose, geom, *material, density, localPose);
		assert(actor);

		// TODO: Transformar el objeto din�mico en un trigger si es necesario
		// 1. Obtener la primera shape del actor
		// 2. Desactivar el flag PxShapeFlag::eSIMULATION_SHAPE
		// 2. Activar el flag PxShapeFlag::eTRIGGER_SHAPE
		if (trigger) {
			PxShape *shape;
			actor->getShapes(&shape, 1, 0);
			shape->setFlag(PxShapeFlag::eSIMULATION_SHAPE, false);
			shape->setFlag(PxShapeFlag::eTRIGGER_SHAPE, true);
			shape->setFlag(PxShapeFlag::eSCENE_QUERY_SHAPE,false);

		}
		// TODO: enlazar el actor f�sico al componente l�gico
		// Usamos el atributo userData del actor para guardar la direcci�n del componente
		// l�gico encargado de la f�sica. 
		actor->userData = (void *) component;

		// TODO: Establecer el grupo de colisi�n
		// Usar la funci�n PxSetGroup
		PxSetGroup(*actor, group);

		// TODO: a�adir el actor a la escena
		_scene->addActor(*actor);

		setupFiltering(actor, group, filterData);

		return actor;
	}

	//--------------------------------------------------------

	PxRigidActor* CServer::createFromFile(const std::string &file, const Vector3 &position, float orientation,
		int group, unsigned int filterData, const IPhysics *component)
	{
		assert(_scene);

		// Preparar par�metros para deserializar
		PxDefaultFileInputData data(file.c_str());
		PxCollection* bufferCollection = _physics->createCollection();
		PxCollection* sceneCollection = _physics->createCollection();
		PxStringTable* stringTable = NULL; 
		PxUserReferences* externalRefs = NULL; 
		PxUserReferences* userRefs = NULL; 

		// Deserializar a partir del fichero RepX
		repx::deserializeFromRepX(data, *_physics, *_cooking, stringTable, externalRefs, 
			*bufferCollection, *sceneCollection, userRefs);

		// A�adir entidades f�sicas a la escena
		_physics->addCollection(*sceneCollection, *_scene); 

		// Buscar una entidad de tipo PxRigidActor. Asumimos que hay exactamente 1 en el fichero.
		PxRigidActor *actor = NULL;
		PxTransform pose(Vector3ToPxVec3(position), 
			PxQuat(Math::fromDegreesToRadians(orientation), Vector3ToPxVec3(Vector3::UNIT_Y))
			);
		for (unsigned int i = 0; (i < sceneCollection->getNbObjects()) && !actor; ++i) {
			PxSerializable *p = sceneCollection->getObject(i);
			actor = p->is<PxRigidActor>();
			actor->setGlobalPose(PxTransform(actor->getGlobalPose().p + pose.p, actor->getGlobalPose().q * pose.q));
		}
		assert(actor);

		// TODO: enlazar el actor f�sico al componente l�gico
		// Usamos el atributo userData del actor para guardar la direcci�n del componente
		// l�gico encargado de la f�sica. 
		actor->userData = (void *) component;

		// TODO: Establecer el grupo 
		// Usar la funci�n PxSetGroup
		PxSetGroup(*actor, group);

		setupFiltering(actor, group, filterData);

		// Liberar recursos
		bufferCollection->release();
		sceneCollection->release();

		return actor;
	}

	//--------------------------------------------------------

	bool CServer::createWorldFromFile(const std::string &file)
	{
		assert(_scene);

		if(_scene)
		{
			// Preparar par�metros para deserializar
			PxDefaultFileInputData data(file.c_str());
			PxCollection* bufferCollection = _physics->createCollection();
			PxCollection* sceneCollection = _physics->createCollection();
			PxStringTable* stringTable = NULL; 
			PxUserReferences* externalRefs = NULL; 
			PxUserReferences* userRefs = NULL; 

			// Deserializar a partir del fichero RepX
			repx::deserializeFromRepX(data, *_physics, *_cooking, stringTable, externalRefs, 
				*bufferCollection, *sceneCollection, userRefs);

			// A�adir entidades f�sicas a la escena
			_physics->addCollection(*sceneCollection, *_scene); 

			// Buscar una entidad de tipo PxRigidActor. Asumimos que hay exactamente 1 en el fichero.
			PxRigidActor *actor = NULL;
			for (unsigned int i = 0; (i < sceneCollection->getNbObjects()) && !actor; ++i) {
				PxSerializable *p = sceneCollection->getObject(i);
				actor = p->is<PxRigidActor>();
				// TODO: Establecer el grupo 
				// Usar la funci�n PxSetGroup
				PxSetGroup(*actor, 1);
				setupFiltering(actor, 1, FilterGroup::EVERYTHING);
			}
			assert(actor);

			// Liberar recursos
			bufferCollection->release();
			sceneCollection->release();

			return true;
		}
		return false;
	}

	//--------------------------------------------------------

	void CServer::destroyActor(physx::PxActor *actor)
	{
		assert(_scene);

		// Eliminar el actor de la escena
		_scene->removeActor(*actor);

		// Liberar recursos
		actor->release();
	}

	//--------------------------------------------------------

	Matrix4 CServer::getActorTransform(const PxRigidActor *actor)
	{
		assert(actor);

		// Devolver la posici�n y orientaci�n en coordenadas l�gicas
		return PxTransformToMatrix4(actor->getGlobalPose());
	}

	//--------------------------------------------------------

	void CServer::moveKinematicActor(physx::PxRigidDynamic *actor, const Matrix4 &transform)
	{
		assert(actor);
		assert(isKinematic(actor));

		// Mover el actor tras transformar el destino a coordenadas l�gicas
		actor->setKinematicTarget(Matrix4ToPxTransform(transform));
	}

	//--------------------------------------------------------

	void CServer::moveKinematicActor(physx::PxRigidDynamic *actor, const Vector3 &displ)
	{
		assert(actor);
		assert(isKinematic(actor));

		// TODO: desplazar el actor cinem�tico
		// 1. Obtener la matriz de transformaci�n del actor
		// 2. Aplicarle el desplazamiento para obtener la posici�n de destino 
		//    (conversi�n de coordenada l�gicas a f�sicas)
		// 3. Mover el actor f�sico a la posici�n de destino usando el m�todo 
		//    adecuado para actores cinem�ticos.
		PxTransform t = actor->getGlobalPose();
		t.p += Vector3ToPxVec3(displ);
		actor->setKinematicTarget(t);
	}

	//--------------------------------------------------------

	void CServer::rotateKinematicActor(physx::PxRigidDynamic *actor, float angle, const Vector3 &rotation)
	{
		assert(actor);
		assert(isKinematic(actor));

		// TODO: rotar el actor cinem�tico
		// 1. Obtener la matriz de transformaci�n del actor
		// 2. Aplicarle el desplazamiento para obtener la posici�n de destino 
		//    (conversi�n de coordenada l�gicas a f�sicas)
		// 3. Mover el actor f�sico a la posici�n de destino usando el m�todo 
		//    adecuado para actores cinem�ticos.
		PxTransform t = actor->getGlobalPose();
		PxQuat quaternion(Math::fromDegreesToRadians(angle), Vector3ToPxVec3(rotation));
		t.q *=  quaternion;
		actor->setKinematicTarget(t);
	}

	//--------------------------------------------------------

	bool CServer::isKinematic(const PxRigidDynamic *actor)
	{
		assert(actor);

		return actor->getRigidDynamicFlags() & PxRigidDynamicFlag::eKINEMATIC;
	}

	//--------------------------------------------------------

	PxCapsuleController* CServer::createCapsuleController(const Vector3 &position, float radius, 
		float height, int collisionGroup, unsigned int filterData, const CPhysicController *component)
	{
		assert(_scene);

		// Nota: PhysX coloca el sistema de coordenadas local en el centro de la c�psula, mientras
		// que la l�gica asume que el origen del sistema de coordenadas est� en los pi�s del 
		// jugador. Para unificar necesitamos realizar una traslaci�n en el eje Y.
		// Desafortunadamente, el descriptor que se usa para crear los controllers no permite
		// definir esta transformaci�n local (que s� permite al crear un actor), por lo que
		// tendremos que realizar la traslaci�n nosotros cada vez. 

		// TODO: transformar entre distemas de coordenadas l�gico y de PhysX
		// 1. offsetY = altura / 2 + radio
		// 2. Transformar entre vector l�gico y vector de PhysX

		float offsetY = height / 2 + radius;
		PxVec3 pos = Vector3ToPxVec3(position + Vector3(0, offsetY, 0));

		// TODO: crear descriptor de controller de tipo c�psula (PxCapsuleControllerDesc)
		// - usar material por defecto del servidor (_defaultMaterial)
		// - usar climbingMode PxCapsuleClimbingMode::eEASY
		PxCapsuleControllerDesc desc;
		desc.position = PxExtendedVec3(pos.x, pos.y, pos.z);
		desc.height = height;
		desc.radius = radius;
		desc.material = _defaultMaterial;
		desc.climbingMode = PxCapsuleClimbingMode::eCONSTRAINED;
		desc.stepOffset = 0.1f;

		// TODO: asociar el gestor de colisiones al controller
		// Asignar el gestor de colisiones al atributo callback del descriptor del controller
		desc.callback = _collisionManager;

		// TODO: enlazar el controller f�sico al componente l�gico
		// Usamos el atributo userData del descriptor del controller para guardar la 
		// direcci�n del componente l�gico encargado de la f�sica. 
		desc.userData = (void *) component;

		// TODO: crear controller a partir del _controllerManager
		PxCapsuleController *controller = 
			(PxCapsuleController *)_controllerManager->createController(*_physics, _scene, desc);

		// TODO: enlazar el actor f�sico del controller al componente l�gico
		// Usamos el atributo userData del actor para guardar la direcci�n del componente
		// l�gico encargado de la f�sica (no es autom�tico)
		controller->getActor()->userData = (void *) component;

		// Asignar el grupo de colision
		controller->setGroupsBitmask(collisionGroup);

		setupFiltering(controller->getActor(), collisionGroup, filterData);

		return controller;
	}

	//--------------------------------------------------------

	void CServer::setGroupCollisions(int group1, int group2, bool enable)
	{
		// Activar / desactivar colisiones entre grupos
		PxSetGroupCollisionFlag(group1, group2, enable);
	}

	//--------------------------------------------------------

	void CServer::setupFiltering(physx::PxRigidActor* actor, unsigned int filterGroup, unsigned int filterMask)
	{
		PxFilterData filterData;
		filterData.word0 = filterGroup; // word0 = own ID
		filterData.word1 = filterMask;  // word1 = ID mask to filter pairs that trigger a contact callback;
		const PxU32 numShapes = actor->getNbShapes();
		PxShape** shapes = (PxShape**)_allocator->allocate(sizeof(PxShape*)*numShapes,"","",0);
		actor->getShapes(shapes, numShapes);
		for(PxU32 i = 0; i < numShapes; ++i)
		{
			PxShape* shape = shapes[i];
			shape->setSimulationFilterData(filterData);
			shape->setQueryFilterData(filterData);
			//shape->setFlag(PxShapeFlag::eUSE_SWEPT_BOUNDS, true);
		}
		_allocator->deallocate(shapes);
	}


	//--------------------------------------------------------

	Logic::CEntity* CServer::raycastClosest (const Ray& ray, float maxDist) const
	{
		assert(_scene);

		// TODO: 1. Inicializar par�metros (origen, direcci�n, distancia,...)
		PxVec3 origin = Vector3ToPxVec3(ray.getOrigin());      // origen     
		PxVec3 unitDir = Vector3ToPxVec3(ray.getDirection());  // direcci�n normalizada   
		PxReal maxDistance = maxDist;                          // distancia m�xima
		PxRaycastHit hit;                 
		const PxSceneQueryFlags outputFlags = PxSceneQueryFlag::eDISTANCE | PxSceneQueryFlag::eIMPACT 
			| PxSceneQueryFlag::eNORMAL; // Info que queremos recuperar	

		// TODO: 2. Lanzar rayo al objeto m�s cercano
		// TODO: 3. Devolver entidad l�gica asociada a la entidad f�sica impactada
		if (_scene->raycastSingle(origin, unitDir, maxDistance, outputFlags, hit))
		{
			PxRigidActor *actor = &hit.shape->getActor();
			IPhysics *component = (IPhysics *) actor->userData;
			if (component) {
				return component->getEntity();
			}
		}
		return NULL;
	}

	//--------------------------------------------------------

	Logic::CEntity* CServer::raycastClosest(const Ray& ray, float maxDist, int group) const 
	{
		assert(_scene);

		// Establecer par�metros del rayo
		PxVec3 origin = Vector3ToPxVec3(ray.getOrigin());      // origen     
		PxVec3 unitDir = Vector3ToPxVec3(ray.getDirection());  // direcci�n normalizada   
		PxReal maxDistance = maxDist;                          // distancia m�xima
		PxRaycastHit hit;                 
		const PxSceneQueryFlags outputFlags;				   // Info que queremos recuperar	

		// Lanzar el rayo
		PxRaycastHit hits[64];
		bool blockingHit;
		PxI32 nHits = _scene->raycastMultiple(origin, unitDir, maxDistance, outputFlags, hits, 64, blockingHit); 

		// Buscar un actot que pertenezca al grupo de colisi�n indicado
		for (int i = 0; i < nHits; ++i) {
			PxRigidActor *actor = &hits[i].shape->getActor();
			if (PxGetGroup(*actor) == group) {
				IPhysics *component = (IPhysics *) actor->userData;
				if (component) {
					return component->getEntity();
				}
			}
		}



		return NULL;

		// Nota: seguro que se puede hacer de manera mucho m�s eficiente usando los filtros
		// de PhysX.
	}

	//--------------------------------------------------------

	bool CServer::raycastClosestNoEntity(const Ray& ray, float maxDist,Vector3 &collisionPoint,Vector3 &collisionNormal)
	{
		assert(_scene);

		// TODO: 1. Inicializar par�metros (origen, direcci�n, distancia,...)
		PxVec3 origin = Vector3ToPxVec3(ray.getOrigin());      // origen     
		PxVec3 unitDir = Vector3ToPxVec3(ray.getDirection());  // direcci�n normalizada   
		PxReal maxDistance = maxDist;                          // distancia m�xima
		PxRaycastHit hit;                 
		const PxSceneQueryFlags outputFlags = PxSceneQueryFlag::eIMPACT | PxSceneQueryFlag::eNORMAL; // Info que queremos recuperar	

		// TODO: 2. Lanzar rayo al objeto m�s cercano
		// TODO: 3. Devolver entidad l�gica asociada a la entidad f�sica impactada

		if(_scene->raycastSingle(origin, unitDir, maxDistance, outputFlags, hit))
		{
			collisionPoint.x = hit.impact.x;
			collisionPoint.y = hit.impact.y;
			collisionPoint.z = hit.impact.z;

			collisionNormal.x = hit.normal.x;
			collisionNormal.y = hit.normal.y;
			collisionNormal.z = hit.normal.z;

			return true;
		}
		return false;

		// Nota: seguro que se puede hacer de manera mucho m�s eficiente usando los filtros
		// de PhysX.
	}


	bool CServer::raycastClosestNoEntity(const Ray& ray, float maxDist)
	{
		assert(_scene);

		// TODO: 1. Inicializar par�metros (origen, direcci�n, distancia,...)
		PxVec3 origin = Vector3ToPxVec3(ray.getOrigin());      // origen     
		PxVec3 unitDir = Vector3ToPxVec3(ray.getDirection());  // direcci�n normalizada   
		PxReal maxDistance = maxDist;                          // distancia m�xima
		PxRaycastHit hit;                 
		const PxSceneQueryFlags outputFlags = PxSceneQueryFlag::eIMPACT | PxSceneQueryFlag::eNORMAL; // Info que queremos recuperar	

		// TODO: 2. Lanzar rayo al objeto m�s cercano
		// TODO: 3. Devolver entidad l�gica asociada a la entidad f�sica impactada

		return _scene->raycastSingle(origin, unitDir, maxDistance, outputFlags, hit);

		// Nota: seguro que se puede hacer de manera mucho m�s eficiente usando los filtros
		// de PhysX.
	}

	//--------------------------------------------------------

	std::vector<Collision> CServer::batchedqueries(std::vector<Ray>rayList, std::vector<float> maxDistList, 
		std::vector<int> groups) const
	{
		std::vector<Collision> out;

		// Definimos filtros
		//PxSceneQueryFilterData filterData(PxSceneQueryFilterFlag::eDYNAMIC);
		PxSceneQueryFilterData filterData = PxSceneQueryFilterData();
		filterData.data.word0 = groups[0];
		for (unsigned int i = 1; i < groups.size(); ++i)
			filterData.data.word0 |= groups[i];

		PxVec3 origin, unitDir;
		PxReal maxDistance;
		PxRaycastHit hit;
		const PxSceneQueryFlags outputFlags = PxSceneQueryFlag::eDISTANCE | PxSceneQueryFlag::eIMPACT 
			| PxSceneQueryFlag::eNORMAL; // Info que queremos recuperar	
		for (unsigned int i = 0; i < rayList.size(); ++i)
		{
			origin = Vector3ToPxVec3(rayList[i].getOrigin());      // origen 
			unitDir = Vector3ToPxVec3(rayList[i].getDirection());  // direcci�n normalizada 
			maxDistance = maxDistList[i];                          // distancia m�xima
			if (_scene->raycastSingle(origin, unitDir, maxDistance, outputFlags, hit, filterData))
			{
				Collision col;
				col.normal = PxVec3ToVector3(hit.normal);
				col.position = PxVec3ToVector3(origin + unitDir * hit.distance);
				col.distance = hit.distance;
				out.push_back(col);
			}
		}

		return out;
	}

	//--------------------------------------------------------

	bool CServer::raycastSingle(const Ray& ray, float maxDist, int group) const
	{
		// Definimos filtros
		//PxSceneQueryFilterData filterData(PxSceneQueryFilterFlag::eDYNAMIC);
		PxSceneQueryFilterData filterData = PxSceneQueryFilterData();
		filterData.data.word0 = group;

		const PxSceneQueryFlags outputFlags = PxSceneQueryFlag::eDISTANCE | PxSceneQueryFlag::eIMPACT 
			| PxSceneQueryFlag::eNORMAL; // Info que queremos recuperar	

		PxVec3 origin = Vector3ToPxVec3(ray.getOrigin());      // origen 
		PxVec3 unitDir = Vector3ToPxVec3(ray.getDirection());  // direcci�n normalizada 
		PxReal maxDistance = maxDist;
		PxRaycastHit hit;
		if (_scene->raycastSingle(origin, unitDir, maxDistance, outputFlags, hit, filterData))
			return true;

		return false;
	}

	//--------------------------------------------------------

	bool CServer::raycastSingle(const Ray& ray, float maxDist, int group, Collision &col) const
	{
		// Definimos filtros
		//PxSceneQueryFilterData filterData(PxSceneQueryFilterFlag::eDYNAMIC);
		PxSceneQueryFilterData filterData = PxSceneQueryFilterData();
		filterData.data.word0 = group;

		const PxSceneQueryFlags outputFlags = PxSceneQueryFlag::eDISTANCE | PxSceneQueryFlag::eIMPACT 
			| PxSceneQueryFlag::eNORMAL; // Info que queremos recuperar	

		PxVec3 origin = Vector3ToPxVec3(ray.getOrigin());      // origen 
		PxVec3 unitDir = Vector3ToPxVec3(ray.getDirection());  // direcci�n normalizada 
		PxReal maxDistance = maxDist;
		PxRaycastHit hit;
		if (_scene->raycastSingle(origin, unitDir, maxDistance, outputFlags, hit, filterData))
		{
			col.normal = PxVec3ToVector3(hit.normal);
			col.position = PxVec3ToVector3(origin + unitDir * hit.distance);
			col.distance = hit.distance;
			return true;
		}
		return false;
	}

	//--------------------------------------------------------

	std::vector<Logic::CEntity *> *CServer::raycastMultiple(const Ray& ray, float maxDist, int group) const
	{
		// Definimos filtros
		//PxSceneQueryFilterData filterData(PxSceneQueryFilterFlag::eDYNAMIC);
		PxSceneQueryFilterData filterData = PxSceneQueryFilterData();
		filterData.data.word0 = group;

		const PxSceneQueryFlags outputFlags = PxSceneQueryFlag::eDISTANCE | PxSceneQueryFlag::eIMPACT 
			| PxSceneQueryFlag::eNORMAL; // Info que queremos recuperar	

		PxVec3 origin = Vector3ToPxVec3(ray.getOrigin());      // origen 
		PxVec3 unitDir = Vector3ToPxVec3(ray.getDirection());  // direcci�n normalizada 
		PxReal maxDistance = maxDist;
		PxRaycastHit hit;
		PxRaycastHit hits[64];
		bool blockingHit;
		PxI32 nHits = _scene->raycastMultiple(origin, unitDir, maxDistance, outputFlags, hits, 64, blockingHit); 
		if (nHits == 0) return NULL;
		std::vector<Logic::CEntity *> *out = new std::vector<Logic::CEntity *>();
		// Recorremos la lista de colisiones a�adiendolas a un vector
		for (int i = 0; i < nHits; ++i) {
			PxRigidActor *actor = &hits[i].shape->getActor();
			IPhysics *component = (IPhysics *) actor->userData;
			if (component) {
				out->push_back(component->getEntity());
			}
		}

		return out;
	}

	//--------------------------------------------------------

	bool CServer::sweepSingle(const Ray& ray, float maxDist, int group, Collision &col, Vector3 dimensions)
	{
		PxSceneQueryFilterData filterData = PxSceneQueryFilterData();
		filterData.data.word0 = group;

		const PxSceneQueryFlags outputFlags = PxSceneQueryFlag::eDISTANCE | PxSceneQueryFlag::eIMPACT 
			| PxSceneQueryFlag::eNORMAL | PxSceneQueryFlag::eINITIAL_OVERLAP 
			| PxSceneQueryFlag::eINITIAL_OVERLAP_KEEP ; // Info que queremos recuperar	

		PxSweepHit hit;
		PxVec3 origin = Vector3ToPxVec3(ray.getOrigin());      // origen 
		PxVec3 unitDir = Vector3ToPxVec3(ray.getDirection());  // direcci�n normalizada 
		PxReal maxDistance = maxDist;
		dimensions.x /= 2; dimensions.y /= 2; dimensions.z /= 2;
		PxBoxGeometry geometry(Vector3ToPxVec3(dimensions)); // creamos una caja con las dimensiones dadas
		PxTransform pose = PxTransform(origin);             // [in] Pose of the object

		if (_scene->sweepSingle(geometry, pose, unitDir, maxDist, outputFlags, hit, filterData))
		{
			col.normal = PxVec3ToVector3(hit.normal);
			col.distance = hit.distance;
			col.position = PxVec3ToVector3(hit.impact);
			return true;
		}
		return false;
	}
}