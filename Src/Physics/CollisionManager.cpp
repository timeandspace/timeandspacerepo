/**
@file CollisionManager.cpp

Contiene la implementaci�n del gestor de colisiones.

@see Physics::CCollisionManager

@author Antonio S�nchez Ruiz-Granados
@date Noviembre, 2012
*/

#include "CollisionManager.h"
#include "Logic/Entity/Components/PhysicController.h"
#include "Logic/Entity/Components/PhysicEntity.h"
#include "Conversions.h"

#include <PxRigidActor.h>
#include <PxShape.h> 
#include <PxSimulationEventCallback.h> 

using namespace Physics;
using namespace Logic;
using namespace physx;

//--------------------------------------------------

CCollisionManager::CCollisionManager()
{

}

//--------------------------------------------------

CCollisionManager::~CCollisionManager()
{

}

//--------------------------------------------------

void CCollisionManager::onConstraintBreak(PxConstraintInfo *constraints, PxU32 count)
{
	// Por ahora ignoramos estos mensajes	
}

//--------------------------------------------------

void CCollisionManager::onWake(PxActor **actors, PxU32 count)
{
	// Por ahora ignoramos estos mensajes	
}

//--------------------------------------------------

void CCollisionManager::onSleep(PxActor **actors, PxU32 count)
{
	// Por ahora ignoramos estos mensajes	
}

//--------------------------------------------------

void CCollisionManager::onContact(const PxContactPairHeader &pairHeader, const PxContactPair *pairs, PxU32 nbPairs) 
{
	// Por ahora ignoramos estos mensajes	
	for(PxU32 i=0; i < nbPairs; i++)
	{
		const PxContactPair& cp = pairs[i];

		if (cp.events & PxPairFlag::eNOTIFY_TOUCH_FOUND)
		{
			IPhysics *component = (IPhysics *)pairHeader.actors[0]->userData;
			IPhysics *otherComponent = (IPhysics *)pairHeader.actors[1]->userData;
			if (component && pairHeader.actors[0]->isRigidDynamic())
				component->onContact(otherComponent);
			if (otherComponent && pairHeader.actors[1]->isRigidDynamic())
				otherComponent->onContact(component);
		}
	}

}

//--------------------------------------------------

void CCollisionManager::onTrigger(PxTriggerPair *pairs, PxU32 count)
{
	// Recorrer el array de colisiones
	for (unsigned int i = 0; i < count; ++i) {

		// Ignoramos los pares en los que alguna de las shapes (del trigger o de la otra entidad) ha sido borrada
		if (pairs[i].flags & (PxTriggerPairFlag::eDELETED_SHAPE_TRIGGER | PxTriggerPairFlag::eDELETED_SHAPE_OTHER))
			continue;

		// Comprobamos si estamos saliendo o entrando en el trigger
		bool enter = pairs[i].status & PxPairFlag::eNOTIFY_TOUCH_FOUND;
		bool exit = pairs[i].status & PxPairFlag::eNOTIFY_TOUCH_LOST;

		// S�lo tenemos en cuenta los eventos de entrada y salida del trigger. En PhysX 2.8 y anteriores
		// tambi�n se notificada si el objeto permanec�a dentro del trigger. La documentaci�n no deja muy
		// claro si este comportamiento se ha eliminado de manera definitiva.
		if (!enter && !exit)
			continue;

		// TODO: notificar a los componentes f�sicos de la l�gica las colisiones
		// 1. Obtener el componente l�gico (IPhysics) asociado al trigger f�sico.
		// 2. Obtener el componente l�gico (IPhysics) asociado a la otra entidad f�sica.
		// 3. Notificar a ambos componentes la colisi�n
		IPhysics *triggerComponent = (IPhysics *)pairs[i].triggerShape->getActor().userData;
		IPhysics *otherComponent = (IPhysics *)pairs[i].otherShape->getActor().userData;

		triggerComponent->onTrigger(otherComponent, enter);
		if (otherComponent)
			otherComponent->onTrigger(triggerComponent, enter);
	}	
}

//--------------------------------------------------

void CCollisionManager::onShapeHit(const PxControllerShapeHit &hit)
{
	// TODO: notificar al componente f�sico la colisi�n con una entidad
	// 1. Obtener el puntero al componente f�sico (CPhysicController)
	// 2. Notificar la colisi�n al componente f�sico
	CPhysicController *component = (CPhysicController *) hit.controller->getUserData();

	component->onShapeHit(hit);
}

//--------------------------------------------------

void CCollisionManager::onControllerHit(const PxControllersHit &hit) 
{
	// TODO: notificar al componente f�sico la colisi�n con otro controller
	// 1. Obtener el puntero al componente f�sico (CPhysicController)
	// 2. Notificar la colisi�n al componente f�sico
	CPhysicController *component = (CPhysicController *) hit.controller->getUserData();

	component->onControllerHit(hit);
}

//--------------------------------------------------

void CCollisionManager::onObstacleHit(const PxControllerObstacleHit &hit)
{
	// Por ahora ignoramos estos mensajes	
}

//--------------------------------------------------

