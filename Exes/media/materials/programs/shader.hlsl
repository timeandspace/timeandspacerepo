//-------------- VERTEX SHADER --------------//

float4x4 ViewProjectionMatrix;
float Thickness;
float Time;
float4 borderColor;
float Alpha;
float MaxAlpha;
struct VsInput
{
	float4 position : POSITION;
	float3 normal : NORMAL;
	float2 uv0 : TEXCOORD0;
};

struct VsOutput
{
	float4 color : COLOR;
	float4 position : POSITION;
	float2 uv0 : TEXCOORD0;
	float3 objectPos : TEXCOORD1;
	float3 normal : TEXCOORD2;
};

VsOutput vsMain (VsInput vsIn) {
	VsOutput vsOut;
	
	float4 pos = vsIn.position;
	pos += float4(normalize(vsIn.normal), 0) * Thickness * Time;
	
	vsOut.position = mul(ViewProjectionMatrix, pos);

	vsOut.objectPos = vsIn.position;
	vsOut.normal = vsIn.normal;
	vsOut.uv0 = vsIn.uv0;
	vsOut.color = float4(1, 1, 1, 1 - (Alpha / MaxAlpha));

	return vsOut;
}

//-------------- PIXEL SHADER --------------//

struct PsInput
{
	float2 uv0 : TEXCOORD0;
	float4 c : COLOR;
};

float4 main(VsOutput psIn) : COLOR {

	//return float4(1, 1, 1, 1);
	return float4(borderColor.xyz, borderColor.a);
}