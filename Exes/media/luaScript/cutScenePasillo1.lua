--------------------------------
-- CUT SCENE PASILLO 1 --
--------------------------------

-----------------------------------------------------
-- Funcion auxiliar que se encarga de volver 
-- a colocar las puertas en su sitio
------------------------------------
function finish_cutScenePasillo1 ()
	cutScene:sendSwitchMessage(0, false)
	cutScene:sendSwitchMessage(1, false)
end

-------------------------------
-- CutScene del pasillo 1
-------------------------------
function cutScenePasillo1 (count, msecs)

	local cameraSpeed = 8 * msecs * 0.001
	local trackNodeSpeed = 8 * msecs * 0.001
	local dir = Vector3.ZERO
	local dirTackNode = Vector3.ZERO
	
	count = count - 5000
	-- Espero 1 seg. para evitar que pegue tanto tirón con el V-Sync activado
	if count == 1000 then
		camera:setCameraPosition(Vector3(5, 3, 5))
		camera:setTargetCameraPosition(Vector3(10, 3, 5))
	
	elseif count > 1000 and count <= 9000 then
		dir = Vector3.UNIT_X
		dirTackNode = Vector3.UNIT_X
	elseif count > 9000 and count < 10000 then
		dir = Vector3.UNIT_X
		dirTackNode = Vector3(Vector3(77.7, 3, 28.0) - camera:getTargetCameraPosition())
	elseif count > 10000 and count < 11500 then
		dir = Vector3.UNIT_Z
		dirTackNode = Vector3(Vector3(77.7, 3, 28.0) - camera:getTargetCameraPosition())
		
	elseif count == 11500 then
		finish_cutScenePasillo1()
		return true
	end
	
	dir = Vector3(dir * cameraSpeed)
	dirTackNode:normalise()
	dirTackNode = Vector3(dirTackNode * trackNodeSpeed)
	camera:setCameraPosition(Vector3(camera:getCameraPosition() + dir))
	camera:setTargetCameraPosition(Vector3(camera:getTargetCameraPosition() + dirTackNode))
	
	-- Activamos las puertas y/o plataformas
	local pos = Vector3.ZERO
	pos = Vector3(camera:getCameraPosition() + Vector3.ZERO)
	if vectorEquals(pos, Vector3(15, 3, 5), 0.2) then
		cutScene:sendSwitchMessage(0, true)
	elseif vectorEquals(pos, Vector3(45, 3, 5), 0.2) then
		cutScene:sendSwitchMessage(1, true)
	end
	
	return false
end
