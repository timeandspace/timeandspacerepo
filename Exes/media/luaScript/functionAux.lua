function vectorEquals(a, b, tolerance)
	
	local x = Vector3(a).x - Vector3(b).x
	local y = Vector3(a).y - Vector3(b).y
	local z = Vector3(a).z - Vector3(b).z
	
	if math.abs(x) <= tolerance and math.abs(y) <= tolerance and math.abs(z) <= tolerance then
		return true;
	end
	
	return false
end
