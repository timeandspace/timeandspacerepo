-----------------------------------
-- CUTSCENE PUZLE ALPHA --
-----------------------------------
function finish_cutScene1 ()
	cutScene:sendSwitchMessage(2, true)
end

function cutScene1 (count, msecs)
	
	local cameraSpeed  = 4 * msecs * 0.001
	local dir = Vector3(camera:getTargetCameraPosition() - camera:getCameraPosition())
	
	-- ñapa para las platformas con autoStart, se desactivan al inicio y se activan al finalizar el cutScene
	if count == 200 then
		cutScene:sendSwitchMessage(2, false)
		camera:setCameraPosition(Vector3(100, 30, 20))
		camera:setTargetCameraPosition(Vector3(camera:getCameraPosition()+dir))
	end
	count = count - 2000
	if count == 1000 then
		dir = Vector3(1, 0, 0)
		camera:setCameraPosition(Vector3(1, 3, 2))
		camera:setTargetCameraPosition(Vector3(camera:getCameraPosition()+dir))
	elseif  count > 1000 and count < 4250 then
		local dir2 = Vector3(dir * cameraSpeed)
		camera:setCameraPosition(Vector3(camera:getCameraPosition() + dir2))
		camera:setTargetCameraPosition(Vector3(camera:getCameraPosition() + dir))
	elseif  count >= 4250 and count < 6000 then
		local x = 0.2 * msecs * 0.001
		dir2 = Vector3(Vector3(0, 0, 1) * x)
		dir = dir + dir2
		camera:setCameraPosition(Vector3(camera:getCameraPosition() + Vector3(dir * cameraSpeed)))
		camera:setTargetCameraPosition(Vector3(camera:getCameraPosition() + dir))
	elseif  count >= 6000 and count < 9000 then
		local x = 0.2 * msecs * 0.001
		dir2 = Vector3(Vector3(0, 0, 1) * x)
		dir = dir - dir2
		camera:setCameraPosition(Vector3(camera:getCameraPosition() + Vector3(dir * cameraSpeed * 0.5 ) + Vector3(Vector3(0, 1, 0) * (1.6 * msecs * 0.001))))
		camera:setTargetCameraPosition(Vector3(camera:getCameraPosition() + dir))
	elseif  count >= 9000 and count < 10000 then
		x = 0.3 * msecs * 0.001
		camera:setCameraPosition(Vector3(camera:getCameraPosition() + Vector3(Vector3(0, 1, 0) * x)))
	elseif  count >= 10000 and count < 25000 then
		if count < 16000 then
			x = 0.2 * msecs * 0.001
			camera:setTargetCameraPosition(camera:getTargetCameraPosition() + Vector3(Vector3(-1, -0.2, -1) * x))
		else
			camera:setTargetCameraPosition(camera:getTargetCameraPosition() + Vector3(Vector3(-0.8, -0.3, 1.2) * x))
		end
	elseif  count == 25000 then
		finish_cutScene1()
		return true
	end

	return false
end
