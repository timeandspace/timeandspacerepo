-----------------------------------------------------------------------------------
-- CutScene que se activa al pulsar el botón que activa el ascensor
-- del nivel Puzle Alpha
-----------------------------------------------------------------------------------
function cutSceneAlpha2 (count, msecs)
	
	local cameraSpeed  = 1.1 * msecs * 0.001
	local trackNodeSpeed = 4 * msecs * 0.001
	local dir = Vector3.ZERO
	local dirTackNode = Vector3.ZERO
	-- (28.821, 0, 18) posicion donde se encuentra la plataforma y por tanto posicion final del target
	dirTackNode = Vector3(Vector3(20.018, 3, -2.076) - camera:getTargetCameraPosition())
	dirTackNode:normalise()
	if count == 500 then
		dir = Vector3(1, 0, 0)
		-- camera:setCameraPosition(Vector3(19, 4, 6))
		camera:setTargetCameraPosition(Vector3(25, 2, 6))
	elseif  count > 500 and count < 3000 then
		dir = Vector3(-0.8, 0, -1.2)
		dir = Vector3(dir * cameraSpeed)
		--camera:setCameraPosition(Vector3(camera:getCameraPosition() + dir))
		
		dirTackNode = Vector3(dirTackNode * trackNodeSpeed)
		vect = Vector3(camera:getTargetCameraPosition() + dirTackNode)
		camera:setTargetCameraPosition(Vector3(camera:getTargetCameraPosition() + dirTackNode))
	elseif  count == 4000 then
		return true
	end

	return false
end
