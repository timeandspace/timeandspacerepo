-----------------------------------------------------------------------------------
-- CutScene que se activa al pulsar el botón situado en la plataforma 
-- superior que activa la plataforma que sale y se oculta de la pared
-----------------------------------------------------------------------------------
function cutSceneAlpha1 (count, msecs)
	
	local cameraSpeed  = 1.1 * msecs * 0.001
	local trackNodeSpeed = 4 * msecs * 0.001
	local dir = Vector3.ZERO
	local dirTackNode = Vector3.ZERO
	-- (28.821, 0, 18) posicion donde se encuentra la plataforma y por tanto posicion final del target
	dirTackNode = Vector3(Vector3(28.821, 0, 18) - camera:getTargetCameraPosition())
	dirTackNode:normalise()
	if count == 500 then
		dir = Vector3(1, 0, 0)
		camera:setCameraPosition(Vector3(31, 8, 6.777))
		camera:setTargetCameraPosition(Vector3(37.796, 4.525, 6.777))
	elseif  count > 500 and count < 4000 then
		dir = Vector3(-0.8, 0, -1.2)
		dir = Vector3(dir * cameraSpeed)
		camera:setCameraPosition(Vector3(camera:getCameraPosition() + dir))
		
		dirTackNode = Vector3(dirTackNode * trackNodeSpeed)
		vect = Vector3(camera:getTargetCameraPosition() + dirTackNode)
		camera:setTargetCameraPosition(Vector3(camera:getTargetCameraPosition() + dirTackNode))
	elseif  count == 5000 then
		return true
	end

	return false
end
