--------------------------------
-- CUTSCENE PASILLO 2 --
--------------------------------

-----------------------------------------------------
-- Funcion auxiliar que se encarga de volver 
-- a colocar las puertas en su sitio
------------------------------------
function finish_cutScenePasillo2Boton ()

end

-------------------------------
-- CutScene del pasillo 2 que se activa al pulsar el boton que se encuentra 
-- al fianl de la plataforma transparente
-------------------------------
function cutScenePasillo2Boton (count, msecs)

	local cameraSpeed = 8 * msecs * 0.001
	local trackNodeSpeed = 8 * msecs * 0.001
	local dir = Vector3.ZERO
	local dirTackNode = Vector3.ZERO
	-- Espero 1 seg. para evitar que pegue tanto tirón con el V-Sync activado
	if count == 50 then
		camera:setCameraPosition(Vector3(40, 0, 4.8))
		camera:setTargetCameraPosition(Vector3(30.446, -2, 8.238))
	elseif count == 2000 then
		finish_cutScenePasillo2Boton()
		return true
	end

	return false
end
