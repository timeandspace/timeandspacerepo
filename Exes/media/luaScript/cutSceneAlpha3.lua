-----------------------------------------------------------------------------------
-- CutScene que se activa al pulsar el botón situado en la plataforma 
-- superior que activa la plataforma que sale y se oculta de la pared
-----------------------------------------------------------------------------------
function cutSceneAlpha3 (count, msecs)
	
	if count == 25 then
		dir = Vector3(1, 0, 0)
		camera:setCameraPosition(Vector3(30, 3, 11.564))
		camera:setTargetCameraPosition(Vector3(42.4, 0, 11.564))
	elseif  count == 3000 then
		return true
	end

	return false
end
