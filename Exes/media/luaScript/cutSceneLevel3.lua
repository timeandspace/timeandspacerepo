------------------------------------------------
-- CUTSCENE PUZLE JUAN 2 (LEVEL 3) --
------------------------------------------------
function finish_cutSceneLevel3 ()
	--cutScene:sendSwitchMessage(0, true)
	cutScene:sendSwitchMessage(1, true)
	cutScene:sendSwitchMessage(3, true)
	cutScene:sendSwitchMessage(2, true)
end

function cutSceneLevel3 (count, msecs)
	
	local cameraSpeed = 0.02 * msecs * 0.001
	local trackNodeSpeed = 1 * msecs * 0.001
	local dir = Vector3.ZERO
	local dirTackNode = Vector3.ZERO
	local yOffset = 1.1 * msecs * 0.001
	
	if count == 200 then
		cutScene:sendSwitchMessage(0, false)
		cutScene:sendSwitchMessage(1, false)
		cutScene:sendSwitchMessage(3, false)
		cutScene:sendSwitchMessage(2, false)
		camera:setCameraPosition(Vector3(100, 30, 20))
		camera:setTargetCameraPosition(Vector3(camera:getCameraPosition()+dir))
	end
	count = count - 3600 --delay para evitar tirones
	if count == 200 then
		camera:setCameraPosition(Vector3(0, 0, 0))
		camera:setTargetCameraPosition(Vector3(camera:getCameraPosition()+dir))
	end
	if count > 200 then
		yOffset = camera:getCameraPosition().y + yOffset
		camera:setCameraPosition(Vector3(7 * math.sin(count * cameraSpeed), yOffset, -7 * math.cos(count * cameraSpeed)))
		dirTackNode = Vector3.UNIT_Y
		dirTackNode = Vector3(dirTackNode * trackNodeSpeed)
		camera:setTargetCameraPosition(Vector3(camera:getTargetCameraPosition() + dirTackNode))
	end
	if  count == 13000 then
		finish_cutSceneLevel3()
		return true
	end

	return false
end
