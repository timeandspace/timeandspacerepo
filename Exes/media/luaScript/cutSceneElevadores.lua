-----------------------------------
-- CUT SCENE PUZLE ALPHA --
-----------------------------------
function finish_cutSceneElevadores ()
	cutScene:sendSwitchMessage(0, false)
end

function cutSceneElevadores (count, msecs)
	
	local cameraSpeed = 4 * msecs * 0.001
	local trackNodeSpeed = 4 * msecs * 0.001
	local dir = Vector3.ZERO
	local dirTackNode = Vector3.ZERO
	
	-- ñapa para las platformas con autoStart, se desactivan al inicio y se activan al finalizar el cutScene
	if count == 100 then
		camera:setCameraPosition(Vector3(100, 30, 20))
		camera:setTargetCameraPosition(Vector3(camera:getCameraPosition()+dir))
	end
	count = count - 2000 -- Retardo para el v-sync
	if count == 100 then
		cutScene:sendSwitchMessage(0, true)
	end
	if count == 1000 then
		dir = Vector3(1, 0, 0)
		camera:setCameraPosition(Vector3(16, 3, 5))
		camera:setTargetCameraPosition(Vector3(0, 3, 5))
	elseif count > 1000 and count <= 2000 then
		dir = Vector3.NEGATIVE_UNIT_X
	elseif count > 2000 and count <= 5000 then
		dir = Vector3.NEGATIVE_UNIT_X
		dirTackNode = Vector3.NEGATIVE_UNIT_Z
	elseif count > 5000 and count <= 8000 then
		dir = Vector3.UNIT_Y
		dirTackNode = Vector3.UNIT_Y
	elseif count > 8000 and count < 13000 then
		cameraSpeed = 8 * msecs * 0.001
		trackNodeSpeed = 8 * msecs * 0.001
		dir = Vector3.NEGATIVE_UNIT_Z
		dirTackNode = Vector3.NEGATIVE_UNIT_Z
	elseif  count == 13000 then
		finish_cutSceneElevadores()
		return true
	end
	
	dir = Vector3(dir * cameraSpeed)
	-- dirTackNode:normalise()
	dirTackNode = Vector3(dirTackNode * trackNodeSpeed)
	camera:setCameraPosition(Vector3(camera:getCameraPosition() + dir))
	camera:setTargetCameraPosition(Vector3(camera:getTargetCameraPosition() + dirTackNode))

	return false
end
