--------------------------------
-- CUTSCENE PASILLO 2 --
--------------------------------

-----------------------------------------------------
-- Funcion auxiliar que se encarga de volver 
-- a colocar las puertas en su sitio
------------------------------------
function finish_cutScenePasillo2 ()
	cutScene:sendSwitchMessage(0, false)
end

-------------------------------
-- CutScene del pasillo 2
-------------------------------
function cutScenePasillo2 (count, msecs)

	local cameraSpeed = 8 * msecs * 0.001
	local trackNodeSpeed = 8 * msecs * 0.001
	local dir = Vector3.ZERO
	local dirTackNode = Vector3.ZERO
	
	if count == 100 then
		camera:setCameraPosition(Vector3(100, 30, 20))
		camera:setTargetCameraPosition(Vector3(camera:getCameraPosition()+dir))
	end
	-- Espero 1 seg. para evitar que pegue tanto tirón con el V-Sync activado
	count = count - 3000
	if count == 1000 then
		cutScene:sendSwitchMessage(0, true)
		camera:setCameraPosition(Vector3(77.876, 4, 36))
		camera:setTargetCameraPosition(Vector3(77.876, 2, 25))
	
	elseif count > 1000 and count <= 4825 then
		dir = Vector3.NEGATIVE_UNIT_Z
		if count > 3425  then
			dirTackNode = Vector3.NEGATIVE_UNIT_X
		else
			dirTackNode = Vector3.NEGATIVE_UNIT_Z
		end
	elseif count > 4825 and count < 11500 then
		dir = Vector3.NEGATIVE_UNIT_X
		dirTackNode = Vector3.NEGATIVE_UNIT_X
		
	elseif count == 11500 then
		finish_cutScenePasillo2()
		return true
	end
	
	dir = Vector3(dir * cameraSpeed)
	dirTackNode:normalise()
	dirTackNode = Vector3(dirTackNode * trackNodeSpeed)
	camera:setCameraPosition(Vector3(camera:getCameraPosition() + dir))
	camera:setTargetCameraPosition(Vector3(camera:getTargetCameraPosition() + dirTackNode))

	return false
end
