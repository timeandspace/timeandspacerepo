Map = {

	SpawnPoint = {
		type = "SpawnPoint",
		position = {-1, 0.1, 28.4},
		orientation = 0,
		entityOrientation = 0,
		entityToSpawn = "Player",
	},
	
	Camera = {
		position = {50, 50, 50},
		type = "Camera",
		nearClip = 0.1,
		farClip = 3500,
		distance = 6,
		height = 4,
		targetDistance = 0,
		targetHeight = 1.8,
		offset = -0.8,
		offsetSpeed = 0.1,
		zoom =  2.5,
		zoomSpeed = 1,
		isStatic = false,
		lookAt = {0, 2, 0},
	},
	
}
