# Resource locations for the levels

[Level1]
ResourceGroups=General,Pasillo
LogicMapFile=../media/levels/maps/Pasillo.txt
SceneResourceGroup=Pasillo
DotSceneFile=Pasillo.scene
PhysicFile=../media/levels/physicFiles/Pasillo.Repx

[Level2]
ResourceGroups=General,PuzleAlfa
LogicMapFile=../media/levels/maps/map1.txt
SceneResourceGroup=PuzleAlfa
DotSceneFile=puzzleAlfa.scene
PhysicFile=../media/levels/physicFiles/puzzleAlfa.Repx

[Level3]
ResourceGroups=General,Elevadores
LogicMapFile=../media/levels/maps/Elevadores.txt
SceneResourceGroup=Elevadores
DotSceneFile=Elevadores.scene
PhysicFile=../media/levels/physicFiles/Elevadores.Repx


[Level4]
ResourceGroups=General,MapaJuan2
LogicMapFile=../media/levels/maps/MapaJuan2.txt
SceneResourceGroup=MapaJuan2
DotSceneFile=MapaJuan2.scene
PhysicFile=../media/levels/physicFiles/MapaJuan2.Repx

[Level5]
ResourceGroups=General,Pasillo,Pasillo2
LogicMapFile=../media/levels/maps/Pasillo2.txt
SceneResourceGroup=Pasillo2
DotSceneFile=Pasillo2.scene
PhysicFile=../media/levels/physicFiles/Pasillo2.Repx

[Level6]
ResourceGroups=General,LaberintoChinotauro
LogicMapFile=../media/levels/maps/LaberintoChinotauro.txt
SceneResourceGroup=LaberintoChinotauro
DotSceneFile=LaberintoChinotauro.scene
PhysicFile=../media/levels/physicFiles/LaberintoChinotauro.Repx

[Level7]
ResourceGroups=General,EscenarioPruebaIA
LogicMapFile=../media/levels/maps/mapWaypoints.txt
SceneResourceGroup=EscenarioPruebaIA
DotSceneFile=EscenarioIA.scene
PhysicFile=../media/levels/physicFiles/EscenarioIA.Repx

[Level8]
ResourceGroups=General,EscenarioCombate
LogicMapFile=../media/levels/maps/mapCombate.txt
SceneResourceGroup=EscenarioCombate
DotSceneFile=EscenarioCombate.scene
PhysicFile=../media/levels/physicFiles/EscenarioCombate.Repx

[Level10]
ResourceGroups=General,Pasillo
LogicMapFile=../media/levels/maps/map3.txt
SceneResourceGroup=Pasillo
DotSceneFile=cubo.scene
PhysicFile=../media/levels/physicFiles/cubo.Repx
