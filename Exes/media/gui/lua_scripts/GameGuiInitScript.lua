

function copias(event)
   local customarg = tolua.cast(event,"GUI::CustomEventArg")
   local logger = CEGUI.Logger:getSingleton()
   logger:logEvent("copias_changed")
   
   local guiSystem = CEGUI.System:getSingleton()
   guiSystem:getDefaultGUIContext():getRootWindow():getChild("LifeBar"):getChild("Copies"):setText(tostring(customarg.num))
   
   logger:logEvent(tostring(customarg.str))
  
end

function damage(event)
   local customarg = tolua.cast(event,"GUI::CustomEventArg")
   local logger = CEGUI.Logger:getSingleton()
   logger:logEvent("damage")
   
   local guiSystem = CEGUI.System:getSingleton()
   
   local lifebar = guiSystem:getDefaultGUIContext():getRootWindow():getChild("LifeBar"):getChild("LifeProgress")
   
   local scroller = CEGUI.toProgressBar(lifebar)
   
   logger:logEvent(customarg.floatValue)
   
   scroller:setProgress(customarg.floatValue)
end

function cross_hairVisible(event)
   local customarg = tolua.cast(event,"GUI::CustomEventArg")
   local logger = CEGUI.Logger:getSingleton()
   logger:logEvent("cross_hairVisible")
   
   local guiSystem = CEGUI.System:getSingleton()
   local root = guiSystem:getDefaultGUIContext():getRootWindow()
   root:getChild("Crosshair"):setVisible(customarg.boolValue)
   logger:logEvent(tostring(customarg.str))
  
end

function timedialogEvent(event)
   local customarg = tolua.cast(event,"GUI::CustomEventArg")
   local logger = CEGUI.Logger:getSingleton()
   logger:logEvent("timedialogEvent")
   
   local guiSystem = CEGUI.System:getSingleton()
   local root = guiSystem:getDefaultGUIContext():getRootWindow()
   root:getChild("TimeDialog"):setVisible(customarg.boolValue)
   
   root:getChild("TimeDialog"):getChild("TimeText"):setText(customarg.str)
   
   logger:logEvent(tostring(customarg.str))
end

function spacedialogEvent(event)
   local customarg = tolua.cast(event,"GUI::CustomEventArg")
   local logger = CEGUI.Logger:getSingleton()
   logger:logEvent("timedialogEvent")
   
   local guiSystem = CEGUI.System:getSingleton()
   local root = guiSystem:getDefaultGUIContext():getRootWindow()
   root:getChild("SpaceDialog"):setVisible(customarg.boolValue)
   
   root:getChild("SpaceDialog"):getChild("SpaceText"):setText(customarg.str)
   
   logger:logEvent(tostring(customarg.str))
end

function cinemaMode(event)
   local customarg = tolua.cast(event,"GUI::CustomEventArg")
   local guiSystem = CEGUI.System:getSingleton()
   local root = guiSystem:getDefaultGUIContext():getRootWindow()
   local logger = CEGUI.Logger:getSingleton()
   local root = guiSystem:getDefaultGUIContext():getRootWindow()
   
   local activate = customarg.boolValue
   
   root:getChild("CinemaBar1"):setVisible(activate)
   root:getChild("CinemaBar2"):setVisible(activate)
   root:getChild("LevelTitle"):setText(customarg.str)
   root:getChild("LevelTitle"):setVisible(activate)
   
   root:getChild("LifeBar"):setVisible(not activate)
   
    if activate then
      logger:logEvent("CINEMA ENABLED")
    else
      logger:logEvent("CINEMA DISABLED")
    end
end

function options_clicked(event)
   local we = CEGUI.toWindowEventArgs(event)
   local logger = CEGUI.Logger:getSingleton()
   logger:logEvent("options_clicked")
   
   Application.CMenuState:getInstance():startReleased()
end

function exit_clicked(event)
   local we = CEGUI.toWindowEventArgs(event)
   local logger = CEGUI.Logger:getSingleton()
   logger:logEvent("exit_clicked")
   
   Application.CMenuState:getInstance():exitReleased()
end

-----------------------------------------
-- Script Entry Point
-----------------------------------------
local guiSystem = CEGUI.System:getSingleton()
local schemeMgr = CEGUI.SchemeManager:getSingleton()
local winMgr = CEGUI.WindowManager:getSingleton()
local logger = CEGUI.Logger:getSingleton()

logger:logEvent( ">>> GameGuiInit script says hello" )

local root = winMgr:loadLayoutFromFile("HUD.layout")
guiSystem:getDefaultGUIContext():setRootWindow(root)
guiSystem:getDefaultGUIContext():getMouseCursor():setVisible(false)

root:getChild("SpaceDialog"):setVisible(false)
root:getChild("TimeDialog"):setVisible(false)
root:getChild("Crosshair"):setVisible(false)
root:getChild("CinemaBar1"):setVisible(false)
root:getChild("CinemaBar2"):setVisible(false)
root:getChild("LevelTitle"):setVisible(false)


CEGUI.System:getSingleton():subscribeEvent("copias","copias")
CEGUI.System:getSingleton():subscribeEvent("crosshair","cross_hairVisible")
CEGUI.System:getSingleton():subscribeEvent("timeEvent","timedialogEvent")
CEGUI.System:getSingleton():subscribeEvent("spaceEvent","spacedialogEvent")
CEGUI.System:getSingleton():subscribeEvent("cinemaEvent","cinemaMode")
-- CEGUI.System:getSingleton():subscribeEvent("damage","damage")


logger:logEvent( ">>> GameGuiInit script says Goodbye" )


