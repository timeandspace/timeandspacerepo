

function resume_clicked(event)
   CEGUI.Logger:getSingleton():logEvent("resume_clicked")
   
   local state = GUI.CHudManager:getSingletonPtr():getState()
   local menuState = tolua.cast(state,"Application::CPauseState")
   menuState:resumeReleased()
end

function restart_clicked(event)
   CEGUI.Logger:getSingleton():logEvent("restart_clicked")
   
   local state = GUI.CHudManager:getSingletonPtr():getState()
   CEGUI.Logger:getSingleton():logEvent("restart_clicked2")
   local menuState = tolua.cast(state,"Application::CPauseState")
   CEGUI.Logger:getSingleton():logEvent("restart_clicked3")
   menuState:restartLevelReleased()
   CEGUI.Logger:getSingleton():logEvent("restart_clicked4")
end

function menu_clicked(event)
   CEGUI.Logger:getSingleton():logEvent("menu_clicked")
   
   local state = GUI.CHudManager:getSingletonPtr():getState()
   local menuState = tolua.cast(state,"Application::CPauseState")
   menuState:menuReleased()
end

-----------------------------------------
-- Script Entry Point
-----------------------------------------
local guiSystem = CEGUI.System:getSingleton()
local schemeMgr = CEGUI.SchemeManager:getSingleton()
local winMgr = CEGUI.WindowManager:getSingleton()
local logger = CEGUI.Logger:getSingleton()

logger:logEvent( ">>> Pause script says hello" )

local root = winMgr:loadLayoutFromFile("Pause.layout")

guiSystem:getDefaultGUIContext():setRootWindow(root)
guiSystem:getDefaultGUIContext():getMouseCursor():setDefaultImage("TaharezLook/MouseArrow")
guiSystem:getDefaultGUIContext():getMouseCursor():setVisible(true)
--root:getChild("DisableScreen"):setAlpha(0.4)
--root:getChild("StartGame"):subscribeEvent("Clicked", "start_clicked")
--root:getChild("ExitGame"):subscribeEvent("Clicked", "exit_clicked")
root:getChild("Options"):getChild("ResumeGame"):subscribeEvent("Clicked", "resume_clicked")
root:getChild("Options"):getChild("RestartGame"):subscribeEvent("Clicked", "restart_clicked")
root:getChild("Options"):getChild("Menu"):subscribeEvent("Clicked", "menu_clicked")
logger:logEvent( ">>> Pause script says Goodbye" )
--root:getChild("DisableScreen"):setAlpha(0.4)

