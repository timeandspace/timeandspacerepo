-- get CEGUI singletons
local logger = CEGUI.Logger:getSingleton()
logger:logEvent( ">>> Init script says hello" )
--logger:setLoggingLevel( CEGUI.Informative )

-- get a local reference to the singletons we use (not required)
local system    = CEGUI.System:getSingleton()   
local fontman   = CEGUI.FontManager:getSingleton()
local schememan = CEGUI.SchemeManager:getSingleton()
local imageMan = CEGUI.ImageManager:getSingleton()

-- load schemes
schememan:createFromFile("TaharezLook.scheme")
schememan:createFromFile("AlfiskoSkin.scheme")
schememan:createFromFile("Generic.scheme")
schememan:createFromFile("GameMenu.scheme")
schememan:createFromFile("TASLook.scheme")
schememan:createFromFile("OgreTray.scheme")

-- load fonts
fontman:createFromFile("Batang-18.font")
fontman:createFromFile("DejaVuSans-10.font")
fontman:createFromFile("DejaVuSans-10-NoScale.font")
fontman:createFromFile("DejaVuSans-12.font")
fontman:createFromFile("DejaVuSans-12-NoScale.font")
fontman:createFromFile("DejaVuSans-14.font")
fontman:createFromFile("DejaVuSans-14-NoScale.font")
fontman:createFromFile("FairChar-30.font")
fontman:createFromFile("GreatVibes-16.font")
fontman:createFromFile("GreatVibes-22.font")
fontman:createFromFile("Jura-13.font")
fontman:createFromFile("Jura-18.font")

logger:logEvent( "<<< Init script says goodbye" )