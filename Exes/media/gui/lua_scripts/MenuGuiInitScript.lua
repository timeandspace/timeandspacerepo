

function start_clicked(event)
   CEGUI.Logger:getSingleton():logEvent("start_clicked")
   
   local state = GUI.CHudManager:getSingletonPtr():getState()
   local menuState = tolua.cast(state,"Application::CMenuState")
   menuState:startReleased()
end

function options_clicked(event)
   CEGUI.Logger:getSingleton():logEvent("options_clicked")
end

function exit_clicked(event)
   CEGUI.Logger:getSingleton():logEvent("exit_clicked")
   
   local state = GUI.CHudManager:getSingletonPtr():getState()
   local menuState = tolua.cast(state,"Application::CMenuState")
   menuState:exitReleased()
end

-----------------------------------------
-- Script Entry Point
-----------------------------------------
local guiSystem = CEGUI.System:getSingleton()
local schemeMgr = CEGUI.SchemeManager:getSingleton()
local winMgr = CEGUI.WindowManager:getSingleton()
local logger = CEGUI.Logger:getSingleton()

logger:logEvent( ">>> Menu script says hello" )

local root = winMgr:loadLayoutFromFile("Menu.layout")
guiSystem:getDefaultGUIContext():setRootWindow(root)
guiSystem:getDefaultGUIContext():getMouseCursor():setDefaultImage("TaharezLook/MouseArrow")
guiSystem:getDefaultGUIContext():getMouseCursor():setVisible(true)

root:getChild("OptionButtons"):getChild("StartGame"):subscribeEvent("Clicked", "start_clicked")
root:getChild("OptionButtons"):getChild("ExitGame"):subscribeEvent("Clicked", "exit_clicked")

logger:logEvent( ">>> Menu script says Goodbye" )


